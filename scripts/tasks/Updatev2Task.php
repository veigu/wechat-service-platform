<?php

/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-5-23
 * Time: 下午7:17
 */
use Models\Customer\CustomerStorage;
use \PDO;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Upload\Fdfs;
use Util\Config;
use Util\Debug;

class Updatev2Task extends \Phalcon\CLI\Task
{
    public $new_db_prefix = "";

    /**
     * @var Mysql
     */
    public $db = null;

    public function __construct()
    {
        set_time_limit(0);
        $this->new_db_prefix = 'dev_test';
        $this->db = new Mysql(array(
            'host' => '218.244.140.32',
            'username' => 'reach',
            'password' => '123456',
            'dbname' => 'mp_reach',
            'port' => '3306',
            "charset" => "utf8"
        ));
    }


    public function syncFSAction()
    {
        $conf = Config::getSite('upload', 'pic');
        Debug::log('----start------------------' . date('Y-m-d H:i:s') . '------------------------', 'info');
        $files = CustomerStorage::find(array('param is null or param=""', 'order' => 'id asc'));
        if (!$files) return;
        foreach ($files as $file) {
            Debug::log('id:' . $file->id . ' file:' . $file->url, 'info');

            $buff = $this->getLocalFile($file->url);

            if (!$buff) {
                continue;
            }

            // save into dfs
            $fileinfo = Fdfs::getInstance($conf['fastDFS'])->upload_filebuff($buff['buff'], $buff['ext']);
            $type = 'fail！';
            if ($fileinfo) {
                $file->update(array('url' => '/' . $fileinfo['group_name'] . '/' . $fileinfo['filename'], 'param' => '1'));
                //@unlink(ROOT . $file->url);
                $type = 'success！';
            }

            Debug::log('id:' . $file->id . '  ' . $type . ' file:' . $file->url, 'info');
        }

        Debug::log('----end------------------' . date('Y-m-d H:i:s') . '------------------------', 'info');
    }

    private function getLocalFile($url)
    {
        $file = ROOT . '/' . ltrim($url, '/');
        if (!file_exists($file)) {
            return false;
        }

        # 缓存文件及后缀
        $buff = file_get_contents($file);
        $ext = pathinfo($url, PATHINFO_EXTENSION);

        return array('buff' => $buff, 'ext' => $ext);
    }

    // 同步商品
    public function syncProductAction()
    {
        // 1. 同步分类
        sprintf('开始同步分类');
        $this->syncCat();

        // 2. 同步品牌
        $brands = $this->db->fetchAll('SELECT * FROM brands order by id asc ', PDO::FETCH_ASSOC);
        if ($brands) {
            foreach ($brands as $brand) {
                $item['id'] = $brand['id'];
                $item['name'] = $brand['name'];
                $item['customer_id'] = $brand['customer_id'];
                $item['logo'] = $brand['image'];
                $this->insertDb($this->new_db_prefix . '.product_brand', $item, true);
            }
        }

        // 3. 同步商品数据
        sprintf('开始同步商品');
        $res = $this->db->fetchAll('SELECT * FROM products order by id asc ', PDO::FETCH_ASSOC);
        if ($res) {
            foreach ($res as $item) {
                // 同步商品
                $this->syncItem($item);
                // 同步属性
                $this->syncItemAttr($item['id'], $item['customer_category_id']);
            }
        }

        sprintf('完成');
    }

    private function syncCat()
    {
        // 1 同步分类
        $cats = $this->db->fetchAll('SELECT c.*,cn.* FROM customer_categories as c left JOIN customer_category_description as cn ON cn.category_id=c.id', PDO::FETCH_ASSOC);
        foreach ($cats as $v) {
            $cat['id'] = $v['id'];
            $cat['customer_id'] = $v['customer_id'];
            $cat['image'] = $v['image'];
            $cat['parent_id'] = $v['pid'];
            $cat['is_parent'] = $v['top'];
            $cat['image'] = $v['image'];
            $cat['sort'] = $v['sort'];
            // 查找名字
            $cat['name'] = $v['name'];
            $cat['sub_name'] = $v['meta_keyword'];
            $cat['detail'] = $v['description'];
            $cat['created'] = $v['created'];
            $cat['modified'] = $v['modified'];
            $cat['published'] = 1;
            $cat['status'] = $v['status'];
            // Debug::log('cat:' . var_export($cat, true));

            $this->insertDb($this->new_db_prefix . '.product_cat ', $cat, true);
        }
    }

    public function syncItemAttr($item_id, $cid)
    {
        $attrs = $this->db->fetchAll('select a.name,v.value,v.created from product_attribute_values as v LEFT JOIN product_attributes as a on v.attribute_id = a.id where  product_id=' . 212, PDO::FETCH_ASSOC);
        if ($attrs) {
            foreach ($attrs as $v) {
                $feature['cid'] = $cid;
                $feature['attr_name'] = $v['name'];
                $feature['attr_type'] = 'input';
                $feature['is_active'] = true;
                $feature['created'] = $v['created'];

                // Debug::log($item_id . '-cat_feature:' . var_export($feature, true));
                // 先分类属性
                $fid = $this->insertDb($this->new_db_prefix . '.product_cat_feature', $feature, true);
                if ($fid) {
                    // 商品属性
                    $attr['product_id'] = $item_id;
                    $attr['attr_id'] = $fid;
                    $attr['attr_name'] = $v['name'];
                    $attr['attr_val'] = $v['value'];
                    $attr['attr_type'] = "input";
                    // Debug::log($item_id . '-attr:' . var_export($attr, true));
                    $this->insertDb($this->new_db_prefix . '.product_attr', $attr, true);
                }
            }
        }
    }

    public function syncItem($item)
    {
        // 单个商品信息
        $product['id'] = $item['id'];
        $product['customer_id'] = $item['customer_id'];
        $product['cid'] = $item['customer_category_id'];
        $product['name'] = $item['name'];
        $product['quantity'] = $item['quantity'];
        $product['brand_id'] = $item['brand_id'];
        $product['sell_price'] = $item['sell_price'];
        $product['original_price'] = $item['original_price'];
        $product['is_recommended'] = $item['is_recommended'];
        $product['is_hot'] = $item['is_hot'];
        $product['is_new'] = $item['is_new'];
        $product['created'] = $item['created'];
        $product['modified'] = $item['modified'];
        $product['is_active'] = $item['status'];
        $product['thumb'] = $item['thumb'];
        // 获取商品图片
        $imgs = $this->db->fetchAll('SELECT image FROM product_images WHERE product_id=' . $item['id'] . '  ORDER by sort desc ', PDO::FETCH_ASSOC);
        $imgs = $imgs ? array_column($imgs, 'image') : array();
        $imgs = $imgs ? implode(';', $imgs) : "";
        $product['imgs'] = $imgs;
        // 商品详情
        $desc = $this->db->fetchOne('SELECT `desc` FROM product_description WHERE product_id=' . $item['id'], PDO::FETCH_ASSOC);
        $product['detail'] = $desc ? $desc['desc'] : "";
        $product['customer_id'] = $item['customer_id'];
        $product['customer_id'] = $item['customer_id'];
        // Debug::log($item['id'] . '-product:' . var_export($product, true));

        $this->insertDb($this->new_db_prefix . '.product ', $product, true);
    }

    public function updatedbAction()
    {
        $sql = [
            "UPDATE customer_menus SET target_value = REPLACE(target_value, '/list/topic/', '/article/topic/');",
            "UPDATE customer_menus SET target_value = REPLACE(target_value, '/list?pid', '/article?pid');",
            "UPDATE customer_menus SET target_value = REPLACE(target_value, '/page/detail/', '/article/detail/');",
            "UPDATE customer_menus SET target_value = REPLACE(target_value, '/module/run/', '/addon/');",
            "UPDATE customer_menus SET default_link = REPLACE(default_link, '/list/topic/', '/article/topic/');",
            "UPDATE customer_menus SET default_link = REPLACE(default_link, '/list?pid', '/article?pid');",
            "UPDATE customer_menus SET default_link = REPLACE(default_link, '/page/detail/', '/article/detail/');",
            "UPDATE customer_menus SET default_link = REPLACE(default_link, '/module/run/', '/addon/');",
            "UPDATE message_settings SET default_link = REPLACE(default_link, '/list/topic/', '/article/topic/');",
            "UPDATE message_settings SET default_link = REPLACE(default_link, '/list?pid', '/article?pid');",
            "UPDATE message_settings SET default_link = REPLACE(default_link, '/page/detail/', '/article/detail/');",
            "UPDATE message_settings SET default_link = REPLACE(default_link, '/module/run/', '/addon/');",
            "UPDATE message_setting_mass SET default_link = REPLACE(default_link, '/list/topic/', '/article/topic/');",
            "UPDATE message_setting_mass SET default_link = REPLACE(default_link, '/list?pid', '/article?pid');",
            "UPDATE message_setting_mass SET default_link = REPLACE(default_link, '/page/detail/', '/article/detail/');",
            "UPDATE message_setting_mass SET default_link = REPLACE(default_link, '/module/run/', '/addon/');",
            "UPDATE message_setting_keywords SET default_link = REPLACE(default_link, '/list/topic/', '/article/topic/');",
            "UPDATE message_setting_keywords SET default_link = REPLACE(default_link, '/list?pid', '/article?pid');",
            "UPDATE message_setting_keywords SET default_link = REPLACE(default_link, '/page/detail/', '/article/detail/');",
            "UPDATE message_setting_keywords SET default_link = REPLACE(default_link, '/module/run/', '/addon/');",
            "UPDATE site_navs SET link = REPLACE(link, '/list/topic/', '/article/topic/');",
            "UPDATE site_navs SET link = REPLACE(link, '/list?pid', '/article?pid');",
            "UPDATE site_navs SET link = REPLACE(link, '/page/detail/', '/article/detail/');",
            "UPDATE site_navs SET link = REPLACE(link, '/module/run/', '/addon/');",
            "UPDATE site_focus SET link = REPLACE(link, '/list/topic/', '/article/topic/');",
            "UPDATE site_focus SET link = REPLACE(link, '/list?pid', '/article?pid');",
            "UPDATE site_focus SET link = REPLACE(link, '/page/detail/', '/article/detail/');",
            "UPDATE site_focus SET link = REPLACE(link, '/module/run/', '/addon/');"
        ];

        foreach ($sql as $s) {
            $this->db->query($s);
        }
    }

    public function insertDb($table, $data, $ignore = false)
    {
        ksort($data);
        $ignore = $ignore ? ' IGNORE ' : ' ';
        $fieldNames = implode(', ', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));
        $sql = "INSERT " . $ignore . " INTO $table ($fieldNames) VALUES ($fieldValues)";

        $sth = $this->db->prepare($sql);

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        if (!$sth->execute()) {
            // Debug::log('ERR:insert--' . $sql);
        }

        return $this->db->lastInsertId();
    }

    // 同步商家信息
    public function syncCustomerAction()
    {
        // 同步商户
        // 1 同步分类
        $customers = $this->db->fetchAll('SELECT * FROM customers', PDO::FETCH_ASSOC);
        foreach ($customers as $item) {
            // customer
            $customer['id'] = $item['id'];
            $customer['host_key'] = $item['host_key'];
            $customer['account'] = $item['account'];
            $customer['password'] = $item['password'];
            $customer['email'] = $item['email'];
            $customer['name'] = $item['name'];
            $customer['industry'] = $item['industry'];
            $customer['active'] = $item['active'];
            $customer['guide_steps'] = '{"profile":true,"pick":true,"order":true}';
            $customer['created'] = $item['created'];
            $this->insertDb($this->new_db_prefix . '.customers', $customer, true);

            // profile
            $profile['customer_id'] = $item['id'];
            $profile['company_name'] = $item['name'];
            $profile['province'] = $item['province'];
            $profile['city'] = $item['city'];
            $profile['town'] = $item['town'];
            $profile['address'] = $item['id'];
            $profile['contact_person'] = $item['contact'];
            $profile['contact_email'] = $item['email'];
            $profile['contact_phone'] = $item['cellphone'];
            $profile['contact_tel'] = $item['telephone'];
            $this->insertDb($this->new_db_prefix . '.customer_profile', $profile, true);

            // package
            $package['customer_id'] = $item['id'];
            $package['has_shop_full'] = 1;
            $package["package"] = 'full';
            $package['use_start'] = time();
            $package['use_end'] = strtotime("+2 year");
            $package['duration'] = 11; // 2年
            $package['limit_module'] = '[]';
            $package['own_module'] = '[]';
            $this->insertDb($this->new_db_prefix . '.customer_package', $package, true);
        }
    }

    public function syncSiteInfo()
    {

    }
}