<?php
// wap host rule
/**
 * 匹配域名 -> 微站三级域名中二级域名段
 * 如 1234067.m.main-domain.com 中'main-domain.com'=>'m'
 */

return new \Phalcon\Config(array(
    'database' => array(
        'adapter' => 'Mysql',
        "host" => "112.124.106.166",
        "username" => "estt",
        "password" => "Estt@123",
        "dbname" => "wx_reach_dev",
        "charset" => "utf8"
    ),

    'mssql' => array(
        'adapter' => 'Mssql',
        "host" => "112.124.106.166:1433",
        "username" => "dev1",
        "password" => "dev1_123456",
        "dbname" => "dev1",
        "charset" => "utf8"
    ),

    //queue server
    'beanstalk' => array(
        "host" => "112.124.106.166",
        "port" => "11300",
    ),

    'mongodb' => array(
        "host" => "112.124.106.166",
        "port" => "27017",
    ),

    'memcached' => array(
        'host' => '112.124.106.166',
        'port' => '11211',
        'lifetime' => 172800, // Cache data for 2 days
        'prefix' => 'dev7'
    ),

    'redis' => array(
        'host' => '112.124.106.166',
        'port' => '6379',
        'name' => '',
        'lifetime' => '17200',
        'cookie_lifetime' => 3600 // Cache data for 2 days
    ),

    'defaultApp' => array(
        'baseUri' => 'http://www.saleasy.net/'
    ),

    'metadata' => array(
        "adapter" => "Apc",
        "suffix" => "my-suffix",
        "lifetime" => "86400"
    ),

    'payment' => array(
        'alipay' => array(
            'sign_type' => 'MD5',
            'input_charset' => 'utf-8',
            'cacert' => getcwd() . DIRECTORY_SEPARATOR . 'cacert.pem',
            'transport' => 'http'
        ),
        'wxpay' => array(
            'SIGNTYPE' => 'sha1'
        ),
        'cbpay' => array(
            'name' > "深圳市智享时代科技有限公司",
            'bank' => "中国建设银行股份有限公司深圳南山大道支行",
            'account' => '44201583900052505254',
        )
    ),
    'customer_config' => array(
        'main' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'smsbao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                ),
                'mandao' => array(
                    'user_name' => 'SDK-WSS-010-06946',
                    'pass_word' => '32b6-[a0',
                    'sign' => '易卖移动电商'
                ),
                'emoi' => array(
                    'user_name' => 'ERPSMS',
                    'pass_word' => 'passw0rd@SMS',
                    'sign' => 'emoi'
                ),
            )
        ),

        'reach' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'smsbao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                ),
                'mandao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                )
            )
        ),
        'yimasou' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'smsbao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                ),
                'mandao' => array(
                    'user_name' => 'SDK-WSS-010-06946',
                    'pass_word' => '32b6-[a0'
                )
            )
        ),
        'estt' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'smsbao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                ),
                'mandao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                )
            )
        )
    ),

));
