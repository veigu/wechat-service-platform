<?php

use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\DI\FactoryDefault;
use Phalcon\Events\Manager as EventManager;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileLog;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('config', $config);

$di->set('router', function () {
    return require ROOT . '/Config/routes.php';
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('mongo', function () use ($config) {
    $mongo = new \MongoClient("mongodb://" . $config->mongodb->host . ":" . $config->mongodb->port, array("connect" => TRUE));
//    $mongo = new \Phalcon\Db\Adapter\Mongo\Client("mongodb://" . $config->mongodb->host . ":" . $config->mongodb->port, array("connect" => TRUE));
    return $mongo->selectDB('wx_service');
}, true);

/**
 * When you use mongodb, you have to set up this listener for mongo model
 */
$di->set('collectionManager', function () {
    return new Phalcon\Mvc\Collection\Manager();
}, true);


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () use ($config) {
    if (isset ($config->models->metadata)) {
        $metaDataConfig = $config->models->metadata;
        $metadataAdapter = 'Phalcon\Mvc\Model\Metadata\\' . $metaDataConfig->adapter;
        return new $metadataAdapter ();
    }
    return new \Phalcon\Mvc\Model\Metadata\Memory();
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () use ($config) {
    $session = new Phalcon\Session\Adapter\Redis(array(
        'path' => $config->redis->host,
        'name' => 'session_key',
        'lifetime' => 17200,
        'cookie_lifetime' => 17200
    ));
    $session->start();
    return $session;
});

/**
 * Register volt as one of the view template engines
 */
$di->set('volt', function ($view, $di) {
    $volt = new \Phalcon\Mvc\View\Engine\Volt ($view);

    $volt->setOptions(array(
        "compiledPath" => "Cache/tpl/",
        "compiledExtension" => '.php',
        'compiledSeparator' => '-',
        "compileAlways" => true
    ));
    return $volt;
});


/**
 * Register the flash service with custom CSS classes
 */
$di->set('flash', function () {
    return new Phalcon\Flash\Direct (array(
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info'
    ));
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
    $eventsManager = new EventManager();
    $filePath = ROOT . "/Cache/log/sql/" . date('Ymd') . '/' . date('H') . ".log";
    $path = dirname($filePath);
    if (!is_dir($path)) {
        @mkdir($path, 0777, true);
    }
    if (file_exists($filePath)) {
        @chmod($filePath, FILE_WRITE_MODE);
    }

    $logger = new FileLog($filePath);

    //Listen all the database events
    $eventsManager->attach('db', function ($event, $connection) use ($logger) {
        if ($event->getType() == 'beforeQuery') {
            $logger->log($connection->getRealSQLStatement(), Logger::INFO);
        }
    });

    $adapter = new DbAdapter(array(
        "adapter" => $config->database->adapter,
        "host" => $config->database->host,
        "username" => $config->database->username,
        "password" => $config->database->password,
        "dbname" => $config->database->dbname,
        "charset" => $config->database->charset,
    ));

    $adapter->setEventsManager($eventsManager);
    return $adapter;
});

$di->set("messenger", function () use ($config, $di) {
    $adapter = $config->customer_config->{HOST_KEY}->messenger->adapter;
    $adapterClass = "\\Components\\ShortMessenger\\Adapter\\" . ucwords($adapter);
    if (class_exists($adapterClass)) {
        $adapter = new $adapterClass();
        return $adapter;
    } else {
        return new Components\ShortMessenger\Adapter\Mandao();
    }
}, true);

$di->set('debugLogger', function () {
    $filePath = ROOT . "/Cache/log/debug/" . date('Ymd') . '/' . date('H') . ".log";
    $path = dirname($filePath);
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
    if (file_exists($filePath)) {
        @chmod($filePath, FILE_WRITE_MODE);
    }
    $logger = new \Phalcon\Logger\Adapter\File($filePath);
    return $logger;
});
$di->set('errorLogger', function () {
    $filePath = ROOT . "/Cache/log/error/" . date('Ymd') . '/' . date('H') . ".log";
    $path = dirname($filePath);
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
    if (file_exists($filePath)) {
        @chmod($filePath, FILE_WRITE_MODE);
    }
    $logger = new \Phalcon\Logger\Adapter\File($filePath);
    return $logger;
});
$di->set('wechatLogger', function () {
    $filePath = ROOT . "/Cache/log/wechat/" . date('Ymd') . '/' . date('H') . ".log";
    $path = dirname($filePath);
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
    if (file_exists($filePath)) {
        @chmod($filePath, FILE_WRITE_MODE);
    }
    $logger = new \Phalcon\Logger\Adapter\File($filePath);
    return $logger;
});

$di->set('paymentLogger', function () {
    $filePath = ROOT . "/Cache/log/payment/" . date('Ymd') . '/' . date('H') . ".log";
    $path = dirname($filePath);
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
    if (file_exists($filePath)) {
        @chmod($filePath, FILE_WRITE_MODE);
    }
    $logger = new \Phalcon\Logger\Adapter\File($filePath);
    return $logger;
});

$di->set('weiboLogger', function () {
    $filePath = ROOT . "/Cache/log/weibo/" . date('Ymd') . '/' . date('H') . ".log";
    $path = dirname($filePath);
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
    if (file_exists($filePath)) {
        @chmod($filePath, FILE_WRITE_MODE);
    }
    $logger = new \Phalcon\Logger\Adapter\File($filePath);
    return $logger;
});

$di->set('paymentLogger', function () {
    $filePath = ROOT . "/Cache/log/payment/" . date('Ymd') . '/' . date('H') . ".log";
    $path = dirname($filePath);
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
    if (file_exists($filePath)) {
        @chmod($filePath, FILE_WRITE_MODE);
    }
    $logger = new \Phalcon\Logger\Adapter\File($filePath);
    return $logger;
});

//Set a cache server for applications
$di->set('memcached', function () use ($config) {
    //Cache data for one hour
    $frontCache = new \Phalcon\Cache\Frontend\Output(array(
        "lifetime" => $config->memcached->lifetime
    ));
// Memcached connection settings
    return new \Phalcon\Cache\Backend\Libmemcached($frontCache, array(
        'servers' => array(
            array(
                'host' => $config->memcached->host,
                'port' => $config->memcached->port,
                'weight' => 1
//                 'persistent' => isset($config->memcached->persistent) ? $config->memcached->persistent : true
            ),
        ),
        'client' => array(
            Memcached::OPT_HASH => Memcached::HASH_MD5,
            Memcached::OPT_PREFIX_KEY => $config->memcached->prefix,
        )
    ));
});

//Set a cache server for applications
$di->set('redis', function () use ($config) {
    // Cache data for 2 days
    $frontCache = new Phalcon\Cache\Frontend\Data(array(
        "lifetime" => $config->redis->lifetime
    ));

    return new Phalcon\Cache\Backend\Redis($frontCache, array(
        "host" => $config->redis->host,
        "port" => $config->redis->port
    ));
});

$di->set('beanstalk', function () use ($config) {
    return new Phalcon\Queue\Beanstalk(array(
        'host' => $config->beanstalk->host,
        'port' => $config->beanstalk->port
    ));
});

$di->set('uri', function () use ($config) {
    return new \Util\Uri();
});
