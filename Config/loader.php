<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(array(__DIR__ . '/..Llibrary/'))
    ->registerNamespaces(array(
        "Components" => __DIR__ . '/../Library/Components',
        "Util" => __DIR__ . '/../Library/Util',
        "Download" => __DIR__ . '/../Library/Download',
        "Models" => __DIR__ . '/../Models',
        "Upload" => __DIR__ . '/../Library/Upload',
        "Library\\O2O" => __DIR__ . '/../Library/O2O',
        "PHPMailer" => __DIR__ . '/../Library/PHPMailer',
        "Phalcon" => __DIR__ . '/../Library/incubator/Library/Phalcon',
    ))->register();

$loader->registerClasses(
    array(
        "Phalcon\\Db\\Adapter\\Pdo\\Mssql" => __DIR__ . '/../Library/Components/Phalcon/Db/Adapter/Pdo/Mssql.php',
        "Phalcon\\Db\\Adapter\\Dialect\\Mssql" => __DIR__ . '/../Library/Components/Phalcon/Db/Dialect/Mssql.php',
    )
)->register();
