<?php

define('MAIN_DOMAIN', "dev2.isaleasy.com"); //主域名


return new \Phalcon\Config(array(
    'database' => array(
        'adapter' => 'Mysql',
        "host" => "112.124.106.166",
        "username" => "estt",
        "password" => "Estt@123",
        "dbname" => "saleasy_test",
        "charset" => "utf8"
    ),

    'uploadSaveType' => 'fdfs',//fdfs|normal
    'freeMode' => false,//注册时是否免费（行指平台）

    //queue server
    'beanstalk' => array(
        "host" => "112.124.106.166",
        "port" => "11300",
    ),

    'mongodb' => array(
        "host" => "112.124.106.166",
        "port" => "27017",
    ),

    'memcached' => array(
        'host' => '112.124.106.166',
        'port' => '11211',
        'lifetime' => 172800, // Cache data for 2 days
        'prefix' => $_SERVER['HTTP_HOST']
    ),

    'redis' => array(
        'host' => '112.124.106.166',
        'port' => '6379',
        'name' => '',
        'lifetime' => '17200',
        'cookie_lifetime' => 3600 // Cache data for 2 days
    ),

    'metadata' => array(
        "adapter" => "Apc",
        "suffix" => "my-suffix",
        "lifetime" => "86400"
    ),
    'payment' => array(
        'alipay' => array(
            'sign_type' => 'MD5',
            'input_charset' => 'utf-8',
            'cacert' => getcwd() . DIRECTORY_SEPARATOR . 'cacert.pem',
            'transport' => 'http'
        ),
        'wxpay' => array(
            'SIGNTYPE' => 'sha1'
        ),
        'cbpay' => array(
            'name' > "深圳市智享时代科技有限公司",
            'bank' => "中国建设银行股份有限公司深圳南山大道支行",
            'account' => '44201583900052505254',
        )
    ),
    'customer_config' => array(
        'main' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'smsbao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                ),
                'mandao' => array(
                    'user_name' => 'SDK-WSS-010-06946',
                    'pass_word' => '32b6-[a0',
                    'sign' => '易卖移动电商'
                ),
                'emoi' => array(
                    'user_name' => 'ERPSMS',
                    'pass_word' => 'passw0rd@SMS',
                    'sign' => 'emoi'
                ),
            )
        ),

        'reach' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'smsbao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                ),
                'mandao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                )
            )
        ),
        'yimasou' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'smsbao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                ),
                'mandao' => array(
                    'user_name' => 'SDK-WSS-010-06946',
                    'pass_word' => '32b6-[a0'
                )
            )
        ),
        'estt' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'mandao' => array(
                    'user_name' => 'SDK-WSS-010-06946',
                    'pass_word' => '32b6-[a0',
                    'sign' => '易卖移动电商'
                )
            )
        ),
        'yimai' => array(
            'messenger' => array(
                'adapter' => 'mandao',
                'smsbao' => array(
                    'user_name' => '',
                    'pass_word' => ''
                ),
                'mandao' => array(
                    'user_name' => 'SDK-WSS-010-06946',
                    'pass_word' => '32b6-[a0',
                    'sign' => '易卖移动电商'
                ),
                'emoi' => array(
                    'user_name' => 'ERPSMS',
                    'pass_word' => 'passw0rd@SMS',
                    'sign' => 'emoi'
                ),
            )
        )

    ),

));
