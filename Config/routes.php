<?php
// 通过主域查找
$hosts = \Models\System\SystemHost::findFirst('domain="' . MAIN_DOMAIN . '"');
if (!$hosts) {
    die("对不起，您的域名不在授权范围内！");
}
$hosts = $hosts->toArray();
if (!defined('HOST_KEY')) define('HOST_KEY', $hosts["key"]);
if (!defined('STATIC_DOMAIN')) define('STATIC_DOMAIN', $hosts["static_host"]); //静态文件（上传）域名
if (!defined('WAP_DOMAIN_END')) define('WAP_DOMAIN_END', ltrim($hosts["wap_host"], '*.'));// 匹配wap域名,带*.开头的就customer的id，不带为1（如*.m.wap.cn或m.emoi.cn）
if (!defined('WAP_DOMAIN_ALONE')) define('WAP_DOMAIN_ALONE', strpos($hosts["wap_host"], '*.') === 0 ? false : true);//是否wap独立域名（如m.emoi.cn）
if (!defined('HOST_BRAND')) define('HOST_BRAND', $hosts["name"]);
if (!defined('FRONT_DOMAIN')) define('FRONT_DOMAIN', $hosts["frontend_host"]);

//宣传host
if (!FRONT_DOMAIN) {
    //不存在,直接到后台管理（针对类似emoi单独分发）
    $router = new Phalcon\Mvc\Router(false);
    $router->setDefaultModule("panel");
    $router->setDefaultController('account');
    $router->setDefaultAction('login');
} else {
    // 宣传网站
    $router = new Phalcon\Mvc\Router(false);
    $router->setDefaultModule("home");
    $router->setDefaultController('index');
    $router->setDefaultAction('index');

    $_router_module[] = array('module' => 'home', 'root_path' => '/home', 'namespace' => '');
    $_router_module[] = array('module' => 'admin', 'root_path' => '/admin', 'namespace' => '');
}
// payment
$_router_module[] = array('module' => 'payment', 'root_path' => '/payment', 'namespace' => '');
// panel
$_router_module[] = array('module' => 'panel', 'root_path' => '/panel', 'namespace' => '');
$_router_module[] = array('module' => 'api', 'root_path' => '/api', 'namespace' => '');
$_router_module[] = array('module' => 'panel', 'root_path' => '/api/addon', 'namespace' => 'Modules\Api');
$_router_module[] = array('module' => 'shop', 'root_path' => '/shop', 'namespace' => '');
// o2o
$_router_module[] = array('module' => 'o2o_home', 'root_path' => '/o2o', 'namespace' => '');
$_router_module[] = array('module' => 'o2o_app', 'root_path' => '/o2o_app', 'namespace' => '');
$_router_module[] = array('module' => 'o2o_app', 'root_path' => '/o2o_app/api', 'namespace' => 'O2O\App\Api');
$_router_module[] = array('module' => 'o2o_operator', 'root_path' => '/o2o_operator', 'namespace' => '');
$_router_module[] = array('module' => 'o2o_operator', 'root_path' => '/o2o_operator/api', 'namespace' => 'O2O\Operator\Api');
$_router_module[] = array('module' => 'o2o_home', 'root_path' => '/o2o_home/api', 'namespace' => 'O2O\Home\Api');

/**
 * 微网站
 * -- 子域名方式域名
 */
$pos = strpos($_SERVER['HTTP_HOST'], WAP_DOMAIN_END);
if ($pos !== false) {
    if (WAP_DOMAIN_ALONE) {
        $app = 1;
    } else {
        $app = substr($_SERVER['HTTP_HOST'], 0, $pos - 1);
    }
    if (is_numeric($app) && $app > 0) {

        define('CUR_APP_ID', $app);
        $_router_module[] = array('module' => 'wap', 'root_path' => '', 'namespace' => '');
        $_router_module[] = array('module' => 'wap', 'root_path' => '/api', 'namespace' => 'Multiple\Wap\Api');
        $_router_module[] = array('module' => 'wap', 'root_path' => '/addon', 'namespace' => 'Multiple\Wap\Module');
        $_router_module[] = array('module' => 'wap', 'root_path' => '/addon/api', 'namespace' => 'Multiple\Wap\Module\Api');
        $_router_module[] = array('module' => 'o2o_store', 'root_path' => '/o2o_store', 'namespace' => '');
        $_router_module[] = array('module' => 'o2o_store', 'root_path' => '/o2o_store/api', 'namespace' => 'O2O\Store\Api');
    }
}

foreach ($_router_module as $_module) {
    genRouter($_module, $router);
}

// 追加不规则的
$router->add("/account([/index]?)", array('module' => 'panel', 'controller' => 'account', 'action' => 'login'));
$router->add("/account/:action/:params", array('module' => 'panel', 'controller' => 'account', 'action' => 1, 'params' => 2));
$router->add("/customer([/index]?)", array('module' => 'panel', 'controller' => 'customer', 'action' => 'login',));
$router->add("/customer/:action/:params", array('module' => 'panel', 'controller' => 'customer', 'action' => 1, 'params' => 2));
$router->add("/panel/addon/([\w]+)/:params", array('module' => 'panel', 'controller' => 'module', 'action' => 'run', '_m' => 1, '_h' => 2, '_a' => 3, 'params' => 4));
$router->add("/panel/addon/([\w]+)/([\w]+)/([\w]+)/:params", array('module' => 'panel', 'controller' => 'module', 'action' => 'run', '_m' => 1, '_h' => 2, '_a' => 3, 'params' => 4));
$router->add("/panel/module/run/([\w]+)/:params", array('module' => 'panel', 'controller' => 'module', 'action' => 'run', '_m' => 1, '_h' => 2, '_a' => 3, 'params' => 4));
$router->add("/panel/module/run/([\w]+)/([\w]+)/([\w]+)/:params", array('module' => 'panel', 'controller' => 'module', 'action' => 'run', '_m' => 1, '_h' => 2, '_a' => 3, 'params' => 4));

return $router;

function genRouter($module, \Phalcon\Mvc\Router $router)
{
    $_repeat = array(
        'root' => '([/]?)',
        'controller' => '/:controller([/]?)',
        'action' => '/:controller/:action([/]?)',
        'param' => '/:controller/:action/:params',
    );

    $_disp = array(
        'root' => array('controller' => 'index', 'action' => 'index'),
        'controller' => array('controller' => 1, 'action' => 'index'),
        'action' => array('controller' => 1, 'action' => 1),
        'param' => array('controller' => 1, 'action' => 2, 'params' => 3),
    );

    foreach ($_repeat as $_i => $_rout) {
        $mvc = $_disp[$_i];
        $mvc['module'] = $module['module'];
        if ($module['namespace']) {
            $mvc['namespace'] = $module['namespace'];
        }
        $router->add($module['root_path'] . $_rout, $mvc);
    }
}

