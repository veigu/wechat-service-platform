<?php
// Register the installed modules
$application->registerModules(array(
    'wap' => array(
        'className' => 'Multiple\Wap\Module',
        'path' => __DIR__ . '/../Apps/Wap/Module.php'
    ),
    'home' => array(
        'className' => 'Multiple\Home\Module',
        'path' => __DIR__ . '/../Apps/Home/Module.php'
    ),
    'panel' => array(
        'className' => 'Multiple\Panel\Module',
        'path' => __DIR__ . '/../Apps/Panel/Module.php'
    ),
    'shop' => array(
        'className' => 'Multiple\Shop\Module',
        'path' => __DIR__ . '/../Apps/Shop/Module.php'
    ),
    'api' => array(
        'className' => 'Multiple\Api\Module',
        'path' => __DIR__ . '/../Apps/Api/Module.php'
    ),
    'admin' => array(
        'className' => 'Multiple\Admin\Module',
        'path' => __DIR__ . '/../Apps/Admin/Module.php'
    ),
    'payment' => array(
        'className' => 'Multiple\Payment\Module',
        'path' => __DIR__ . '/../Apps/Payment/Module.php'
    ),
    'agent' => array(
        'className' => 'Multiple\Agent\Module',
        'path' => __DIR__ . '/../Apps/Agent/Module.php'
    ),
    // o2o
    'o2o_home' => array(
        'className' => 'O2O\Home\Module',
        'path' => __DIR__ . '/../O2O/Home/Module.php'
    ),
    'o2o_operator' => array(
        'className' => 'O2O\Operator\Module',
        'path' => __DIR__ . '/../O2O/Operator/Module.php'
    ),
    'o2o_store' => array(
        'className' => 'O2O\Store\Module',
        'path' => __DIR__ . '/../O2O/Store/Module.php'
    ),
    'o2o_app' => array(
        'className' => 'O2O\App\Module',
        'path' => __DIR__ . '/../O2O/App/Module.php'
    ),
));