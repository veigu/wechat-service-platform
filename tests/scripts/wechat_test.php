<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-8-5
 * Time: 下午4:25
 */

/**
 * @param $url
 * @param array | string $params
 * @param bool $isPost
 * @return bool|mixed
 */
function postRequest($url, $params = null, $inBody = false)
{
    $ch = curl_init();
    if (stripos($url, "https://") !== FALSE) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if ($inBody) {
        if(is_array($params)) {
            $params = json_encode($params, JSON_UNESCAPED_UNICODE);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; chartset=utf-8',
                'Content-Length: ' . strlen($params))
        );
    }
//            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    @curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_NOSIGNAL, true);
    try {
        $response = curl_exec($ch);
//        $response = json_decode($response, true);
    } catch (\Exception $e) {
        $response = false;
    }
    return $response;
}


/**
 * 1 request send message
 */

$data = <<<EOF
<xml><ToUserName><![CDATA[gh_c4c2816804e7]]></ToUserName>
<FromUserName><![CDATA[oeZ6AuEivzIC6C712GyKIDzxh1so]]></FromUserName>
<CreateTime>1407293196</CreateTime>
<MsgType><![CDATA[text]]></MsgType>
<Content><![CDATA[摸摸]]></Content>
<MsgId>6044278252903898762</MsgId>
</xml>
EOF;


echo postRequest("http://12345717.dev1.isaleasy.com/wechat/service?nonce=294914587&signature=79772e713e5ede5f2512d62c055590ad5da4fdb0&timestamp=1407293497", $data, true);
echo "\r\n";
