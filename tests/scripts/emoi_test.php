<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-8-5
 * Time: 下午4:25
 */

/**
 * @param $url
 * @param array | string $params
 * @param bool $isPost
 * @return bool|mixed
 */
function postRequest($url, $params = null, $inBody = false)
{
    $ch = curl_init();
    if (stripos($url, "https://") !== FALSE) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if ($inBody && is_array($params)) {
        $params = json_encode($params, JSON_UNESCAPED_UNICODE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; chartset=utf-8',
                'Content-Length: ' . strlen($params))
        );
    }
//            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    @curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_NOSIGNAL, true);
    try {
        $response = curl_exec($ch);
//        $response = json_decode($response, true);
    } catch (\Exception $e) {
        $response = false;
    }
    return $response;
}


/**
 * 1 request send message
 */

$data = array(
    'userId' => "WXSMS1", // 序列号
    'password' => "password123", //密码
    'pszMobis' => '13686828071', // 手机号 多个用英文的逗号隔开 post理论没有长度限制.推荐群发一次小于等于10000个手机号
    'pszMsg' => (iconv("UTF-8", "gb2312//IGNORE", "aaaaaa")), // 短信内容
    'iMobiCount' => 1,
    'pszSubPort' => '*' // 默认空 如果空返回系统生成的标识串 如果传值保证值唯一 成功则返回传入的值
);

//$data = http_build_query($data);
//$rs = file_get_contents("http://58.251.24.190:8082/MWGate/wmgw.asmx/MongateCsSpSendSmsNew?" . $data);
//
//echo $rs;

echo postRequest("http://58.251.24.190:8082/MWGate/wmgw.asmx/MongateCsSpSendSmsNew", $data);

//$data_str = '{"text":"默默哦","type":"text","receiver_id":5231991575,"sender_id":2194482767,"created_at":"Tue Aug 05 22:23:05 +0800 2014","data":{}}';

/*try {
    //$client = new SoapClient("HelloService.wsdl",array('encoding'=>'UTF-8'));
    $client = new SoapClient("http://58.251.24.190:8082/MWGate/wmgw.asmx?wsdl", array(
        'login' => 'WXSMS1',
        'password' => 'passw0rd@SMS',
        'soap_version' => SOAP_1_2,
        'encoding'=>'UTF-8'
    ));
    var_dump($client->__getFunctions());
    print("<br/>");
    var_dump($client->__getTypes());
    print("<br/>");

    $arrResult = $client->MongateCsSpSendSmsNew(array(
        'userId' => 'WXSMS1',
        'password' => 'passw0rd@SMS',
        'pszMobis' => '13286828071',
        'pszMsg' => 'Hello',
        'iMobiCount' => 1,
        'pszSubPort' => '*'
    ));

    echo $arrResult . "<br/>";
} catch (SOAPFault $e) {
    print $e;
}*/