<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-8-5
 * Time: 下午4:25
 */

/**
 * @param $url
 * @param array | string $params
 * @param bool $isPost
 * @return bool|mixed
 */
function postRequest($url, $params = null, $inBody = false)
{
    $ch = curl_init();
    if (stripos($url, "https://") !== FALSE) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if ($inBody && is_array($params)) {
        $params = json_encode($params, JSON_UNESCAPED_UNICODE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; chartset=utf-8',
                'Content-Length: ' . strlen($params))
        );
    }
//            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    @curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_NOSIGNAL, true);
    try {
        $response = curl_exec($ch);
//        $response = json_decode($response, true);
    } catch (\Exception $e) {
        $response = false;
    }
    return $response;
}


/**
 * 1 request send message
 */

$data = array(
    'type' => 'text',
    'sender_id'=> '2194482767',
    'receiver_id' => '5231991575',
    'created_at' => 'Tue Aug 05 22:23:05 +0800 2014',
    'text' => "你好",
    'data'=> array()
);

echo postRequest("http://12345717.dev1.isaleasy.com/weibo/service?nonce=236900766&signature=40eda0eafc983a82881719048bc0056950e8fc64&timestamp=1407249669159", $data, true);

$data_str = '{"text":"默默哦","type":"text","receiver_id":5231991575,"sender_id":2194482767,"created_at":"Tue Aug 05 22:23:05 +0800 2014","data":{}}';