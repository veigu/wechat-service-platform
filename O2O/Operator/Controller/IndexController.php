<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2014/10/24
 * Time: 11:11
 */

namespace O2O\Operator\Controller;


use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        echo "Operator";

        $this->view->disable();
    }

} 