<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/28/14
 * Time: 2:41 PM
 */

namespace O2O\Store\Controller;


use Components\Module\VipcardManager;
use Components\UserStatus;
use Models\Modules\Pointmall\AddonPointMallOrder;
use Models\Modules\Store\AddonStore;
use Models\Product\ProductComment;
use Models\Shop\ShopOrderItem;
use Models\Shop\ShopOrders;
use Models\User\Users;
use Components\UserStatusWap;
use Phalcon\Mvc\Model;
use Util\Cookie;
use Util\EasyEncrypt;


class UserController extends StoreBase
{
    public $cookies = '';
    public static $referer = null;
    public $userInfo = null;
    public $cardInfo = null;

    public function indexAction()
    {
        if (!$this->checkLogin()) {
            return;
        }
        $this->userInfo = UserStatusWap::init()->getUserInfo();
        $cardInfo = VipcardManager::init()->getCardInfoByUid($this->checkLogin(), CUR_APP_ID);
        $this->view->setVar('user', $this->userInfo);
        $this->view->setVar('cardInfo', $cardInfo);
    }

    public function profileAction()
    {
    }

    public function signupAction()
    {
        if (UserStatus::getUid()) {
            $this->refresh('user');
        }

        // 避免结算页面重复
        Cookie::set('_cart_settle_page', EasyEncrypt::encode($this->uri->baseUrl('user/' . $this->router->getActionName())));
    }

    public function loginAction()
    {
        $uid = UserStatus::getUid();
        if ($uid) {
            // check bind card status
            VipcardManager::init()->checkNeedBindCard($uid, CUR_APP_ID);
            $this->refresh('user');
        }

        // 避免结算页面重复
        Cookie::set('_cart_settle_page', EasyEncrypt::encode($this->uri->baseUrl('user/' . $this->router->getActionName())));
    }

    public function logoutAction()
    {
        UserStatusWap::init()->logout();
        $this->response->redirect('user')->send();
    }

    private function refresh($uri)
    {
        $this->response->redirect(ltrim($uri, '/'))->send();
    }

    public function settingAction()
    {
        $this->checkLogin();
        $userInfo = UserStatusWap::init()->getUserInfo();
        $this->view->setVar('user', $userInfo);
    }

    /**
     * 用户订单
     */
    public function orderAction()
    {
        $user_id = $this->checkLogin();

        $where = "customer_id='" . CUR_APP_ID . "' AND user_id='" . $user_id . "' AND is_deleted = '0' ";
        $status = $this->request->get('s');
        if (is_numeric($status)) {
            $where .= ' AND status = ' . $status;
        }

        $orders = ShopOrders::find(array($where, 'order' => 'created desc,is_paid asc', 'limit' => '20'));
        $order1 = array();
        if ($orders) {
            $orders = $orders->toArray();

            $store = AddonStore::findFirst("customer_id=" . CUR_APP_ID);
            foreach ($orders as &$order) {
                if ($order['use_point'] > 0) {
                    $order['point_money'] = round($order['use_point'] / $store->redeem_proportion, 2) >= $order['total_cash'] + $order['logistics_fee'] ? $order['total_cash'] + $order['logistics_fee'] : round($order['use_point'] / $store->redeem_proportion, 2);
                }
                $item_list = ShopOrderItem::find('order_number="' . $order['order_number'] . '"');
                if (ProductComment::count("order_number='" . $order['order_number'] . "' AND user_id=$user_id") > 0) {
                    ProductComment::count("order_number='" . $order['order_number'] . "' AND user_id=$user_id");
                    $order['comment_yes'] = true;
                } else {
                    $order['comment_yes'] = false;
                }


                // 积分商城订单
                if ($order['is_pointmall']) {
                    $pointMall = AddonPointMallOrder::findFirst('order_number="' . $order['order_number'] . '"');
                    $order['pointmall'] = $pointMall ? $pointMall->toArray() : [];
                }

                $order['list'] = $item_list ? $item_list->toArray() : '';
            }
        }
        $this->view->setVar('orders', $orders);
    }


    public function forgotAction()
    {
        $phone = base64_decode($this->request->get('p'));

        $this->view->setVar('phone', $phone);
        $this->view->setVar('user', '');

        if (preg_match('/^(1[\d]{10})$/', $phone)) {
            $user = Users::findFirst('host_key="' . HOST_KEY . '" and phone="' . $phone . '"');
            $this->view->setVar('user', $user ? $user->toArray() : '');
        }
    }

    public function resetPassAction()
    {
    }

    public function bindPhoneAction()
    {
    }

    public function bindWecahtAction()
    {
    }

    public function bindWeiboAction()
    {
    }

    public function bindAction()
    {
        $uid = UserStatus::getUid();

        if ($uid > 0) {
            $cardInfo = VipcardManager::init()->getCardInfoByUid($uid, CUR_APP_ID);

            if ($cardInfo) {
                return $this->response->redirect("user")->send();
            }
        }

        $this->view->setVar('user', $this->userInfo);
    }

    public function getOpenIdForClientRespondAction()
    {
        UserStatusWap::init()->getOpenIdForClientRespond();
    }


    /*
     * 我的收藏
     */
    public function collectionAction()
    {
        $user_id = $this->checkLogin();
        $collection = $this->modelsManager->createBuilder()
            ->addFrom('Models\\Product\\ProductCollection', 'pc')
            ->leftJoin('Models\\Product\\Product', 'pc.product_id=p.id', 'p')
            ->andWhere('pc.customer_id=' . CUR_APP_ID . " AND pc.user_id=" . $user_id)
            ->columns('pc.id as pcid,pc.created,p.id as pid, p.created,p.name,p.thumb,p.sell_price')
            ->getQuery()
            ->execute();
        $collection = $collection ? $collection->toArray() : [];
        $this->view->setVar('collection', $collection);

    }
}
