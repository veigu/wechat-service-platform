<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-11
 * Time: 下午2:45
 */

namespace O2O\Store\Controller;


use Components\Payments\PaymentUtil;
use Components\Product\OrderManager;
use Components\Product\TransactionManager;
use Components\TplManager;
use Models\Modules\Pointmall\AddonPointMallItem;
use Models\Modules\Pointmall\AddonPointMallOrder;
use Models\Shop\Shop;
use Models\Shop\ShopOrderCombine;
use Models\Shop\ShopOrderItem;
use Models\Shop\ShopOrderReturn;
use Models\Shop\ShopOrders;
use Models\User\UserAddress;
use Multiple\Wap\Helper\Meta;
use Components\UserStatusWap;
use Util\Cookie;
use Util\EasyEncrypt;

class OrderController extends StoreBase
{
    protected $checkLogin = true;

    public function initialize()
    {

        parent::initialize();
        TplManager::$_config['hideFooter'] = true;
        TplManager::$_config['hideHeader'] = true;
        // 定制模板
        $view_base = MODULE_PATH . '/Views';
        if (in_array($this->tpl['tpl_mode'], array('customize', 'kit'))) {
            $customize = json_decode($this->tpl[$this->tpl['tpl_mode']], true);
            $customize_dir = isset($customize['serial_number']) && $customize['serial_number'] ? $customize['serial_number'] : '';
            if ($customize_dir) {
                $view_tmp = $view_base . '/' . $this->tpl['tpl_mode'] . '/' . $customize_dir;
                $order_dir = $view_tmp . '/' . $this->router->getControllerName();
                $order_dir_file = $order_dir . '/' . $this->router->getActionName() . '.phtml';
                if (is_dir($order_dir) && file_exists($order_dir_file)) {
                    $view_base = $view_tmp;
                } else {
                    $view_base .= "/default";
                }
            } else {
                $view_base .= "/default";
            }
        } else {
            $view_base .= "/default";
        }

        $this->view->setViewsDir($view_base);
    }

    // 购物车
    public function cartAction()
    {
        $this->checkLogin();
        $this->view->nocache = true;
        Meta::$head_title['shop_cart'] = "购物车";

        // 避免结算页面重复
        Cookie::set('_cart_settle_page', EasyEncrypt::encode($this->uri->baseUrl('order/' . $this->router->getActionName())));
    }

    // 结算
    public function settleAction()
    {
        Meta::$head_title['shop_settle'] = "确认订单";
        $this->view->nocache = true;

        // 只允许从商品页和购物车,登陆注册页来
        // 判断来源
        if (!(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'])) {
            $this->view->setVar('tip', '尚无数据');
            $this->view->pick('order/settle/error');
            return;
        }

        if (!Cookie::get('_cart_settle_page')) {
            $this->view->setVar('tip', '错误来源');
            $this->view->pick('order/settle/error');
            return;
        }

        // 来自商品页或购物车
        if (strpos($_SERVER['HTTP_REFERER'], EasyEncrypt::decode(Cookie::get('_cart_settle_page'))) === false) {
            $this->view->setVar('tip', '页面过期');
            $this->view->pick('order/settle/error');
            return;
        }

        // 接收参数
        $all = $this->dispatcher->getParam(0);
        $from = $this->request->get('f');

        if (!in_array($from, array("cart", "item"))) {
            $this->view->setVar('tip', '参数错误');
            $this->view->pick('order/settle/error');
            return;
        }

        // 判断参数
        $cart = json_decode(base64_decode($all), true);
        // 商品详情页面
        if ($from == 'item') {
            if (!$cart && !empty($cart['id']) && !empty($cart['num'])) {
                $this->view->setVar('tip', '参数错误~');
                $this->view->pick('order/settle/error');
                return;
            }
        }
        // 购物车页面
        if ($from == 'cart') {
            if (!($cart && count($cart) > 0)) {
                $this->view->setVar('tip', '参数错误.');
                $this->view->pick('order/settle/error');
                return;
            }
        }

        // 避免重复订单
        $this->session->set('_cart_settle_ok_' . $this->user_id, false);

        // 设置订单号
        $order_number = OrderManager::init()->generateOrderNumber($this->user_id);
        $this->session->set('_order_user_' . $this->user_id, $order_number);
    }

    // 我的订单
    public function mineAction()
    {
        $user_id = $this->checkLogin();

        $where = "customer_id='" . CUR_APP_ID . "' AND user_id='" . $user_id . "' AND is_deleted = '0' ";
        $status = $this->request->get('s');
        $return = $this->request->get('b');
        if (is_numeric($status)) {
            $where .= ' AND status = ' . $status;
        }
        if ($return)//退货/售后商品
        {
            $list = OrderManager::init()->getReturnGoods(CUR_APP_ID, $user_id);
            $this->view->setVar('list', $list);
        } else {
            $orders = ShopOrders::find(array($where, 'order' => 'created desc,is_paid asc', 'limit' => '20'));
            if ($orders) {
                $orders = $orders->toArray();
                $store = Shop::findFirst("customer_id=" . CUR_APP_ID);
                foreach ($orders as &$order) {
                    if ($order['use_point'] > 0) {
                        $order['point_money'] = round($order['use_point'] / $store->redeem_proportion, 2) >= $order['total_cash'] + $order['logistics_fee'] ? $order['total_cash'] + $order['logistics_fee'] : round($order['use_point'] / $store->redeem_proportion, 2);
                    }
                    $item_list = ShopOrderItem::find('order_number="' . $order['order_number'] . '"');
                    // 积分商城订单
                    if ($order['is_pointmall']) {
                        $pointMall = AddonPointMallItem::findFirst('order_number="' . $order['order_number'] . '"');
                        $order['pointmall'] = $pointMall ? $pointMall->toArray() : [];
                    }
                    $order['list'] = OrderManager::init()->getOrderItem($order);

                }
            }
            $this->view->setVar('orders', $orders);

        }

    }

    // 支付
    public function payAction()
    {
        $this->view->nocache = true;
        $user_id = $this->checkLogin();
        $order_number = $this->request->get('order');
        if (!$order_number) {
            return $this->err('400', '订单号不存在!');
        }

        if (preg_match('/P/', $order_number)) {
            $where = "user_id = " . $user_id . " and combine_order_number  ='" . $order_number . "' AND is_deleted = 0 ";
            $order = ShopOrderCombine::findFirst($where);
            $order = $order ? $order->toArray() : [];
            $orders = $order;
            if (!$order) {
                return $this->err('400', '订单号未找到!');
            }
            $shopOrders = ShopOrders::find("combine_order_number='" . $order['combine_order_number'] . "' and user_id=" . $user_id);
            if (!$shopOrders) {
                return $this->err('400', '订单号未找到!');
            }
            $shopOrders = $shopOrders ? $shopOrders->toArray() : [];
            foreach ($shopOrders as $k => $item) {
                $shop_item = ShopOrderItem::find("order_number='" . $item['order_number'] . "' and user_id=" . $user_id);
                $shopOrders[$k]['list'] = $shop_item ? $shop_item->toArray() : [];
            }
            $addr = $order['address_id'];
            $address = UserAddress::findFirst('id=' . $addr);
            $this->view->setVar('addr', $address ? $address->toArray() : '');
            $this->view->setVar('combineOrder', $order);
            $this->view->setVar('shopOrder', $shopOrders);

        } else {

            $where = 'user_id = ' . $user_id . ' and order_number  ="' . $order_number . '" AND is_deleted = 0 ';
            $order = ShopOrders::findFirst($where);
            if (!$order) {
                return $this->err('400', '订单号未找到!');
            }


            if ($order->is_paid != 0) {
                return $this->err('400', '订单不可支付!');
            }
            if ($order->combine_order_number) {

                $combineOrder = ShopOrderCombine::findFirst("combine_order_number='" . $order->combine_order_number . "'");

                if (!$combineOrder || ($combineOrder && ($combineOrder->use_coupon != '' || $combineOrder->use_point != 0))) {
                    $order = $order->toArray();
                    $item_list = ShopOrderItem::find('order_number="' . $order['order_number'] . '"');
                    $order['list'] = $item_list ? $item_list->toArray() : '';
                    $this->view->setVar('order', $order);
                    // 积分换购
                    $point_item = '';
                    if ($order['is_pointmall']) {
                        $where = 'user_id = ' . $user_id . ' and order_number  ="' . $order_number . '"';
                        $point_item = AddonPointMallOrder::findFirst($where);
                        if ($point_item) {
                            $point_item = $point_item->toArray();
                        }
                    }
                    $addr = $order['address_id'];
                    $address = UserAddress::findFirst('id=' . $addr);
                    $this->view->setVar('addr', $address ? $address->toArray() : '');
                    $this->view->setVar('point_item', $point_item);
                    $this->view->setVar('order', $order);
                } else {

                    $order = $combineOrder ? $combineOrder->toArray() : [];
                    if (!$order) {
                        return $this->err('400', '订单号未找到!');
                    }
                    $shopOrders = ShopOrders::find("combine_order_number='" . $order['combine_order_number'] . "' and user_id=" . $user_id);
                    if (!$shopOrders) {
                        return $this->err('400', '订单号未找到!');
                    }
                    $shopOrders = $shopOrders ? $shopOrders->toArray() : [];
                    foreach ($shopOrders as $k => $item) {
                        $shop_item = ShopOrderItem::find("order_number='" . $item['order_number'] . "' and user_id=" . $user_id);
                        $shopOrders[$k]['list'] = $shop_item ? $shop_item->toArray() : [];
                    }
                    $addr = $order['address_id'];
                    $address = UserAddress::findFirst('id=' . $addr);
                    $this->view->setVar('addr', $address ? $address->toArray() : '');
                    $this->view->setVar('combineOrder', $order);
                    $this->view->setVar('shopOrder', $shopOrders);
                }
            } else {
                $where = 'user_id = ' . $user_id . ' and order_number  ="' . $order_number . '" AND is_deleted = 0 ';
                $order = ShopOrders::findFirst($where);
                if (!$order) {
                    return $this->err('400', '订单号未找到!');
                }


                if ($order->is_paid != 0) {
                    return $this->err('400', '订单不可支付!');
                }
                $order = $order->toArray();
                $item_list = ShopOrderItem::find('order_number="' . $order['order_number'] . '"');
                $order['list'] = $item_list ? $item_list->toArray() : '';
                $this->view->setVar('order', $order);
                // 积分换购
                $point_item = '';
                if ($order['is_pointmall']) {
                    $where = 'user_id = ' . $user_id . ' and order_number  ="' . $order_number . '"';
                    $point_item = AddonPointMallOrder::findFirst($where);
                    if ($point_item) {
                        $point_item = $point_item->toArray();
                    }
                }
                $addr = $order['address_id'];
                $address = UserAddress::findFirst('id=' . $addr);
                $this->view->setVar('addr', $address ? $address->toArray() : '');
                $this->view->setVar('point_item', $point_item);

            }


        }
        $payment = PaymentUtil::instance(HOST_KEY)->getCustomerPayments(CUR_APP_ID);
        $this->view->setVar('payment', $payment);

    }

    // 详情
    public function detailAction()
    {
        $this->view->nocache = true;
        $user_id = $this->checkLogin();
        $order_number = $this->dispatcher->getParam(0);
        if (!$order_number) {
            $this->view->setVar('tip', '参数错误.');
            $this->view->pick('order/settle/error');
            return;
        }

        $order = ShopOrders::findFirst('user_id = ' . $user_id . ' and order_number  ="' . $order_number . '" AND is_deleted = 0 ');
        if (!$order) {
            $this->view->setVar('tip', '订单不存在');
            $this->view->pick('order/settle/error');
            return;
        }

        $order = $order->toArray();

        if (in_array($order['status'], array(TransactionManager::ORDER_STATUS_WAIT_BUYER_CONFIRM_GOODS, TransactionManager::ORDER_STATUS_SELLER_DELIVERED, TransactionManager::ORDER_STATUS_TRADE_SUCCESS))) {
            $order['logistics_name'] = TransactionManager::instance(HOST_KEY)->getCustomerLogisticsByType($order['logistics_type'], CUR_APP_ID)['name'];
        }

        // $item_list = ShopOrderItem::find('order_number="' . $order['order_number'] . '"');
        $order['list'] = OrderManager::init()->getOrderItem($order);//$item_list ? $item_list->toArray() : '';
        $this->view->setVar('order', $order);
        $addr = $order['address_id'];
        $address = UserAddress::findFirst('id=' . $addr);
        $this->view->setVar('addr', $address ? $address->toArray() : '');
    }

    public function addrAction()
    {
        $user_id = $this->checkLogin();
        $addr = UserAddress::find('user_id=' . $user_id);
        $this->view->setVar('address', $addr ? $addr->toArray() : '');
    }

    public function commentAction()
    {
        $order_id = $this->dispatcher->getParam(0); //订单id;
        $user_id = UserStatusWap::getUid();

        $andWhere = '';
        $this->view->setVar('order', false);

        if (is_numeric($order_id) && $order_id > 0) {

            $order = ShopOrders::findFirst('order_number="' . $order_id . '" and user_id= ' . $user_id);

            if (!$order) {
                $this->err('404', '订单不存在！');
                return;
            }

            $this->view->setVar('order', $order->toArray());
            $andWhere = ' and pc.order_number="' . $order_id . '"';
        }
        $comment =
            $this->modelsManager->createBuilder()
                ->addFrom('\\Models\\Product\\ProductComment', 'pc')
                ->leftJoin('\\Models\\Product\Product', 'pc.product_id=p.id', 'p')
                ->andWhere("pc.user_id=" . $user_id . $andWhere)
                ->columns('pc.product_id,pc.rank,pc.created,pc.content,p.name,p.thumb')
                ->limit(30)
                ->getQuery()
                ->execute();
        $this->view->setVar('list', $comment ? $comment->toArray() : []);
    }

    // 评价
    public function rateAction()
    {
        $order_id = $this->dispatcher->getParam(0); //订单id
        $user_id = $this->checkLogin(); //用户id

        if (!is_numeric($order_id) || $order_id <= 0) {
            return $this->err('404', '订单不存在！');
        }

        $order = ShopOrders::findFirst("order_number='" . $order_id . "' AND user_id='" . $user_id . "'");
        if (!$order) {
            return $this->err('404', '订单不存在!');
        }
        $order = $order->toArray();
        if ($order['is_deleted']) { //已经评价过
            return $this->err('404', '订单已经删除!');
        }

        // 交易完成
        if (!in_array($order['status'], array(TransactionManager::ORDER_STATUS_TRADE_SUCCESS, TransactionManager::ORDER_STATUS_SELLER_DELIVERED))) {
            return $this->err('404', '交易尚未完成!');
        }

        if ($order['is_rate']) { //已经评价过
            return $this->err('404', '您已评价!');
        }

        $list = ShopOrderItem::find("order_number='" . $order_id . "'");

        $this->view->setVar('order_number', $order_id);
        $this->view->setVar('list', $list ? $list->toArray() : []);
    }

    //退换货申请页
    public function returnAction()
    {
        $this->checkLogin();
        $item_id = EasyEncrypt::decode($this->dispatcher->getParam(0));
        $type = EasyEncrypt::decode($this->dispatcher->getParam(1));
        if (!in_array($type, array(1, 2))) {
            $this->flash->error("illegal params");
            exit;
        }
        if (!is_numeric($item_id) || $item_id <= 0) {
            $this->flash->error("illegal params");
            exit;
        }
        $item = ShopOrderItem::findFirst("id=" . $item_id . " and user_id=" . $this->user_id);
        if (!$item) {
            $this->flash->error("illegal params");
            exit;
        }
        $item_info = $this->db->query("
          select soi.item_name,soi.order_number,soi.item_price,soi.thumb,soi.item_price,soi.quantity,soi.id,soi.item_id from  shop_order_item as soi left join
          shop_order_return  as  sor on soi.id=sor.order_item_id where soi.id=" . $item_id
        )->fetchAll();

        $return_item = ShopOrderReturn::findFirst("customer_id=" . CUR_APP_ID . " and  user_id=" . $this->user_id . " and order_item_id=" . $item_id . " and type='" . $type . "'");
        if ($return_item) //申请过退/换货
        {
            $this->response->redirect('/order/mine?b=' . TransactionManager::ORDER_STATUS_BUYER_CHANGED_GOODS);
            exit;
            // if (OrderManager::init()->checkReturnProduct($item_id)) {
            //    $form_show = true;
            // }
        }
        if (!OrderManager::init()->checkReturnProduct($item_id))//不符合条件
        {
            $this->flash->error("this goods is not available");
            exit;
        }
        $this->view->setVar('type', $type);
        $this->view->setVar('item_info', $item_info);

    }

    //退换货回寄商品物流填写
    public function backGoodAction()
    {
        $id = EasyEncrypt::decode($this->dispatcher->getParam(0));
        if (!is_numeric($id)) {
            $this->flash->error('illegal params');
            exit;
        }
        $orderReturn = ShopOrderReturn::find("customer_id=" . CUR_APP_ID . " AND user_id=" . $this->user_id . " and id=" . $id);
        if (!$orderReturn) {
            $this->flash->error('illegal params');
            exit;
        }
        $item_info = $this->db->query("
          select soi.item_name,soi.order_number,soi.item_price,soi.thumb,soi.item_price,soi.quantity,soi.item_id
          ,sor.reason,sor.reason_detail,sor.id  from  shop_order_return as sor left join
          shop_order_item  as  soi on soi.id=sor.order_item_id where sor.id=" . $id
        )->fetchAll();
        $logistics = TransactionManager::instance(HOST_KEY)->getDefaultLogistics();
        $this->view->setVar('logistics', $logistics);
        $this->view->setVar('item_info', $item_info);
    }
} 
