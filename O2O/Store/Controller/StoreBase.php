<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2014/10/28
 * Time: 17:20
 */

namespace O2O\Store\Controller;


use Phalcon\Mvc\Controller;

class StoreBase extends Controller
{
    protected $tpl = null;
    public $user_id = null;
    protected $checkLogin = false;
    protected $site_info = null;

    protected $_css = array(
        'base.css',
        'color.css'
    );

    public function initialize()
    {
        $this->view->setMainView('index');
        $this->initCss();
    }

    /**
     * init css
     */
    public function initCss()
    {
        // init css
        $css = "??";
        $this->_css[] = 'default/' . $this->dispatcher->getControllerName() . '.css';

        $this->_css = array_unique($this->_css);
        $css .= implode(',', $this->_css);
        $this->view->setVar('css', $css);
    }

    /**
     * usage:
     * return $this->err();
     */
    protected function err($code = "403", $msg = '404 page no found')
    {
        $this->view->setViewsDir(MODULE_PATH . '/View');
        $this->response->setHeader('content-type', 'text/html;charset=utf-8');
        $this->response->setStatusCode($code, $msg);

        $this->view->setVar('msg', $msg);

        return $this->view->pick('base/error');
    }

    protected function checkLogin()
    {
    }
} 