<?php
namespace O2O\App;

use Phalcon\DI;
use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;

# 根据路径获取应用名称
$app_path = dirname(__FILE__);
$app_name = substr($app_path, strrpos($app_path, DIRECTORY_SEPARATOR) + 1);

define('MODULE_NAME', $app_name);
define("MODULE_PATH", __DIR__);

// 匹配子域名(包含本地域名如12345709.m.local)
class Module
{
    public function registerAutoloaders()
    {
        $loader = new Loader();

        $loader->registerNamespaces(array(
            'O2O\App\Controller' => 'O2O/App/Controller/',
            'O2O\App\Helper' => 'O2O/App/Helper/',
            'O2O\App\Api' => 'O2O/App/Api/',
            'O2O\App\Module' => 'O2O/App/Module/'
        ));

        $loader->register();
    }

    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    public function registerServices(DI $di)
    {
        $di->set('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace('\O2O\App\Controller');
            return $dispatcher;
        });

        $di->set('view', function () {
            $view = new View();
            $view->setViewsDir(MODULE_PATH . '/Views');
            $view->registerEngines(array(
                '.phtml' => 'Phalcon\Mvc\View\Engine\Php',
            ));
            return $view;
        });
    }

}