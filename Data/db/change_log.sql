-- 11-26
ALTER TABLE `shop`
ADD COLUMN `cashback_proportion` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '推广返现百分比（0-100）' AFTER `free_postage`,
ADD COLUMN `cashback_enable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否开启返现功能' AFTER `cashback_proportion`;
ALTER TABLE `shop_order_item`
DROP INDEX `item_id_order_number`,
ADD UNIQUE INDEX `item_id_order_number_item_spec` (`item_id`, `order_number`, `item_spec`);
ALTER TABLE `shop_orders`
CHANGE COLUMN `logistics_fee` `logistics_fee` FLOAT(5,2) NULL DEFAULT '0.00' COMMENT '物流费用' AFTER `logistics_type`;
ALTER TABLE `shop_orders`
CHANGE COLUMN `combine_order_number` `combine_order_number` CHAR(19) NOT NULL DEFAULT '' AFTER `order_number`;
ALTER TABLE `shop_order_combine`
ADD COLUMN `logistics_fee` FLOAT(5,2) NOT NULL NOT NULL DEFAULT '0' AFTER `created`;
ALTER TABLE `shop_freight_tpl_items`
ADD COLUMN `first_num` INT(5) NULL DEFAULT NULL COMMENT '首重或首件数量' AFTER `customer_id`,
ADD COLUMN `first_cash` FLOAT(10,2) NULL DEFAULT NULL COMMENT '首重钱数' AFTER `first_num`,
ADD COLUMN `addon_num` INT(5) NULL DEFAULT NULL COMMENT '续件' AFTER `first_cash`,
ADD COLUMN `addon_cash` FLOAT(10,2) NULL DEFAULT NULL COMMENT '续件费用' AFTER `addon_num`;
ALTER TABLE `shop_freight_tpl`
CHANGE COLUMN `description` `description` VARCHAR(250) NULL COMMENT '模板描述' AFTER `valuation_way`;
ALTER TABLE `shop_freight_tpl`
CHANGE COLUMN `deliverAddress` `deliver_address` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '发货地址' AFTER `tpl_name`;
ALTER TABLE `shop_freight_tpl`
CHANGE COLUMN `valuation_way` `valuation_way` ENUM('number','volume','weight') NULL DEFAULT 'number' COMMENT '计价方式(\'number\',\'volume\',\'weight\')'
AFTER `deliver_address`;
ALTER TABLE `shop_freight_tpl`
ADD COLUMN `mail_mode` VARCHAR(50) NOT NULL DEFAULT 'express' COMMENT '运送方式（可多种）' AFTER `valuation_way`;
ALTER TABLE `shop_freight_tpl_items`
DROP COLUMN `fr_one`,
DROP COLUMN `fr_one_money`,
DROP COLUMN `fr_co`,
DROP COLUMN `fr_co_money`;
ALTER TABLE `shop_freight_tpl_items`
CHANGE COLUMN `mails` `mail_mode` ENUM('mail','express','ems') NOT NULL DEFAULT "express" COMMENT '运送方式' AFTER `freight_id`;


-- ----------------------------- add user settings for customer ------------------------------------
ALTER TABLE `user_settings`
ADD COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT FIRST ,
ADD COLUMN `need_check`  tinyint(2) NULL AFTER `point_usage`,
ADD PRIMARY KEY (`id`);


ALTER TABLE `addon_coupon_order_log`
ADD COLUMN `user_id` INT(11) NOT NULL DEFAULT '0' AFTER `customer_id`;

-- ------------------------------------- add user settings for customer 2014-12-13 ----------------------------------
ALTER TABLE `user_for_customers`
ADD COLUMN `active`  tinyint(2) NOT NULL DEFAULT 1 COMMENT '是否激活' AFTER `points_available`;

