<?php
namespace Models\User;

use Models\BaseModel;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class UsersWechat extends BaseModel
{
    public $id;
    public $user_id;
    public $open_id;
    public $nickname;
    public $headimgurl;
    public $language;

    public function getSource()
    {
        return 'users_wechat';
    }
}
