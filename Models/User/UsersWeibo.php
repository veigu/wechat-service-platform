<?php
namespace Models\User;

use Models\BaseModel;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class UsersWeibo extends BaseModel
{
    public $id;
    public $user_id;
    public $uid;
    public $nickname;
    public $headimgurl;
    public $language;

    public function getSource()
    {
        return 'users_weibo';
    }
}
