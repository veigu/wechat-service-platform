<?php
namespace Models\User;

use Models\BaseModel;

class UserForCustomers extends BaseModel
{
    public $id;
    public $customer_id;
    public $user_id;
    public $open_id;
    public $nickname;
    public $type;
    public $grade;
    public $subscribe;
    public $subscribe_time;
}
