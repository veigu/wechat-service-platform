<?php
namespace Models\User;

use Models\BaseModel;

class UserWechatGroup extends BaseModel
{
    public $grade_id;
    public $group_id;
    public $customer_id;
}
