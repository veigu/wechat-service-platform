<?php
namespace Models\User;

use Models\BaseModel;

class UserLocations extends BaseModel
{
    public $customer_id;
    public $open_id;
    public $latitude;
    public $longitude;
    public $precision;
    public $created;
}
