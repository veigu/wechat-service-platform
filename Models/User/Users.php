<?php
namespace Models\User;

use Models\BaseModel;
use Models\Modules\Vipcard\VipcardNumber;
use Models\Modules\Vipcard\VipcardRules;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;
use Phalcon\Mvc\Model\Query\Builder as Querybuilder;
use Models\Modules\Vipcard\VipcardRecords;

class Users extends BaseModel
{
    public $id;

    public function getSource()
    {
        return 'users';
    }

    /**
     * 获取会员卡消费总额
     *
     * @access public
     * @return void
     */
    public function getVipcardConsume($customer)
    {
        return $this->getVipcardConsumeTotal($customer);
    }

    /**
     * 会员卡所有积分
     *
     * @access public
     * @return int
     */
    public function getVipcardScore($customer)
    {
        return $this->getVipcardConsumeScore($customer) + $this->getCheckInScore($customer);
    }

    /**
     * 会员卡剩余积分
     *
     * @access public
     * @return int
     */
    public function getVipcardRemainScore($customer)
    {
        if (!empty($this->id)) {
            $card = VipcardNumber::getUserVipcard($this->id, $customer);
            if (!empty($card)) {
                return $card->score;
            }
        }
        return 0;
    }

    /**
     * 会员卡剩余金额
     *
     * @access public
     * @return void
     */
    public function getVipcardRemainMoney($customer)
    {
        if (!empty($this->id)) {
            $card = VipcardNumber::getUserVipcard($this->id, $customer);
            if (!empty($card)) {
                return $card->money;
            }
        }
        return 0;
    }

    /**
     * 获得签到积分统计
     *
     * @access public
     * @return int
     */
    public function getCheckinScore($customer)
    {
        $queryBuilder = new Querybuilder();
        $query = $queryBuilder
            ->from("Models\Modules\Vipcard\VipcardSignin")
            ->columns(array('SUM(Models\Modules\Vipcard\VipcardSignin.score) as score'))
            ->where("vn.user_id = '$this->id' AND customer_id='{$customer}'")
            ->leftJoin("Models\Modules\Vipcard\VipcardNumber", "vn.id = Models\Modules\Vipcard\VipcardSignin.cardid", "vn")
            ->getQuery()
            ->execute();

        if (empty($query->getFirst())) {
            return 0;
        }

        return (int)$query->getFirst()->score;
    }

    /**
     * 获得消费积分统计
     *
     * @access public
     * @return int
     */
    public function getVipcardConsumeScore($customer)
    {
        if (!empty($this->id)) {
            $vipcardNumberModel = new VipcardNumber();
            $vipcardNumber = $vipcardNumberModel->getUserVipcard($this->id, $customer);
            if ($vipcardNumber) {
                return (int)VipcardRecords::sum(array("cardid = $vipcardNumber->id AND user_id = $this->id", "column" => "score"));
            }
        }
        return 0;
    }

    /**
     * 获得消费金额统计
     *
     * @access public
     * @return float
     */
    public function getVipcardConsumeTotal($customer)
    {
        if (!empty($this->id)) {
            $vipcardNumberModel = new VipcardNumber;
            $vipcardNumber = $vipcardNumberModel->getUserVipcard($this->id, $customer);
            if ($vipcardNumber) {
                return (int)VipcardRecords::sum(array("cardid = $vipcardNumber->id AND user_id = $this->id", "column" => "total"));
            }
        }

        return 0;
    }

    /**
     * 获得消费规则数据
     *
     * @access public
     * @return void
     */
    public function getVipcardRules($customer)
    {
        if (!empty($this->id)) {
            $vipcardNumberModel = new VipcardNumber;
            $vipcardNumber = $vipcardNumberModel->getUserVipcard($this->id, $customer);
            if ($vipcardNumber) {
                $rules = new VipcardRules();
                $cardRules = $rules->find('set_id="' . $vipcardNumber->cardid . '" ');
                return $cardRules;
            } else {
                return array();
            }

        } else {
            return array();
        }
    }
}
