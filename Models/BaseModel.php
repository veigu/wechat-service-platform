<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 5/20/14
 * Time: 10:53 AM
 */

namespace Models;


use Phalcon\Db\RawValue;
use Phalcon\Mvc\Model;

// remove the automatic not null validation
Model::setup(array(
    'notNullValidations' => false,
    'exceptionOnFailedSave' => true,
));

class BaseModel extends Model
{
    public function validation()
    {
        // 设置null为'' allow empty string
        $notNullAttributes = $this->getModelsMetaData()->getNotNullAttributes($this);
        foreach ($notNullAttributes as $field) {
            if (!isset($this->$field) || $this->$field === null) {
                $this->$field = new RawValue('DEFAULT');
            }
        }
    }

    public function exists()
    {
        if ($this->findFirst()) {
            return true;
        } else {
            return false;
        }
    }
}