<?php
namespace Models\Modules;

use Models\BaseModel;

class SystemOrderResources extends BaseModel
{
    public $id;
    public $account;
    public $name;
    public $active;
    public $time_start;
    public $time_end;
    public $type;
    public $password;
    public $email;
    public $industry;
    public $contact;
    public $cellphone;
    public $telephone;
    public $province;
    public $city;
    public $town;
    public $address;
    public $version;
    public $created;

	public function validate()
	{
		return false;
	}
}
