<?php
namespace Models\Modules;
use Models\BaseModel;

class ResourceForSystemOrders extends BaseModel
{
    public $order_number;
    public $resource_id;
    public $trade_price;
    public $years;
    public $months;
    public $days;

	public function validate()
	{
		return false;
	}
}
