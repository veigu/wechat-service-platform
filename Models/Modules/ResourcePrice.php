<?php
/**
 * Created by PhpStorm.
 * User: luguiwu
 * Date: 14-5-6
 * Time: 上午10:06
 */

namespace Models\Modules;


use Models\BaseModel;

class ResourcePrice extends BaseModel
{
    public $id;
    public $sales_id;
    public $resource_id;
    public $year_price;
    public $month_price;
    public $year_price_min;
    public $year_price_max;
    public $month_price_min;
    public $month_price_max;
} 