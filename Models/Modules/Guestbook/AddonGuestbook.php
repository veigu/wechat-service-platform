<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-18
 * Time: 下午8:55
 */

namespace Models\Modules\Guestbook;


use Models\BaseModel;
use Models\User\Users;

class AddonGuestbook extends BaseModel
{
    const STATUS_PASS = 1;
    const STATUS_BLOCK = 0;

    public function validation()
    {

    }

    /**
     * get message by customer id
     *
     * @param mixed $customer_id
     * @access public
     * @return array
     */
    public function getMessagesByCustomerId($customer_id, $status = null, $limit = 10)
    {
        $conditions[] = "Models\Modules\Guestbook\AddonGuestbook.customer_id = $customer_id";
        if (!is_null($status)) {
            $conditions[] = "Models\Modules\Guestbook\AddonGuestbook.status = $status";
        }

        $conditions = implode(' AND ', $conditions);

        $data = self::query()
            ->where($conditions)
            ->leftJoin('Models\User\Users', 'ufc.id = Models\Modules\Guestbook\AddonGuestbook.user_id', 'ufc')
            ->columns("Models\Modules\Guestbook\AddonGuestbook.id, Models\Modules\Guestbook\AddonGuestbook.customer_id, Models\Modules\Guestbook\AddonGuestbook.user_id , Models\Modules\Guestbook\AddonGuestbook.message, Models\Modules\Guestbook\AddonGuestbook.reply, Models\Modules\Guestbook\AddonGuestbook.status, Models\Modules\Guestbook\AddonGuestbook.created, ufc.username")
            ->limit($limit)
            ->orderBy('Models\Modules\Guestbook\AddonGuestbook.created desc')
            ->execute();

        return $data;
    }

    /**
     * get status
     *
     * @access public
     * @return void
     */
    public function getStatus()
    {
        $states = array(
            self::STATUS_BLOCK => '禁止',
            self::STATUS_PASS => '通过'
        );

        return $states[$this->status];
    }

    public function getUsers($user_id)
    {
        return Users::findFirst($user_id);
    }
} 