<?php

namespace Models\Modules\Reserve;

use Models\BaseModel;
use Phalcon\Mvc\Model\Query\Builder as Querybuilder;

class AddonReserve extends BaseModel
{
    /**
     * get records
     *
     * @param $user_id
     * @param int $page
     * @param int $limit
     * @return bool|mixed
     */
    static public function getRecords($user_id = null, $page = 1, $limit = 7)
    {

        $queryBuilder = new Querybuilder;
        $query = $queryBuilder
            ->from('Models\Modules\Reserve\AddonReserve')
//             ->where("user_id = $user_id AND cardid = $vipcardNumber->id")
            ->limit($limit, ($page - 1) * $limit)
            ->getQuery()
            ->execute();

        return $query;
    }
}
