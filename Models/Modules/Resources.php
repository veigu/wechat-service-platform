<?php
namespace  Models\Modules;
use Models\BaseModel;

class Resources extends  BaseModel
{
    public $id;
    public $name;
    public $year_price;
    public $month_price;
    public $type;
    public $description;
    public $belong;
    public $settings;
    public $created;
    public $free_period;
    public $year_price_min;
    public $year_price_max;
    public $month_price_min;
    public $month_price_max;
}
