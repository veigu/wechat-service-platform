<?php

namespace Models\WeChat;

use Phalcon\Mvc\Model;

class MessageResourceNewsContent extends Model {
    public $id;
    public $customer_id;
    public $news_id;
    public $content;
}

?>