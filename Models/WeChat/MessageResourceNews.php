<?php

namespace Models\WeChat;

use Phalcon\Mvc\Model;

class MessageResourceNews extends Model {
    public $id;
    public $customer_id;
    public $title;
    public $cover;
    public $link;
    public $created;
}

?>