<?php

namespace Multiple\Agent;

use Components\Auth\AclListener;
use Components\StaticFileManager;
use Multiple\Agent\Plugins\Menubar;
use Phalcon\Events\Manager as EventManager;
use Phalcon\Flash\Direct;
use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;

class Module
{
	public function registerAutoloaders()
	{
        $loader = new Loader();

        $loader->registerNamespaces(array(
            'Multiple\Agent\Controllers' => 'Apps/Agent/Controllers/',
            'Multiple\Agent\Plugins' => 'Apps/Agent/Plugins/',
        ));

        $loader->register();
    }

    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    public function registerServices($di)
    {

		//Registering a dispatcher
		$di->set('dispatcher', function() {

            $dispatcher = new Dispatcher();

            //Attach a event listener to the dispatcher
            $eventManager = new EventManager();
            $eventManager->attach('dispatch', new AclListener('admin'));

            $dispatcher->setEventsManager($eventManager);
            $dispatcher->setDefaultNamespace("Multiple\\Agent\\Controllers\\");
            return $dispatcher;
        });

        //Registering the view component
        $di->set('view', function () {
            //Create an event manager
            $eventsManager = new EventManager();
            $viewListener = new StaticFileManager();
            //Attach a listener for type "view"
            $eventsManager->attach("view:beforeRender", $viewListener);
            $view = new View();
            $view->registerEngines(array(
                '.phtml' => 'volt'
            ));
            $view->setViewsDir('Apps/Agent/Views/');
            $view->setEventsManager($eventsManager);
			return $view;
		});

		$di->set("menubar", function() {
            return new Menubar('admin');
        });

        /**
         * Register the flash service with custom CSS classes
         */
	    $di->set('flash', function () {
            return new Direct(array(
                'error' => 'tips tips-small tips-warning',
                'success' => 'tips tips-small  tips-success',
                'notice' => 'tips tips-small tips-notice'
	        ));
	    });
	}

}