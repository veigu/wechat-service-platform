<?php

namespace Multiple\Agent\Controllers;

use Models\Logo;
use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Models\Sales;
use Models\ResourcePrice;


class SalesController extends ControllerBase
{
    /**
     * 数据异步加载
     */
    public function searchAction()
    {

    }

    /**
     * 异步加载数据库数据
     */
    public function listAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        }
        else {
            $sales = $this->session->get('admin_info');

            $query = Criteria::fromInput($this->di, "\\Models\\Sales", $_POST);
            $parameters = $query->getParams();
            $count = Sales::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', "int", 20);

            if(!$query instanceof \Phalcon\Mvc\Model\Criteria) {
                $query = Sales::query($this->di);
            }

            $list = $query->columns("id,name,email,alipay,qq,phone,role,plat_sales,first_agent,second_agent,plat_sales,area,FROM_UNIXTIME(created, '%Y-%m-%m') AS created")
                ->andWhere("first_agent={$sales->id}")->limit($limit, $start)->execute()->toArray();

            $result = array();
            foreach($list as $theSale)
            {
                $first_agent = $theSale['first_agent'];
                $second_agent = $theSale['second_agent'];

                if($first_agent !=0 && $first_agent !=null)
                {
                    $first_agent_name = $query->columns("name") ->where('id='. $first_agent)->execute()->toArray();
                    $theSale['first_agent'] = $first_agent_name[0]['name']."(ID:{$first_agent})";
                }else{
                    $theSale['first_agent'] = '';
                }

                if($second_agent !=0 && $second_agent !=null)
                {
                    $second_agent_name = $query->columns("name") ->where('id='. $second_agent)->execute()->toArray();
                    $theSale['second_agent'] = $second_agent_name[0]['name']."(ID:{$second_agent})";
                }else{
                    $theSale['second_agent'] = '';
                }
                $result[] = $theSale;
            }

            $this->response->setJsonContent(array(
                'rows' => $result,
                'results' => $count
            ))->send();
        }
    }

    /**
     * 删除数据
     */
    public function removeAction()
    {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $ids = $this->request->getPost('ids');
        if(!is_array($ids)) {
            $ids = array($this->filter->sanitize($ids, 'int'));
        }
        if(empty($ids)) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $this->db->begin();
        $result = true;
        try {
            foreach($ids as $id) {
                $sales = Sales::findFirst($id);
                if (!$sales) {
                    $result = false;
                    throw new \Exception("功能{$id}没有找到！");
                }

                //删除一级代理的同时，删除logo数据
                $logo =  Logo::findFirst("sales_id='{$id}'");
                if($logo)
                {
                    $logo->delete();
                }

                if (!$sales->delete()) {
                    $messages = [];
                    foreach ($sales->getMessages() as $message) {
                        $messages[] = (string) $message;
                    }
                    $result = false;
                    throw new \Exception($messages);
                }
            }
            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
        }

        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    /**
     * 添加记录
     */
    public function createAction()
    {
        $this->view->disable();
        $result = $this->saveSales();
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }


    public function editAction()
    {
        $this->view->disable();
        $id = $this->request->getPost('id', 'int');
        $result = $this->saveSales($id);
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }


    /**
     * 修改数据库记录
     * @param null $id
     * @return array|bool|string
     */
    private function saveSales($id = null)
    {
        if(is_numeric($id) && $id > 0) {
            $sales = Sales::findFirst($id);
            if ($sales == false) {
                return "功能未找到";
            }
        }
        else {
            $salesman = $this->session->get('admin_info');
            $sales = new Sales();
            $sales->created = time();
            $sales->plat_sales = 1;
            $sales->first_agent=$salesman->id;
            $sales->second_agent=0;
            $sales->role=$this->request->getPost('role', 'striptags');
        }

        $sales->name=$this->request->getPost('name', 'striptags');
        $sales->email=$this->request->getPost('email', 'striptags');
        $sales->qq=$this->request->getPost('qq', 'striptags');
        $sales->phone=$this->request->getPost('phone', 'striptags');
        $sales->area=$this->request->getPost('area', 'striptags');
        $sales->alipay=$this->request->getPost('alipay', 'striptags');
        $sales->password=$this->request->getPost('password', 'striptags');
        $password = sha1($sales->password);
        $sales->password=$password;

        if (!$sales->save()) {
            $messages = [];
            foreach ($sales->getMessages() as $message) {
                $messages[] = (string) $message;
            }
            return $messages;
        }
    }

    public function resourcedetailAction()
    {
        $sales = $this->request->get('sales', 'striptags');
        $query = new \Phalcon\Mvc\Model\Query("SELECT r.id as rid,p.id,r.name,p.year_price,p.month_price,r.type,r.description,r.belong,r.free_period,p.year_price_min,p.year_price_max,p.month_price_min,p.month_price_max,p.sales_id FROM \Models\Resources AS  r LEFT JOIN \Models\ResourcePrice AS p  ON sales_id={$sales} AND r.id=p.resource_id ", $this->getDI());
        $resource = $query->execute();

        $this->view->setVar('resources', $resource);
        $this->view->setVar('sales', $sales);
    }

    public function savedetailAction()
    {
        $id=$this->request->getPost('id', 'striptags');
        if(is_numeric($id) && $id > 0) {
            $resourcePrice = ResourcePrice::findFirst($id);
            if ($resourcePrice == false) {
                return "功能未找到";
            }
        }else{
            $resourcePrice = new ResourcePrice();
        }

        $resourcePrice->sales_id = $this->request->get('sales');
        $resourcePrice->resource_id = $this->request->getPost('rid', 'striptags');
        $resourcePrice->month_price = $this->request->getPost('month_price', 'striptags');
        $resourcePrice->year_price = $this->request->getPost('year_price', 'striptags');
        $resourcePrice->year_price_min = $this->request->getPost('year_price_min', 'striptags');
        $resourcePrice->year_price_max = $this->request->getPost('year_price_max', 'striptags');
        $resourcePrice->month_price_min = $this->request->getPost('month_price_min', 'striptags');
        $resourcePrice->month_price_max = $this->request->getPost('month_price_max', 'striptags');

        $result = true;
        if (!$resourcePrice->save()) {
            $result = false;
        }
        $this->response->setJsonContent(array(
            'success' => $result,
            'error' => ''
        ))->send();
    }

    public function editDetail()
    {

    }

    public function addDetail()
    {

    }
}