<?php

namespace Multiple\Agent\Controllers;

class IndexController extends ControllerBase
{

	public function indexAction()
	{
        $this->view->setLayout("main");
		\Phalcon\Tag::setTitle('平台管理员首页');
	}

    public function dashboardAction() {

    }

    /**
     * 一级代理
     */
    public function agentAction()
    {
        $this->view->setLayout("main");
        \Phalcon\Tag::setTitle('顶级代理管理平台');
    }
}