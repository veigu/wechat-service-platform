<?php

namespace Multiple\Agent\Controllers;

use Models\System\SystemAdmin;
class ControllerBase extends \Phalcon\Mvc\Controller
{
    /**
     * @var Admins
     */
    public $admin;
	public function initialize() {
		\Phalcon\Tag::setTitle("管理平台");
        $this->view->setLayout("blank");
		$controler = $this->dispatcher->getControllerName();
		if(!$this->session->get('agent_auth') && $controler != "session") {
		    $this->session->set('agent_referer', 'agent/' . $controler . '/' . $this->dispatcher->getActionName());
			$this->response->redirect('agent/session/index');
		}
		$adminInfo = $this->session->get('agent_info');
		$this->view->setVar('nick_name', $adminInfo->name);
		$this->admin = $adminInfo;
	}

	public function indexAction()
	{
		
	}
	
	protected function forward($uri)
	{
	    $uriParts = explode('/', $uri);
	    $partsNum = count($uriParts);
	    switch ($partsNum) {
	    	case 2:
	    	    return $this->dispatcher->forward(
	    	    array(
	    	    'controller' => $uriParts[0],
	    	    'action' => $uriParts[1]
	    	    )
	    	    );
	    	    break;
	    	case 3:
	    	    return $this->dispatcher->forward(
	    	    array(
	    	    'namespace' => $uriParts[0],
	    	    'controller' => $uriParts[1],
	    	    'action' => $uriParts[2]
	    	    )
	    	    );
	    	    break;
	    }
	}

}