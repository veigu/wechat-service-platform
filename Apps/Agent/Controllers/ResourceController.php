<?php

namespace Multiple\Agent\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Models\Modules\Resources;
use Phalcon\Logger;
use Models\Modules\ResourcePrice;


class ResourceController extends ControllerBase
{
    /**
     * 数据异步加载
     */
    public function searchAction()
    {

    }

    /**
     * 异步加载数据库数据
     */
    public function listAction() {

        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        }
        else {
            $sales = $this->session->get('admin_info');
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', "int", 20);
            $query = new \Phalcon\Mvc\Model\Query("SELECT resource_price.id,resources.name,resources.type,resources.free_period,resources.description,resources.belong,FROM_UNIXTIME(resources.created, '%Y-%m-%m') AS created,resource_price.year_price_min,resource_price.year_price_max,resource_price.month_price_min,resource_price.month_price_max, resource_price.year_price, resource_price.month_price FROM \Models\Modules\ResourcePrice as resource_price LEFT JOIN \Models\Modules\Resources as resources ON resources.id = resource_id WHERE sales_id={$sales->id} LIMIT {$limit} OFFSET {$start}", $this->getDI());
            $list = $query->execute()->toArray();
            $count = count($list);

            $result = array();
            foreach($list as $theResource)
            {
                $year_price_min = $theResource['year_price_min'];
                $year_price_max = $theResource['year_price_max'];
                $month_price_min = $theResource['month_price_min'];
                $month_price_max = $theResource['month_price_max'];

                $theResource['month_price_range']="{$month_price_min}-{$month_price_max}";
                $theResource['year_price_range']="{$year_price_min}-{$year_price_max}";

                $result[] = $theResource;
            }

            $this->response->setJsonContent(array(
                'rows' => $result,
                'results' => $count
            ))->send();
        }
    }

    /**
     * 删除数据
     */
    public function removeAction()
    {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $ids = $this->request->getPost('ids');
        if(!is_array($ids)) {
            $ids = array($this->filter->sanitize($ids, 'int'));
        }
        if(empty($ids)) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $this->db->begin();
        $result = true;
        try {
            foreach($ids as $id) {
                $resources = Resources::findFirst($id);
                if (!$resources) {
                    $result = false;
                    throw new \Exception("功能{$id}没有找到！");
                }

                if (!$resources->delete()) {
                    $messages = [];
                    foreach ($resources->getMessages() as $message) {
                        $messages[] = (string) $message;
                    }
                    $result = false;
                    throw new \Exception($messages);
                }
            }
            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
        }

        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    /**
     * 添加记录
     */
    public function createAction()
    {
        $this->di->get('logger')->log("respond result:" . "add", Logger::INFO);
        $this->view->disable();
        $result = $this->saveResource();
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }


    public function editAction()
    {
        $this->di->get('logger')->log("respond result:" . "edit", Logger::INFO);
        $this->view->disable();
        $id = $this->request->getPost('id', 'int');
        $result = $this->saveResource($id);
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    /**
     * 修改数据库记录
     * @param null $id
     * @return array|bool|string
     */
    private function saveResource($id = null)
    {
        $this->di->get('logger')->log("saveResource:" . $id, Logger::INFO);
        if(is_numeric($id) && $id > 0) {
            $resources = Resources::findFirst($id);
            if ($resources == false) {
                return "功能未找到";
            }
        }
        else {
            $resources = new Resources();
            $resources->created = time();
            $this->di->get('logger')->log("time:" . $resources->created, Logger::INFO);
        }

        $resources->name=$this->request->getPost('name', 'striptags');
        $resources->year_price=$this->request->getPost('year_price', 'striptags');
        $resources->month_price=$this->request->getPost('month_price', 'striptags');
        $resources->type=$this->request->getPost('type', 'striptags');
        $resources->description=$this->request->getPost('description', 'striptags');
        $resources->belong=$this->request->getPost('belong', 'striptags');
        $resources->free_period=$this->request->getPost('free_period', 'striptags');
        $resources->year_price_min=$this->request->getPost('year_price_min', 'striptags');
        $resources->year_price_max=$this->request->getPost('year_price_max', 'striptags');
        $resources->month_price_min=$this->request->getPost('month_price_min', 'striptags');
        $resources->month_price_max=$this->request->getPost('month_price_max', 'striptags');

        $this->di->get('logger')->log("year_price:" . $resources->year_price, Logger::INFO);
        $this->di->get('logger')->log("year_price_max:" . $resources->year_price_max, Logger::INFO);
        $this->di->get('logger')->log("year_price_min:" . $resources->year_price_min, Logger::INFO);
        $this->di->get('logger')->log("month_price:" .  $resources->month_price, Logger::INFO);
        $this->di->get('logger')->log("month_price_min:" . $resources->month_price_min, Logger::INFO);
        $this->di->get('logger')->log("month_price_max:" . $id, Logger::INFO);

        if( $resources->year_price > $resources->year_price_max || $resources->year_price <  $resources->year_price_min)
        {
            $this->di->get('logger')->log("error year:" . $id, Logger::INFO);
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' =>"年标价范围. {$resources->year_price_min} .-. {$resources->year_price_max}"
            ))->send();
            exit(1);
        }else if($resources->month_price > $resources->month_price_max || $resources->month_price <  $resources->month_price_min){
            $this->di->get('logger')->log("error month:" . $id, Logger::INFO);
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' =>"月标价范围. {$resources->month_price_min} .-. {$resources->month_price_max}"
            ))->send();
            exit(1);
        }

        if (!$resources->save()) {
            $messages = [];
            foreach ($resources->getMessages() as $message) {
                $messages[] = (string) $message;
            }
            return $messages;
        }
        return true;
    }


}