<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-24
 * Time: 下午5:59
 */

namespace Multiple\Agent\Controllers;


use Models\Users;
use Phalcon\Mvc\Model\Criteria;

class UserController extends ControllerBase {
    public function searchAction() {

    }

    public function listAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        }
        else {
            $query = Criteria::fromInput($this->di, "\\Models\\Users", $_POST);
            $time_start = $this->request->getPost('created_min', 'striptags', false);
            if(!empty($time_start)) {
                $time_start = strtotime($time_start);
            }
            $time_end = $this->request->getPost('time_end', 'striptags', false);
            if(!empty($time_end)) {
                $time_end = strtotime($time_end);
            }
            
            if($time_start > 0) {
                $query->andWhere("created > '{$time_start}'");
            }
            if($time_end > 0) {
                if($time_start > 0 && $time_end > $time_start || empty($time_start)) {
                    $query->andWhere("created <= '{$time_end}'");
                }
            }
            $parameters = $query->getParams();
            $count = Users::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', 'int', 20);

            $list = Users::find(array($parameters, 'limit' => $start . ','.$limit))->toArray();
            $this->response->setJsonContent(array(
                'rows' => $list,
                'results' => $count
            ))->send();
        }
    }
    
    public function detailAction() {
        
    }

} 