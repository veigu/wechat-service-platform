<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-24
 * Time: 上午11:12
 */

namespace Multiple\Agent\Controllers;


use Components\IndustryManager;
use Components\StaticFileManager;

class OperateConfigController extends ControllerBase {
    public function agentGradeAction() {

    }
    public function functionsAction() {

    }

    public function zoneAction() {

    }

    public function industryAction() {
        \Phalcon\Tag::setTitle('微信聚合平台-商品分类设置');
        $this->assets->addCss('static/bootstrap/css/bootstrap.css');
        $this->assets->addCss('static/bootstrap/css/jquery.tree.css');
        $this->assets->addCss('static/css/home/weixin/weixin.message.css');
        $this->assets->addJs('static/bootstrap/js/bootstrap.js');
        $this->assets->addJs('static/bootstrap/js/jquery.bootstrap.dialog.js');
        $this->assets->addJs('static/bootstrap/js/jquery.bootstrap.tree.js');
        $this->assets->addCss('static/js/jQuery-File-Upload/css/jquery.fileupload.css');
        $this->assets->addJs('static/js/jQuery-File-Upload/js/vendor/jquery.ui.widget.js');
        $this->assets->addJs('static/js/jQuery-File-Upload/js/jquery.iframe-transport.js');
        $this->assets->addJs('static/js/jQuery-File-Upload/js/jquery.fileupload.js');
        $this->assets->addJs('static/js/app/admin/company.industry.js');

        $categories = IndustryManager::instance()->getIndustries();

        $this->view->setVar('categories', $categories);
    }
} 