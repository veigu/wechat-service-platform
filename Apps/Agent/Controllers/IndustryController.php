<?php

namespace Multiple\Agent\Controllers;


use Components\IndustryManager;
use Components\StaticFileManager;

use Phalcon\Tag;
use Models\System\SystemIndustry;
class IndustryController extends ControllerBase
{
    public function addAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '只接受AJAX请求!'
            ));
            $this->response->send();
        }
        $id = $this->request->getPost('cid', 'int');
        $name = $this->request->getPost('name', 'string');
        $pid = $this->request->getPost('pid', 'int');
        $logo = $this->request->getPost('image', 'string');
        $sort = $this->request->getPost('sort', 'int');
        $status = $this->request->getPost('status', 'int');
        $desc = $this->request->getPost('description', 'string');
        
        $data = array(
            'code' => 0,
            'result' => '',
            'message' => ''
        );

        if(empty($name)) {
            $data['code'] = 1;
            $data['message'][] = "分类名称不能为空！";
        }
        if(empty($desc)) {
            $data['code'] = 1;
            $data['message'][] = "描述不能为空！";
        }
        
        if(empty($sort)) {
            $sort = 0;
        }
        if(empty($pid)) {
            $pid = 0;
        }


        if($data['code'] > 0) {
            $this->response->setJsonContent($data);
            $this->response->send();
            exit(1);
        }
        else {
            if($id) {
                $indusry = SystemIndustry::findFirst("id='{$id}'");
                $indusry->modified = time();
            }
            else {
                $indusry = new SystemIndustry();
                $indusry->created = time();
                $indusry->modified = time();
            }
            $indusry->pid = $pid;
            $indusry->name = $name;
            $indusry->image = $logo;
            $indusry->sort = $sort;
            $indusry->status = 1;
            $indusry->desc = $desc;              

            if(!$indusry->save()) {
                $data['code'] = 1;
                foreach($indusry->getMessages() as $message) {
                    $data['message'][] = (string)$message;
                }
            }
            else {
                IndustryManager::instance()->getIndustries(true);
                $data['result'] = $indusry->id;
            }
            $this->response->setJsonContent($data);
            $this->response->send();
        }
    }

    public function removeAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '只接受AJAX请求!'
            ));
            $this->response->send();
            exit(1);
        }
        $id = $this->request->getPost('cid', 'int');
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if(empty($id)) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '没有找到您要删除的数据!'
            ));
            $this->response->send();
            exit(1);
        }

        $category = SystemIndustry::findFirst($id);
        if($category) {
            if($category->pid == 0) {
                $subCategories = SystemIndustry::count("pid='{$id}'");
                if($subCategories > 0) {
                    $this->response->setJsonContent(array(
                        'code' => 1,
                        'message' => '该分类下面还有子分类没有删除，请先删除子分类!'
                    ));
                    $this->response->send();
                    exit(1);
                }
            }

            $result = array(
                'code' => 0,
                'message' => ''
            );
            //start a transaction

            if(!$category->delete()) {
                $messages = array();
                foreach($category->getMessages() as $message) {
                    $messages[] = $message;
                }
                $result = array(
                    'code' => 1,
                    'message' => $messages
                );
            }
            IndustryManager::instance()->getIndustries(true);
            $this->response->setJsonContent($result);
            $this->response->send();
            exit(1);
        }
        else {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '没有找到您要删除的数据!'
            ));
            $this->response->send();
            exit(1);
        }
    }

}