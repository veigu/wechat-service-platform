<?php
namespace Multiple\Agent\Plugins;
/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class Menubar extends \Phalcon\Mvc\User\Component
{

    private $_headerMenu = array(
        'left' => array(
            'index' => array(
                'caption' => '首页',
                'action' => ''
            ),
            'help' => array(
                'caption' => '帮助中心',
                'action' => ''
            ),
        ),
        'right' => array(
            'home\\session' => array(
                'caption' => '登录/注册',
                'action' => 'register'
            ),
        )
    );

    private $_tabs = array(
        '发票' => array(
            'controller' => 'invoices',
            'action' => 'index',
            'any' => false
        ),
        '企业' => array(
            'controller' => 'companies',
            'action' => 'index',
            'any' => true
        ),
        '产品' => array(
            'controller' => 'products',
            'action' => 'index',
            'any' => true
        ),
        '产品类型' => array(
            'controller' => 'producttypes',
            'action' => 'index',
            'any' => true
        ),
        '我的' => array(
            'controller' => 'invoices',
            'action' => 'profile',
            'any' => false
        )
    );

    /**
     * Builds header menu with left and right items
     *
     * @return string
     */
    public function getLeftMenu()
    {

        $auth = $this->session->get('auth');
//         unset($this->_headerMenu['left']['invoices']);
        
        $firm = 'app-'.$this->dispatcher->getParam('app');
        $controllerName = $this->view->getControllerName();
        $menu = $this->_headerMenu['left'];
        foreach ($menu as $controller => $option) {
            if ($controllerName == $controller) {
                echo '<li class="active">';
            } else {
                echo '<li>';
            }
            echo \Phalcon\Tag::linkTo($controller, $option['caption']);
            echo '</li>';
        }
    }
    
    /**
     * Builds header menu with left and right items
     *
     * @return string
     */
    public function getRightMenu()
    {
        $auth = $this->session->get('auth');
        if ($auth) {
            $this->_headerMenu['right']['home\\session'] = array(
                'caption' => '退出',
                'action' => 'end'
            );
        }
    
        $firm = 'app-'.$this->dispatcher->getParam('app');
        $controllerName = $this->view->getControllerName();
        $menu = $this->_headerMenu['right'];
        foreach ($menu as $controller => $option) {
            if ($controllerName == $controller) {
                echo '<li class="active">';
            } else {
                echo '<li>';
            }
            echo \Phalcon\Tag::linkTo($controller.'/'.$option['action'], $option['caption']);
            echo '</li>';
        }
    }

    public function getTabs()
    {
        $firm = 'app-'.$this->dispatcher->getParam('app');
        $controllerName = $this->view->getControllerName();
        $actionName = $this->view->getActionName();
        echo '<ul class="nav nav-tabs">';
        foreach ($this->_tabs as $caption => $option) {
            if ($option['controller'] == $controllerName && ($option['action'] == $actionName || $option['any'])) {
                echo '<li class="active">';
            } else {
                echo '<li>';
            }
            echo \Phalcon\Tag::linkTo($option['controller'].'/'.$option['action'], $caption), '<li>';
        }
        echo '</ul>';
    }
}
