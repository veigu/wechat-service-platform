<?php

namespace Multiple\Home;

use Phalcon\Mvc\View;


# 根据路径获取应用名称
$app_path = dirname(__FILE__);
$app_name = substr($app_path, strrpos($app_path, DIRECTORY_SEPARATOR) + 1);

define('MODULE_NAME', $app_name);
define("MODULE_PATH", __DIR__);

class Module
{
    public function registerAutoloaders()
    {

        $loader = new \Phalcon\Loader();

        $loader->registerNamespaces(array(
            'Multiple\Home\Controllers' => 'Apps/Home/Controllers/',
            'Multiple\Home\Controllers\Main' => 'Apps/Home/Controllers/main',
            'Multiple\Home\Plugins' => 'Apps/Home/Plugins/',
            'Modules' => 'Modules/'
        ));

        $loader->register();
    }

    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    public function registerServices($di)
    {

        //Registering a dispatcher
        $di->set('dispatcher', function () {

            $dispatcher = new \Phalcon\Mvc\Dispatcher();

            //Attach a event listener to the dispatcher
            $eventManager = new \Phalcon\Events\Manager();
            $eventManager->attach('dispatch', new \Components\Auth\AclListener('panel'));

            $dispatcher->setEventsManager($eventManager);
            $dispatcher->setDefaultNamespace("Multiple\\Home\\Controllers\\");
            return $dispatcher;
        });


        //Registering the view component
        $di->set('view', function () {
            //Create an event manager
            $eventsManager = new \Phalcon\Events\Manager();
            $viewListener = new \Components\StaticFileManager();
            //Attach a listener for type "view"
            $eventsManager->attach("view:beforeRender", $viewListener);
            $view = new \Phalcon\Mvc\View();
            $view->registerEngines(array(
                '.phtml' => "volt"
                // '.volt' => "volt"
            ));

            $view->setViewsDir('Apps/Home/Views/');

            $view->setEventsManager($eventsManager);
            return $view;
        });

    }


}