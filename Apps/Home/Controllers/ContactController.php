<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-15
 * Time: 上午10:50
 */

namespace Multiple\Home\Controllers;


use Models\System\SystemContactMessage;
use Phalcon\Tag;

class ContactController extends ControllerBase
{

    public function indexAction()
    {
        Tag::setTitle('联系我们');
    }

    public function saveMessageAction()
    {
        $this->view->disable();
        $contact_name = $this->request->getPost('contact_name', 'striptags', false);
        $contact_email = $this->request->getPost('contact_email', 'email', false);
        $contact_phone = $this->request->getPost('contact_phone', 'int', false);
        $contact_message = $this->request->getPost('contact_message', 'striptags', false);
        $ip = $this->request->getClientAddress();

        $dayTimeStart = strtotime(date("Y-m-d") . ' 00:00:00');
        $todayPosts = SystemContactMessage::count("ip = '{$ip}' AND created > $dayTimeStart");

        if ($todayPosts > 5) {
            return $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '您今天已经连续提交了5次申请，我们建议您直接跟我们的客服联系，谢谢合作！'
            ))->send();
        }

        $message = new SystemContactMessage();
        $message->name = $contact_name;
        $message->email = $contact_email;
        $message->phone = $contact_phone;
        $message->message = $contact_message;
        $message->ip = $ip;
        $message->created = time();
        if (!$message->save()) {
            $messages = [];
            foreach ($message->getMessages() as $msg) {
                $messages[] = (string)$msg;
            }
            return $this->response->setJsonContent(array(
                'code' => 1,
                'message' => $messages
            ))->send();
        } else {
            return $this->response->setJsonContent(array(
                'code' => 0,
                'message' => '您的申请已经提交成功，我们将会尽快处理，谢谢您的支持与合作！'
            ))->send();
        }
    }
} 