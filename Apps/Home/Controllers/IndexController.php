<?php

namespace Multiple\Home\Controllers;

use Models\help\SystemHelpArticle;
use Models\help\SystemHelpCat;
use Models\User\UsersWechat;
use Phalcon\Tag;

class IndexController extends ControllerBase
{
    public function notFoundAction()
    {
        $this->flash->error("not found");
    }

    public function indexAction()
    {
        Tag::setTitle(HOST_BRAND);
    }

    public function servicesAction()
    {
        Tag::setTitle('功能列表');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function casesAction()
    {
        Tag::setTitle('案例展示');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function agentAction()
    {
        Tag::setTitle('代理加盟');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function helpAction()
    {
        Tag::setTitle('帮助中心');
        $helpCats = SystemHelpCat::find(array("status=1", 'sort' => 'sort', 'columns' => "id, name, image"));
        $helpArticles = [];
        foreach ($helpCats as $cat) {
            $articles = SystemHelpArticle::find(array("cid='{$cat->id}'", 'columns' => 'id, title'));
            if (!$articles) {
                $articles = [];
            }
            $helpArticles[] = array(
                'category' => $cat,
                'articles' => $articles
            );
        }
        $this->view->setVar('articles', $helpArticles);
    }

    public function helpTopicAction($topic = null)
    {
        Tag::setTitle('帮助中心');
        $topic = $this->request->get('topic', array('striptags', 'trim'));
        if ($topic) {
            $topic = SystemHelpArticle::findFirst("id='$topic'");
            if (!$topic) {
                $this->flash->notice("没有找到你想要的主题哦");
            } else {
                $this->view->setVar('article', $topic);
            }
        } else {
            $this->flash->notice("没有找到你想要的主题哦");
        }
    }

    public function aboutAction()
    {
        Tag::setTitle('关于我们');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function descAction()
    {
        Tag::setTitle('产品介绍');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function customerAction()
    {
        Tag::setTitle('客服');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function dataAction()
    {
        Tag::setTitle('数据中心');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function weixingwallAction()
    {
        Tag::setTitle('微信墙');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function tuangouAction()
    {
        Tag::setTitle('微团购');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function shequAction()
    {
        Tag::setTitle('社区');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function shangchengAction()
    {
        Tag::setTitle('商城');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function vipAction()
    {
        Tag::setTitle('会员');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function cityAction()
    {
        Tag::setTitle('会员');
        /* $this->dispatcher->forward(array(
         'controller' => 'login',
            'action' => 'index'
        )); */
    }

    public function contactAction()
    {
        $uw = new UsersWechat();
        $this->db->begin();
        try {
            /*$uw->open_id = 'asfasfasfsafsdfasdfasdf';
            $uw->nickname = 'www';
            $uw->language = 'zh_CN';
            $uw->headimgurl = "http:\/\/dfasdfafasdfasdfasdfasdfasdfasdfasd";
            $uw->user_id = 222;
            $uw->save();
            $this->db->commit();*/
            $openId = "oeZ6AuEivzIC6C712GyKIDzxh1so";
            $this->customer = 233;
//            $userCustomer = Cus::findFirst("open_id='{$openId}' AND customer_id='{$this->customer}' AND type='wx'");
//            var_dump($userCustomer);
        } catch (\Exception $e) {
            $this->db->rollback();
            echo $e->getMessage();
        }
        exit(1);
    }

    public function priceAction()
    {

    }





}