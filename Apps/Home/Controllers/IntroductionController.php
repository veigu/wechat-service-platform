<?php
/**
 * Created by PhpStorm.
 * User: kuangyong
 * Date: 14-7-16
 * Time: 中午12:05
 */

namespace Multiple\Home\Controllers;



use Phalcon\Tag;

class IntroductionController extends ControllerBase
{

    public function indexAction()
    {
        Tag::setTitle('公司简介');
    }
}