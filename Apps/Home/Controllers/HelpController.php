<?php
namespace Multiple\Home\Controllers;

use Models\System\SystemHelpArticle;
use Models\System\SystemHelpCat;

class HelpController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();

        \Phalcon\Tag::setTitle("系统帮助中心！");

        $helpCats = SystemHelpCat::find(array("status=1 AND host_key='" . HOST_KEY . "'", 'sort' => 'sort', 'columns' => "id, name, image"));
        $helpArticles = [];
        foreach ($helpCats as $cat) {
            $articles = SystemHelpArticle::find(array("cid='{$cat->id}'", 'columns' => 'id, title'));
            if (!$articles) {
                $articles = [];
            }
            $helpArticles[] = array(
                'category' => $cat,
                'articles' => $articles
            );
        }
        $this->view->setVar('articles', $helpArticles);
    }

    public function indexAction()
    {
    }

    public function topicAction()
    {
        $topic = $this->request->get('topic', array('striptags', 'trim'));
        if ($topic) {
            $topic = SystemHelpArticle::findFirst("id='$topic'");
            if (!$topic) {
                $this->flash->notice("没有找到你想要的主题哦");
            } else {
                $this->view->setVar('topic', $topic);
            }
        } else {
            $this->flash->notice("没有找到你想要的主题哦");
        }
    }
}
