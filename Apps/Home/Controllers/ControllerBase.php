<?php

namespace Multiple\Home\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    /**
     * @var \Models\Customer\Customers
     */
    protected $customer = null;
    protected $customer_id = 0;
    /**
     * @var \Models\Customer\CustomerOpenInfo
     */
    protected $customer_wechat = null;

    public function initialize()
    {
        $this->view->setLayout('index');

        if ($this->is_wap()) {
            $viewDir = trim($this->view->getViewsDir(), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'wap' . DIRECTORY_SEPARATOR;
        }
        else {
            $viewDir = trim($this->view->getViewsDir(), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'pc' . DIRECTORY_SEPARATOR;
        }

        if (is_dir(ROOT . DIRECTORY_SEPARATOR . $viewDir . HOST_KEY)) {
            $viewDir = $viewDir . HOST_KEY;
        }
        else {
            $viewDir = $viewDir . "main";
        }

        /**
         * fingoo special handle
         */
        if (HOST_KEY == "reach") {
            if ($this->is_wap()) {
                header("location:http://www.yimasou.com/index.php?g=Wap&m=Index&a=index&token=youneq1404199993");
            }
        }
        $this->view->setViewsDir($viewDir);
    }


    protected function err($code = "404", $msg = '404 page no found')
    {
        $this->response->setStatusCode($code, $msg);

        $this->view->setVar('msg', $msg);

        $this->view->pick('index/error');
    }

    protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $partsNum = count($uriParts);
        switch ($partsNum) {
            case 2:
                return $this->dispatcher->forward(
                    array(
                        'controller' => $uriParts[0],
                        'action' => $uriParts[1]
                    )
                );
                break;
            case 3:
                return $this->dispatcher->forward(
                    array(
                        'namespace' => $uriParts[0],
                        'controller' => $uriParts[1],
                        'action' => $uriParts[2]
                    )
                );
                break;
        }
    }

    protected function is_wap()
    {
        $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
        $uachar = "/(nokia|sony|ericsson|mot|samsung|sgh|lg|philips|panasonic|alcatel|lenovo|cldc|midp|wap|mobile)/i";
        if (preg_match($uachar, $ua)) {//如果在访问的URL中已经找到 wap字样，表明已经在访问WAP页面，无需跳转，下一版本增加 feed访问时也不跳转
            return true;
        } else {
            return false;
        }
    }
}