<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-15
 * Time: 上午10:50
 */

namespace Multiple\Home\Controllers;


use Models\Modules\Resources;

class AboutController extends ControllerBase
{

    public function indexAction()
    {
    }

    public function saveAction($name = null)
    {
        $name = $this->dispatcher->getParams()[0];
        if (empty($name)) {
            return $this->dispatcher->forward(array(
                'action' => "index"
            ));
        } else {
            $module = Resources::findFirst("module_name='{$name}'");
            if (!$module) {
                $this->flash->error("对不起，暂时没有提供此项服务");
            }
            $this->view->setVar('module', $module);
        }
    }
} 