<?php

namespace Multiple\Shop;

use Components\StaticFileManager;
use Phalcon\Events\Manager as EventManager;
use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;

# 根据路径获取应用名称
$app_path = dirname(__FILE__);
$app_name = substr($app_path, strrpos($app_path, DIRECTORY_SEPARATOR) + 1);

define('MODULE_NAME', $app_name);
define("MODULE_PATH", __DIR__);

class Module
{
    public function registerAutoloaders()
    {

        $loader = new Loader();

        $loader->registerNamespaces(array(
            'Multiple\Shop\Controller' => 'Apps/Shop/Controller/',
            'Multiple\Shop\Helper' => 'Apps/Shop/Helper/',
        ));

        $loader->register();
    }

    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    public function registerServices($di)
    {

        //Registering a dispatcher
        $di->set('dispatcher', function () {

            $dispatcher = new Dispatcher();

            $dispatcher->setDefaultNamespace("Multiple\\Shop\\Controller\\");
            return $dispatcher;
        });


        //Registering the view component
        $di->set('view', function () {
            //Create an event manager
            $eventsManager = new EventManager();
            $viewListener = new StaticFileManager();
            //Attach a listener for type "view"
            $eventsManager->attach("view:beforeRender", $viewListener);
            $view = new View();
            $view->registerEngines(array(
                '.phtml' => 'Phalcon\Mvc\View\Engine\Php',
                '.phtml' => "volt"
            ));
            $view->setMainView("index");
            $view->setLayout("main");
            $view->setLayoutsDir('layout');
            $view->setViewsDir('Apps/Shop/View');

            $view->setEventsManager($eventsManager);
            return $view;
        });

    }


}