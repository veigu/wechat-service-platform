<?php
namespace Multiple\Shop\Controller;

use Components\Product\TransactionManager;
use Models\Customer\Customers;
use Models\Shop\ShopFreightTpl;
use Phalcon\Tag;

class FreightController extends ShopBase
{
    public function indexAction()
    {
        Tag::setTitle("物流模板列表");
        $this->assets->addJs("static/ace/js/jquery.form.js");
        $this->assets->addJs("static/panel/js/app/panel/shop/setting.logistics.js");
        $freight = ShopFreightTpl::find("customer_id='{$this->customer_id}'");
        $this->view->setVar('freight', $freight);
    }

    public function addAction()
    {
        Tag::setTitle("物流模板设置");
    }

    public function logisticAction()
    {
        Tag::setTitle("物流方式");
        $this->assets->addJs("static/ace/js/jquery.form.js");
        $this->assets->addJs("static/panel/js/app/panel/shop/setting.logistics.js");
        $logistics = TransactionManager::instance(HOST_KEY)->getCustomerLogistics($this->customer_id, true);
        $this->view->setVar('logistics', $logistics);
    }


}