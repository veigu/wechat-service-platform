<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2014/10/28
 * Time: 16:09
 */

namespace Multiple\Shop\Controller;

use Models\Product\Distribution\ProductShareStatMonth;
use Models\Shop\Shop;

class PromotionController extends ShopBase
{
    public function settingAction()
    {
        if ($this->request->isPost()) {
            $a = $this->request->getPost();
            $cashback_enable = $this->request->getPost('cashback_enable', 'int');
            $cashback_proportion = $this->request->getPost('cashback_proportion', 'int');
            $cashback_proportion = $cashback_proportion > 100 ? 100 : $cashback_proportion;
            $cashback_proportion = $cashback_proportion < 0 ? 0 : $cashback_proportion;
            $data = array("cashback_enable" => $cashback_enable, "cashback_proportion" => $cashback_proportion);
            $shop = Shop::findFirst('customer_id=' . CUR_APP_ID);
            if (!$shop->save($data)) {
                $messages = '';
                foreach ($shop->getMessages() as $message) {
                    $messages .= $message . '。';
                }
                $this->flash->error($messages);
            } else {
                $this->flash->success("资料保存成功！");
            }
        }

        // 设置
        $shop = Shop::findFirst('customer_id=' . CUR_APP_ID);
        $this->view->setVar('shop', $shop->toArray());

        // 统计
        $stat = ProductShareStatMonth::findFirst('customer_id=' . CUR_APP_ID . ' and status=1 and month=' . date("Ym"));
        $this->view->setVar('month_stat', $stat ? $stat->toArray() : '');
    }

    public function statAction()
    {
    }

    public function orderAction()
    {
    }
} 