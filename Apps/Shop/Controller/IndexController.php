<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/22
 * Time: 14:27
 */

namespace Multiple\Shop\Controller;


use Components\ModuleManager\ModuleManager;
use Phalcon\Tag;

class IndexController extends ShopBase
{
    public function indexAction()
    {
        Tag::setTitle('管理平台');
        $this->view->setVar('customer', $this->customer);

        $this->view->setVar('customer_wechat', $this->customer_wechat);


        $notice = '';
        if (!empty($notice)) {
            $this->flash->notice($notice);
        }
    }
} 