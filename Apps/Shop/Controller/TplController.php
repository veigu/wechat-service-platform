<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/8/14
 * Time: 11:37 AM
 */

namespace Multiple\Shop\Controller;


use Components\TplManager;
use Models\Wap\SiteTpl;
use Phalcon\Mvc\Model\Query;
use Phalcon\Tag;

class TplController extends ShopBase
{
    protected $tpl;

    public function initialize()
    {
        parent::initialize();

        $this->tpl = $tpl = TplManager::init(CUR_APP_ID)->getTpl(true);

        $this->view->setVar('tpl', $tpl);
    }


    public function indexAction()
    {

    }

    /**
     * type kit
     */
    public function kitAction()
    {
        Tag::setTitle('套装模板 - 模板选择');

        $curTpl = json_decode($this->tpl['kit'], true);
        $this->view->setVar('curTpl_serial', $curTpl['serial_number']);

        $all = SiteTpl::find('type="kit"');
        $this->view->setVar('list', $all ? $all->toArray() : []);
    }

    public function singleAction()
    {
        $type = $this->request->get('type');
        $type = in_array($type, array('home', 'user', 'shop', 'menu')) ? $type : 'home';
        $all = SiteTpl::find(array('type="' . $type . '"', 'order' => "serial_number asc"));
        $this->view->setVar('list', $all ? $all->toArray() : []);

        $curTpl = json_decode($this->tpl[$type], true);
        $this->view->setVar('curTpl_serial', $curTpl['serial_number']);

        Tag::setTitle('单品模板  - ' . $this->title[$type]);
    }

    public function customizeAction()
    {
        Tag::setTitle('个性定制模板 - 套装模板');

        $curTpl = json_decode($this->tpl['customize'], true);
        $this->view->setVar('curTpl_serial', $curTpl['serial_number']);

        $all = SiteTpl::find('type="customize"');
        $this->view->setVar('list', $all ? $all->toArray() : []);
    }
}