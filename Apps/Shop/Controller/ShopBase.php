<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/22
 * Time: 14:27
 */

namespace Multiple\Shop\Controller;


use Components\CustomerManager;
use Components\PackageManager;
use Components\Product\TransactionManager;
use Models\Customer\Customers;
use Phalcon\Mvc\Controller;
use Phalcon\Tag;

class ShopBase extends Controller
{
    /**
     * @var \Models\Customer\Customers
     */
    protected $customer = null;
    protected $customer_id = 0;
    protected $store = null;

    /**
     * @var \Models\Customer\CustomerOpenInfo
     */
    protected $customer_wechat = null;

    /**
     * @var \Models\Customer\CustomerOpenInfo
     */
    protected $customer_weibo = null;

    public function initialize()
    {
        $auth = $this->session->get('customer_auth');

        $this->view->setVar('auth', $auth);

        if (empty($auth)) {
            $this->session->set("current_request_url", $this->uri->fullUrl());
            return $this->response->redirect($this->uri->baseUrl('/account/login'))->send();
        }

        $this->customer = $this->session->get("customer_info");
        if (!$this->customer instanceof Customers) {
            return $this->response->redirect($this->uri->baseUrl('/account/login'))->send();
        }

        $this->customer_wechat = $this->session->get("customer_wechat");
        $this->customer_weibo = $this->session->get("customer_weibo");

        if (!defined('CUR_APP_ID')) define('CUR_APP_ID', $this->customer->id);
        // 检查权限
        if (!$this->_checkPermission($this->customer->active)) {
            return false;
        }
        $this->store = $this->view->store = TransactionManager::instance(HOST_KEY)->getStoreSetting(CUR_APP_ID, true);
        $this->view->setVar("customer_id", CUR_APP_ID);
        $this->view->setVar("customer", $this->customer);
        $customer_name = empty($this->customer->name) ? $this->customer->account : $this->customer->name;
        $this->view->setVar("customer_name", $customer_name);
    }

    private function _checkPermission()
    {
        $customer_package = $this->session->get('customer_package');
        if ($customer_package == 'custom') {
            if (!($customer_package['has_shop_base'] || $customer_package['has_shop_full'])) {
                return $this->err('400', '对不起，您的自选套餐尚未选择商城功能！<a href="/panel">回到微站管理 ></a>');
            }
        } else {
            if (!($customer_package['has_shop_base'] || $customer_package['has_shop_full'])) {
                return $this->err('400', '对不起，' . PackageManager::$package_name[$customer_package['package']] . '套餐不支持商城功能！<a href="/panel">回到微站管理 ></a>');
            }
        }

        // 已经过期
        if (in_array($this->customer->active, array(CustomerManager::ACCOUNT_STATUS_EXPIRED_TRIAL, CustomerManager::ACCOUNT_STATUS_EXPIRED_PACKAGE))) {
            return $this->err('400', '对不起，' . @CustomerManager::$_active_name[$this->customer->active] . '，无法使用该扩展功能！<a href="/panel">回到微站管理 ></a>');
        }

        return true;
    }

    /**
     * render custom err page
     *
     * usage:
     * return $this->err();
     *
     * @param string $code
     * @param string $msg
     * @return mixed
     */
    protected function err($code = "404", $msg = '404 page no found')
    {
        Tag::setTitle('运行时错误');
        $this->view->setViewsDir(MODULE_PATH . '/View');
        $this->response->setHeader('content-type', 'text/html;charset=utf-8');
        $this->response->setStatusCode($code, $msg);

        $this->view->setVar('msg', $msg);

        return $this->view->pick('base/error');
    }

} 