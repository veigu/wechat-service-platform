<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-16
 * Time: 上午10:20
 */

namespace Multiple\Shop\Controller;


use Components\Payments\PaymentUtil;
use Models\Payments\PaymentConfigItems;
use Models\Payments\PaymentForCustomers;

class PaymentController extends ShopBase
{

    public function paymentAction()
    {
        \Phalcon\Tag::setTitle("设置——支付方式");
        $this->assets->addJs("static/ace/js/jquery.form.js");
        $this->assets->addJs("static/panel/js/app/panel/shop/setting.payment.js");
        $payments = PaymentUtil::instance(HOST_KEY)->getCustomerPayments(CUR_APP_ID, null, true);
        $this->view->setVar('payments', $payments);
    }

    public function paymentDetailAction()
    {
        $this->view->disable();
        $type = $this->request->getPost('type', 'striptags', false);
        if (!$type) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => "没有找到对应的支付方式"
            ))->send();
            exit;
        }

//        $config = PaymentUtil::instance(HOST_KEY)->getCustomerPaymentConfig(CUR_APP_ID, $type, true);
        $metas = PaymentUtil::instance(HOST_KEY)->getPaymentConfigMeta($type, true);
        if ($metas) {
            $data = [];
            foreach ($metas as $meta) {
                $config = PaymentConfigItems::findFirst("config_key='{$meta['meta_name']}' AND belong_type='" . PaymentUtil::BELONG_TYPE_CUSTOMER . "' AND key_id=" . CUR_APP_ID . " AND payment_type='{$type}'");
                $meta['config_value'] = $config ? $config->config_value : "";
                $data[] = $meta;
            }
        }

        $this->response->setJsonContent(array(
            'code' => 0,
            'result' => $data
        ))->send();
        exit;
    }

    public function paymentConfigSaveAction()
    {
        $this->view->disable();
        $type = $this->request->getPost('payment_key', 'striptags');
        if (!$type) {
            $this->response->setJsonContent(array("code" => 1, "message" => "没有指定支付方式"));
            $this->response->send();
            exit;
        }

        $is_active = $this->request->getPost('is_active', 'int', 0);
        $use_platform = $this->request->getPost('use_platform', 'int', 0);

        $messages = [];
        $code = 0;

        $this->db->begin();
        try {
            if ($is_active && !$use_platform) {
                $config = PaymentUtil::instance(HOST_KEY)->getPaymentConfigMeta($type, true);
                $postData = $this->request->getPost();
                foreach ($config as $item) {
                    if (!array_key_exists($item['meta_name'], $postData) || empty($postData[$item['meta_name']])) {
                        $code = 1;
                        $messages[] = "保存失败！如果要激活此支付方式，请填写完整配置参数";
                        throw new \Phalcon\Exception(join(',', $messages));
                    }
                }
            }

            foreach ($this->request->getPost() as $key => $val) {
                if ($key != 'payment_key' && $key != 'is_active' && $key != 'use_platform') {
                    $configItem = new PaymentConfigItems();
                    $configItem->belong_type = PaymentUtil::BELONG_TYPE_CUSTOMER;
                    $configItem->key_id = CUR_APP_ID;
                    $configItem->payment_type = $type;
                    $configItem->config_key = $key;
                    $configItem->config_value = $val;

                    if (!$configItem->save()) {
                        foreach ($configItem->getMessages() as $message) {
                            $messages[] = (string)$message;
                        }
                        throw new \Phalcon\Exception(join(',', $messages));
                    }
                }
            }

            // update cache
            PaymentUtil::instance(HOST_KEY)->getCustomerPaymentConfig(CUR_APP_ID, $type, true);

            $paymentForCustomer = PaymentForCustomers::findFirst("customer_id=" . CUR_APP_ID . " AND type='{$type}'");
            if (!$paymentForCustomer) {
                $paymentForCustomer = new PaymentForCustomers();
                $paymentForCustomer->customer_id = CUR_APP_ID;
                $paymentForCustomer->type = $type;
                $paymentForCustomer->is_active = $is_active;
                $paymentForCustomer->use_platform = $use_platform;
                if (!$paymentForCustomer->save()) {
                    $code = 1;
                    foreach ($paymentForCustomer->getMessages() as $message) {
                        $messages[] = (string)$message;
                        throw new \Phalcon\Exception(join(',', $messages));
                    }
                }
            } else {
                if (!$paymentForCustomer->update(array(
                    'is_active' => $is_active,
                    'use_platform' => $use_platform
                ))
                ) {
                    $code = 1;
                    foreach ($paymentForCustomer->getMessages() as $message) {
                        $messages[] = (string)$message;
                        throw new \Phalcon\Exception(join(',', $messages));
                    }
                }
            }
            //update cache
            PaymentUtil::instance(HOST_KEY)->getCustomerPayments(CUR_APP_ID, $type, true);

            $this->db->commit();
        } catch (\Phalcon\Exception $e) {
            $this->db->rollback();
            $messages = $e->getMessage();
            $code = 1;
            $this->di->get('errorLogger')->error($e->getFile() . ' line ' . $e->getLine() . ':' . $e->getMessage());
        }

        $this->response->setJsonContent(array(
            'code' => $code,
            'message' => $messages
        ))->send();
    }
}