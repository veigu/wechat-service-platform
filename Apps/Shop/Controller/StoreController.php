<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-5-22
 * Time: 下午5:18
 */

namespace Multiple\Shop\Controller;


use Components\Product\TransactionManager;
use Components\StaticFileManager;
use Components\TplManager;
use Models\Shop\Shop;
use Models\Wap\SiteFocus;
use Models\Wap\SiteNavs;
use Models\Wap\SiteTpl;
use Phalcon\Tag;

class StoreController extends ShopBase
{
    /**
     * @var \Models\Shop\Shop
     */
    public $store_info = null;

    protected $tpl;
    public function initialize()
    {
        parent::initialize();
        $storeInfo = Shop::findFirst("customer_id=" . CUR_APP_ID . "");
        if ($storeInfo instanceof Shop) {
            $this->store_info = $storeInfo;
        }
        $this->tpl = $tpl = TplManager::init(CUR_APP_ID)->getTpl(true);

        $this->view->setVar('tpl', $tpl);
    }

    public function indexAction()
    {
        if ($this->request->isPost()) {
            $request = $this->request;
            $name = $request->getPost('name', 'striptags');
            $logo = $request->getPost('logo', 'striptags');
            $background = $request->getPost('background', 'striptags');
            $phone = $request->getPost('telephone', 'int');
            $province = $request->getPost('province', 'striptags');
            $city = $request->getPost('city', 'striptags');
            $town = $request->getPost('town', 'striptags');
            $address = $request->getPost('address', 'striptags');
            $intro = $request->getPost('intro', 'striptags');
            $guide = $request->getPost('guide', 'striptags');
            $service = $request->getPost('service', 'striptags');
            $enable = intval($request->getPost('enable', 'int'));
            $back_point = intval($request->getPost('back_point', 'int'));
            $orders_proportion = intval($request->getPost('orders_proportion', 'int'));
            $redeem_proportion = intval($request->getPost('redeem_proportion', 'int'));
            $firm_orders_proportion = intval($request->getPost('firm_orders_proportion', 'int'));
            $firm_redeem_proportion = intval($request->getPost('firm_redeem_proportion', 'int'));
            $free_postage = intval($request->getPost('free_postage', 'int'));

//            $pay_type = $request->getPost('pay_type', 'striptags');
            $enable_cod = $request->getPost('enable_cod', 'int');
            $keywords = $request->getPost('keywords');

            if (empty($name) || mb_strlen($name, 'utf8') < 2 || mb_strlen($name, 'utf8') > 20) {
                $this->flash->error("店铺名称必须正确设置，长度为2~20个字符");
            } else {
                $storeInfo = Shop::findFirst("customer_id=" . CUR_APP_ID . "");
                if (!$storeInfo) {
                    $storeInfo = new Shop();
                    $storeInfo->customer_id = CUR_APP_ID;
                }
                $storeInfo->name = $name;
                $storeInfo->enable = intval($enable);
                $storeInfo->enable_cod = intval($enable_cod);
//                $storeInfo->pay_type = trim($pay_type);
                $storeInfo->logo = $logo;
                $storeInfo->background = $background;
                $storeInfo->phone = $phone;
                $storeInfo->province = $province;
                $storeInfo->city = $city;
                $storeInfo->town = $town;
                $storeInfo->address = $address;
                $storeInfo->introduction = $intro;
                $storeInfo->consume_guide = $guide;
                $storeInfo->customer_service = $service;
                $storeInfo->keywords = $keywords;
                $storeInfo->back_point = $back_point;
                $storeInfo->orders_proportion = $orders_proportion;
                $storeInfo->redeem_proportion = $redeem_proportion;
//                $storeInfo->firm_redeem_proportion = $firm_redeem_proportion;
//                $storeInfo->firm_orders_proportion = $firm_orders_proportion;
                $storeInfo->free_postage = $free_postage;

                if (!$storeInfo->save()) {
                    $messages = '';
                    foreach ($storeInfo->getMessages() as $message) {
                        $messages .= $message . '。';
                    }
                    $this->flash->error($messages);
                } else {
                    $this->flash->success("资料保存成功！");
                }
                $this->store_info = $storeInfo;
                TransactionManager::instance(HOST_KEY)->getStoreSetting(CUR_APP_ID, true);
            }
        }

        \Phalcon\Tag::setTitle("商城管理——店铺设置");
        $this->assets->addJs('static/panel/js/app/panel/shop/store.setting.js');
        if ($this->store_info instanceof Shop) {
            $this->view->setVar('store', $this->store_info);
        }

    }

   /* public function navAction()
    {
        Tag::setTitle('网站设置 - 微网站分类导航 ');

        StaticFileManager::addCssFile('static/wap/css/color.css');

        $navs = SiteNavs::find('customer_id=' . CUR_APP_ID . " and position = 'part' AND page_type='shop'");
        $this->view->setVar('list', $navs ? $navs->toArray() : []);
    }

    public function footAction()
    {
        Tag::setTitle('网站设置 - 底部快捷导航');

        StaticFileManager::addCssFile('static/wap/css/color.css');

        $navs = SiteNavs::find('customer_id=' . CUR_APP_ID . " and position = 'foot' AND page_type='shop'");
        $this->view->setVar('list', $navs ? $navs->toArray() : []);
    }

    public function focusAction()
    {
        Tag::setTitle('网站设置 - 微网站焦点图片');

        $focus = SiteFocus::find('customer_id=' . CUR_APP_ID . " AND page_type='shop'");
        $this->view->setVar('list', $focus ? $focus->toArray() : []);
    }*/

 /*   public function tplAction()
    {
        Tag::setTitle('我的模板 - 模板选择');
        $type = 'shop';
        $all = SiteTpl::find(array('type="' . $type . '"', 'order' => "serial_number asc"));
        $this->view->setVar('list', $all ? $all->toArray() : []);
        $curTpl = json_decode($this->tpl[$type], true);


        $this->view->setVar('curTpl_serial', $curTpl['serial_number']);

        Tag::setTitle('单品模板  - ' . $this->title[$type]);
    }
    */

} 