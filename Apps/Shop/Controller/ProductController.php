<?php

namespace Multiple\Shop\Controller;

use Models\Product\Product;
use Models\Product\ProductBrand;
use Models\Product\ProductCat;
use Phalcon\Tag;
use Util\Pagination;

class ProductController extends ShopBase
{
    public function catAction()
    {
        Tag::setTitle('微商城 - 商品分类设置');
    }

    public function attrAction()
    {
        Tag::setTitle('微商城 - 商品类目属性设置');
    }

    public function specAction()
    {
        Tag::setTitle('微商城 - 商品规格设置');
    }

    public function editAction()
    {
        Tag::setTitle('微商城 - 商品编辑');
    }

    public function brandAction()
    {
        Tag::setTitle('微商城 - 商品分类设置');
        $list = ProductBrand::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('list', $list ? $list->toArray() : []);
    }

    public function listAction()
    {
        Tag::setTitle('微商城 - 商品列表');

        $page = $this->request->get('p');
        $cid = $this->uri->getParam('cid');
        $key = $this->request->get('key');
        $where = is_numeric($cid) && $cid >= 0 ? ' customer_id=' . CUR_APP_ID . ' and cid = ' . $cid : ' customer_id=' . CUR_APP_ID;
        $where = $key ? $where . ' and name like "%' . $key . '%"' : $where;
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 12;

        $count = Product::count($where);
        $res = Product::find(array($where, "order" => "created desc", "limit" => $curpage * $limit . "," . $limit));
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $res ? $res->toArray() : []);

        $cats = ProductCat::find('customer_id = ' . CUR_APP_ID);
        $this->view->setVar('cats', $cats ? $cats->toArray() : []);
    }
}
