<?php
namespace Multiple\Shop\Controller;

use Components\Module\VipcardManager;
use Components\Payments\OrderPaymentUtil;
use Components\Product\TransactionManager;
use Models\Product\Product;
use Models\Shop\ShopOrderItem;
use Models\Shop\ShopOrders;
use Models\User\UserAddress;
use Models\User\Users;

/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-14
 * Time: 下午2:12
 */
class TransactionController extends ShopBase
{

    public $store_setting = [];

    public function initialize()
    {
        parent::initialize();
        $this->store_setting = TransactionManager::instance(HOST_KEY)->getStoreSetting($this->customer_id, true);
        $this->view->setVar("store_setting", $this->store_setting);
    }

    public function ordersAction()
    {
        \Phalcon\Tag::setTitle("微信聚合平台-订单管理");
        $this->assets->addJs('static/panel/js/app/panel/shop/transaction.order.js');
        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $name = trim($this->request->get('name', 'striptags', ''));
        $status = intval($this->request->get('status', 'striptags', -1));
        $order = trim($this->request->get('order', 'striptags', ''));

        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\Shop\\ShopOrders', 'so')
            ->leftJoin("\\Models\\User\\UserAddress", 'so.address_id = ua.id', 'ua')
            ->leftJoin("\\Models\\Shop\\Logistics", 'l.key = so.logistics_type', 'l')
            ->andWhere("so.customer_id='{$this->customer->id}'")
            ->columns("so.order_number,so.created, so.total_cash, so.paid_cash, so.is_paid, so.is_cod, so.paid_time, so.paid_type, so.paid_order, so.is_payment_arrived,
                    so.status,so.is_delivered, l.name as logistics_name, so.delivered_time, so.logistics_type, so.logistics_fee, so.logistics_order,
                    ua.name, ua.province, ua.city, ua.town, ua.address, ua.phone, ua.zip_code")
            ->orderBy("so.created DESC");

        if (strlen($name) > 0) {
            $this->view->setVar("name", $name);
        }

        if ($status > -1) {
            $queryBuilder->andWhere("so.status = '{$status}'");
        }
        $this->view->setVar('status', $status);

        if (strlen($order) > 0) {
            $this->view->setVar('order', $order);
            $queryBuilder->andWhere("so.order_number='{$order}'");
        }


        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));
        $this->view->setVar('list', $pagination->getPaginate());
        $orderStatus = TransactionManager::getOrderStatus();
        $this->view->setVar("orderStatus", $orderStatus);
    }

    public function refundAction()
    {
        \Phalcon\Tag::setTitle("微信聚合平台-订单管理");
        $this->assets->addJs('static/panel/js/app/panel/shop/transaction.order.js');
        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $name = trim($this->request->get('name', 'striptags', ''));
        $type = intval($this->request->get('type', 'striptags', -1));
        $order = trim($this->request->get('order', 'striptags', ''));

        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\Shop\\ShopOrderReturn', 'so')
            ->leftJoin("\\Models\\Shop\\ShopOrderItem", 'so.order_item_id=si.id', 'si')
            ->columns('so.id,so.order_number,so.quantity,so.reason,so.reason_detail,so.seller_refused_reason,so.type,
               so.is_seller_accept,so.recharge_money,so.created,so.buyer_deliver_no,
               so.buyer_delivered,so.buyer_deliver_logistics,so.is_buyer_delivered,
               so.is_seller_delivered,so.seller_delivered,so.seller_deliver_no,
               so.seller_deliver_logistics,so.seller_received_goods,so.buyer_received_goods,
               so.is_end,so.is_seller_refunded,si.total_cash')
            ->andWhere("so.customer_id='{$this->customer->id}'")
            ->orderBy("so.created DESC");

        if (strlen($name) > 0) {
            $this->view->setVar("name", $name);
        }

        if ($type > -1) {
            $queryBuilder->andWhere("so.type = '{$type}'");
        }
        $this->view->setVar('type', $type);

        if (strlen($order) > 0) {
            $this->view->setVar('order', $order);
            $queryBuilder->andWhere("so.order_number='{$order}'");
        }


        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));
        $list_arr = $pagination->getPaginate();

        $list = $list_arr->items;
        //echo "<pre>";
        //  var_dump($list->toArray());exit;

        $list = TransactionManager::getRefundStatus($this->customer->id, $list->toArray());
        $this->view->setVar('list', $list);
        $this->view->setVar('list_arr', $list_arr);
        //$changeStatus = TransactionManager::getChangeGoodsStatus();
        //$refundStatus = TransactionManager::getRefundStatus();
        //$this->view->setVar("changeStatus", $changeStatus);
        //$this->view->setVar("refundStatus", $refundStatus);
    }

    public function refundDetailAction()
    {
        \Phalcon\Tag::setTitle("交易管理——退换货详情");
        $id = $this->request->get('id');
        if (!$id || !is_numeric($id)) {
            $this->flash->error("没有指定申请流水号");
        } else {

            $apply = TransactionManager::getItemReturn($this->customer->id, $id);
            if (!$apply) {
                $this->flash->error("没有找到指定的申请");
            } else {
                $user_info = Users::findFirst("id=" . $apply['user_id']);
                $this->view->setVar("apply", $apply);
                $this->view->setVar('user', $user_info);
            }
        }
    }

    public function getOrderProductAction()
    {
        $this->view->disable();
        $order = trim($this->request->getPost('order', 'striptags', ''));
        if (empty($order)) {
            array();
            exit;
        }
        $Product = ShopOrderItem::find("order_number='{$order}'");
        $this->response->setJsonContent(array(
            'code' => 0,
            'result' => $Product->toArray()
        ))->send();
        exit;
    }

    public function orderDetailAction()
    {
        \Phalcon\Tag::setTitle("交易管理——订单详情");
        $this->assets->addCss("static/ace/css/jquery-ui-1.10.3.full.min.css");
        $this->assets->addJs("static/ace/js/jquery.form.js");
        $this->assets->addJs('static/panel/js/tools/region.select.js');
        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.full.min.js');
        $this->assets->addJs('static/panel/js/app/panel/shop/transaction.order.js');
        $order_number = $this->dispatcher->getParam(0);

        $order = ShopOrders::findFirst("order_number='{$order_number}'");
        if (empty($order)) {
            $this->flash->error("没有找到该订单！");
        } else {
            $this->view->setVar('order', $order);
            $address = UserAddress::findFirst("id='{$order->address_id}'");
            if ($address) {
                $this->view->setVar('address', $address);
            }

            if ($order->is_delivered && strlen($order->logistics_type) > 0) {
                $order_logistics = TransactionManager::instance(HOST_KEY)->getDefaultLogisticsByType($order->logistics_type);
                if ($order_number) {
                    $this->view->setVar("order_logistics", $order_logistics);
                }
            }

            $Product = ShopOrderItem::find("order_number='{$order_number}'");
            $this->view->setVar('products', $Product);

            $user = Users::findFirst('id=' . $order->user_id);

            $this->view->setVar('user', $user);

            $logistics = TransactionManager::instance(HOST_KEY)->getCustomerLogistics($this->customer_id, true);
            $this->view->setVar("logistics", $logistics);

            $orderStatus = TransactionManager::getOrderStatus();
            $this->view->setVar("orderStatus", $orderStatus);

        }

    }

    public function setOrderStatusAction()
    {
        $this->view->disable();
        $order_number = $this->request->getPost("order", 'striptags', false);
        $status = $this->request->getPost('status', 'int', false);
        if ($order_number === false || $status === false) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => "没有指定要更新的订单"
            ))->send();
            exit;
        }

        $order = ShopOrders::findFirst("order_number='{$order_number}'");
        if (!$order) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => "没有找到指定的订单"
            ))->send();
            exit;
        }

        if (!$order->update(array('status' => $status))) {
            $messages = [];
            foreach ($order->getMessages() as $message) {
                $messages[] = (string)$message;
            }
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => $messages
            ))->send();
            exit;
        }

        $this->response->setJsonContent(array(
            'code' => 0,
            'message' => "订单状态修改成功"
        ))->send();
        exit;
    }

    public function deliverAction()
    {
        $this->view->disable();
        $order = $this->request->getPost('order_number', 'striptags', false);
        $logistics_type = $this->request->getPost('logistics_type', 'striptags', false);
        $logistics_order = $this->request->getPost('logistics_order', 'striptags', false);
        if (empty($order) || empty($logistics_type) || empty($logistics_order)) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => "您有必填信息没有提供"
            ))->send();
            exit;
        }
        $order = ShopOrders::findFirst("order_number='{$order}' AND customer_id='".CUR_APP_ID."'");
        if (!$order) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => "没有找到指定的订单"
            ))->send();
            exit;
        }

        if ($order->is_delivered > 0) {
            $data = array(
                'logistics_type' => $logistics_type,
                'logistics_order' => $logistics_order,
                'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_CONFIRM_GOODS
            );
        } else {
            $data = array(
                'is_delivered' => 1,
                'logistics_type' => $logistics_type,
                'logistics_order' => $logistics_order,
                'delivered_time' => time(),
                'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_CONFIRM_GOODS
            );
        }

        if (!$order->update($data)) {
            $messages = [];
            foreach ($order->getMessages() as $message) {
                $messages[] = (string)$message;
            }
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => $messages
            ))->send();
            exit;
        }
        $result = OrderPaymentUtil::instance()->deliverNotice(CUR_APP_ID, $order->order_number, 1);
        $this->response->setJsonContent(array(
            'code' => 01,
            'message' => "发货设置成功！",
            'deliver_msg' => $result
        ))->send();
        exit;
    }

    public function autoProductAction()
    {
        $this->view->disable();
        $keyword = trim($this->request->getPost("keyword", 'striptags'));
        if (empty($keyword)) {
            $this->response->setJsonContent(array(
                'code' => 0,
                'result' => array()
            ))->send();
            exit;
        }

        $count = Product::count("customer_id='{$this->customer_id}' AND LOCATE('{$keyword}', name) > 0");
        $Product = Product::find("customer_id='{$this->customer_id}' AND LOCATE('{$keyword}', name) > 0");
        $this->response->setJsonContent(array(
            'code' => 0,
            'count' => $count,
            'result' => $Product->toArray()
        ))->send();
        exit;
    }

    public function saveProductAction()
    {
        $this->view->disable();
        $order_number = $this->request->getPost('order_number', 'striptags');
        $product_id = $this->request->getPost('product_id', 'int');
        $quantity = intval($this->request->getPost('quantity', 'int'));
        $price = floatval($this->request->getPost('price', 'striptags'));
        $total_price = floatval($this->request->getPost("total_price", 'striptags'));

        if (empty($order_number) || empty($product_id) || empty($quantity)) {
            $this->response->setJsonContent(array(
                'code= ' > 1,
                'message' => '您提交的数据不完整，请更正后再试'
            ))->send();
            exit;
        }

        $messages = [];
        $code = 0;

        $this->db->begin();
        try {
            $order = ShopOrders::findFirst("order_number='{$order_number}'");
            if (!$order) {
                $messages = "对不起，您操作的订单不存在！";
                throw new \Phalcon\Exception($messages);
            }

            $product = Product::findFirst("id='{$product_id}' AND customer_id='{$this->customer_id}'");
            if (!$product) {
                $messages = "对不起，找不到您指定的商品！";
                throw new \Phalcon\Exception($messages);
            }

            $item = ShopOrderItem::findFirst("order_number='{$order_number}' AND item_id='{$product_id}'");
            if (!$item) {
                $item = new ShopOrderItem();
                $item->order_number = $order_number;
                $item->item_id = $product_id;
                $item->item_name = $product->name;
                $item->created = time();
            }

            $item->item_price = $price;
            $item->item_sell_price = $product->sell_price;
            $item->quantity = $quantity;
            $item->total_cash = $total_price;
            $item->total_preferential = $product->sell_price * $quantity - $total_price;

            if (!$item->save()) {
                foreach ($item->getMessages() as $message) {
                    $messages[] = (string)$message;
                }
                throw new \Phalcon\Exception(join(',', $messages));
            }

            $total_cash = 0;
            $paid_cash = 0;
            $productItems = ShopOrderItem::find("order_number='{$order_number}'");
            if ($productItems) {
                foreach ($productItems as $item) {
                    $total_cash += $item->item_sell_price;
                    $paid_cash += $item->item_price;
                }
            }

            //折扣和积分
            //会员卡信息
            $user = Users::findFirst('id="' . $order->user_id . '"');
            $rulesArr = $user->getVipcardRules();
            $discount = 0;
            $score = 0;
            if (count($rulesArr) > 0) {
                foreach ($rulesArr as $rule) {
                    $range = explode('-', $rule->consume_rand);
                    if ($total_cash >= floatval($range[0]) && $total_cash <= floatval($range[1])) {
                        $discount = intval($rule->discount);
                        $score = intval($rule->minscore);
                    }
                }
            }

            // 计算折扣
            $discount_cash = $paid_cash;
            // 优惠多少
            $vip_preferential_amount = 0;

            if ($discount > 0 && $discount < 100) {
                $discount_cash = $paid_cash * ($discount / 100);
                $vip_preferential_amount = $paid_cash - $discount_cash;
            }

            if (!$order->update(array(
                'back_coin' => $score,
                'total_cash' => $total_cash,
                'paid_cash' => $discount_cash, // 最终优惠后的价格
                'discount_cash' => $vip_preferential_amount,
            ))
            ) {
                foreach ($order->getMessages() as $message) {
                    $messages[] = (string)$message;
                }
                throw new \Phalcon\Exception(join(',', $messages));
            }
            $this->db->commit();
        } catch (\Phalcon\Exception $e) {
            $code = 1;
            $this->db->rollback();
            $this->di->get('errorLog')->error($e->getFile() . ' ' . $e->getLine() . ":" . $e->getMessage());
        }
        $this->response->setJsonContent(array(
            'code' => $code,
            'message' => $messages
        ))->send();
        exit;
    }

    public function removeProductAction()
    {
        $this->view->disable();
        $order_number = $this->request->getPost('order_number', 'striptags');
        $product_id = $this->request->getPost('product_id', 'int');

        if (empty($order_number) || empty($product_id)) {
            $this->response->setJsonContent(array(
                'code= ' > 1,
                'message' => '您提交的数据不完整，请更正后再试'
            ))->send();
            exit;
        }

        $messages = [];
        $code = 0;

        $this->db->begin();
        try {
            $order = ShopOrders::findFirst("order_number='{$order_number}'");
            if (!$order) {
                $messages = "对不起，您操作的订单不存在！";
                throw new \Phalcon\Exception($messages);
            }

            $product = ShopOrderItem::findFirst("order_number='{$order_number}' AND item_id='{$product_id}'");
            if (!$product) {
                $messages = "对不起，您要删除的商品不在此订单中！";
                throw new \Phalcon\Exception($messages);
            }

            if (!$product->delete()) {
                foreach ($product->getMessages() as $message) {
                    $messages[] = (string)$message;
                }
                throw new \Phalcon\Exception(join(',', $messages));
            }

            $total_cash = 0;
            $paid_cash = 0;
            $productItems = ShopOrderItem::find("order_number='{$order_number}'");
            if ($productItems) {
                foreach ($productItems as $item) {
                    $total_cash += $item->item_sell_price;
                    $paid_cash += $item->item_price;
                }
            }

            //折扣和积分
            //会员卡信息
            $user = Users::findFirst('id="' . $order->user_id . '"');
            $rulesArr = $user->getVipcardRules();
            $discount = 0;
            $score = 0;
            if (count($rulesArr) > 0) {
                foreach ($rulesArr as $rule) {
                    $range = explode('-', $rule->consume_rand);
                    if ($total_cash >= floatval($range[0]) && $total_cash <= floatval($range[1])) {
                        $discount = intval($rule->discount);
                        $score = intval($rule->minscore);
                    }
                }
            }

            // 计算折扣
            $discount_cash = $paid_cash;
            // 优惠多少
            $vip_preferential_amount = 0;

            if ($discount > 0 && $discount < 100) {
                $discount_cash = $paid_cash * ($discount / 100);
                $vip_preferential_amount = $paid_cash - $discount_cash;
            }

            if (!$order->update(array(
                'back_coin' => $score,
                'total_cash' => $total_cash,
                'paid_cash' => $discount_cash, // 最终优惠后的价格
                'discount_cash' => $vip_preferential_amount,
            ))
            ) {
                foreach ($order->getMessages() as $message) {
                    $messages[] = (string)$message;
                }
                throw new \Phalcon\Exception(join(',', $messages));
            }
            $this->db->commit();
        } catch (\Phalcon\Exception $e) {
            $code = 1;
            $this->db->rollback();
            $this->di->get('errorLogger')->error($e->getFile() . ' ' . $e->getLine() . ":" . $e->getMessage());
        }
        $this->response->setJsonContent(array(
            'code' => $code,
            'message' => $messages
        ))->send();
        exit;
    }

    public function addressAction()
    {
        $this->view->disable();
        $address_id = $this->request->getPost("address_id", 'int');
        $order_number = $this->request->getPost('order_number', 'striptags');
        $name = $this->request->getPost('name', 'striptags');
        $province = $this->request->getPost('province', 'striptags');
        $city = $this->request->getPost("citye", 'striptags');
        $town = $this->request->getPOst('town', 'striptags');
        $address = $this->request->getPost('address', 'striptags');
        $phone = $this->request->getPost('phone', 'striptags');
        $zipcode = $this->request->getPost('zip_code', 'int');

        $order = ShopOrders::findFirst("order_number='{$order_number}' AND customer_id='{$this->customer_id}'");
        if (!$order) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => "订单编号有误或者您没有找到权限编辑此订单"
            ))->send();
            exit;
        }

        if ($address_id > 0) {
            $addressObj = UserAddress::findFirst("id='{$address_id}'");
        } else {
            $addressObj = new UserAddress();
        }
        $addressObj->name = $name;
        $addressObj->province = $province;
        $addressObj->city = $city;
        $addressObj->town = $town;
        $addressObj->address = $address;
        $addressObj->phone = $phone;
        $addressObj->zip_code = $zipcode;

        $this->db->begin();
        $code = 0;
        $messages = [];

        try {
            if (!$addressObj->save()) {
                foreach ($addressObj->getMessages() as $message) {
                    $messages[] = (string)$message;
                }
                throw new \Phalcon\Exception(join(',', $messages));
            }

            if (!$order->update(array(
                'address_id' => $addressObj->id
            ))
            ) {
                foreach ($order->getMessages() as $message) {
                    $messages[] = (string)$message;
                }
                throw new \Phalcon\Exception(join(',', $messages));
            }
            $this->db->commit();
        } catch (\Phalcon\Exception $e) {
            $code = 1;
            $messages = $e->getMessage();
            $this->db->rollback();
            $this->di->get('errorLogger')->error($e->getFile() . ' ' . $e->getLine() . ":" . $e->getMessage());
        }

        $this->response->setJsonContent(array(
            'code' => $code,
            'message' => $messages
        ))->send();
        exit;
    }
} 