<?php
namespace Multiple\Admin\Controllers;

use Models\System\SystemAdmin;
use Phalcon\Mvc\Controller;
use Phalcon\Tag as Tag;

class SessionController extends Controller
{
    protected $host_key;

    public function initialize()
    {
        $this->view->setLayout('session');
        Tag::setTitle('管理员登陆');

    }

    public function indexAction()
    {
        if (!$this->request->isPost()) {
            Tag::setDefault('nick_name', '');
            Tag::setDefault('password', '');
        }
    }

    public function registerAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            $name = $request->getPost('name', array('string', 'striptags'));
            $username = $request->getPost('username', 'striptags');
            $email = $request->getPost('email', 'email');
            $password = $request->getPost('password', 'string');
            $repeatPassword = $this->request->getPost('repeatPassword', 'string');

            if ($password != $repeatPassword) {
                $this->flash->error('两次密码不一样');
                return false;
            }

            $user = new SystemAdmin();
            $user->host_key = HOST_KEY;
            $user->username = $username;
            $user->password = sha1($password);
            $user->name = $name;
            $user->email = $email;
            $user->created_at = new \Phalcon\Db\RawValue('now()');
            $user->active = 'Y';
            if ($user->save() == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
            } else {
                Tag::setDefault('email', '');
                Tag::setDefault('password', '');
                $this->flash->success('Thanks for sign-up, please log-in to start generating invoices');
                return $this->forward('session/index');
            }
        }
    }

    /**
     * Register authenticated user into session data
     *
     * @param Admins $user
     */
    private function _registerSession($user)
    {
        $this->session->set('admin_auth', true);
        $this->session->set('admin_info', $user);
    }

    /**
     * This actions receive the input from the login form
     *
     */
    public function startAction()
    {
        if ($this->request->isPost()) {
            $nick_name = $this->request->getPost('nick_name', 'email');

            $password = $this->request->getPost('password', 'string');
            $password = sha1($password);
            $user = SystemAdmin::findFirst("nick_name='$nick_name' AND password='$password' AND active='1' AND host_key='".HOST_KEY."'");
            if ($user != false) {
                $this->_registerSession($user);
                $this->flash->success('Welcome ' . $user->name);
                $referer = $this->session->get('admin_referer');
                if ($referer) {
                    $this->response->redirect($referer);
                } else {
                    $this->response->redirect('admin');
                }
            }

            $this->flash->error('用户名或密码错误');
        }

        $this->forward('session/index');
    }

    /**
     * Finishes the active session redirecting to the index
     *
     * @return unknown
     */
    public function endAction()
    {
        $this->session->remove('admin_auth');
        $this->session->remove('admin_info');
        $this->flash->success('Goodbye!');
        $this->response->redirect('admin/session/index');
    }

    protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $partsNum = count($uriParts);
        switch ($partsNum) {
            case 2:
                return $this->dispatcher->forward(array(
                    'controller' => $uriParts[0],
                    'action' => $uriParts[1]
                ));
                break;
            case 3:
                return $this->dispatcher->forward(array(
                    'namespace' => $uriParts[0],
                    'controller' => $uriParts[1],
                    'action' => $uriParts[2]
                ));
                break;
        }
    }
}
