<?php

namespace Multiple\Admin\Controllers;

class IndexController extends ControllerBase
{

	public function indexAction()
	{
        $this->view->setLayout("main");
		\Phalcon\Tag::setTitle('平台管理员首页');
	}

    public function dashboardAction() {
        $this->flash->notice("coming soon");
    }
}