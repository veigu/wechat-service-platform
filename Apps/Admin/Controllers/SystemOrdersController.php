<?php
namespace Multiple\Admin\Controllers;
use Components\ModuleManager\ModuleManager;
use Models\Modules\Resources;
use	Phalcon\Mvc\Model\Criteria;
use Models\System\SystemOrders;
use Models\Customer\Customers;
use Models\Modules\ResourceForSystemOrders;
use Phalcon\Logger;

class SystemOrdersController extends ControllerBase
{
    const ORDER_NUMBER_PREFIX_AUTO = 'SO_AUTO_';
    const ORDER_NUMBER_PREFIX_HANDLE = 'SO_HANDLE_';
	public function indexAction()
	{

	}

    //直销
    public function directAction()
    {

    }

    //直销订单异步加载
    public function directlistAction()
    {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        }
        else {
            $query = Criteria::fromInput($this->di, "\\Models\\System\\SystemOrders", $_POST);
            $query->leftJoin("\\Models\\Customer\\Customers", 'customer.id = customer_id', 'customer');
            $time_start = $this->request->getPost('created_min', 'striptags', false);
            if(!empty($time_start)) {
                $time_start = strtotime($time_start);
            }
            $time_end = $this->request->getPost('time_end', 'striptags', false);
            if(!empty($time_end)) {
                $time_end = strtotime($time_end);
            }

            if($time_start > 0) {
                $query->andWhere("ordered >= '{$time_start}'");
            }
            if($time_end > 0) {
                if($time_start > 0 && $time_end > $time_start || empty($time_start)) {
                    $query->andWhere("ordered <= '{$time_end}'");
                }
            }

            $account = $this->request->getPost('account', 'striptags', false);
            if($account) {
                $query->andWhere("");
            }

            $query->andWhere("customer.host_key='" . HOST_KEY . "'");

            $parameters = $query->getParams();
            $count = SystemOrders::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', "int", 20);

            $columns = <<<EOF
            order_number, resource_count, total_money,
            customer_id, status, paid_time,
            IF(ordered > 0, FROM_UNIXTIME(ordered, '%Y-%m-%d'), '') AS ordered,
            customer.account, customer.active, customer.name
EOF;

            $list = $query->columns($columns)
                ->limit($limit, $start)->execute()->toArray();
            $this->response->setJsonContent(array(
                'rows' => $list,
                'results' => $count
            ))->send();
        }
    }

    public function listAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        }
        else {
            $query = Criteria::fromInput($this->di, "\\Models\\System\\SystemOrders", $_POST);
            $query->leftJoin("\\Models\\Customer\\Customers", 'customer.id = customer_id', 'customer');
            $time_start = $this->request->getPost('created_min', 'striptags', false);
            if(!empty($time_start)) {
                $time_start = strtotime($time_start);
            }
            $time_end = $this->request->getPost('time_end', 'striptags', false);
            if(!empty($time_end)) {
                $time_end = strtotime($time_end);
            }
            
            if($time_start > 0) {
                $query->andWhere("ordered >= '{$time_start}'");
            }
            if($time_end > 0) {
                if($time_start > 0 && $time_end > $time_start || empty($time_start)) {
                    $query->andWhere("ordered <= '{$time_end}'");
                }
            }
            
            $account = $this->request->getPost('account', 'striptags', false);
            if($account) {
                $query->andWhere("");
            }
            $query->andWhere("customer.host_key='" . HOST_KEY . "'");
            
            $parameters = $query->getParams();
            $count = SystemOrders::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', "int", 20);

            $columns = <<<EOF
            order_number, resource_count, total_money,
            customer_id, status, paid_time,
            IF(ordered > 0, FROM_UNIXTIME(ordered, '%Y-%m-%d'), '') AS ordered,
            customer.account, customer.active, customer.name
EOF;

            $list = $query->columns($columns)
            ->limit($limit, $start)->execute()->toArray();
            $this->response->setJsonContent(array(
                'rows' => $list,
                'results' => $count
            ))->send();
        }
    }

	public function detailAction()
	{
	    $orderNumber = $this->request->get('order_number', 'striptags');
        $order = SystemOrders::findFirst("order_number='{$orderNumber}'");
        if(!$order) {
            $this->flash->error("没有找到该订单");
        }
        else {
            $customer = Customers::findFirst("id='{$order->customer_id}'");

            $query = new \Phalcon\Mvc\Model\Query("SELECT s.order_number,s.resource_id,s.trade_price,s.years,s.months,s.days,s.discount_price ,r.year_price_min,r.year_price_max,r.month_price_min,r.month_price_max FROM \Models\Modules\ResourceForSystemOrders AS s LEFT JOIN \Models\Modules\Resources AS r  ON  s.resource_id=r.id where s.order_number='{$orderNumber}'", $this->getDI());
            $orderResources = $query->execute();

            $resources = Resources::find();
            $this->view->setVar('order', $order);
            $this->view->setVar('customer', $customer);
            $this->view->setVar('resources', $resources);
            $this->view->setVar('orderResources', $orderResources);
        }
	}

	public function editAction()
	{
        $orderNumber = $this->request->get('order_number', 'striptags');
        $order = SystemOrders::findFirst("order_number='{$orderNumber}'");
        $resources = ModuleManager::instance(HOST_KEY)->getSystemModules();
        if($order) {
            $customer = Customers::findFirst("id='{$order->customer_id}'");
            $orderResources = ResourceForSystemOrders::find("order_number='{$orderNumber}'");
            $this->view->setVar('order', $order);
            $this->view->setVar('customer', $customer);
            $this->view->setVar('resources', $resources);
            $this->view->setVar('orderResources', $orderResources);
        }
        else {
            $this->view->setVar('order_number', self::ORDER_NUMBER_PREFIX_HANDLE . date("YmdHis") ."_" . strtoupper(substr(sha1(uniqid(join('', microtime()))), 0, 6)));
            $this->view->setVar('resources', $resources);
        }
	}

    public function aaAction()
    {
        $this->di->get('logger')->log("respond result:" . "save", Logger::INFO);
    }

	public function saveAction()
	{
        $this->view->disable();
        $request = $this->request;
        $order_number = $request->getPost('order_number', 'striptags');
        if(!empty($order_number)) {
            $order = SystemOrders::findFirst("order_number='{$order_number}'");
            if ($order == false) {
                $order = new SystemOrders();
                $order->host_key = HOST_KEY;
                $order->order_number = $order_number;
                $order->ordered = time();
                $customer_id = $request->getPost('customer_id', 'int', false);
                if(!$customer_id) {
                    $this->response->setJsonContent(array(
                        'code' => 1,
                        'message' => '客户编号必须填写'
                    ))->send();
                    exit(1);
                }
                else {
                    $customerCount = Customers::count("id='{$customer_id}'");
                    if($customerCount == 0) {
                        $this->response->setJsonContent(array(
                            'code' => 1,
                            'message' => '不存在编号为[' . $customer_id . ']的客户'
                        ))->send();
                        exit(1);
                    }
                }
            }
        }
        else {
            $order = new SystemOrders();
            $order->ordered = time();
            $order_number = self::ORDER_NUMBER_PREFIX_HANDLE . date("YmdHis") . '_' .strtoupper(substr(sha1(uniqid(join('', microtime()))), 0, 6));
            $order->order_number = $order_number;
            $customer_id = $request->getPost('customer_id', 'int', false);
            if(!$customer_id) {
                $this->response->setJsonContent(array(
                	'code' => 1,
                    'message' => '客户编号必须填写'
                ))->send();
                exit(1);
            }
        }

        $ordered = $request->getPost('ordered', 'striptags', false);
        if($ordered && strlen($ordered) > 0) {
            $ordered = strtotime($ordered);
            $order->ordered = $ordered;
        }

        $paid_time = $request->getPost('paid_time', 'striptags', false);
        if($paid_time && strlen($paid_time) > 0) {
            $paid_time = strtotime($paid_time);
            $order->paid_time = $paid_time;
        }

        $customer_id = $request->getPost('customer_id', 'int', false);
        if($customer_id) {
            $order->customer_id = $customer_id;
        }
        $status = $request->getPost('status', 'int', false);
        if($status !== false || is_numeric($status)) {
            $order->status = $status;
        }

        $resources = $request->getPost('resources');
//        $this->moneyValid($resources);

        $this->db->begin();
        $messages = [];
        $code = 0;
        try {
            $OrderResources = ResourceForSystemOrders::find("order_number = '{$order_number}'");
            $total_money = 0;
            if(is_array($resources)) {
                $order->resource_count = count($resources);
                $resourIds = [];
                foreach($resources as $r) {
                    $resourceId = $r['source_id'];
//                    $year_price_max = $r['year_price_max'];
//                    $discount_price = $r['discount_price'];
//                    $this->di->get('logger')->log($discount_price, Logger::INFO);
                    $years = $r['years'];
                    $months = $r['months'];
                    $trade_price = $r['trade_price'];
//                    $discount_price = $r['discount_price'];
                    $resource = ResourceForSystemOrders::findFirst("order_number='{$order_number}' AND resource_id='{$resourceId}'");
                    if(!$resource) {
                        $resource = new ResourceForSystemOrders();
                        $resource->resource_id = $resourceId;
                    }
                    $resource->order_number = $order_number;
                    $resource->trade_price = $trade_price;
//                    $resource->discount_price = $discount_price;
                    $resource->years = $years;
                    $resource->months = $months;
                    $resource->days = '0';
                    if(!$resource->save()) {
                        foreach ($order->getMessages() as $message) {
                            $messages[] = (string) $message;
                        }
                        throw new \Phalcon\Exception(join('。', $messages));
                    }
                    $resourIds[] = $resourceId;
                    $total_money += $trade_price * $years;
                }
            }
            else {
                $order->resource_count = 0;
            }
            
            foreach ($OrderResources as $r) {
                if(!in_array($r->resource_id, $resourIds)) {
                    if(!$r->delete()) {
                        foreach ($order->getMessages() as $message) {
                            $messages[] = (string) $message;
                        }
                        throw new \Phalcon\Exception(join('。', $messages));
                    }
                }
            }

            $total_money_posted = $request->getPost('total_money', 'int', 0.00);
            if($total_money_posted > 0) {
                $order->total_money = $total_money_posted;
            }
            else {
                $order->total_money = $total_money;
            }

            if (!$order->save()) {
                foreach ($order->getMessages() as $message) {
                    $messages[] = (string) $message;
                }
                throw new \Phalcon\Exception(join('。', $messages));
            }
            $this->db->commit();
        }
        catch(\Phalcon\Exception $e) {
            $this->db->rollback();
            $code = 1;
            $messages[] = $e->getMessage();
        }

        $this->response->setJsonContent(array(
            'code' => $code,
            'message' => $messages
        ))->send();
	}

    public function moneyValid($resources)
    {
        foreach($resources as $resource) {
            $month_price_min = $resource['month_price_min'];

            $month_price_max = $resource['month_price_max'];

            $year_price_min = $resource['year_price_min'];

            $year_price_max = $resource['year_price_max'];

            $years = $resource['years'];

            $months = $resource['months'];

            $discount_price = $resource['discount_price'];

            $maxprice = $month_price_max*$months+$year_price_max*$years;
            $minprice = $month_price_min*$months+$year_price_min*$years;

            $this->di->get('logger')->log("respond result:" . $maxprice, Logger::INFO);
            $this->di->get('logger')->log("respond result:" . $minprice, Logger::INFO);

            if($discount_price > $maxprice || $discount_price < $minprice)
            {
                $source_id = $resource['source_id'];
                $aresource = Resources::findFirst("id='{$source_id}'");
                $this->response->setJsonContent(array(
                    'code' => 1,
                    'message' => $aresource->name."(".$years."年".$months."月)的 标价范围:".$minprice."-".$maxprice
                ))->send();
                exit(1);
            }
        }
    }

	public function removeAction()
	{
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $ids = $this->request->getPost('ids');
        if(!is_array($ids)) {
            $ids = array($this->filter->sanitize($ids, 'int'));
        }
        if(empty($ids)) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $this->db->begin();
        $result = true;
        try {
            foreach($ids as $id) {
                $order = SystemOrders::findFirst($id);
                if (!$order) {
                    $result = false;
                    throw new \Exception("企业{$id}没有找到！");
                }

                if (!$order->delete()) {
                    $messages = [];
                    foreach ($order->getMessages() as $message) {
                        $messages[] = (string) $message;
                    }
                    $result = false;
                    throw new \Exception($messages);
                }
            }
            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
        }

		$this->response->setJsonContent(array(
            'success' => $result
        ))->send();
	}
}
