<?php
namespace Multiple\Admin\Controllers;

use Models\System\SystemHelpArticle;
use Models\System\SystemHelpCat;

class HelpController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }

    public function listAction()
    {
        \Phalcon\Tag::setTitle('帮助文章列表');

        $cats = SystemHelpCat::find("host_key='" . HOST_KEY . "'")->toArray();
        $this->view->setVar('cats', $cats);
    }

    public function catAction()
    {
        \Phalcon\Tag::setTitle('帮助类别');
        $cats = SystemHelpCat::find("host_key='" . HOST_KEY . "'")->toArray();
        $this->view->setVar('cats', $cats);
    }


    public function addAction()
    {
        \Phalcon\Tag::setTitle('添加帮助');
        if ($this->request->isPost()) {
            $admin = $this->admin->toArray();
            $data['cid'] = $this->request->getPost('cid', 'int');
            $data['published'] = $this->request->getPost('published', 'int', 1);
            $data['title'] = $this->request->getPost('title', 'string');
            $data['content'] = $this->request->getPost('content');
            $data['author_id'] = $admin['id'];
            $data['author_name'] = $admin['nick_name'];
            $data['views'] = 1;
            $data['created'] = time();
            $data['modified'] = time();
            $data['cover'] = '';
            $data['id'] = null;
            $data['host_key'] = HOST_KEY;
            $ha = new SystemHelpArticle();
            if (!$ha->create($data)) {
                foreach ($ha->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
            } else {
                $this->flash->success('添加成功 <a href="/admin/help/update/' . $ha->id . '">查看与修改 >></a>');
            }
        }

        $cats = SystemHelpCat::find("host_key='" . HOST_KEY . "'")->toArray();
        $this->view->setVar('cats', $cats);

    }

    public function updateAction()
    {
        \Phalcon\Tag::setTitle('修改帮助');
        $cats = SystemHelpCat::find("host_key='" . HOST_KEY . "'")->toArray();
        $this->view->setVar('cats', $cats);
        $id = $this->dispatcher->getParam(0);
        $article = SystemHelpArticle::findFirst(' id = ' . $id);
        if (!$article) {
            // todo error
        } else {
            if ($this->request->isPost()) {
                $data['cid'] = $this->request->getPost('cid', 'int');
                $data['published'] = $this->request->getPost('published', 'int', 1);
                $data['title'] = $this->request->getPost('title', 'string');
                $data['content'] = $this->request->getPost('content');
                $data['cover'] = '';
                $data['modified'] = time();

                if (!$article->update($data)) {
                    foreach ($article->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                } else {
                    $this->flash->success('修改成功');
                }
            }

            $this->view->setVar('article', $article->toArray());
        }

    }

    /**
     * api
     */
    public function getListAction()
    {
        $this->view->disable();
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        } else {
            $query = new SystemHelpArticle();
            $query = $query->query();

            $key = $this->request->get('keywords');
            $cid = $this->request->get('cid');

            $query->where(' title like "%' . $key . '%" ');

            if ($cid) {
                $query->andWhere("cid = '{$cid}'");
            }

            $time_start = $this->request->getPost('created_min', 'striptags', false);

            if (!empty($time_start)) {
                $time_start = strtotime($time_start);
                if ($time_start > 0) {
                    $query->andWhere("created > '{$time_start}'");
                }
            }

            $time_end = $this->request->getPost('created_max', 'striptags', false);
            if (!empty($time_end)) {
                $time_end = strtotime($time_end);
                if ($time_end > 0) {
                    if ($time_start > 0 && $time_end > $time_start || empty($time_start)) {
                        $query->andWhere("created <= '{$time_end}'");
                    }
                }
            }
            $query->andWhere("host_key='" . HOST_KEY . "'");

            $parameters = $query->getParams();
            $count = SystemHelpArticle::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', "int", 10);

            $list = $query->columns("id, title, cid, author_name, views,FROM_UNIXTIME(created, '%Y-%m-%d %H:%i') AS created, FROM_UNIXTIME(modified, '%Y-%m-%d %H:%i') AS created")
                ->limit($limit, $start)->execute()->toArray();
            // cat name
            $cats = SystemHelpCat::find("host_key='" . HOST_KEY . "'")->toArray();
            $cats_name = array_column($cats, 'name', 'id');
            $cats_name[0] = '未选择';

            foreach ($list as &$v) {
                $v['category_name'] = $cats_name[$v['cid']];
            }

            $this->response->setJsonContent(array(
                'rows' => $list,
                'results' => $count
            ))->send();
        }
    }

    public function delAction()
    {
        $this->view->disable();
        $result = array(
            'code' => 0,
            'msg' => '',
            'result' => ''
        );

        $data = $this->request->getPost('ids');
        if ($data) {
            $hc = SystemHelpArticle::find("host_key='" . HOST_KEY . "' AND id in (" . implode(',', $data) . ')');

            $res = $hc->delete();

            if (!$res) {
                $result['code'] = 1;
                $result['msg'] = $hc->getMessages();
            } else {
                $result['result'] = 1;
            }

        }

        $this->response->setHeader('content-type', 'text/json;charset=utf-8');
        $this->response->setJsonContent($result);
        $this->response->send();
    }

    public function getCatAction()
    {
        $this->view->disable();

        $result = array(
            'code' => 0,
            'msg' => '',
            'result' => ''
        );

        $cats = SystemHelpCat::find("host_key='" . HOST_KEY . "'");
        $cats = $cats ? $cats->toArray() : [];
        $result['rows'] = $cats;
        $result['results'] = SystemHelpCat::count("host_key='" . HOST_KEY . "'");

        $this->response->setHeader('content-type', 'text/json;charset=utf-8');
        $this->response->setJsonContent($result);
        $this->response->send();
    }

    public function saveCatAction()
    {
        $this->view->disable();

        $result = array(
            'code' => 0,
            'msg' => '',
            'result' => ''
        );

        $data = $this->request->getPost('data');
        if ($data) {
            $id = isset($data['id']) ? $data['id'] : 0;
            unset($data['id']);
            $data['modified'] = time();

            if ($id) {
                $hc = SystemHelpCat::findFirst('id=' . $id);
                $res = $hc->save($data);
            } else {
                $hc = new SystemHelpCat();
                $data['pid'] = 0;
                $data['created'] = time();
                $data['modified'] = time();
                $data['host_key'] = HOST_KEY;

                $res = $hc->create($data);
            }

            if (!$res) {
                $result['code'] = 1;
                $result['msg'] = $hc->getMessages();
            } else {
                $result['result'] = 1;
            }

        }

        $this->response->setHeader('content-type', 'text/json;charset=utf-8');
        $this->response->setJsonContent($result);
        $this->response->send();
    }

    public function delCatAction()
    {
        $this->view->disable();
        $result = array(
            'code' => 0,
            'msg' => '',
            'result' => ''
        );

        $data = $this->request->getPost('data');
        if ($data) {
            $hc = SystemHelpCat::find("host_key='" . HOST_KEY . "' AND id in (" . implode(',', $data) . ')');

            $res = $hc->delete();

            if (!$res) {
                $result['code'] = 1;
                $result['msg'] = $hc->getMessages();
            } else {
                $result['result'] = 1;
            }
        }

        $this->response->setHeader('content-type', 'text/json;charset=utf-8');
        $this->response->setJsonContent($result);
        $this->response->send();
    }
}
