<?php

namespace Multiple\Admin\Controllers;

use Components\Product\CategoryManager;
use Models\Product\Categories;
use Models\Product\CategoryDescription;
use Phalcon\Tag;
class CategoryController extends ControllerBase
{
    public function indexAction() {
        \Phalcon\Tag::setTitle('微信聚合平台-商品分类设置');
        $this->assets->addCss('static/bootstrap/css/bootstrap.css');
        $this->assets->addCss('static/bootstrap/css/jquery.tree.css');
        $this->assets->addCss('static/panel/modules/wechat/css/wechat.message.css');
        $this->assets->addJs('static/bootstrap/js/bootstrap.js');
        $this->assets->addJs('static/panel/js/app/admin/product.category.js');

        $categories = CategoryManager::instance()->getTreeSystemCategories(HOST_KEY);

        $this->view->setVar('categories', $categories);
    }

    public function addAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '只接受AJAX请求!'
            ));
            $this->response->send();
        }
        $id = $this->request->getPost('id', 'int');
        $name = $this->request->getPost('text', 'string');
        $pid = $this->request->getPost('pid', 'int');
        $logo = $this->request->getPost('image', 'string');
        $sort = $this->request->getPost('sort', 'int');
        $status = $this->request->getPost('status', 'int');
        $desc = $this->request->getPost('desc', 'string');
        $metaKeywords = $this->request->getPost('meta_keyword', 'string');
        $metaDesc = $this->request->getPost('meta_desc', 'string');

        $data = array(
            'code' => 0,
            'result' => '',
            'message' => ''
        );

        if(empty($name)) {
            $data['code'] = 1;
            $data['message'][] = "分类名称不能为空！";
        }
        if(empty($metaKeywords)) {
            $data['code'] = 1;
            $data['message'][] = "META关键词不能为空！";
        }
        if(empty($metaDesc)) {
            $data['code'] = 1;
            $data['message'][] = "META描述不能为空！";
        }
        if(empty($sort)) {
            $sort = 0;
        }
        if(empty($pid)) {
            $pid = 0;
        }


        if($data['code'] > 0) {
            $this->response->setJsonContent($data);
            $this->response->send();
            exit(1);
        }
        else {
            $this->db->begin();
            try {
                if($id) {
                    $ug = Categories::findFirst("id='{$id}'");
                    $ug->modified = time();
                }
                else {
                    $ug = new Categories();
                    $ug->created = time();
                    $ug->modified = time();
                    $ug->host_key = HOST_KEY;
                }
                $ug->pid = $pid;
                $ug->top = $pid;
                $ug->image = $logo;
                $ug->sort = $sort;
                $ug->status = 1;

                if(!$ug->save()) {
                    $data['code'] = 1;
                    foreach($ug->getMessages() as $message) {
                        $data['message'][] = $message;
                    }
                    throw new \PharException(implode(' ', $data['message']));
                }
                else {
                    $catDesc = CategoryDescription::findFirst("category_id='{$ug->id}'");
                    if(!$catDesc) {
                        $catDesc = new CategoryDescription();
                        $catDesc->category_id = $ug->id;
                    }
                    $catDesc->name = $name;
                    $catDesc->description = $desc;
                    $catDesc->meta_keyword = $metaKeywords;
                    $catDesc->meta_description = $metaDesc;
                    if(!$catDesc->save()) {
                        $data['code'] = 1;
                        foreach($catDesc->getMessages() as $message) {
                            $data['message'][] = $message;
                        }
                        throw new \PharException(implode(' ', $data['message']));
                    }
                    CategoryManager::instance()->getTreeSystemCategories(HOST_KEY, true);
                    $data['result'] = $ug->id;
                }
                $this->db->commit();
            }
            catch( \PharException $e) {
                $this->db->rollback();
                $data['message'][] = $e->getMessage();
            }

            $this->response->setJsonContent($data);
            $this->response->send();
        }
    }

    public function removeAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '只接受AJAX请求!'
            ));
            $this->response->send();
            exit(1);
        }
        $id = $this->request->getPost('cid', 'int');
        $data = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if(empty($id)) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '没有找到您要删除的数据!'
            ));
            $this->response->send();
            exit(1);
        }

        $category = Categories::findFirst($id);
        if($category) {
            if($category->pid == 0) {
                $subCategories = Categories::count("pid='{$id}'");
                if($subCategories > 0) {
                    $this->response->setJsonContent(array(
                        'code' => 1,
                        'message' => '该分类下面还有子分类没有删除，请先删除子分类!'
                    ));
                    $this->response->send();
                    exit(1);
                }
            }

            $result = array(
                'code' => 0,
                'message' => ''
            );
            //start a transaction
            $this->db->begin();
            if($category->delete()) {
                $messages = array("删除成功！");
                $catDesc = CategoryDescription::findFirst("category_id='{$id}'");
                if(!$catDesc->delete()) {
                    $this->db->rollback();
                    foreach($category->getMessages() as $message) {
                        $messages[] = $message;
                    }
                    $result = array(
                        'code' => 1,
                        'message' => $messages
                    );
                }
            }
            else {
                $messages = array();
                foreach($category->getMessages() as $message) {
                    $messages[] = $message;
                }
                $result = array(
                    'code' => 1,
                    'message' => $messages
                );
                $this->db->rollback();
            }
            $this->db->commit();
            CategoryManager::instance()->getTreeSystemCategories(HOST_KEY, true);
            $this->response->setJsonContent($result);
            $this->response->send();
            exit(1);
        }
        else {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '没有找到您要删除的数据!'
            ));
            $this->response->send();
            exit(1);
        }
    }

}