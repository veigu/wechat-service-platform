<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-24
 * Time: 上午11:12
 */

namespace Multiple\Admin\Controllers;


use Components\IndustryManager;

class OperateConfigController extends ControllerBase {
    public function agentGradeAction() {

    }
    public function functionsAction() {

    }

    public function zoneAction() {

    }

    public function industryAction() {
        \Phalcon\Tag::setTitle('微信聚合平台-商品分类设置');
        $this->assets->addCss('static/bootstrap/css/bootstrap.css');
        $this->assets->addCss('static/bootstrap/css/jquery.tree.css');
        $this->assets->addJs('static/bootstrap/js/bootstrap.js');
        $this->assets->addJs('static/panel/js/app/admin/company.industry.js');

        $categories = IndustryManager::instance()->getTreeData(HOST_KEY);

        $this->view->setVar('categories', $categories);
    }
} 