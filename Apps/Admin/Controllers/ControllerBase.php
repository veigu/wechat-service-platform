<?php

namespace Multiple\Admin\Controllers;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    /**
     * @var SystemAdmin
     */
    public $admin;

    public function initialize()
    {

        \Phalcon\Tag::setTitle("管理平台");
        $this->view->setLayout("blank");
        $controler = $this->dispatcher->getControllerName();
        if (!$this->session->get('admin_auth') && $controler != "session") {
            $this->session->set('admin_referer', 'admin/' . $controler . '/' . $this->dispatcher->getActionName());
            $this->response->redirect('admin/session/index');
        }
        $adminInfo = $this->session->get('admin_info');
        $this->view->setVar('nick_name', $adminInfo->nick_name);
        $this->admin = $adminInfo;
    }

    public function indexAction()
    {

    }

    protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $partsNum = count($uriParts);
        switch ($partsNum) {
            case 2:
                return $this->dispatcher->forward(
                    array(
                        'controller' => $uriParts[0],
                        'action' => $uriParts[1]
                    )
                );
                break;
            case 3:
                return $this->dispatcher->forward(
                    array(
                        'namespace' => $uriParts[0],
                        'controller' => $uriParts[1],
                        'action' => $uriParts[2]
                    )
                );
                break;
        }
    }

}