<?php

namespace Multiple\Admin\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Models\Modules\Resources;


class ResourceController extends ControllerBase
{
    /**
     * 数据异步加载
     */
    public function searchAction()
    {

    }

    /**
     * 异步加载数据库数据
     */
    public function listAction()
    {

        $this->view->disable();
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        } else {
            $query = Criteria::fromInput($this->di, "\\Models\\Modules\\Resources", $_POST);
            $parameters = $query->getParams();
            $count = Resources::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', "int", 20);

            if (!$query instanceof \Phalcon\Mvc\Model\Criteria) {
                $query = Resources::query($this->di);
            }

            $list = $query->columns("id, name, year_price, month_price, type,free_period, description, belong, image, settings, FROM_UNIXTIME(created, '%Y-%m-%m') AS created,year_price_min,year_price_max,month_price_min,month_price_max")
                ->limit($limit, $start)->execute()->toArray();

            $result = array();
            foreach ($list as $theResource) {
                $year_price_min = $theResource['year_price_min'];
                $year_price_max = $theResource['year_price_max'];
                $month_price_min = $theResource['month_price_min'];
                $month_price_max = $theResource['month_price_max'];

                $theResource['month_price_range'] = "{$month_price_min}-{$month_price_max}";
                $theResource['year_price_range'] = "{$year_price_min}-{$year_price_max}";

                $result[] = $theResource;
            }

            $this->response->setJsonContent(array(
                'rows' => $result,
                'results' => $count
            ))->send();
        }
    }

    /**
     * 删除数据
     */
    public function removeAction()
    {
        $this->view->disable();
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $ids = $this->request->getPost('ids');
        if (!is_array($ids)) {
            $ids = array($this->filter->sanitize($ids, 'int'));
        }
        if (empty($ids)) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $this->db->begin();
        $result = true;
        try {
            foreach ($ids as $id) {
                $resources = Resources::findFirst($id);
                if (!$resources) {
                    $result = false;
                    throw new \Exception("功能{$id}没有找到！");
                }

                if (!$resources->delete()) {
                    $messages = [];
                    foreach ($resources->getMessages() as $message) {
                        $messages[] = (string)$message;
                    }
                    $result = false;
                    throw new \Exception($messages);
                }
            }
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
        }

        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    /**
     * 添加记录
     */
    public function createAction()
    {
        $this->view->disable();
        $result = $this->saveResource();
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }


    public function editAction()
    {
        $this->view->disable();
        $id = $this->request->getPost('id', 'int');
        $result = $this->saveResource($id);
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    /**
     * 修改数据库记录
     * @param null $id
     * @return array|bool|string
     */
    private function saveResource($id = null)
    {
        if (is_numeric($id) && $id > 0) {
            $resources = Resources::findFirst($id);
            if ($resources == false) {
                return "功能未找到";
            }
        } else {
            $resources = new Resources();
            $resources->created = time();
        }

        $resources->name = $this->request->getPost('name', 'striptags');
        $resources->module_name = $this->request->getPost('module_name', 'striptags');
        $resources->year_price = $this->request->getPost('year_price', 'striptags');
        $resources->month_price = $this->request->getPost('month_price', 'striptags');
        $resources->type = $this->request->getPost('type', 'striptags');
        $resources->description = $this->request->getPost('description', 'striptags');
        $resources->belong = $this->request->getPost('belong', 'striptags');
        $resources->free_period = $this->request->getPost('free_period', 'striptags');
        $resources->year_price_min = $this->request->getPost('year_price_min', 'striptags');
        $resources->year_price_max = $this->request->getPost('year_price_max', 'striptags');
        $resources->month_price_min = $this->request->getPost('month_price_min', 'striptags');
        $resources->month_price_max = $this->request->getPost('month_price_max', 'striptags');

        if (!$resources->save()) {
            $messages = [];
            foreach ($resources->getMessages() as $message) {
                $messages[] = (string)$message;
            }
            return $messages;
        }
        return true;
    }


}