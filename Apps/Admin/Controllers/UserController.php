<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-24
 * Time: 下午5:59
 */

namespace Multiple\Admin\Controllers;


use Models\User\UserForCustomers;
use Models\User\Users;
use Models\User\UsersWechat;
use Phalcon\Mvc\Model\Criteria;

class UserController extends ControllerBase {
    public function searchAction() {

    }

    public function listAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        }
        else {
            $query = Criteria::fromInput($this->di, "\\Models\\User\\Users", $_POST);
            $time_start = $this->request->getPost('created_min', 'striptags', false);
            if(!empty($time_start)) {
                $time_start = strtotime($time_start);
            }
            $time_end = $this->request->getPost('time_end', 'striptags', false);
            if(!empty($time_end)) {
                $time_end = strtotime($time_end);
            }
            
            if($time_start > 0) {
                $query->andWhere("created > '{$time_start}'");
            }
            if($time_end > 0) {
                if($time_start > 0 && $time_end > $time_start || empty($time_start)) {
                    $query->andWhere("created <= '{$time_end}'");
                }
            }
            $query->andWhere("host_key='" . HOST_KEY . "'");
            $parameters = $query->getParams();
            $count = Users::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', 'int', 20);

            $list = Users::find(array($parameters, 'limit' => $start . ','.$limit))->toArray();
            $this->response->setJsonContent(array(
                'rows' => $list,
                'results' => $count
            ))->send();
        }
    }
    
    public function detailAction() {
        $uid = $this->request->get('user_id', 'int', 0);
        if($uid > 0) {
            $user = Users::findFirst($uid);
            $this->view->setVar('user', $user);
            $wechat = UsersWechat::findFirst("user_id='{$uid}'");
            $this->view->setVar('wechat', $wechat);
            $customers = UserForCustomers::query()
                ->leftJoin('\\Models\\Customer\\Customers', "customer_id = c.id", 'c')
                ->andWhere("user_id = '{$uid}'")
                ->columns("customer_id, c.name, c.account")
                ->execute();
            $this->view->setVar('customers', $customers);
        }
        else {
            $this->flash->error("没有找到对应的用户！");
        }
    }

} 