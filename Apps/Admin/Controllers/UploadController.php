<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/8/14
 * Time: 11:37 AM
 */

namespace Multiple\Admin\Controllers;


use Upload\Upload;
use Util\Ajax;

class UploadController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->ajax = new Ajax();
    }

    /**
     * upload img
     */
    public function imgAction()
    {
        $this->view->disable();
        $upload = new Upload();
        $upload->upExt = 'jpg|jpeg|gif|png|bmp';
        $upload->maxAttachSize = 2097152; // 2M
        $upload->setAttachDir("uploads");
        $buff = $upload->upOne();
        $file = $upload->saveFile($buff);

        if ($this->request->get('from') == 'xheditor') {
            echo $msg = "{'msg':'" . $file['url'] . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
            exit;
        }
        return Ajax::init()->outRight('', array('url' => $file['url']));
    }

    /**
     * upload video
     */
    public function videoAction()
    {
        $upload = new Upload();
        $upload->upExt = 'mp4|3gp|ogg|webm';
        $upload->maxAttachSize = 209715200; // 200M
        $upload->setAttachDir("Public/estt/images/upload");
        $buff = $upload->upOne();
        $file = $upload->saveFile($buff);
        if ($this->request->get('from') == 'xheditor') {
            echo $msg = "{'msg':'" . $file['url'] . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
            exit;
        }
        return Ajax::init()->outRight('', array('url' => $file['url']));
    }

    /**
     * upload video
     */
    public function fileAction()
    {
        $upload = new Upload();
        $upload->upExt = 'zip|rar|doc|xls|ppt|docx|pptx|xlsx';
        $upload->maxAttachSize = 20971520; // 20M
        $upload->setAttachDir("Public/estt/images/upload");
        $buff = $upload->upOne();
        $file = $upload->saveFile($buff);
        if ($this->request->get('from') == 'xheditor') {
            echo $msg = "{'msg':'" . $file['url'] . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
            exit;
        }
        return Ajax::init()->outRight('', array('url' => $file['url']));
    }

    /**
     * save remote img
     */
    public function saveimgAction()
    {
        $arrUrls = explode('|', $_REQUEST['urls']);
        $urlCount = count($arrUrls);
        $upload = new Upload();
        $upload->setAttachDir("Public/estt/images/upload");
        $upload->upExt = 'jpg|jpeg|gif|png|bmp';
        for ($i = 0; $i < $urlCount; $i++) {
            $fileinfo = $upload->saveRemoteImg($arrUrls[$i]);
            if (!empty($fileinfo['buff']) && !empty($fileinfo['ext'])) {
                $file = $upload->saveFile($fileinfo);
                $arrUrls[$i] = $file['url'];
            }
        }

//        echo 'http://' . Config::getItem('domain.img') . implode('http://' . Config::getItem('domain.img') . '|', $arrUrls);

    }


} 