<?php
/**
 * Created by PhpStorm.
 * User: luguiwu
 * Date: 14-5-7
 * Time: 下午5:50
 */

namespace Multiple\Admin\Controllers;
use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Logger;
use Models\Logo;

class LogoController  extends ControllerBase {
    /**
     * 数据异步加载
     */
    public function searchAction()
    {

    }

    /**
     * 异步加载数据库数据
     */
    public function listAction() {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        }
        else {
            $query = Criteria::fromInput($this->di,"\\Models\\Logo", $_POST);
            $parameters = $query->getParams();
            $count = Logo::count($parameters);

            $query = new \Phalcon\Mvc\Model\Query("SELECT l.id,l.sales_id,l.domain,l.logo,l.brand_name ,s.name FROM \Models\Logo AS l LEFT JOIN \Models\Sales  AS s ON l.sales_id=s.id", $this->getDI());
            $list = $query->execute()->toArray();

            $this->response->setJsonContent(array(
                'rows' => $list,
                'results' => $count
            ))->send();
        }
    }

    public function createAction()
    {
        $this->view->disable();
        $result = $this->saveLogo();
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    public function editAction()
    {
        $this->di->get('logger')->log("respond result:" . "edit", Logger::INFO);
        $this->view->disable();
        $id = $this->request->getPost('id', 'int');
        $result = $this->saveLogo($id);
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    private function saveLogo($id = null)
    {
        if(is_numeric($id) && $id > 0) {
            $sales = Sales::findFirst($id);
            if ($sales == false) {
                return "功能未找到";
            }
        }
        else {
            $sales = new Sales();
            $sales->created = time();
            $sales->plat_sales = 1;
            $sales->first_agent=0;
            $sales->second_agent=0;
            $sales->role=$this->request->getPost('role', 'striptags');
        }

        $sales->name=$this->request->getPost('name', 'striptags');
        $sales->email=$this->request->getPost('email', 'striptags');
        $sales->qq=$this->request->getPost('qq', 'striptags');
        $sales->phone=$this->request->getPost('phone', 'striptags');
        $sales->area=$this->request->getPost('area', 'striptags');
        $sales->alipay=$this->request->getPost('alipay', 'striptags');

        if (!$sales->save()) {
            $messages = [];
            foreach ($sales->getMessages() as $message) {
                $messages[] = (string) $message;
            }
            return $messages;
        }
        return true;
    }

    public function removeAction()
    {
        $this->view->disable();
        if(!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $ids = $this->request->getPost('ids');
        if(!is_array($ids)) {
            $ids = array($this->filter->sanitize($ids, 'int'));
        }
        if(empty($ids)) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $this->db->begin();
        $result = true;
        try {
            foreach($ids as $id) {
                $sales = Sales::findFirst($id);
                if (!$sales) {
                    $result = false;
                    throw new \Exception("功能{$id}没有找到！");
                }

                if (!$sales->delete()) {
                    $messages = [];
                    foreach ($sales->getMessages() as $message) {
                        $messages[] = (string) $message;
                    }
                    $result = false;
                    throw new \Exception($messages);
                }
            }
            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
        }

        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }
} 