<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-6-5
 * Time: 下午2:44
 */

namespace Multiple\Admin\Controllers;

use Components\Payments\PaymentUtil;
use Models\Payments\PaymentConfigItems;
use Models\Payments\PaymentForHosts;
use Models\Payments\PaymentLog;
use Models\Payments\Payments;
use Phalcon\Mvc\Model\Criteria;

class PaymentController extends ControllerBase {
    public function listAction() {
        $this->view->disable();
        $payments = PaymentUtil::instance(HOST_KEY)->getHostPayments(null, true);

        $this->response->setJsonContent(array(
            'rows' => $payments,
            'results' => count($payments)
        ))->send();
    }

    public function detailAction() {

        $key = $this->request->get('type', 'striptags');
        if(!$key) {
            $this->flash->error("没有您想要的支付方式！");
        }

        $paymentUtil = PaymentUtil::instance(HOST_KEY);
        $payment = $paymentUtil->getHostPayments($key, true);
        $payment_config = $paymentUtil->getHostPaymentConfig($key, true);

        $this->view->setVar('payment', $payment);
        $this->view->setVar('config', $payment_config);
    }

    public function saveAction() {
        $this->view->disable();
        $type = $this->request->getPost('payment_key', 'striptags');
        if(!$type) {
            $this->response->setJsonContent(array("code" => 0, "message" => "没有指定支付方式"));
            $this->response->send();
            exit;
        }

        $messages = [];
        $code = 0;

        foreach($this->request->getPost() as $key => $val) {
            if($key != 'payment_key') {
                $configItem = new PaymentConfigItems();
                $configItem->belong_type = PaymentUtil::BELONG_TYPE_HOST;
                $configItem->key_id = HOST_KEY;
                $configItem->payment_type = $type;
                $configItem->config_key = $key;
                $configItem->config_value = $val;

                if(!$configItem->save()) {
                    foreach($configItem->getMessages() as $message) {
                        $messages[] = (string)$message;
                    }
                }
            }
        }

        PaymentUtil::instance(HOST_KEY)->getHostPaymentConfig($type, true);

        $this->response->setJsonContent(array(
            'code' => $code,
            'message' => $messages
        ))->send();
    }

    public function activeAction() {
        $type = $this->request->getPost('type', 'striptags', '');
        $active = $this->request->getPost('active', 'int', false);
        $this->view->disable();
        if(empty($type)) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '请求参数不全！'
            ))->send();
            exit;
        }

        if(empty($active)) {
            $active = 0;
        }
        else {
            $active = 1;
        }

        $paymentForHost = PaymentForHosts::findFirst("host_key='" . HOST_KEY . "' AND type='{$type}'");
        if(!$paymentForHost) {
            $payment = Payments::findFirst("key='{$type}'");
            if(!$payment) {
                $this->response->setJsonContent(array(
                    'code' => 1,
                    'message' => '暂时不支持你指定的支付方式！'
                ))->send();
                exit;
            }
            $paymentForHost = new PaymentForHosts();
            $paymentForHost->host_key = HOST_KEY;
            $paymentForHost->type = $type;
            $paymentForHost->is_active = 0;
            if(!$paymentForHost->save()) {
                $messages = [];
                foreach($paymentForHost->getMessages() as $message) {
                    $messages[] = (string)$message;
                }
                $this->response->setJsonContent(array(
                    'code' => 1,
                    'message' => $messages
                ))->send();
                exit;
            }
        }

        if(!$paymentForHost->update(array(
            'is_active' => $active
        ))) {
            $messages = [];
            foreach($paymentForHost->getMessages() as $message) {
                $messages[] = (string)$message;
            }
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => $messages
            ))->send();
            exit;
        }
        PaymentUtil::instance(HOST_KEY)->getHostPayments($type, true);
        $this->response->setJsonContent(array(
            'code' => 0,
            'message' => '激活状态更新成功！！'
        ))->send();
        exit;
    }

    public function logAction() {

    }

    public function logListAction() {
        $this->view->disable();
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        } else {
            $query = Criteria::fromInput($this->di, "\\Models\\Payments\\PaymentLog", $this->request->getPost());
            $query->andWhere("\\Models\\Payments\\PaymentLog.host_key='" . HOST_KEY . "' AND \\Models\\Payments\\PaymentLog.entry_type = '" . PaymentUtil::BELONG_TYPE_HOST . "'");
            $query->leftJoin("\\Models\\Customer\\Customers", 'customer.id = customer_id', 'customer');
            $time_start = $this->request->getPost('created_min', 'striptags', false);
            if (!empty($time_start)) {
                $time_start = strtotime($time_start);
            }
            $time_end = $this->request->getPost('time_end', 'striptags', false);
            if (!empty($time_end)) {
                $time_end = strtotime($time_end);
            }

            if ($time_start > 0) {
                $query->andWhere("ordered >= '{$time_start}'");
            }
            if ($time_end > 0) {
                if ($time_start > 0 && $time_end > $time_start || empty($time_start)) {
                    $query->andWhere("ordered <= '{$time_end}'");
                }
            }

            $account = $this->request->getPost('account', 'striptags', false);
            if ($account) {
                $query->andWhere("");
            }
            $query->andWhere("customer.host_key='" . HOST_KEY . "'");

            $parameters = $query->getParams();
            $count = PaymentLog::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', "int", 20);

            $columns = <<<EOF
            order_number, customer_id, cash, order_type, paid_order, paid_type,
            is_success, paid_time,
            entry_type, customer.account, customer.active, customer.name
EOF;

            $list = $query->columns($columns)
                ->limit($limit, $start)->execute()->toArray();
            $this->response->setJsonContent(array(
                'rows' => $list,
                'results' => $count
            ))->send();
        }
    }
} 