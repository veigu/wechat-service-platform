<?php
namespace Multiple\Admin\Controllers;

use Models\Customer\Customers;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Tag;

class CompaniesController extends ControllerBase
{
    protected function getForm($entity = null, $edit = false)
    {
        $form = new Form($entity);

        if (!$edit) {
            $form->add(new Text("id", array(
                "size" => 10,
                "maxlength" => 10
            )));
        } else {
            $form->add(new Hidden("id"));
        }

        $form->add(new Text("name", array(
            "size" => 24,
            "maxlength" => 70
        )));

        $form->add(new Text("telephone", array(
            "size" => 10,
            "maxlength" => 30
        )));

        $form->add(new Text("address", array(
            "size" => 14,
            "maxlength" => 40
        )));

        $form->add(new Text("city", array(
            "size" => 14,
            "maxlength" => 40
        )));

        return $form;
    }

    public function searchAction()
    {
        /* $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "\\Models\\Customers", $_POST);
            $this->session->set('customer_search_params', $query->getParams());
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = $this->session->get('customer_search_params', array());

        $query = $this->modelsManager->createBuilder()->addFrom("\\Models\\Customers")->where($parameters);
        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $query,
            "limit" => 10,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("companies", $page->items); */
    }

    public function listAction()
    {
        $this->view->disable();
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => "只支持AJAX异步访问"
            ))->send();
            exit(1);
        } else {
            $query = Criteria::fromInput($this->di, "\\Models\\Customer\\Customers", $_POST);
            $time_start = $this->request->getPost('created_min', 'striptags', false);
            if (!empty($time_start)) {
                $time_start = strtotime($time_start);
            }
            $time_end = $this->request->getPost('time_end', 'striptags', false);
            if (!empty($time_end)) {
                $time_end = strtotime($time_end);
            }

            if ($time_start > 0) {
                $query->andWhere("created > '{$time_start}'");
            }
            if ($time_end > 0) {
                if ($time_start > 0 && $time_end > $time_start || empty($time_start)) {
                    $query->andWhere("created <= '{$time_end}'");
                }
            }
            $query->andWhere("host_key='" . HOST_KEY . "'");

            $parameters = $query->getParams();

            $count = Customers::count($parameters);
            $start = $this->request->getPost("start", "int", 0);
            $limit = $this->request->getPost('limit', "int", 20);

            if (!$query instanceof \Phalcon\Mvc\Model\Criteria) {
                $query = Customers::query($this->di);
            }

            $query->leftJoin("\\Models\\Customer\\CustomerProfile", 'id = cp.customer_id', 'cp');

            $list = $query->columns("id, account, cp.company_name as name, email, industry, cp.contact_person as contact, cp.contact_tel as telephone, cp.contact_phone as cellphone, cp.province, cp.city, cp.town, cp.address, active,  FROM_UNIXTIME(created, '%Y-%m-%d') AS created")
                ->limit($limit, $start)->execute()->toArray();
            $this->response->setJsonContent(array(
                'rows' => $list,
                'results' => $count
            ))->send();
        }
    }

    public function detailAction()
    {
        $id = $this->request->getPost('id', 'int');
        $company = Customers::findFirst("id='{$id}'");
        $this->view->setVar('company', $company);
    }

    public function editAction()
    {
        $this->view->disable();
        $id = $this->request->getPost('id', 'int');
        $result = $this->saveCustomer($id);
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    public function createAction()
    {
        $this->view->disable();
        $result = $this->saveCustomer();
        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }

    private function saveCustomer($id = null)
    {
        if (is_numeric($id) && $id > 0) {
            $companies = Customers::findFirst($id);
            if ($companies == false) {
                return "企业信息未找到";
            }
        } else {
            $companies = new Customers();
            $companies->password = sha1("111111");
            $companies->created = time();
            $companies->account = $this->request->getPost('account', "striptags");
            $companies->type = 'f';
        }

        $companies->active = $this->request->getPost('active', 'int');
        $companies->name = $this->request->getPost("name", "striptags");
        $companies->contact = $this->request->getPost('contact', "striptags");
        $companies->telephone = $this->request->getPost("telephone", "striptags");
        $companies->cellphone = $this->request->getPost("cellphone", "striptags");
        $companies->email = $this->request->getPost('email', "email");
        $companies->industry = $this->request->getPost('industry', "striptags");
        $companies->province = $this->request->getPost('province', "striptags");
        $companies->city = $this->request->getPost('city', "striptags");
        $companies->town = $this->request->getPost('town', "striptags");
        $companies->address = $this->request->getPost("address", "striptags");
        $companies->time_start = $this->request->getPost('time_start', "striptags");
        if (strlen($companies->time_start) > 0 && !is_numeric($companies->time_start)) {
            $companies->time_start = strtotime($companies->time_start);
        }
        $companies->time_end = $this->request->getPost('time_end', "striptags");
        if (strlen($companies->time_end) > 0 && !is_numeric($companies->time_end)) {
            $companies->time_end = strtotime($companies->time_end);
        }

        if (!$companies->save()) {
            $messages = [];
            foreach ($companies->getMessages() as $message) {
                $messages[] = (string)$message;
            }
            return $messages;
        }
        return true;
    }

    public function removeAction()
    {
        $this->view->disable();
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $ids = $this->request->getPost('ids');
        if (!is_array($ids)) {
            $ids = array($this->filter->sanitize($ids, 'int'));
        }
        if (empty($ids)) {
            $this->response->setJsonContent(array(
                'success' => false
            ))->send();
            exit(1);
        }
        $this->db->begin();
        $result = true;
        try {
            foreach ($ids as $id) {
                $companies = Customers::findFirst($id);
                if (!$companies) {
                    $result = false;
                    throw new \Exception("企业{$id}没有找到！");
                }

                if (!$companies->delete()) {
                    $messages = [];
                    foreach ($companies->getMessages() as $message) {
                        $messages[] = (string)$message;
                    }
                    $result = false;
                    throw new \Exception($messages);
                }
            }
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
        }

        $this->response->setJsonContent(array(
            'success' => $result
        ))->send();
    }
}
