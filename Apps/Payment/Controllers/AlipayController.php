<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-8
 * Time: 上午11:49
 */

namespace Multiple\Payment\Controllers;

use Components\Payments\Alipay\AlipayNotify;
use Components\Payments\Alipay\AlipaySubmit;
use Components\Payments\Alipay\AlipayWapSubmit;
use Components\Payments\PaymentUtil;
use Components\Product\TransactionManager;
use Models\Product\AddonVipcardRechargeOrders;
use Models\Shop\ShopOrders;
use Models\System\SystemOrders;
use Models\User\UserAddress;
use Util\EasyEncrypt;

class AlipayController extends ControllerBase
{

    public $order_type = null;
    public $pay_type = null;
    public $order_number = null;
    /**
     * @var array
     */
    public $alipay_config = null;
    /**
     * @var \Phalcon\Mvc\Model
     */
    public $order = null;

    public $is_combined = false;

    public $belong_type = false;

    private function initParams($order_type = null, $order_number = null)
    {
        $url = $this->request->get('_url');
        $param = $this->request->getQuery();
        $this->di->get('paymentLogger')->info("received post url: " . $url . " query param: " . json_encode($param));
        if (empty($order_type)) {
            $this->view->enable();
            $this->di->get('paymentLogger')->info("请求失败: 参数order_type未设置");
            $this->printMessage("对不起，没有接收到有效的数据，说明这是非法的请求");
            exit;
        }
        $order_type = strtoupper($order_type);
        if (!in_array(strtoupper($order_type), array(
            PaymentUtil::ORDER_TYPE_SHOP_ORDER, PaymentUtil::ORDER_TYPE_PLATFORM, PaymentUtil::ORDER_TYPE_VIP_CARD
        ))
        ) {
            $this->di->get('paymentLogger')->info("请求失败: 暂时不支持此类订单的支付，order_type = {$order_type}");
            $this->view->enable();
            $this->printMessage("对不起，暂时不支持此类订单的支付");
            exit;
        }
        $this->order_type = strtoupper($order_type);

        if (empty($order_number)) {
            $this->view->enable();
            $this->di->get('paymentLogger')->info("请求失败: 没有接收到有效的订单号，order_number = {$order_number}");
            $this->printMessage("对不起，没有接收到有效的订单号，说明这是非法的请求");
            exit;
        }
        $this->order_number = EasyEncrypt::decode($order_number);
        if (empty($this->order_number)) {
            $this->view->enable();
            $this->di->get('paymentLogger')->info("请求失败: 订单号编码解析失败，decoded order_number = {$order_number}");
            $this->printMessage("对不起，您提供的订单号不正确，说明这是非法的请求");
            exit;
        }

        if (is_string($this->order_number)) {
            switch ($this->order_type) {
                case PaymentUtil::ORDER_TYPE_PLATFORM: {
                    $this->order = SystemOrders::findFirst("order_number='{$this->order_number}'");
                    break;
                }
                case PaymentUtil::ORDER_TYPE_SHOP_ORDER:
                {
//                    $this->order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                    if (preg_match('/P/', $this->order_number)) {
                        $this->order = ShopOrderCombine::findFirst("combine_order_number='" . $this->order_number . "'");
                        $this->is_combined = true;
                    } else {
                        $order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                        if ($order->combine_order_number) {
                            $combineOrder = ShopOrderCombine::findFirst("combine_order_number='" . $order->combine_order_number . "'");
                            if (!$combineOrder || ($combineOrder && ($combineOrder->use_coupon != '' || $combineOrder->use_point != 0))) {
                                $this->order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                            } else {
                                $this->order = $combineOrder;
                                $this->order_number = $combineOrder->combine_order_number;
                                $this->is_combined = true;
                            }
                        } else {
                            $this->order = $order;
                        }
                    }
                    break;
                }
                case PaymentUtil::ORDER_TYPE_VIP_CARD: {
                    $this->order = AddonVipcardRechargeOrders::findFirst("order_number='{$this->order_number}'");
                }
            }
        }

        if (!$this->order) {
            $this->view->enable();
            $this->di->get('paymentLogger')->info("请求失败: 系统没有找到对应的订单，order_number = {$this->order_number}");
            $this->printMessage("对不起，系统没有找到此订单");
            exit;
        }
        $this->customer_id = intval($this->order->customer_id);

        //
        if (!defined('CUR_APP_ID')) define('CUR_APP_ID', $this->customer_id);

        if ($order_type == PaymentUtil::ORDER_TYPE_PLATFORM) {
            $alipay_setting = PaymentUtil::instance(HOST_KEY)->getHostPayments(PaymentUtil::PAYMENT_KEY_ALIPAY, true);
            if (intval($alipay_setting['is_active']) != 1) {
                $this->view->enable();
                $this->di->get('paymentLogger')->info("请求失败: 平台商家不支持支付宝付款");
                $this->printMessage("对不起，商家不支持支付宝付款");
                exit;
            }
            $this->belong_type = PaymentUtil::BELONG_TYPE_HOST;
            $this->alipay_config = PaymentUtil::instance(HOST_KEY)->getPaymentConfig(PaymentUtil::BELONG_TYPE_HOST, HOST_KEY, PaymentUtil::PAYMENT_KEY_ALIPAY);
        } else {
            $alipay_setting = PaymentUtil::instance(HOST_KEY)->getCustomerPayments(CUR_APP_ID, PaymentUtil::PAYMENT_KEY_ALIPAY, true);
            if (intval($alipay_setting['is_active']) != 1) {
                $this->view->enable();
                $this->di->get('paymentLogger')->info("请求失败: 商家不支持支付宝付款");
                $this->printMessage("对不起，商家不支持支付宝付款");
                exit;
            }
            $this->belong_type = $alipay_setting['use_platform'] > 0 ? PaymentUtil::BELONG_TYPE_HOST : PaymentUtil::BELONG_TYPE_CUSTOMER;
            $this->alipay_config = PaymentUtil::instance(HOST_KEY)->getPaymentConfig($this->belong_type, CUR_APP_ID, PaymentUtil::PAYMENT_KEY_ALIPAY, true);
        }

        if (empty($this->alipay_config['key']) || empty($this->alipay_config['partner'])) {
            $this->view->enable();
            $this->di->get('paymentLogger')->info("请求失败: 商家没有设置对应的参数");
            $this->printMessage("对不起，商家还没有正确设置支付参数");
            exit;
        }

        if($this->isWap) {
            $this->pay_type = PaymentUtil::ALIPAY_TYPE_WAP_DIRECT;
        }
        else {
            /*$store_info = TransactionManager::instance(HOST_KEY)->getStoreSetting(CUR_APP_ID);
            if($store_info && $store_info['pay_mode']) {
                $this->pay_type = $store_info['pay_mode'];
            }
            else {*/
                $this->pay_type = PaymentUtil::ALIPAY_TYPE_DIRECT;
//            }
        }
//        $this->pay_type = $this->alipay_config['pay_type'];
    }

    /**
     * platform order request url: pay/PO/OS_123131313131312312
     * customer order request url: pay/SO/1231313123123123123
     * vip card recharge order request url: pay/CO/1231313123123123123
     *
     * @param null $order_type
     * @param null $order_number
     * @return mixed
     */
    public function payAction($order_type = null, $order_number = null)
    {
        $this->initParams($order_type, $order_number);
        $this->view->disable();

        /* *
         * 功能：即时到账交易接口接入页
         * 版本：3.3
         * 修改日期：2012-07-23
         * 说明：
         * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
         * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。

         *************************注意*************************
         * 如果您在接口集成过程中遇到问题，可以按照下面的途径来解决
         * 1、商户服务中心（https://b.alipay.com/support/helperApply.htm?action=consultationApply），提交申请集成协助，我们会有专业的技术工程师主动联系您协助解决
         * 2、商户帮助中心（http://help.alipay.com/support/232511-16307/0-16307.htm?sh=Y&info_type=9）
         * 3、支付宝论坛（http://club.alipay.com/read-htm-tid-8681712.html）
         * 如果不想使用扩展功能请把扩展功能参数赋空值。
         */
        /**************************请求参数**************************/

        //支付类型
        $payment_type = "1";
        //必填，不能修改
        //服务器异步通知页面路径
        $notify_url = "http://" . FRONT_DOMAIN . "/payment/alipay/notice/{$this->order_type}/{$order_number}";
        //需http://格式的完整路径，不能加?id=123这类自定义参数

        //页面跳转同步通知页面路径
        $return_url = "http://" . FRONT_DOMAIN . "/payment/alipay/return/{$this->order_type}";
        //需http://格式的完整路径，不能加?id=123这类自定义参数

        //卖家支付宝帐户
        $seller_email = $this->alipay_config['account'];
        //必填

        //客户端的IP地址
        //非局域网的外网IP地址，如：221.0.0.1
        $exter_invoke_ip = $this->request->getServer("REMOTE_ADDRESS");

        //必填
        //付款金额
        $price = "";
        //必填
        //商品数量
        $quantity = "1";
        //必填，建议默认为1，不改变值，把一次交易看成是一次下订单而非购买一件商品
        //物流费用
        $logistics_fee = "0.00";
        //必填，即运费
        //物流类型
        $logistics_type = "POST";
        //必填，三个值可选：EXPRESS（快递）、POST（平邮）、EMS（EMS）
        //物流支付方式
        $logistics_payment = "SELLER_PAY";
        //必填，两个值可选：SELLER_PAY（卖家承担运费）、BUYER_PAY（买家承担运费）
        //订单描述
        //需以http://开头的完整路径，如：http://www.xxx.com/myorder.html
        //收货人姓名
        $receive_name = '';
        //如：张三
        //收货人地址
        $receive_address = '';
        //如：XX省XXX市XXX区XXX路XXX小区XXX栋XXX单元XXX号
        //收货人邮编
        $receive_zip = '';
        //如：123456
        //收货人电话号码
        $receive_phone = '';
        //如：0571-88158090
        //收货人手机号码
        $receive_mobile = '';
        //如：13312341234

        //处理业务
        switch ($this->order_type) {
            case PaymentUtil::ORDER_TYPE_PLATFORM: {
                $out_trade_no = $this->order->order_number;
                $subject = "购买平台功能服务，商家ID：" . CUR_APP_ID;
                $total_fee = $this->order->total_money;
                $body = "";
                $show_url = "";
                break;
            }
            case PaymentUtil::ORDER_TYPE_SHOP_ORDER: {
                $out_trade_no = $this->order_number;
                $subject = "购买商品-平台商家ID：" . CUR_APP_ID;
                $total_fee = $this->order->paid_cash;
                $body = "";
                $show_url = "";
                //必填
                //付款金额
                $price = $this->order->paid_cash;
                //必填
                //商品数量
                $quantity = "1";
                //必填，建议默认为1，不改变值，把一次交易看成是一次下订单而非购买一件商品
                //物流费用
                $logistics_fee = $this->order->logistics_fee;
                //必填，即运费
                //物流类型

                //必填，两个值可选：SELLER_PAY（卖家承担运费）、BUYER_PAY（买家承担运费）
                //必填，三个值可选：EXPRESS（快递）、POST（平邮）、EMS（EMS）
                if (empty($logistics_fee)) {
                    //物流支付方式
                    $logistics_fee = "0.00";
                    $logistics_payment = "SELLER_PAY";
                    $logistics_type = "POST";
                } else {
                    //物流支付方式
                    $logistics_payment = "BUYER_PAY";
                    $logistics_type = "EXPRESS";
                }
                $total_fee += $logistics_fee;

                //订单描述
                //需以http://开头的完整路径，如：http://www.xxx.com/myorder.html
                //收货人姓名
                if (isset($this->order->address_id) && $this->order->address_id > 0) {
                    $addressModel = UserAddress::findFirst("id='{$this->order->address_id}'");
                    $receive_name = $addressModel->name;
                    //如：张三
                    //收货人地址
                    $receive_address = $addressModel->province . " " . $addressModel->city . ' ' . $addressModel->address;
                    //如：XX省XXX市XXX区XXX路XXX小区XXX栋XXX单元XXX号
                    //收货人邮编
                    $receive_zip = $addressModel->zip_code;
                    //如：123456
                    //收货人电话号码
                    $receive_phone = $addressModel->phone;
                    //如：0571-88158090
                    //收货人手机号码
                    $receive_mobile = "";
                    //如：13312341234
                }
                break;
            }
            case PaymentUtil::ORDER_TYPE_VIP_CARD: {
                $out_trade_no = $this->order->order_number;
                $card_number = $this->order->card_number;
                $subject = "会员充值--商家ID：" . CUR_APP_ID . ' 会员卡号:' . $card_number;
                $total_fee = $this->order->paid_cash;
                $body = "";
                $show_url = "";
                //必填
                //付款金额
                $price = $this->order->paid_cash;
                //必填
                //商品数量
                $quantity = "1";
                //必填，建议默认为1，不改变值，把一次交易看成是一次下订单而非购买一件商品
                break;
            }
            default: {
            $this->view->enable();
            $this->printMessage("对不起，您请求的服务不存在");
            exit;
            }
        }

        $alipaySubmit = new AlipaySubmit($this->alipay_config);
        //防钓鱼时间戳
        $anti_phishing_key = $alipaySubmit->query_timestamp();
        //若要使用请调用类文件submit中的query_timestamp函数

        /************************************************************/

        switch ($this->pay_type) {
            case PaymentUtil::ALIPAY_TYPE_WAP_DIRECT:
            {
                $merchant_url = "http://" . CUR_APP_ID . '.' . WAP_DOMAIN_END;
                //请求业务参数详细
                $req_data = '<direct_trade_create_req><notify_url>' . $notify_url . '</notify_url><call_back_url>' . $return_url . '</call_back_url><seller_account_name>' . $seller_email . '</seller_account_name><out_trade_no>' . $out_trade_no . '</out_trade_no><subject>' . $subject . '</subject><total_fee>' . $total_fee . '</total_fee><merchant_url>' . $merchant_url . '</merchant_url></direct_trade_create_req>';
                //必填
                /************************************************************/

                //构造要请求的参数数组，无需改动
                $para_token = array(
                    "service" => "alipay.wap.trade.create.direct",
                    "partner" => trim($this->alipay_config['partner']),
                    "sec_id" => trim($this->alipay_config['sign_type']),
                    "format" => 'xml',
                    "v" => '2.0',
                    "req_id" => date('Ymdhis'),
                    "req_data" => $req_data,
                    "_input_charset" => trim(strtolower($this->alipay_config['input_charset']))
                );
                $alipaySubmit = new AlipayWapSubmit($this->alipay_config);
                $html_text = $alipaySubmit->buildRequestHttp($para_token);
                //URLDECODE返回的信息
                $html_text = urldecode($html_text);

                //解析远程模拟提交后返回的信息
                $para_html_text = $alipaySubmit->parseResponse($html_text);

                if (!isset($para_html_text['request_token'])) {
                    $message = isset($para_html_text['res_error']) ? $para_html_text['res_error'] : "商家支付宝不支持WAP网站支付";
                    $this->printMessage($message);
                    exit;
                }
                //获取request_token
                $request_token = $para_html_text['request_token'];


                /**************************根据授权码token调用交易接口alipay.wap.auth.authAndExecute**************************/

                //业务详细
                $req_data = '<auth_and_execute_req><request_token>' . $request_token . '</request_token></auth_and_execute_req>';
                //必填

                //构造要请求的参数数组，无需改动
                $parameter = array(
                    "service" => "alipay.wap.auth.authAndExecute",
                    "partner" => trim($this->alipay_config['partner']),
                    "sec_id" => trim($this->alipay_config['sign_type']),
                    "format" => 'xml',
                    "v" => '2.0',
                    "req_id" => date('Ymdhis'),
                    "req_data" => $req_data,
                    "_input_charset" => trim(strtolower($this->alipay_config['input_charset']))
                );
                break;
            }
            case PaymentUtil::ALIPAY_TYPE_DIRECT: {
                //构造要请求的参数数组，无需改动
                $parameter = array(
                    "service" => "create_direct_pay_by_user",
                    "partner" => trim($this->alipay_config['partner']),
                    "payment_type" => $payment_type,
                    "notify_url" => $notify_url,
                    "return_url" => $return_url,
                    "seller_email" => $seller_email,
                    "out_trade_no" => $out_trade_no,
                    "subject" => $subject,
                    "total_fee" => $total_fee + (isset($this->order->logistics_fee) ? floatval($this->order->logistics_fee) : 0),
                    "body" => $body,
                    "show_url" => $show_url,
                    "anti_phishing_key" => $anti_phishing_key,
                    "exter_invoke_ip" => $exter_invoke_ip,
                    "_input_charset" => trim(strtolower($this->alipay_config['input_charset']))
                );
                break;
            }
            case PaymentUtil::ALIPAY_TYPE_ESCOW: {
                //构造要请求的参数数组，无需改动
                $parameter = array(
                    "service" => "create_partner_trade_by_buyer",
                    "partner" => trim($this->alipay_config['partner']),
                    "payment_type" => $payment_type,
                    "notify_url" => $notify_url,
                    "return_url" => $return_url,
                    "seller_email" => $seller_email,
                    "out_trade_no" => $out_trade_no,
                    "subject" => $subject,
                    "price" => $price,
                    "quantity" => $quantity,
                    "logistics_fee" => $logistics_fee,
                    "logistics_type" => $logistics_type,
                    "logistics_payment" => $logistics_payment,
                    "body" => $body,
                    "show_url" => $show_url,
                    "receive_name" => $receive_name,
                    "receive_address" => $receive_address,
                    "receive_zip" => $receive_zip,
                    "receive_phone" => $receive_phone,
                    "receive_mobile" => $receive_mobile,
                    "_input_charset" => trim(strtolower($this->alipay_config['input_charset']))
                );
                break;
            }
            case PaymentUtil::ALIPAY_TYPE_BOTH: {
                //构造要请求的参数数组，无需改动
                $parameter = array(
                    "service" => "trade_create_by_buyer",
                    "partner" => trim($this->alipay_config['partner']),
                    "payment_type" => $payment_type,
                    "notify_url" => $notify_url,
                    "return_url" => $return_url,
                    "seller_email" => $seller_email,
                    "out_trade_no" => $out_trade_no,
                    "subject" => $subject,
                    "price" => $price,
                    "quantity" => $quantity,
                    "logistics_fee" => $logistics_fee,
                    "logistics_type" => $logistics_type,
                    "logistics_payment" => $logistics_payment,
                    "body" => $body,
                    "show_url" => $show_url,
                    "receive_name" => $receive_name,
                    "receive_address" => $receive_address,
                    "receive_zip" => $receive_zip,
                    "receive_phone" => $receive_phone,
                    "receive_mobile" => $receive_mobile,
                    "_input_charset" => trim(strtolower($this->alipay_config['input_charset']))
                );
                break;
            }
            default : {
            $parameter = array();
            }
        }
        //建立请求
        $html_text = $alipaySubmit->buildRequestForm($parameter, "get", "即将进入支付页面");
        echo $html_text;
    }

    public function returnAction($order_type = null)
    {
        $order_number = $this->request->get('out_trade_no', 'striptags');
        $this->initParams($order_type, EasyEncrypt::encode($order_number));
        $alipayNotify = new AlipayNotify($this->alipay_config);
        $verify_result = $alipayNotify->verifyReturn();
        $this->view->enable();
        if ($verify_result) { //验证成功
            $this->payCallback();
            $redirectUrl = "http://" . CUR_APP_ID . '.' . WAP_DOMAIN_END . '/order/detail/' . $this->order_number;
            $this->flash->error("您已经支付成功！<script type='text/javascript'>setTimeout(function() {window.location.href = '$redirectUrl';}, 2000);</script>");
        } else {
            //验证失败
            //如要调试，请看alipay_notify.php页面的verifyReturn函数
            $this->printMessage("支付回调验证失败，非法的回调请求！");
        }
    }

    public function noticeAction($order_type = null, $order_number = null)
    {
        if(empty($order_number)) {
            $order_number = $this->request->get('out_trade_no', 'striptags');
            if($order_number) {
                $order_number = EasyEncrypt::encode($order_number);
            }
        }

        $this->initParams($order_type, $order_number);
        //计算得出通知验证结果
        $alipayNotify = new AlipayNotify($this->alipay_config);
        $verify_result = $alipayNotify->verifyNotify();

        $this->view->disable();
        if ($verify_result) { //验证成功
            $this->payCallback();
            echo "success";
        } else {
            //验证失败
            echo "fail";
        }
    }

    private function payCallback()
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //请在这里加上商户的业务逻辑程序代
        //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——

        //交易状态
        $trade_status = strtoupper($this->request->get('trade_status', 'striptags'));
        if(empty($trade_status)) {
            $trade_status = strtoupper($this->request->get("result", 'string'));
        }

        $trade_no = $this->request->get('trade_no', 'striptags');
        $isFailed = false;
        $this->di->get('paymentLogger')->info("支付宝入账类型：" . $this->pay_type . '支付状态：' . $trade_status);
        if ($this->pay_type == PaymentUtil::ALIPAY_TYPE_DIRECT || $this->pay_type = PaymentUtil::ALIPAY_TYPE_WAP_DIRECT) {
            switch ($trade_status) {
                case "SUCCESS";
                case "TRADE_SUCCESS":
                case "TRADE_FINISHED": {
                    if(!$this->order->update(array(
                        'is_paid' => 1,
                        'is_payment_arrived' => 1,
                        'paid_time' => time(),
                        'paid_order' => $trade_no,
                        'paid_type' => PaymentUtil::PAYMENT_KEY_ALIPAY,
                        'status' => TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS //待发货
                    ))) {
                        $isFailed = true;
                    }
                    $this->writeLog(CUR_APP_ID, $this->order->order_number, $this->order_type, $this->order->paid_cash, PaymentUtil::PAYMENT_KEY_ALIPAY, $trade_no, 1, $this->belong_type);
                    break;
                }
            }
        } else {
            switch ($trade_status) {
                case "WAIT_BUYER_PAY"; {
                    $this->order->update(array(
                        'is_paid' => 0,
                        'is_payment_arrived' => 0,
                        'paid_time' => time(),
                        'paid_order' => $trade_no,
                        'paid_type' => PaymentUtil::PAYMENT_KEY_ALIPAY,
                        'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY //待发货
                    ));
                    //平台支付日志记录
//                    $this->writeLog(CUR_APP_ID, $this->order->order_number, $this->order_type, $this->order->paid_cash, PaymentUtil::PAYMENT_KEY_ALIPAY, $trade_no, 1, $this->belong_type);
                    break;
                }
                //担保交易已在支付宝生成订单没有支付
                case "WAIT_SELLER_SEND_GOODS": {
                    $this->order->update(array(
                        'is_paid' => 1,
                        'is_payment_arrived' => 0,
                        'paid_time' => time(),
                        'paid_order' => $trade_no,
                        'paid_type' => PaymentUtil::PAYMENT_KEY_ALIPAY,
                        'status' => TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS //待发货
                    ));
                    break;
                }
                case "WAIT_BUYER_CONFIRM_GOODS": {
                    $this->order->update(array(
                        'is_paid' => 1,
                        'is_payment_arrived' => 0,
                        'paid_time' => time(),
                        'paid_order' => $trade_no,
                        'paid_type' => PaymentUtil::PAYMENT_KEY_ALIPAY,
                        'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_CONFIRM_GOODS //待发货
                    ));
                    break;
                }
                case "TRADE_SUCCESS":
                case "TRADE_FINISHED": {
                    $this->order->update(array(
                        'is_paid' => 1,
                        'is_payment_arrived' => 1,
                        'paid_time' => time(),
                        'paid_order' => $trade_no,
                        'status' => TransactionManager::ORDER_STATUS_TRADE_SUCCESS //待发货
                    ));

                    if ($this->is_combined) {
                        $orders = ShopOrders::find("combine_order_number = '{$this->order_number}'");
                        if ($orders) {
                            foreach ($orders as $order) {
                                if (!$order->update(array(
                                    'is_paid' => 1,
                                    'is_payment_arrived' => 1,
                                    'paid_time' => time(),
                                    'paid_order' => $trade_no,
                                    'status' => TransactionManager::ORDER_STATUS_TRADE_SUCCESS //待发货
                                ))
                                ) {
                                    $this->di->get("paymentLogger")->error("update order status failed, order:" . $order->order_number);
                                }
                            }
                        }
                    }
                    $this->writeLog(CUR_APP_ID, $this->order->order_number, $this->order_type, $this->order->paid_cash, PaymentUtil::PAYMENT_KEY_ALIPAY, $trade_no, 1, $this->belong_type);
                    break;
                }
            }
            if($isFailed) {
                $err = "";
                foreach($this->order->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get('paymentLogger')->info("支付回调订单更新失败" . $err);
            }
            else {
                $this->di->get('paymentLogger')->info("支付回调订单更新成功");
            }
        }

        return true;
    }

}
