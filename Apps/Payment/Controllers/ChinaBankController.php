<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-16
 * Time: 上午10:06
 */

namespace Multiple\Payment\Controllers;


use Components\Payments\ChinaBank\ChinaBankForm;
use Components\Payments\PaymentUtil;
use Components\Product\TransactionManager;
use Models\Shop\ShopOrders;
use Models\System\SystemOrders;

class ChinaBankController extends ControllerBase{

    public $cbpay_config;

    public $pay_mode;

    public $order_type;

    public $order;

    public $order_number;

    private function initParams($order_type = null, $order_number = null) {
        if(empty($order_type)) {
            $this->view->enable();
            echo "对不起，您没有传递有效的参数";
            exit;
        }
        $order_type = strtoupper($order_type);
        if(!in_array(strtoupper($order_type), array(
            PaymentUtil::ORDER_TYPE_SHOP_ORDER, PaymentUtil::ORDER_TYPE_PLATFORM, PaymentUtil::ORDER_TYPE_VIP_CARD
        ))) {
            $this->view->enable();
            echo "对不起，您没有此操作权限";
            exit;
        }
        $this->order_type = $order_type;

        if(empty($order_number)) {
            $this->view->enable();
            echo "对不起，没有提供订单编号";
            exit;
        }
        $this->order_number = $order_number;

        if(is_string($this->order_number)) {
            switch($this->order_type) {
                case PaymentUtil::ORDER_TYPE_PLATFORM: {
                    $this->order = SystemOrders::findFirst("order_number='{$this->order_number}'");
                    break;
                }
                case PaymentUtil::ORDER_TYPE_SHOP_ORDER: {
                    $this->order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                    break;
                }
                case PaymentUtil::ORDER_TYPE_VIP_CARD: {
                    $this->order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                }
            }
        }

        if(!$this->order) {
            $this->view->enable();
            echo "对不起，没有找到对应的订单";
            exit;
        }
        $this->customer_id = intval($this->order->customer_id);

        if($order_type == PaymentUtil::ORDER_TYPE_PLATFORM) {
            $cbpay_config = PaymentUtil::instance(HOST_KEY)->getHostPayments(PaymentUtil::PAYMENT_KEY_CBPAY);
            if($cbpay_config['is_active'] != '1') {
                $this->view->enable();
                echo "对不起，商家不支持网银在线付款";
                exit;
            }
            $this->cbpay_config = PaymentUtil::instance(HOST_KEY)->getPaymentConfig(PaymentUtil::BELONG_TYPE_HOST, HOST_KEY, PaymentUtil::PAYMENT_KEY_CBPAY);
        }
        else {
            $cbpay_config = PaymentUtil::instance(HOST_KEY)->getCustomerPayments(CUR_APP_ID, PaymentUtil::PAYMENT_KEY_CBPAY);
            if($cbpay_config['is_active'] != '1') {
                $this->view->enable();
                echo "对不起，商家不支持网银在线付款";
                exit;
            }
            $this->cbpay_config = PaymentUtil::instance(HOST_KEY)->getPaymentConfig(PaymentUtil::BELONG_TYPE_CUSTOMER, CUR_APP_ID, PaymentUtil::PAYMENT_KEY_CBPAY);
        }

        if(empty($this->cbpay_config['key']) || empty($this->cbpay_config['mid'])) {
            $this->view->enable();
            echo "对不起，商家还没有正确设置支付参数";
            exit;
        }

        $store_info = TransactionManager::instance(HOST_KEY)->getStoreSetting(CUR_APP_ID);
        if($store_info && $store_info['pay_mode']) {
            $this->pay_type = $store_info['pay_mode'];
        }
        else {
            $this->pay_type = PaymentUtil::ALIPAY_TYPE_DIRECT;
        }
    }

    public function sendAction($order_type = null, $order_number = null) {
        $this->initParams($order_type, $order_number);
        $this->view->disable();
        $formBuilder = new ChinaBankForm($this->cbpay_config);
        //****************************************
        $v_mid = $this->cbpay_config['mid']; 							    // 商户号，这里为测试商户号1001，替换为自己的商户号(老版商户号为4位或5位,新版为8位)即可
        $v_url = $this->request->getScheme() . '://' . FRONT_DOMAIN . '/payment/receive/' . $this->order_type . '/' . $this->order_number;	// 请填写返回url,地址应为绝对路径,带有http协议
        $v_auto_url = $this->request->getScheme() . '://' . FRONT_DOMAIN . '/china-bank/autoReceive' . $this->order_type . '/' . $this->order_number;	// 请填写返回url,地址应为绝对路径,带有http协议


        // 登陆后在上面的导航栏里可能找到“资料管理”，在资料管理的二级导航栏里有“MD5密钥设置”
        // 建议您设置一个16位以上的密钥或更高，密钥最多64位，但设置16位已经足够了
        //****************************************

        $order = $this->session->get('current_order');
        if(!$order instanceof SystemOrders) {
            $order = SystemOrders::findFirst("order_number='{$order}'");
        }

        $v_oid = $order->order_number;

        $v_amount = $order->paid_cash;                   //支付金额
        $v_moneytype = "CNY";                              //币种

        $v_md5info = $formBuilder->buildRequestSign($v_oid, $v_mid, $v_url, $v_amount, $v_moneytype);                             //md5函数加密并转化成大写字母

        $remark1 = "平台功能服务购买收费";					 //备注字段1
        $remark2 = "[url:={$v_auto_url}]";                   //备注字段2

        $v_rcvname   = empty($this->customer->name)?CUR_APP_ID: $this->customer->name;		// 收货人
        $v_rcvaddr   = ''  ;		// 收货地址
        $v_rcvtel    = empty($this->customer->telephone)?$this->customer->telephone:'';	// 收货人电话
        $v_rcvpost   = '';		// 收货人邮编
        $v_rcvemail  = empty($this->customer->email)?$this->customer->email:'';		// 收货人邮件
        $v_rcvmobile = empty($this->customer->cellphone)?$this->customer->cellphone:'';		// 收货人手机号

        $v_ordername   = empty($this->customer->name)?"智享时代营销平台客户，ID:".CUR_APP_ID: $this->customer->name;	// 订货人姓名
        $v_orderaddr   = '';	// 订货人地址
        $v_ordertel    = empty($this->customer->telephone)?$this->customer->telephone:'';	// 订货人电话
        $v_orderpost   = '';	// 订货人邮编
        $v_orderemail  =  empty($this->customer->email)?$this->customer->email:'';	// 订货人邮件
        $v_ordermobile = empty($this->customer->cellphone)?$this->customer->cellphone:'';	// 订货人手机号

        $data = array(
            'v_mid' => $v_mid,
            'v_oid' => $v_oid,
            'v_amount' => $v_amount,
            'v_moneytype' => $v_moneytype,
            'v_url' => $v_url,
            'v_md5info' => $v_md5info,
            'remark1' => $remark1,
            'remark2' => $remark2,
            'v_rcvname' => $v_rcvname,
            'v_rcvtel' => $v_rcvtel,
            'v_rcvpost' => $v_rcvpost,
            'v_rcvaddr' => $v_rcvaddr,
            'v_rcvemail' =>$v_rcvemail,
            'v_rcvmobile' => $v_rcvmobile,
            'v_ordername' => $v_ordername,
            'v_ordertel' => $v_ordertel,
            'v_orderpost' => $v_orderpost,
            'v_orderaddr' => $v_orderaddr,
            'v_ordermobile' => $v_ordermobile,
            'v_orderemail' => $v_orderemail
        );

        echo $formBuilder->buildRequestForm($data, "POST", "即将跳转至支付平台");
    }

    public function receiveAction($order_type = null, $order_number = null) {
        //****************************************	//MD5密钥要跟订单提交页相同，如Send.asp里的 key = "test" ,修改""号内 test 为您的密钥
        //如果您还没有设置MD5密钥请登陆我们为您提供商户后台，地址：https://merchant3.chinabank.com.cn/
        $config = $this->di->get("config");
        $cbPayConfig = $config->payment->cb;							//登陆后在上面的导航栏里可能找到“B2C”，在二级导航栏里有“MD5密钥设置”
        //建议您设置一个16位以上的密钥或更高，密钥最多64位，但设置16位已经足够了
//****************************************

        $formBuilder = new ChinaBankForm($cbPayConfig);
        $request = $this->request;

        $v_oid     = trim($request->getPost('v_oid', 'striptags', ''));       // 商户发送的v_oid定单编号
        $v_pmode   = iconv("gbk", 'utf-8', $request->getPost('v_pmode', 'striptags', ''));    // 支付方式（字符串）
        $v_pstatus = trim($request->getPost('v_pstatus', 'striptags', ''));   //  支付状态 ：20（支付成功）；30（支付失败）
        $v_pstring = iconv("gbk", "utf-8", $request->getPost('v_pstring', 'striptags', ''));   // 支付结果信息 ： 支付完成（当v_pstatus=20时）；失败原因（当v_pstatus=30时,字符串）；
        $v_amount  = trim($request->getPost('v_amount', 'striptags', ''));     // 订单实际支付金额
        $v_moneytype  =trim($request->getPost('v_moneytype', 'striptags', '')); //订单实际支付币种
        $remark1   = iconv("gbk", "utf-8", $request->getPost('remark1', 'striptags', ''));      //备注字段1
        $remark2   = iconv("gbk", "utf-8", $request->getPost('remark2', 'striptags', ''));     //备注字段2
        $v_md5str  = trim($request->getPost('v_md5str', 'striptags', ''));   //拼凑后的MD5校验值

        if ($formBuilder->checkSign($v_oid, $v_pstatus, $v_amount,$v_moneytype, $v_md5str))
        {
            if($v_pstatus=="20")
            {
                //支付成功，可进行逻辑处理！
                //商户系统的逻辑处理（例如判断金额，判断支付状态，更新订单状态等等）......
                $order = SystemOrders::findFirst("order_number='{$v_oid}'");
                if($order) {
                    if($order->update(array("status" => '1', 'paid_time' => time(), 'paid_type' => $v_pmode, 'paid_order_no' => $v_pstring, 'remark' => $remark1 . $remark2))) {
                        $this->session->remove("current_order");
                        $this->flash->success("恭喜您，付款成功！您可以继续进行下一步操作了！<a href='/panel'>返回控制面板</a> <a href='/'>返回首页</a> ");
                    }
                }
                else {
                    $this->flash->error("支付失败，请您检查账户金额是否足够；如果有疑问请联系客服。");
                }
            }
            else{
                $this->flash->error("支付失败，请您检查账户金额是否足够；如果有疑问请联系客服。");
            }

        }else{
            $this->flash->error("校验失败,数据可疑");
        }
    }

    public function autoReceiveAction($order_type = null, $order_number = null) {
        $this->view->disable();
        //****************************************	//MD5密钥要跟订单提交页相同，如Send.asp里的 key = "test" ,修改""号内 test 为您的密钥
        //如果您还没有设置MD5密钥请登陆我们为您提供商户后台，地址：https://merchant3.chinabank.com.cn/
        $config = $this->di->get("config");
        $cbPayConfig = $config->payment->cb;							//登陆后在上面的导航栏里可能找到“B2C”，在二级导航栏里有“MD5密钥设置”
        //建议您设置一个16位以上的密钥或更高，密钥最多64位，但设置16位已经足够了
//****************************************

        $formBuilder = new ChinaBankForm($cbPayConfig);
        $request = $this->request;

        $v_oid     =trim($request->getPost('v_oid', 'striptags', ''));       // 商户发送的v_oid定单编号
        $v_pmode   =trim($request->getPost('v_pmode', 'striptags', ''));    // 支付方式（字符串）
        $v_pstatus =trim($request->getPost('v_pstatus', 'striptags', ''));   //  支付状态 ：20（支付成功）；30（支付失败）
        $v_pstring =trim($request->getPost('v_pstring', 'striptags', ''));   // 支付结果信息 ： 支付完成（当v_pstatus=20时）；失败原因（当v_pstatus=30时,字符串）；
        $v_amount  =trim($request->getPost('v_amount', 'striptags', ''));     // 订单实际支付金额
        $v_moneytype  =trim($request->getPost('v_moneytype', 'striptags', '')); //订单实际支付币种
        $remark1   =trim($request->getPost('remark1', 'striptags', ''));      //备注字段1
        $remark2   =trim($request->getPost('remark2', 'striptags', ''));     //备注字段2
        $v_md5str  =trim($request->getPost('v_md5str', 'striptags', ''));   //拼凑后的MD5校验值

        if ($formBuilder->checkSign($v_oid, $v_pstatus, $v_amount,$v_moneytype, $v_md5str))
        {
            if($v_pstatus=="20")
            {
                //支付成功，可进行逻辑处理！
                //商户系统的逻辑处理（例如判断金额，判断支付状态，更新订单状态等等）......
                $order = SystemOrders::findFirst("order_number='{$v_oid}'");
                if($order) {
                    if($order->update(array("status" => '1', 'paid_time' => time(), 'paid_type' => $v_pmode, 'paid_order_no' => $v_pstring, 'remark' => $remark1 . $remark2))) {
                        $this->session->remove("current_order");
                        echo "ok";
                    }
                }
                else {
                    echo "error";
                }
            }
            else{
                echo "error";
            }

        }else{
            echo "error";
        }
    }
} 