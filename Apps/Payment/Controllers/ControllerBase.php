<?php

namespace Multiple\Payment\Controllers;

use Models\Payments\PaymentLog;
use Phalcon\Mvc\Controller;
use Util\Ajax;

class ControllerBase extends Controller
{
    /**
     * @var \Models\Customer\Customers
     */
    protected $customer = null;
    protected $customer_id = 0;
    /**
     * @var \Models\Customer\CustomerOpenInfo
     */
    protected $customer_wechat = null;

    protected $isWap = false;

    public function initialize()
    {

        $this->isWap = true;
        $this->view->setVar('uri', new \Util\Uri());
        $this->isWap = Ajax::isMobile();
//        $this->isWap = isset($_SERVER['HTTP_VIA']) ? (stristr($_SERVER['HTTP_VIA'],"wap") ? true : false) : false;
    }

    protected function err($code = "404", $msg = '404 page no found')
    {
        \Phalcon\Tag::setTitle('运行时错误');
        $this->view->setViewsDir(MODULE_PATH . '/Views');
        $this->response->setHeader('content-type', 'text/html;charset=utf-8');
        $this->response->setStatusCode($code, $msg);

        $this->view->setVar('msg', $msg);

        $this->view->pick('base/error');
    }

    protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $partsNum = count($uriParts);
        switch ($partsNum) {
            case 2:
                return $this->dispatcher->forward(
                    array(
                        'controller' => $uriParts[0],
                        'action' => $uriParts[1]
                    )
                );
                break;
            case 3:
                return $this->dispatcher->forward(
                    array(
                        'namespace' => $uriParts[0],
                        'controller' => $uriParts[1],
                        'action' => $uriParts[2]
                    )
                );
                break;
        }
    }

    /**
     * @param $customer_id
     * @param $order_number
     * @param $order_type 系统订单 商城订单 会员充值
     * @param $cash
     * @param $paid_type  alipay wxpay unionpay tenpay
     * @param $paid_order
     * @param $is_paid
     * @param $entry_type 商家账户， 平台账号
     */
    protected function writeLog($customer_id, $order_number, $order_type, $cash, $paid_type, $paid_order, $is_paid, $entry_type) {
        $log = PaymentLog::findFirst("order_number='{$order_number}' AND order_type='{$order_type}'");
        if($log) {
            if($log->is_paid) {
                return true;
            }
            $data = array(
                'customer_id' => $customer_id,
                'cash' => $cash,
                'paid_type' => $paid_type,
                'paid_order' => $paid_order,
                'is_success' => $is_paid,
                'entry_type' => $entry_type
            );
            if($is_paid) {
                $data['paid_time'] = date("Y-m-d H:i:s");
            }
            if(!$log->update($data)) {
                $msgs = [];
                foreach($log->getMessages() as $m) {
                    $msgs[] = (string)$m;
                }
                $this->di->get('paymentLogger')->error("payment log insert failed with data: " . json_encode($data) . ' ----- error message:' . json_encode($msgs));
            }
        }
        else {
            $log = new PaymentLog();
            $data = array(
                'host_key' => HOST_KEY,
                'customer_id' => $customer_id,
                'order_number' => $order_number,
                'order_type' => $order_type,
                'cash' => $cash,
                'paid_type' => $paid_type,
                'paid_order' => $paid_order,
                'is_success' => $is_paid,
                'entry_type' => $entry_type
            );
            if($is_paid) {
                $data['paid_time'] = date("Y-m-d H:i:s");
            }
            if(!$log->save($data)) {
                $msgs = [];
                foreach($log->getMessages() as $m) {
                    $msgs[] = (string)$m;
                }
                $this->di->get('paymentLogger')->error("payment log insert failed with data: " . json_encode($data) . ' ----- error message:' . json_encode($msgs));
            }
        }
    }

    protected function printMessage($message) {
        $str = <<<PARAM
<div style="margin:50px; font-size: 16px;text-align:center; padding: 5px;">
        <h1 style="border-bottom: 1px solid #ddd; font-size: 20px; text-align: left;">支付遇到问题了哦~</h1>
        <div style="color: red;">
        {$message}
        </div>
</div>
PARAM;
        echo $str;
        exit;
    }
}