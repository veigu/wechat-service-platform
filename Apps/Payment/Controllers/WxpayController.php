<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-8
 * Time: 上午11:49
 */

namespace Multiple\Payment\Controllers;

use Components\Payments\PaymentUtil;
use Components\Payments\WxPay\WxPayHelper;
use Components\Product\TransactionManager;
use Models\Shop\ShopOrderCombine;
use Models\Shop\ShopOrders;
use Models\System\SystemOrders;
use Util\EasyEncrypt;

class WxpayController extends ControllerBase
{

    public $order_type = null;
    public $pay_type = null;
    public $order_number = null;
    /**
     * @var array
     */
    public $wxpay_config = null;
    /**
     * @var \Phalcon\Mvc\Model
     */
    public $order = null;

    public $is_combined = false;

    public $belong_type = false;

    private function initParams($order_type = null, $order_number = null)
    {
        if (empty($order_type)) {
            $this->view->enable();
            $this->printMessage("对不起，您没有传递有效的参数");
            exit;
        }
        $order_type = strtoupper($order_type);
        if (!in_array(strtoupper($order_type), array(
            PaymentUtil::ORDER_TYPE_SHOP_ORDER, PaymentUtil::ORDER_TYPE_PLATFORM, PaymentUtil::ORDER_TYPE_VIP_CARD
        ))
        ) {
            $this->view->enable();
            $this->di->get('paymentLogger')->error("order type not property specified. type:" . $order_type . ' number:' . $order_number);
            echo "对不起，您没有此操作权限";
            exit;
        }
        $this->order_type = $order_type;

        if (empty($order_number)) {
            $this->view->enable();
            $this->di->get('paymentLogger')->info("请求失败: 没有接收到有效的订单号，order_number = {$order_number}");
            $this->printMessage("对不起，没有接收到有效的订单号，说明这是非法的请求");
            exit;
        }

        $this->order_number = EasyEncrypt::decode($order_number);
        if (empty($this->order_number)) {
            $this->view->enable();
            $this->di->get('paymentLogger')->error("order number not property specified. number:" . $order_number);
            echo "对不起，您提供订单编号不正确";
            exit;
        }

        if (is_string($this->order_number)) {
            switch ($this->order_type) {
                case PaymentUtil::ORDER_TYPE_PLATFORM: {
                    $this->order = SystemOrders::findFirst("order_number='{$this->order_number}'");
                    break;
                }
                case PaymentUtil::ORDER_TYPE_SHOP_ORDER: {
                    //  $this->order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                    if (preg_match('/P/', $this->order_number)) {
                        $this->order = ShopOrderCombine::findFirst("combine_order_number='" . $this->order_number . "'");
                        $this->is_combined = true;
                    } else {
                        $order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                        if ($order->combine_order_number) {
                            $combineOrder = ShopOrderCombine::findFirst("combine_order_number='" . $order->combine_order_number . "'");
                            if (!$combineOrder || ($combineOrder && ($combineOrder->use_coupon != '' || $combineOrder->use_point != 0))) {
                                $this->order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                            } else {
                                $this->order = $combineOrder;
                                $this->order_number = $combineOrder->combine_order_number;
                                $this->is_combined = true;
                            }
                        } else {
                            $this->order = $order;
                        }
                    }
                    break;
                }
                case PaymentUtil::ORDER_TYPE_VIP_CARD: {
                    $this->order = ShopOrders::findFirst("order_number='{$this->order_number}'");
                }
            }
        }

        if (!$this->order) {
            $this->view->enable();
            $this->di->get('paymentLogger')->error("order not found. number:" . $order_number . " type:" . $this->order_type);
            echo "对不起，没有找到对应的订单";
            exit;
        }
        $this->customer_id = intval($this->order->customer_id);
        //
        if (!defined('CUR_APP_ID')) define('CUR_APP_ID', $this->customer_id);

        if ($order_type == PaymentUtil::ORDER_TYPE_PLATFORM) {
            $wxpay_config = PaymentUtil::instance(HOST_KEY)->getHostPayments(PaymentUtil::PAYMENT_KEY_WXPAY);
            if ($wxpay_config['is_active'] != '1') {
                $this->view->enable();
                $this->di->get('paymentLogger')->error("merchat not support wechat payment..config:" . json_decode($this->wxpay_config));
                echo "对不起，商家不支持微信付款付款";
                exit;
            }
            $this->belong_type = PaymentUtil::BELONG_TYPE_HOST;
            $this->wxpay_config = PaymentUtil::instance(HOST_KEY)->getPaymentConfig(PaymentUtil::BELONG_TYPE_HOST, HOST_KEY, PaymentUtil::PAYMENT_KEY_WXPAY);
        } else {
            $wxpay_config = PaymentUtil::instance(HOST_KEY)->getCustomerPayments(CUR_APP_ID, PaymentUtil::PAYMENT_KEY_WXPAY);
            if ($wxpay_config['is_active'] != '1') {
                $this->view->enable();
                $this->di->get('paymentLogger')->error("merchat not support wechat payment.. config:" . json_decode($this->wxpay_config));
                echo "对不起，商家不支持微信付款付款";
                exit;
            }
            $this->belong_type = $wxpay_config['use_platform'] > 0 ? PaymentUtil::BELONG_TYPE_HOST : PaymentUtil::BELONG_TYPE_CUSTOMER;
            $this->wxpay_config = PaymentUtil::instance(HOST_KEY)->getPaymentConfig($this->belong_type, CUR_APP_ID, PaymentUtil::PAYMENT_KEY_WXPAY);
        }

        if (empty($this->wxpay_config['appid']) || empty($this->wxpay_config['appkey']) || empty($this->wxpay_config['partnerkey']) || empty($this->wxpay_config['appsecret'])) {
            $this->view->enable();
            $this->di->get('paymentLogger')->error("merchat not supplied property config for payment. config:" . json_decode($this->wxpay_config));
            echo "对不起，商家还没有正确设置支付参数";
            exit;
        }
    }

    public function indexAction($order_type = null)
    {
        $this->view->enable();
        $order_number = trim($this->request->get('order'));
        if (empty($order_type)) {
            $order_type = $this->request->get('order_type');
        }
        $this->initParams($order_type, $order_number);
        $customer = EasyEncrypt::decode($this->request->get('customer'));

        $this->view->setVar('customer', $customer);
        $this->view->setVar('order', $this->order);
        $this->view->setVar('order_number', $this->order_number);
        $this->view->setVar('order_type', $this->order_type);
        $this->view->setVar('wxpay_config', $this->wxpay_config->toArray());
    }

    //js支付回调
    public function jsAction($order_type = null)
    {
        $this->view->enable();
        $order_number = trim($this->request->get('order'));
        if (empty($order_type)) {
            $order_type = $this->request->get('order_type');
        }
        $this->initParams($order_type, $order_number);
        $customer = EasyEncrypt::decode($this->request->get('customer'));
//        $order = ShopOrders::findFirst("order_number='{$order_number}'");

        $this->view->setVar('customer', $customer);
        $this->view->setVar('order', $this->order);
        $this->view->setVar('order_number', $this->order_number);
        $this->view->setVar('order_type', $this->order_type);
        $this->view->setVar('wxpay_config', $this->wxpay_config->toArray());
    }

    //原生支付回调获取package数据
    public function nativeAction($order_type = null)
    {

        $postData = $this->request->getRawBody();
        $postXml = simplexml_load_string($postData);
        if (strlen($postData) > 0 && $postXml instanceof \SimpleXMLElement) {
            $order_number = $postXml->ProductId;
            $this->initParams($order_type, $order_number);
            $this->view->disable();

            $wxPayHelper = WxPayHelper::instance($this->wxpay_config);

//            appid、 appkey、 productid、 timestamp、 noncestr、 openid、 issubscribe

            $signData['appid'] = $postXml->AppId;
            $signData['productid'] = $postXml->ProductId;
            $signData['timestamp'] = $postXml->TimeStamp;
            $signData['noncestr'] = $postXml->NonceStr;
            $signData['openid'] = $postXml->OpenId;
            $signData['issubscribe'] = $postXml->IsSubscribe;

            $caculateSign = $wxPayHelper->get_biz_sign($signData);
            if ($caculateSign !== $postXml->AppSignature) {
                echo "非法请求，签名验证未通过";
            }

            $wxPayHelper->setParameter("bank_type", "WX");
            $wxPayHelper->setParameter("body", "微信支付购买商品");
            $wxPayHelper->setParameter("partner", $wxPayHelper->partnerKey);
            $wxPayHelper->setParameter("out_trade_no", $this->order->order_number);
            $wxPayHelper->setParameter("total_fee", "{$this->order->total_cash}");
            $wxPayHelper->setParameter("fee_type", "1");
            $wxPayHelper->setParameter("notify_url", "http://" . FRONT_DOMAIN . "/payment/wxpay/notice/{$this->order_type}");
            $wxPayHelper->setParameter("spbill_create_ip", $this->request->getClientAddress());
            $wxPayHelper->setParameter("input_charset", "UTF-8");

            echo $wxPayHelper->create_native_package();
            exit;
        } else {
            $this->printMessage("非法请求");
            exit;
        }
    }

    //同步回调url
    public function noticeAction($order_type = null)
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //请在这里加上商户的业务逻辑程序代
        //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——

        $postData = $this->request->getRawBody();
        $queryData = $this->request->getQuery();
        $postXml = simplexml_load_string($postData);
        $this->di->get('paymentLogger')->info('wechat payment start, received posted xml data:' . $postData . '\r\n query data:' . json_encode($queryData));
        if (strlen($postData) > 0 && $postXml instanceof \SimpleXMLElement) {
            $order_number = EasyEncrypt::encode($queryData['out_trade_no']);
            $this->initParams($order_type, $order_number);
            //验证签名
            $signData['appid'] = $postXml->AppId;
            $signData['appkey'] = $this->wxpay_config['appkey'];
            $signData['timestamp'] = $postXml->TimeStamp;
            $signData['noncestr'] = $postXml->NonceStr;
            $signData['openid'] = $postXml->OpenId;
            $signData['issubscribe'] = $postXml->IsSubscribe;

            $wxPayHelper = WxPayHelper::instance($this->wxpay_config);
            $caculateSign = $wxPayHelper->get_biz_sign($signData);
            if ($caculateSign != $postXml->AppSignature) {
                $this->di->get('paymentLogger')->error("signature is not equal. received sign:" . $postXml->AppSignature . ' gen sign:' . $caculateSign);
                echo "非法请求，签名验证未通过";
                exit;
            }

            //交易状态
            $trade_status = intval($this->request->get("trade_state", 'int'));

            $trade_no = $this->request->get('transaction_id', 'striptags');
            if (empty($trade_no)) {
                $trade_no = $this->request->get("bank_billno", "string");
            }

            switch ($trade_status) {
                case 0: {
                    $this->di->get("paymentLogger")->error("--------start update order information for order:" . $this->order->order_number);
                    if (!$this->order->update(array(
                        'is_paid' => 1,
                        'is_payment_arrived' => 1,
                        'paid_time' => strtotime($this->request->get("time_end", "int")),
                        'paid_order' => $trade_no,
                        'paid_type' => PaymentUtil::PAYMENT_KEY_WXPAY,
                        'status' => TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS, //待发货,
                        'wx_open_id' => "{$signData['openid']}"
                    ))
                    ) {
                        $this->di->get("paymentLogger")->error("update order status failed, order:" . $this->order->order_number);
                    }
                    if ($this->is_combined) {
                        $orders = ShopOrders::find("combine_order_number = '{$this->order_number}'");
                        if ($orders) {
                            foreach ($orders as $order) {
                                if (!$order->update(array(
                                    'is_paid' => 1,
                                    'is_payment_arrived' => 1,
                                    'paid_time' => strtotime($this->request->get("time_end", "int")),
                                    'paid_order' => $trade_no,
                                    'paid_type' => PaymentUtil::PAYMENT_KEY_WXPAY,
                                    'status' => TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS, //待发货,
                                    'wx_open_id' => "{$signData['openid']}"
                                ))
                                ) {
                                    $this->di->get("paymentLogger")->error("update order status failed, order:" . $order->order_number);
                                }
                            }
                        }
                    }

                    $this->writeLog(CUR_APP_ID, $this->order->order_number, $this->order_type, $this->order->paid_cash, PaymentUtil::PAYMENT_KEY_WXPAY, $trade_no, 1, $this->belong_type);
                    break;
                }
            }
        } else {
            $this->printMessage("非法请求，没有收到有效数据");
        }
        $this->di->get('paymentLogger')->info('wechat payment end. \r\n\r\n');
        echo "success";
        exit;
    }

    public function afterSaleAction()
    {

    }

    public function alertAction()
    {
        echo "success";
    }
}
