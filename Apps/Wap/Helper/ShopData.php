<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 6/20/14
 * Time: 4:19 PM
 */

namespace Multiple\Wap\Helper;

use Components\Product\ProductManager;
use Components\Product\TransactionManager;
use Models\Modules\Pointmall\AddonPointMallItem;
use Models\Modules\store\AddonStore;
use Models\Product\Product;
use Models\Product\ProductBrand;
use Models\Product\ProductCat;
use Phalcon\Mvc\User\Plugin;
use Util\EasyEncrypt;



class ShopData extends Plugin
{

    public static function init()
    {
        return new self();
    }

    public function cat()
    {

    }

    public function getStore($refresh = false)
    {
        // 是否开启店铺
       // echo CUR_APP_ID;exit;
        return TransactionManager::instance(HOST_KEY)->getStoreSetting(CUR_APP_ID, $refresh);
    }

    public function getCatalog()
    {
        $cats = ProductManager::instance()->getCats(CUR_APP_ID);

        return $cats;
    }

    public static function getListInSubCat($cid, $page = 0, $limit = 10, $order = 'created desc,modified desc')
    {
        $sub = ProductCat::find('customer_id = ' . CUR_APP_ID . ' and parent_id=' . $cid);
        $cids = array($cid);
        if ($sub) {
            $sub = $sub->toArray();
            $arr = array_column($sub, 'id');
            $cids = array_merge($arr, $cids);
        }
        $where = 'is_active=1 and customer_id=' . CUR_APP_ID . ' and cid in(' . implode(',', $cids) . ')';
        $res = Product::find(array(
            $where,
            'order' => $order,
            'limit' => $page * $limit . ',' . $limit
        ));

        return $res ? $res->toArray() : null;
    }

    public function getCount($where = null)
    {
        return Product::count($where);
    }

    /**
     *
     * @param $where
     * @param $page
     * @param $limit
     * @param $order
     * @return array|null
     */
    public function getList($where, $page = 0, $limit = 10, $order = 'created desc,modified desc')
    {
        $res = Product::find(array(
            $where,
            'order' => $order,
            'limit' => $page * $limit . ',' . $limit
        ));

        return $res ? $res->toArray() : null;
    }

    public function getList1($where, $page = 0, $limit = 10, $order = 'created desc,modified desc')
    {

        $sql = " SELECT p.*,ccd.name as cname,b.name as bname FROM product AS p  left join product_cat AS ccd ON p.cid=ccd.id
              LEFT JOIN product_brand as b ON  p.brand_id=b.id

             ";
        if ($where) {
            $sql .= " where " . $where;
        }
        if ($order) {
            $sql .= " order by " . $order;
        }
        $sql .= " limit " . $page * $limit . "," . $limit;
        $res = $this->db->query($sql)->fetchAll();
        return $res;
    }

    public function getItem($item_id = null)
    {
        $id = $this->getItemId($item_id);
        if ($id > 0) {
            $item = Product::findFirst('is_active = 1 and customer_id = ' . CUR_APP_ID . ' and id =' . $id);
            $item = $item ? $item->toArray() : null;
            return $item;
        }

        return null;
    }

    /**
     * 获取商品所属类目
     */
    public function getItemCat($item_id = null)
    {
        $id = $this->getItemId($item_id);
        if ($id > 0) {
            $cat = ProductManager::instance()->getCat($id, CUR_APP_ID);
            return $cat;
        }

        return null;
    }

    public function getItemAttr($item_id = null)
    {
        $id = $this->getItemId($item_id);
        if ($id > 0) {
            $attrs = ProductManager::instance()->getProductAttr($id);

            return $attrs;
        }
        return null;
    }

    public function getItemBrand($brand_id, $field = '')
    {
        if (!$brand_id) {
            return false;
        }
        $brand = ProductBrand::findFirst('id=' . $brand_id);
        if ($brand && $field) {
            return isset($brand->$field) ? $brand->$field : '';
        }
        return $brand ? $brand->toArray() : [];
    }

    public function getItemSpec($item_id = null)
    {
        $id = $this->getItemId($item_id);
        if ($id > 0) {
            $spec = ProductManager::instance()->getProductSpec($id);

            return $spec;
        }
        return null;
    }

    public function getPointMallItem($item_id)
    {
        $cur = AddonPointMallItem::findFirst('item_id=' . $item_id . ' and customer_id=' . CUR_APP_ID);
        return $cur ? $cur->toArray() : [];
    }

    /**
     *
     * @param $item_id
     * @return int|string
     */
    private function getItemId($item_id)
    {
        return is_numeric($item_id) && $item_id > 0 ? $item_id : intval(EasyEncrypt::decode($this->dispatcher->getParam(0)));
    }

    public function getSubStores()
    {
        // 是否开启店铺
        $stores = AddonStore::find('customer_id=' . CUR_APP_ID . ' and published=1');
        return $stores->toArray();
    }

    //获取商城所有一级分类及其分类下的热销商品
    public function getCatGoods($limit = 6)
    {
        $category = ProductManager::instance()->getCategories(CUR_APP_ID,false);//ProductManager::instance()->getCat(CUR_APP_ID);

        if ($category) {
            $category = array_slice($category, 0, 3);

            foreach ($category as $key => $item) {
                $cat = array($item['id']);
                if (!empty($item['subs'])) {
                    foreach ($item['subs'] as $sub) {
                        array_push($cat, $sub['id']);
                    }
                }
                $product = Product::find("customer_id=" . CUR_APP_ID . " and cid in ( " . implode(',', $cat) . ") and is_hot=1  ORDER BY created DESC LIMIT 0," . $limit);
                $category[$key]['goods'] = $product ? $product->toArray() : [];
            }
            return $category;
        }
        return [];

    }


} 