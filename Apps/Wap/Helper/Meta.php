<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 3/4/14
 * Time: 9:31 AM
 */

namespace Multiple\Wap\Helper;


use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;

class Meta extends Component
{
    static $siteName = '';

    public static function init()
    {
        return new self();
    }

    public function set()
    {
        self::$siteName = SiteData::init()->getSite('site_name');
        $controller = $this->dispatcher->getControllerName() ? $this->dispatcher->getControllerName() : 'index';
        $action = $this->dispatcher->getActionName() ? $this->dispatcher->getActionName() : 'index';

        $uri = $controller . '/' . $action;
        $title = isset(static::$meta[$uri]['title']) ? static::$meta[$uri]['title'] : '';
        $fullName = self::$siteName;
        $this->view->title = $title;
        $this->view->keywords = empty(static::$meta[$uri]['keywords']) ? $fullName : static::$meta[$uri]['keywords'];
        $this->view->description = empty(static::$meta[$uri]['description']) ? $fullName : static::$meta[$uri]['description'];
        $this->view->siteName = $fullName;
    }

    static $meta = array(
        'index/index' => array(
            'title' => '首页',
            'keywords' => '',
            'description' => '',
        ),

        'article/detail' => array(
            'title' => '文章详情',
            'keywords' => '',
            'description' => '',
        ),

        'article/index' => array(
            'title' => '文章栏目',
            'keywords' => '',
            'description' => '',
        ),

        'article/topic' => array(
            'title' => '栏目详情',
            'keywords' => '',
            'description' => '',
        ),

        'about/index' => array(
            'title' => '关于我们',
            'keywords' => '',
            'description' => '',
        ),

        'about/detail' => array(
            'title' => '内容详情',
            'keywords' => '',
            'description' => '',
        ),

        'user/login' => array(
            'title' => '用户登陆',
            'keywords' => '',
            'description' => '',
        ),

        'user/signup' => array(
            'title' => '用户注册',
            'keywords' => '',
            'description' => '',
        ),

        'user/index' => array(
            'title' => '用户中心',
            'keywords' => '',
            'description' => '',
        ),

        'user/setting' => array(
            'title' => '用户设置',
            'keywords' => '',
            'description' => '',
        ),

        'order/mine' => array(
            'title' => '用户订单',
            'keywords' => '',
            'description' => '',
        ),

        'user/vote' => array(
            'title' => '微投票',
            'keywords' => '',
            'description' => '',
        ),

        'user/survey' => array(
            'title' => '微问卷',
            'keywords' => '',
            'description' => '',
        ),

        'user/forgot' => array(
            'title' => '找回密码',
            'keywords' => '',
            'description' => '',
        ),

        'user/resetPass' => array(
            'title' => '修改密码',
            'keywords' => '',
            'description' => '',
        ),

        'order/addr' => array(
            'title' => '收货地址',
            'keywords' => '',
            'description' => '',
        ),

        'shop/index' => array(
            'title' => '店铺首页',
            'keywords' => '',
            'description' => '',
        ),

        'shop/catalog' => array(
            'title' => '全部分类',
            'keywords' => '',
            'description' => '',
        ),

        'shop/cat' => array(
            'title' => '商品列表',
            'keywords' => '',
            'description' => '',
        ),

        'order/detail' => array(
            'title' => '订单详情',
            'keywords' => '',
            'description' => '',
        ),

        'shop/search' => array(
            'title' => '商品搜索',
            'keywords' => '',
            'description' => '',
        ),

        'shop/item' => array(
            'title' => '商品详情',
            'keywords' => '',
            'description' => '',
        ),

        'order/pay' => array(
            'title' => '订单支付',
            'keywords' => '',
            'description' => '',
        ),

        'shop/cart' => array(
            'title' => '购物车',
            'keywords' => '',
            'description' => '',
        ),

        'shop/all' => array(
            'title' => '全部商品',
            'keywords' => '',
            'description' => '',
        ),

        'shop/settle' => array(
            'title' => '商品结算',
            'keywords' => '',
            'description' => '',
        ),

        'shop/share' => array(
            'title' => '店铺推广',
            'keywords' => '',
            'description' => '',
        ),
    );

    static $head_title = array(
        'shop_index' => '店铺中心',
        'shop_cat' => '商品列表',
        'shop_all' => '商品列表',
        'shop_item' => '商品详情',
        'shop_settle' => '确认订单',
        'shop_cart' => '购物车',
        'shop_catalog' => '全部分类',
        'shop_pay' => '订单支付',
        'shop_order' => '订单详情',
        'shop_share' => '店铺推广',
        'about_index' => '关于我们',
        'about_detail' => '企业信息',
        'article_index' => '文章栏目',
        'article_topic' => '栏目列表',
        'article_detail' => '内容详情',
        'user_login' => '用户登陆',
        'user_setting' => '用户设置',
        'user_signup' => '用户注册',
        'user_forgot' => '找回密码',
        'user_resetPass' => '修改密码',
        'user_order' => '用户订单',
        'user_addr' => '收货地址'
    );
}