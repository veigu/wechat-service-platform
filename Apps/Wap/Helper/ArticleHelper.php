<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 6/26/14
 * Time: 2:04 PM
 */

namespace Multiple\Wap\Helper;


use Models\Article\SiteArticlesCat;
use Phalcon\Mvc\User\Plugin;

class ArticleHelper extends Plugin
{

    public static function init()
    {
        return new self();
    }

    public function getCat($cid)
    {

    }

    public function getCats($p, $where)
    {

    }

    /**
     *
     * @param $cid
     * @return array|bool|null
     */
    public function getSubCats($cid)
    {
        if (!$cid) {
            return false;
        }

        $list = SiteArticlesCat::find(array('parent_id=' . $cid, 'order' => 'sort desc'));
        return $list ? $list->toArray() : null;
    }

    public function getPostList()
    {

    }

    /**
     *获取文章栏目
    **/
    public function getArticleCat()
    {
       $cat= SiteArticlesCat::find(array('customer_id='.CUR_APP_ID,'order'=>'sort desc'));
        return $cat ? $cat->toArray() :[];
    }
    /**
     *获取文章栏目的子栏目个数
     **/
    public function getSubCatCount($id)
    {
         $cat= SiteArticlesCat::count("customer_id=".CUR_APP_ID." and parent_id=".$id);
         return $cat;
    }


} 