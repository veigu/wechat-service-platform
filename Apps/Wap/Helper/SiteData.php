<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 5/19/14
 * Time: 3:09 PM
 */

namespace Multiple\Wap\Helper;


use Models\Article\SiteArticles;
use Models\Wap\SiteFocus;
use Models\Wap\SiteInfo;
use Models\Wap\SiteNavs;
use Models\Wap\SitePage;
use Phalcon\Mvc\User\Plugin;

class SiteData extends Plugin
{
    public static function init()
    {
        return new self();
    }

    public function getSite($key = null)
    {
        $cache = $this->getDI()->get('memcached');

        $res = $cache->get('site_' . CUR_APP_ID);
        if (!$res) {
            $info = SiteInfo::findFirst('customer_id=' . CUR_APP_ID);
            $info = $info ? $info->toArray() : '';
            if ($info) {
                // cache it
                $cache->save('site_' . CUR_APP_ID, $info);
                $res = $info;
            }
        }

        return isset($res[$key]) ? $res[$key] : $res;
    }

    public function getFocus($type)
    {

        $focus = SiteFocus::find(array('published = 1 and customer_id=' . CUR_APP_ID, '', "order" => "sort desc,created asc"));

        return $focus ? $focus->toArray() : array();
    }

    public function getNavs($position)
    {
        $focus = SiteNavs::find(array("published = 1 and customer_id=" . CUR_APP_ID . " and position = '" . $position . "'", 'limit' => 20,'order'=>'sort desc ,created asc'));
        return $focus ? $focus->toArray() : array();
    }

    public function getAllNavs($position)
    {
        $focus=SiteNavs::find(array("published = 1 and customer_id=" . CUR_APP_ID . " and position = '" . $position . "' AND parent_id=0 ", 'limit' => 4,'order'=>'sort desc ,created asc'));
        $focus= $focus ? $focus->toArray() : [];
        if($focus)
        {
           foreach($focus as $key=>$val)
           {
              $son= SiteNavs::find(array("published = 1 and customer_id=" . CUR_APP_ID . " and position = '" . $position . "' AND parent_id='".$val['id']."'", 'limit' => 20,'order'=>'sort desc ,created asc'));
              $focus[$key]['son']=$son ? $son->toArray() :[];
           }
        }
        return  $focus;
    }
    /**
     * 获取公司信息
     *
     * @param $id
     * @return \Phalcon\Mvc\Model|\Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getPageData($id = null)
    {
        $and_where = is_numeric($id) && $id > 0 ? " and id=" . $id : '';

        if ($and_where) {
            $data = SitePage::findFirst(array('published = 1 and customer_id=' . CUR_APP_ID . $and_where, 'order' => 'sort asc'));
        } else {
            $data = SitePage::find(array('published = 1 and customer_id=' . CUR_APP_ID . $and_where, 'order' => 'sort asc'));
        }

        return $data ? $data->toArray() : '';
    }


    /**
     *
     * @param $where
     * @param $page
     * @param $limit
     * @param $order
     * @return array|null
     */
    public function getList($where, $page = 0, $limit = 10, $order = 'created desc,modified desc')
    {

        $res = SiteArticles::find(array(
            $where,
            'order' => $order,
            'limit' => $page * $limit . ',' . $limit
        ));

        return $res ? $res->toArray() : null;
    }

} 