<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-9
 * Time: 下午4:31
 */

namespace Multiple\Wap\Api;


use Models\Article\SiteArticles;
use Util\EasyEncrypt;

class ArticleController extends ApiBase
{
    /**
     * 文章列表数据
     */
    public function listAction()
    {
        $id = $this->dispatcher->getParam(0);
        $id = EasyEncrypt::decode($id);
        $page = $this->request->get('page');
        $limit = 10;
        $list = new SiteArticles();
        $where = 'published = 1 and customer_id = ' . CUR_APP_ID . ' and cid=' . $id;
        $list = $list->find(array(
            $where,
            "order" => "created desc",
            "limit" => $limit * $page . ", " . $limit
        ));

        $res = array();
        if ($list) {
            $res = $list->toArray();
        }

        $data = array();
        if (count($res) > 0) {

            foreach ($res as $item) {

                $html = '<li ' . (empty($item['cover']) ? 'class="nocover"' : '') . '>';
                $html .= '<a href="' . $this->uri->baseUrl('/article/detail/' . \Util\EasyEncrypt::encode($item['id'])) . '">';
                $html .= '<dl>';
                $html .= '<dt>';
                $html .= '<h4 class="title">' . $item['title'] . '</h4>';
                if ($item['cover']) {
                    $html .= '<img src="/static/wap/images/default/loading.gif" class="ui-imglazyload" data-original="' . $item['cover'] . '" alt=""/>';
                }
                $html .= '</dt>';
                $html .= '<dd>';
                $html .= '<div class="summary">' . $item['summary'] . '</div>';
                $html .= '</dd>';
                $html .= '</dl>';

                $html .= '</a>';
                $html .= '</li>';

                $data[] = array(
                    'html' => $html
                );
            }
        }

        $this->view->setVar('data', $data);
    }
} 