<?php
/**
 * 需要验证用户是否登陆下的接口
 *
 * 验证和输出请统一使用Ajax工具类
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/28/14
 * Time: 2:41 PM
 */

namespace Multiple\Wap\Api;


use Components\Module\VipcardManager;
use Components\Rules\PointRule;
use Components\UserStatus;
use Models\Product\ProductCollection;
use Models\User\Users as user;
use Components\UserStatusWap;
use Upload\Fdfs;
use Upload\Upload;
use Util\Ajax;
use Util\Config;
use Util\Cookie;

class UserController extends ApiBase
{
    protected $checkLogin = null;

    public function saveProfileAction()
    {
        return UserStatusWap::init()->saveProfile();
    }

    public function resetPassAction()
    {
        return UserStatusWap::init()->resetPass();
    }

    public function forgetPassAction()
    {
        return UserStatusWap::init()->resetEmoiPass();
    }

    public function setPassAction()
    {
        return UserStatusWap::init()->setPass();
    }

    public function bindPhoneAction()
    {
        return UserStatusWap::init()->bindPhone();
    }

    public function bindWecahtAction()
    {
        return UserStatusWap::init()->bindWechat();
    }

    public function bindWeiboAction()
    {
        return UserStatusWap::init()->bindWeibo();
    }

    public function wechatWapResponse()
    {
        $code = $this->request->get('code', 'int', false);
    }

    public function weiboWapResponse()
    {

    }

    /*更新头像*/
    public function updatePortraitAction()
    {
        $url = $this->upload();
        $user = user::findFirst("id=" . $this->user_id);
        $msg = '';
        $type = 'ok';
        if (!$user) {
            $msg = "该用户不存在";
            $type = 'err';
        }
        $user->avatar = $url;

        if ($user->update()) {
            $msg = "更新成功";
            $type = 'ok';

        } else {
            $msg = "更新失败";
            $type = 'err';

        }
        $content = "<script>window.parent.myAlert('" . $msg . "','" . $type . "');
           window.parent.location.reload();</script>";
        return $this->response->setContent($content)->send();

    }

    private function upload()
    {
        // 初始化配置
        $conf = Config::getSite('upload', 'pic');
        $upload = new Upload();
        $upload->upExt = $conf['ext'];
        $upload->maxAttachSize = $conf['maxAttachSize'];
        $buff = $upload->upOne();

        // save into dfs
        $file = Fdfs::getInstance($conf['fastDFS'])->upload_filebuff($buff['buff'], $buff['ext']);

        $fileUrl = rtrim(Config::getSite('upload', 'baseUrl'), '/') . '/' . $file['group_name'] . '/' . $file['filename'];
        return $fileUrl;
    }

    /*
    * 我的收藏删除操作
    */
    public function rmCollectAction()
    {
        $id = $this->request->getPost('id');
        if (!$id || !is_numeric($id)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '非法的参数列表');
        }
        $uid = UserStatus::getUid();
        if (!$uid) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '你没有登录');
        }
        $product = ProductCollection::findFirst('customer_id=' . CUR_APP_ID . " AND user_id=" . $uid . " AND product_id=" . $id);
        if (!$product) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '该记录不存在');
        }
        if (!$product->delete()) {
            $this->di->get('errorLogger')->error(json_encode($product->getMessages()));
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '删除失败');
        } else {
            $vipgrade = VipcardManager::getUserVipGrade($uid, CUR_APP_ID);
            PointRule::init(CUR_APP_ID, $vipgrade)->executeRule($uid, PointRule::BEHAVIOR_DEL_FAVORITE);
        }
        return Ajax::init()->outRight('删除成功');
    }


}
