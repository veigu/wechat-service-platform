<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-9
 * Time: 下午5:26
 */

namespace Multiple\Wap\Api;


use Components\Order\CartManager;
use Components\Order\OrderGenerator;
use Components\Order\OrderManager;

class OrderController extends ApiBase
{
    // 删除购物车商品
    public function delCartAction()
    {
        CartManager::init()->delById();
    }

    // 更新购物车
    public function upCartAction()
    {
        CartManager::init()->updateCart();
    }

    // 批量删除购物车商品
    public function delProductAction()
    {
        CartManager::init()->delProduct();
    }

    // 生成订单
    public function generateOrderAction()
    {
        OrderGenerator::init()->settle();
    }

    // 取消订单
    public function cancelOrderAction()
    {
        OrderManager::init()->cancalOrder();
    }

    // 删除订单
    public function delOrderAction()
    {
        OrderManager::init()->delOrder();
    }

    //改变订单为货到付款
    public function orderCodAction()
    {
        OrderManager::init()->orderCod();
    }

    //确认收货
    public function confirmReceiptAction()
    {
        return OrderManager::init()->confirmReceipt();
    }

    //提交退/换货申请
    public function returnApplyAction()
    {
        OrderManager::init()->returnApply();
    }

    //取消退/换货
    public function cancelBackAction()
    {
        OrderManager::init()->cancelBack();
    }

    /*取消物流信息*/
    public function backGoodAction()
    {
        OrderManager::init()->backGood();
    }
    /*提交评论*/
    public function commentSubmitAction()
    {
       OrderManager::init()->commentSubmit();
    }

}

