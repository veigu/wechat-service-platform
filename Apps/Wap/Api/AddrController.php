<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-9
 * Time: 下午4:50
 */

namespace Multiple\Wap\Api;


use Models\User\UserAddress;
use Util\Ajax;

class AddrController extends ApiBase
{
    protected $checkLogin = true;

    public function saveAction()
    {
        $data = $this->request->get('data');
        $data['user_id'] = $this->user_id;
        $id = $this->request->get('id');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id > 0) {
            $m = UserAddress::findFirst('user_id = ' . $this->user_id . ' and id = ' . $id);
            if (!$m) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }
            // update
            if (!$m->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        } else {
            $count = UserAddress::count('user_id=' . $this->user_id);
            if ($count > 5) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '最多可以添加5个地址');
            }

            // 加入默认
            if ($count == 0) {
                $data['is_default'] = 1;
            }

            $m = new UserAddress();
            // create
            if (!$m->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        }


//        return $result;
        return Ajax::init()->outRight('');
    }

    public function getListAction()
    {
        $addr_list = '';

        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        $from_user_addr_page = $referer && strpos($referer, 'order/addr') !== false ? true : false;

        // 原有设为0
        $all = UserAddress::find('user_id = ' . $this->user_id);
        if ($all) {
            $all = $all->toArray();
            foreach ($all as $addr) {
                $tap_addr_class = $from_user_addr_page ? '' : ' tap-addr';
                $checked = $addr['is_default'] == '1' ? 'checked' : '';
                $choosed = $addr['is_default'] == '1' ? 'choosed' : '';
                $address = $addr['province'] . '·' . $addr['city'] . '·' . $addr['town'];
                $addr_info = <<<EOF
                <div class="addr {$choosed}" data-id="{$addr['id']}">
                    <a class="addr-content {$tap_addr_class}" data-id="{$addr['id']}" href="javascript:;">
                        <p class="address">{$address} </p>

                        <p>{$addr['address']}</p>

                        <p>
                            {$addr['name']}
                            {$addr['phone']}
                        </p>
                    </a>

                    <p class="addr-act">
                            <span class="right">
                                <a href="javascript:;" class="editAddrBtn"
                                   data-id="{$addr['id']}"
                                   data-name="{$addr['name']}"
                                   data-phone="{$addr['phone']}"
                                   data-zip_code="{$addr['zip_code']}"
                                   data-province="{$addr['province']}"
                                   data-city="{$addr['city']}"
                                   data-town="{$addr['town']}"
                                   data-address="{$addr['address']}"
                                    >
                                    <img src="/static/wap/images/order/edit.png" alt=""/>
                                </a>
                                <a href="javascript:;" class="rmAddrBtn" data-id="{$addr['id']}">
                                    <img src="/static/wap/images/order/del.png" alt=""/>
                                </a>
                            </span>
                            <span>
                                <input type="radio" name="is_default" data-id="{$addr['id']}"
                                       class="is_default" {$checked}/> 设为默认
                            </span>
                    </p>
                </div>
EOF;
                $addr_list .= $addr_info;
            }
        }
        return Ajax::init()->outRight('', $addr_list);
    }

    public function setDefaultAction()
    {
        $id = $this->request->get('id');
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $m = UserAddress::findFirst('user_id = ' . $this->user_id . ' and id = ' . $id);
        if (!$m) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        // 原有设为0
        $all = UserAddress::find('user_id = ' . $this->user_id);
        if ($all) {
            foreach ($all as $addr) {
                $addr->is_default = 0;
                $addr->update();
            }
        }

        // 更新当前为1
        if (!$m->update(array('is_default' => 1))) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED, '设置默认地址失败');
        }

        return Ajax::init()->outRight('');
    }

    public function delAction()
    {
        $id = $this->request->get('id');
        if (!($id)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $addr = UserAddress::findFirst('user_id=' . $this->user_id . ' and id =' . $id);
        if (!$addr) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        // 重新设默认地址
        if ($addr->is_default == 1) {
            $def = UserAddress::findFirst('user_id=' . $this->user_id);
            $def->is_default = 1;
            $def->update();
        }

        if (!$addr->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }

        return Ajax::init()->outRight('');
    }

} 