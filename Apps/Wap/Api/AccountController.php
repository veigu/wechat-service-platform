<?php
namespace Multiple\Wap\Api;


use Components\UserStatusWap;
use Multiple\Wap\Helper\SiteData;
use Util\Ajax;
use Util\EasyEncrypt;

class AccountController extends ApiBase
{
    /**
     * login
     */
    public function loginAction()
    {
        return UserStatusWap::init()->ajaxLogin();
    }

    /**
     * reg
     */
    public function regAction()
    {
        return UserStatusWap::init()->ajaxReg();
    }

    /**
     * 重置密码
     *
     */
    public function sendPassAction()
    {
        $email = $this->request->get('email');
        $phone = EasyEncrypt::decode($this->request->get('phone'));
        $siteName = SiteData::init()->getSite('site_name');

        UserStatusWap::init()->sendPassMail($email, $phone, $siteName);
    }

    /**
     * 重置密码
     *
     */
    public function reset_passAction()
    {
        $this->ajax = new Ajax();
        $phone = $this->request->get('phone');
        $pass = $this->request->get('pass');

        UserStatusWap::init()->resetPassByPhone($phone, $pass);
    }

    public function sendVerifyCodeAction()
    {
        $phone = $this->request->getPost("phone");
        $check_unique = $this->request->getPost("check_unique", "string");

        UserStatusWap::init()->sendVerifyCode($phone, $check_unique);
    }

    public function checkVerifyCodeAction()
    {
        $code = $this->request->getPost("code", 'int');

        UserStatusWap::init()->checkVerifyCode($code);
    }


    public function checkPhoneAction()
    {
        $phone = $this->request->getPost("phone", 'int');

        UserStatusWap::init()->checkPhone($phone);
    }

    public function checkNickAction()
    {
        $nickname = $this->request->getPost("nickname", 'string');

        UserStatusWap::init()->checkNick($nickname);
    }
}