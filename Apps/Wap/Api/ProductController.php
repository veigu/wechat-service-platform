<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-9
 * Time: 下午4:04
 */

namespace Multiple\Wap\Api;

use Models\Product\Product;
use Models\Product\ProductCat;
use Components\Product\TransactionManager;

class ProductController extends ApiBase
{

    /**
     * 获取商品列表
     */
    public function item_listAction()
    {
        $page = $this->request->get('page');
        $order = $this->request->get('order', null, 'is_hot');
        $sort = $this->request->get('sort');
        $cid = intval($this->request->get('cid'));
        $keyword = $this->request->get('keyword');

        $where = "is_active = 1 AND customer_id = " . CUR_APP_ID;

        if ($cid > 0) {
            $where .= ' AND cid=' . $cid;
            $sub = ProductCat::find('customer_id = ' . CUR_APP_ID . ' and parent_id=' . $cid);
            $cids = array($cid);
            if ($sub) {
                $sub = $sub->toArray();
                $arr = array_column($sub, 'id');
                $cids = array_merge($arr, $cids);
            }
            $where .= ' and cid in(' . implode(',', $cids) . ') ';
        }

        if (!empty($keyword)) {
            $where .= " AND name like '%$keyword%'";
        }

        $products = Product::query()
            ->where($where)
            ->limit(10, 10 * $page)
            ->orderBy("$order $sort")
            ->execute();

        $data = array();
        if (count($products) > 0) {

            foreach ($products as $idx => $pro) {
                $html = "<li>";
                $html .= "<a href=\"" . $this->uri->baseUrl('shop/item/' . \Util\EasyEncrypt::encode($pro->id)) . "\">";
                $html .= '<p><img src="/static/wap/images/default/loading.gif" class="ui-imglazyload" data-original="' . $pro->thumb . '" /></p>';
//                $html .= '<p><img src="' . $pro->thumb . '" class="ui-imglazyload" data-original="' . $pro->thumb . '" /></p>';
                $html .= "<div class=\"detail\">";
                $html .= "<p class=\"price\">&yen; $pro->sell_price <del>$pro->original_price</del></p>";
                $html .= "<p class=\"name\">$pro->name</p>";
                $html .= "</div></a></li>";
                $data[] = array(
                    'html' => $html
                );
            }
        }

        $this->view->setVar('data',$data);        
    }

    /*emoi 的商品筛选*/
    public function item_emoiListAction()
    {
        $page = $this->request->get('page'); //第几页
        $page = $page ? $page : 0;

        $where = $this->request->get('where'); //第几页

        $product = $this->db->query(
            " SELECT p.* ,soi.item_id,IFNULL(soi.saleVolumn,0) as saleVolumn FROM  product AS p LEFT JOIN

         (SELECT item_id,SUM(quantity) as saleVolumn FROM shop_order_item as soi left join shop_orders as so ON soi.order_number=so.order_number where status <> ".TransactionManager::ORDER_STATUS_BUYER_CANCELED."  GROUP BY item_id ) AS soi

         ON p.id=soi.item_id
         " . $where . " LIMIT " . ($page * 10) . " ,10"
        )->fetchAll();

        $data = array();
        if (count($product) > 0) {

            foreach ($product as $idx => $pro) {
                $html = "<li>";
                $html .= "<div class='pic'><a href=\"" . $this->uri->baseUrl('shop/item/' . \Util\EasyEncrypt::encode($pro['id'])) . "\">";
                $html .= '<img src="/static/wap/images/default/loading.gif" class="ui-imglazyload" data-original="' . $pro['thumb'] . '" /></a></div>';
                $html .= "<div class=\"details\">";
                $html .= "<div class=\"name\">" . $pro['name'] . "</div>";
                $html .= "<div class=\"saleVolumn\">总销量:" . $pro['saleVolumn'] . "</div>";
                $html .= " <div class='price'>".($pro['original_price']>$pro['sell_price'] ? "<span style='text-decoration: line-through'>".$pro['original_price']."</span>" : '')."￥" . $pro['sell_price'] . "</div>";
                $html .= "</div>";
                $html .= "<div class='clear'></div>";
                $html .= "</li>";
                $data[] = array(
                    'html' => $html
                );
            }
        }
        $this->view->setVar('data', $data);
    }

    /*仁豪 的商品筛选*/
    public function item_renhaoListAction()
    {
        $page = $this->request->get('page'); //第几页
        $page = $page ? $page : 0;

        $where = $this->request->get('where'); //第几页

        $product = $this->db->query(
            " SELECT p.* ,soi.item_id,ccd.name as cname , b.name as bname, IFNULL(soi.saleVolumn,0) as saleVolumn FROM  product AS p LEFT JOIN

 (SELECT item_id,SUM(quantity) as saleVolumn FROM shop_order_item as soi left join shop_orders as so ON soi.order_number=so.order_number where status <> ".TransactionManager::ORDER_STATUS_BUYER_CANCELED."  GROUP BY item_id ) AS soi

 ON p.id=soi.item_id LEFT JOIN product_cat as ccd ON p.cid=ccd.id
  LEFT JOIN product_brand as b ON  p.brand_id=b.id
 " . $where . " LIMIT " . ($page * 10) . " ,10"
        )->fetchAll();

        $data = array();
        if (count($product) > 0) {


            foreach ($product as $idx => $pro) {
                $html = "<li>";
                $html .= "<div class='li_wrapper " . ($idx % 2 == 0 ? "m_l'" : "m_r'") . "><a href=\"" . $this->uri->baseUrl('shop/item/' . \Util\EasyEncrypt::encode($pro['id'])) . "\">";
                $html .= '<img src="/static/wap/images/default/loading.gif" class="ui-imglazyload" data-original="' . $pro['thumb'] . '" alt="' . $pro['name'] . '" /></a>';
                $html .= "<p>" . $pro['name'] . "</p>";
                $html .= "<p><span>品牌：</span>" . $pro['bname'] . "</p>";
                $html .= "<p><span>类型：</span>" . $pro['cname'] . "</p></div>";
                $html .= "</li>";
                $data[] = array(
                    'html' => $html
                );
            }
        }

        $this->view->setVar('data', $data);
    }

} 