<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-9
 * Time: 下午4:42
 */

namespace Multiple\Wap\Api;


use Components\UserStatusWap;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\View;
use Util\Ajax;

class ApiBase extends Controller
{
    protected $checkLogin = false;
    protected $user_id = 0;

    public function initialize()
    {
        $this->view->disable();
        $this->user_id = UserStatusWap::getUid();

        if ($this->checkLogin) {
            $this->checkLogin();
        }
    }

    protected function checkLogin()
    {
        if (!UserStatusWap::init()->isLogged()) {
            Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
            $this->afterExecuteRoute();
            exit;
        }
    }

    // ajax 输出
    public function afterExecuteRoute()
    {
        $this->setHead();
        $data = $this->view->getParamsToView();
        $result = array(
            'error' => array('code' => Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, 'msg' => Ajax::getErrorMsg(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED), 'more' => "数据无返回"),
            'result' => 0,
        );
        // 设置了数据
        if (isset($data['data'])) {
            $result = $data['data'];
        }

        $this->response->setContent(json_encode($result, JSON_UNESCAPED_UNICODE));
        return $this->response->send();
    }

    public function setHead()
    {
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setHeader('Access-Control-Allow-Origin', '*');
        $this->response->setHeader('Access-Control-Allow-Headers', 'content-disposition, origin, content-type, accept');
        $this->response->setHeader('Access-Control-Allow-Credentials', 'true');
        $this->response->setHeader('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
        $this->response->setHeader('Last-Modified', gmdate("D, d M Y H:i:s") . " GMT");
        $this->response->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
        $this->response->setHeader('Pragma', 'no-cache');
    }
}