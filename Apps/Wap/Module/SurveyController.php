<?php

namespace Multiple\Wap\Module;

use Models\Modules\VoteSurvey\AddonVoteSurvey;
use Models\Modules\VoteSurvey\AddonVoteSurveyLog;
use Models\Modules\VoteSurvey\AddonVoteSurveyOption;
use Models\Modules\VoteSurvey\AddonVoteSurveyQuestion;
use Multiple\Wap\Helper\Meta;
use Multiple\Wap\Helper\SiteData;
use Phalcon\Tag;

class SurveyController extends ModuleBase
{
    /*问卷调查汇总页*/
    public function indexAction()
    {

        Meta::$head_title['vote_index'] = "问卷调查";
        $time = time();
        $list = AddonVoteSurvey::find(array(
            'customer_id=' . CUR_APP_ID . "AND type='survey' AND start_date <= " . $time . ' AND end_date>=' . $time,
            'order' => 'created desc',
        ));
        $list = $list ? $list->toArray() : '';
        if (!$list) {
            $this->err('404', '暂无问卷调查内容！');
            return;
        }
        $this->view->setVar('list', $list);
    }

    /*问卷调查详情页*/
    public function surveyAction()
    {
        if (!$this->checkLogin()) {
            header('user/');
        }
        $id = \Util\EasyEncrypt::decode($this->dispatcher->getParam(0));

        if (!($id && is_numeric($id))) {
            $this->err('404', '未找到相关问卷调查,可能原因:该投票已不存在或者已过期');
            return;
        }
        $time = time();
        $opt = AddonVoteSurvey::findFirst('id=' . $id . " AND type='survey'  AND customer_id=" . CUR_APP_ID . ' AND start_date <=' . $time . ' AND end_date>=' . $time);
        $this->view->setVar('survey', $opt ? $opt->toArray() : []);
        if (!$opt) {
            $this->err('404', '未找到相关问卷调查,可能原因:该投票已不存在或者已过期');
            return;
        }
        $opt = $opt->toArray();
        $question = AddonVoteSurveyQuestion::find(array('vote_id=' . $id, 'limit' => '0,10', 'order' => 'id'));
        $count = AddonVoteSurveyQuestion::count('vote_id=' . $id);
        if ($question) {
            $question = $question->toArray();
            foreach ($question as $key => $val) {
                $option = AddonVoteSurveyOption::find('question_id=' . $val['id']);
                $question[$key]['option'] = $option ? $option->toArray() : [];
            }
        }

        $this->view->setVar('question', $question);
        $this->view->setVar('count', $count);
    }

    /**
     ** 问卷调查提交
     **/
    public function submitAction()
    {
        $this->view->disable();
        $survey_id = $this->request->getPost('survey_id');
        if (!$survey_id) {
            $this->flash->error('参数不正确');
        }
        $survey = AddonVoteSurvey::findFirst('id=' . $survey_id)->toArray();
        $parm = $_POST;
        if (!empty($parm) && $survey) {
            if (AddonVoteSurveyLog::count('user_id=' . $this->checkLogin() . " AND vote_id=" . $survey_id) >= 1) {
                return $this->response->setContent("<script>if(confirm('非常抱歉,您已经填写过问卷调查了,谢谢合作')){window.location.href='/addon/survey'}</script>")->send();
            }


            $option = @array_diff($parm, array('survey_id' => $survey_id));
            $question = array();
            foreach ($option as $key => $item) {

                $question_ex = explode('_', $key);
                $question_id = array_pop($question_ex);

                if (is_array($item)) //checkbox
                {

                    foreach ($item as $val) {
                        $question[$question_id][] = $val;
                        $res = $this->modelsManager->createQuery(" update \\Models\\Modules\\VoteSurvey\\AddonVoteSurveyOption set option_count=option_count+1 WHERE id=" . $val)
                            ->execute();
                    }
                } else //radio
                {
                    $question[$question_id][] = $item;
                    $res = $this->modelsManager->createQuery(" update \\Models\\Modules\\VoteSurvey\\AddonVoteSurveyOption set option_count=option_count+1 WHERE id=" . $item)
                        ->execute();
                }
            }
            $log = new AddonVoteSurveyLog();
            $log->vote_id = $survey_id;
            $log->customer_id = CUR_APP_ID;
            $log->vote_topic = $survey['topic'];
            $log->user_id = $this->checkLogin();
            $log->vote_result = serialize($question);
            $log->vote_time = time();
            $log->type = 'survey';
            if ($log->save()) {
                return $this->response->setContent("<script>if(confirm('恭喜,您已经成功的提交了您的问卷调查')){window.location.href='/addon/survey'}</script>")->send();
            }
        }

    }

    public function beforeRender()
    {
        $this->view->title = '问卷调查 - ' . SiteData::init()->getSite('site_name');
    }

}


