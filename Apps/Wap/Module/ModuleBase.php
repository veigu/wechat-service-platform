<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-10
 * Time: 下午1:59
 */

namespace Multiple\Wap\Module;


use Components\ModuleManager\ModuleManager;
use Components\TplManager;
use Models\Customer\CustomerPackage;
use Models\Modules\Resources;
use Multiple\Wap\Controllers\Base\MultiTplBase;

class ModuleBase extends MultiTplBase
{
    protected $_module = '';

    public function initialize()
    {
        parent::initialize();;
        TplManager::$_config['hideFooter'] = true;
        TplManager::$_config['hideHeader'] = true;
        // set cache lib
        $this->_cache = $this->view->_cache = $cache = $this->getDI()->get('memcached');

        $css_dir = 'module';
        $view_base = MODULE_PATH . '/Views';
        $this->_module = $this->dispatcher->getControllerName();

        if (!$this->checkModule()) {
            return;
        }

        if (in_array($this->tpl['tpl_mode'], array('customize', 'kit'))) {
            $customize = json_decode($this->tpl[$this->tpl['tpl_mode']], true);
            $customize_dir = isset($customize['serial_number']) && $customize['serial_number'] ? $customize['serial_number'] : '';
            if ($customize_dir) {
                $view_tmp = $view_base . '/' . $this->tpl['tpl_mode'] . '/' . $customize_dir;
                if (is_dir($view_tmp . '/module/' . $this->_module)) {
                    $view_base = $view_tmp . '/module/';
                    $css_dir = $this->tpl['tpl_mode'] . '/' . $customize_dir;
                } else {
                    $view_base .= "/module";
                }
            } else {
                $view_base .= "/module";
            }
        } else {
            $view_base .= "/module";
        }
        $this->view->setViewsDir($view_base);
        $this->getModuleCss($css_dir);
    }

    public function getModuleCss($tpl)
    {
        $base_dir = $tpl ? $tpl : 'module';
        $this->_css[] = trim($base_dir) . '/' . $this->_module . '.css';
        $this->initCss();
    }

    protected function checkModule()
    {
        $module_name = $this->_module;
        // 商家是否购买
        $module_info = ModuleManager::instance(HOST_KEY, CUR_APP_ID)->getCustomerModuleInfo($module_name);
        if ($module_info) {
            $this->view->title = $module_info['resource']['name'];
            return $module_info['resource'];
        }

        // 商家是否拥有
        $package = CustomerPackage::findFirst("customer_id=" . CUR_APP_ID);
        if (!$package) {
            return false;
        }
        $package = $package->toArray();
        if ($package['package'] == 'custom') {
            if (!in_array($this->_module, json_decode($package['own_module'], true))) {
                $this->err('404', '模块尚未授权！');
                $this->view->title = "模块尚未授权";
                return false;
            }

        } else {
            if (in_array($this->_module, json_decode($package['limit_module'], true))) {
                $this->err('404', '模块尚未授权~！');
                $this->view->title = "模块尚未授权";
                return false;
            }

        }

        // 查找系统
        $module = Resources::findFirst('module_name="' . $module_name . '"');
        if (!$module) {
            $this->err('404', '模块不存在！');
            $this->view->title = "模块不存在";
            return false;
        }

        $module = $module->toArray();
        $this->view->title = $module['name'];
        return $module;
    }
} 