<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 6/25/14
 * Time: 1:11 AM
 */

namespace Multiple\Wap\Module;


use Models\Modules\AddonPanorama;
use Multiple\Wap\Helper\Meta;
use Util\EasyEncrypt;

class PanoramaController extends ModuleBase
{

    public function indexAction()
    {
        $list = AddonPanorama::find(array(
            'customer_id=' . CUR_APP_ID . ' and published = 1',
            'order' => 'sort desc,created desc',
            'limit' => 5
        ));

        $this->view->setVar('list', $list ? $list->toArray() : '');
    }

    public function detailAction()
    {
        $id = $this->dispatcher->getParam(0);

        $id = EasyEncrypt::decode($id);
        if (!($id && is_numeric($id))) {
            // todo error
            return $this->err('404', '参数有误！');
        } else {
            $item = AddonPanorama::findFirst('published = 1 and customer_id = ' . CUR_APP_ID . ' and id=' . $id);
            if (!$item) {
                // todo error
                return $this->err('404', '全景图未找到或尚未发布！');
            } else {
                $item->update(array('views' => $item->views + 1));

                $item = $item->toArray();

                // meta
                $this->view->title = $item['title'] . ' - 360全景展示';
                $this->view->keywords = $item['title'] . ' - 360全景展示 - ' . Meta::$siteName;
                $this->view->description = $item['title'] . ' - 360全景展示 - ' . Meta::$siteName;

                $this->view->setVar('item', $item);
            }
        }
    }

    public function xmlAction()
    {
        $this->view->disable();
        $id = $this->dispatcher->getParam(0);
        $str = '';
        $id = EasyEncrypt::decode($id);
        if (!($id && is_numeric($id))) {
            // todo error
        } else {
            $item = AddonPanorama::findFirst('published = 1 and customer_id = ' . CUR_APP_ID . ' and id=' . $id);

            if (!$item) {
                // todo error
            } else {
                // 前右后左上下
                $item = $item->toArray();
                $url0 = $item['front'];
                $url1 = $item['right'];
                $url2 = $item['back'];
                $url3 = $item['left'];
                $url4 = $item['top'];
                $url5 = $item['down'];

                $str .= '<panorama id="">';
                $str .= '    <view fovmode="0" pannorth="0">';
                $str .= '        <start pan="5.5" fov="80" tilt="1.5"/>';
                $str .= '        <min pan="0" fov="80" tilt="-90"/>';
                $str .= '        <max pan="360" fov="80" tilt="90"/>';
                $str .= '    </view>';
                $str .= '    <userdata title="" datetime="" description="" copyright="estt" tags="" author="" source="" comment="" info="" longitude="" latitude=""/>';
                $str .= '    <hotspots width="180" height="20" wordwrap="1">';
                $str .= '        <label width="180" backgroundalpha="1" enabled="0" height="20" backgroundcolor="0xffffff" bordercolor="0x000000" border="1" textcolor="0x000000" background="1" borderalpha="1" borderradius="1" wordwrap="1" textalpha="1"/>';
                $str .= '        <polystyle mode="1" backgroundalpha="0.2509803921568627" backgroundcolor="0x0000ff" bordercolor="0x0000ff" borderalpha="1"/>';
                $str .= '    </hotspots>';
                $str .= '    <media/>';

                $str .= '    <input tilesize="500" tilescale="1.014285714285714"
                tile0url="' . $url0 . '"
                tile1url="' . $url1 . '"
                tile2url="' . $url2 . '"
                tile3url="' . $url3 . '"
                tile4url="' . $url4 . '"
                tile5url="' . $url5 . '"/>';
                $str .= '    <autorotate speed="0.200" nodedelay="0.00" startloaded="1" returntohorizon="0.000" delay="5.00"/>';
                $str .= '    <control simulatemass="1" lockedmouse="0" lockedkeyboard="0" dblclickfullscreen="0" invertwheel="0" lockedwheel="0" invertcontrol="1" speedwheel="1" sensitivity="8"/>';
                $str .= '</panorama>';
            }
        }

        header('Content-Type: text/xml');
        echo $str;
    }

}