<?php
namespace Multiple\Wap\Module;

use Components\Module\CouponManager;
use Components\Rules\PointRule;
use Components\UserStatus;
use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionOption;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\User\UserForCustomers;
use Models\User\UserPointLog;
use Multiple\Wap\Helper\SiteData;
use Components\UserStatusWap;
use Phalcon\Exception;
use Util\Ajax;
use Util\EasyEncrypt;

class ScratchController extends ModuleBase
{

    public function indexAction()
    {
        $user_id = $this->checkLogin();

        $scratch = AddonPromotion::find("code='scratch' AND customer_id='" . CUR_APP_ID . "'");
        $this->view->setVar('list',$scratch ? $scratch->toArray() :array());

    }
    public function detailAction()
    {
        $id=EasyEncrypt::decode($this->dispatcher->getParam(0));
        if(!$id || $id<0)
        {
            header("location:/addon/scratch/");
        }
        $user_id = $this->checkLogin();
        //开始时间小于等于当天，结束时间大于等于当天或者不限制
        $scratch = AddonPromotion::findFirst("code = 'scratch' AND customer_id = " . CUR_APP_ID." AND id=".$id);
        //奖品选项
        if (!empty($scratch)) {
            $scratchOptions = AddonPromotionOption::find("parent_id='" . $scratch->id . "' AND option_number>0");
            $this->view->setVar('scratchOptions', $scratchOptions);
            $this->view->setVar('scratch', $scratch);
//            $this->view->setVar('lotteryNumber', $this->_getLotteryNumber(UserStatus::getUid()));

            //$uid = UserStatus::getUid();
            $records = AddonPromotionPrize::query()
                ->where("user_id = ".$user_id." AND customer_id = '" . CUR_APP_ID . "' AND activity_id = '" . $scratch->id . "'")
                ->join("Models\\Modules\\Promotion\\AddonPromotionOption", "mo.option_id = Models\\Modules\\Promotion\\AddonPromotionPrize.prize_id", 'mo')
                ->execute();

            $this->view->setVar('record', $records);
            $this->view->setVar('lotteryNumber', count($records));
            $this->view->setVar('id',$id);
        }

    }
    public function beforeRender()
    {
        $this->view->title = '刮刮卡 - ' . SiteData::init()->getSite('site_name');
    }

}

