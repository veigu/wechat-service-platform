<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 7/10/14
 * Time: 5:06 PM
 */

namespace Multiple\Wap\Module;


use Models\Modules\Coupon\AddonCoupon;
use Models\Modules\Coupon\AddonCouponOfflineLog;
use Models\Modules\Coupon\AddonCouponOrderLog;
use Models\Modules\Coupon\AddonCouponReceive;
use Multiple\Wap\Controllers\ControllerBase;
use Multiple\Wap\Helper\SiteData;
use Components\UserStatusWap;
use Util\EasyEncrypt;

class CouponController extends ModuleBase
{
    public function indexAction()
    {
        $uid = UserStatusWap::init()->checkLogin();
        $where = 'customer_id=' . CUR_APP_ID . ' and published = 1 and deleted = 0 and receive_end>' . time();
        $list = AddonCoupon::find(array(
            $where,
            'order' => 'created desc',
            'limit' => 10
        ));

        $list = $list ? $list->toArray() : [];

        // get user coupon
        $receive_list = AddonCouponReceive::find(array('customer_id=' . CUR_APP_ID . ' and user_id=' . $uid, 'order' => 'used asc,created desc'));
        $receive_list = $receive_list ? $receive_list->toArray() : [];
        $receive_ids = [];
        if ($receive_list && $list) {
            $receive_ids = array_column($receive_list, 'coupon_id', 'id');

            foreach ($receive_list as &$v) {
                $coupon = AddonCoupon::findFirst('id=' . $v['coupon_id']);
                if ($coupon) {
                    $v['use_end'] = $coupon->use_end;
                    $v['name'] = $coupon->name;
                    $v['use_limit'] = $coupon->use_limit;
                    $v['coupon_type'] = $coupon->coupon_type;
                    $v['coupon_param'] = $coupon->coupon_param;
                    $v['use_limit'] = $coupon->use_limit;
                }
            }

        }

        $this->view->setVar('all_list', $list);
        $this->view->setVar('receive_ids', $receive_ids);
        $this->view->setVar('user_list', $receive_list);
    }

    public function beforeRender()
    {
        $this->view->title = '优惠劵 - ' . SiteData::init()->getSite('site_name');
    }


    public function detailAction()
    {
        $uid = UserStatusWap::init()->checkLogin();

        $coupon_id = $this->check();
        if (!$coupon_id) {
            return;
        }

        $item = AddonCoupon::findFirst("customer_id = " . CUR_APP_ID . ' and published = 1 and deleted = 0  and id=' . $coupon_id);
        if (!$item) {
            $this->err('400', '优惠劵不存在！');
            return;
        }

        $own = AddonCouponReceive::findFirst("customer_id = " . CUR_APP_ID . ' and coupon_id=' . $coupon_id . ' and user_id=' . $uid);

        $this->view->setVar('item', $item->toArray());
        $this->view->setVar('own', $own ? $own->toArray() : []);
        $this->view->title = '优惠劵详情';
    }

    public function mineAction()
    {
        $uid = UserStatusWap::init()->checkLogin();

        $receive_id = $this->check();
        if (!$receive_id) {
            return;
        }

        $own = AddonCouponReceive::findFirst("customer_id = " . CUR_APP_ID . ' and id=' . $receive_id . ' and user_id=' . $uid);
        if (!$own) {
            $this->err('400', '发放的优惠劵不存在！');
            return;
        }

        $own = $own->toArray();
        $item = AddonCoupon::findFirst('id=' . $own['coupon_id']);

        //
        $use_log = [];
        if ($own['used']) {
            if ($own['used_offline']) {
                $use_log = AddonCouponOfflineLog::findFirst('serial_number="' . $own['serial_number'] . '"');
                $use_log = $use_log ? $use_log->toArray() : [];
            } else {
                $use_log = AddonCouponOrderLog::findFirst('coupon_serial="' . $own['serial_number'] . '"');
                $use_log = $use_log ? $use_log->toArray() : [];
            }
        }

        $this->view->setVar('item', $item ? $item->toArray() : []);
        $this->view->setVar('own', $own);
        $this->view->setVar('use_log', $use_log);
        $this->view->title = '我的优惠劵详情';
    }

    public function offlineAction()
    {
        $serial = $this->dispatcher->getParam(0);
        if (!$serial || strlen($serial) != 16) {
            $this->err('400', '错误的参数！');
            return;
        }

        $receive = AddonCouponReceive::findFirst('customer_id=' . CUR_APP_ID . ' and serial_number="' . $serial . '"');
        if (!$receive) {
            $this->err('400', '发放的优惠劵不存在！');
        }

        $item = AddonCoupon::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $receive->coupon_id);

        $this->view->setVar('own', $receive->toArray());
        $this->view->setVar('item', $item->toArray());
        $this->view->title = '商家确认';
    }


    private function check()
    {
        $id = $this->dispatcher->getParam(0);
        $id = EasyEncrypt::decode($id);

        if (is_numeric($id)) {
            return $id;
        }
        $this->err('400', '优惠劵不存在~！');
        return false;
    }
} 