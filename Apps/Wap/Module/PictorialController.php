<?php
namespace Multiple\Wap\Module;

use Models\Modules\Pictorial\AddonPictorial;
use Models\Modules\Pictorial\AddonPictorialOption;
use Phalcon\Tag;

class PictorialController extends ModuleBase
{

    /*微画报汇总页*/
    public function indexAction()
    {
        $this->view->title = '微画报';
        $time = time();
        $list = AddonPictorial::find(array(
            'customer_id=' . CUR_APP_ID,
            'order' => 'created desc',
        ));
        $list = $list ? $list->toArray() : '';
        if (!$list) {
            $this->err('404', '暂无画报内容！');
            return;
        }
        $this->view->setVar('list', $list);
    }

    /*微画报详情页*/
    public function pictorialAction()
    {
        $id = \Util\EasyEncrypt::decode($this->dispatcher->getParam(0));
        if (!$id || !is_numeric($id)) {
            $this->err('404', '暂无画报内容！');
            return;
        }
        $pictorial = AddonPictorial::findFirst('customer_id=' . CUR_APP_ID . ' AND id=' . $id);
        if (!$pictorial) {
            $this->err('404', '暂无画报内容！');
            return;
        }
        $pictorial = $pictorial->toArray();
        $this->view->title = '微画报-' . $pictorial['name'];

        $option = AddonPictorialOption::find(array(
            'customer_id=' . CUR_APP_ID . ' AND pictorial_id=' . $id,
            'order' => 'id desc',
        ));
        $this->view->setVar('pictorial', $pictorial);
        $this->view->setVar('list', $option ? $option->toArray() : '');
    }

}