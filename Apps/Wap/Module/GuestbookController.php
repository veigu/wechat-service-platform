<?php

namespace Multiple\Wap\Module;

use Models\Modules\Guestbook\AddonGuestbook;
use Models\Modules\Guestbook\AddonGuestbookSettings;
use Multiple\Wap\Helper\SiteData;

class GuestbookController extends ModuleBase
{
    public function indexAction()
    {
        $messageModel = new AddonGuestbook();
        $messages = $messageModel->getMessagesByCustomerId(CUR_APP_ID, AddonGuestbook::STATUS_PASS, 10);
        $this->view->setVar('messages', $messages);
        $this->view->setVar('baseUri', '/addon');
    }


    public function writeAction()
    {
        $user_id = $this->checkLogin();
        if ($this->request->isPost()) {
            $this->view->disable();
            $setting = AddonGuestbookSettings::findFirst('customer_id=' . CUR_APP_ID);
            $needCheck = true;
            if ($setting) {
                $settingParam = @unserialize($setting->params);
                $needCheck = boolval($settingParam['examine']);
            }
            $message = $this->request->getPost('message', 'striptags');
            if (strlen($message) < 5) {
                $result = array('error' => true, 'msg' => '留言至少5个字');
				return $this->response->setJsonContent($result)->send();
            }

            $data = array(
                'user_id' => $user_id,
                'customer_id' => CUR_APP_ID,
                'message' => $message,
                'status' => $needCheck ? AddonGuestbook::STATUS_BLOCK : AddonGuestbook::STATUS_PASS,
                'created' => date('Y-m-d H:i:s'),
                'reply' => ''
            );
            try {
                $message = new AddonGuestbook();
                if (!$message->save($data)) {
                    $msgs = [];
                    foreach ($message->getMessages() as $m) {
                        $msgs[] = (string)$m;
                    }
                    throw new \Phalcon\Exception("留言发布失败！" . join(',', $msgs));
                }
                $result = array('succeed' => true, 'msg' => ($needCheck ? '发表成功，管理员正在审核！' : '发表成功！'));
                return $this->response->setJsonContent($result)->send();

            } catch (\Exception $e) {
                $result = array('error' => true, 'msg' => $e->getMessage());
                return $this->response->setJsonContent($result)->send();
            }
        }
    }


    public function beforeRender()
    {
        $this->view->title = '留言板 - ' . SiteData::init()->getSite('site_name');
    }
}
