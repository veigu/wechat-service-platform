<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 8/1/14
 * Time: 9:23 AM
 */

namespace Multiple\Wap\Module;


use Models\Modules\Store\AddonStore;
use Multiple\Wap\Helper\Meta;
use Multiple\Wap\Helper\SiteData;
use Util\EasyEncrypt;

class StoreController extends ModuleBase
{
    public function indexAction()
    {
    }

    public function detailAction()
    {
        $id = $this->dispatcher->getParam(0);

        $id = EasyEncrypt::decode($id);
        if (!($id && is_numeric($id))) {
            // todo error
            $this->err('404', '参数有误！');
            return;
        } else {
            $item = AddonStore::findFirst('published = 1 and customer_id = ' . CUR_APP_ID . ' and id=' . $id);
            if (!$item) {
                // todo error
                $this->err('404', '门店未找到或尚未发布！');
                return;
            } else {
                $item->update(array('views' => $item->views + 1));

                $item = $item->toArray();

                // meta
                $this->view->title = $item['name'] . ' - LBS门店信息';
                $this->view->keywords = $item['name'] . ' - LBS门店信息 - ' . Meta::$siteName;
                $this->view->description = $item['name'] . ' - LBS门店信息 - ' . Meta::$siteName;

                $this->view->setVar('item', $item);
            }
        }
    }

    public function mapAction()
    {
        $id = $this->dispatcher->getParam(0);

        $id = EasyEncrypt::decode($id);
        if (!($id && is_numeric($id))) {
            // todo error
            $this->err('404', '参数有误！');
            return;
        } else {
            $item = AddonStore::findFirst('published = 1 and customer_id = ' . CUR_APP_ID . ' and id=' . $id);
            if (!$item) {
                // todo error
                $this->err('404', '门店未找到或尚未发布！');
                return;
            } else {
                $item->update(array('views' => $item->views + 1));

                $item = $item->toArray();

                // meta
                $this->view->title = $item['name'] . ' - LBS门店导航';
                $this->view->keywords = $item['name'] . ' - LBS门店导航 - ' . Meta::$siteName;
                $this->view->description = $item['name'] . ' - LBS门店导航 - ' . Meta::$siteName;

                $this->view->setVar('item', $item);
            }
        }
    }

    public function beforeRender()
    {
        $this->view->title = 'LBS门店 - ' . SiteData::init()->getSite('site_name');
    }

} 