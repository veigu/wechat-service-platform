<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 7/10/14
 * Time: 5:06 PM
 */

namespace Multiple\Wap\Module;


use Components\Order\OrderGenerator;
use Components\Product\OrderManager;
use Components\Product\ProductManager;
use Components\UserStatus;
use Models\Modules\Pointmall\AddonPointMallItem;
use Models\Modules\Pointmall\AddonPointMallOrder;
use Models\Product\Product;
use Models\Product\ProductComment;
use Models\User\UserAddress;
use Multiple\Wap\Helper\ShopData;
use Util\Ajax;
use Util\Cookie;
use Util\EasyEncrypt;

class PointmallController extends ModuleBase
{

    public function indexAction()
    {
        $list = AddonPointMallItem::find(array(
            'customer_id=' . CUR_APP_ID . ' and published = 1',
            'order' => 'sort desc,created desc',
            'limit' => 10
        ));

        $list = $list ? $list->toArray() : [];
        $res = [];

        if ($list) {
            foreach ($list as &$item) {
                $info = Product::findFirst('id=' . $item['item_id'] . ' and customer_id=' . CUR_APP_ID);
                if ($info) {
                    $item['item'] = $info->toArray();
                    $res[] = $item;
                }
            }
        }

        $this->view->setVar('list', $res);
    }

    public function itemAction()
    {
        $item = ShopData::init()->getItem();
        if ($item) {
            $page = $this->request->get('p');
            $rank = $this->request->get('r');
            if ($rank >= 0 && $rank <= 2) {
                $rank = $rank + 0;
            } else {
                $rank = 3;
            }

            $where = "product_id='" . $item['id'] . "' AND is_show='1'";
            $count = ProductComment::count($where);

            $curpage = $page <= 0 ? 0 : $page - 1;
            $limit = 3;
            $res = $this->modelsManager->createBuilder()
                ->addFrom('\\Models\\Product\\ProductComment', 'pc')
                ->leftJoin('\\Models\\User\\Users', 'pc.user_id=u.id', 'u')
                ->andWhere("pc.product_id=" . $item['id'] . " and pc.is_show=1")
                ->columns('pc.id,pc.content,pc.created,pc.rank,u.username')
                ->orderBy('pc.created DESC')
                ->getQuery()
                ->execute();

            $res = $res ? $res->toArray() : [];
            foreach ($res as $k => $v) {

                $son = $this->modelsManager->createBuilder()
                    ->addFrom('\\Models\\Product\\ProductComment', 'pc')
                    ->leftJoin('Models\Customer\Customers', 'pc.user_id=u.id', 'u')
                    ->andWhere("pc.product_id=" . $item['id'] . " AND pc.parent_id <> 0 AND pc.parent_id='" . $v['id'] . "'")
                    ->columns('pc.id,pc.content,pc.created,pc.rank,u.name')
                    ->orderBy('pc.created DESC')
                    ->getQuery()
                    ->execute();

                if ($son) {
                    $res[$k]['sonComment'] = $son->toArray();
                } else {
                    $res[$k]['sonComment'] = [];
                }
            }

            // $res = ProductComment::find(array($where, "order" => "created desc", "limit" => $curpage * $limit . "," . $limit));

            //  Pagination::instance($this->view)->showPage($page, $count, $limit);

            $this->view->setVar('list', $res);


        }
        // 避免结算页面重复
        Cookie::set('_cart_settle_page', EasyEncrypt::encode($this->uri->baseUrl('/addon/pointmall/item')));
    }

    public function countOrderAction()
    {
        $this->view->disable();
        $this->ajax = new Ajax();

        if (!UserStatus::getUid()) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }

        $uid = UserStatus::getUid();

        $count = AddonPointMallOrder::count('customer_id= ' . CUR_APP_ID . 'user_id=' . $uid);

        return Ajax::init()->outRight('', $count);
    }

    /**
     * 订单结算
     *
     * @return bool
     */
    public function settleAction()
    {
        $this->view->nocache = true;

        $user_id = $this->checkLogin();
        if (!$user_id) {
            return;
        }
        // 判断是从支付页面过来的
        if (Cookie::get('_from_settle')) {
            // 最后删除
            Cookie::del('_from_settle');
        }

        // 只允许从商品页和购物车页来
        // 判断来源
        if (!(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'])) {
            $this->err('400', '请从积分商城选择兑换商品1');
            return;
        }

        // 设置订单号
        $order_number = OrderGenerator::init()->generateOrderNumber();
        $this->session->set('_order_user_' . $user_id, $order_number);

        $all = $this->dispatcher->getParam(0);
        $cart = json_decode(base64_decode($all), true);
        if (!$cart) {
            $this->err('402', '错误的来源页面');
            return;
        }

        $point_item = AddonPointMallItem::findFirst('customer_id = ' . CUR_APP_ID . ' and item_id = ' . $cart['id']);
        if ($point_item) {
            $point_item = $point_item->toArray();
            $point_item['num'] = $cart['num'];
            if ($cart['spec']) {
                $point_item['spec_data'] = ProductManager::instance()->getProductSpecData($cart['id'], $cart['spec'], 'spec_data');
            }
            $point_item['total_paid'] = $point_item['cost_cash'] * $cart['num'];
            $point_item['total_point'] = $point_item['cost_point'] * $cart['num'];
        }

        $address = UserAddress::find('user_id=' . $user_id);
        $default_address = UserAddress::findFirst('is_default = 1 and user_id=' . $user_id);

        $this->view->setVar('cart', $cart);
        $this->view->setVar('item', $point_item);

        $this->view->setVar('default_addr', $default_address ? $default_address->toArray() : '');
        $this->view->setVar('address', $address ? $address->toArray() : '');
    }

    public function beforeRender()
    {
        $this->view->title = '积分换购';
    }
} 