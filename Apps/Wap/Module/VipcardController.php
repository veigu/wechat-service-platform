<?php

namespace Multiple\Wap\Module;

use Components\Module\VipcardManager;
use Components\Rules\PointRule;
use Models\Modules\Vipcard\AddonVipcardField;
use Models\Modules\Vipcard\AddonVipcardSettings;
use Models\Modules\Vipcard\AddonVipcardUsers;
use Models\User\Users;
use Multiple\Wap\Helper\EmoiUser;
use Multiple\Wap\Helper\Meta;
use Components\UserStatusWap;
use Phalcon\Escaper;


class VipcardController extends ModuleBase
{
    /**
     * @var VipcardManager
     */
    protected $vipcard_manager = null;
    public function initialize() {
        parent::initialize();
        $this->vipcard_manager = VipcardManager::init();
    }
    public function checkSetting()
    {
        $setting = VipcardManager::getCustomerSetting(CUR_APP_ID);
        if (!(isset($setting['enable']) && $setting['enable'])) {
            $this->err('400', '商家尚未开启会员卡功能');
            return false;
        }
        return $setting;
    }

    /**
     * 领取会员卡
     *
     * @access public
     * @return void
     */
    public function indexAction()
    {
        $setting = $this->checkSetting();

        if (!$setting) {
            return;
        }

        Meta::$head_title['vipcard_index'] = "领取会员卡";

        $user_id = $this->checkLogin();
        $localUser = UserStatusWap::init()->getUserInfo();

        $card = VipcardManager::init()->getCardInfoByPhone($localUser['phone'], CUR_APP_ID, false);

        if (isset($card['is_active']) && $card['is_active'] == 1) {
            $this->response->redirect($this->uri->wapModuleUrl('vipcard/mine'))->send();
            return;
        }

        //查看商家有没有设置会员卡
        $card_s = VipcardManager::getCustomerCard(CUR_APP_ID);

        if (!$card_s) {
            $this->err('400', '商家尚未启用会员卡功能');
            return;
        }

        $fields = AddonVipcardField::find('customer_id=' . CUR_APP_ID);
        $card_s['setting'] = $setting;
        $this->view->setVar('card', $card_s);
        $this->view->setVar('vipcard', $card);

        $this->view->setVar('fields', $fields ? $fields->toArray() : []);

    }

    /**
     * 会员卡首页
     *
     * @access public
     * @return void
     */
    public function mineAction()
    {
        $user_id = $this->checkLogin();
        $this->view->title = '会员卡';
        $localUser = UserStatusWap::init()->getUserInfo();

        $card = VipcardManager::getCardInfoByPhone($localUser['phone'], CUR_APP_ID);
       // echo "<pre>";
       // var_dump($card);exit;
        if (!$card || (isset($card['is_active']) && $card['is_active'] == 0)) {
            //还没有会员卡
            $this->response->redirect($this->uri->wapModuleUrl('vipcard'))->send();
        }

      /*  // todo sync emoi user to local
        if ($_SERVER['HTTP_HOST'] == 'm.emoi.cn' || $_SERVER['HTTP_HOST'] == 'wap.emoi.cn') {
            EmoiUser::instance()->syncUserToLocal($card['phone'], $card['user_id']);
        }*/
        $card['card_info'] = VipcardManager::getCustomerCard(CUR_APP_ID);

        $user = Users::findFirst('id=' . $user_id);

        //是否已经签到

        $isSignin = VipcardManager::isCheckIn($user_id, CUR_APP_ID);
        $user = $user->toArray();
        $user['isSignin'] = $isSignin;

        $this->view->setVar('user', $user);
        $this->view->setVar('card', $card);
    }

    public function bindAction()
    {
        UserStatusWap::init()->logout();
    }

    /**
     * 会员卡说明
     *
     * @access public
     * @return void
     */
    public function introAction()
    {
        Meta::$head_title['vipcard_intro'] = "会员卡说明";

        $card = VipcardManager::getCustomerCard(CUR_APP_ID);

        $setting = AddonVipcardSettings::findFirst("customer_id = " . CUR_APP_ID);

        $this->view->setVar('card', $card);

        $this->view->setVar("option", $setting);
    }

    /**
     * 会员卡积分
     *
     * @access public
     * @return void
     */
    public function scoreAction()
    {

        $user_id = $this->checkLogin();

        Meta::$head_title['vipcard_score'] = "会员卡积分";

        $card = VipcardManager::getCardInfoByUid($user_id, CUR_APP_ID);
        if (!$card || (isset($card) && $card['is_active'] == 0)) {
            //还没有会员卡
            $this->response->redirect($this->uri->wapModuleUrl('vipcard'));
        }

        $logs = VipcardManager::getPointLog($user_id, CUR_APP_ID);

        $this->view->setVar('logs', $logs);

        $this->view->setVar('card', $card);
    }

    //会员卡设置
    public function settingAction()
    {
        $setting = $this->checkSetting();

        if (!$setting) {
            return;
        }

        Meta::$head_title['vipcard_index'] = "领取会员卡";

        $user_id = $this->checkLogin();
        $localUser = UserStatusWap::init()->getUserInfo();

        $card = VipcardManager::init()->getCardInfoByPhone($localUser['phone'], CUR_APP_ID, false);

        //查看商家有没有设置会员卡
        $card_s = VipcardManager::getCustomerCard(CUR_APP_ID);

        if (!$card_s) {
            $this->err('400', '商家尚未会员卡');
            return;
        }

        $fields = AddonVipcardField::find('customer_id=' . CUR_APP_ID);
        $card_s['setting'] = $setting;
        $this->view->setVar('card_setting', $card_s);
        $this->view->setVar('vipcard', $card);

        $this->view->setVar('fields', $fields ? $fields->toArray() : []);

    }

    public function profileAction() {
        $setting = $this->checkSetting();

        if (!$setting) {
            return;
        }
        $localUser = UserStatusWap::init()->getUserInfo();

        $card = VipcardManager::init()->getCardInfoByPhone($localUser['phone'], CUR_APP_ID, false);

        //查看商家有没有设置会员卡
        $card_s = VipcardManager::getCustomerCard(CUR_APP_ID);

        if (!$card_s) {
            $this->err('400', '商家尚未会员卡');
            return;
        }

        $fields = AddonVipcardField::find('customer_id=' . CUR_APP_ID);
        $card_s['setting'] = $setting;
        $this->view->setVar('card', $card_s);
        $this->view->setVar('user', $card);

        $this->view->setVar('fields', $fields ? $fields->toArray() : []);
    }

    public function beforeRender()
    {
        $this->view->title = '会员卡';
    }

}
