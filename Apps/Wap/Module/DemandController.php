<?php

namespace Multiple\Wap\Module;

use Components\Module\DemandManager;
use Models\Modules\Demand\AddonDemand;
use Models\Modules\Demand\AddonDemandReply;
use Multiple\Wap\Helper\SiteData;
use Components\UserStatusWap;
use Util\Ajax;
use Util\EasyEncrypt;

class DemandController extends ModuleBase
{
    public function indexAction()
    {
        $uid = UserStatusWap::init()->checkLogin();

        $page = $this->request->get('p');
        $t = intval($this->request->get('t'));
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 20;
        $key = $this->request->get('key');

        $where = 'customer_id=' . CUR_APP_ID . ' and user_id= ' . $uid;

        if ($key) {
            $where .= ' and title like "%' . $key . '%"';
        }

        if ($t) {
            $where .= ' and type_id=' . $t;
        }

        $list = AddonDemand::find(array(
            $where,
            'order' => 'created desc',
            'limit' => $curpage * $limit . "," . $limit
        ));

        $count = AddonDemand::count($where);

        $this->view->setVar('list', $list ? $list->toArray() : '');
        $this->view->setVar('count', $count);
    }

    public function detailAction()
    {
        $uid = UserStatusWap::init()->checkLogin();
        $where = 'customer_id=' . CUR_APP_ID . ' and user_id= ' . $uid;
        $count = AddonDemand::count($where);

        $reserve_id = $this->check();
        if (!$reserve_id) {
            return;
        }

        $this->checkLogin();

        $item = AddonDemand::findFirst("customer_id = " . CUR_APP_ID . ' and id=' . $reserve_id);
        if (!$item) {
            $this->err('400', '商家预约不存在！');
            return;
        }
        $this->view->setVar('item', $item->toArray());
        $this->view->setVar('count', $count);
    }

    private function check()
    {
        $id = $this->dispatcher->getParam(0);
        $id = EasyEncrypt::decode($id);

        if (is_numeric($id)) {
            return $id;
        }
        $this->err('400', '需求未找到~！');
        return false;
    }


    public function beforeRender()
    {
        $this->view->title = '需求咨询 - ' . SiteData::init()->getSite('site_name');
    }
}
