<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 6/25/14
 * Time: 1:11 AM
 */

namespace Multiple\Wap\Module\Api;


use Models\Modules\AddonPanorama;
use Multiple\Wap\Api\ApiBase;

class PanoramaController extends ApiBase
{
    public function moreAction()
    {
        $page = $this->request->get('page');

        $list = AddonPanorama::query()
            ->where("published = 1 AND customer_id = " . CUR_APP_ID)
            ->limit(5, 5 * $page)
            ->orderBy("sort desc,created desc")
            ->execute();

        $data = array();

        if (count($list) > 0) {
            foreach ($list as $item) {
                $item = $item->toArray();
                $date = date('Y年m月d日', $item['created']);
                $html = <<<EOF
<li class="item">
    <a href="{$this->uri->wapModuleUrl('panorama/detail/' . \Util\EasyEncrypt::encode($item['id']))}">
        <p class="title">{$item['title']}</p>

        <p class="date"><span
                class="right">浏览 {$item['views']}
                次</span>{$date}
        </p>

        <p class="cover">
            <img src="{$item['front']}" alt=""/>
        </p>

        <p class="detail">{$item['detail']}</p>
    </a>
</li>
EOF;


                $data[] = array(
                    'html' => $html
                );
            }
        }

        // ajax
        $this->view->setVar('data', $data);
    }
}