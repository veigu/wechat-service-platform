<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-18
 * Time: 下午8:41
 */

namespace Multiple\Wap\Module\Api;


use Models\Modules\Guestbook\AddonGuestbook;
use Multiple\Wap\Api\ApiBase;

class GuestbookController extends ApiBase
{
    public function moreAction()
    {
        $this->view->disable();
        $page = $this->request->get('page', 'int');

        $messages = AddonGuestbook::query()
            ->where("Models\Modules\Guestbook\AddonGuestbook.customer_id = '" . CUR_APP_ID . "'")
            ->leftJoin('Models\\User\\Users', 'ufc.id = Models\\Modules\\Guestbook\\AddonGuestbook.user_id', 'ufc')
            ->columns("Models\Modules\Guestbook\AddonGuestbook.id,ufc.username, Models\Modules\Guestbook\AddonGuestbook.customer_id, Models\Modules\Guestbook\AddonGuestbook.user_id , Models\Modules\Guestbook\AddonGuestbook.message, Models\Modules\Guestbook\AddonGuestbook.reply, Models\Modules\Guestbook\AddonGuestbook.status, Models\Modules\Guestbook\AddonGuestbook.created, IFNULL(ufc.username,'')")
            ->limit(10, 10 * $page)
            ->orderBy('Models\Modules\Guestbook\AddonGuestbook.created desc')
            ->execute();
        $data = array();
        if (count($messages) > 0) {

            foreach ($messages as $msg) {
                $html = "<li>";
                $html .= "<dl>";
                $html .= "<dt>";
                $html .= $msg->username;
                $html .= "<span>$msg->created</span>";
                $html .= "</dt><dd>$msg->message";
                if (!empty($msg->reply)) {
                    $html .= "<div class=\"reply\">回复：$msg->reply</div>";
                }
                $html .= "</dd></dl></li>";
                $data[] = array(
                    'html' => $html
                );
            }
        }

        // ajax
        $this->view->setVar('data', $data);
    }
} 