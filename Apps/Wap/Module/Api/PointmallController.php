<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-18
 * Time: 下午8:22
 */

namespace Multiple\Wap\Module\Api;


use Components\Order\OrderGenerator;
use Models\Modules\Pointmall\AddonPointMallItem;
use Models\Product\Product;
use Multiple\Wap\Api\ApiBase;

class PointmallController extends ApiBase
{
    public function moreAction()
    {
        $this->view->disable();
        $page = $this->request->get('page');

        $list = AddonPointMallItem::query()
            ->where("published = 1 AND customer_id = " . CUR_APP_ID)
            ->limit(10, 10 * $page)
            ->orderBy("sort desc,created desc")
            ->execute();

        $data = array();

        if (count($list) > 0) {
            foreach ($list as $item) {
                $item = $item->toArray();
                $info = Product::findFirst('id=' . $item['item_id'] . ' and customer_id=' . CUR_APP_ID);
                if ($info) {
                    $info = $info->toArray();
                    $url = $this->uri->wapModuleUrl('pointmall/item/' . \Util\EasyEncrypt::encode($item['item_id']));;
                    $html = <<<EOF
                <li class="item">
                        <a href="{$url}">
                            <p class="cover">
                                <img src="{$info['thumb']}" alt=""/>
                            </p>
                            <p class="title">{$item['item_name']}</p>
                            <p style="color: #999;">
                                <s> 商品价格：  {$item['sell_price']}</s>
                            </p>
                            <p>
                                <span class="red">积分:{$item['cost_point']}</span>
                                <span class="orange">￥{$item['cost_cash']}</span>
                            </p>
                        </a>
                    </li>
EOF;
                    $data[] = array(
                        'html' => $html
                    );
                }
            }
        }

        // ajax
        $this->view->setVar('data', $data);
    }


    public function generateOrderAction()
    {
        OrderGenerator::init()->pointMallSettle();
    }

} 