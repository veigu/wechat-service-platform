<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 8/18/14
 * Time: 2:04 PM
 */

namespace Multiple\Wap\Module\Api;


use Components\Module\VipcardManager;
use Components\Rules\PointRule;
use Components\UserStatus;
use Models\Modules\Vipcard\AddonVipcardUsers;
use Multiple\Wap\Api\ApiBase;
use Multiple\Wap\Helper\EmoiUser;
use Components\UserStatusWap;
use Util\Ajax;

class VipcardController extends ApiBase
{
    public function applyAction()
    {
        $data = $this->request->get('data');
        //    Ajax::outError(Ajax::CUSTOM_ERROR_MSG,$data);
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        VipcardManager::init()->userApply($data);

       return Ajax::init()->outRight();
    }

    public function updateUserAction()
    {
        $uid = UserStatus::getUid();
        if (!$uid) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }
        $data = $this->request->getPost('data');
        if(isset($data['company_info']))  {
            $data['company_info'] = json_encode($data['company_info'], JSON_UNESCAPED_UNICODE);
        }

        $localUser = UserStatusWap::init()->getUserInfo();
        $user = AddonVipcardUsers::findFirst('customer_id=' . CUR_APP_ID . " AND phone='" . $localUser['phone'] . "'");
        $user->assign($data);
        if (!$user->update($data)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '数据更新失败！');
        }

        return Ajax::init()->outRight();
    }

    /**
     * ajaxcheckin
     *
     * @access public
     *
     */
    public function checkInAction()
    {
        $uid = UserStatus::getUid();
        if (!$uid) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }

        if (VipcardManager::isCheckIn($uid, CUR_APP_ID)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您已经签过到了！');
        }

        // 签到
        $grade = VipcardManager::init()->getUserVipGrade($uid, CUR_APP_ID);
        $pointRule = PointRule::init(CUR_APP_ID, $grade);

        $vipgrade = VipcardManager::getUserVipGrade($uid, CUR_APP_ID);
        $point= PointRule::init(CUR_APP_ID, $vipgrade)->executeRule($uid, PointRule::BEHAVIOR_VIPCARD_CHECK_IN);
        if ($point) {
            return Ajax::init()->outRight();
        } else {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, "签到失败！");
        }
        return Ajax::init()->outRight();

    }


}