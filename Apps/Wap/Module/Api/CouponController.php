<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-18
 * Time: 下午8:28
 */

namespace Multiple\Wap\Module\Api;


use Components\Module\CouponManager;
use Multiple\Wap\Api\ApiBase;

class CouponController extends ApiBase
{
    public function receiveAction()
    {
        $coupon_id = $this->request->get('coupon_id');
        CouponManager::init()->receiveCoupon($coupon_id);
    }

    public function offlineUseAction()
    {
        $serial = $this->request->get('serial');
        $pass = $this->request->get('passwd');
        CouponManager::init()->offlineUseConfirm($serial, $pass);
    }
} 