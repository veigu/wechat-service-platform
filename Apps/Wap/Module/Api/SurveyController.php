<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-9
 * Time: 下午4:37
 */

namespace Multiple\Wap\Module\Api;


use Models\Modules\VoteSurvey\AddonVoteSurvey;
use Models\Modules\VoteSurvey\AddonVoteSurveyOption;
use Models\Modules\VoteSurvey\AddonVoteSurveyQuestion;
use Multiple\Wap\Api\ApiBase;

class SurveyController extends ApiBase
{

    public function listAction()
    {
        $id = $this->request->get('id');
        $page = $this->request->get('page');
        $limit = 10;
        $opt = AddonVoteSurvey::findFirst('id=' . $id . " AND type='survey'");
        $this->view->setVar('survey', $opt ? $opt->toArray() : []);
        if ($opt) {
            $opt = $opt->toArray();
            $count = AddonVoteSurveyQuestion::count('vote_id=' . $id);

            $question = AddonVoteSurveyQuestion::find(
                array(
                    'vote_id=' . $id,
                    'order' => 'id',
                    "limit" => $limit * $page . ", " . $limit)
            );
            if ($question) {
                $question = $question->toArray();
                foreach ($question as $key => $val) {
                    $option = AddonVoteSurveyOption::find('question_id=' . $val['id']);
                    $question[$key]['option'] = $option ? $option->toArray() : [];
                }
            }
            $i = $limit * $page + 1;
            $data = array();
            foreach ($question as $item) {
                if (!empty($item['option'])) {
                    $html = "<li class='title'>" . $i . "、" . "" . $item['desc'] . "</li>";
                    foreach ($item['option'] as $itemo) {
                        $name = $item['type'] == 'radio' ? 'radio_' . $item['id'] : 'checkbox_' . $item['id'] . '[]';
                        $html .= " <li class='option_wrap'>";
                        $html .= "<input type='" . $item['type'] . "' value='" . $itemo['id'] . "' data-id='" . $item['id'] . "' id='option_" . $item['id'] . "' name='" . $name . "'/>";
                        $html .= " <span class='option' for='option_" . $item['id'] . "'>" . $itemo['option_name'] . "</span>";
                        $html .= '</li>';
                    }
                    $data[] = array(
                        'html' => $html
                    );
                    $i++;
                }
            }
            if ($question && $count <= $limit * $page + $limit) {
                $data[] = array('html' => ' <li class="overlinebox"><input type="submit" value="提交"  class="submit-box submit"></li>');
            }

            // ajax
            $this->view->setVar('data', $data);
        }

    }
} 