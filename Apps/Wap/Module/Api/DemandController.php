<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 10/10/2014
 * Time: 11:46 AM
 */

namespace Multiple\Wap\Module\Api;


use Components\Module\DemandManager;
use Components\UserStatus;
use Models\Modules\Demand\AddonDemand;
use Models\Modules\Demand\AddonDemandReply;
use Multiple\Wap\Api\ApiBase;
use Components\UserStatusWap;
use Util\Ajax;
use Util\EasyEncrypt;

class DemandController extends ApiBase
{


    public function replyAction()
    {
        $this->view->disable();

        $data = $this->request->getPost('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $demand = AddonDemand::findFirst('id=' . $data['demand_id'] . ' and customer_id=' . CUR_APP_ID);
        if (!$demand) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        if ($demand->status == DemandManager::DEMAND_STATUS_END) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '回复功能已经结束，不能提交！');
        }

        // 添加回复
        $data['created'] = time();
        $m = new AddonDemandReply();
        if (!$m->create($data)) {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '数据插入失败！');
        }

        // 当商家回复查看或回复过,重新更新状态为等待回复
        if ($demand->status != DemandManager::DEMAND_STATUS_DEFAULT) {
            $demand->update(array('status' => DemandManager::DEMAND_STATUS_WAIT));
        }

        return Ajax::init()->outRight();
    }


    public function addDemandAction()
    {
        $this->view->disable();

        $data = $this->request->getPost('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $data['created'] = time();
        $data['status'] = 0;
        $data['expected_time'] = strtotime($data['expected_time']);
        $data['customer_id'] = CUR_APP_ID;
        $data['user_id'] = UserStatus::getUid();
        $m = new AddonDemand();
        if (!$m->create($data)) {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '数据插入失败！');
        }
        $url = $this->uri->wapModuleUrl('demand/detail/' . EasyEncrypt::encode($m->id));
        return Ajax::init()->outRight('', $url);
    }

    public function endReplyAction()
    {
        $this->view->disable();
        $id = $this->request->getPost('id');
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $demand = AddonDemand::findFirst('id=' . $id . ' and customer_id=' . CUR_APP_ID);
        if (!$demand) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        // 更新状态为已经结束
        if (!$demand->update(array('status' => DemandManager::DEMAND_STATUS_END))) {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED);
        };

        return Ajax::init()->outRight();
    }
} 