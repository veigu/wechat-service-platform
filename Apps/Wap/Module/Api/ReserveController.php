<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-9
 * Time: 下午4:37
 */

namespace Multiple\Wap\Module\Api;


use Components\UserStatus;
use Models\Modules\Reserve\AddonReserve;
use Models\Modules\Reserve\AddonReserveOrders;
use Multiple\Wap\Api\ApiBase;
use Components\UserStatusWap;
use Util\Ajax;
use Util\EasyEncrypt;
use Util\Uri;

class ReserveController extends ApiBase
{
    public function submitOrderAction()
    {
        $this->view->disable();
        $this->ajax = new Ajax();

        // set uri
        $this->uri = $this->uri = new Uri();

        if (!UserStatus::getUid()) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }
        $user_id = UserStatus::getUid();

        $data = $this->request->getPost('data', 'string');
        $reserve_id = EasyEncrypt::decode($this->request->getPost('reserve_id'));
        $id = $this->request->getPost('id', 'string');

        if (!($data && $reserve_id)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if ($id) {
            $order_id = EasyEncrypt::decode($id);

            if (!$order_id) {
                return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
            }

            $order = AddonReserveOrders::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $order_id . ' and user_id=' . $user_id);

            if (!$order) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }

            $data['deleted'] = 0;
            if (!$order->update($data)) {

            }

            return Ajax::init()->outRight('订单修改成功！');

        } else {

            $reserve = AddonReserve::findFirst("id = " . $reserve_id);

            if (!$reserve) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }

            $data['reserve_id'] = $reserve_id;
            $data['customer_id'] = CUR_APP_ID;
            $data['user_id'] = $user_id;
            $data['created'] = time();
            $data['status'] = 0;
            $data['deleted'] = 0;


            $record = new AddonReserveOrders();

            if (!$record->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED);
            }
            return Ajax::init()->outRight('订单创建成功！');
        }
    }

    public function moreAction()
    {
        $this->view->disable();
        $page = $this->request->get('page');

        $list = AddonReserve::query()
            ->where("is_active = 1 AND customer_id = " . CUR_APP_ID)
            ->limit(5, 5 * $page)
            ->orderBy("created desc")
            ->execute();

        $data = array();

        if (count($list) > 0) {
            foreach ($list as $item) {
                $item = $item->toArray();
                $html = <<<EOF
<li class="item">
                    <a href="{$this->uri->wapModuleUrl('reserve/detail/' . \Util\EasyEncrypt::encode($item['id']))}">
                        <p class="title">{$item['name']}</p>

                        <p class="date">
                            预约/预订时间：{$item['start_date']} . ' -- ' . {$item['start_date']}
                        </p>

                        <p class="cover">
                            <img src="{$item['title_pic']}" alt=""/>
                        </p>

                        <p class="more">
                            电话：{$item['tel']}
                        </p>

                        <p class="more">
                            地址：{$item['address']}
                        </p>
                    </a>

                    <p class="go">
                        <a href="{$this->uri->wapModuleUrl('reserve/detail/' . \Util\EasyEncrypt::encode($item['id']))}"
                           class="btn btn-red ">立即预约</a>
                    </p>
                </li>
EOF;

                $data[] = array(
                    'html' => $html
                );
            }
        }
        return Ajax::init()->outRight('', $data);
    }

    public function cancelOrderAction()
    {
        $this->view->disable();
        $this->ajax = new Ajax();

        $user_id = UserStatus::getUid();
        if (!$user_id) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }

        $id = EasyEncrypt::decode($this->request->getPost('id'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $record = AddonReserveOrders::findFirst("user_id ={$user_id} and id = '{$id}'");
        if (!$record) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        $record->deleted = 1;
        $record->update();

        return Ajax::init()->outRight('订单修改成功！');
    }
}