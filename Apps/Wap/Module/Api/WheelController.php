<?php
/**
 * Created by PhpStorm.
 * User: luguiwu
 * Date: 14-11-28
 * Time: 下午6:28
 */

namespace Multiple\Wap\Module\Api;


use Components\Module\CouponManager;
use Components\Rules\PointRule;
use Components\UserStatus;
use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionOption;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\User\UserForCustomers;
use Models\User\UserPointLog;
use Multiple\Wap\Api\ApiBase;
use Util\Ajax;

class WheelController  extends ApiBase{
    /*
        * 经典的概率算法，
        * $proArr是一个预先设置的数组，
        * 假设数组为：array(100,200,300，400)，
        * 开始是从1,1000 这个概率范围内筛选第一个数是否在他的出现概率范围之内，
        * 如果不在，则将概率空间，也就是k的值减去刚刚的那个数字的概率空间，
        * 在本例当中就是减去100，也就是说第二个数是在1，900这个范围内筛选的。
        * 这样 筛选到最终，总会有一个数满足要求。
        * 就相当于去一个箱子里摸东西，
        * 第一个不是，第二个不是，第三个还不是，那最后一个一定是。
        * 这个算法简单，而且效率非常 高
        */
    protected function _getRand($proArr)
    {
        $result = '';
        //概率数组的总概率精度
        $proSum = array_sum($proArr);
        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset ($proArr);
        return $result;
    }

    protected function _getPrizeArray($parent_id)
    {
        //根据设置及用户中奖情况获取奖品数组
        $prizeArr = array();
        $sql = "SELECT mo.*, m.* FROM addon_promotion_option as mo LEFT JOIN addon_promotion as  m ON mo.parent_id=m.id
				WHERE m.customer_id=".CUR_APP_ID." AND m.code= 'wheel' and mo.parent_id=".$parent_id;
        //$result = $this->modelsManager->executeQuery($sql, array('cid' => CUR_APP_ID, 'code' => 'wheel'));
        $result=$this->db->query($sql)->fetchAll();
        $userPrize = new AddonPromotionPrize();
        foreach ($result as $row) {
            $countExists = $userPrize->count('prize_id=' . $row['option_id'] . ' AND start_time<=' . strtotime(date('Y-m-d')) . ' AND end_time>=' . strtotime(date('Y-m-d')) . ' AND customer_id=' . CUR_APP_ID);
            if ( $row['option_number'] > $countExists ||  $row['option_number'] == 0) {
                //该奖项还可以抽奖
                $prizeArr[ $row['option_id']] = array('id' =>  $row['option_id'], 'prize_level' =>  $row['option_name'], 'prize' =>  $row['option_desc'], 'prize_number' =>  $row['option_number'], 'angle' =>  $row['angle'], 'chance' => $row['chance']);
            }
        }
        return $prizeArr;
    }

    public function getPrizeAction()
    {
        $this->view->disable();
        $id=$this->request->get("id");
        if(!$id || $id<=0)
        {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'illegal params');

        }
        /*
         * 每次前端页面的请求，PHP循环奖项设置数组，
         * 通过概率计算函数_getRand获取抽中的奖项id。
         * 将中奖奖品保存在数组$res['yes']中，
         * 而剩下的未中奖的信息保存在$res['no']中，
         * 最后输出json个数数据给前端页面。
         */
        //初始化返回数据
        $data = array();

        //检查用户是否登录
        if (!UserStatus::getUid()) {
           // $data['error'] = 'invalid';
            //$data['errorMsg'] = '请先登录，再抽奖';
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'请先登录，再抽奖');
          //  return $this->response->setJsonContent($data)->send();
        }

        $uid = UserStatus::getUid();
        //大转盘的所有奖项
        $options = array();
        $marketing = AddonPromotion::findFirst('customer_id=' . CUR_APP_ID . ' and code="wheel" and id='.$id);
        $marketingOptions = AddonPromotionOption::find('parent_id=' . $marketing->id);
        if ($marketingOptions) {
            foreach ($marketingOptions as $item) {
                $options[] = $item->option_id;
            }
        }

        $userPrize = new AddonPromotionPrize();
        $userHistoryCount = $userPrize->count('user_id="' . $uid . '" AND prize_id in (' . implode(',', $options) . ')  AND start_time<="' . strtotime(date('Y-m-d')) . '" AND end_time>="' . strtotime(date('Y-m-d')) . '" AND customer_id="' . CUR_APP_ID . '"');
        if ($userHistoryCount >= $marketing->times) {
           // $data['error'] = 'invalid';
         //   $data['errorMsg'] = '您已经抽了' . $marketing->times . '次奖了！';
           // return $this->response->setJsonContent($data)->send();

            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'您已经抽了' . $marketing->times . '次奖了！');

        }


        $arr = array();
        $prizeArr = $this->_getPrizeArray($id);

        //var_dump($prizeArr);
        foreach ($prizeArr as $key => $val) {
            $arr[$val['id']] = $val['chance'];
        }

        $rid = $this->_getRand($arr); //根据概率获取奖项id
        $hitOption = $prizeArr[$rid]; //选中奖项

        //判断是否奖
        $code = ''; //初始化中奖码
        if ($hitOption['prize_number'] > 0) {
            //中奖啦,记录下来
            try {
                $code = $this->_getPrizeSn();
                $data['prize_sn'] = $code;
            } catch (Exception $e) {
                //var_dump($e);exit;
              //  $data['error'] = 'invalid';
              //  $data['errorMsg'] = '请先登录，再抽奖';
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'请先登录，再抽奖');
            }
        }

        //记录每次抽奖
        $userPrize->user_id = $uid;
        $userPrize->prize_id = $rid;
        $userPrize->code = $code;
        $userPrize->available = 1;
        $userPrize->activity_id = $marketing->id;
        $userPrize->created = date('Y-m-d H:i:s');
        $userPrize->customer_id = CUR_APP_ID;
        $userPrize->start_time = $marketing->start_time;
        $userPrize->end_time = $marketing->end_time;
        if (!$userPrize->save()) {
            $msg=array();
            foreach ($userPrize->getMessages() as $message) {
                $msg[] = $message;
            }

            $data['error'] = 'invalid';
            $data['errorMsg'] = join(',', $msg);
        }

        $data['success'] = true;
        $data['option'] = $hitOption;

       return Ajax::init()->outRight($data);
        //return $this->response->setJsonContent($data)->send();
    }

    /**
     * 生成中奖随机码
     * @return  string
     */
    function _getPrizeSn()
    {
        /* 选择一个随机的方案 */
        $code = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
        //查看随便码是否已经存在
        $existCount = AddonPromotionPrize::count('code ="' . $code . '"');
        if ($existCount > 0) {
            $code = $this->_getPrizeSn();
        }
        return $code;
    }

    //兑奖
    public function exchangeAction()
    {
        $this->view->disable();
        //检查用户是否登录
        if (!UserStatus::getUid()) {
            return Ajax::init()->outError(Ajax::ACT_WHEEL_CODE_NO_LOGIN, '你还没有登陆,请先登录再抽奖');
        }

        $uid = UserStatus::getUid();
        $code = $this->request->getPost('code');
        $id= $this->request->getPost('id');
        if(!$code || !$id || !is_numeric($id)|| $id <=0)
        {
            return   Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, 'illegal params');
        }
        $wheel=AddonPromotion::findFirst("id='".$id."'");
        if(!$wheel)
        {
            return   Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, 'illegal params');
        }
        if($wheel->exchange_time_limit>0)//兑奖时间有限制
        {
            if(strtotime(date('Y-m-d',strtotime("-".$wheel->exchange_time_limit." day")))>$wheel->end_time)
            {
                return   Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '该活动兑奖时间已过');
            }

        }
        $prize = AddonPromotionPrize::findFirst('user_id="' . $uid . '" AND code<>"" AND code="' . $code . '"');
        if ($prize) {
            if ($prize->available == 0) {
                return Ajax::init()->outError(Ajax::ACT_WHEEL_HAS_EXCHANGE, '此兑奖码已经兑换过了');
            } else {

                $option = AddonPromotionOption::findFirst('option_id=' . $prize->prize_id);
                if ($option->award_type == 'points') {
                    $point = new UserPointLog();
                    $point->user_id = $uid;
                    $point->in_out = 1;
                    $point->action = PointRule::BEHAVIOR_WHEEL;
                    $point->action_desc = "大转盘活动中奖";//PointRule::$behaviorNameMap[PointRule::BEHAVIOR_WHEEL];
                    $point->logged = time();
                    $point->value = $option->option_value;

                    $point->save();
                    $user = UserForCustomers::findFirst('customer_id=' . CUR_APP_ID . " AND user_id=" . $uid);
                    if ($user) {
                        $user->points = $option->option_value + intval($user->points);
                        $user->points_available = $user->points_available + intval($option->option_value);
                        $user->update();
                    }
                } else if ($option->award_type == 'coupon') {
                    CouponManager::init()->receiveCoupon($option->option_value);
                }
                $prize->available = 0;
//                $prize->user_prize_id = $prize->user_prize_id;
                $prize->save();
                return Ajax::init()->outRight('', '兑奖成功');
            }
        }
        return Ajax::init()->outError(Ajax::ACT_WHEEL_CODE_NOT_EXIST, '兑奖码不存在');
    }

} 