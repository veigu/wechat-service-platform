<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-18
 * Time: 下午8:02
 */

namespace Multiple\Wap\Module\Api;


use Models\Modules\Store\AddonStore;
use Multiple\Wap\Api\ApiBase;

class StoreController extends ApiBase
{

    public function getListAction()
    {
        $page = intval($this->request->get('page'));
        $lat = intval($this->request->get('lat'));
        $lng = intval($this->request->get('lng'));

        $where = "published = 1 AND customer_id = " . CUR_APP_ID;
        $columns = 'id,name,contact,address,lat,lng';
        $order = "created desc";
        if ($lat && $lng) {
            $columns .= ' , GETDISTANCE(lat, lng, ' . $lat . ', ' . $lng . ') AS distance';
            $order = "distance ASC,created desc";
        }
        $list = AddonStore::query()
            ->where($where)
            ->limit(8, 8 * $page)
            ->columns($columns)
            ->orderBy($order)
            ->execute();
//        echo $sql = 'SELECT *,GETDISTANCE(lat, lng, ' . $lat . ', ' . $lng . ') AS distance FROM store_subs WHERE ' . $where . ' ORDER BY distance ASC LIMIT ' . $limit;
//        echo ' < br>';
//        $res = $this->getDI()->get('db')->execute($sql);
//        print_r($res->toArray());
        $data = array();
        if (count($list) > 0) {
            foreach ($list as $item) {
                $item = $item->toArray();

                $tel_str = '';
                if ($item['contact']) {
                    $tels = explode('\n', $item['contact']);
                    if ($tels) {
                        foreach ($tels as $tel) {
                            $tel_str .= '<a href="tel:' . $tel . '">' . $tel . '</a>';
                        }
                    }
                }
                // 长度
                $long = isset($item['distance']) ? $this->LongFormat($item['distance']) : '0 km';


                $html = <<<EOF
                <li class="store">
                    <dl>
                        <dt class="store-name"><span class="right">{$long}</span>{$item['name']}</dt>
                        <dd>
                            <a href="{$this->uri->wapModuleUrl('store/map/' . \Util\EasyEncrypt::encode($item['id']))}">
                            <span class="right">
                                <i class="icon icon-angle-right"></i>
                            </span>
                                地址：
                                {$item['address']}
                            </a>
                        </dd>
                        <dd>
                            <span class="right">
                                <i class="icon icon-angle-right"></i>
                            </span>
                            电话：{$tel_str}
                        </dd>
                    </dl>
                </li>
EOF;
                $data[] = array(
                    'html' => $html
                );
            }
        }

        // ajax
        $this->view->setVar('data', $data);
    }

    //根据经纬度计算距离 其中A($lat1,$lng1)、B($lat2,$lng2)
    public static function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        //地球半径
        $R = 6378137;

        //将角度转为狐度
        $radLat1 = deg2rad($lat1);
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);

        //结果
        $s = acos(cos($radLat1) * cos($radLat2) * cos($radLng1 - $radLng2) + sin($radLat1) * sin($radLat2)) * $R;

        //精度
        $s = round($s * 10000) / 10000;

        return round($s);
    }

    public function  LongFormat($long)
    {
        if ($long > 1000) {
            return round($long / 1000) . "公里";
        } elseif ($long < 1000 && $long > 0) {
            return round($long) . "米";
        } else {
            return "旁边";
        }

    }
} 