<?php
/**
 * Created by PhpStorm.
 * User: luguiwu
 * Date: 14-12-1
 * Time: 下午12:45
 */

namespace Multiple\Wap\Module\Api;


use Components\Module\CouponManager;
use Components\Rules\PointRule;
use Components\UserStatus;
use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionOption;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\User\UserForCustomers;
use Models\User\UserPointLog;
use Multiple\Wap\Api\ApiBase;
use Util\Ajax;

class ScratchController extends ApiBase {

    /*
	 * 经典的概率算法，
	 * $proArr是一个预先设置的数组，
	 * 假设数组为：array(100,200,300，400)，
	 * 开始是从1,1000 这个概率范围内筛选第一个数是否在他的出现概率范围之内，
	 * 如果不在，则将概率空间，也就是k的值减去刚刚的那个数字的概率空间，
	 * 在本例当中就是减去100，也就是说第二个数是在1，900这个范围内筛选的。
	 * 这样 筛选到最终，总会有一个数满足要求。
	 * 就相当于去一个箱子里摸东西，
	 * 第一个不是，第二个不是，第三个还不是，那最后一个一定是。
	 * 这个算法简单，而且效率非常 高
	 */
    protected function _getRand($proArr)
    {
        $result = '';
        //概率数组的总概率精度
        $proSum = array_sum($proArr);
        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset ($proArr);
        return $result;
    }

    protected function _getPrizeArray($parent_id)
    {
        //根据设置及用户中奖情况获取奖品数组
        $prizeArr = array();
        $sql = "SELECT mo.*, m.* FROM addon_promotion_option as mo LEFT JOIN addon_promotion as  m ON mo.parent_id=m.id
				WHERE m.customer_id=".CUR_APP_ID." AND m.code= 'scratch' and mo.parent_id=".$parent_id;
      /*  $sql = "SELECT mo.*, m.* FROM Models\\Modules\\Promotion\\AddonPromotionOption as mo LEFT JOIN Models\\Modules\\Promotion\\AddonPromotion m ON mo.parent_id=m.id
				WHERE m.customer_id=:cid: AND m.code=:code: AND start_time<='" . strtotime(date('Y-m-d')) . "' AND end_time>='" . strtotime(date('Y-m-d')) . "'";*/
      //  $result = $this->modelsManager->executeQuery($sql, array('cid' => CUR_APP_ID, 'code' => 'scratch'));
        $result=$this->db->query($sql)->fetchAll();
        $userPrize = new AddonPromotionPrize();
        foreach ($result as $row) {
            $countExists = $userPrize->count('prize_id=' . $row['option_id'] . ' AND customer_id="' . CUR_APP_ID . '"');
            if ( $row['option_number'] > $countExists ||  $row['option_number'] == 0) {
                //该奖项还可以抽奖
                $prizeArr[ $row['option_id']] = array('id' =>  $row['option_id'], 'prize_level' =>  $row['option_name'], 'prize' =>  $row['option_desc'], 'prize_number' =>  $row['option_number'], 'chance' =>  $row['chance']);
            }
        }

        return $prizeArr;
    }

    public function getPrizeAction()
    {
        $id=$this->request->get("id");
        if(!$id || $id<=0)
        {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'illegal params');

        }
        $this->view->disable();
        /*
         * 每次前端页面的请求，PHP循环奖项设置数组，
         * 通过概率计算函数_getRand获取抽中的奖项id。
         * 将中奖奖品保存在数组$res['yes']中，
         * 而剩下的未中奖的信息保存在$res['no']中，
         * 最后输出json个数数据给前端页面。
         */
        //初始化返回数据
        $data = array();

        //检查用户是否登录
        if (!UserStatus::getUid()) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'请先登录，再抽奖');
          /*  $data['error'] = 'invalid';
            $data['errorMsg'] = '请先登录，再抽奖';
            return $this->response->setJsonContent($data)->send();*/
        }

        $uid = UserStatus::getUid();
        //大转盘的所有奖项
        $options = array();
        $marketing = AddonPromotion::findFirst('customer_id=' . CUR_APP_ID . ' AND code="scratch" and id='.$id);
        $marketingOptions = AddonPromotionOption::find('parent_id=' . $marketing->id);
        if ($marketingOptions) {
            foreach ($marketingOptions as $item) {
                $options[] = $item->option_id;
            }
        }
        if(empty($options)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'诶哟，商家连奖项都没设置，怎么抽得了奖呢！？');
           /* return $this->response->setJsonContent(array(
                'error' => 'invalid',
                'errorMsg' => "诶哟，商家连奖项都没设置，怎么抽得了奖呢！？"
            ))->send();*/
        }
        $userPrize = new AddonPromotionPrize();
        $userHistoryCount = $userPrize->count('user_id="' . $uid . '" AND activity_id= ' . $marketing->id . ' AND customer_id = ' . CUR_APP_ID);
        $data['lotteryNumber'] = intval($userHistoryCount);
        if ($userHistoryCount >= $marketing->times) {
          /*  $data['error'] = 'invalid';
            $data['errorMsg'] = '您已经刮了' . $marketing->times . '次了！';
            return $this->response->setJsonContent($data)->send();*/
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'您已经刮了' . $marketing->times . '次了！');
        }
        $arr = array();
        $prizeArr = $this->_getPrizeArray($id);
        foreach ($prizeArr as $key => $val) {
            $arr[$val['id']] = $val['chance'];
        }


        $rid = $this->_getRand($arr); //根据概率获取奖项id
        $hitOption = $prizeArr[$rid]; //选中奖项
        //判断是否奖
        $code = ''; //初始化中奖码
        if ($hitOption['prize_number'] > 0) {
            //中奖啦,记录下来
            try {
                $code = $this->_getPrizeSn();
                $data['prize_sn'] = $code;
            } catch (Exception $e) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"生成奖券号码失败");
                //return $this->response->setJsonContent(Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "生成奖券号码失败"))->send();
            }
        }

        //记录每次抽奖
        $userPrize->user_id = $uid;
        $userPrize->prize_id = $rid;
        $userPrize->code = $code;
        $userPrize->available = 1;
        $userPrize->activity_id =  $marketing->id;
        $userPrize->created = date('Y-m-d H:i:s');
        $userPrize->customer_id = CUR_APP_ID;
        $userPrize->start_time = $marketing->start_time;
        $userPrize->end_time = $marketing->start_time;
        $userPrize->save();

        $data['success'] = true;
        $data['option'] = $hitOption;
        return Ajax::init()->outRight("",$data);
       /* return $this->response->setJsonContent($data)->send();*/
    }

    /**
     * 抽奖次数
     * @return  integer
     */
    function _getLotteryNumber($uid,$parent_id)
    {
        $number = 0;
        $sql = "SELECT COUNT(*) as number FROM Models\Modules\Promotion\AddonPromotionPrize up LEFT JOIN Models\Modules\Promotion\AddonPromotionOption mo ON up.prize_id = mo.option_id
				LEFT JOIN Models\Modules\Promotion\AddonPromotion m ON mo.parent_id = m.id WHERE up.user_id=:uid: AND m.customer_id = :cid: AND m.code='scratch' and m.id=".$parent_id;
        $result = $this->modelsManager->executeQuery($sql, array('uid' => $uid, 'cid' => CUR_APP_ID));
        foreach ($result as $row) {
            $number = $row->number;
        }
        return $number;
    }

    /**
     * 生成中奖随机码
     * @return  string
     */
    function _getPrizeSn()
    {
        /* 选择一个随机的方案 */
        $code = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
        //查看随便码是否已经存在
        $existCount = AddonPromotionPrize::count('code ="' . $code . '"');
        if ($existCount > 0) {
            $code = $this->_getPrizeSn();
        }
        return $code;
    }

    //兑奖
    public function exchangeAction()
    {
        $this->view->disable();
        //检查用户是否登录
        if (!UserStatus::getUid()) {
            return   Ajax::init()->outError(Ajax::ACT_WHEEL_CODE_NOT_EXIST, '您还没有登陆,请登录后在抽奖');
        }

        $uid = UserStatus::getUid();
        $code = $this->request->getPost('code');
        $id= $this->request->getPost('id');
        if(!$code || !$id || !is_numeric($id)|| $id <=0)
        {
            return   Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, 'illegal params');
        }
        $scratch=AddonPromotion::findFirst("id='".$id."'");
        if(!$scratch)
        {
            return   Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, 'illegal params');
        }
       /* if($scratch->exchange_time_limit>0)//兑奖时间有限制
        {
            if(strtotime(date('Y-m-d',strtotime("-".$scratch->exchange_time_limit." day")))>$scratch->end_time)
            {
                return   Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '该活动兑奖时间已过');
            }

        }*/
        $prize = AddonPromotionPrize::findFirst('user_id="' . $uid . '" AND code<>"" AND code="' . $code . '"');

        if ($prize) {
            if ($prize->available == 0) {
                return   Ajax::init()->outError(Ajax::ACT_WHEEL_CODE_NOT_EXIST, '次兑奖码已兑奖过了');
            } else {
                $option = AddonPromotionOption::findFirst('option_id=' . $prize->prize_id);
                if ($option->award_type == 'points') {
                    $point = new UserPointLog();
                    $point->user_id = $uid;
                    $point->in_out = 1;
                    $point->action = PointRule::BEHAVIOR_SCRATCH;
                    $point->action_desc = PointRule::$behaviorNameMap[PointRule::BEHAVIOR_SCRATCH];
                    $point->logged = time();
                    $point->value = $option->option_id;
                    $point->save();
                    $user = UserForCustomers::findFirst('customer_id=' . CUR_APP_ID . " AND user_id=" . $uid);
                    if ($user) {
                        $user->points = $option->option_value + intval($user->points);
                        $user->points_available = $user->points_available + intval($option->option_value);
                        $user->update();
                    }
                } else if ($option->award_type == 'coupon') {
                    CouponManager::init()->receiveCoupon($option->option_value);
                }
                $prize->available = 0;
                $prize->save();
                return   Ajax::init()->outRight();
            }
        }
       return   Ajax::init()->outError(Ajax::ACT_WHEEL_CODE_NOT_EXIST, '兑奖码不存在');

    }


} 