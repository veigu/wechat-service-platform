<?php
namespace Multiple\Wap\Module;

use Components\UserStatusWap;
use Util\Ajax;
use Models\Modules\Reserve\AddonReserve;
use Models\Modules\Reserve\AddonReserveColumns;
use Models\Modules\Reserve\AddonReserveOrders;
use Util\Cookie;
use Util\EasyEncrypt;
use Util\Uri;

class ReserveController extends ModuleBase
{

	public function indexAction()
	{
        $list = AddonReserve::find(array(
            'customer_id=' . CUR_APP_ID . ' and is_active = 1',
            'order' => 'created desc',
            'limit' => 5
        ));

        $this->view->setVar('list', $list ? $list->toArray() : '');
    }

    public function detailAction()
    {
        $reserve_id = $this->check();
        if (!$reserve_id) {
            return;
        }

        $user_id = $this->checkLogin();

        $reserve = AddonReserve::findFirst("is_active = 1 and customer_id = " . CUR_APP_ID . ' and id=' . $reserve_id);
        if (!$reserve) {
            $this->err('400', '商家预约不存在！');
            return;
        }

        $totalNum = AddonReserveOrders::count("reserve_id = " . $reserve_id);

        // 查找已有订单
        $order = AddonReserveOrders::findFirst('customer_id=' . CUR_APP_ID . ' and reserve_id=' . $reserve_id . ' and user_id=' . $user_id);

        $columns = AddonReserveColumns::find('reserve_id = ' . $reserve_id . ' and is_active=1');

        $this->view->setVar('columns', $columns ? $columns->toArray() : null);
        $this->view->setVar('reserve', $reserve);
        $this->view->setVar('total', $totalNum);
        $this->view->setVar('order', $order);
        $this->view->title = $reserve->name . ' - 预约';
    }

    private function check()
    {
        $id = $this->dispatcher->getParam(0);
        $id = EasyEncrypt::decode($id);

        if (is_numeric($id)) {
            return $id;
        }
        $this->err('400', '商家未设置预约！');
        return false;
    }

    public function orderListAction()
    {
        $user_id = $this->checkLogin();

        $order = AddonReserveOrders::find('customer_id = ' . CUR_APP_ID . ' and user_id=' . $user_id);

        $res = [];
        if ($order) {
            foreach ($order as $item) {
                $tmp = $item;
                $tmp->reserve = AddonReserve::findFirst('customer_id =' . CUR_APP_ID . ' and id=' . $item->reserve_id);
                $res[] = $tmp;
            }
        }

        //$re
        $this->view->setVar('orders', $res);
        $this->view->title = '我的预约列表 - 预约';
    }

    public function editOrderAction()
    {
        $reserve_id = $this->check();
        if (!$reserve_id) {
            return;
        }

        $user_id = $this->checkLogin();

        $reserve = AddonReserve::findFirst("is_active = 1 and customer_id = " . CUR_APP_ID . ' and id=' . $reserve_id);
        if (!$reserve) {
            $this->err('400', '商家预约不存在！');
            return;
        }

        // 查找已有订单
        $order = AddonReserveOrders::findFirst('customer_id=' . CUR_APP_ID . ' and reserve_id=' . $reserve_id . ' and user_id=' . $user_id);

        $columns = AddonReserveColumns::find('reserve_id = ' . $reserve_id . ' and is_active=1');
        $this->view->setVar('columns', $columns ? $columns->toArray() : null);
        $this->view->setVar('reserve', $reserve);
        $this->view->setVar('order', $order);
        $this->view->title = '编辑预约订单 - 预约';
    }

    public function beforeRender()
    {
        $this->view->title = '预约商家 - 预约功能';
    }
}

