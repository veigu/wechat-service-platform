<?php

namespace Multiple\Wap\Module;

use Components\UserStatus;
use Models\Modules\VoteSurvey\AddonVoteSurvey;
use Models\Modules\VoteSurvey\AddonVoteSurveyLog;
use Models\Modules\VoteSurvey\AddonVoteSurveyOption;
use Models\Modules\VoteSurvey\AddonVoteSurveyQuestion;
use Multiple\Wap\Helper\Meta;
use Multiple\Wap\Helper\SiteData;
use Components\UserStatusWap;
use Phalcon\Tag;

class VoteController extends ModuleBase
{
    /*投票汇总页*/
    public function indexAction()
    {
        Meta::$head_title['vote_index'] = "微投票";
        $time = time();

        $list = AddonVoteSurvey::find(array(
            'customer_id=' . CUR_APP_ID . "AND type='vote' AND start_date <= " . $time . ' AND end_date>=' . $time,
            'order' => 'created desc',
        ));
        $list = $list ? $list->toArray() : '';
        if (!$list) {
            $this->err('404', '暂无投票');
            return;
        }
        $this->view->setVar('list', $list);
    }

    /*投票详情页*/
    public function voteAction()
    {

        $id = \Util\EasyEncrypt::decode($this->dispatcher->getParam(0));

        if (!($id && is_numeric($id))) {
            $this->err('404', '未找到相关投票,可能原因:该投票已不存在或者已过期');
            return;
        }
        $time = time();
        $opt = AddonVoteSurvey::findFirst('id=' . $id . " AND type='vote' AND customer_id=" . CUR_APP_ID . ' AND start_date <=' . $time . ' AND end_date>=' . $time);
        $this->view->setVar('vote', $opt ? $opt->toArray() : []);
        if (!$opt) {
            $this->err('404', '未找到相关投票,可能原因:该投票已不存在或者已过期');
            return;
        }
        $option = $this->modelsManager->createBuilder()
            ->addFrom('\Models\Modules\VoteSurvey\AddonVoteSurveyQuestion', 'vsq')
            ->leftJoin('\Models\Modules\VoteSurvey\AddonVoteSurveyOption', 'vsq.id=vso.question_id', 'vso')
            ->andWhere('vsq.vote_id=' . $id)
            ->columns('vso.*')
            ->getQuery()
            ->execute();
        $this->view->setVar('list', $option ? $option->toArray() : []);
    }

    public function ajaxVoteAction()
    {
        $this->view->disable();
        if (!UserStatus::getUid()) {
            return $this->response->setJsonContent(array('s' => '抱歉,您还未登陆.请先登录再投票', 'err' => '2'))->send(); //
        }
        $user_id = $this->checkLogin();
        $option_value = $this->request->getPost('id');
        $vote_id = $this->request->getPost('vote_id');

        //检查投票数是否超过了最多能够投票的次数
        $vote = AddonVoteSurvey::findFirst(array("id=" . $vote_id));

        if (!$vote) {
            return $this->response->setJsonContent(array('s' => '抱歉,该投票已不存在', 'err' => '1'))->send(); //
        }
        $vote = $vote->toArray();
        $question = AddonVoteSurveyQuestion::findFirst('vote_id=' . $vote_id);

        $question = $question ? $question->toArray() : [];

        $votetotal = AddonVoteSurveyLog::count('user_id=' . $user_id . ' AND vote_id=' . $vote_id);
        if ($votetotal >= $vote['max_by_total']) {
            return $this->response->setJsonContent(array('s' => '抱歉,您的投票总次数已经达到上限了', 'err' => '1'))->send(); //
        }


        $voteday = AddonVoteSurveyLog::count('user_id=' . $user_id . ' AND vote_id=' . $vote_id . " AND FROM_UNIXTIME(vote_time,'%Y-%m-%d')=curdate()");

        if ($voteday >= $vote['max_by_day']) {
            return $this->response->setJsonContent(array('s' => '抱歉,您的投票次数已经达到每日投票上限了', 'err' => '1'))->send();
        }

        $voteuser = new AddonVoteSurveyLog();
        $voteuser->vote_id = $vote_id;
        $voteuser->user_id = $user_id;
        $voteuser->vote_time = time();
        $voteuser->vote_result = serialize(array($question['id'] => $option_value));
        $voteuser->vote_topic = $vote['topic'];
        $voteuser->type = 'vote';
        $voteuser->customer_id = CUR_APP_ID;
        if ($voteuser->save()) {

            $voteoption = new AddonVoteSurveyOption();

            $res = $this->modelsManager->createQuery('update \\Models\Modules\VoteSurvey\AddonVoteSurveyOption set option_count=option_count+1 WHERE id=' . $option_value)
                ->execute();
            if ($res) {
                $option = AddonVoteSurveyOption::findFirst('id=' . $option_value);
                return $this->response->setJsonContent(array('s' => $option->toArray()['option_count'], 'err' => '0'))->send(); //投票成功
            }
            return $this->response->setJsonContent(array('s' => '投票失败', 'err' => '1'))->send(); //

        }
        return $this->response->setJsonContent(array('s' => '投票失败', 'err' => '1'))->send(); //'投票失败
    }

    public function afterExecuteRoute()
    {
        $this->view->title = '微投票';
    }

}


