<?php
namespace Multiple\Wap\Module;

use Components\Module\CouponManager;
use Components\Rules\PointRule;
use Components\UserStatus;
use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionOption;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\User\UserForCustomers;
use Models\User\UserPointLog;
use Multiple\Wap\Helper\SiteData;
use Components\UserStatusWap;
use Phalcon\Exception;
use Util\Ajax;
use Util\EasyEncrypt;

class WheelController extends ModuleBase
{

    public function indexAction()
    {
        $user_id = $this->checkLogin();

        $wheel = AddonPromotion::find("code='wheel' AND customer_id='" . CUR_APP_ID . "'");
        $this->view->setVar('list',$wheel ? $wheel->toArray() :array());
    }

    public function detailAction()
    {

        $id=EasyEncrypt::decode($this->dispatcher->getParam(0));
        if(!$id || $id<0)
        {
            header("location:/addon/wheel/");
        }
        $user_id = $this->checkLogin();

      //  $wheel = AddonPromotion::findFirst("code='wheel' AND customer_id='" . CUR_APP_ID . "' AND start_time<='" . strtotime(date('Y-m-d')) . "' AND (end_time > start_time AND end_time>='" . strtotime(date('Y-m-d')) . "' OR end_time = 0) and id=".$id);
        $wheel = AddonPromotion::findFirst("code='wheel' AND customer_id='" . CUR_APP_ID . "' and id=".$id);
        if (!empty($wheel)) {
            //奖品选项
            $wheelOptions = AddonPromotionOption::find("parent_id='" . $wheel->id . "'");
            $this->view->setVar('wheel', $wheel);
            $this->view->setVar('wheelOptions', $wheelOptions);

            $uid = UserStatus::getUid();
            $records = AddonPromotionPrize::query()
                ->where("user_id = ".$user_id." AND customer_id = '" . CUR_APP_ID . "' AND mo.parent_id = '{$wheel->id}'")
                ->join("Models\\Modules\\Promotion\\AddonPromotionOption", "mo.option_id = Models\\Modules\\Promotion\\AddonPromotionPrize.prize_id", 'mo')
                ->execute();
            $this->view->setVar('record', $records);

            $res = $this->modelsManager->createBuilder()
                ->addFrom('\\Models\\Modules\\Promotion\\AddonPromotionPrize', 'up')
                ->leftJoin('\\Models\\Modules\\Promotion\\AddonPromotionOption', 'up.prize_id=mp.option_id', 'mp')
                ->andWhere('mp.parent_id=' . $wheel->id . " AND up.user_id=" . $user_id)
                ->columns('up.prize_id')
                ->getQuery()
                ->execute();
            $count = count($res->toArray());
            $this->view->setVar('count', $count);
            $this->view->setVar('id',$id);

        }
    }

    public function beforeRender()
    {
        $this->view->title = '大转盘' . SiteData::init()->getSite('site_name');
    }

}

