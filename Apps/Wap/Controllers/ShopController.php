<?php
namespace Multiple\Wap\Controllers;

use Components\Product\ProductManager;
use Library\O2O\O2oShareManager;
use Models\Product\Product;
use Models\Shop\Shop;
use Multiple\Wap\Controllers\Base\MultiTplBase;
use Multiple\Wap\Helper\Meta;
use Util\Cookie;
use Util\EasyEncrypt;

class ShopController extends MultiTplBase
{
    public $top_title;

    public function initialize()
    {
        parent::initialize();;

        if (!$this->store) {
            $this->err('400', '该商家尚未开启店铺功能');
            return;
        }

        $cats = ProductManager::instance()->getCats(CUR_APP_ID);
        $this->view->setVar('cats', $cats);
    }

    public function indexAction()
    {
        Meta::$head_title['shop_index'] = '';
    }

    public function catAction()
    {

    }

    public function itemAction()
    {
        // 避免结算页面重复
        Cookie::set('_cart_settle_page', EasyEncrypt::encode($this->uri->baseUrl('shop/' . $this->router->getActionName())));
    }

    public function searchAction()
    {
        Meta::$head_title['shop_search'] = "商品搜索";

        $data['store'] = Shop::findFirst('customer_id=' . CUR_APP_ID . ' and enable=1');

        $this->view->setVars($data);
    }

    public function catalogAction()
    {
    }

    public function allAction()
    {

    }

    public function columnAction()
    {


    }

    public function listGoodsAction()
    {

    }


    public function shareAction()
    {
        if (!$this->store['cashback_enable']) {
            $this->err('200', '对不起！商家尚未支持推广分成');
            return;
        }

        O2oShareManager::init()->visitCount();
    }

    /*更多商品*/
    function  moreAction()
    {
        $type = $this->request->get('type');
        $title = "商品列表";
        $where = "";
        if (in_array($type, array('hot', 'new', 'recommended'))) {
            $where = "and  is_" . $type . "=1 ";
            if ($type == 'hot') {
                $title = "热卖商品";
            } else if ($type == 'new') {
                $title = "最新商品";
            } elseif ($type == 'recommended') {
                $title = "推荐商品";
            }

        }
        $list = Product::find("customer_id=" . CUR_APP_ID . " AND is_active=1 " . $where);
        $this->view->setVar('list', $list ? $list->toArray() : []);
        $this->view->setVar('title', $title);

    }
}