<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/28/14
 * Time: 2:41 PM
 */

namespace Multiple\Wap\Controllers;


use Components\Module\VipcardManager;
use Components\Rules\PointRule;
use Models\Modules\Pointmall\AddonPointMallOrder;
use Models\Modules\Store\AddonStore;
use Models\Modules\VoteSurvey\AddonVoteSurveyLog;
use Models\Product\ProductCollection;
use Models\Product\ProductComment;
use Models\Shop\ShopOrderItem;
use Models\Shop\ShopOrders;
use Models\User\UserAddress;
use Models\User\Users;
use Multiple\Wap\Controllers\Base\MultiTplBase;
use Components\UserStatusWap;
use Multiple\Wap\Helper\VipCard;
use Phalcon\Db\Adapter\Mongo\Collection;
use Phalcon\Mvc\Model;
use Util\Ajax;
use Util\Cookie;
use Util\EasyEncrypt;


class UserController extends MultiTplBase
{
    public $cookies = '';
    public static $referer = null;
    public $userInfo = null;
    public $cardInfo = null;

    public function indexAction()
    {
        if(!$this->checkLogin()){
            return ;
        }
        $this->userInfo = UserStatusWap::init()->getUserInfo();
        $cardInfo = VipcardManager::init()->getCardInfoByUid($this->checkLogin(), CUR_APP_ID);
        $this->view->setVar('user', $this->userInfo);
        $this->view->setVar('cardInfo', $cardInfo);
    }

    public function profileAction()
    {

    }

    public function signupAction()
    {
        if (UserStatusWap::getUid()) {
            $this->refresh('user');
        }

        // 避免结算页面重复
        Cookie::set('_cart_settle_page', EasyEncrypt::encode($this->uri->baseUrl('user/' . $this->router->getActionName())));
    }

    public function loginAction()
    {
        $uid = UserStatusWap::getUid();
        if ($uid) {
            // check bind card status
            VipcardManager::init()->checkNeedBindCard($uid, CUR_APP_ID);
            $this->refresh('user');
        }

        // 避免结算页面重复
        Cookie::set('_cart_settle_page', EasyEncrypt::encode($this->uri->baseUrl('user/' . $this->router->getActionName())));
    }

    public function logoutAction()
    {
        UserStatusWap::init()->logout();
         $this->response->redirect('user')->send();
    }

    private function refresh($uri)
    {
         $this->response->redirect(ltrim($uri, '/'))->send();
    }

    public function settingAction()
    {
        $this->checkLogin();
        $userInfo = UserStatusWap::init()->getUserInfo();
        $this->view->setVar('user', $userInfo);
    }

    /**
     * 用户订单
     */
    public function orderAction()
    {
        $user_id = $this->checkLogin();

        $where = "customer_id='" . CUR_APP_ID . "' AND user_id='" . $user_id . "' AND is_deleted = '0' ";
        $status = $this->request->get('s');
        if (is_numeric($status)) {
            $where .= ' AND status = ' . $status;
        }

        $orders = ShopOrders::find(array($where, 'order' => 'created desc,is_paid asc', 'limit' => '20'));
        $order1 = array();
        if ($orders) {
            $orders = $orders->toArray();

            $store = AddonStore::findFirst("customer_id=" . CUR_APP_ID);
            foreach ($orders as &$order) {
                if ($order['use_point'] > 0) {
                    $order['point_money'] = round($order['use_point'] / $store->redeem_proportion, 2) >= $order['total_cash'] + $order['logistics_fee'] ? $order['total_cash'] + $order['logistics_fee'] : round($order['use_point'] / $store->redeem_proportion, 2);
                }
                $item_list = ShopOrderItem::find('order_number="' . $order['order_number'] . '"');
                if (ProductComment::count("order_number='" . $order['order_number'] . "' AND user_id=$user_id") > 0) {
                    ProductComment::count("order_number='" . $order['order_number'] . "' AND user_id=$user_id");
                    $order['comment_yes'] = true;
                } else {
                    $order['comment_yes'] = false;
                }


                // 积分商城订单
                if ($order['is_pointmall']) {
                    $pointMall = AddonPointMallOrder::findFirst('order_number="' . $order['order_number'] . '"');
                    $order['pointmall'] = $pointMall ? $pointMall->toArray() : [];
                }

                $order['list'] = $item_list ? $item_list->toArray() : '';
            }
        }
        $this->view->setVar('orders', $orders);
    }


    public function forgotAction()
    {
        $phone = base64_decode($this->request->get('p'));

        $this->view->setVar('phone', $phone);
        $this->view->setVar('user', '');

        if (preg_match('/^(1[\d]{10})$/', $phone)) {
            $user = Users::findFirst('host_key="' . HOST_KEY . '" and phone="' . $phone . '"');
            $this->view->setVar('user', $user ? $user->toArray() : '');
        }
    }

    public function resetPassAction()
    {
    }

    public function addrAction()
    {
        $user_id = $this->checkLogin();
        $addr = UserAddress::find('user_id=' . $user_id);
        $this->view->setVar('address', $addr ? $addr->toArray() : '');
    }

    public function  commentAction()
    {
        $order_id = \Util\EasyEncrypt::decode($this->dispatcher->getParam(0)); //订单id

        $user_id = $this->checkLogin(); //用户id
        if (!is_numeric($order_id) || $order_id <= 0) {
            return $this->err('404', 'order could not found');
        }
        $count = ShopOrders::count("order_number='" . $order_id . "' AND user_id='" . $user_id . "'");
        if ($count == 0) {
            return $this->err('404', 'order could not found');
        }
        $list = ShopOrderItem::find("order_number='" . $order_id . "'");
        $this->view->setVar('order_number', $order_id);
        $this->view->setVar('list', $list ? $list->toArray() : []);


    }

    public function submitCommentAction()
    {

        $order_id = $this->request->getPost('order_number');
        $user_id = $this->checkLogin();
        $goodslist = $this->request->getPost('goodslist');
        $goodsarray = explode(",", $goodslist);
        if ($goodsarray) {
            $sqlb = " INSERT INTO Models\Product\ProductComment (order_number,parent_id,product_id,user_id,rank,content,created,is_show) ";
            $flag = 1;
            foreach ($goodsarray as $val) {
                $value = "('" . $order_id . "','0','" . $val . "','" . $user_id . "','" . $this->request->getPost('rank' . $val) . "','" . $this->request->getPost('content' . $val) . "','" . time() . "','0')";
                $sql = $sqlb . ' VALUES  ' . $value;
                if (!$this->modelsManager->executeQuery($sql)) {
                    $flag = 0;
                }
            }
            $this->view->setVar('flag', $flag);

        }

    }


    public function  commentLookAction()
    {
        $order_id = \Util\EasyEncrypt::decode($this->dispatcher->getParam(0)); //订单id;
        $user_id = $this->checkLogin();
        $comment =
            $this->modelsManager->createBuilder()
                ->addFrom('\\Models\\Product\\ProductComment', 'pc')
                ->leftJoin('\\Models\\Product\Product', 'pc.product_id=p.id', 'p')
                ->andWhere("pc.order_number='" . $order_id . "' AND pc.user_id=" . $user_id)
                ->columns('pc.id,pc.rank,pc.created,pc.content,p.name,p.thumb')
                ->getQuery()
                ->execute();
        $this->view->setVar('list', $comment ? $comment->toArray() : []);
    }

    public function bindPhoneAction()
    {

    }

    public function bindWechatAction()
    {

    }

    public function bindWeiboAction()
    {

    }

    public function bindAction()
    {
        $uid = UserStatusWap::getUid();

        if ($uid > 0) {
            $cardInfo = VipcardManager::init()->getCardInfoByUid($uid, CUR_APP_ID);

            if ($cardInfo) {
                return $this->response->redirect("user")->send();
            }
        }

        $this->view->setVar('user', $this->userInfo);
    }

    public function getOpenIdForClientRespondAction()
    {
        UserStatusWap::init()->getOpenIdForClientRespond();
    }


    /*微投票*/
    public function voteAction()
    {

        $vote = AddonVoteSurveyLog::find('user_id=' . $this->checkLogin() . " AND type='vote'");
        $this->view->setVar('vote', $vote ? $vote->toArray() : []);

    }

    /*微问卷*/
    public function surveyAction()
    {
        $survey = AddonVoteSurveyLog::find('user_id=' . $this->checkLogin() . " AND type='survey'");
        $this->view->setVar('survey', $survey ? $survey->toArray() : []);
    }

    /*
     * 我的收藏
     */
    public function collectionAction()
    {
        $user_id = $this->checkLogin();
        $collection = $this->modelsManager->createBuilder()
            ->addFrom('Models\\Product\\ProductCollection', 'pc')
            ->leftJoin('Models\\Product\\Product', 'pc.product_id=p.id', 'p')
            ->andWhere('pc.customer_id=' . CUR_APP_ID . " AND pc.user_id=" . $user_id)
            ->columns('pc.id as pcid,pc.created,p.id as pid, p.created,p.name,p.thumb,p.sell_price')
            ->getQuery()
            ->execute();
        $collection = $collection ? $collection->toArray() : [];
        $this->view->setVar('collection', $collection);

    }

    /*收藏商品*/
    public function ajaxCollectAction()
    {

        $user_id = UserStatusWap::getUid();
        if (!$user_id) {
            die(json_encode(array('error' => '1', 'msg' => '请登录后再收藏')));
        }
        $goods_id = $this->request->getPost('id');

        if (!$goods_id || !is_numeric($goods_id)) {
            die(json_encode(array('error' => '2', 'msg' => '错误的参数')));
        }

        $count = ProductCollection::count('customer_id=' . CUR_APP_ID . ' AND user_id=' . $user_id . " AND product_id=" . $goods_id);
        if ($count >= 1) {
            die(json_encode(array('error' => '3', 'msg' => '该商品已经在您的收藏夹内,请勿重复收藏')));
        }

        $collection = new ProductCollection();
        $collection->customer_id = CUR_APP_ID;
        $collection->user_id = $user_id;
        $collection->product_id = $goods_id;
        $collection->created = time();

        $this->view->disable();

        if ($collection->save()) {
            $vipgrade = VipcardManager::getUserVipGrade($user_id, CUR_APP_ID);
            PointRule::init(CUR_APP_ID, $vipgrade)->executeRule($user_id, PointRule::BEHAVIOR_ADD_FAVORITE);
            return $this->response->setJsonContent(array('error' => '0', 'msg' => '收藏成功'))->send();
        } else {
            $this->di->get('errorLogger')->error(json_encode($collection->getMessages()));
            return $this->response->setJsonContent(array('error' => '4', 'msg' => '收藏失败'))->send();
        }

    }


    /*注册成功页面*/
    function  registerSuccessAction()
    {

    }

}
