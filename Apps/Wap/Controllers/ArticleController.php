<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-9
 * Time: 上午11:07
 */

namespace Multiple\Wap\Controllers;

use Models\Article\SiteArticles;
use Models\Article\SiteArticlesCat;
use Multiple\Wap\Controllers\Base\MultiTplBase;
use Multiple\Wap\Helper\ArticleHelper;
use Multiple\Wap\Helper\Meta;
use Util\EasyEncrypt;

class ArticleController extends MultiTplBase
{
    // 栏目列表
    public function indexAction()
    {
        if ($this->request->get('pid')) {

            $pid = \Util\EasyEncrypt::decode($this->request->get('pid'));
            if (!is_numeric($pid)) {
                $this->err('404', '无效的参数列表');
                return;
            }
            $catInfo = SiteArticlesCat::findFirst(array('customer_id=' . CUR_APP_ID . " AND id=" . $pid, 'order' => 'sort desc'));
            $list = SiteArticlesCat::find(array('customer_id=' . CUR_APP_ID . " AND parent_id=" . $pid, 'order' => 'sort desc'));
            if ($list) {
                $list = $list->toArray();
                foreach ($list as $key => $item) {
                    $list[$key]['subCount'] = ArticleHelper::init()->getSubCatCount($item['id']);
                }
            }
            $this->view->setVar('list', $list ? $list : []);
            $this->view->setVar('catInfo', $catInfo ? $catInfo->toArray() : []);
            $this->view->setVar('pid', $pid);
        } else {

            //      $list = SiteArticlesCat::find('customer_id=' . CUR_APP_ID." AND parent_id=0");
            $list = SiteArticlesCat::find(array('customer_id=' . CUR_APP_ID, 'order' => 'sort desc'));

            if ($list) {

                $list = $list->toArray();
                foreach ($list as $key => $item) {
                    $list[$key]['subCount'] = ArticleHelper::init()->getSubCatCount($item['id']);

                }

            }

            $this->view->setVar('list', $list ? $list : []);

        }
    }

    // 栏目文章列表
    public function topicAction()
    {
        $id = $this->dispatcher->getParam(0);

        $id = EasyEncrypt::decode($id);

        if (!$id || !is_numeric($id)) {
            // todo error
            return $this->err('404', '文章栏目不存在');
        } else {
            $item = SiteArticlesCat::findFirst('customer_id = ' . CUR_APP_ID . ' and id=' . $id);

            if (!$item) {
                // todo error
                return $this->err('404', '文章栏目未找到');
            } else {
                $this->view->title = $item->name;
                $this->view->setVar('pagec', 0);
                $where = "customer_id = " . CUR_APP_ID . " AND parent_id = $id";
                $subCategories = SiteArticlesCat::find(array($where, 'order' => 'sort desc'));
                $this->view->setVar('nav', $item->toArray());
                if (count($subCategories) > 0) {
                    $this->view->setVar("categories", $subCategories);
                }
                else {
                    $this->view->setVar('limit_page', $item->allow_login == 1 ? true : false);
                    if ($item->allow_login == 1) //有登录限制
                    {
                        $this->view->setVar('pagec', $item->page);
                    } else {
                        if ($item->parent_id != 0) //此分类为子分类
                        {
                            $p = SiteArticlesCat::findFirst("customer_id=" . CUR_APP_ID . " AND id=" . $item->parent_id);
                            if ($p && $p->allow_login == 1 && $p->page > 0) {
                                $this->view->setVar('pagec', $p->page);
                            }
                        }
                    }

                    $list = new SiteArticles();
                    $where = 'published = 1 and customer_id = ' . CUR_APP_ID . ' and cid=' . $id;
                    $list = $list->find(array(
                        $where,
                        "order" => "modified desc",
                        //    "limit" => $limit * $curpage . ", " . $limit
                    ));

                    if ($list) {
                        $res = $list->toArray();
                        $this->view->setVar('list', $res);
                    }

                    $this->view->setVar('cat', $item);
                    $this->view->setVar('id', $id);

                    // meta
                    $this->view->keywords = $item->name . ' - ' . Meta::$siteName;
                    $this->view->description = $item->name . ' - ' . Meta::$siteName;

                }
            }
        }
    }

    // 所有文章列表（很少用）
    public function listAction()
    {
    }

    // 文章详情页面
    public function detailAction()
    {
        $id = $this->dispatcher->getParam(0);

        $id = EasyEncrypt::decode($id);
        if (!($id && is_numeric($id))) {
            // todo error
            $this->err('404', '文章未找到！');
            return;
        } else {
            $item = SiteArticles::findFirst('published = 1 and customer_id = ' . CUR_APP_ID . ' and id=' . $id);
            if (!$item) {
                // todo error
                $this->err('404', '文章未找到！');
                return;
            } else {
                if($item->allow_login > 0) {
                    $this->checkLogin();
                }
                $item->update(array('views' => $item->views + 1));

                $item = $item->toArray();

                $navs = SiteArticlesCat::findFirst('customer_id = ' . CUR_APP_ID . ' and id=' . $item['cid']);
                $item['cat_name'] = '';
                if ($navs) {
                    $navs = $navs->toArray();
                    $item['cat_name'] = $navs['name'];
                }
                // meta
                $this->view->title = $item['title'];
                $this->view->keywords = $item['meta_keyword'] . ' - ' . $item['title'] . ' - ' . $item['cat_name'] . ' - ' . Meta::$siteName;
                $this->view->description = mb_substr(strip_tags($item['content']), 0, 120, 'utf-8') . ' - ' . $item['cat_name'] . ' - ' . Meta::$siteName;
                $this->view->setVar('item', $item);
            }
        }
    }
} 