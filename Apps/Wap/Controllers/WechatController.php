<?php
namespace Multiple\Wap\Controllers;


use Components\WeChat\MessageManager;
use Components\WeChat\WeChatService;
use Models\Customer\CustomerOpenInfo;
use Models\Customer\Customers;
use Multiple\Wap\Module\ModuleBase;
use Phalcon\Logger;

class WechatController extends ModuleBase
{
    /**
     *
     * @var WeChatService
     */
    private $server = null;

    public function initialize()
    {
        $this->app = CUR_APP_ID;
        $this->view->disable();
        $customer = Customers::findFirst("id = '{$this->app}'");
        if (!$customer) {
            $this->err('404', '您查找的页面不存在！');
            exit(1);
        }

        /*if(!$customer->active) {
            $this->flash->error("此用户在平台的功能还没有激活！");
            $this->response->send();
            exit;
        }*/

        $openInfo =  CustomerOpenInfo::findFirst("customer_id = '{$this->app}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIXIN . "'");
        $this->getDI()->get('wechatLogger')->info(json_encode($openInfo->toArray()));
        $this->server = new WeChatService(array(
            'token' => $openInfo->token,
            'customer_id' => $this->app,
            'customer_app_account' => $openInfo->app_account,
            'app_id' => $openInfo->app_id,
            'app_secret' => $openInfo->app_secret
        ), $this->request);
    }

    public function serviceAction()
    {
        $result = $this->server->handle();
        $this->response->setContentType('text/xml', 'utf-8');
        if ($result == false) {
            $result = 0;
        }
        $this->response->setContent($result);
        $this->response->send();
    }

    public function oauthAction()
    {
        $appid = "wxdf8fefd22346dd0e";
        $secret = "2b48e1394bf6412da2b24cf25434c97a";
        $redirect_uri = $this->uri->baseUrl('wechat/oauth');
        $code = isset($_GET["code"]) ? $_GET["code"] : "";

        if (!$code) {
            $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $appid . '&redirect_uri=' . urlencode($redirect_uri) . '&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect';
            header("Location:" . $url);
        } else {
            $get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . $appid . '&secret=' . $secret . '&code=' . $code . '&grant_type=authorization_code';
            print_r($code);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $get_token_url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            $res = curl_exec($ch);
            curl_close($ch);
            $json_obj = json_decode($res, true);

            //根据openid和access_token查询用户信息
            $access_token = $json_obj['access_token'];
            $openid = $json_obj['openid'];
            $get_user_info_url = 'https://api.weixin.qq.com/sns/userinfo?access_token=' . $access_token . '&openid=' . $openid . '&lang=zh_CN';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $get_user_info_url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            $res = curl_exec($ch);
            curl_close($ch);

            //解析json
            $user_obj = json_decode($res, true);
            $_SESSION['user'] = $user_obj;
            print_r($user_obj);
        }
    }

    public function backAction()
    {

    }
}
