<?php
namespace Multiple\Wap\Controllers;


use Multiple\Wap\Controllers\Base\MultiTplBase;

class IndexController extends MultiTplBase
{
    public function indexAction()
    {
    }

    public function activityAction()
    {

    }

    public function notFoundAction()
    {
        echo '404 no found';
    }

    public function videoAction()
    {

    }
}

