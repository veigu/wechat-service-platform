<?php
namespace Multiple\Wap\Controllers;

use Models\Wap\SitePage;
use Multiple\Wap\Controllers\Base\MultiTplBase;
use Multiple\Wap\Helper\Meta;
use Util\EasyEncrypt;

class AboutController extends MultiTplBase
{
    public function indexAction()
    {
    }

    public function detailAction()
    {
        $id = $this->dispatcher->getParam(0);

        // app-id like 123456-1
        $id = EasyEncrypt::decode($id);

        if (!($id && is_numeric($id))) {
            return $this->err('404', 'article could not found');
        } else {
            $item = SitePage::findFirst(array('published = 1 and customer_id = ' . CUR_APP_ID . ' and id=' . $id, 'order' => 'sort asc'));
            if (!$item) {
                // todo error
                return $this->err('404', 'article could not found');
            } else {
                $item->update(array('views' => $item->views + 1));

                $item = $item->toArray();

                // meta
                $this->view->title = $item['title'];
                $this->view->keywords = $item['meta_keyword'] . ' - ' . $item['title'] . ' - ' . $item['cat_name'] . ' - ' . Meta::$siteName;
                $this->view->description = mb_substr(strip_tags($item['content']), 0, 120, 'utf-8') . ' - ' . $item['cat_name'] . ' - ' . Meta::$siteName;

                $this->view->setVar('item', $item);
                $this->view->setVar('top_title', $item['name']);
            }
        }
    }

    /*
     *
     * */
    public function  fashionAction()
    {

    }

}