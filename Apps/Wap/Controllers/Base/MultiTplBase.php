<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-11
 * Time: 下午3:14
 */

namespace Multiple\Wap\Controllers\Base;


use Components\TplManager;
use Models\Wap\SiteFocus;
use Multiple\Wap\Helper\Meta;
use Multiple\Wap\Helper\ShopData;
use Multiple\Wap\Helper\SiteData;
use Components\UserStatusWap;
use Phalcon\Mvc\Controller;

class MultiTplBase extends Controller
{
    protected $app = 0;

    protected $cache = null;
    protected $tpl = null;
    protected $customer_id = null;
    protected $site_info = null;
    protected $store = null;
    protected $_css = array(
        'base.css',
        'color.css'
    );

    protected function initialize()
    {
        if ($this->router->getControllerName() == 'user' && $this->router->getActionName() == 'logout') {
            return;
        }

        // set cache lib
        $this->_cache = $this->view->_cache = $cache = $this->getDI()->get('memcached');

        $this->tpl = $tpl = TplManager::init(CUR_APP_ID)->getTpl();

        if (!$tpl) {
            $this->err('502', '尚未选择模板～！');
            return;
        }

        if (!(is_numeric(CUR_APP_ID) && intval(CUR_APP_ID) > 0)) {
            $this->err('404', '您查看的应用尚不存在,访问地址有误!');
            return;
        }

        $this->site_info = SiteFocus::findFirst("id=" . CUR_APP_ID);
        $this->view->setVar("site_info", $this->site_info);

        // 是否开启店铺
        $this->store = $this->view->store = ShopData::init()->getStore(true);
        $this->view->setVar('store', ShopData::init()->getStore(true));

        $this->initTpl();
        $this->initCss();

        $this->view->setVar('site_name', SiteData::init()->getSite('site_name'));
        // set meta
        Meta::init()->set();

        // 微信/微博openid登陆
//        UserStatusWap::init()->auto_login();
    }

    /**
     * init css
     */
    public function initCss()
    {
        // init css
        $css = "??";
        $this->_css[] = TplManager::init(CUR_APP_ID)->getCss($this->tpl['tpl_mode']);
        $this->_css[] = TplManager::init(CUR_APP_ID)->getCss($this->tpl['tpl_mode'], 'menu');
        $this->_css = array_unique($this->_css);

        $css .= implode(',', $this->_css);
        $this->view->setVar('css', $css);
    }

    /**
     * init tpl
     *
     */
    public function initTpl()
    {
        $dir = TplManager::init(CUR_APP_ID)->getViewTypeDir($this->tpl['tpl_mode']);
        $this->view->setViewsDir(MODULE_PATH . '/Views/' . $dir);
    }

    /**
     * usage:
     * return $this->err();
     */
    protected function err($code = "403", $msg = '404 page no found')
    {
        $this->view->setViewsDir(MODULE_PATH . '/Views');
        $this->response->setHeader('content-type', 'text/html;charset=utf-8');
        $this->response->setStatusCode($code, $msg);

        $this->view->setVar('msg', $msg);


        return $this->view->pick('base/error');
    }

    protected function checkLogin()
    {
        return UserStatusWap::init()->checkLogin();
        /*if (!UserStatus::getUid()) {
            $cur_url = $this->uri->fullUrl();
            $redirect = 'user/login?go=' . urlencode($cur_url);
            return $this->response->redirect($redirect)->send();
        }

        return UserStatus::getUid();*/
    }
}
