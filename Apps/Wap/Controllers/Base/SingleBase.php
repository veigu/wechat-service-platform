<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-11
 * Time: 下午3:15
 */

namespace Multiple\Wap\Controllers\Base;

// 模板选择
use Components\TplManager;
use Components\UserStatusWap;
use Models\Customer\Customers;
use Models\Wap\SiteInfo;
use Phalcon\Mvc\Controller;

class SingleBase extends Controller
{
    protected $tpl = null;
    public $user_id = null;
    protected $checkLogin = false;
    protected $site_info = null;

    protected $_css = array(
        'base.css',
        'color.css'
    );

    public function initialize()
    {
        $this->tpl = $tpl = TplManager::init()->getTpl();
        if (!$tpl) {
            $this->err('502', '尚未选择模板～！');
            return;
        }

        if (!(is_numeric(CUR_APP_ID) && intval(CUR_APP_ID) > 0)) {
            $this->err('404', '您查看的应用尚不存在,访问地址有误!');
            return;
        }

        $this->site_info = SiteInfo::findFirst("customer_id=" . CUR_APP_ID);
        $this->view->setVar("site_info", $this->site_info);

        $this->user_id = UserStatusWap::getUid();
        if ($this->checkLogin && !$this->user_id) {
            $this->checkLogin();
            return;
        }
        $this->initCss();
        $this->view->setViewsDir(MODULE_PATH . '/Views/default');
    }

    /**
     * init css
     */
    public function initCss()
    {
        // init css
        $css = "??";
        $this->_css[] = 'default/' . $this->dispatcher->getControllerName() . '.css';
        $this->_css[] = TplManager::init()->getCss($this->tpl['tpl_mode'], 'menu');

        $this->_css = array_unique($this->_css);
        $css .= implode(',', $this->_css);
        $this->view->setVar('css', $css);
    }

    /**
     * usage:
     * return $this->err();
     */
    protected function err($code = "403", $msg = '404 page no found')
    {
        $this->view->setViewsDir(MODULE_PATH . '/Views');
        $this->response->setHeader('content-type', 'text/html;charset=utf-8');
        $this->response->setStatusCode($code, $msg);

        $this->view->setVar('msg', $msg);


        return $this->view->pick('base/error');
    }

    protected function checkLogin()
    {
        return UserStatusWap::init()->checkLogin();
    }
}