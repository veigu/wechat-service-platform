<?php
namespace Multiple\Wap\Controllers;


use Components\WeChat\MessageManager;
use Components\WeiBo\FansService;
use Models\Customer\CustomerOpenInfo;
use Models\Customer\Customers;
use Multiple\Wap\Controllers\Base\MultiTplBase;
use Phalcon\Logger;

class WeiboController extends MultiTplBase
{
    /**
     *
     * @var FansService
     */
    private $server = null;

    public function initialize()
    {
        $this->app = CUR_APP_ID;
        $this->view->disable();
        $customer = Customers::findFirst("id = '{$this->app}'");
        if (!$customer) {
            $this->err('404', '您查找的页面不存在！');
            exit(1);
        }
        /*if (!$customer->active) {
            $this->flash->error("此用户在平台的功能还没有激活！");
            $this->response->send();
            exit;
        }*/
        $openInfo = CustomerOpenInfo::findFirst("customer_id = '{$this->app}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO . "'");
        $this->getDI()->get('weiboLogger')->info(json_encode($openInfo->toArray()));
        $this->server = new FansService($openInfo->app_id, $openInfo->app_secret, $openInfo->token);
    }

    public function serviceAction()
    {
        $this->view->disable();
        $result = $this->server->handle();
        $this->di->get("weiboLogger")->info("respond data:" . json_encode($result));
        $this->response->setContentType('text/xml', 'utf-8');
        if ($result == false) {
            $result = 0;
        } 
        else if (is_array($result)) {
            $this->response->setContentType('text/javascript', 'utf-8');
        }
        $this->response->setContent($result);
        $this->response->send();
    }
}
