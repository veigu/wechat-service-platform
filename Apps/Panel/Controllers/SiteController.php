<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/8/14
 * Time: 11:37 AM
 */

namespace Multiple\Panel\Controllers;


use Util\EasyEncrypt;
use Components\StaticFileManager;
use Models\Customer\CustomerProfile;
use Models\Wap\SiteFocus;
use Models\Wap\SiteInfo;
use Models\Wap\SiteNavs;
use Models\Wap\SitePage;

use Phalcon\Tag;

class SiteController extends PanelBase
{
    public function infoAction()
    {
        StaticFileManager::addCssFile('static/wap/css/color.css');

        Tag::setTitle('网站设置 - 微网站信息设置 ');

        $info = SiteInfo::findFirst('customer_id=' . CUR_APP_ID);

        if (!$info) {
            // init tpl base data
            $default = array(
                'customer_id' => CUR_APP_ID,
                'created' => time(),
            	'count_code'=>''
            );

            // inert tpl data
            $m = new SiteInfo();
            if (!$m->create($default)) {
                foreach ($m->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
            }

            $info = SiteInfo::findFirst('customer_id=' . CUR_APP_ID);
        }

        if ($this->request->isPost()) {
            $info->site_name = $this->request->getPost('site_name');
            $info->site_logo = $this->request->getPost('site_logo');
            $info->site_icon = $this->request->getPost('site_icon');
            $info->site_color = $this->request->getPost('site_color');
            $info->site_bg = $this->request->getPost('site_bg');
            $info->count_code = $this->request->getPost('count_code');
            $info->company = $this->request->getPost('company');
            $info->address = $this->request->getPost('address');
            $info->record = $this->request->getPost('record');
            $info->qq = $this->request->getPost('qq');
            $info->phone = $this->request->getPost('phone');
            $info->weibo = ltrim($this->request->getPost('weibo'), '@');

            $info->modified = time();
            $info->update();

            // cache it
            $this->getDI()->get('memcached')->save('site_' . CUR_APP_ID, $info->toArray());
        }

        $this->view->setVar('info', $info ? $info->toArray() : []);
    }

   /* public function navAction()
    {
        Tag::setTitle('网站设置 - 微网站分类导航 ');

        StaticFileManager::addCssFile('static/wap/css/color.css');

        $navs = SiteNavs::find('customer_id=' . CUR_APP_ID . " and position = 'part' AND page_type='home'");
        $this->view->setVar('list', $navs ? $navs->toArray() : []);
    }

    public function footAction()
    {
        Tag::setTitle('网站设置 - 底部快捷导航');

        StaticFileManager::addCssFile('static/wap/css/color.css');

        $navs = SiteNavs::find('customer_id=' . CUR_APP_ID . " and position = 'foot' AND page_type='home'");
        $this->view->setVar('list', $navs ? $navs->toArray() : []);
    }

    public function focusAction()
    {
        Tag::setTitle('网站设置 - 微网站焦点图片');

        $focus = SiteFocus::find('customer_id=' . CUR_APP_ID." AND page_type='home'");
        $this->view->setVar('list', $focus ? $focus->toArray() : []);
    }*/

    public function pageAction()
    {
        Tag::setTitle('网站设置 - 微网站焦点图片');

        $focus = SitePage::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('list', $focus ? $focus->toArray() : []);
    }

    public function pageAddAction()
    {
    }

    public function pageUpAction()
    {
        Tag::setTitle('微网站 - 公司信息设置');

        $id = $this->dispatcher->getParam('0');
        // app-id like 123456-1
        $id = EasyEncrypt::decode($id);

        if (!($id && is_numeric($id))) {
            // todo error
            $this->err('404', 'article could not found');
        } else {
            $article = SitePage::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $id);
            if (!$article) {
                // todo error
                $this->err('404', 'article could not found');
            } else {
                $this->view->setVar('item', $article->toArray());
            }
        }
    }
}