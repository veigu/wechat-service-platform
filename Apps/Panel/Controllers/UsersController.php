<?php

namespace Multiple\Panel\Controllers;

use Components\Module\VipcardManager;
use Components\Product\TransactionManager;
use Components\Rules\PointRule;
use Components\UserManager;
use Models\Modules\Vipcard\AddonVipcard;
use Models\Modules\Vipcard\AddonVipcardField;
use Models\Modules\Vipcard\AddonVipcardGrade;
use Models\Modules\Vipcard\AddonVipcardSettings;
use Models\Modules\Vipcard\AddonVipcardUsers;
use Models\Shop\ShopOrders;
use Models\User\UserForCustomers;
use Models\User\UserGroups;
use Models\User\UserPointRules;
use Models\User\Users;
use Models\User\UserSettings;
use Multiple\Api\Controllers\UserController;
use Phalcon\Tag;
use Util\Ajax;
use Util\EasyEncrypt;

class UsersController extends PanelBase
{

    public function editAction()
    {

    }

    public function settingAction()
    {
        $setting = UserSettings::findFirst("customer_id=" .CUR_APP_ID ."");
        if($this->request->isPost()) {
            $need_check = $this->request->getPost('need_check', 'int', 0);
            if(!$setting) {
                $setting = new UserSettings();
                $setting->customer_id = CUR_APP_ID;
            }
            $setting->need_check = $need_check;
            if(!$setting->save()) {
                $errs = "";
                foreach($setting->getMessages() as $err) {
                    $errs .= (string)$err;
                }
                $this->di->get('errorLogger')->error("customer save user settings failed: " . $errs);
            }
            else {
                $this->flash->success("设置成功！");
            }

        }
        $this->view->setVar("setting", $setting);
    }

    public function rulesAction()
    {
        $this->ruleSetting();
    }

    public function firmRulesAction() {
        $this->ruleSetting('firm');
    }

    protected  function ruleSetting($type = 'normal')
    {
        if(empty($type)) {
            $type = 'normal';
        }
        if(!in_array($type,array('normal','firm')))
        {
            $this->flash->error("illegal parm");
            return $this->response->send();
        }
        $where="";
        if($type=='normal')
        {
            $where=" and  is_firm=0 ";
        }
        else
        {
            $where=" and  is_firm=1 ";
        }
        \Phalcon\Tag::setTitle("用户积分规则");
        $behaviorNameMap = PointRule::$behaviorNameMap;
        $rule = new PointRule(CUR_APP_ID, 0);
        $termNameMap = $rule->termNameMap;
        $pointTypeMap = $rule->actionNameMap;

        $list = UserPointRules::find('customer_id=' . CUR_APP_ID . ' and vip_grade = 0 '. $where);

        $list = $list ? $list->toArray() : [];
        $exits_rules = array_map(function ($row) {
            return $row['behavior'];
        }, $list);

        $new_add = array_diff_key($behaviorNameMap, array_flip($exits_rules));

        $this->view->data = $list;
        $this->view->new_add = $new_add;
        $this->view->behaviorNameMap = $behaviorNameMap;
        $this->view->termNameMap = $termNameMap;
        $this->view->pointTypeMap = $pointTypeMap;
        $this->view->type = $type;
    }

    public function indexAction()
    {

        \Phalcon\Tag::setTitle("手机用户列表");
        $currentPage = $this->request->get('page', 'int');
        $group = $this->request->get('group', 'int');
        $this->view->setVar('group', $group);
        $card_grade = $this->request->get('card_grade', 'int');
        $this->view->setVar('card_grade', $card_grade);
        $id = $this->request->get('id', 'int');
        $this->view->setVar('id', $id);
        $nick = $this->request->get('name', 'striptags');
        $this->view->setVar('name', $nick);
        $date_start = $this->request->get('date_start', 'int');
        $date_end = $this->request->get('date_end', 'int');

        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom("\\Models\\User\\UserForCustomers",  'uc')
            ->leftJoin('\\Models\\User\\Users', "u.id = uc.user_id", 'u')
            ->leftJoin("\\Models\\Modules\\Vipcard\\AddonVipcardUsers", "u.id = uv.user_id", "uv")
            ->leftJoin("\\Models\\Modules\\Vipcard\\AddonVipcardGrade", "uvg.id = uv.card_grade", "uvg")
            ->where("uc.customer_id=".CUR_APP_ID)
            ->columns("u.id, u.username, u.gender, u.phone, u.avatar, u.province, u.city, u.town, u.address, u.email, uc.active, uc.points, uc.points_available, uc.wx_open_id,uc.wb_uid, uv.card_no, uvg.name");
        if ($group) {
            $queryBuilder->andWhere("uc.group_id='{$group}'");
        }
        if ($card_grade) {
            $queryBuilder->andWhere("uv.card_grade='{$card_grade}'");
        }
        if ($id) {
            $queryBuilder->andWhere("u.id = '{$id}'");
        }
        if ($nick) {
            $queryBuilder->andWhere("LOCATE('{$nick}', u.username) > 0");
        }
        if ($date_start) {
            $queryBuilder->andWhere("uc.subscribe_time > '{$date_start}'");
            if ($date_end && $date_end > $date_start) {
                $queryBuilder->andWhere("uc.subscribe_time < '{$date_start}'");
            }
        } else {
            if ($date_end) {
                $queryBuilder->andWhere("uc.subscribe_time < '{$date_start}'");
            }
        }

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));
        $this->view->setVar('list', $pagination->getPaginate());

        $groups = UserManager::instance()->getUserGroups(CUR_APP_ID);
        $cardTypes = VipcardManager::init()->getAllGrade(CUR_APP_ID);
        $this->view->setVar('groups', $groups);
        $this->view->setVar('cardTypes', $cardTypes);

    }

    public function wechatAction()
    {
        \Phalcon\Tag::setTitle("微信用户列表");
        $currentPage = $this->request->get('page', 'int');
        $group = $this->request->get('group', 'int');
        $this->view->setVar('group', $group);
        $nick = $this->request->get('name', 'striptags');
        $this->view->setVar('name', $nick);
        $date_start = $this->request->get('date_start', 'int');
        $date_end = $this->request->get('date_end', 'int');

        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\User\\UsersWechat', 'u')
            ->leftJoin("\\Models\\User\\UserWechatGroup", 'u.group_id = ug.group_id', 'ug')
            ->columns("u.headimgurl, u.nickname, u.sex, ug.name, u.province, u.city, u.is_binded, u.user_id, u.subscribe_time")
            ->where("u.customer_id='".$this->customer_id."'");
        if ($group) {
            $queryBuilder->andWhere("u.group_id='{$group}'");
        }
        if ($nick) {
            $queryBuilder->andWhere("LOCATE('{$nick}', u.username) > 0");
        }
        if ($date_start) {
            $queryBuilder->andWhere("u.subscribe_time > '{$date_start}'");
            if ($date_end && $date_end > $date_start) {
                $queryBuilder->andWhere("u.subscribe_time < '{$date_start}'");
            }
        } else {
            if ($date_end) {
                $queryBuilder->andWhere("u.subscribe_time < '{$date_start}'");
            }
        }

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));
        $this->view->setVar('list', $pagination->getPaginate());

        $groups = UserManager::instance()->getWechatGroups($this->customer_id);
        $this->view->setVar('groups', $groups);
    }


    public function weiboAction()
    {
        \Phalcon\Tag::setTitle("微信用户列表");
        $currentPage = $this->request->get('page', 'int');
        $group = $this->request->get('group', 'int');
        $this->view->setVar('group', $group);
        $nick = $this->request->get('name', 'striptags');
        $this->view->setVar('name', $nick);
        $date_start = $this->request->get('date_start', 'int');
        $date_end = $this->request->get('date_end', 'int');

        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\User\\UsersWeibo', 'u')
            ->leftJoin("\\Models\\User\\UserWeiboGroup", 'u.group_id = ug.group_id', 'ug')
            ->columns("u.headimgurl, u.nickname, u.sex, ug.name, u.province, u.city, u.is_binded, u.user_id, u.subscribe_time")
            ->where("u.customer_id=".$this->customer_id."");
        if ($group) {
            $queryBuilder->andWhere("u.group_id='{$group}'");
        }
        if ($nick) {
            $queryBuilder->andWhere("LOCATE('{$nick}', u.username) > 0");
        }
        if ($date_start) {
            $queryBuilder->andWhere("u.subscribe_time > '{$date_start}'");
            if ($date_end && $date_end > $date_start) {
                $queryBuilder->andWhere("u.subscribe_time < '{$date_start}'");
            }
        } else {
            if ($date_end) {
                $queryBuilder->andWhere("u.subscribe_time < '{$date_start}'");
            }
        }

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));
        $this->view->setVar('list', $pagination->getPaginate());

        $groups = UserManager::instance()->getWeiboGroups($this->customer_id);
        $this->view->setVar('groups', $groups);
    }

    public function groupAction()
    {
        \Phalcon\Tag::setTitle("自定义分组列表");
        $this->assets->addJs('static/panel/js/app/panel/users/user.group.js');
        $this->assets->addCss('static/panel/modules/users/group.css');

        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()->addFrom('\\Models\\User\\UserGroups', 'grade')->andWhere("grade.customer_id='".$this->customer_id."'");
        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 20,
            "page" => $currentPage
        ));
        $this->view->setVar('local', $pagination->getPaginate());
        $typ= 1;//UserController::GROUP_TYPE_LOCAL;
        $this->view->setVar("type",$typ);
       /* $queryBuilder = $this->modelsManager->createBuilder()->addFrom('\\Models\\User\\UserWechatGroup', 'grade')->andWhere("grade.customer_id='".$this->customer_id."'");
        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));
        $this->view->setVar('wechat', $pagination->getPaginate());

        $queryBuilder = $this->modelsManager->createBuilder()->addFrom('\\Models\\User\\UserWeiboGroup', 'grade')->andWhere("grade.customer_id='".$this->customer_id."'");
        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 20,
            "page" => $currentPage
        ));
        $this->view->setVar('weibo', $pagination->getPaginate());*/

    }

    public function wechatGroupAction()
    {
        \Phalcon\Tag::setTitle("自定义分组列表");
        $this->assets->addJs('static/panel/js/app/panel/users/user.group.js');
        $this->assets->addCss('static/panel/modules/users/group.css');

        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()->addFrom('\\Models\\User\\UserWechatGroup', 'grade')->andWhere("grade.customer_id=".CUR_APP_ID."");
        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));
        $typ= 2;
       // UserController::GROUP_TYPE_WECHAT;
        $this->view->setVar("type",$typ);
        $this->view->setVar('list', $pagination->getPaginate());

    }

    public function weiboGroupAction()
    {
        \Phalcon\Tag::setTitle("自定义分组列表");
        $this->assets->addJs('static/panel/js/app/panel/users/user.group.js');
        $this->assets->addCss('static/panel/modules/users/group.css');

        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()->addFrom('\\Models\\User\\UserWeiboGroup', 'grade')->andWhere("grade.customer_id=".CUR_APP_ID."");
        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));
        $typ= 4;
        // UserController::GROUP_TYPE_WEIBO;
        $this->view->setVar("type",$typ);
        $this->view->setVar('list', $pagination->getPaginate());
    }

    public function detailAction($id = null, $type = '') {
        Tag::setTitle("用户详情");
        $id = EasyEncrypt::decode($id);
        if(empty($id)) {
            $this->flash->error("你指定的用户不存在！");
        }
        else {
            $this->view->setVar('user_id', $id);
            $user = UserForCustomers::findFirst("user_id='{$id}' AND customer_id = '" . CUR_APP_ID . "'");
            if (!$user) {
                $this->flash->error("您指定的用户不存在或者没有管理此用户的权限");
            }
            else {
                if($type && $type == 'o') {
                    $this->view->setVar('type', 'o');
                    $currentPage = $this->request->get('page', 'int');
                    if (empty($currentPage)) {
                        $currentPage = 1;
                    }
                    $name = trim($this->request->get('name', 'striptags', ''));
                    $status = intval($this->request->get('status', 'striptags', -1));
                    $order = trim($this->request->get('order', 'striptags', ''));

                    $queryBuilder = $this->modelsManager->createBuilder()
                        ->addFrom('\\Models\\Shop\\ShopOrders', 'so')
                        ->leftJoin("\\Models\\User\\UserAddress", 'so.address_id = ua.id', 'ua')
                        ->leftJoin("\\Models\\Shop\\Logistics", 'l.key = so.logistics_type', 'l')
                        ->andWhere("so.customer_id='" . CUR_APP_ID . "' AND so.user_id = '{$id}'")
                        ->columns("so.order_number,so.created, so.total_cash, so.paid_cash, so.is_paid, so.is_cod, so.paid_time, so.paid_type, so.paid_order, so.is_payment_arrived,
                    so.status,so.is_delivered, l.name as logistics_name, so.delivered_time, so.logistics_type, so.logistics_fee, so.logistics_order,
                    ua.name, ua.province, ua.city, ua.town, ua.address, ua.phone, ua.zip_code")
                        ->orderBy("so.created DESC");

                    if (strlen($name) > 0) {
                        $this->view->setVar("name", $name);
                    }

                    if ($status > -1) {
                        $queryBuilder->andWhere("so.status = '{$status}'");
                    }
                    $this->view->setVar('status', $status);

                    if (strlen($order) > 0) {
                        $this->view->setVar('order', $order);
                        $queryBuilder->andWhere("so.order_number='{$order}'");
                    }
                    $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
                        "builder" => $queryBuilder,
                        "limit" => 10,
                        "page" => $currentPage
                    ));
                    $list = $pagination->getPaginate();
                    $this->view->setVar('list', $list);
                    $orderStatus = TransactionManager::getOrderStatus();
                    $this->view->setVar("orderStatus", $orderStatus);
                }
                else {
                    $base_info = $user->toArray();
                    $user_info = Users::findFirst("id = '{$id}'");
                    $base_info = array_merge($base_info, $user_info->toArray());
                    $this->view->setVar('user_base', $base_info);
                    $groups = UserGroups::find("customer_id = '" . CUR_APP_ID . "'");
                    $this->view->setVar("groups", $groups);
                    $vipSetting = AddonVipcardSettings::findFirst("customer_id='" .CUR_APP_ID . "'");
                    if($vipSetting && $vipSetting->enable) {
                        $vip_grade = AddonVipcardGrade::find("customer_id='" . CUR_APP_ID  . "'");
                        $this->view->setVar("vip_grades", $vip_grade);
                        $vip_info = AddonVipcardUsers::findFirst("user_id='{$user_info->id}'");
                        if($vip_info) {
                            $this->view->setVar('vip_card_info', $vip_info->toArray());
                        }
                        $fields = AddonVipcardField::find('customer_id=' . CUR_APP_ID);
                        if($fields) {
                            $this->view->setVar("fields", $fields->toArray());
                        }
                    }
                }
            }
        }
    }
}