<?php
namespace Multiple\Panel\Controllers;

use Components\CustomerManager;
use Models\Customer\Customers;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\View;
use Phalcon\Tag as Tag;

class AccountController extends Controller
{
    public $customer;

    public function initialize()
    {
        $action = $this->dispatcher->getActionName();
        if($action != 'logout') {
            $customer = $this->session->get('customer_info');
            if ($customer instanceof Customers) {
                $this->customer = $customer;
                $redirectUrl = CustomerManager::init()->checkJoinStepRedirect($customer);
                $this->response->redirect(ltrim($redirectUrl, '/'))->send();
                return;
            }
        }
        $this->view->setMainView('account');
        $this->view->setLayout('account');
    }

    public function loginAction()
    {
        Tag::setTitle("客户登陆");
    }

    public function regAction()
    {
        Tag::setTitle("客户入住");
    }

    public function logoutAction()
    {
        CustomerManager::init()->logout();
        $this->response->redirect('account/login')->send();
    }

    public function protocolAction()
    {
        Tag::setTitle('客户协议');
        $this->view->setLayout('plain');
    }

    public function forgotAction()
    {
        Tag::setTitle('找回密码');
    }

}
