<?php

namespace Multiple\Panel\Controllers;

use Components\IndustryManager;
use Models\Customer\CustomerProfile;
use Phalcon\Tag;

class SettingController extends PanelBase
{
    public function indexAction()
    {
        $this->view->setVar('customer', $this->customer);

        Tag::setTitle("资料设置");
        $industry = IndustryManager::instance()->getTreeData(true);
        $profile = CustomerProfile::findFirst('customer_id=' . CUR_APP_ID);

        $this->view->setVar('profile', $profile ? $profile->toArray() : []);
        $this->view->setVar('industry', $industry);
    }

    public function passwordAction()
    {
        \Phalcon\Tag::setTitle('微信聚合平台-密码修改');
        if ($this->request->isPost()) {
            $original = $this->request->getPost('original', array('string'));
            $original = sha1($original);
            if ($original != $this->customer->password) {
                $this->flash->error("原始密码有误");
                return false;
            }

            $password = $this->request->getPost('password', 'striptags');
            $repeatPassword = $this->request->getPost('repeatPassword', 'striptags');
            if (empty($password)) {
                $this->flash->error("新密码不能为空");
                return false;
            }
            if ($password != $repeatPassword) {
                $this->flash->error("两次密码不一致");
                return false;
            }
            $password = sha1($password);
            if (!$this->customer->update(array(
                'password' => $password
            ))
            ) {
                foreach ($this->customer->getMessages() as $msg) {
                    $this->flash->error((string)$msg);
                }
                return false;
            }
            $this->customer->password = $password;
            $this->flash->success("密码修改成功");
        } else {
            $this->flash->notice("为了您的账号安全，请谨慎修改！");
        }
    }

    public function shortMessageAction() {

    }

    public function emailAction() {

    }

}