<?php

namespace Multiple\Panel\Controllers;

use Components\StaticFileManager;
use Components\WeChat\MessageManager;
use Components\WeChat\RequestFactory;
use Components\WeChat\ResourceManager;
use Models\WeChat\MessageResourceImages;
use Models\WeChat\MessageResourceNewsContent;
use Models\WeChat\MessageResourceVoices;
use Models\WeChat\MessageResourceVideos;
use Models\WeChat\MessageResourceMusics;
use Models\WeChat\MessageResourceNews;
use Multiple\Panel\Plugins\Upload;
use Phalcon\Paginator\Adapter\QueryBuilder;

class ResourceController extends PanelBase
{
    public function indexAction()
    {
        \Phalcon\Tag::setTitle("资源管理");
        StaticFileManager::addCssFile('static/panel/css/resource/resource.css');
        StaticFileManager::addJsFile('js/app/panel/resource/resource.js');
        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\WeChat\\MessageResourceNews', 'news')
            ->where("news.customer_id=".CUR_APP_ID."");

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 20,
            "page" => $currentPage
        ));
        $data = $pagination->getPaginate();
        $this->view->setVar('data', $data);
    }

    public function imagesAction()
    {
        \Phalcon\Tag::setTitle("资源管理");
        StaticFileManager::addCssFile('static/panel/css/resource/resource.css');
        StaticFileManager::addJsFile('js/app/panel/resource/resource.js');
        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\WeChat\\MessageResourceImages', 'image')
            ->where("image.customer_id=".CUR_APP_ID."");

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 20,
            "page" => $currentPage
        ));
        $data = $pagination->getPaginate();
        $this->view->setVar('data', $data);
    }

    public function voicesAction()
    {
        \Phalcon\Tag::setTitle("资源管理");
        StaticFileManager::addCssFile('static/panel/css/resource/resource.css');
        StaticFileManager::addJsFile('js/app/panel/resource/resource.js');
        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\WeChat\\MessageResourceVoices', 'voice')
            ->where("voice.customer_id=".CUR_APP_ID."");

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 20,
            "page" => $currentPage
        ));
        $data = $pagination->getPaginate();
        $this->view->setVar('data', $data);
    }

    public function videosAction()
    {
        \Phalcon\Tag::setTitle("资源管理");
        StaticFileManager::addCssFile('static/panel/css/resource/resource.css');
        StaticFileManager::addJsFile('js/app/panel/resource/resource.js');
        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\WeChat\\MessageResourceVideos', 'video')
            ->where("video.customer_id=".CUR_APP_ID."");

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 20,
            "page" => $currentPage
        ));
        $data = $pagination->getPaginate();
        $this->view->setVar('data', $data);
    }

    public function saveArticleAction() {
        $this->view->disable();
        $title = $this->request->getPost('title', 'string');
        $cover = $this->request->getPost('cover', 'string');
        $link = $this->request->getPost('link', 'url');
        $content = $this->request->getPost('content', 'string');

        $code = 0;
        $messages = [];
        if(empty($title)) {
            $code = 1;
            $messages[] = "图文标题不能为空";
        }
        if(empty($cover)) {
            $code = 1;
            $messages[] = "封面图片不能为空";
        }
        if(empty($content)) {
            $code = 1;
            $messages[] = "图文内容不能为空";
        }
        if($code > 0) {
            $this->response->setJsonContent(array(
                'code' => $code,
                'message' => $messages,
                'result' => ''
            ))->send();
            exit(1);
        }
        $result = '';
        try {
            $this->db->begin();
            $news = new MessageResourceNews();
            $news->customer_id = CUR_APP_ID;
            $news->title = $title;
            $news->cover = $cover;
            $news->link = $link;
            $news->created = time();
            if(!$news->save()) {
                $this->db->rollback();
                $code = 1;
                foreach($news->getMessages() as $message) {
                    $messages[] = $message;
                }
            }
            else {
                $newsContent = new MessageResourceNewsContent();
                $newsContent->customer_id = CUR_APP_ID;
                $newsContent->news_id = $news->id;
                $newsContent->content = $content;
                if(!$newsContent->save()) {
                    $code = 1;
                    foreach($newsContent->getMessages()  as $message) {
                        $messages[] = $message;
                    }
                }
                else {
                    $result = $news->id;
                }
            }
            $this->db->commit();
        }
        catch(\PharException $e) {
            $this->db->rollback();
            $messages[] = $e->getMessage();
        }
        $this->response->setJsonContent(array(
            'code' => $code,
            'message' => $messages,
            'result' => $result
        ))->send();
        exit(1);
    }

    public function uploadAction()
    {
        $this->view->disable();
        $type = $this->dispatcher->getParam(0, array("string"));
        if (empty($type)) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '没有上传任何文件',
                'results' => ''
            ))->send();
            exit(1);
        } else {
            $files = $this->request->getUploadedFiles();
            if (count($files) == 0) {
                $this->response->setJsonContent(array(
                    'code' => 1,
                    'message' => '没有上传任何文件',
                    'results' => ''
                ))->send();
                exit(1);
            }

            $file = $files[0];
            $errorCode = 0;
            $errorMessage = [];
            $result = '';

            # 缓存文件及后缀
            $buff = file_get_contents($file->getTempName());
            $buffMd5 = md5($buff);
            $filePath = $this->getLocalPath($buffMd5, $file->getRealType());

            //prepare for upload media file to WeChat server
            $request = RequestFactory::create('FileUpload', $this->customer->id, $this->customer_wechat->app_id, $this->customer_wechat->app_secret);
            $request->file_path = $filePath['full_path'];
            $request->mime = $file->getRealType();
            $request->name = $file->getName();

            //save to database
            switch ($type) {
                case ResourceManager::MESSAGE_TYPE_VOICE:
                {
                    //upload to local storage
                    $fileExts = 'mp3|amr';
                    $maxAttachSize = 262144; // 256k
                    //upload to WeChat server
                    //@TODO
                    $request->set('type', 'voice');
                    $result = $request->run();
                    if(isset($request['errcode']) && $result['errcode'] > 0) {
                        $media_id = '';
                    }
                    else {
                        $media_id = $result['media_id'];
                    }

                    $resource = MessageResourceVoices::findFirst("customer_id=".CUR_APP_ID." AND md5='{$buffMd5}'");
                    if(!$resource) {
                        $resource = new MessageResourceVoices();
                        $resource->customer_id = $this->customer->id;
                        $resource->created = time();
                        $resource->time = '60';
                        $resource->media_id = $media_id;
                        $resource->url = $filePath['url'];
                        $resource->md5 = $buffMd5;
                        $resource->name = $file->getName();
                        if (!$resource->save()) {
                            $errorCode = 1;
                            foreach($resource->getMessages() as $message) {
                                $errorMessage[] = (string)$message;
                            }
                        }
                        else {
                            $result = $this->saveUploadFile($file, $fileExts, $maxAttachSize, $filePath['full_path']);
                            if($result !== true) {
                                $errorCode = 1;
                                $errorMessage = $result;
                            }
                        }
                    }
                    else {
                        //update media id for old resource
                        //$resource->update(array('media_id' => $media_id, 'name' => $file->getName()));
                    }
                    break;
                }
                case ResourceManager::MESSAGE_TYPE_VIDEO:
                {
                    //upload to local storage
                    $fileExts = 'mp4';
                    $maxAttachSize = 1024000; // 1m
                    $url = $file['url'];
                    //upload to WeChat server
                    //@TODO
                    /*$request = RequestFactory::create('FileUpload', $this->customer['id']);
                    $request->set('type', 'video');
                    $result = $request->run();
                    $request->getErrorMessage($request['code']);*/

                    $resource = MessageResourceVideos::findFirst("customer_id=".CUR_APP_ID." AND md5='{$buffMd5}'");
                    if(!$resource) {
                        $resource = new MessageResourceVideos();
                        $resource->customer_id = $this->customer->id;
                        $resource->created = time();
                        $resource->title = "";
                        $resource->desc = '';
                        $resource->media_id = $file->getTempName();
                        $resource->url = $filePath['url'];
                        $resource->md5 = $buffMd5;
                        $resource->name = $file->getName();
                        if (!$resource->save()) {
                            $errorCode = 1;
                            foreach($resource->getMessages() as $message) {
                                $errorMessage[] = (string)$message;
                            }
                        }
                        else {
                            $result = $this->saveUploadFile($file, $fileExts, $maxAttachSize, $filePath['full_path']);
                            if($result !== true) {
                                $errorCode = 1;
                                $errorMessage[] = $result;
                            }
                        }
                    }
                    else {
                        //update media id for old resource
                        //$resource->update(array('media_id' => $media_id, 'name' => $file->getName()));
                    }
                    break;
                }
                case ResourceManager::MESSAGE_TYPE_IMAGE:
                default:
                {
                    //upload to local storage
                    $fileExts = 'jpg|jpeg'; //jpg
                    $maxAttachSize = 131072; // 128k
                    $result = $this->saveUploadFile($file, $fileExts, $maxAttachSize, $filePath['full_path']);
                    if($result !== true) {
                        $errorCode = 1;
                        $errorMessage = $result;
                    }
                    //upload to WeChat server
                    //@TODO
                    $request->type = 'image';
                    $request->run();
                    $result = $request->getResult();
                    if($request->isFailed()) {
                        $errorCode = 1;
                        $errorMessage = $request->getErrorMessage();
                    }
                    else {
                        $media_id = $result['media_id'];
                        $resource = MessageResourceImages::findFirst("customer_id=".CUR_APP_ID." AND md5='{$buffMd5}'");
                        if(!$resource) {
                            $resource = new MessageResourceImages();
                            $resource->customer_id = $this->customer->id;
                            $resource->name = $media_id;
                            $resource->url = $filePath['url'];
                            $resource->md5 = $buffMd5;
                            $resource->media_id = $buffMd5;
                            $resource->created = time();

                            if (!$resource->save()) {
                                $errorCode = 1;
                                foreach($resource->getMessages() as $message) {
                                    $errorMessage[] = (string)$message;
                                }
                            }
                            else {

                            }
                        }
                        else {
                            //update media id for old resource
                            $resource->update(array('media_id' => $media_id, 'name' => $file->getName()));
                        }
                    }
                    break;
                }
            }

            if($errorCode == 0) {
                $result = array(
                    'id' => $resource->id,
                    'url' => $filePath['url'],
                    'media_id' => $file->getTempName(),
                    'name' => $file->getName()
                );
            }

            $data = array(
                'code' => $errorCode,
                'message' => $errorMessage,
                'result' => $result
            );
            $this->response->setJsonContent($data);
            $this->response->send();
        }
    }

    public function getAction()
    {
        $this->view->disable();
        if (!$this->request->isAjax()) {
            $this->flash->warning("对不起，此接口只接受ajax请求！");
            $this->response->send();
            exit;
        }

        $page = $this->request->get("page", "int");
        $type = $this->request->getPost('type', array("string"));
        if (empty($type)) {
            $data = array(
                'code' => -1,
                'result' => array(
                    'total_items' => 0,
                    'items' => array()
                )
            );
        } else {
            $resource = ResourceManager::instance()->getCustomerMessage($this->customer->id, $type, $page);
            $items = array();
            foreach ($resource->items as $item) {
                $items[] = json_decode(json_encode($item, JSON_OBJECT_AS_ARRAY), true);
            }
            $resource->items = $items;
            $data = array(
                'code' => 0,
                'result' => $resource
            );
        }
        $this->response->sendHeaders("content-type: text/javascript; charset=utf-8");
        $this->response->setJsonContent($data);
        $this->response->send();
    }

    /**
     * 根据文件md5获取文件存放路径 保证文件唯一性
     *
     * @param $md5
     * @param $sExt
     * @return array
     */
    private function getLocalPath($md5, $sExt)
    {
        if (strpos($sExt, '/') > 0) {
            $sExt = explode('/', $sExt);
            $sExt = $sExt[1];
        }

        $fileDir = '/' . strtoupper(substr(md5($this->customer->id), 0, 8)) . '/' . strtolower($sExt) . '/' . strtoupper(substr($md5, 0, 2)) . '/' . strtoupper(substr($md5, 2, 2));
        $fullPath = ROOT . '/Public/uploads' . $fileDir;
        if (!is_dir($fullPath)) {
            @mkdir($fullPath, 0777, true);
        }
        $newFilename = $md5 . '.' . $sExt;
        $targetPath = $fullPath . '/' . $newFilename;
        $fileUrl = '/uploads' . $fileDir . '/' . $newFilename;
        return array('url' => $fileUrl, 'full_path' => $targetPath);
    }

    private function saveUploadFile(\Phalcon\Http\Request\FileInterface $file, $fileExtensions, $maxAttachSize, $destination) {
        // 匹配格式
        $pattern = '/(' . $fileExtensions . ')$/i';
        if (!preg_match($pattern, $file->getRealType(), $sExt)) {
            return "不支持此类型文件上传";
        };

        // 文件太大
        if ($maxAttachSize < $file->getSize()) {
            return "文件太大，不得超过" . round($maxAttachSize / 1024, 2) . 'K';
        };

        if (!file_exists($destination)) {
            try {
                if (!$file->moveTo($destination)) {
                    return "文件移动到目的地失败！";
                }
            }
            catch(\Exception $e) {
                return "文件移动到目的地失败！";
            }
        }
        return true;
    }

    public function removeAction() {
        $this->view->disable();
        $type = $this->request->getPost('type', 'string');
        $item = $this->request->getPost('item', 'int');
        if(!in_array($type, array())) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'result' => '',
                'message' => "不支持的资源类型【{$type}】"
            ))->send();
            exit(1);
        }

        switch($type) {
            case ResourceManager::MESSAGE_TYPE_IMAGE: {
                $source = MessageResourceImages::findFirst("customer_id=" .CUR_APP_ID ." AND id='{$item}'");
                break;
            }
            case ResourceManager::MESSAGE_TYPE_VOICE: {
                $source = MessageResourceVoices::findFirst("customer_id=" .CUR_APP_ID ." AND id='{$item}'");
                break;
            }
            case ResourceManager::MESSAGE_TYPE_VIDEO: {
                $source = MessageResourceVideos::findFirst("customer_id=" .CUR_APP_ID ." AND id='{$item}'");
                break;
            }
            case ResourceManager::MESSAGE_TYPE_NEWS: {
                $source = MessageResourceNews::findFirst("customer_id=" .CUR_APP_ID ." AND id='{$item}'");
                break;
            }
        }
    }
}
