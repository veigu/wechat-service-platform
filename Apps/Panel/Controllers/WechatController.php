<?php

namespace Multiple\Panel\Controllers;

use Components\StaticFileManager;
use Components\UserManager;
use Components\WeChat\MenuManager;
use Components\WeChat\MessageManager;
use Components\WeChat\ResourceManager;
use Components\WeChat\WeChatAutoImport;
use Components\WeChat\WeChatAutoImportSync;
use Models\Customer\CustomerOpenInfo;
use Models\WeChat\CustomerMenus;
use Models\WeChat\MessageHistory;
use Models\WeChat\MessageSettingKeywords;
use Models\WeChat\MessageSettingMass;
use Models\WeChat\MessageSettings;
use Util\Pagination;

class WechatController extends PanelBase
{
    private $wx;

    public function initialize()
    {
        parent::initialize();
        if (empty($this->customer_wechat->is_binded) && ($this->dispatcher->getActionName() != 'auto' && $this->dispatcher->getActionName() != 'binding')) {
            return $this->forward('wechat/binding');
        }
    }

    public function autoAction()
    {
        \Phalcon\Tag::setTitle("微信公众号自助绑定");
        $this->flash->success('请使用微信账号登陆后自动绑定');
        if ($this->request->isPost()) {
            $user = trim($this->request->getPost('wx_user', array("striptags")), '');
            $pass = trim($this->request->getPost('wx_pass', array("striptags"), ''));
            $imgcode = trim($this->request->getPost('wx_imgcode', array("striptags"), ''));
//            $user = 'register@estt.com.cn';
//            $pass = 'estt@123';
            $wx = new WeChatAutoImport();
            $this->wx = $wx;
            if (!$wx->init(CUR_APP_ID, $user, $pass, $imgcode)) {
                $this->flash->error($wx->getMsg());
                $this->view->setVar('user', $user);
                $this->view->setVar('pass', $pass);
                $this->view->setVar('verifycode', $wx->getCodeImg($user));
            } else {
                WeChatAutoImportSync::init()->save($wx);
            }
        }
    }

    public function respondAction($type = null)
    {
        if (empty($type)) {
            $type = MessageManager::EVENT_TYPE_SUBSCRIBE;
        } else {
            if (!in_array($type, array(
                MessageManager::EVENT_TYPE_SUBSCRIBE,
                MessageManager::EVENT_TYPE_NORMAL,
                MessageManager::EVENT_TYPE_KEYWORD
            ))
            ) {
                $this->flash->error("您指定的设置不存在，已经给你调整为关注时的设置");
                $type = MessageManager::EVENT_TYPE_SUBSCRIBE;
            }
        }
        \Phalcon\Tag::setTitle('微信聚合平台-回复消息设置');
        $this->assets->addCss('static/panel/css/module/wechat/wechat.respond.css');

        $data = [];
        switch ($type) {
            case MessageManager::EVENT_TYPE_SUBSCRIBE: {
                $data = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIXIN, $this->customer->id, MessageManager::EVENT_TYPE_SUBSCRIBE, true);
                break;
            }
            case MessageManager::EVENT_TYPE_NORMAL: {
                $data = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIXIN, $this->customer->id, MessageManager::EVENT_TYPE_NORMAL, true);
                break;
            }
            case MessageManager::EVENT_TYPE_KEYWORD: {
                $this->flash->error("为了保障系统性能，限制每个用户不得设置超过10个规则");
                $data = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIXIN, $this->customer->id, MessageManager::EVENT_TYPE_KEYWORD, true);
                break;
            }
        }

        $messageType = isset($data['message_type']) ? $data['message_type'] : ResourceManager::MESSAGE_TYPE_TEXT;
        $this->view->setVar('message', $data);
        $this->view->setVar('message_type', $messageType);
        $this->view->setVar('messageSettingJson', json_encode($data));
        $this->view->setVar("respond_type", $type);
    }

    public function messageAction($type = null)
    {
        \Phalcon\Tag::setTitle('微信聚合平台-历史消息记录');
        $currentPage = $this->request->get('p', 'int');
        if (empty($currentPage) || $currentPage < 0) {
            $currentPage = 1;
        }
        $pageSize = 20;
        $skip = ($currentPage - 1) * $pageSize;

      //  $condition = ["customer_id" => CUR_APP_ID];
        $condition="customer_id=".CUR_APP_ID;

        if ($type && $type == 1) {
            $condition.= " and  is_replied=1 ";
        } else if (empty($type)) {
            $condition.= " and  is_replied=0 ";
        }
        $count = MessageHistory::count($condition);
        $data = MessageHistory::find(array(
            "customer_id='{$this->customer->id}'",
            'order' => 'received desc',
            "limit" => $pageSize));

        Pagination::instance($this->view)->showPage($currentPage, $count, $pageSize);

        $this->view->setVar('list', $data);
    }

    public function bindingAction()
    {
        \Phalcon\Tag::setTitle('微信聚合平台-微信设置');
        if ($this->request->isPost()) {
            $app_account = $this->request->getPost('app_account', 'string');
            $app_id = $this->request->getPost('app_id', 'string');
            $app_secret = $this->request->getPost('app_secret', 'string');
            $original_id = $this->request->getPost('original_id', 'string');
            $enable_dkf = $this->request->getPost('enable_dkf', 'int');
            $openInfo = CustomerOpenInfo::findFirst("customer_id=" . CUR_APP_ID . " AND platform='" . MessageManager::PLATFORM_TYPE_WEIXIN . "'");
            if (!$openInfo) {
                $openInfo = new CustomerOpenInfo();
                $openInfo->platform = MessageManager::PLATFORM_TYPE_WEIXIN;
                $openInfo->customer_id = CUR_APP_ID;
            }
            $openInfo->app_account = $app_account;
            $openInfo->app_id = $app_id;
            $openInfo->app_secret = $app_secret;
            $openInfo->original_id = $original_id;
            $openInfo->enable_dkf = $enable_dkf;
            if (!$openInfo->save()) {
                foreach ($openInfo->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                $this->session->set('customer_wechat', $openInfo);
                $this->flash->success("数据更新成功");
            }
        }

        $openInfo = CustomerOpenInfo::findFirst("customer_id=" . CUR_APP_ID . " AND platform='" . MessageManager::PLATFORM_TYPE_WEIXIN . "'");
        $this->view->setVar("openInfo", $openInfo);
        $this->view->setVar('customer_id', CUR_APP_ID);
        if ($openInfo && $openInfo->app_id && $openInfo->app_secret && empty($this->is_binded)) {
            $this->flash->notice("您需要在微信公众平台开发者模式下设置回调URL和验证TOKEN");
        } elseif (empty($openInfo->app_id) || empty($openInfo->app_secret)) {
            $this->flash->notice("您需要输入微信公众平台开发者模式下的appId和app_secret，并设置回调URL和验证TOKEN");
        }
    }

    public function refreshTokenAction()
    {
        $this->view->disable();
        echo json_encode(array(
            "token" => "aaaa"
        ));
    }

    public function menuAction()
    {
        \Phalcon\Tag::setTitle('微信聚合平台-微信菜单设置');

        $this->assets->addCss('static/panel/css/module/wechat/wechat.message.css');
        $this->assets->addJs('static/ace/js/jquery.nestable.min.js');
        $this->assets->addJs('static/ace/js/bootbox.min.js');
        $menus = MenuManager::instance()->getMenus($this->customer->id, MessageManager::PLATFORM_TYPE_WEIXIN);
        if (!$menus) {
            MenuManager::instance()->syncFromWeChat($this->customer->id, $this->customer_wechat->app_id, $this->customer_wechat->app_secret);
            $menus = MenuManager::instance()->getMenus($this->customer->id, MessageManager::PLATFORM_TYPE_WEIXIN);
        }

        $this->view->setVar('menus', $menus);
    }

    public function massAction()
    {
        \Phalcon\Tag::setTitle("微信公众号-消息群发");
        $this->assets->addCss('static/panel/css/module/wechat/wechat.message.css');
        $monthStartTime = strtotime('last month');
        $thisMonthMassCount = MessageSettingMass::count(array("customer_id=" . CUR_APP_ID . " AND time >= '{$monthStartTime}'"));
        $this->view->setVar('monthCount', $thisMonthMassCount);
//        $available = 4 - $thisMonthMassCount;

        $this->flash->success("本月您已经群发<code>{$thisMonthMassCount}</code>条消息");
        $groups = UserManager::instance()->getWechatGroups(CUR_APP_ID, true);
        $this->view->setVar('groups', $groups);
    }

}
