<?php

namespace Multiple\Panel\Controllers;

use Components\ModuleManager\ModuleManager;
use Phalcon\Tag;

class IndexController extends PanelBase
{
    public function indexAction()
    {
        Tag::setTitle('管理平台');
        $this->view->setVar('customer', $this->customer);

        $this->view->setVar('customer_wechat', $this->customer_wechat);

        $this->assets->addCss('panel/css/module/module.mine.css');
        $mine = ModuleManager::instance(HOST_KEY, CUR_APP_ID)->getCustomerModules();
        if ($mine) {
            $this->view->setVar("mineModules", $mine);
        }

        $notice = '';
        if (!$this->customer_weibo) {
            $notice .= "您还没有绑定企业微博粉丝服务";
        }

        if (!$this->customer_wechat) {
            $notice .= "您还没有绑定微信公众号";
        }

        if (!empty($notice)) {
            $this->flash->notice($notice);
        }

    }

    public function noFoundAction()
    {
    }
}