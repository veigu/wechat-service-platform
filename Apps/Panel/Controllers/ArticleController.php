<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/8/14
 * Time: 11:37 AM
 */

namespace Multiple\Panel\Controllers;


use Util\EasyEncrypt;
use Models\Article\SiteArticles;
use Models\Article\SiteArticlesCat;
use Phalcon\Tag;
use Util\Pagination;

class ArticleController extends PanelBase
{

    public function listAction()
    {
        Tag::setTitle('微网站-文章列表');

        $page = $this->request->get('p');
        $cid = $this->uri->getParam('cid');
        $key = $this->request->get('key');
        $where = is_numeric($cid) && $cid >= 0 ? ' customer_id=' . CUR_APP_ID . ' and cid = ' . $cid : ' customer_id=' . CUR_APP_ID;
        $where = $key ? $where . ' and title like "%' . $key . '%"' : $where;
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 12;

        $count = SiteArticles::count($where);
        $res = SiteArticles::find(array($where, "order" => "created desc", "limit" => $curpage * $limit . "," . $limit));
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $res ? $res->toArray() : []);

        $cats = SiteArticlesCat::find('customer_id = ' . CUR_APP_ID);
        $this->view->setVar('cats', $cats ? $cats->toArray() : []);
    }


    public function addAction()
    {
        Tag::setTitle('微网站-添加文章');

        $cats = SiteArticlesCat::find('customer_id = ' . CUR_APP_ID);
        $this->view->setVar('cats', $cats ? $cats->toArray() : []);

        if ($this->request->isPost()) {
            $article = new SiteArticles();
            $article->customer_id = CUR_APP_ID;
            $article->cid = $this->request->getPost('cid');
            $article->cover = $this->request->getPost('cover');
            $article->title = $this->request->getPost('title');
            $article->author = $this->request->getPost('author');
            $article->meta_keyword = $this->request->getPost('meta_keyword');
            $article->meta_desc = $this->request->getPost('meta_desc');
            $article->content = $this->request->getPost('content');
            $article->recommend = $this->request->getPost('recommend');
            $article->published = $this->request->getPost('published');
            $article->summary = $this->request->getPost('summary');
            $article->allow_login = $this->request->getPost('allow_login');

            $article->created = time();
            $article->modified = time();

            if ($article->save() == false) {
                foreach ($article->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
            } else {
                $this->flash->success("文章添加成功！ <a href='/panel/article/update/" . EasyEncrypt::encode($article->id) . "'>查看并修改</a>");
            }
        }
    }

    public function updateAction()
    {
        Tag::setTitle('微网站-更新文章');

        $cats = SiteArticlesCat::find('customer_id = ' . CUR_APP_ID);
        $this->view->setVar('cats', $cats ? $cats->toArray() : []);

        $id = $this->dispatcher->getParam('0');
        // app-id like 123456-1
        $id = EasyEncrypt::decode($id);

        if (!($id && is_numeric($id))) {
            // todo error
            return $this->err('404', 'article could not found');
        } else {

            $article = SiteArticles::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $id);
            if (!$article) {
                // todo error
                return $this->err('404', 'article could not found');
            } else {
                if ($this->request->isPost()) {
                    $article->cid = $this->request->getPost('cid');
                    $article->cover = $this->request->getPost('cover');
                    $article->title = $this->request->getPost('title');
                    $article->author = $this->request->getPost('author');
                    $article->meta_keyword = $this->request->getPost('meta_keyword');
                    $article->meta_desc = $this->request->getPost('meta_desc');
                    $article->content = $this->request->getPost('content');
                    $article->recommend = $this->request->getPost('recommend');
                    $article->published = $this->request->getPost('published');
                    $article->summary = $this->request->getPost('summary');
                    $article->allow_login = $this->request->getPost('allow_login');

                    $article->modified = time();

                    if ($article->update() == false) {
                        foreach ($article->getMessages() as $message) {
                            $this->flash->error((string)$message);
                        }
                    } else {
                        $this->flash->success("文章修改成功！");
                    }
                }

                $this->view->setVar('article', $article->toArray());
            }

        }
    }

    public function catAction()
    {
        Tag::setTitle('微网站 - 文章栏目');
        $this->assets->addCss('static/wap/css/color.css');

        $cat = SiteArticlesCat::find('customer_id = ' . CUR_APP_ID);

        $this->view->setVar('list', $cat ? $cat->toArray() : []);
    }

}