<?php

namespace Multiple\Panel\Controllers;

use Components\CustomerManager;
use Components\ModuleManager\Helper\MenuHelper;
use Components\ModuleManager\ModuleHandler;
use Components\ModuleManager\ModuleManager;
use Components\Payments\PaymentUtil;
use Models\Customer\Customers;
use Models\Modules\ResourceForCustomers;
use Models\Modules\ResourceForSystemOrders;
use Models\System\SystemOrders;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Tag;

class ModuleController extends PanelBase
{
    protected $_module;
    protected $_handler;
    protected $_action;


    public function runAction()
    {
        \Phalcon\Tag::setTitle("模块管理");
        $this->view->_module = $this->_module = $this->dispatcher->getParam('_m', 'striptags');
        $this->view->_handler = $this->_handler = $this->dispatcher->getParam('_h', 'striptags');
        $this->view->_action = $this->_action = $this->dispatcher->getParam('_a', 'striptags');
        if (empty($this->_module) || empty($this->_handler)) {
            return $this->err('400', '模块参数不正确！');
        }

        if (empty($this->_action)) {
            $this->_action = "index";
        }

        $module = $this->getModule();
        if (!$module instanceof ModuleHandler) {
            if ($module == -1) {
                return $this->err('401', '没有找到对应的Handler对象');
            } else if (empty($module)) {
                return $this->err('400', '没有找到此模块！');
            }
        }

        if (!$module->isAuthed()) {
            $customer_package = $this->session->get('customer_package');
            // 已经过期
            if (in_array($this->customer->active, array(CustomerManager::ACCOUNT_STATUS_EXPIRED_TRIAL, CustomerManager::ACCOUNT_STATUS_EXPIRED_PACKAGE))) {
                return $this->err('400', '对不起，' . @CustomerManager::$_active_name[$this->customer->active] . '，无法使用该扩展功能！');
            }

            if ($customer_package['package'] == 'custom') {
                if (!in_array($this->_module, json_decode($customer_package['own_module'], true))) {
                    return $this->err('400', '你没有使用该功能的权限1！');
                }
            } else {
                if (in_array($this->_module, json_decode($customer_package['limit_module'], true))) {
                    return $this->err('400', '你没有使用该功能的权限2！');
                }
            }
        }

        $moduleModel = ModuleManager::instance(HOST_KEY, CUR_APP_ID)->getModuleResource($this->_module);
        call_user_func(array($module, 'initialize'));

        $eventManager = new EventsManager();
        $eventManager->attach('view', $module);
        $module->setEventsManager($eventManager);
        $this->view->setVar('moduleName', $moduleModel->name);

        if (method_exists($module, $this->_action)) {
            call_user_func(array($module, $this->_action));
            $mainViewFile = 'module/' . $this->_module . '/' . $this->_handler . '/' . $this->_action;
            if (!$this->view->isDisabled()) {
                $this->view->partial($mainViewFile);
            }
        } else {
            return $this->err('400', '没有找到对应的处理函数！');
        }
    }

    public function buyAction($module_name = null)
    {
        \Phalcon\Tag::setTitle("模块购买");
        if (empty($module_name)) {
            return $this->response->redirect("panel/module/more");
        }

        $mm = ModuleManager::instance(HOST_KEY, CUR_APP_ID);
        $module = $mm->getModuleResource($module_name);
        $this->view->setVar('module', $module);
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.custom.min.css');
        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.custom.min.js');
        $this->assets->addJs('static/ace/js/jquery.ui.touch-punch.min.js');
        $this->assets->addJs('static/panel/js/app/panel/module/module.buy.js');

        if ($this->request->isPost()) {
            $id = $this->request->getPost('id', 'int', 0);
            $year = $this->request->getPost('years', 'int', 0);
            $month = $this->request->getPost('months', 'int', 0);
            if (empty($year) && empty($month)) {
                $this->flash->error("您至少需要购买一个月的期限！");
            } else {
                $total_cash = $module->year_price * $year + $module->month_price * $month;
                $this->db->begin();
                try {
                    $order = new SystemOrders();
                    $order->order_number = ModuleManager::instance(HOST_KEY)->orderNumberGenerator();
                    $order->customer_id = CUR_APP_ID;
                    $order->host_key = HOST_KEY;
                    $order->ordered = time();
                    $order->resource_count = 1;
                    $order->total_money = $total_cash;
                    $order->status = 0;
                    if (!$order->save()) {
                        $messages = [];
                        foreach ($order->getMessages() as $message) {
                            $messages[] = (string)$message;
                        }
                        throw new \Phalcon\Exception($messages);
                    }

                    $orderResources = new ResourceForSystemOrders();
                    $orderResources->order_number = $order->order_number;
                    $orderResources->resource_id = $module->id;
                    $orderResources->trade_price = $total_cash;
                    $orderResources->years = $year;
                    $orderResources->months = $month;
                    $orderResources->days = 0;
                    if (!$orderResources->save()) {
                        $messages = [];
                        foreach ($orderResources->getMessages() as $message) {
                            $messages[] = (string)$message;
                        }
                        throw new \Phalcon\Exception($messages);
                    }
                    $this->db->commit();
                    $this->session->set("current_order", $order->order_number);
                    return $this->response->redirect("panel/module/confirmOrder");
                } catch (\Phalcon\Exception $e) {
                    $this->db->rollback();
                    $this->di->get('debugLogger')->error("购买模块时生成订单失败." . $e->getMessage());
                    return $this->err('500', '系统出错，攻城狮正在处理中，您可以联系客服提交疑问，谢谢您对支持与合作！');
                }
            }
        }
    }

    public function confirmOrderAction()
    {
        \Phalcon\Tag::setTitle("订单确认-支付");

        $this->assets->addCss('static/panel/css/account.steps.css');
        $config = $this->di->get("config");
        $alipay_config = $config->payment->alipay;
        $cbpay_config = $config->payment->cbpay;
        $order = $this->session->get('current_order');
        if (empty($order)) {
            $this->flash->error("抱歉，没有找到您的订单！");
        } else {
            if (is_string($order)) {
                $order = SystemOrders::findFirst("order_number='{$order}'");
            }

            if (!$order) {
                $this->flash->error("抱歉，没有找到您的订单！");
            } else {
                $payments = PaymentUtil::instance(HOST_KEY)->getHostPayments();
                $this->view->setVar('payments', $payments);
                $resources = ResourceForSystemOrders::find("order_number='{$order->order_number}'");
                $moduleManager = ModuleManager::instance(HOST_KEY);
                $modules = [];
                if ($resources) {
                    foreach ($resources as $resource) {
                        $module = $moduleManager->getModuleResourceById($resource->resource_id);
                        $modules[] = $module;
                    }
                }

                $this->view->setVar('order', $order);
                $this->view->setVar('modules', $modules);
                $this->view->setVar('alipayConfig', $alipay_config);
                $this->view->setVar('cbpayConfig', $cbpay_config);
            }
        }
    }

    public function editOrderAction()
    {
        \Phalcon\Tag::setTitle('平台入驻——功能设置');
        $customer = $this->session->get('customer_info');
        if (empty($customer)) {
            return $this->response->redirect('panel/account/reg');
        }
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.custom.min.css');
        $this->assets->addCss('static/ace/css/select2.css');
        $this->assets->addCss('static/ace/css/chosen.css');
        $this->assets->addCss('static/panel/css/module/order.edit.css');

        $this->assets->addJs('static/ace/js/fuelux/fuelux.wizard.min.js');
        $this->assets->addJs('static/ace/js/jquery.validate.min.js');
        $this->assets->addJs('static/ace/js/additional-methods.min.js');
        $this->assets->addJs("static/ace/js/chosen.jquery.min.js");
        $this->assets->addJs('static/ace/js/bootbox.min.js');
        $this->assets->addJs('static/ace/js/jquery.maskedinput.min.js');
        $this->assets->addJs('static/ace/js/select2.min.js');
        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.custom.min.js');
        $this->assets->addJs('static/ace/js/jquery.ui.touch-punch.min.js');
        $this->assets->addJs('static/panel/js/app/panel/module/order.edit.js');

        $modules = ModuleManager::instance(HOST_KEY)->getSystemModules();
        $this->view->setVar('modules', $modules);
        $this->view->setVar('customer', $customer);
        $order = $this->session->get('current_order');
        if (is_string($order)) {
            $order = SystemOrders::findFirst("order_number='$order'");
        }
        $customerModules = ModuleManager::instance(HOST_KEY)->getCustomerModules(CUR_APP_ID);
        $this->view->setVar("customer_modules", $customerModules);

        if ($order instanceof SystemOrders) {
            $resources = ResourceForSystemOrders::find("order_number = '{$order->order_number}'");
            $this->view->setVar('order', $order);
            $this->view->setVar('resources', $resources->toArray());
        }
    }

    /**
     * @return \Components\ModuleManager\ModuleHandler | null
     */
    protected function getModule()
    {
        $params = array(
            'customer' => $this->customer,
            'customer_id' => CUR_APP_ID,
            'customer_wechat' => $this->customer_wechat,
            'module' => $this->_module,
            'handler' => $this->_handler,
            'action' => $this->_action
        );
        $module = ModuleManager::instance(HOST_KEY, CUR_APP_ID)->getModuleHandler($this->_module, $this->_handler, $params);

        MenuHelper::setModule($this->_module);
        MenuHelper::setHandler($this->_handler);
        MenuHelper::setAction($this->_action);
        return $module;
    }

    public function mineAction()
    {
        \Phalcon\Tag::setTitle("模块管理——我购买的模块");
        $this->assets->addCss('static/panel/css/module/module.mine.css');
        $mine = ModuleManager::instance(HOST_KEY, CUR_APP_ID)->getCustomerModules();
        if ($mine) {
            $this->view->setVar("mineModules", $mine);
        }
    }

    public function moreAction()
    {
        \Phalcon\Tag::setTitle("模块管理——所有模块列表");
        $this->assets->addCss('static/panel/css/module/module.mine.css');
        $resources = ModuleManager::instance(HOST_KEY, CUR_APP_ID)->getSystemModules(true);
        if ($resources) {
            $this->view->setVar("resources", $resources);
        }
    }

    public function ordersAction()
    {
        \Phalcon\Tag::setTitle("模块管理——订单管理");

        $unpaidOrder = SystemOrders::find("customer_id=" . CUR_APP_ID . " AND status='0'");
        if ($unpaidOrder) {
            $this->view->setVar("unpaidOrders", $unpaidOrder);
        }

        $currentPage = $this->request->get('page', 'int');
        $date_start = $this->request->get('date_start', 'int');
        $date_end = $this->request->get('date_end', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\SystemOrders', 'so')
            ->where("so.customer_id=" . CUR_APP_ID . " AND so.status > '0'");

        if ($date_start) {
            $queryBuilder->andWhere("so.ordered > '{$date_start}'");
            if ($date_end && $date_end > $date_start) {
                $queryBuilder->andWhere("so.ordered < '{$date_start}'");
            }
        } else {
            if ($date_end) {
                $queryBuilder->andWhere("so.ordered < '{$date_start}'");
            }
        }

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 20,
            "page" => $currentPage
        ));
        $this->view->setVar('list', $pagination->getPaginate());
    }

    public function orderDetailAction()
    {
        Tag::setTitle("订单详情");
        $order_number = $this->request->get('order');
        $order = SystemOrders::findFirst("order_number = '{$order_number}'");
        if ($order) {
            $this->session->set('current_order', $order);
            $resources = ResourceForSystemOrders::findFirst("customer_id='" . CUR_APP_ID . "' AND order_number='{$order_number}'");
            $this->view->setVar("order", $order);
            $this->view->setVar("bought_modules", $resources);
        }
    }

    public function detailAction($module_name = null)
    {
        if (empty($module_name)) {
            $module_name = $this->dispatcher->getParam(0);
        }
        if (empty($module_name)) {
            return $this->err('400', '对不起，模块未找到');
        } else {
            $mm = ModuleManager::instance(HOST_KEY);
            $module = $mm->getModuleResource($module_name);

            if (!$module) {
                $this->flash->error("对不起，暂时没有提供此项服务");
            } else {
                $isAuthed = $mm->checkAuth($module_name, CUR_APP_ID);
                $deadline = $mm->getCustomerModuleDeadLine($module_name);
                $this->view->setVar('module', $module);
                $this->view->setVar("isAuthed", $isAuthed);
                $this->view->setVar("deadline", $deadline);
            }

        }
    }

    public function trialAction()
    {
        if (empty($this->customer) || !$this->customer instanceof Customers) {
            $this->response->redirect('account/login');
        }
        if ($this->customer->active > 0) {

        }
        $this->view->disable();
        $order = $this->session->get('current_order');
        if (!$order instanceof SystemOrders) {
            if (!is_string($order)) {
                $this->response->redirect("account/registerAfter");
            }
            $order = SystemOrders::findFirst("order_number='{$order}' AND customer_id=" . CUR_APP_ID . "");
            if (!$order) {
                $this->response->redirect("panel/module/more");
            }
        }

        $resources = ResourceForSystemOrders::query()->leftJoin("\\Models\\Modules\\Resources", 'r.id = resource_id', 'r')->andWhere("order_number='{$order->order_number}'")->execute();
        $this->db->begin();
        try {
            if ($resources) {
                $time_end = strtotime('+6 month');
                foreach ($resources as $resource) {
                    $rfc = ResourceForCustomers::findFirst("resource_id='{$resource->resource_id}' AND customer_id=" . CUR_APP_ID . "");
                    if (!$rfc) {
                        $rfc = new ResourceForCustomers();
                        $rfc->customer_id = $this->customer->id;
                        $rfc->resource_id = $resource->resource_id;
                        $rfc->time_end = $time_end;
                        $rfc->link = $resource->link;
                        if (!$rfc->save()) {
                            $messages = [];
                            foreach ($rfc->getMessages() as $message) {
                                $messages[] = $message;
                            }
                            throw new \Phalcon\Exception("为试用客户保存服务信息失败！" . join(',', $messages));
                        }
                        ModuleManager::instance(HOST_KEY, CUR_APP_ID)->getCustomerModules(null, true);
                    } else {
                        throw new \Phalcon\Exception("对不起，您已经试用过此功能，请联系客服续费!");
                    }
                }
            } else {
                $this->flash->error("对不起，没有找到该服务或功能。");
            }
            $this->db->commit();

            //return $this->response->redirect("panel/module/mine");
        } catch (\Phalcon\Exception $e) {
            $this->db->rollback();
            $this->flash->error($e->getMessage());
            return $this->forward('module/editOrder');
        }
        return $this->response->redirect("panel/module/mine");
    }

    public function addFreeAction($module_name = null)
    {

        if (empty($module_name)) {
            $this->flash->error("对不起，没有指定要添加的服务");
        }

        $this->db->begin();
        try {
            $resource = ModuleManager::instance(HOST_KEY)->getModuleResource($module_name);
            if ($resource) {
                $rfc = ResourceForCustomers::findFirst("resource_id='{$resource->id}' AND customer_id=" . CUR_APP_ID . "");
                if ($rfc && $rfc->time_end > time()) {
                    throw new \Phalcon\Exception("对不起，您已经添加过此服务，请联系客服续费!");
                }
                if (!$rfc) {
                    $rfc = new ResourceForCustomers();
                    $rfc->customer_id = $this->customer->id;
                    $rfc->resource_id = $resource->id;
                    $rfc->link = $resource->link;
                }

                $rfc->time_end = time('6 month');
                if (!$rfc->save()) {
                    $messages = [];
                    foreach ($rfc->getMessages() as $message) {
                        $messages[] = $message;
                    }
                    throw new \Phalcon\Exception("添加免费服务失败！" . join(',', $messages));
                }

                ModuleManager::instance(HOST_KEY, CUR_APP_ID)->getCustomerModules(null, true);
            } else {
                throw new \Phalcon\Exception("对不起，没有找到该服务或功能。");
            }
            $this->db->commit();
        } catch (\Phalcon\Exception $e) {
            $this->db->rollback();
            $this->flash->error($e->getMessage());
            return $this->forward('module/detail' . $module_name);
        }
        return $this->response->redirect("panel/module/mine");
    }

}

?>
