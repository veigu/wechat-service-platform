<?php

namespace Multiple\Panel\Controllers;

use Components\CustomerManager;
use Models\Customer\Customers;
use Phalcon\Mvc\Controller;
use Phalcon\Tag;

class PanelBase extends Controller
{
    /**
     * @var \Models\Customer\Customers
     */
    protected $customer = null;
    protected $customer_id = 0;

    /**
     * @var \Models\Customer\CustomerOpenInfo
     */
    protected $customer_wechat = null;

    /**
     * @var \Models\Customer\CustomerOpenInfo
     */
    protected $customer_weibo = null;

    public function initialize()
    {
        $auth = $this->session->get('customer_auth');

        $this->view->setVar('auth', $auth);
        $this->view->setLayout('index');

        if (empty($auth)) {
            $this->session->set("current_request_url", $this->uri->fullUrl());
            $this->response->redirect('account/login')->send();
            return;
        }

        $this->customer = $this->session->get("customer_info");
        if (!$this->customer instanceof Customers) {
            $this->response->redirect('account/login')->send();
            return;
        }

        // 未填写基础资料时跳转
        $current_request_url = $this->session->get("current_request_url");
        $redirect = CustomerManager::init()->checkJoinStepRedirect($this->customer);
        if ($redirect != '/panel' && $redirect != $current_request_url) {
            $this->response->redirect(ltrim($redirect, '/'))->send();
            return;
        }

        $this->customer_wechat = $this->session->get('customer_wechat');
        $this->customer_weibo = $this->session->get('customer_weibo');
        if ($this->customer_wechat) {
            $customer_avatar = $this->customer_wechat->avatar;
            $this->view->setVar('customer_avatar', $customer_avatar);
        } else {
//            $this->flash->notice("您还没有绑定微信公众号");
        }
        if (!$this->customer_weibo) {
//            $this->flash->notice("您还没有绑定企业微博粉丝服务");
        }

        if (!defined('CUR_APP_ID')) define('CUR_APP_ID', $this->customer->id);

        $this->view->setVar("customer_id", CUR_APP_ID);
        $this->customer_id = CUR_APP_ID;
        $this->view->setVar("customer", $this->customer);
        $this->view->setVar("customer_package", $this->session->get('customer_package'));
        $customer_name = empty($this->customer->name) ? $this->customer->account : $this->customer->name;
        $this->view->setVar("customer_name", $customer_name);
    }

    /**
     * render custom err page
     *
     * usage:
     * return $this->err();
     *
     * @param string $code
     * @param string $msg
     * @return mixed
     */
    protected function err($code = "404", $msg = '404 page no found')
    {
        Tag::setTitle('运行时错误');
        $this->view->setViewsDir(MODULE_PATH . '/Views');
        $this->response->setHeader('content-type', 'text/html;charset=utf-8');
        $this->response->setStatusCode($code, $msg);

        $this->view->setVar('msg', $msg);

        return $this->view->pick('base/error');
    }


    protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $partsNum = count($uriParts);
        switch ($partsNum) {
            case 2:
                return $this->dispatcher->forward(
                    array(
                        'controller' => $uriParts[0],
                        'action' => $uriParts[1]
                    )
                );
                break;
            case 3:
                return $this->dispatcher->forward(
                    array(
                        'namespace' => $uriParts[0],
                        'controller' => $uriParts[1],
                        'action' => $uriParts[2]
                    )
                );
                break;
        }
    }
}
