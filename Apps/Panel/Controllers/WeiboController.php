<?php

namespace Multiple\Panel\Controllers;

use Components\UserManager;
use Components\WeChat\MenuManager;
use Components\WeChat\MessageManager;
use Components\WeChat\ResourceManager;
use Components\WeChat\WeiBoAutoImportSync;
use Components\WeiBo\SaeTClientV2;
use Components\WeiBo\WeiBoAutoImport;
use Models\Customer\CustomerOpenInfo;
use Models\WeChat\CustomerMenus;
use Models\WeiBo\MessageHistory;
use Models\WeChat\MessageSettingKeywords;
use Models\WeChat\MessageSettingMass;
use Models\WeChat\MessageSettings;

class WeiboController extends PanelBase
{
    private $wx;

    public function initialize()
    {
        parent::initialize();
        if (empty($this->customer_weibo->is_binded) && ($this->dispatcher->getActionName() != 'auto' && $this->dispatcher->getActionName() != 'binding')) {
            return $this->forward('weibo/binding');
        }
    }

    public function autoAction()
    {
        \Phalcon\Tag::setTitle("微博粉丝服务绑定");
        $this->flash->success('请使用微博账号登陆后自动绑定');
        if ($this->request->isPost()) {
            $user = trim($this->request->getPost('wx_user', array("striptags")), '');
            $pass = trim($this->request->getPost('wx_pass',array("striptags"), ''));
            $imgcode = trim($this->request->getPost('wx_imgcode', array("striptags"), ''));
//            $user = 'weixin@estt.com.cn';
//            $pass = 'estt@123';
            $wx = new WeiBoAutoImport($user, $pass);
            $this->wx = $wx;
            if (!$wx->init(CUR_APP_ID, $user, $pass, $imgcode)) {
                $this->flash->error($wx->getMsg());
                $this->view->setVar('user', $user);
                $this->view->setVar('pass', $pass);
                $this->view->setVar('verifycode', $wx->getCodeImg($user));
            } else {
                WeiBoAutoImportSync::init()->save($wx);
            }
        }
    }

    public function syncAction()
    {
        $this->view->disable();
        set_time_limit(0);
//        if ($this->request->isPost()) {
        $cm = CustomerOpenInfo::findFirst('customer_id=' . CUR_APP_ID);

        // 获取用户及组
        $res = MenuManager::instance()->syncFromWeChat(CUR_APP_ID, $cm->app_id, $cm->app_secret);
        $this->out($res, '6. 同步菜单');

        $res = UserManager::instance()->syncGroupFromWeChat(CUR_APP_ID, $cm->app_id, $cm->app_secret);
        $this->out($res, '7. 同步组');

        $res = UserManager::instance()->syncUserFromWechat(CUR_APP_ID, $cm->app_id, $cm->app_secret);
        $this->out($res, '8. 同步用户');
//        }
    }


    public function out($status, $msg)
    {
//        ob_start();

        if ($status) {
            echo $msg . '成功</br>';
        } else {
            echo '！！' . $msg . '失败</br>';
        }

//        ob_end_clean();

    }

    public function respondAction($type = null)
    {
        if(empty($type)) {
            $type = MessageManager::EVENT_TYPE_SUBSCRIBE;
        }
        else {
            if(!in_array($type, array(
                MessageManager::EVENT_TYPE_SUBSCRIBE,
                MessageManager::EVENT_TYPE_NORMAL,
                MessageManager::EVENT_TYPE_KEYWORD
            ))) {
                $this->flash->error("您指定的设置不存在，已经给你调整为关注时的设置");
                $type = MessageManager::EVENT_TYPE_SUBSCRIBE;
            }
        }
        \Phalcon\Tag::setTitle('微博粉丝服务-回复消息设置');
        $this->assets->addCss('static/panel/css/module/weibo/weibo.respond.css');
        $this->assets->addJs('static/panel/js/app/module/weibo/weibo.respond.js');

        $data = [];
        switch($type) {
            case MessageManager::EVENT_TYPE_SUBSCRIBE: {
                $data = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer->id, MessageManager::EVENT_TYPE_SUBSCRIBE, true);
                break;
            }case MessageManager::EVENT_TYPE_NORMAL: {
                $data = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer->id, MessageManager::EVENT_TYPE_NORMAL, true);
                break;
            }
            case MessageManager::EVENT_TYPE_KEYWORD: {
                $this->flash->error("为了保障系统性能，限制每个用户不得设置超过10个规则");
                $data = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer->id, MessageManager::EVENT_TYPE_KEYWORD, true);
                break;
            }
        }

        $messageType = isset($data['message_type']) ? $data['message_type'] : ResourceManager::MESSAGE_TYPE_TEXT;
        $this->view->setVar('message', $data);
        $this->view->setVar('message_type', $messageType);
        $this->view->setVar('messageSettingJson', json_encode($data));
        $this->view->setVar("respond_type", $type);
    }

    public function messageAction($type = null)
    {
        \Phalcon\Tag::setTitle('微信聚合平台-历史消息记录');
        $currentPage = $this->request->get('p', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $pageSize = 20;
        $skip = ($currentPage - 1) * $pageSize;

        $condition = ["customer_id" => CUR_APP_ID];
        if($type && $type == 1) {
            $condition['is_replied'] = 1;
        }
        else if(empty($type)) {
            $condition['is_replied'] = 0;
        }

        $data = MessageHistory::find(array($condition, 'sort' => 'created', 'limit' => $pageSize, "skip" => $skip));

        $this->view->setVar('list', $data);
    }

    public function saveAction()
    {
        $this->view->disable();
        $result = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if ($this->request->isPost()) {
            $respondType = $this->request->getPost('respondType', "string");
            $messageType = $this->request->getPost('messageType', "string");
            $message = $this->request->getPost('value', "string");
            $defaultLink = $this->request->getPost('link', 'string');

            $messageSetting = MessageSettings::findFirst("customer_id=".CUR_APP_ID." AND event_type='{$respondType}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO . "'");
            if (!$messageSetting) {
                $messageSetting = new MessageSettings();
                $messageSetting->customer_id = $this->customer->id;
                $messageSetting->event_type = $respondType;
                $messageSetting->platform = MessageManager::PLATFORM_TYPE_WEIBO;
                $messageSetting->created = time();
            }
            $messageSetting->message_type = $messageType;
            $messageSetting->value = $message;
            $messageSetting->default_link = $defaultLink;

            if ($messageSetting->save() == false) {
                $result['code'] = 1;
                $result['message'] = $messageSetting->getMessages();
            }
            MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer->id, $respondType, true);
        }
        $this->response->setHeader('content-type', 'text/json;charset=utf-8');
        $this->response->setJsonContent($result);
        $this->response->send();
    }

    public function saveKeywordAction()
    {
        $this->view->disable();
        $result = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if ($this->request->isPost()) {
            $kid = $this->request->getPost('kid', 'int');
            $ruleName = $this->request->getPost('ruleName', "string");
            $keywords = $this->request->getPost('keywords');
            $messageType = $this->request->getPost('messageType', 'string');
            $messageData = $this->request->getPost('messageData', "string");
            $messageLink = $this->request->getPost('messageLink', 'string');

            $count = MessageSettingMass::count("customer_id=" . CUR_APP_ID . "");

            if($count > 10) {
                $result['code'] = 1;
                $result['message'] = "对不起，您已经设置了10个关键字规则，已经达到系统限制上限，您可以删除不常用的规则，然后再添加新规则";
            }
            else {
                if ($kid > 0) {
                    $messageSetting = MessageSettingKeywords::findFirst("customer_id=" . CUR_APP_ID . " AND id='{$kid}'");
                } else {
                    $messageSetting = new MessageSettingKeywords();
                    $messageSetting->customer_id = $this->customer->id;
                    $messageSetting->platform = MessageManager::PLATFORM_TYPE_WEIBO;
                }
                $messageSetting->name = $ruleName;
                $messageSetting->keywords = json_encode($keywords);
                $messageSetting->message_type = $messageType;
                $messageSetting->respond_message = $messageData;
                $messageSetting->default_link = $messageLink;
                if ($messageSetting->save() == false) {
                    $result['code'] = 1;
                    $result['message'] = $errorMessage = $messageSetting->getMessages();
                } else {
                    $result['result'] = $messageSetting->id;
                }
                MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer->id, MessageManager::EVENT_TYPE_KEYWORD, true);
            }
        }
        $this->response->setHeader('content-type', 'text/json;charset=utf-8');
        $this->response->setJsonContent($result);
        return $this->response->send();
    }

    public function removeKeywordAction()
    {
        $this->view->disable();
        $result = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if ($this->request->isPost()) {
            $kid = $this->request->getPost('kid', 'int');
            if (empty($kid)) {
                $result['code'] = 1;
                $result['message'] = "没有指定要删除的关键词";
            } else {
                $messageSetting = MessageSettingKeywords::findFirst("customer_id=" . CUR_APP_ID . " AND id='{$kid}'");
                if (!$messageSetting) {
                    $result['code'] = 1;
                    $result['message'] = "找不到指定的关键词规则";
                } else {
                    if ($messageSetting->delete() == false) {
                        $result['code'] = 1;
                        $result['message'] = $errorMessage = $messageSetting->getMessages();
                        MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer->id, MessageManager::EVENT_TYPE_KEYWORD, true);
                    } else {
                        $result['result'] = $messageSetting->id;
                    }
                }
            }
        }
      return  $result;
    }

    public function bindingAction()
    {
        \Phalcon\Tag::setTitle('微信聚合平台-微信设置');
        if ($this->request->isPost()) {
            $token = $this->request->getPost("token", 'string');
            $app_account = $this->request->getPost('app_account', 'string');
            $app_id = $this->request->getPost('app_id', 'string');
            $app_secret = $this->request->getPost('app_secret', 'string');
            $openInfo = CustomerOpenInfo::findFirst("customer_id=" . CUR_APP_ID . " AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO . "'");

            if (!$openInfo) {
                $openInfo = new CustomerOpenInfo();
                $openInfo->platform = MessageManager::PLATFORM_TYPE_WEIBO;
                $openInfo->customer_id = CUR_APP_ID;
            }

            $openInfo->token = $token;
            $openInfo->app_account = $app_account;
            $openInfo->app_id = $app_id;
            $openInfo->app_secret = $app_secret;
            $openInfo->original_id = '';
            $openInfo->enable_dkf = 0;
            if (!$openInfo->save()) {
                foreach ($openInfo->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                $this->session->set('customer_weibo', $openInfo);
                $this->flash->success("数据更新成功");
            }
        }

        $openInfo = CustomerOpenInfo::findFirst("customer_id=" . CUR_APP_ID . " AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO . "'");

        $this->view->setVar("openInfo", $openInfo);
        $this->view->setVar('customer_id', CUR_APP_ID);
        if ($openInfo && $openInfo->app_id && $openInfo->app_secret && empty($openInfo->is_binded)) {
            $this->flash->notice("您需要在您的微博首页粉丝服务的开发者模式下设置回调URL和验证TOKEN");
        } elseif (empty($openInfo->app_id) || empty($openInfo->app_secret)) {
            $this->flash->notice("您需要填写微博粉丝服务应用的appkey和appsecret，请参考下面右侧的说明。");
        }
    }

    public function refreshTokenAction()
    {
        $this->view->disable();
        echo json_encode(array(
            "token" => "aaaa"
        ));
    }

    public function menuAction()
    {
        \Phalcon\Tag::setTitle('微博粉丝服务-菜单设置');

        $this->assets->addCss('static/panel/css/module/weibo/weibo.message.css');
        $this->assets->addJs('static/ace/js/jquery.nestable.min.js');
        $this->assets->addJs('static/ace/js/bootbox.min.js');
        $this->assets->addJs('static/panel/js/app/module/weibo/weibo.menu.js');
        $menus = MenuManager::instance()->getMenus($this->customer->id, MessageManager::PLATFORM_TYPE_WEIBO);
        if (!$menus && $this->customer_weibo) {
            MenuManager::instance()->syncFromWeiBo($this->customer->id, $this->customer_weibo->app_id, $this->customer_weibo->app_secret, $this->customer_weibo->token);
            $menus = MenuManager::instance()->getMenus($this->customer->id, MessageManager::PLATFORM_TYPE_WEIBO);
        }

        $this->view->setVar('menus', $menus);
    }

    public function menuAddAction()
    {
        $this->view->disable();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '只支持AJAX请求。'
            ))->send();
            exit;
        }
        $mid = $this->request->getPost('mid', 'int');
        $name = $this->request->getPost('name', 'string');
        $sort = $this->request->getPost('sort', 'int', 0);
        if (empty($name)) {
            $data['code'] = 1;
            $data['message'][] = "菜单名称必须填写。";
        }
        $pid = $this->request->getPost('pid', 'int');
        if (empty($mid)) {
            if (empty($pid)) {
                $pid = 0;
                $topMenuCount = CustomerMenus::count(array("customer_id=" . CUR_APP_ID . " AND pid='0' AND platform='"  . MessageManager::PLATFORM_TYPE_WEIBO. "'"));
                if ($topMenuCount >= 3) {
                    $data['code'] = 1;
                    $data['message'][] = '您已经添加了3个顶级菜单，不能再添加了！';
                }
            } else {
                $topMenuCount = CustomerMenus::count(array("customer_id=" . CUR_APP_ID . " AND pid='{$pid}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO . "'"));
                if ($topMenuCount >= 5) {
                    $data['code'] = 1;
                    $data['message'][] = '您已经添加了5个子菜单，不能再添加了！';
                }
            }
        }
        if ($data['code'] > 0) {
            $this->response->setJsonContent($data);
            return $this->response->send();
        }
        $type = strtoupper($this->request->getPost('type', 'string'));
        if (empty($type)) {
            $data['code'] = 1;
            $data['message'][] = "菜单类型必须指定";
        }
        $target_value = $this->request->getPost('value', 'string');
        $defaultLink = $this->request->getPost('default_link', 'string');

        $eventKey = '';
        $messageType = '';
        if ($type == MenuManager::TYPE_VIEW) {
            if (empty($target_value)) {
                $data['code'] = 1;
                $data['message'][] = "没有指定菜单到达的目标链接。";
            } else if (!filter_var($target_value, FILTER_VALIDATE_URL)) {
                $data['code'] = 1;
                $data['message'][] = "目标链接格式不正确。";
            }
        } else if ($type == MenuManager::TYPE_CLICK) {
            $messageType = $this->request->getPost('messageType', 'string');
            if (empty($messageType)) {
                $messageType = ResourceManager::MESSAGE_TYPE_TEXT;
            }
            $eventKey = sha1(CUR_APP_ID + time() + "event_key" + $type + $messageType);
            if (empty($target_value)) {
                $data['code'] = 1;
                $data['message'][] = "没有设置菜单的响应信息。";
            }
            if ($defaultLink && strlen($defaultLink) > 0 && !filter_var($defaultLink, FILTER_VALIDATE_URL)) {
                $data['code'] = 1;
                $data['message'][] = "默认链接的格式不正确。";
            }
        }

        if ($data['code'] > 0) {
            $this->response->setJsonContent($data);
            return $this->response->send();
        }

        if ($mid > 0) {
            $menuItem = CustomerMenus::findFirst("id='{$mid}'");
            if ($menuItem) {
                if (!$menuItem->update(array(
                    'name' => $name,
                    'pid' => $pid,
                    'type' => $type,
                    'platform' => MessageManager::PLATFORM_TYPE_WEIBO,
                    'target_value' => $target_value,
                    'default_link' => $defaultLink,
                    'sort' => $sort,
                    'message_type' => $messageType,
                    'key' => $eventKey
                ))
                ) {
                    $data['code'] = 1;
                    foreach ($menuItem->getMessages() as $message) {
                        $data['message'][] = (string)$message;
                    }
                }
            }
        } else {
            $menuItem = new CustomerMenus();
            $menuItem->platform = MessageManager::PLATFORM_TYPE_WEIBO;
            $menuItem->customer_id = $this->customer->id;
            $menuItem->name = $name;
            $menuItem->pid = $pid;
            $menuItem->type = $type;
            $menuItem->target_value = $target_value;
            $menuItem->sort = $sort;
            $menuItem->created = time();
            $menuItem->message_type = $messageType;
            $menuItem->key = $eventKey;
            if (!$menuItem->save()) {
                $data['code'] = 1;
                foreach ($menuItem->getMessages() as $message) {
                    $data['message'][] = (string)$message;
                }
            }
        }

        if ($data['code'] == 0 && $pid > 0) {
            $parentMenu = CustomerMenus::findFirst("id='{$pid}' AND customer_id=".CUR_APP_ID."");
            if ($parentMenu) {
                $parentMenu->update(array(
                    'type' => MenuManager::TYPE_TOP_BAR
                ));
            }
        }
        //Automatically sync to weibo server
        MenuManager::instance()->syncToWeiBo(CUR_APP_ID, $this->customer_weibo->app_id, $this->customer_weibo->app_secret, $this->customer_weibo->token);

        $this->response->setJsonContent($data);
        $this->response->send();
    }

    public function syncToWeiBoAction() {
        $this->view->disable();
        $code = 0;
        $message = '';
        try {
            MenuManager::instance()->syncToWeiBo(CUR_APP_ID, $this->customer_weibo->app_id, $this->customer_weibo->app_secret, $this->customer_weibo->token);
        }
        catch(\Phalcon\Exception $e) {
            $code = 1;
            $message = $e->getMessage();
        }

        $this->response->setJsonContent(array(
            'code' => $code,
            'message' => $message
        ));
        return $this->response->send();
    }

    public function menuRemoveAction()
    {
        $this->view->disable();
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '只支持AJAX请求'
            ))->send();
        }

        $mid = $this->request->getPost('mid', 'int');

        if (!$mid || $mid <= 0) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '请指定要删除的菜单'
            ))->send();
        } else {
            $data = array(
                'code' => 0,
                'message' => '',
                'result' => ''
            );
            $menuItem = CustomerMenus::findFirst("id='{$mid}' AND customer_id=".CUR_APP_ID." AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO . "'");
            if (!$menuItem->delete()) {
                $data['code'] = 1;
                foreach ($menuItem->getMessages() as $message) {
                    $data['message'][] = $message;
                }
            } else {
                //Automatically sync to WeChat server
                MenuManager::instance()->syncToWeiBo(CUR_APP_ID, $this->customer_weibo->app_id, $this->customer_weibo->app_secret, $this->customer_weibo->token);
            }
            $data;
        }
    }

    public function massAction()
    {
        $monthStartTime = strtotime('last month');
        $thisMonthMassCount = MessageSettingMass::count(array("customer_id=" . CUR_APP_ID . " AND time >= '{$monthStartTime}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO ."'"));
        $this->view->setVar('monthCount', $thisMonthMassCount);
//        $available = 4 - $thisMonthMassCount;
        if ($this->request->isAjax() || $this->request->isPost()) {
            $this->view->disable();
            /*if ($thisMonthMassCount > 4) {
                return $this->response->setJsonContent(array(
                    'code' => 1,
                    'message' => "您本月已经发送了4条群发，请等到下月再发吧",
                    'result' => ''
                ))->send();
            }*/
            $group = $this->request->getPost('group', "int", false);
            $gender = $this->request->getPost("gender", array('striptags', 'string'), false);
            $messageType = $this->request->getPost('messageType', 'striptags', "text");
            $message = $this->request->getPost('messages');
            $defaultLink = $this->request->getPost('default_link', 'string');
            $code = 0;
            $messages = '';
            try {
                $massMessage = new MessageSettingMass();
                $massMessage->customer_id = CUR_APP_ID;
                $massMessage->platform = MessageManager::PLATFORM_TYPE_WEIBO;
                $massMessage->message_type = $messageType;
                $massMessage->message = $message;
                $massMessage->default_link = $defaultLink;
                $massMessage->group = $group;
                $massMessage->gender = $gender;
                $massMessage->is_send = 0;
                $massMessage->time = time();
                if ($massMessage->save()) {
                    //加入到消息队列
                    $beanstalk =  $this->di->get('beanstalk');
                    $jobId = $beanstalk->put(array("mass-message-queue", array(
                        'customer_id' => CUR_APP_ID,
                        'group' => $group,
                        'gender' => $gender,
                        'messageType' => $messageType,
                        'messages' => $message,
                        'platform' => MessageManager::PLATFORM_TYPE_WEIBO
                    )));
                    if($jobId) {
                        $massMessage->update(array('is_send' => 1, 'queue_job_id' => $jobId));
                    }
                } else {
                    $messages = [];
                    foreach ($massMessage->getMessages() as $message) {
                        $messages[] = (string)$message;
                    }
                    throw new \Phalcon\Exception(join(',', $messages));
                }
                var_dump($massMessage->toArray());exit;
            } catch (\Exception $e) {
                $code = 1;
                $messages = $e->getMessage();
            }

            return $this->response->setJsonContent(array(
                'code' => $code,
                'message' => $messages,
                'result' => ''
            ))->send();
        }
        \Phalcon\Tag::setTitle("微博粉丝服务-消息群发");
        $this->assets->addJs('static/panel/js/app/module/weibo/weibo.mass.js');
        $this->assets->addCss('static/panel/css/module/weibo/weibo.message.css');

        $this->flash->success("本月您已经群发<code>{$thisMonthMassCount}</code>条消息");
        $groups = UserManager::instance()->getWeiboGroups(CUR_APP_ID, true);
        $this->view->setVar('groups', $groups);
    }
}
