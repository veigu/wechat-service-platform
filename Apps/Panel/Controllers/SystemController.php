<?php
/**
 * Created by PhpStorm.
 * User: luguiwu
 * Date: 14-12-2
 * Time: 上午11:16
 */

namespace Multiple\Panel\Controllers;


use Components\StaticFileManager;
use Components\TplManager;
use Models\Shop\Shop;
use Models\Wap\SiteFocus;
use Models\Wap\SiteNavs;
use Models\Wap\SiteTpl;
use Phalcon\Tag;

class SystemController extends PanelBase {
    public $store_info = null;

    protected $tpl;
    public function initialize()
    {
        parent::initialize();
        $storeInfo = Shop::findFirst("customer_id=" . CUR_APP_ID . "");
        if ($storeInfo instanceof Shop) {
            $this->store_info = $storeInfo;
        }
        $this->tpl = $tpl = TplManager::init(CUR_APP_ID)->getTpl(true);

        $this->view->setVar('tpl', $tpl);
    }

    public function navAction()
    {
        Tag::setTitle('网站设置 - 微网站分类导航 ');

        StaticFileManager::addCssFile('static/wap/css/color.css');

        $navs = SiteNavs::find('customer_id=' . CUR_APP_ID . " and position = 'part'");
        $this->view->setVar('list', $navs ? $navs->toArray() : []);
    }

    public function footAction()
    {
        Tag::setTitle('网站设置 - 底部快捷导航');

        StaticFileManager::addCssFile('static/wap/css/color.css');

        $navs = SiteNavs::find('customer_id=' . CUR_APP_ID . " and position = 'foot'");
        $this->view->setVar('list', $navs ? $navs->toArray() : []);
    }

    public function focusAction()
    {
        Tag::setTitle('网站设置 - 微网站焦点图片');

        $focus = SiteFocus::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('list', $focus ? $focus->toArray() : []);
    }

    public function tplAction()
    {
        Tag::setTitle('我的模板 - 模板选择');
        $type = 'shop';
        $all = SiteTpl::find(array('type="' . $type . '"', 'order' => "serial_number asc"));
        $this->view->setVar('list', $all ? $all->toArray() : []);
        $curTpl = json_decode($this->tpl[$type], true);


        $this->view->setVar('curTpl_serial', $curTpl['serial_number']);

     //   Tag::setTitle('单品模板  - ' . $this->title[$type]);
    }

} 