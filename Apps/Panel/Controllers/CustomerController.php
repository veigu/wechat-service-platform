<?php
namespace Multiple\Panel\Controllers;

use Components\IndustryManager;
use Models\Customer\CustomerPackage;
use Models\Customer\CustomerProfile;
use Models\Customer\Customers;
use Models\Modules\Resources;
use Models\O2o\O2oCustomerApply;
use Models\System\SystemOrders;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\View;
use Phalcon\Tag as Tag;

class CustomerController extends Controller
{
    public $customer;


    public function initialize()
    {
        $customer = $this->session->get('customer_info');
        if ($customer instanceof Customers) {
            $this->customer = $customer;
        }

        if (!defined('CUR_APP_ID')) define('CUR_APP_ID', $this->customer->id);

        $this->view->setVar('customer', $customer);

        $this->view->setMainView('account');
        $this->view->setLayout('account');
    }

    public function profileAction()
    {
        Tag::setTitle("企业资料");
        $industry = IndustryManager::instance()->getTreeData(true);
        $profile = CustomerProfile::findFirst('customer_id=' . CUR_APP_ID);
        $customer = Customers::findFirst('id=' . CUR_APP_ID);
        $this->view->setVar('profile', $profile ? $profile->toArray() : []);
        $this->view->setVar('industry', $industry);
        $this->view->setVar('customer', $customer);
    }

    // 套餐详情
    public function packageAction()
    {
        Tag::setTitle("套餐详情");
    }

    // 选择套装
    public function pickAction()
    {
        Tag::setTitle("行业套餐");
        $industry = IndustryManager::instance()->getTreeData(true);
        $this->view->setVar('industry', $industry);
    }

    //自定义套餐
    public function customAction()
    {
        Tag::setTitle("自选套餐");
        $industry = IndustryManager::instance()->getTreeData(true);
        $this->view->setVar('industry', $industry);

        $module = Resources::find();

        $choosed_package = CustomerPackage::findFirst('customer_id=' . CUR_APP_ID . ' and package="custom"');
        $unpaid_order = SystemOrders::findFirst(array('customer_id=' . CUR_APP_ID . ' and is_paid=0 and package!="" and package!="custom"', 'order' => "ordered desc"));

        $this->view->setVar('modules', $module->toArray());
        $this->view->setVar('choosed_package', $choosed_package ? $choosed_package->toArray() : []);
        $this->view->setVar('unpaid_order', $unpaid_order ? $unpaid_order->toArray() : []);
    }

    public function orderAction()
    {
        Tag::setTitle("确认订单");
        $package_info = CustomerPackage::findFirst("customer_id=" . CUR_APP_ID);
        $this->view->setVar('package_info', $package_info ? $package_info->toArray() : '');
    }

    public function o2oAction()
    {
        Tag::setTitle("申请入住O2O平台");
        $apply = O2oCustomerApply::findFirst('customer_id=' . CUR_APP_ID);
        $this->view->setVar('apply', $apply ? $apply->toArray() : []);
    }
}
