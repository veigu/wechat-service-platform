<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 5/14/14
 * Time: 10:50 AM
 */

namespace Multiple\Panel\Plugins;


use Phalcon\Mvc\User\Plugin;

class PanelMenu extends Plugin
{
    protected static $_headMenu = array(
        'console' => array('name' => '控制面板', 'uri' => 'panel', 'active' => array('index', 'setting')),
        'web' => array('name' => '微站管理', 'uri' => 'panel/site/info', 'active' => array('site', 'article', 'tpl')),
        'user' => array('name' => '用户管理', 'uri' => 'panel/users/index', 'active' => array('users')),
        'trd_party' => array('name' => '微信接入', 'uri' => 'panel/wechat/menu', 'active' => array('wechat', 'weibo')),
        'module' => array('name' => '功能模块', 'uri' => 'panel/module/mine', 'active' => array('module')),
    );

    protected static $_sideFstMenu = array(
        'wechat' => array('head' => 'trd_party', 'name' => '微信接入', 'class' => "icon-retweet"),
        'weibo' => array('head' => 'trd_party', 'name' => '微博接入', 'class' => "icon-retweet"),
        'web_set' => array('head' => 'web', 'name' => '网站设置', 'class' => 'icon-home'),
        'web_page' => array('head' => 'web', 'name' => '公司信息', 'class' => 'icon-exclamation-sign'),
        'web_post' => array('head' => 'web', 'name' => '内容管理', 'class' => 'icon-book'),
        'web_tpl' => array('head' => 'web', 'name' => '微站模板', 'class' => 'icon-tablet'),

        'user-setting' => array('head' => 'user', 'name' => '基础设置', 'c' => 'users', 'a' => 'setting', 'class' => "icon-cogs"),
        'user-rules' => array('head' => 'user', 'name' => '积分规则', 'c' => 'users', 'a' => 'rules', 'class' => "icon-asterisk"),
        'user-list' => array('head' => 'user', 'name' => '用户信息', 'class' => "icon-check"),
//        'user-group' => array('head' => 'user', 'name' => '分组设置', 'class' => "icon-group"),
        'user-card' => array('head' => 'user', 'name' => '会员卡', 'm' => 'vipcard', 'c' => 'card', 'a' => 'index', 'class' => "icon-credit-card", "is_module" => true),
        'more' => array('head' => 'module', 'name' => '所有功能', 'c' => 'module', 'a' => 'more', 'class' => 'icon-globe'),
        'mine' => array('head' => 'module', 'name' => '我的功能', 'c' => 'module', 'a' => 'mine', 'class' => 'icon-gift'),

        // 放在最后面
        'console_info' => array('head' => 'console', 'name' => '控制面板', 'class' => 'icon-home', 'c' => 'index', 'a' => 'index'),
        'link_setting' => array('head' => 'console', 'name' => '企业信息', 'class' => 'icon-magic', 'c' => 'setting', 'a' => 'index'),
        'wechat1' => array('head' => 'console', 'name' => '微信接入', 'class' => 'icon-retweet', 'c' => 'wechat', 'a' => 'binding'),
        'link_web' => array('head' => 'console', 'name' => '微站设置', 'class' => 'icon-laptop', 'c' => 'site', 'a' => 'info'),
        'link_tpl' => array('head' => 'console', 'name' => '模板选择', 'class' => 'icon-folder-close-alt', 'c' => 'tpl', 'a' => 'index'),
        'link_user' => array('head' => 'console', 'name' => '用户管理', 'class' => 'icon-user', 'c' => 'users', 'a' => 'index'),
        'link_module' => array('head' => 'console', 'name' => '功能模块', 'class' => 'icon-tags', 'c' => 'module', 'a' => 'more'),
    );

    protected static $_sideSecMenu = array(
        array('fst' => 'link_setting', 'name' => '商户信息', 'class' => 'icon-laptop', 'c' => 'setting', 'a' => 'index'),
        array('fst' => 'link_setting', 'name' => '修改密码', 'class' => 'icon-laptop', 'c' => 'setting', 'a' => 'password'),
        array('fst' => 'web_set', 'name' => '基本信息', 'class' => 'icon-laptop', 'c' => 'site', 'a' => 'info'),
        array('fst' => 'web_set', 'name' => '焦点图片', 'class' => 'icon-screenshot', 'c' => 'site', 'a' => 'focus'),
        array('fst' => 'web_set', 'name' => '菜单导航', 'class' => 'icon-folder-open-alt', 'c' => 'site', 'a' => 'nav'),
        array('fst' => 'web_set', 'name' => '底部导航', 'class' => 'icon-flag', 'c' => 'site', 'a' => 'foot'),
        // 模块设置
        array('fst' => 'web_tpl', 'name' => '我的设置', 'c' => 'tpl', 'a' => 'index', 'class' => "icon-adjust"),
        array('fst' => 'web_tpl', 'name' => '模板套装', 'c' => 'tpl', 'a' => 'kit', 'class' => "icon-laptop"),
        array('fst' => 'web_tpl', 'name' => '模板单品', 'c' => 'tpl', 'a' => 'single', 'class' => "icon-tablet"),
//        array('fst' => 'web_tpl', 'name' => '个性定制', 'c' => 'tpl', 'a' => 'customize', 'class' => "icon-magic"),
        // 微信管理
        array('fst' => 'wechat', 'name' => '微信设置', 'c' => 'wechat', 'a' => 'binding', 'class' => "icon-random"),
        array('fst' => 'wechat', 'name' => '微信菜单', 'c' => 'wechat', 'a' => 'menu', 'class' => "icon-sitemap"),
        array('fst' => 'wechat', 'name' => '自动回复', 'c' => 'wechat', 'a' => 'respond', 'class' => "icon-reply"),
//        array('fst' => 'wechat', 'name' => '消息群发', 'c' => 'wechat', 'a' => 'mass', 'class' => "icon-reply"),
        array('fst' => 'wechat', 'name' => '消息管理', 'c' => 'wechat', 'a' => 'message', 'class' => "icon-book"),

        // 微博管理
        array('fst' => 'weibo', 'name' => '微博设置', 'c' => 'weibo', 'a' => 'binding', 'class' => "icon-random"),
        array('fst' => 'weibo', 'name' => '微博菜单', 'c' => 'weibo', 'a' => 'menu', 'class' => "icon-sitemap"),
        array('fst' => 'weibo', 'name' => '自动回复', 'c' => 'weibo', 'a' => 'respond', 'class' => "icon-reply"),
//        array('fst' => 'weibo', 'name' => '消息群发', 'c' => 'weibo', 'a' => 'mass', 'class' => "icon-reply"),
//        array('fst' => 'weibo', 'name' => '消息管理', 'c' => 'weibo', 'a' => 'message', 'class' => "icon-book"),

        // 单页
        array('fst' => 'web_page', 'name' => '信息列表', 'c' => 'site', 'a' => 'page', 'class' => " icon-spinner"),
        array('fst' => 'web_page', 'name' => '添加信息', 'c' => 'site', 'a' => 'pageAdd', 'class' => "icon-pencil"),
        array('fst' => 'web_page', 'name' => '修改信息', 'c' => 'site', 'a' => 'pageUp', 'class' => "icon-pencil", 'hide' => 1),

        // 微网站
        array('fst' => 'web_post', 'name' => '文章列表', 'c' => 'article', 'a' => 'list', 'class' => " icon-spinner"),
        array('fst' => 'web_post', 'name' => '文章栏目', 'c' => 'article', 'a' => 'cat', 'class' => "icon-bookmark-empty"),
        array('fst' => 'web_post', 'name' => '添加文章', 'c' => 'article', 'a' => 'add', 'class' => "icon-pencil"),
        array('fst' => 'web_post', 'name' => '修改文章', 'c' => 'article', 'a' => 'update', 'class' => "icon-pencil", 'hide' => 1),

        //用户管理
        array('fst' => 'user-list', 'name' => '已绑定用户', 'class' => 'icon-screenshot', 'c' => 'users', 'a' => 'index'),
//        array('fst' => 'user-list', 'name' => '微信用户', 'class' => 'icon-screenshot', 'c' => 'users', 'a' => 'wechat'),
//        array('fst' => 'user-list', 'name' => '微博用户', 'class' => 'icon-folder-open-alt', 'c' => 'users', 'a' => 'weibo'),
        array('fst' => 'user-list', 'name' => '用户分组', 'class' => 'icon-flag', 'c' => 'users', 'a' => 'group'),
//        array('fst' => 'user-group', 'name' => '微信用户组', 'class' => 'icon-flag', 'c' => 'users', 'a' => 'wechatGroup'),
//        array('fst' => 'user-group', 'name' => '微博用户组', 'class' => 'icon-flag', 'c' => 'users', 'a' => 'weiboGroup'),

    );


    public static function setSideFstMenu($menu)
    {
        self::$_sideFstMenu = $menu;
    }

    public static function addSideFstMenu($menu)
    {
        array_push(self::$_sideFstMenu, $menu);
    }

    public static function addSideSecMenu($menu)
    {
        array_push(self::$_sideSecMenu, $menu);
    }

    public static function clearSideFstMenu()
    {
        self::$_sideFstMenu = array();
    }

    public static function init()
    {
        return new self();
    }

    /**
     * @return array
     */
    public function getCurMenuKey()
    {
        $topMenu = self::$_headMenu;
        $secMenu = self::$_sideSecMenu;
        $fstMenu = self::$_sideFstMenu;

        $controller = $this->view->getControllerName();
        $action = $this->view->getActionName();
        $curHead = '';
        $curFst = '';
        $curSec = '';
        $curThr = '';
        $path = [];

        // 获取顶部当前菜单
        foreach ($topMenu as $k => $top) {
            if (in_array($controller, $top['active'])) {
                $curHead = $k;
                $path['head']['name'] = $top['name'];
                $path['head']['uri'] = $top['uri'];
                break;
            }
        }

        // 2级菜单找当前
        foreach ($secMenu as $k => $sec) {
            if (isset($sec['thr']) && is_array($sec['thr'])) {
                $thrMenu = $sec['thr'];
                foreach ($thrMenu as $t => $thr) {
                    if ($thr['c'] == $controller && $thr['a'] == $action) {
                        $curFst = $sec['fst'];
                        $curSec = $k;
                        $curThr = $t;
                        $path['sec']['name'] = $sec['name'];
                        $path['thr']['name'] = $thr['name'];
                        $path['thr']['uri'] = $thr['c'] . '/' . $thr['a'];
                        $path['sec']['uri'] = isset($sec['c']) && $sec['c'] ? $thr['c'] . '/' . $thr['a'] : $path['thr']['uri'];
                        break;
                    }
                }
            } else {
                if ($sec['c'] == $controller && $sec['a'] == $action) {
                    $curFst = $sec['fst'];
                    $curSec = $k;
                    $curThr = '';
                    $path['sec']['name'] = $sec['name'];
                    $path['sec']['uri'] = $sec['c'] . '/' . $sec['a'];
                    break;
                }
            }
        }

        if (!$curFst) {
            foreach ($fstMenu as $k => $fst) {
                if (isset($fst['c']) && $fst['c'] == $controller && $fst['a'] == $action) {
                    $curFst = $k;
                    $path['fst']['name'] = $fst['name'];
                    $path['fst']['uri'] = $fst['c'] . '/' . $fst['a'];
                    break;
                }
            }
        } else {
            $path['fst']['name'] = $fstMenu[$curFst]['name'];
            $path['fst']['uri'] = isset($fstMenu[$curFst]['c']) && $fstMenu[$curFst]['c'] ? $fstMenu[$curFst]['c'] : $path['sec']['uri'];
        }

        return array('head' => $curHead, 'side' => array('fst' => $curFst, 'sec' => $curSec, 'thr' => $curThr), 'path' => $path);
    }

    /**
     * @return array
     */
    public static function getMenu()
    {
        return array('head' => self::$_headMenu, 'side' => array('fst' => self::$_sideFstMenu, 'sec' => self::$_sideSecMenu));
    }
}
