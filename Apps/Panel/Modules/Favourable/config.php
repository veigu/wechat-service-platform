<?php

return array(
    'name' => 'test',
    'menus' => array(
        'fstMenu' => array(
            'name' => '商品促销活动', 'class' => 'icon-lightbulb',
        ),
        'secMenu' => array(
            array('name' => '促销活动列表', 'c' => 'favourable', 'a' => 'index'),
            array('name' => '新增促销活动', 'c' => 'favourable', 'a' => 'add'),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'default',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
