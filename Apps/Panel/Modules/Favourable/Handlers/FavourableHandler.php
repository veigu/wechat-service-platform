<?php

namespace Modules\Favourable\Handlers;

use Components\ModuleManager\ModuleHandler;
use Phalcon\Tag;

use Models\User\Users;


class FavourableHandler extends ModuleHandler
{
    /**
     * initialize
     * @access public
     * @return void
     */
    public function initialize()
    {
        Tag::setTitle('模块管理-商品促销');
        $this->view->setVar('pageTitle', '商品促销');
    }

    /**
     *
     * @access public
     * @return void
     */
    public function index()
    {


    }

}