<?php

return array(
    'name' => 'test',
    'menus' => array(
        'fstMenu' => array(
            'name' => '会员卡', 'c' => 'default', 'a' => 'index'
        ),
        'secMenu' => array(
            array('name' => '基本设置', 'c' => 'card', 'a' => 'index'),
          array('name' => '背景设置', 'c' => 'card', 'a' => 'setting'),
            array('name' => '等级设置', 'c' => 'card', 'a' => 'grade'),
            array('name' => '申请选项', 'c' => 'card', 'a' => 'field'),
            array('name' => '积分规则', 'c' => 'card', 'a' => 'rules'),
            array('name' => '已发放会员卡', 'c' => 'card', 'a' => 'member'),
            array('name' => '申请列表', 'c' => 'card', 'a' => 'apply'),
            array('name' => '消费记录', 'c' => 'card', 'a' => 'record'),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'default',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
