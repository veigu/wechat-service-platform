<?php

namespace Modules\Vipcard\Handlers;

use Components\Module\VipcardManager;
use Components\ModuleManager\ModuleHandler;
use Components\Rules\PointRule;
use Models\Modules\Vipcard\AddonVipcard;
use Models\Modules\Vipcard\AddonVipcardField;
use Models\Modules\Vipcard\AddonVipcardGrade;
use Models\Modules\Vipcard\AddonVipcardSettings;
use Models\Modules\Vipcard\AddonVipcardUsers;
use Models\User\UserForCustomers;
use Models\User\UserPointRules;

class CardHandler extends ModuleHandler
{
    /**
     * initialize
     *
     * @access public
     * @return void
     */
    public function initialize()
    {
        \Phalcon\Tag::setTitle('模块管理-VIP Card');
        $this->view->setVar('pageTitle', 'VIP Card');
    }

    public function setting()
    {
        $this->view->setVar('pageTitle', '背景设置');
        $this->view->setVar('description', '对会员卡模块进行基本的设置');

        if ($this->request->isPost()) {

            $fieldValue = array("top_bg", "score_bg", "card_use_icon", "card_score_icon", "customer_icon", 'auto_send');


            if (count($fieldValue) > 0) {
                foreach ($fieldValue as $field) {
                    $data[$field] = $this->request->getPost($field);
                }
            }
            $setting = AddonVipcardSettings::findFirst("customer_id = " . CUR_APP_ID);

            if ($setting) {
                if ($setting->update($data) == false) {
                    foreach ($setting->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                }
            } else {
                $setting = new AddonVipcardSettings();
                $data['customer_id'] = CUR_APP_ID;
                if ($setting->create($data) == false) {
                    foreach ($setting->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                }
            }

        }

        $setting = AddonVipcardSettings::findFirst("customer_id = " . CUR_APP_ID);

        $this->view->setVar('option', $setting);
    }

    public function index()
    {
        $this->view->setVar('pageTitle', '会员卡设置');
        $this->view->setVar('description', '设置会员卡类型，如：钻石卡、金卡、银卡等');

        $this->assets->addCss('static/panel/css/module/guestbook.css');
        $this->assets->addCss('static/panel/css/vipcard/vipcard.css');
        $this->assets->addJs('static/panel/js/tools/jscolor.js');

        $vipcard = AddonVipcard::findFirst('customer_id=' . CUR_APP_ID);
        $this->view->setVar('vipcard', $vipcard);
        $vipcardSetting = AddonVipcardSettings::findFirst('customer_id=' . CUR_APP_ID);
        $this->view->setVar('vipcardSetting', $vipcardSetting);

        $request = $this->request;
        if ($request->isPost()) {
            $data['card_name'] = $request->getPost('card_name');
            $data['card_name_color'] = $request->getPost('card_name_color');
            $data['card_number_color'] = $request->getPost('card_number_color');
            $data['card_logo'] = $request->getPost('card_logo');
            $data['card_bg'] = $request->getPost('card_bg');
            $data['card_back_bg'] = $request->getPost('card_back_bg');
            $data['card_detail'] = $request->getPost('card_detail');
            $data['card_point_detail'] = $request->getPost('card_point_detail');
            $data['is_company'] = $request->getPost('is_company');
            $data['min_consume'] = $request->getPost('min_consume');
            $data['min_point'] = $request->getPost('min_point');
            $data['card_receive'] = $request->getPost('card_receive');
            $data['auto_send'] = $request->getPost('auto_send');

            if ($vipcard) {
                if (!$vipcard->update($data)) {
                    $this->flash->error('更新失败～！');
                }
            } else {
                $vipcard = new AddonVipcard();
                $data['customer_id'] = CUR_APP_ID;
                if (!$vipcard->create($data)) {
                    $this->flash->error('插入数据失败～！');
                }
            }

            $enable = $request->getPost('enable', 'int', '0');
            if($vipcardSetting) {
                if(!$vipcardSetting->update(array(
                    'enable' => $enable
                ))) {
                    $this->flash->error('更新失败～！');
                }
            }
            else {
                $vipcardSetting = new AddonVipcardSettings();
                if(!$vipcardSetting->save(array(
                    'customer_id' => CUR_APP_ID,
                    'enable' => $enable
                ))) {
                    $this->flash->error('插入数据失败～！');
                }
            }
        }
    }

    public function grade()
    {
        $this->view->setVar('pageTitle', '等级设置');
        $this->view->setVar('description', '会员卡等级设置');

        $fields = AddonVipcardGrade::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('data', $fields ? $fields->toArray() : '');
    }

    public function field()
    {
        $this->view->setVar('pageTitle', '申请选项');
        $fields = AddonVipcardField::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('fields', $fields ? $fields->toArray() : '');
    }

    public function rules()
    {
        $vip_grade = intval($this->request->get('grade', 'int', 1));
        $this->view->setVar('now_grade', $vip_grade);
        $this->view->setVar('pageTitle', '积分规则设置');
        $this->view->setVar('description', '会员卡积分规则设置-可根据等级进行设置');
        $grade_info = VipcardManager::getCardGradeInfo($vip_grade, CUR_APP_ID);

        if (!$grade_info) {
            $this->flash->error("您指定的vip等级不存在");
            return;
        }


        $behaviorNameMap = PointRule::$behaviorNameMap;
        $rule = new PointRule(CUR_APP_ID, $vip_grade);
        $termNameMap = $rule->termNameMap;
        $pointTypeMap = $rule->actionNameMap;

        $list = UserPointRules::find('customer_id=' . CUR_APP_ID . ' and vip_grade=' . $vip_grade);
        $list = $list ? $list->toArray() : [];
        $exits_rules = array_map(function ($row) {
            return $row['behavior'];
        }, $list);

        $new_add = array_diff_key($behaviorNameMap, array_flip($exits_rules));

        $this->view->data = $list;
        $this->view->new_add = $new_add;
        $this->view->behaviorNameMap = $behaviorNameMap;
        $this->view->termNameMap = $termNameMap;
        $this->view->pointTypeMap = $pointTypeMap;
    }

    /**
     * 消费记录
     *
     * @access public
     * @return void
     */
    public function record()
    {
        $this->view->setVar('pageTitle', '消费记录');
        $this->view->setVar('description', '所持会员卡会员的消费记录');

        $this->assets->addCss('static/panel/css/interation/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');

        $this->assets->addJs('static/ace/js/typeahead-bs2.min.js');
        $this->assets->addJs('static/ace/js/jquery.validate.min.js');
        $this->assets->addJs('static/ace/js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('static/ace/js/dropzone.min.js');
        $this->assets->addJs('static/ace/js/jquery.hotkeys.min.js');
        $this->assets->addJs('static/ace/js/bootstrap-wysiwyg.min.js');
        $this->assets->addJs('static/ace/js/jqGrid/jquery.jqGrid.min.js');
        $this->assets->addJs('static/ace/js/jqGrid/i18n/grid.locale-zh_CN.js');

    }

    public function member()
    {
        $this->view->setVar('pageTitle', '已发放会员卡');
        $this->view->setVar('description', '所持会员卡会员的消费记录');

        $list = $this->modelsManager->createBuilder()
            ->addFrom('Models\\Modules\\Vipcard\\AddonVipcardUsers', 'avu')
            ->leftJoin('Models\\User\\UserForCustomers', 'ufc.user_id=avu.user_id', 'ufc')
            ->andWhere('avu.customer_id=' . CUR_APP_ID . " and  ufc.customer_id=" . CUR_APP_ID)
            ->columns('avu.id,avu.user_id, avu.realname,avu.card_no,avu.phone,avu.email,avu.birthday,avu.gender,avu.card_grade_name,avu.province,avu.city,avu.town,avu.balance,avu.created ,ufc.points,ufc.points_available')
            ->getQuery()
            ->execute();

        $this->view->setVar('list', $list ? $list->toArray() : []);
    }

    public function apply()
    {

        $this->view->setVar('pageTitle', '申请记录');
        $card_setting = AddonVipcardSettings::findFirst('customer_id=' . CUR_APP_ID);
        if (!$card_setting) {

           // $this->flash->error("还未设置会员卡信息哦");
             $this->view->setVar('tip', '还未设置会员卡信息哦');
           // exit;
        }

         if ($card_setting && $card_setting->auto_send == 1) {
            $this->view->setVar('tip', '所有会员卡都是无需审核发放的哦');
        } else {
            $list = AddonVipcardUsers::find('customer_id=' . $this->customer_id);
            $card = AddonVipcard::findFirst('customer_id=' . $this->customer_id);
            $grade = AddonVipcardGrade::find("customer_id='{$this->customer_id}'");
            $list = $list ? $list->toArray() : [];

            $this->view->setVar('list', $list);
            $this->view->setVar("grade_list", $grade);
            $this->view->setVar('card', $card ? $card->toArray() : []);
        }
    }

}
