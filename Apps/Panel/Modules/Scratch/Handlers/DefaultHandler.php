<?php

namespace Modules\Scratch\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionOption;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\Modules\Coupon\AddonCoupon;
use Models\User\Users;
use Util\Ajax;

class DefaultHandler extends ModuleHandler {

    public function index() {

        $marketing = AddonPromotion::find("code='scratch' AND customer_id=" . $this->customer_id);
        if (!$marketing) {
            //
            $viewHelper = $this->view->getVar('viewHelper');
            $this->response->redirect(ltrim($viewHelper->getUrl('addAction'), '/'));

        }
        $this->assets->addCss('static/panel/css/module/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-timepicker.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');
        $this->view->setVar('list', $marketing ? $marketing->toArray() : []);
        $this->view->setVar('pageTitle', '刮刮卡');
        $this->view->setVar('description', '活动列表');

    }

    public function edit() {
        $id = $_REQUEST['aid'];
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        \Phalcon\Tag::setTitle('刮刮卡奖项设置');
        $coupon = AddonCoupon::find("customer_id=" . CUR_APP_ID);
        $this->assets->addJs('static/ace/js/jquery.validate.min.js');
        $this->assets->addJs('static/ace/js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('static/panel/modules/market.wheel.js');
        $request = $this->request;
        $optionId = $request->getQuery('option_id');
        $marketing = AddonPromotion::findFirst("code='scratch' AND customer_id='" . CUR_APP_ID . "'" . " AND id=" . $id);
        if (!empty($optionId)) {
            $option = AddonPromotionOption::findFirst('option_id="' . $optionId . '"');
        } else {
            $option = new AddonPromotionOption();
        }

        $this->view->setVar('option', $option);
        $this->view->setVar('aid', $id);
        $this->view->setVar('coupon', $coupon ? $coupon->toArray() : []);

        if ($request->isPost()) {
            $optionName = $request->getPost('option_name');
            $optionDesc = $request->getPost('option_desc');
            $optionNumber = $request->getPost('option_number');
            $chance = $request->getPost('chance');
            $option_value = $request->getPost('option_value');
            $award_type = $request->getPost('award_type');

            if (empty($optionName)) {
                $this->flash->error('活动名称不能为空！');
                return false;
            }

            $this->db->begin();
            try {
                $option->option_name = $optionName;
                $option->option_desc = $optionDesc;
                $option->option_number = $optionNumber;
                $option->chance = $chance;
                $option->parent_id = $marketing->id;
                $option->option_value = $option_value;
                $option->award_type = $award_type;


                if ($option->save() == false) {
                    foreach ($option->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                    throw new \Phalcon\Exception("保存失败");
                }

                $this->db->commit();
                $viewHelper = $this->view->getVar('viewHelper');
                $this->flash->success('保存成功!');
                $this->response->redirect(ltrim($viewHelper->getUrl('awardList/' . $id), '/'));
            } catch (\Exception $e) {
                $this->db->rollback();
                $this->flash->error($e->getMessage());
            }
        }
    }

    public function remove() {
        $request = $this->request;

        if ($request->isPost()) {

            $id = $request->getPost('id');

            if ($request->getPost('oper') == 'del') {

                try {

                    $ids = array();
                    if (!empty($id)) {
                        $ids = explode(',', $id);

                        foreach ($ids as $id) {
                            AddonPromotionOption::findFirst("option_id = $id")->delete();
                        }
                    }

                } catch (\Exception $e) {

                    $this->db->rollback();
                    $this->flash->error($e->getMessage());

                }
            }
        }
        exit;
    }

    public function setting() {
        $id = $this->dispatcher->getParam(0);
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        \Phalcon\Tag::setTitle('刮刮卡设置');
        $this->assets->addJs('static/ace/js/jquery.validate.min.js');
        $this->assets->addJs('static/ace/js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('static/ace/js/jquery.hotkeys.min.js');
        $this->assets->addJs('static/ace/js/bootstrap-wysiwyg.min.js');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->assets->addJs('static/panel/modules/market.wheel.js');
        $request = $this->request;
        $marketing = AddonPromotion::findFirst('code="scratch" AND customer_id="' . CUR_APP_ID . '" AND id=' . $id);

        if (empty($marketing)) {
            $marketing = new AddonPromotion;
            $marketing->code = "scratch";
        }

        if ($request->isPost()) {
            $name = $request->getPost('name');
            $description = $request->getPost('description');
            $times = $request->getPost('times');
            $exchange_time_limit= $request->getPost('exchange_time_limit');
            $file = $request->getPost('file');
            $startDate = strtotime($request->getPost('start_date'));
            $endDate = strtotime($request->getPost('end_date'));

            if (empty($name)) {
                $this->flash->error('活动名称不能为空！');
                return false;
            }

            if (empty($file)) {
                $this->flash->error('活动图片不能为空！');
                return false;
            }

            if (empty($startDate)) {
                $this->flash->error('活动开始时间不能为空！');
                return false;
            }

            if (empty($endDate)) {
                $this->flash->error('活动结束时间不能为空！');
                return false;
            }

            if ($endDate < $startDate) {
                $this->flash->error('活动结束时间不能小于开始时间！');
                return false;
            }

            $this->db->begin();
            try {
                $marketing->name = $name;
                $marketing->description = $description;
                $marketing->customer_id = CUR_APP_ID;
                $marketing->image = $file;
                $marketing->times = $times;
                $marketing->exchange_time_limit = $exchange_time_limit;
                $marketing->start_time = $startDate;
                $marketing->end_time = $endDate;

                if ($marketing->save() == false) {
                    foreach ($marketing->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                    throw new \Phalcon\Exception("保存失败");
                }

                $this->db->commit();
                $viewHelper = $this->view->getVar('viewHelper');
                $this->flash->success('保存成功，现在你可以添加奖项啦。<a href="' . $viewHelper->getUrl('awardList/' . $id) . '">添加奖项</a>');
            } catch (\Exception $e) {
                $this->db->rollback();
                $this->flash->error($e->getMessage());
            }
        }

        $this->view->setVar('marketing', $marketing);
    }

    public function removePic() {
        $id = $this->request->get('id');
        $marketing = AddonPromotion::findFirst("id = $id");
        $file = ROOT . DS . 'Public' . $marketing->image;
        if (!empty($marketing->image) && file_exists($file)) {

            $this->db->begin();
            try {

                $marketing->image = '';

                if ($marketing->save()) {
                    @unlink($file);
                } else {

                    throw new \Phalcon\Exception("删除失败");
                }

                $this->db->commit();

                $this->flash->success("删除图片成功！");
                $viewHelper = $this->view->getVar('viewHelper');
                $this->response->redirect($viewHelper->getUrl('setting'));

            } catch (\Exception $e) {
                $this->db->rollback();
                $this->flash->error($e->getMessage());
            }
        }
        $this->view->disable();
    }


    public function stats() {
        $id = $this->dispatcher->getParam(0);
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        $this->view->setVar('pageTitle', '结果统计');
        $this->view->setVar('description', '大转盘游戏中奖结果统计');

        $this->assets->addCss('static/panel/css/interation/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-datetimepicker.min.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');

        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.full.min.js');
        $this->assets->addJs('static/ace/js/typeahead-bs2.min.js');
        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.full.min.js');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->assets->addJs('static/ace/js/jqGrid/jquery.jqGrid.min.js');
        $this->assets->addJs('static/ace/js/jqGrid/i18n/grid.locale-zh_CN.js');
        $this->assets->addJs('static/ace/js/jquery.easy-pie-chart.min.js');
        $this->assets->addJs('static/ace/js/flot/jquery.flot.min.js');
        $this->assets->addJs('static/ace/js/flot/jquery.flot.pie.min.js');


        $queryString = <<<EOF
    COUNT(*) AS times,
    SUM(IF(p.code IS NOT NULL AND LENGTH(p.code) > 0, 1, 0)) AS win_times,
    SUM(IF(p.code IS NOT NULL AND LENGTH(p.code) > 0, 0, 1)) AS lose_times,
    COUNT(*) AS participants,
    SUM(IF(p.code IS NOT NULL AND LENGTH(p.code) > 0 AND p.available = 0, 1, 0)) AS cashed_lottery,
    SUM(IF(p.code IS NOT NULL AND LENGTH(p.code) > 0 AND p.available = 1, 1, 0)) AS not_cashed_lottery
EOF;
        $statistic = $this->modelsManager->createBuilder()->addFrom("\\Models\\Modules\\Promotion\\AddonPromotionPrize", 'p')
            ->columns($queryString)
            ->where("p.activity_id = '{$id}'")
            ->getQuery()
            ->execute()[0]
            ->toArray();

        $prize_stats = $this->modelsManager->createBuilder()->addFrom("\\Models\\Modules\\Promotion\\AddonPromotionOption", 'op')
            ->columns("COUNT(*) AS times, op.option_name")
            ->join("\\Models\\Modules\\Promotion\\AddonPromotionPrize", "p.prize_id = op.option_id", "p")
            ->where("p.activity_id = '{$id}'")
            ->groupBy("op.option_id")
            ->limit(1)
            ->getQuery()
            ->execute()
            ->toArray();
        $statistic['prize_statisic'] = $prize_stats;
        $prizes = array();
        if (count($statistic['prize_statisic'])) {
            foreach ($statistic['prize_statisic'] as $prize) {
                $prizes[] = array('label' => $prize['option_name'] . " ({$prize['times']}次)", 'data' => $prize['times'], 'color' => '#' . sprintf("%02X", rand(0, 255)) . sprintf("%02X", rand(0, 255)) . sprintf("%02X", rand(0, 255)));
            }
        }

        $statistic['prizes'] = json_encode($prizes);


        $this->view->setVars($statistic);
        $this->view->setVar('id', $id);
    }

    public function beforeRender($event, $view) {

    }

    /* 新增活动*/
    public function  addAction() {
        if ($this->request->isPost()) {
            $data = $this->request->getPost();
            $start = strtotime($this->request->getPost('start_date'));
            $end = strtotime($this->request->getPost('end_date'));
            $data['start_time'] = $start;
            $data['end_time'] = $end;
            $data['customer_id'] = CUR_APP_ID;
            $data['code'] = 'scratch';
            $market = new AddonPromotion();
            $market->assign($data);
            $market->create();
            header('location:/panel/addon/scratch/default/index');
        }

        $this->assets->addCss('static/panel/css/module/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-timepicker.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');
        $this->assets->addCss('static/ace/css/datepicker.css');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->view->setVar('pageTitle', '刮刮卡');
        $this->view->setVar('description', '新增刮刮卡');
    }

    /*奖项列表*/
    public function awardList() {
        $id = $this->dispatcher->getParam(0);
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        $marketing = AddonPromotion::findFirst("code='scratch' AND customer_id='" . CUR_APP_ID . "' AND id=" . $id);
        if (!$marketing) {
            //该活动不存在
            $viewHelper = $this->view->getVar('viewHelper');
            $this->response->redirect(ltrim($viewHelper->getUrl('addAction'), '/'));
        }
        $this->assets->addCss('static/panel/css/module/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-timepicker.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');

        $this->view->setVar('pageTitle', '刮刮卡');
        $this->view->setVar('description', '刮刮卡游戏奖项');
        $this->view->setVar('id', $id);
    }

    /*添加奖项*/
    public function addAward() {

        $id = $_REQUEST['id'];
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        \Phalcon\Tag::setTitle('大转盘奖项设置');
        $this->assets->addJs('static/ace/js/jquery.validate.min.js');
        $this->assets->addJs('static/ace/js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('static/panel/modules/market.wheel.js');
        $request = $this->request;
        $optionId = $request->getQuery('option_id');
        $marketing = AddonPromotion::findFirst("code='scratch' AND customer_id='" . CUR_APP_ID . "' AND id=" . $id);
        if (!empty($optionId)) {
            $option = AddonPromotionOption::findFirst('option_id="' . $optionId . '"');
        } else {
            $option = new AddonPromotionOption();
        }


        $this->view->setVar('option', $option);
        $this->view->setVar('id', $id);
        if ($this->request->isPost()) {
            $optionName = $request->getPost('option_name');
            $optionDesc = $request->getPost('option_desc');
            $optionNumber = $request->getPost('option_number');
            $angle = $request->getPost('angle');
            $chance = $request->getPost('chance');
            $option_value = $request->getPost('option_value');
            $award_type = $request->getPost('award_type');

            if (empty($optionName)) {
                $this->flash->error('活动名称不能为空！');
                return false;
            }

            $this->db->begin();
            try {
                $option->option_name = $optionName;
                $option->option_desc = $optionDesc;
                $option->option_number = $optionNumber;
                $option->angle = $angle;
                $option->chance = $chance;
                $option->parent_id = $marketing->id;
                $option->option_value = $option_value;
                $option->award_type = $award_type;


                if ($option->save() == false) {
                    foreach ($option->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                    throw new \Phalcon\Exception("保存失败");
                }

                $this->db->commit();
                $viewHelper = $this->view->getVar('viewHelper');
                $this->flash->success('保存成功!');
                $this->response->redirect(ltrim($viewHelper->getUrl('awardList/' . $id), '/'));
            } catch (\Exception $e) {
                $this->db->rollback();
                $this->flash->error($e->getMessage());
            }
        }
    }

}
