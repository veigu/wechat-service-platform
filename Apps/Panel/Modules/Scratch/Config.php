<?php

return array(
    'name' => 'guestbook',
    'entry' => array(
        'handler' => 'list',
        'action' => 'index'
    ),
    'menus' => array(
        'fstMenu' => array(
            'name' => '刮刮卡', 'c' => 'default', 'a' => 'index'
        ),
        'secMenu' => array(
            array('name' => '活动列表', 'c' => 'default', 'a' => 'index'),
            array('name' => '添加活动', 'c' => 'default', 'a' => 'addAction', 'class' => "icon-bar-chart"),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'list',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
