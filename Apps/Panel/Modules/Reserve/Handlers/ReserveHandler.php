<?php

namespace Modules\Reserve\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\Reserve\AddonReserve;
use Models\Modules\Reserve\AddonReserveColumns;
use Models\Modules\Reserve\AddonReserveOrders;
use Phalcon\Tag;
use Util\EasyEncrypt;
use Util\Pagination;

//use Components\EasyEncrypt;

class ReserveHandler extends ModuleHandler
{
    /**
     * initialize
     * @access public
     * @return void
     */
    public function initialize()
    {
        Tag::setTitle('模块管理-预约管理');
        $this->view->setVar('pageTitle', '预约管理');
    }

    /**
     * 预约说明
     * @access public
     * @return void
     */
    public function index()
    {
        $where = 'customer_id=' . CUR_APP_ID;

        $count = AddonReserve::count($where);
        $res = AddonReserve::find(array($where, "order" => "created desc"));

        $this->view->setVar('list', $res ? $res->toArray() : []);
        $this->view->setVar('count', $count);
    }


    //订单设置
    public function option()
    {
        $reserve_id = $this->request->get('id');
        $all_reserve = AddonReserve::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('columns', []);
        $this->view->setVar('reserve', []);
        if ($reserve_id) {
            $reserve = AddonReserve::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $reserve_id);
            if ($reserve) {
                $id = $this->request->get('id', 'int');
                $columns = AddonReserveColumns::find('reserve_id = ' . $id);
                $this->view->setVar('columns', $columns ? $columns->toArray() : []);
                $this->view->setVar('reserve', $reserve ? $reserve->toArray() : []);
            }
        }
        $this->view->setVar('all_reserve', $all_reserve ? $all_reserve->toArray() : []);
    }

    //预定订单管理
    public function orderManagement()
    {
        $page = $this->request->get('p');
        $reserve_id = $this->request->get('id');
        $key = $this->request->get('key');
        $all_reserve = AddonReserve::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('list', []);
        $this->view->setVar('reserve', []);
        if ($reserve_id) {
            $reserve = AddonReserve::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $reserve_id);
            if ($reserve) {
                $where = 'customer_id=' . CUR_APP_ID . ' and reserve_id=' . $reserve_id;
                $where = $key ? $where . ' and username like "%' . $key . '%"' : $where;
                $curpage = $page <= 0 ? 0 : $page - 1;
                $limit = 12;

                $count = AddonReserveOrders::count($where);
                $res = AddonReserveOrders::find(array($where, "order" => "created desc", "limit" => $curpage * $limit . "," . $limit));
                Pagination::instance($this->view)->showPage($page, $count, $limit);

                $this->view->setVar('list', $res ? $res->toArray() : []);
                $this->view->setVar('reserve', $reserve ? $reserve->toArray() : []);
            }
        }
        $this->view->setVar('all_reserve', $all_reserve ? $all_reserve->toArray() : []);
    }

    //预定统计
    public function statistic()
    {
        //获取最近一个月的订单信息
        $customer_id = CUR_APP_ID;
        $time1 = mktime(0, 0, 0, date("m") - 1, date("d"), date("Y"));
        $time2 = time();
        $sql = "SELECT count(*) as total,created from Models\Modules\Reserve\AddonReserveOrders where deleted = 0 and customer_id = '.$customer_id.' and created >= $time1 and created <= $time2 group by FROM_UNIXTIME(created,'%Y-%m-%d') order by created desc";
        $records = $this->modelsManager->executeQuery($sql);
        $data = array();
        foreach ($records as $key => $value) {
            $data[$key]['total'] = $value->total;
            $data[$key]['date'] = date('m-d', $value->created);
        }

        $date = "[";
        $datas = "[";
        for ($i = 0; $i <= 30; $i++) {
            $d = date('m-d', $time1 + $i * 3600 * 24);
            $date .= "'" . $d . "',";
            if (sizeof($data) < 1) {
                $datas .= "0,";
                continue;
            }
            foreach ($data as $k => $v) {
                if ($v['date'] == $d) $datas .= "" . $v['total'] . ",";
                else $datas .= "0,";
            }
        }

        $date .= "]";
        $datas .= "]";

        $t_time1 = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
        $t_time2 = $t_time1 + 3600 * 24;
        $y_time1 = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));
        $y_time2 = $y_time1 + 3600 * 24;
        $todayNum = AddonReserveOrders::count('customer_id = ' . $customer_id . ' and deleted = 0 and created > ' . $t_time1 . ' and created < ' . $t_time2);
        $yesterdayNum = AddonReserveOrders::count('customer_id = ' . $customer_id . ' and deleted = 0 and created > ' . $y_time1 . ' and created < ' . $y_time2);
        $totalNum = AddonReserveOrders::count('customer_id = ' . $customer_id . ' and deleted = 0');
        $this->view->setVar('todayNum', $todayNum);
        $this->view->setVar('yesterdayNum', $yesterdayNum);
        $this->view->setVar('totalNum', $totalNum);

        $this->view->setVar('date', $date);
        $this->view->setVar('datas', $datas);
    }

    //增加预定类型
    public function addReserve()
    {
        if (!empty($_POST)) {

            //@TODO 此处需要重新调整代码逻辑
            $_POST['customer_id'] = CUR_APP_ID;
            $_POST['created'] = time();
            $_POST['is_active'] = 1;
            $record = new AddonReserve();
            $record->assign($_POST);

            // count 大于10
            if ($record->count('customer_id=' . CUR_APP_ID) >= 10) {
                $this->flash->error('每个商家最多可以创建10个预约');
                $this->assets->addCss('static/panel/css/vipcard/vipcard.css');
                $this->assets->addCss('static/ace/css/datepicker.css');
                $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
            } else {
                if ($record->create()) $this->response->redirect('panel/addon/reserve/reserve/index');
                else $this->error();
                $this->view->disable();
            }
        } else {
            $this->assets->addCss('static/panel/css/vipcard/vipcard.css');
            $this->assets->addCss('static/ace/css/datepicker.css');
            $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        }
    }

    //修改用户订单
    public function editOrder()
    {
        $id = $_GET['id'];
        //获取订单对应数据columns
        $reserveOrder = AddonReserveOrders::findFirst("id = '{$id}'");
        //$reserve = AddonReserve::findFirst('id = '.$reserveOrder->reserve_id);
        $reservecolums = AddonReserveColumns::find("reserve_id = '{$reserveOrder->reserve_id}'");
        foreach ($reservecolums as $key => $value) {
            //if($value->isActive == 0) continue;
            if ($value->is_active == 0) continue;
            $reservecolumn[$key]['id'] = EasyEncrypt::encode($value->id);
            $reservecolumn[$key]['column_type'] = $value->column_type;
            $reservecolumn[$key]['column_name'] = $value->column_name;
            $reservecolumn[$key]['data'] = $value->data;
        }
        $datas = explode('|', $reserveOrder->data);
        unset($datas[0]);
        for ($i = 1; $i < sizeof($datas); $i += 2) {
            $k = $datas[$i];
            $v = $datas[$i + 1];
            $data[$k] = $v;
        }
        $this->assets->addCss('static/panel/css/vipcard/vipcard.css');
        $this->assets->addCss('static/ace/css/datepicker.css');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->view->setVar('reservecolumn', $reservecolumn);
        //$this->view->setVar('reserve',$reserve);
        $this->view->setVar('reserveOrder', $reserveOrder);
        $this->view->setVar('data', $data);
        //获取订单的数据

    }

    //删除用户订单
    public function deleteOrder()
    {
        //@TODO 验证是否是合法操作
        $id = $this->request->get('id', 'int');
        $record = AddonReserveOrders::findFirst("id = '{$id}'");
        if ($record && $record->delete()) $this->goBack();
        else $this->error();
        $this->view->disable();
    }

    //客服确认用户订单
    public function confirmOrder()
    {
        //@TODO 验证是否是合法操作
        $id = $this->request->get('id', 'int');
        $record = AddonReserveOrders::findFirst("id = '{$id}'");
        $record->status = 1;
        if ($record->update()) $this->goBack();
        else $this->error();
        $this->view->disable();
    }

    //修改预定类型
    public function editReserve()
    {
        //获取此预定类型所有的columns,编辑之后重新提交,如果有修改的，修改再添加
        //$columns = AddonReserveColumns::find('reserve_id = '.$id);

        if (!empty($_POST)) {
            /*
            if($_FILES['titlePic']['size'] != 0)
            {
                if ((($_FILES['titlePic']["type"] != "image/gif")
                        && ($_FILES['titlePic']["type"] != "image/jpeg")
                        && ($_FILES['titlePic']["type"] != "image/pjpeg")
                        && ($_FILES['titlePic']["type"] != "image/png"))){echo "非法文件,请上传正确的图片文件";exit;}
                if ($_FILES['titlePic']["error"] > 0){echo "错误返回码: " . $_FILES['titlePic']["error"] . "<br />";exit;}

                if (!file_exists("uploads/reserve" . $_FILES['titlePic']["name"]))
                {
                    move_uploaded_file($_FILES['titlePic']["tmp_name"],"uploads/reserve/" . $_FILES['titlePic']["name"]);
                }

                $_POST['titlePic'] = "uploads/reserve/" . $_FILES['titlePic']["name"];
            }
            if($_FILES['infoPic']['size'] != 0)
            {
                if ((($_FILES['infoPic']["type"] != "image/gif")
                        && ($_FILES['infoPic']["type"] != "image/jpeg")
                        && ($_FILES['infoPic']["type"] != "image/pjpeg")
                        && ($_FILES['infoPic']["type"] != "image/png"))){echo "非法文件,请上传正确的图片文件";exit;}
                if ($_FILES['infoPic']["error"] > 0){echo "错误返回码: " . $_FILES['infoPic']["error"] . "<br />";exit;}

                if (!file_exists("uploads/reserve" . $_FILES['infoPic']["name"]))
                {
                    move_uploaded_file($_FILES['infoPic']["tmp_name"],"uploads/reserve/" . $_FILES['infoPic']["name"]);
                }

                $_POST['infoPic'] = "uploads/reserve/" . $_FILES['infoPic']["name"];
            }
            * */
            $id = $_POST['reserve_id'];
            unset($_POST['reserve_id']);
            $reserve = AddonReserve::findFirst("id = '{$id}'");

            if ($reserve->update($_POST)) $this->goBack();
            else $this->error();

        }
        $id = $_GET['id'];
        $this->assets->addCss('static/panel/css/vipcard/vipcard.css');
        $this->assets->addCss('static/ace/css/datepicker.css');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $reserve = AddonReserve::findFirst("id = '{$id}'");
        $this->view->setVar('reserve', $reserve);
    }

    //操作错误的返回信息
    public function error()
    {
        echo '操作失败!,点击<span onclick="history.go(-1);" style="color:red;">返回</span>重新操作';
        exit;
    }

    public function goBack()
    {
        echo '<script>history.go(-1);</script>';
        exit;
    }

    public function beforeRender()
    {
        $this->assets->addCss('static/panel/css/module/reserve.css');
    }

    public function refresh()
    {
        echo '<script>history.go(0);</script>';
        exit;
    }
}

