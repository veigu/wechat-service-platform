<?php

return array(
    'name' => 'test',
    'menus' => array(
        'fstMenu' => array(
            'name' => '预约管理', 'c' => 'reserve', 'a' => 'index',
        ),
        'secMenu' => array(
            array('name' => '预约列表', 'c' => 'reserve', 'a' => 'index'),
            array('name' => '新增预约', 'c' => 'reserve', 'a' => 'addReserve'),
            array('name' => '预约统计', 'c' => 'reserve', 'a' => 'statistic'),
            array('name' => '预约订单', 'c' => 'reserve', 'a' => 'orderManagement'),
            array('name' => '预约选项', 'c' => 'reserve', 'a' => 'option'),
            array('name' => '编辑预约', 'c' => 'reserve', 'a' => 'editReserve', 'is_hide' => true),
            array('name' => '编辑订单', 'c' => 'reserve', 'a' => 'editOrder', 'is_hide' => true),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'default',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
