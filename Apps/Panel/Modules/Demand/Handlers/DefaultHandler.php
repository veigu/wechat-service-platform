<?php

namespace Modules\Demand\Handlers;

use Components\Module\DemandManager;
use Components\ModuleManager\ModuleHandler;
use Models\Modules\Demand\AddonDemand;
use Models\Modules\Demand\AddonDemandType;
use Models\User\Users;
use Components\UserStatusWap;
use Phalcon\Tag;
use Util\EasyEncrypt;
use Util\Pagination;
use Util\Uri;

class DefaultHandler extends ModuleHandler
{
    /**
     * initialize
     *
     * @access public
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->uri = new Uri();
        Tag::setTitle('询盘/需求管理');
    }

    /**
     * list for messages of users
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $page = $this->request->get('p');
        $t = intval($this->request->get('t'));
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;
        $key = $this->request->get('key');

        $where = 'customer_id=' . CUR_APP_ID;

        if ($key) {
            $where .= ' and title like "%' . $key . '%"';
        }

        if ($t) {
            $where .= ' and type_id=' . $t;
        }

        $list = AddonDemand::find(array(
            $where,
            'order' => 'created desc',
            'limit' => $curpage * $limit . "," . $limit
        ));

        $count = AddonDemand::count($where);
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $list ? $list->toArray() : '');
    }

    /**
     * list for messages of users
     *
     * @access public
     * @return void
     */
    public function wait()
    {
        $page = $this->request->get('p');
        $t = intval($this->request->get('t'));
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;
        $key = $this->request->get('key');

        $where = 'customer_id=' . CUR_APP_ID . ' and status in(0,1) ';

        if ($key) {
            $where .= ' and title like "%' . $key . '%"';
        }

        if ($t) {
            $where .= ' and type_id=' . $t;
        }

        $list = AddonDemand::find(array(
            $where,
            'order' => 'created desc',
            'limit' => $curpage * $limit . "," . $limit
        ));

        $count = AddonDemand::count($where);
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $list ? $list->toArray() : '');
    }


    public function type()
    {
        Tag::setTitle('询盘/需求管理 - 需求类型管理');

        $list = AddonDemandType::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('list', $list ? $list->toArray() : []);
    }

    public function detail()
    {
        Tag::setTitle('询盘/需求管理 - 需求详情');

        $id = $this->request->get('id');
        // app-id like 123456-1
        $id = EasyEncrypt::decode($id);

        if (!($id && is_numeric($id))) {
            $this->err('404', '内容未找到');
            return;
        } else {
            $item = AddonDemand::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $id);
            if (!$item) {
                $this->err('404', '内容未找到');
                return;
            } else {
                // 判断状态为默认就更改
                if ($item->status == DemandManager::DEMAND_STATUS_DEFAULT) {
                    $item->update(array('status' => DemandManager::DEMAND_STATUS_WAIT));
                }

                $user = Users::findFirst('id=' . $item->user_id);

                $this->view->setVar('item', $item->toArray());
            }
        }
    }

    public function intro()
    {

    }


}
