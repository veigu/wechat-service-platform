<?php

return array(
    'name' => 'guestbook',
    'entry' => array(
        'handler' => 'list',
        'action' => 'index'
    ),
    'menus' => array(
        'fstMenu' => array(
            'name' => '询盘/需求管理', 'c' => 'default', 'a' => 'index'
        ),
        'secMenu' => array(
            array('name' => '询盘/需求列表', 'c' => 'default', 'a' => 'index'),
            array('name' => '等待回复列表', 'c' => 'default', 'a' => 'wait'),
            array('name' => '询盘/需求类型', 'c' => 'default', 'a' => 'type'),
            array('name' => '询盘/需求详情', 'c' => 'default', 'a' => 'detail', 'is_hide' => true),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'list',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
