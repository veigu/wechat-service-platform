<?php

namespace Modules\Store\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\Store\AddonStoreGroup;
use Models\Modules\Store\AddonStore;
use Phalcon\Tag;
use Util\EasyEncrypt;
use Util\Pagination;
use Util\Uri;

class DefaultHandler extends ModuleHandler
{
    /**
     * initialize
     *
     * @access public
     * @return void
     */
    public function initialize()
    {
        $this->uri = new Uri();
        Tag::setTitle('LBS门店系统');
    }

    /**
     * list for messages of users
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $page = intval($this->request->get('p'));
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;
        $key = $this->request->get('key');
        $group = intval($this->request->get('g'));

        $where = 'customer_id=' . CUR_APP_ID;

        if ($key) {
            $where .= ' and name like "%' . $key . '%"';
        }

        if ($group) {
            $where .= ' and group_id = ' . $key;
        }

        $list = AddonStore::find(array(
            $where,
            'order' => 'sort desc,created desc',
            'limit' => $curpage * $limit . "," . $limit
        ));

        $count = AddonStore::count($where);
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $list ? $list->toArray() : '');
    }

    public function add()
    {
        Tag::setTitle('添加分店');
    }

    public function group()
    {
        Tag::setTitle('分店分组');

        $list = AddonStoreGroup::find('customer_id=' . CUR_APP_ID);
        $this->view->setVar('list', $list ? $list->toArray() : []);

    }

    public function update()
    {
        Tag::setTitle('修改分店');

        $id = $this->request->get('id');
        // app-id like 123456-1
        $id = EasyEncrypt::decode($id);

        if (!($id && is_numeric($id))) {
            // todo error
            $this->err('404', '分店未找到');
            return;
        }
        $pano = AddonStore::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $id);
        if (!$pano) {
            // todo error
            $this->err('404', '分店未找到');
            return;
        }

        $this->view->setVar('item', $pano->toArray());
    }
}
