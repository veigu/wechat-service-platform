<?php

return array(
    'name' => 'guestbook',
    'entry' => array(
        'handler' => 'list',
        'action' => 'index'
    ),
    'menus' => array(
        'fstMenu' => array(
            'name' => 'LBS门店系统', 'c' => 'default', 'a' => 'index'
        ),
        'secMenu' => array(
            array('name' => '门店列表', 'c' => 'default', 'a' => 'index'),
            array('name' => '门店分组', 'c' => 'default', 'a' => 'group'),
            array('name' => '添加门店', 'c' => 'default', 'a' => 'add'),
            array('name' => '修改门店', 'c' => 'default', 'a' => 'update', 'is_hide' => true),
        )
    )
);
