<?php

namespace Modules\Guestbook\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Customer\Customers;
use Models\Modules\Guestbook\AddonGuestbook;
use Models\Modules\Guestbook\AddonGuestbookSettings;

class ListHandler extends ModuleHandler
{
    /**
     * initialize
     *
     * @access public
     * @return void
     */
    public function initialize()
    {
        \Phalcon\Tag::setTitle('模块管理-微互动-留言板');
    }

    /**
     * list for messages of users
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $this->assets->addJs('static/ace/js/ace-extra.min.js');
        $this->assets->addCss('static/panel/css/module/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-timepicker.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');

        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.full.min.js');
        $this->assets->addJs('static/ace/js/typeahead-bs2.min.js');
        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.full.min.js');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->assets->addJs('static/ace/js/jqGrid/jquery.jqGrid.min.js');
        $this->assets->addJs('static/ace/js/jqGrid/i18n/grid.locale-zh_CN.js');


        $this->view->setVar('pageTitle', '留言板');
        $this->view->setVar('description', '用户留言管理');
    }

    /**
     * edit or delete
     *
     * @access public
     * @return void
     */
    public function edit()
    {
        $post = $this->request->getPost();

        if ($post['oper'] == 'edit') {

            if (!empty($post['id'])) {
                $record = AddonGuestbook::findFirst("id = {$post['id']}");
                $record->assign($post);
                $record->update();
            }

        } else if ($post['oper'] == 'del') {

            if (!empty($post['id'])) {
                $ids = explode(',', $post['id']);
                if (count($ids) > 0) {
                    foreach ($ids as $id) {
                        $record = AddonGuestbook::findFirst("id = $id");
                        $record->delete();
                    }
                }
            }
        }
        exit;
    }

    public function batch()
    {
        $post = $this->request->getPost();

        if (!empty($post['id'])) {
            $ids = explode(',', $post['id']);

            if (count($ids) > 0) {

                unset($post['id']);
                unset($post['oper']);
                foreach ($ids as $id) {
                    $row = AddonGuestbook::findFirst("id = $id");
                    foreach ($post as $field => $value) {
                        if ($value != '') {
                            $row->$field = $value;
                        }
                    }
                    $row->save();
                }
            }
        }

        exit;
    }

    public function beforeRender($event, $view)
    {

    }

    public function ajaxMessagesGrid()
    {
        // get post
        $order = $this->request->getPost('sidx', null, 'created');
        $sort = $this->request->getPost('sord');
        $page = $this->request->getPost('page', null, 1);
        $limit = $this->request->getPost('rows');
        $search = $this->request->getPost('_search');

        // search filters
        if ($search == true) {
            $filter = $this->request->getPost('filters');
            if (!empty($filter)) {
                $filter = json_decode($filter);
                foreach ($filter->rules as $rule) {
                    if ($rule->data != '-1') {
                        $cond = "Models\Modules\Guestbook\AddonGuestbook." . $rule->field;
                        switch ($rule->op) {
                            default:
                            case 'eq':
                                $cond .= " = $rule->data";
                                break;

                            case 'bw':
                                //$cond .= " like \"%$rule->data%\"";
                                $cond = "LOCATE('$rule->data', $cond) > 0";
                                break;
                        }
                        $conditions[] = $cond;
                    }
                }
            }
        }

        // build conditions
        $conditions[] = "Models\Modules\Guestbook\AddonGuestbook.customer_id = " . CUR_APP_ID . "";
        $conditions = implode(" AND ", $conditions);

        // get messages
        $builder = $this->modelsManager->createBuilder()
            ->from("Models\Modules\Guestbook\AddonGuestbook")
            ->where($conditions)
            ->join('Models\User\Users', 'ufc.id = Models\Modules\Guestbook\AddonGuestbook.user_id', 'ufc')
            ->orderBy('Models\Modules\Guestbook\AddonGuestbook.' . $order . ' ' . $sort);

        // pagination
        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(
            array(
                "builder" => $builder,
                "limit" => $limit,
                "page" => $page
            )
        );
        $page = $paginator->getPaginate();

        // build rows for jqgrid
        $data['rows'] = array();
        if (count($page->items) > 0) {

            foreach ($page->items as $idx => $msg) {
                $data['rows'][$idx]['user_id'] = $msg->getUsers($msg->user_id)->username;
                $data['rows'][$idx]['status'] = $msg->getStatus();
                $data['rows'][$idx]['id'] = $msg->id;
                $data['rows'][$idx]['message'] = $msg->message;
                $data['rows'][$idx]['reply'] = $msg->reply;
                $data['rows'][$idx]['created'] = $msg->created;
            }

            $data['page'] = $page->current;
            $data['total'] = $page->total_pages;
            $data['records'] = count($builder->getQuery()->execute());
        }

        echo json_encode($data);

        exit;
    }

    /**
     * users for filtertoolbar
     *
     * @access public
     * @return void
     */
    public function ajaxGetUsers()
    {
        $customer = Customers::getCustomer(CUR_APP_ID);
        $users = [];
        if ($customer) {
            $users = $customer->getUsers();
        }

        if (count($users) > 0) {
            $html = "<select><option value=''>全部</option>";
            foreach ($users as $user) {
                $html .= "<option value=\"{$user->id}\">{$user->username}</option>";
            }
            $html .= "</select>";
        } else {
            $html = "<select><option value=''> </option></select>";
        }
        echo $html;
        exit;
    }

    public function setting()
    {
        $settings = AddonGuestbookSettings::findFirst("customer_id = '" . CUR_APP_ID . "'");
        if (empty($settings)) {
            $settings = new AddonGuestbookSettings();
        }

        if ($this->request->isPost()) {
            $params = $this->request->getPost('params');
            if (!empty($params)) {
                $settings->params = serialize($params);
            }
            $settings->customer_id = CUR_APP_ID;
            if (isset($settings->id) && $settings->id > 0) {
                $settings->update();
            } else {
                $settings->create();
            }
            $this->flash->success('保存成功!');
        }

        if (!empty($settings->params)) {
            $settings->params = unserialize($settings->params);
        }

        $this->view->setVar("setting", $settings);
    }
}
