<?php

namespace Modules\Coupon\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\Coupon\AddonCoupon;
use Models\Modules\Coupon\AddonCouponOfflineLog;
use Models\Modules\Coupon\AddonCouponOrderLog;
use Models\Modules\Coupon\AddonCouponReceive;
use Models\Modules\Coupon\AddonCouponSettings;
use Models\User\Users;
use Phalcon\Tag;
use Util\EasyEncrypt;
use Util\Pagination;
use Util\Uri;

class DefaultHandler extends ModuleHandler
{
    /**
     * initialize
     *
     * @access public
     * @return void
     */
    public function initialize()
    {
        $this->checkSetting();

        $this->uri = new Uri();
        Tag::setTitle('优惠劵 - 促销活动');
    }

    public function setting()
    {
        Tag::setTitle('优惠劵 - 基本设置');

        if ($this->request->isPost()) {
            $passwd = $this->request->getPost('passwd');
            $repasswd = $this->request->getPost('repasswd');
            if (strlen($passwd) < 6) {
                $this->flash->error('密码不得小于6位');
                return;
            }
            if ($repasswd != $passwd) {
                $this->flash->error('两次输入的密码不一致!');
                return;
            }

            $setting = AddonCouponSettings::findFirst('customer_id=' . CUR_APP_ID);

            $data = array('passwd' => md5($passwd));
            if ($setting) {
                if (!$setting->update($data)) {
                    $this->flash->error('数据更新失败!');
                }
            } else {
                $m = new AddonCouponSettings();
                $data['customer_id'] = CUR_APP_ID;
                if (!$m->create($data)) {
                    $this->flash->error('数据插入失败!');
                }
            }

            $this->flash->success('操作成功!');
        }
    }

    /**
     * list for messages of users
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $page = $this->request->get('p');
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;
        $key = $this->request->get('key');

        $where = 'deleted=0 and customer_id=' . CUR_APP_ID;
        $where = $key ? $where . ' and name like "%' . $key . '%"' : $where;

        $list = AddonCoupon::find(array(
            $where,
            'order' => 'created desc',
            'limit' => $curpage * $limit . "," . $limit
        ));

        $count = AddonCoupon::count($where);
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $list ? $list->toArray() : '');
    }

    private function checkSetting()
    {
        $setting = AddonCouponSettings::findFirst('customer_id=' . CUR_APP_ID);
        if (!$setting && $this->view->_action != 'setting') {
            $this->response->redirect('panel/addon/coupon/default/setting')->send();
        }

    }

    public function add()
    {
        $this->assets->addCss('static/panel/js/tools/datetimepicker/bootstrap-datetimepicker.min.css');

        Tag::setTitle('优惠劵 - 添加优惠劵商品');
    }

    public function update()
    {
        $this->assets->addCss('static/panel/js/tools/datetimepicker/bootstrap-datetimepicker.min.css');

        Tag::setTitle('优惠劵 - 修改优惠劵商品');

        $id = $this->request->get('id');
        // app-id like 123456-1
        $id = EasyEncrypt::decode($id);

        if (!($id && is_numeric($id))) {
            // todo error
            $this->err('404', '优惠劵未找到');
            return;
        } else {
            $item = AddonCoupon::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $id);
            if (!$item) {
                // todo error
                $this->err('404', '优惠劵未找到');
                return;
            } else {
                $this->view->setVar('item', $item->toArray());
            }
        }
    }

    public function receive()
    {
        $page = $this->request->get('p');
        $coupon_id = $this->request->get('coupon_id');
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;
        $coupon = $coupon_id ? ' and coupon_id= ' . $coupon_id : '';
        $sql = 'SELECT re.*,co.name,co.use_start,co.use_end,co.coupon_type,co.coupon_param,co.receive_end,co.use_limit,u.username FROM addon_coupon_receive as re
        left join addon_coupon as co on co.id=re.coupon_id
        left join users as u on u.id=re.user_id
        where co.customer_id=' . CUR_APP_ID . $coupon . ' and co.deleted=0 and co.published=1  order by `used` asc
        limit ' . $curpage * $limit . ', ' . $limit . ' ';

        $result = $this->db->fetchAll($sql, \PDO::FETCH_ASSOC);

        $count = AddonCouponReceive::count('customer_id=' . CUR_APP_ID . $coupon);

        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $result);
    }

    public function order()
    {
        $page = $this->request->get('p');
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;

        $where = 'customer_id=' . CUR_APP_ID;

        $list = AddonCouponOrderLog::find(array(
            $where,
            'order' => 'created desc',
            'limit' => $curpage * $limit . "," . $limit
        ));

        if ($list) {
            $list = $list->toArray();
            foreach ($list as &$v) {
                $v['coupon'] = AddonCoupon::findFirst('id=' . $v['coupon_id'])->toArray();
                $v['user'] = Users::findFirst('id=' . $v['user_id'])->toArray();
            }
        }

        $count = AddonCoupon::count($where);
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $list);
    }


    public function offline()
    {
        $page = $this->request->get('p');
        $where = array('customer_id=' . CUR_APP_ID);
        $count = AddonCouponOfflineLog::count($where);
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 12;

        $res = AddonCouponOfflineLog::find(array($where, "order" => "created desc", "limit" => $curpage * $limit . "," . $limit));
        if ($res) {
            $res = $res->toArray();
            foreach ($res as &$v) {
                $v['coupon'] = AddonCoupon::findFirst('id=' . $v['coupon_id'])->toArray();
                $v['user'] = Users::findFirst('id=' . $v['user_id'])->toArray();
            }
        }
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $res);
        $this->view->setVar('count', $count);
    }
}
