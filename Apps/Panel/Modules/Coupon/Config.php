<?php

return array(
    'name' => 'guestbook',
    'entry' => array(
        'handler' => 'list',
        'action' => 'index'
    ),
    'menus' => array(
        'fstMenu' => array(
            'name' => '优惠劵', 'c' => 'default', 'a' => 'index',
        ),
        'secMenu' => array(
            array('name' => '优惠劵列表', 'c' => 'default', 'a' => 'index'),
            array('name' => '添加优惠劵', 'c' => 'default', 'a' => 'add'),
            array('name' => '修改优惠劵', 'c' => 'default', 'a' => 'update', 'is_hide' => true),
            array('name' => '已发放优惠劵', 'c' => 'default', 'a' => 'receive'),
            array('name' => '优惠劵订单', 'c' => 'default', 'a' => 'order'),
            array('name' => '线下使用订单', 'c' => 'default', 'a' => 'offline'),
            array('name' => '商家密码设置', 'c' => 'default', 'a' => 'setting'),
        )
    )
);
