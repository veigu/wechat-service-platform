<?php

return array(
    'name' => 'guestbook',
    'entry' => array(
        'handler' => 'list',
        'action' => 'index'
    ),
    'menus' => array(
        'fstMenu' => array(
            'name' => '积分换购', 'c' => 'default', 'a' => 'index',
        ),
        'secMenu' => array(
            array('name' => '商品列表', 'c' => 'default', 'a' => 'index', 'class' => "icon-random"),
            array('name' => '添加换购商品', 'c' => 'default', 'a' => 'add', 'class' => "icon-random"),
            array('name' => '修改换购商品', 'c' => 'default', 'a' => 'update', 'class' => "icon-random", 'is_hide' => true),
            array('name' => '换购订单', 'c' => 'default', 'a' => 'order', 'class' => "icon-random"),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'list',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
