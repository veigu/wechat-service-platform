<?php

namespace Modules\Pointmall\Handlers;

use Components\ModuleManager\ModuleHandler;
use Components\Product\TransactionManager;
use Models\Modules\Pointmall\AddonPointMallItem;
use Models\Product\Product;
use Phalcon\Tag;
use Util\EasyEncrypt;
use Util\Pagination;
use Util\Uri;

class DefaultHandler extends ModuleHandler
{
    /**
     * initialize
     *
     * @access public
     * @return void
     */
    public function initialize()
    {
        $this->uri = new Uri();
        Tag::setTitle('积分换购 - 积分商城');
    }

    /**
     * list for messages of users
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $page = $this->request->get('p');
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;
        $key = $this->request->get('key');

        $where = 'customer_id=' . CUR_APP_ID;
        $where = $key ? $where . ' and item_name like "%' . $key . '%"' : $where;

        $list = AddonPointMallItem::find(array(
            $where,
            'order' => 'sort desc,created desc',
            'limit' => $curpage * $limit . "," . $limit
        ));

        $count = AddonPointMallItem::count($where);
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $list ? $list->toArray() : '');
    }

    public function add()
    {
        Tag::setTitle('积分换购 - 添加积分换购商品');

        $id = $this->request->get('item_id');
        if ($id && is_numeric($id)) {
            $sell_item = Product::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $id);
            $hasErr = false;
            $point_item = AddonPointMallItem::findFirst('customer_id=' . CUR_APP_ID . ' and item_id=' . $id);
            if ($point_item) {
                $this->response->redirect('panel/addon/pointmall/default/update?id=' . EasyEncrypt::encode($point_item->id))->send();
                return;
            }

            if ($this->request->isPost()) {
                if (!$sell_item) {
                    $hasErr = true;
                    $this->flash->success("原始商品不存在！");
                }

                $point_item = new AddonPointMallItem();
                $point_item->item_id = $id;
                $point_item->customer_id = CUR_APP_ID;
                $point_item->created = time();

                $point_item->item_name = $this->request->getPost('item_name');
                $point_item->cost_point = $this->request->getPost('cost_point');
                $point_item->sell_price = $this->request->getPost('sell_price');
                $point_item->sort = intval($this->request->getPost('sort'));
                $point_item->postage_fee = floatval($this->request->getPost('postage_fee'));
                $point_item->item_cover = $this->request->getPost('item_cover');

                $cost_cash = $this->request->getPost('cost_cash');
                $item_limit = $this->request->getPost('item_limit');
                $item_quantity = $this->request->getPost('item_quantity');
                if ($cost_cash == "" || ((is_numeric($cost_cash) || is_float($cost_cash)) && $sell_item->sell_price >= $cost_cash)) {
                    $point_item->cost_cash = $cost_cash;
                } else {
                    $this->flash->error("用户换购所需的现金,不得大于商品销售价格（￥" . $sell_item->sell_price . "元）！");
                    $hasErr = true;
                }

                if (is_numeric($item_quantity) && $sell_item->quantity >= $item_quantity) {
                    $point_item->item_quantity = $item_quantity;
                } else {
                    $this->flash->error("商品的可换购数量，不得大于商品库存（" . $sell_item->quantity . "）！");
                    $hasErr = true;
                }

                if (is_numeric($item_limit) && $item_quantity >= $item_limit) {
                    $point_item->item_limit = $item_limit;
                } else {
                    $this->flash->error("每个用户可换购数量,不得大于可换购库存量（" . $item_quantity . "）！");
                    $hasErr = true;
                }

                $point_item->modified = time();

                if (!$hasErr) {
                    if ($point_item->save() == false) {
                        foreach ($point_item->getMessages() as $message) {
                            $this->flash->error((string)$message);
                        }
                    } else {
                        $point_item = AddonPointMallItem::findFirst('customer_id=' . CUR_APP_ID . ' and item_id=' . $id);
                        $this->response->redirect('panel/addon/pointmall/default/update?id=' . EasyEncrypt::encode($point_item->id));
                    }
                }
            }

            $this->view->setVar('sell_item', $sell_item ? $sell_item->toArray() : []);
        }
    }

    public function update()
    {
        Tag::setTitle('积分换购 - 修改积分换购商品');

        $id = $this->request->get('id');
        // app-id like 123456-1
        $id = EasyEncrypt::decode($id);

        if (!($id && is_numeric($id))) {
            // todo error
            $this->err('404', '商品未找到');
            return;
        } else {

            $point_item = AddonPointMallItem::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $id);

            if (!$point_item) {
                // todo error
                $this->err('404', '商品未找到');
                return;
            } else {

                $sell_item = Product::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $point_item->item_id);
                $hasErr = false;
                if ($this->request->isPost()) {
                    if (!$sell_item) {
                        $hasErr = true;
                        $this->flash->success("原始商品不存在！");
                    }
                    $point_item->item_name = $this->request->getPost('item_name');
                    $point_item->cost_point = $this->request->getPost('cost_point');
                    $point_item->sell_price = $this->request->getPost('sell_price');
                    $point_item->sort = intval($this->request->getPost('sort'));
                    $point_item->postage_fee = floatval($this->request->getPost('postage_fee'));
                    $point_item->item_cover = $this->request->getPost('item_cover');
                    $cost_cash = $this->request->getPost('cost_cash');
                    $item_limit = $this->request->getPost('item_limit');
                    $item_quantity = $this->request->getPost('item_quantity');
                    if ((is_numeric($cost_cash) || is_float($cost_cash)) && $sell_item->sell_price >= $cost_cash) {
                        $point_item->cost_cash = $cost_cash;
                    } else {
                        $this->flash->error("用户换购所需的现金,不得大于商品销售价格（￥" . $sell_item->sell_price . "元）！");
                        $hasErr = true;
                    }

                    if (is_numeric($item_quantity) && $sell_item->quantity >= $item_quantity) {
                        $point_item->item_quantity = $item_quantity;
                    } else {
                        $this->flash->error("商品的可换购数量，不得大于商品库存（" . $sell_item->quantity . "）！");
                        $hasErr = true;
                    }

                    if (is_numeric($item_limit) && $item_quantity >= $item_limit) {
                        $point_item->item_limit = $item_limit;
                    } else {
                        $this->flash->error("每个用户可换购数量,不得大于可换购库存量（" . $item_quantity . "）！");
                        $hasErr = true;
                    }

                    $point_item->modified = time();

                    if (!$hasErr) {
                        if ($point_item->save() == false) {
                            foreach ($point_item->getMessages() as $message) {
                                $this->flash->error((string)$message);
                            }
                        } else {
                            $this->flash->success("积分换购商品更新成功！");
                        }
                    }
                }

                $this->view->setVar('item', $point_item->toArray());
                $this->view->setVar('sell_item', $sell_item ? $sell_item->toArray() : []);
            }
        }
    }

    public function order()
    {
        \Phalcon\Tag::setTitle("微信聚合平台-订单管理");
        $this->assets->addJs('static/panel/js/app/panel/shop/transaction.order.js');
        $currentPage = $this->request->get('page', 'int');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $name = trim($this->request->get('name', 'striptags', ''));
        $status = intval($this->request->get('status', 'striptags', -1));
        $order = trim($this->request->get('order', 'striptags', ''));

        $queryBuilder = $this->modelsManager->createBuilder()
            ->addFrom('\\Models\\Shop\\ShopOrders', 'so')
            ->leftJoin("\\Models\\User\\UserAddress", 'so.address_id = ua.id', 'ua')
            ->leftJoin("\\Models\\Shop\\Logistics", 'l.key = so.logistics_type', 'l')
            ->leftJoin("\\Models\\Modules\\Pointmall\\AddonPointMallOrder", 'poi.order_number = so.order_number', 'poi')
            ->andWhere("so.customer_id=" . CUR_APP_ID . " and so.is_pointmall=1")
            ->columns("so.order_number, so.created, so.total_cash, so.paid_cash, so.is_paid, so.is_cod, so.paid_time, so.paid_type, so.paid_order, so.is_payment_arrived,
                    so.status,so.is_delivered, l.name as logistics_name, so.delivered_time, so.logistics_type, so.logistics_fee, so.logistics_order,poi.total_point,
                    ua.name, ua.province, ua.city, ua.town, ua.address, ua.phone, ua.zip_code")
            ->orderBy("so.created DESC");

        if (strlen($name) > 0) {
            $this->view->setVar("name", $name);
        }

        if ($status > -1) {
            $queryBuilder->andWhere("so.status = '{$status}'");
        }
        $this->view->setVar('status', $status);

        if (strlen($order) > 0) {
            $this->view->setVar('order', $order);
            $queryBuilder->andWhere("so.order_number='{$order}'");
        }


        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $currentPage
        ));

        $this->view->setVar('list', $pagination->getPaginate());
        $orderStatus = TransactionManager::getOrderStatus();
        $this->view->setVar("orderStatus", $orderStatus);
    }

}
