<?php

namespace Modules\Vote\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\VoteSurvey\AddonVoteSurvey;

use Models\Modules\VoteSurvey\AddonVoteSurveyLog;
use Models\Modules\VoteSurvey\AddonVoteSurveyQuestion;
use Models\Modules\VoteSurvey\AddonVoteSurveyOption;
use Phalcon\Tag;
use Util\EasyEncrypt;
use Util\Pagination;


use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionOption;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\User\Users;

//use Components\EasyEncrypt;

class VoteHandler extends ModuleHandler
{
    /**
     * initialize
     * @access public
     * @return void
     */
    public function initialize()
    {
        Tag::setTitle('模块管理-投票管理');
        $this->view->setVar('pageTitle', '投票管理');
    }

    /**
     * 投票说明
     * @access public
     * @return void
     */
    public function index()
    {
        $vote = AddonVoteSurvey::find('customer_id=' . $this->customer->id . " AND type='vote'");
        $this->view->setVar('vote', $vote->toArray());

    }

    /**
     * 添加主题
     * @access public
     * @return void
     */
    public function add()
    {
        if (!empty($_POST)) {

            $_POST['customer_id'] = CUR_APP_ID;
            $_POST['start_date'] = strtotime($_POST['start_date']);
            $_POST['end_date'] = strtotime($_POST['end_date']);
            $_POST['created'] = time();
            $_POST['type'] = 'vote';

            $question = array('');
            $record = new AddonVoteSurvey();

            $record->assign($_POST);

            // count 大于10
            /*  if ($record->count('customer_id=' . CUR_APP_ID) >= 10) {
                  $this->flash->error('每个商家最多可以创建10个预约');
                  $this->assets->addCss('static/panel/css/vipcard/vipcard.css');
                  $this->assets->addCss('static/ace/css/datepicker.css');
                  $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
              }*/
            if ($record->create()) {

                $this->response->redirect('panel/addon/vote/vote/index');

            } else $this->error();
            $this->view->disable();

        } else {
            $this->assets->addJs('static/panel/js/tools/jscolor.js');
            $this->assets->addCss('static/ace/css/datepicker.css');
            $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        }

    }

    /**
     * 投票统计
     * @access public
     * @return void
     */
    public function statistic()
    {
          $id=$this->dispatcher->getParam(0);
          if($id && is_numeric($id))
          {
              $qu=AddonVoteSurveyQuestion::findFirst('vote_id='.$id.' AND customer_id='.CUR_APP_ID);
              if($qu)
              {
                  $question_id=$qu->toArray()['id'];
                  $option=AddonVoteSurveyOption::find('question_id='.$question_id.' AND customer_id='.CUR_APP_ID);
                //  $total_count=AddonVoteSurveyOption::sum(array("question_id='.$question_id.'","column"=>"option_count"));
                  if($option)
                  {
                      $this->view->setVar('option',$option ? $option->toArray():[] );
                  }
              }
          }
    }

    //操作错误的返回信息
    public function error()
    {
        echo '操作失败!,点击<span onclick="history.go(-1);" style="color:red;">返回</span>重新操作';
        exit;
    }

    public function goBack()
    {
        echo '<script>history.go(-1);</script>';
        exit;
    }
    public function optionEdit()
    {


        $vote_id = $this->dispatcher->getParam(0);
        $vote = AddonVoteSurvey::findFirst("id=" . $vote_id . " AND customer_id=" . $this->customer->id);
        $question=AddonVoteSurveyQuestion::findFirst('vote_id='.$vote_id);
        $option=[];
        if($question)
        {
            $op=$question->toArray();
            $options=AddonVoteSurveyOption::find('question_id='.$op['id'].' AND customer_id='.CUR_APP_ID);
            $option=$options->toArray();

        }
        $this->view->setVar('vote', $vote ? $vote->toArray() : []);
        $this->view->setVar('vote_item', $option);



    }
    /*主题编辑页面*/
    public  function  voteEdit()
    {
      if($this->request->getPost())
      {
          $vote_id=$this->request->getPost('vote_id');
          $vote= AddonVoteSurvey::findFirst('id='.$vote_id." AND customer_id=".$this->customer->id);
          $vote->bgcolor=$this->request->getPost('bgcolor');
          $vote->max_by_day=$this->request->getPost('max_by_day');
          $vote->max_by_total=$this->request->getPost('max_by_total');
          $vote->topic=$this->request->getPost('topic');
          $vote->desc=$this->request->getPost('desc');
          $vote->start_date=strtotime($this->request->getPost('start_date'));
          $vote->end_date=strtotime($this->request->getPost('end_date'));
          $vote->update();
          echo "<script>window.location.href='/panel/addon/vote/vote/index'</script>";
      }
      $vote_id=$this->dispatcher->getParam(0);
      $vote = AddonVoteSurvey::findFirst("id=" . $vote_id . " AND customer_id=" . $this->customer->id);
        $this->assets->addJs('static/panel/js/tools/jscolor.js');
        $this->assets->addCss('static/ace/css/datepicker.css');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
      $this->view->setVar('vote',$vote ? $vote->toArray() :[] );
    }

    /*增加选项*/
    public function  addOption()
    {
            $vote_id = $this->request->getPost('vote_id');
            $question = AddonVoteSurveyQuestion::findFirst('vote_id=' . $vote_id.' AND customer_id='.CUR_APP_ID);
            $question= $question ? $question->toArray(): [];
            $question_id=0;
            if (!$question) {
                $res = new AddonVoteSurveyQuestion();
                $res->vote_id = $vote_id;
                $res->type = 'radio';
                $res->desc = '';
                $res->customer_id=CUR_APP_ID;
                if ($res->create()) {
                    $question_id=$res->id;
                }
            }

            $question_id=$question_id>0 ? $question_id : $question['id'];
            $option_name = $this->request->getPost('option_name');
            $option_image = $this->request->getPost('option_image') ? $this->request->getPost('option_image') : '' ;
            $option_desc = $this->request->getPost('option_desc')  ? $this->request->getPost('option_desc') : '';
            $option=new AddonVoteSurveyOption();

            $option->customer_id=CUR_APP_ID;
            $option->question_id=$question_id;
            $option->option_name=$option_name;
            $option->option_image=$option_image;
            $option->option_desc=$option_desc;
            $option->option_count=0;
            $option->create();
    }
    /*刪除選項*/
    public function  deleteOption()
    {
        $id=$this->request->getPost('id');
       $option=new AddonVoteSurveyOption();
       $option->id=$id;
        if(  $option->delete())
        {  die(json_encode(array('s'=>1)));}
        die(json_encode(array('s'=>0)));

    }
    /*删除主题*/
    public function  deleteVote()
    {
        $id=$this->request->getPost('id');
        $flag=1;
        $ques= AddonVoteSurveyQuestion::findFirst('vote_id='.$id.'  AND customer_id='.CUR_APP_ID);
        if($ques)
        {
           $qu= $ques->toArray();
           $option=AddonVoteSurveyOption::find('question_id='.$qu['id'].' AND customer_id='.CUR_APP_ID);
            //先删除选项
            if($option->delete())
            {
                //删除问题
                if(!$ques->delete())
                {
                    $flag=0;
                }
            }
            else{  $flag=0;}
        }
        if($flag){
        //删除主题
            $vote=AddonVoteSurvey::find('id='.$id.' AND customer_id='.CUR_APP_ID);
            $vote->delete();
            die(json_decode(array('error'=>1)));}
    }



}

?>