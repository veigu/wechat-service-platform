<?php

return array(
    'name' => 'test',
    'menus' => array(
        'fstMenu' => array(
            'name' => '投票管理', 'c' => 'default', 'a' => 'index'
        ),
        'secMenu' => array(
            array('name' => '投票活动列表', 'c' => 'vote', 'a' => 'index'),
            array('name' => '新增投票主题', 'c' => 'vote', 'a' => 'add'),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'default',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
