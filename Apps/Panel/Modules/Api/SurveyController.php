<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 20:30
 */

namespace Modules\Api;


use Models\Modules\VoteSurvey\AddonVoteSurvey;
use Models\Modules\VoteSurvey\AddonVoteSurveyOption;
use Models\Modules\VoteSurvey\AddonVoteSurveyQuestion;
use Util\Ajax;

class SurveyController extends ModuleApi
{
    public function delSurveyAction()
    {
        $flag = 1;
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $survey = AddonVoteSurvey::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');
        if (!$survey) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG);
        }
        $suu = $survey->toArray();
        foreach ($suu as $item) {
            //删除选题
            $question = AddonVoteSurveyQuestion::find('vote_id=' . $item['id'] . ' AND customer_id=' . CUR_APP_ID);
            if ($question) {
                $quu = $question->toArray();
                foreach ($quu as $item1) {
                    $option = AddonVoteSurveyOption::find('question_id=' . $item1['id'] . ' AND customer_id=' . CUR_APP_ID);
                    if ($option) {
                        if (!$option->delete()) {
                            $flag = 0;
                        }
                    }

                }
                if ($flag == 1) {
                    if (!$question->delete()) {
                        $flag = 0;
                    }

                }

            }
        }
        if ($flag == 1) {
            if (!$survey->delete()) {
                $flag = 0;
            }
        }
        if ($flag == 1) {
            return Ajax::init()->outRight('');
        } else {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG);
        }
    }

    public function delQuestionActionAction()
    {
        $data = $this->request->get('data');

        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id = $data;
        $flag = 1;
        $ques = AddonVoteSurveyQuestion::findFirst('id=' . $id);
        if ($ques) {
            $qu = $ques->toArray();
            $option = AddonVoteSurveyOption::find('question_id=' . $qu['id'] . ' AND customer_id=' . CUR_APP_ID);
            //先删除选项
            if ($option->delete()) {
                //删除问题
                if (!$ques->delete()) {
                    $flag = 0;
                }
            } else {
                $flag = 0;
            }
        }
        if ($flag) {
            //删除主题
            $vote = AddonVoteSurvey::find('id=' . $id . ' AND customer_id=' . CUR_APP_ID);
            $vote->delete();
        }
        return Ajax::init()->outRight('');
    }

    public function  addOptionAction()
    {
        $option_name = $this->request->get('data');
        $qid = $this->request->get('question_id');
        $q = new AddonVoteSurveyOption();
        $q->question_id = $qid;
        $q->option_name = $option_name;
        $q->option_count = 0;
        $q->customer_id = CUR_APP_ID;
        if (!$q->save()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }
        return Ajax::init()->outRight();
    }

    public function  delOptionAction()
    {
        $option_id = $this->request->get('data');
        $option = AddonVoteSurveyOption::find('id=' . $option_id . ' AND customer_id=' . CUR_APP_ID);

        if (!$option->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }

        return Ajax::init()->outRight();
    }

    public function updateQuestionAction()
    {
        $id = $this->request->get('id');
        $type = $this->request->get('type');
        $desc = $this->request->get('desc');

        $question = AddonVoteSurveyQuestion::findFirst('id=' . $id . ' AND customer_id=' . CUR_APP_ID);
        if ($question) {
            $question->type = $type;
            $question->desc = $desc;
            if (!$question->save()) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }

        }
        return Ajax::init()->outRight();
    }
} 