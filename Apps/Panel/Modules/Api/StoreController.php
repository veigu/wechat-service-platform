<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 20:29
 */

namespace Modules\Api;


use Models\Modules\Store\AddonStore;
use Models\Modules\Store\AddonStoreGroup;
use Util\Ajax;

class StoreController extends ModuleApi
{
    public function delAction()
    {
        $this->view->disable();
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonStore::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function delGroupAction()
    {
        $this->view->disable();
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonStoreGroup::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function publishAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id_in = is_array($id) && $id ? implode(',', $id) : $id;

        $m = AddonStore::find('customer_id = ' . CUR_APP_ID . ' and id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->update(array('published' => $published));
        }

        return Ajax::init()->outRight('');
    }

    public function setStoreAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $data['modified'] = time();
        if (isset($data['id']) && $data['id']) {
            $store = AddonStore::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $data['id']);
            unset($data['id']);
            if (!$store->update($data)) {
                $this->di->get('errorLogger')->error(json_encode($store->getMessages()));
                return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '数据更新失败');
            }
        } else {
            $store = new AddonStore();
            $data['created'] = time();
            $data['customer_id'] = CUR_APP_ID;
            if (!$store->create($data)) {
                $this->di->get('errorLogger')->error(json_encode($store->getMessages()));
                return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '数据更新失败');
            }
        }

        // log
        return Ajax::init()->outRight('');
    }

    public function setGroupAction()
    {
        // request params
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        foreach ($data as $row) {
            $id = isset($row['id']) ? $row['id'] : null;
            unset($row['id']); // for update it
            //if name is not exists
            if (!$row['name']) continue;
            // insert or update
            $rowData = array('name' => $row['name'], 'desc' => $row['desc']);
            if ($id) {
                unset($rowData['created']);
                $m = AddonStoreGroup::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $id);
                $res = $m->update($rowData);
            } else {
                $m = new AddonStoreGroup();
                $rowData['customer_id'] = CUR_APP_ID;
                $rowData['created'] = time();
                $res = $m->create($rowData);
            }
        }

        // log
        return Ajax::init()->outRight('');
    }
} 