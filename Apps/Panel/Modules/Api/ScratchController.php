<?php

namespace Modules\Api;

use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\Modules\Coupon\AddonCoupon;
use Models\User\Users;
use Util\Ajax;

class ScratchController extends ModuleApi {

    /**
     * ajaxPrizesGrid
     *
     * @access public
     * @return void
     */
    public function ajaxPrizesGridAction() {
        $id = $this->request->get('id');
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        // get post
        $order = $this->request->getPost('sidx', null, 'option_id');
        $sort = $this->request->getPost('sord');
        $page = $this->request->getPost('page', null, 1);
        $limit = $this->request->getPost('rows');
        $search = $this->request->getPost('_search');

        // search filters
        if ($search == true) {
            $filter = $this->request->getPost('filters');
            if (!empty($filter)) {
                $filter = json_decode($filter);
                foreach ($filter->rules as $rule) {

                    if ($rule->field == 'id') {
                        $cond = "option_id";
                    } else {
                        $cond = $rule->field;
                    }

                    switch ($rule->op) {
                        default:
                        case 'eq':
                            $cond .= " = $rule->data";
                            break;

                        case 'bw':
                            //$cond .= " like \"%$rule->data%\"";
                            $cond = "LOCATE('%$rule->data%', $cond)>0";
                            break;
                    }
                    $conditions[] = $cond;
                }
            }
        }

        // build conditions
        $conditions[] = "m.code = 'scratch'";
        $conditions[] = "m.customer_id = '" . CUR_APP_ID . "' AND id=" . $id;
        $conditions = implode(" AND ", $conditions);

        // get prizes
        $prizes = $builder = $this->modelsManager->createBuilder()->from("Models\Modules\Promotion\AddonPromotionOption")->where($conditions)->join('Models\Modules\Promotion\AddonPromotion', "Models\Modules\Promotion\AddonPromotionOption.parent_id = m.id", 'm')->orderBy($order . ' ' . $sort);
        //             ->getQuery()
        //             ->execute();

        // pagination
        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array("builder" => $prizes, "limit" => $limit, "page" => $page));
        $page = @$paginator->getPaginate();

        // build rows for jqgrid
        $data['rows'] = array();
        if (count($page->items) > 0) {

            foreach ($page->items as $idx => $prize) {
                $data['rows'][$idx]['id'] = $prize->option_id;
                $data['rows'][$idx]['option_name'] = $prize->option_name;
                $data['rows'][$idx]['option_desc'] = $prize->option_desc;
                $data['rows'][$idx]['option_value'] = $prize->award_type == 'coupon' ? AddonCoupon::findFirst('id=' . $prize->option_value)->name : $prize->option_value;
                $data['rows'][$idx]['option_number'] = $prize->option_number;
                $data['rows'][$idx]['angle'] = $prize->angle;
                $data['rows'][$idx]['chance'] = $prize->chance;
                $data['rows'][$idx]['award_type'] = $prize->award_type == 'goods' ? '商品' : ($prize->award_type == 'points' ? '积分' : '优惠券');
            }

            $data['page'] = $page->current;
            $data['total'] = $page->total_pages;
            $data['records'] = count($prizes->getQuery()->execute());
        }

        return $this->response->setJsonContent($data)->send();
    }

    public function ajaxPrizeResultsAction() {
        $order = $this->request->getPost('sidx', null, 'option_id');
        $sort = $this->request->getPost('sord');
        $page = $this->request->getPost('page', null, 1);
        $limit = $this->request->getPost('rows');
        $search = $this->request->getPost('_search');

        $id = $this->request->get('id');
        if (empty($id)) {
            return $this->response->setJsonContent(array('page' => 0, 'records' => array(), 'total' => 0, 'rows' => array()))->send();
        }
        $conditions = array();
        // search filters
        if ($search == true) {
            $filter = $this->request->getPost('filters');
            if (!empty($filter)) {
                $filter = json_decode($filter);
                foreach ($filter->rules as $rule) {

                    if ($rule->field == 'id') {
                        $cond = "p.user_prize_id";

                    } elseif ($rule->field == 'user') {

                        $users = Users::find("LOCATE(username, '{$rule->data}') > -1");
                        if (count($users) > 0) {
                            $uids = array();
                            foreach ($users as $user) {
                                $uids[] = $user->id;
                            }
                            $cond = "P.user_id";
                            $rule->data = implode(',', $uids);
                        }
                    } else {
                        $cond = $rule->field;
                    }
                    switch ($rule->op) {
                        default:
                        case 'eq':
                            $cond .= " = $rule->data";
                            break;
                        case 'bw':
                            $cond = "LOCATE('{$rule->data}', $cond) > 0";
                            break;
                        case 'in':
                            $cond .= " IN ($rule->data)";
                            break;
                    }
                    $conditions[] = $cond;
                }
            }
        }

        $conditions[] = "p.activity_id = '{$id}'";

        // build conditions
        $conditions = implode(" AND ", $conditions);

        // get prizes
        $prizes = $this->modelsManager->createBuilder()
            ->columns("p.user_prize_id AS id, p.created, mo.option_name, u.username AS user, p.code, IF(p.available = 0, '已兑奖', '未兑奖') AS available, IF(p.is_delivery = 1, '已发货', '未发货') AS is_delivery, p.delivery_company, p.delivery_no")
            ->addFrom("Models\\Modules\\Promotion\\AddonPromotionPrize", 'p')
            ->leftJoin('Models\\Modules\\Promotion\\AddonPromotionOption', "mo.option_id = p.prize_id", 'mo')
            ->leftJoin('Models\\User\\Users', "p.user_id = u.id", 'u')
            ->where($conditions)
            ->orderBy('p.' . $order . ' ' . $sort);

        // pagination
        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array("builder" => $prizes, "limit" => $limit, "page" => $page));
        $page = $paginator->getPaginate();

        // build rows for jqgrid
        $data['rows'] = array();
        if (count($page->items) > 0) {
            $data['rows'] = $page->items->toArray();
            $data['page'] = $page->current;
            $data['total'] = $page->total_pages;
            $data['records'] = count($prizes->getQuery()->execute());
        }
        $this->view->disable();
        return $this->response->setJsonContent($data)->send();
    }

    public function editPrizesAction() {
        $this->view->disable();
        $post = $this->request->getPost();

        if ($post['oper'] == 'edit') {

            if (!empty($post['id'])) {
                $record = AddonPromotionPrize::findFirst("user_prize_id = {$post['id']}");
                $record->assign($post);
                $record->update();
            }

        } else if ($post['oper'] == 'del') {

            if (!empty($post['id'])) {
                $ids = explode(',', $post['id']);
                if (count($ids) > 0) {
                    foreach ($ids as $id) {
                        $record = AddonPromotionPrize::findFirst("user_prize_id = $id");
                        $record->delete();
                    }
                }
            }
        }
        $this->response->setJsonContent(Ajax::init()->outRight())->send();
    }

    /*活动列表*/
    public function ajaxActionListGridAction() {
        // get post
        $page = $this->request->getPost('page', null, 1);
        $limit = $this->request->getPost('rows');
        $marketing = AddonPromotion::find("code = 'scratch' AND customer_id =" . CUR_APP_ID);
        // pagination
        $paginator = new \Phalcon\Paginator\Adapter\Model(array("data" => $marketing, "limit" => $limit, "page" => $page));
        $page = $paginator->getPaginate();

        // build rows for jqgrid
        $data['rows'] = array();
        if (count($page->items) > 0) {

            foreach ($page->items as $idx => $prize) {
                $data['rows'][$idx]['id'] = $prize->id;
                $data['rows'][$idx]['name'] = $prize->name;
                $data['rows'][$idx]['image'] = $prize->image;
                $data['rows'][$idx]['image'] = $prize->image;
                $data['rows'][$idx]['description'] = $prize->description;
                $data['rows'][$idx]['times'] = $prize->times;
                $data['rows'][$idx]['start_time'] = date('Y-m-d', $prize->start_time);
                $data['rows'][$idx]['end_time'] = date('Y-m-d', $prize->end_time);
            }

            $data['page'] = $page->current;
            $data['total'] = $page->total_pages;
            $data['records'] = $marketing ? count($marketing->toArray()) : 0;
        }

        return $this->response->setJsonContent($data)->send();
    }

    public function afterExecuteRoute() {

    }
}
