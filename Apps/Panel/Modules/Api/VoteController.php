<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 20:42
 */

namespace Modules\Api;


use Models\Modules\VoteSurvey\AddonVoteSurvey;
use Models\Modules\VoteSurvey\AddonVoteSurveyOption;
use Models\Modules\VoteSurvey\AddonVoteSurveyQuestion;
use Util\Ajax;

class VoteController extends ModuleApi
{
    public function delAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id = $data[0];
        $flag = 1;
        $ques = AddonVoteSurveyQuestion::findFirst('vote_id=' . $id . ' AND customer_id=' . CUR_APP_ID);
        if ($ques) {
            $qu = $ques->toArray();
            $option = AddonVoteSurveyOption::find('question_id=' . $qu['id'] . ' AND customer_id=' . CUR_APP_ID);
            //先删除选项
            if ($option->delete()) {
                //删除问题
                if (!$ques->delete()) {
                    $flag = 0;
                }
            } else {
                $flag = 0;
            }
        }
        if ($flag) {
            //删除主题
            $vote = AddonVoteSurvey::find('id=' . $id);
            $vote->delete();
        }
        return Ajax::init()->outRight('');
    }
} 