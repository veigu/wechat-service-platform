<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 20:22
 */

namespace Modules\Api;


use Models\Modules\Pictorial\AddonPictorial;
use Models\Modules\Pictorial\AddonPictorialOption;
use Util\Ajax;

class PictorialController extends ModuleApi
{
    /**
     * 增加画报选项
     **/
    public function  addOptionAction()
    {

        $id = $this->request->getPost('id');
        $option_name = $this->request->getPost('option_name');
        $thumb = $this->request->getPost('thumb');

        if (!$id || !is_numeric($id)) {
            return Ajax::init()->outError('参数错误');
        }
        $pictorial = AddonPictorial::findFirst('customer_id=' . CUR_APP_ID . ' AND id=' . $id);
        if ($pictorial) {
            $option = new AddonPictorialOption();
            $option->option_images = $thumb;
            $option->option_name = $option_name;
            $option->pictorial_id = $id;
            $option->customer_id = CUR_APP_ID;
            if ($option->create()) {
                return Ajax::init()->outRight('');
            } else {

                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'添加失败');
            }
        } else {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '未找到相关画报');
        }

    }

    /**
     * 单个删除画报选项图片
     **/
    public function delOptionAction()
    {
        $id = $this->request->getPost('id');
        if (!$id || !is_numeric($id)) {
            return Ajax::init()->outError('参数错误');
        }
        $option = AddonPictorialOption::findFirst('customer_id=' . CUR_APP_ID . ' AND id=' . $id);
        if (!$option) {
            return Ajax::init()->outError('未找到相关画报图片选项');
        }
        if ($option->delete()) {
            return Ajax::init()->outRight();
        } else {
            return Ajax::init()->outError('删除失败');
        }
    }

    /**
     *  删除选中的画报图片
     **/
    public function delAllOptionAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonPictorialOption::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    /**
     * 删除画报
     **/
    public function delAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $option = AddonPictorialOption::find('customer_id=' . CUR_APP_ID . ' and pictorial_id in (' . implode(',', $data) . ')');
        //先删除选项
        if ($option->delete()) {
            $pictorial = AddonPictorial::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');
            $pictorial->delete();
        }
        return Ajax::init()->outRight('');
    }

} 
