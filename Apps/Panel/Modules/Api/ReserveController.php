<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 20:27
 */

namespace Modules\Api;


use Models\Modules\Reserve\AddonReserve;
use Models\Modules\Reserve\AddonReserveColumns;
use Models\Modules\Reserve\AddonReserveOrders;
use Util\Ajax;

class ReserveController extends ModuleApi
{
    public function publishAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id_in = is_array($id) && $id ? implode(',', $id) : $id;

        $m = AddonReserve::find('customer_id = ' . CUR_APP_ID . ' and id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->is_active = $published;
            if (!$item->update()) {
            }
        }

        return Ajax::init()->outRight('');
    }

    public function pubColumnAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $id_in = is_array($id) && $id ? implode(',', $id) : $id;
        if (!$id_in) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $m = AddonReserveColumns::find(' id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->is_active = $published;
            if (!$item->update()) {
            }
        }

        return Ajax::init()->outRight('');
    }

    public function auditAction()
    {
        $id = $this->request->getPost('id');
        $status = intval($this->request->getPost('status'));

        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $id_in = is_array($id) && $id ? implode(',', $id) : $id;

        $m = AddonReserveOrders::find('customer_id = ' . CUR_APP_ID . ' and id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->update(array('status' => $status));
        }

        return Ajax::init()->outRight('');
    }

    public function delReserveAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonReserve::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function delOrderAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonReserveOrders::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function saveColumnAction()
    {
        $data = $this->request->getPost('data');
        $reserve_id = $this->request->getPost('reserve_id');
        $id = $this->request->getPost('id');

        if (!$reserve_id && $data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id) {
            $column = AddonReserveColumns::findFirst('id=' . $id);
            if (!$column) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }

            if (!$column->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED);
            };
        } else {
            $column = new AddonReserveColumns();
            $data['created'] = time();
            $data['is_active'] = 1;
            $data['reserve_id'] = $reserve_id;
            $column->create($data);
        }

        return Ajax::init()->outRight('');
    }

    public function delColumnAction()
    {
        $data = $this->request->get('data');
        if (!($data && is_array($data))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonReserveColumns::find('id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }
}