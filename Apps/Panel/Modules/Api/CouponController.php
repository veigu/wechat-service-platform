<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 17:41
 */

namespace Modules\Api;


use Models\Modules\Coupon\AddonCoupon;
use Util\Ajax;

class CouponController extends ModuleApi
{

    public function delAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonCoupon::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->update(array('deleted' => 1));
        }

        return Ajax::init()->outRight('');
    }

    public function setAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $data['receive_end'] = strtotime($data['receive_end']);
        $data['use_start'] = strtotime($data['use_start']);
        $data['use_end'] = strtotime($data['use_end']);
        $data['modified'] = time();
        if (isset($data['id']) && $data['id']) {
            $store = AddonCoupon::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $data['id']);
            unset($data['id']);
            if (!$store->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '数据更新失败');
            }
        } else {
            $store = new AddonCoupon();
            $data['created'] = time();
            $data['customer_id'] = CUR_APP_ID;
            if (!$store->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '数据更新失败');
            }
        }

        // log
        return Ajax::init()->outRight('');
    }

    public function publishAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id_in = is_array($id) && $id ? implode(',', $id) : $id;

        $m = AddonCoupon::find('customer_id = ' . CUR_APP_ID . ' and id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->update(array('published' => $published));
        }

        return Ajax::init()->outRight('');
    }
} 