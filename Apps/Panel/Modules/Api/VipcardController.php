<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 20:39
 */

namespace Modules\Api;


use Components\Module\VipcardManager;
use Components\Rules\PointRule;
use Models\Modules\Vipcard\AddonVipcard;
use Models\Modules\Vipcard\AddonVipcardField;
use Models\Modules\Vipcard\AddonVipcardGrade;
use Models\Modules\Vipcard\AddonVipcardSettings;
use Models\Modules\Vipcard\AddonVipcardUsers;
use Models\User\UserForCustomers;
use Models\User\UserPointLog;
use Models\User\UserPointRules;
use Util\Ajax;

class VipcardController extends ModuleApi
{
    public function publishAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id_in = is_array($id) && $id ? implode(',', $id) : $id;

        $m = AddonVipcard::find('customer_id = ' . CUR_APP_ID . ' and id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->is_active = $published;
            if (!$item->update()) {
            }
        }

        return Ajax::init()->outRight('');
    }

    public function setStatusAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('status'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id_in = is_array($id) && $id ? implode(',', $id) : $id;
        $this->db->begin();
        try {
            $m = AddonVipcardUsers::find('customer_id = ' . CUR_APP_ID . ' and id in (' . $id_in . ')');
            foreach ($m as $item) {
                $item->is_active = $published;
                if (!$item->update()) {
                    $err = "";
                    foreach($item->getMessages() as $m) {
                        $err .= (string)$m;
                    }
                    throw new \Phalcon\Exception($err);
                }
            }
            $this->db->commit();
        }
        catch(\Phalcon\Exception $e) {
            $this->db->collback();
            $this->di->get('errorLogger')->error("upate vipcard status failed: " . $e->getMessage());
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '更新用户VIP状态失败！');
        }

        return Ajax::init()->outRight('');
    }

    public function pubFieldAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $id_in = is_array($id) && $id ? implode(',', $id) : $id;
        if (!$id_in) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $m = AddonVipcardField::find(' id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->is_active = $published;
            if (!$item->update()) {
            }
        }

        return Ajax::init()->outRight('');
    }

    public function saveFieldAction()
    {
        $data = $this->request->getPost('data');
        $id = $this->request->getPost('id');

        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id) {
            $column = AddonVipcardField::findFirst('id=' . $id);
            if (!$column) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }

            if (!$column->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED);
            };
        } else {
            $column = new AddonVipcardField();
            $data['created'] = time();
            $data['is_active'] = 1;
            $data['customer_id'] = CUR_APP_ID;
            $column->create($data);
        }

        return Ajax::init()->outRight('');
    }

    public function delFieldAction()
    {
        $data = $this->request->get('data');
        if (!($data && is_array($data))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonVipcardField::find('id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function setGradeAction()
    {
        $data = $this->request->get('data');
        if (!($data && is_array($data))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $settingModel = new AddonVipcardGrade();
        foreach ($data as $row) {
            $id = isset($row['id']) ? $row['id'] : null;
            unset($row['id']); // for update it
            //if name is not exists
            if (!$row['name']) continue;
            // insert or update
            $rowData = array('name' => $row['name'], 'amount_start' => $row['amount_start'], 'amount_end' => $row['amount_end'], 'discount' => $row['discount']);
            $grade = $settingModel->findFirst('id=' . $id);
            if ($grade) {
                $grade->update($rowData);
            }
        }
        return Ajax::init()->outRight();
    }

    /**
     * set club type
     */
    public function addGradeAction()
    {
        $amount_end = intval(filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_NUMBER_INT));
        $discount = filter_input(INPUT_POST, 'discount', FILTER_SANITIZE_NUMBER_INT);
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        if (!($name && $amount_end && $discount)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $rowData = array(
            'name' => $name,
            'customer_id' => CUR_APP_ID,
            'discount' => $discount,
            'amount_end' => $amount_end,
        );

        $id = VipcardManager::init()->addGrade($rowData);

        if ($id === -1) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '请设置结束积分并且大于开始积分！');
        }
        if (!$id) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '设置失败!');
        }

        // log
        return Ajax::init()->outRight('设置添加等级头衔成功！', $id);
    }

    public function setPointAction()
    {
        // params
        $data = $this->request->get('data');
        if (!($data)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        // each do
        foreach ($data as $row) {
            $id = isset($row['id']) ? $row['id'] : null;
            unset($row['id']); // for update it
            $row['points'] = $row['quantity'];
            unset($row['quantity']);


            if ($id) {
                // update data
                $rule = UserPointRules::findFirst('id=' . $id);
                $rule->update($row);
            } else {
                $rule = new UserPointRules();
                $row['customer_id'] = CUR_APP_ID;
                $row['created'] = time();
                $rule->create($row);
            }
        }

        // log
        return Ajax::init()->outRight('操作成功', '');
    }

    /*会员卡审核*/
    public function  vipcardCheckAction()
    {
        $card_no = $this->request->get('card_no');
        $id = $this->request->get('id');
        $is_active = $this->request->get('is_active');
        $grade = $this->request->getPost('grade');
        $grade_name = $this->request->getPost('grade_name');
        $user = AddonVipcardUsers::findFirst('customer_id=' . CUR_APP_ID . " AND card_no='" . $card_no . "' AND id <> " . $id);
        if ($user) {
            Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '该卡号已经存在');
        }
        $vipcard = AddonVipcardUsers::findFirst('customer_id=' . CUR_APP_ID . " AND id = " . $id);
        $setting = AddonVipcardSettings::findFirst("customer_id=" . CUR_APP_ID . " and enable=1");
        $vipcard->card_no = $card_no;
        $vipcard->card_grade = $grade;
        $vipcard->card_grade_name = $grade_name;
        $vipcard->is_active = $is_active;
        if ($setting->auto_send == 0) {
            PointRule::init(CUR_APP_ID, $grade)->executeRule($vipcard->user_id, PointRule::BEHAVIOR_BIND_VIPCARD, $vipcard->is_company);
            /*$pointlog = UserPointLog::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $vipcard->user_id . " AND action=" . PointRule::BEHAVIOR_BIND_VIPCARD);
            if (!$pointlog) {

                $points = PointRule::init(CUR_APP_ID, 0)->getRulePoints(PointRule::BEHAVIOR_BIND_VIPCARD, $vipcard->is_company);

                $user_cus = UserForCustomers::findFirst('user_id=' . $vipcard->user_id . '  and customer_id=' . CUR_APP_ID);
                $user_cus->points = $user_cus->points + $points;
                $user_cus->points_available = $user_cus->points_available + $points;
                if ($user_cus->update()) ;
                {
                    $log = new UserPointLog();
                    $log->user_id = $vipcard->user_id;
                    $log->in_out = 'in';
                    $log->action = PointRule::BEHAVIOR_BIND_VIPCARD;
                    $log->action_desc = PointRule::$behaviorNameMap[PointRule::BEHAVIOR_BIND_VIPCARD];
                    $log->logged = time();
                    $log->value = $points;
                    $log->create();
                }
            }*/
        }
        if ($vipcard->update()) {
            Ajax::init()->outRight('操作成功', '');
        } else {
            Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '操作失败');
        }
    }

    public function setUserGradeAction() {
        $uid = $this->request->getPost('card_no', 'string');
        $gid = $this->request->getPost("gid", 'int');

        $user = AddonVipcardUsers::findFirst("customer_id = '" . CUR_APP_ID . "' AND card_no = '{$uid}'");
        if(!$user) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '用户不存在，或者您没有修改此用户信息的权限！');
        }

        $grade = AddonVipcardGrade::findFirst("customer_id = '" .CUR_APP_ID . "' AND id='{$gid}'");
        if(!$grade) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您指定的VIP等级不存在！');
        }

        if(!$user->update(array(
            'card_grade' => $gid,
            'card_grade_name' => $grade->name
        ))) {
            $errors = "";
            foreach($user->getMessages() as $err) {
                $errors .= (string)$err;
            }
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $errors);
        }

        return Ajax::init()->outRight();
    }
} 