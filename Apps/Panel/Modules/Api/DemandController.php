<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 18:12
 */

namespace Modules\Api;


use Components\Module\DemandManager;
use Models\Modules\Demand\AddonDemand;
use Models\Modules\Demand\AddonDemandReply;
use Models\Modules\Demand\AddonDemandType;
use Util\Ajax;

class DemandController extends ModuleApi
{
    public function delAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonDemand::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');
        foreach ($list as $item) {
            $item->delete();
        }

        // 删除回复
        $replies = AddonDemandReply::find('demand_id in (' . implode(',', $data) . ')');
        foreach ($replies as $reply) {
            $reply->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function delTypeAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonDemandType::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');
        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function delReplyAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonDemandReply::find(' id in (' . implode(',', $data) . ')');
        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function setTypeAction()
    {
        // request params
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        foreach ($data as $row) {
            $id = isset($row['id']) ? $row['id'] : null;
            unset($row['id']); // for update it
            //if name is not exists
            if (!$row['name']) continue;
            // insert or update
            $rowData = array('name' => $row['name'], 'desc' => $row['desc']);
            if ($id) {
                unset($rowData['created']);
                $m = AddonDemandType::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $id);
                $res = $m->update($rowData);
            } else {
                $m = new AddonDemandType();
                $rowData['customer_id'] = CUR_APP_ID;
                $rowData['created'] = time();
                $res = $m->create($rowData);
            }
        }

        // log
        return Ajax::init()->outRight('');
    }

    public function replyAction()
    {
        $data = $this->request->getPost('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $demand = AddonDemand::findFirst('id=' . $data['demand_id'] . ' and customer_id=' . CUR_APP_ID);
        if (!$demand) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        if ($demand->status == DemandManager::DEMAND_STATUS_END) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '回复功能已经结束，不能提交！');
        }

        // 添加回复
        $data['created'] = time();
        $m = new AddonDemandReply();
        if (!$m->create($data)) {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '数据插入失败！');
        }

        // 更新状态为已经回复
        $demand->update(array('status' => DemandManager::DEMAND_STATUS_REPLAYED));

        return Ajax::init()->outRight();
    }

    public function endReplyAction()
    {
        $id = $this->request->getPost('id');
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $demand = AddonDemand::findFirst('id=' . $id . ' and customer_id=' . CUR_APP_ID);
        if (!$demand) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        // 默认和未回复状态不能结束

        if (in_array($demand->status, array(DemandManager::DEMAND_STATUS_DEFAULT))) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '回复功能已经结束，不能提交！');
        }

        // 更新状态为已经结束
        $demand->update(array('status' => DemandManager::DEMAND_STATUS_END));

        return Ajax::init()->outRight();
    }
} 