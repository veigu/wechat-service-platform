<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/19
 * Time: 20:24
 */

namespace Modules\Api;


use Models\Modules\AddonPanorama;
use Util\Ajax;

class PanoramaController extends ModuleApi
{
    public function delAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = AddonPanorama::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }


    public function publishAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id_in = is_array($id) && $id ? implode(',', $id) : $id;

        $m = AddonPanorama::find('customer_id = ' . CUR_APP_ID . ' and id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->update(array('published' => $published));
        }

        return Ajax::init()->outRight('');
    }

} 