<?php

namespace Modules\Pictorial\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\Pictorial\AddonPictorialOption;
use Models\Modules\Pictorial\AddonPictorial;
use Phalcon\Tag;
use Util\Pagination;

class PictorialHandler extends ModuleHandler
{
    /**
     * initialize
     * @access public
     * @return void
     */
    public function initialize()
    {
        Tag::setTitle('模块管理-微画报管理');
        $this->view->setVar('pageTitle', '微画报管理');
    }

    public function index()
    {
        $page = $this->request->get('p');
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;
        $where = 'customer_id=' . CUR_APP_ID;
        $pictorial = AddonPictorial::find(array(
            $where,
            'order' => 'id desc',
            'limit' => $curpage * $limit . "," . $limit
        ));
        $count = AddonPictorial::count($where);
        Pagination::instance($this->view)->showPage($page, $count, $limit);
        $this->view->setVar('pictorial', $pictorial ? $pictorial->toArray() : []);

    }

    /**
     * 增加画报
     **/
    public function add()
    {
        if (!empty($_POST)) {
            $_POST['customer_id'] = CUR_APP_ID;
            $_POST['created'] = time();
            $pictorial = new AddonPictorial();
            $pictorial->assign($_POST);
            if ($pictorial->create()) {
                $this->response->redirect('panel/addon/pictorial/pictorial/index');
            } else {
                $this->error();
            }
            $this->view->disable();
        } else {
            $pictorial = AddonPictorial::find('customer_id=' . CUR_APP_ID);
            $this->view->setVar('pictorial', $pictorial ? $pictorial->toArray() : []);
        }

    }

    /**
     *画报信息编辑
     **/

    public function pictorialEdit()
    {
        if (!empty($_POST)) {
            $thumb = $this->request->getPost('thumb');
            $name = $this->request->getPost('name');
            $desc = $this->request->getPost('desc');
            $keywords = $this->request->getPost('keywords');
            $periodical = $this->request->getPost('periodical');
            $copyright = $this->request->getPost('copyright');
            $pictorial_id = $this->request->getPost('pictorial_id');
            $allow_login = $this->request->getPost('allow_login');
            $page = $this->request->getPost('page');

            $pictorial = AddonPictorial::findFirst('id=' . $pictorial_id . ' AND customer_id=' . CUR_APP_ID);
            $pictorial->thumb = $thumb;
            $pictorial->name = $name;
            $pictorial->desc = $desc;
            $pictorial->keywords = $keywords;
            $pictorial->periodical = $periodical;
            $pictorial->copyright = $copyright;
            $pictorial->allow_login = $allow_login;
            $pictorial->page = $page;
            if ($pictorial->update()) {
                $this->response->redirect('panel/addon/pictorial/pictorial/index');
            } else {
                $this->error();
            }
            $this->view->disable();

        } else {
            $pictorial_id = $this->dispatcher->getParam(0);
            $pictorial = AddonPictorial::findFirst("id=" . $pictorial_id . " AND customer_id=" . $this->customer->id);
            $this->view->setVar('pictorial', $pictorial ? $pictorial->toArray() : []);
        }
    }

    /**
     * 画报图片集列表
     **/
    function optionList()
    {
        $pictorial_id = $this->request->get('id');
        if ($pictorial_id && is_numeric($pictorial_id)) {
            $pictorial = AddonPictorial::findFirst('id=' . $pictorial_id . ' AND customer_id=' . CUR_APP_ID);

            if ($pictorial) {
                $page = $this->request->get('p');
                $curpage = $page <= 0 ? 0 : $page - 1;
                $limit = 10;
                $where = 'customer_id=' . CUR_APP_ID . '  AND pictorial_id=' . $pictorial_id;
                $option = AddonPictorialOption::find(array(
                    $where,
                    'order' => 'id desc',
                    'limit' => $curpage * $limit . "," . $limit
                ));
                $count = AddonPictorialOption::count($where);
                Pagination::instance($this->view)->showPage($page, $count, $limit);
                $pictorial = $pictorial->toArray();
                $this->view->setVar('option', $option ? $option->toArray() : []);
                $this->view->setVar('pictorial', $pictorial);

            }
        }
    }

    //操作错误的返回信息
    public function error()
    {
        echo '操作失败!,点击<span onclick="history.go(-1);" style="color:red;">返回</span>重新操作';
        exit;
    }


}