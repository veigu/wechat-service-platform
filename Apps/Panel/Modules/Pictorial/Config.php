<?php

return array(
    'name' => 'test',
    'menus' => array(
        'fstMenu' => array(
            'name' => '微画报管理', 'c' => 'pictorial', 'a' => 'index'
        ),
        'secMenu' => array(
            array('name' => '画报列表', 'c' => 'pictorial', 'a' => 'index'),
            array('name' => '新增画报', 'c' => 'pictorial', 'a' => 'add'),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'default',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
