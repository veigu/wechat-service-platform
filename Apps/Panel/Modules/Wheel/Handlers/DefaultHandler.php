<?php

namespace Modules\Wheel\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionOption;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\Modules;
use Models\Modules\Coupon\AddonCoupon;
use Models\User\Users;
use Phalcon\Tag;
use Upload\Upload;
use Util\Ajax;

class DefaultHandler extends ModuleHandler
{
    /**
     * initialize
     *
     * @access public
     * @return void
     */
    public function initialize()
    {
        Tag::setTitle('模块管理-微营销-大转盘');
    }

    /*活动列表*/
    public function index()
    {
        $marketing = AddonPromotion::find("code='wheel' AND customer_id='" . CUR_APP_ID . "'");
        if (!$marketing) {
            //
            $viewHelper = $this->view->getVar('viewHelper');
            $this->response->redirect(ltrim($viewHelper->getUrl('addAction'), '/'));

        }
        $this->assets->addCss('static/panel/css/module/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-timepicker.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');
        $this->view->setVar('list', $marketing ? $marketing->toArray() : []);
        $this->view->setVar('pageTitle', '大转盘');
        $this->view->setVar('description', '大转盘活动列表');
    }

    /* 新增活动*/
    public function  addAction()
    {
        if ($this->request->isPost()) {
            $data = $this->request->getPost();
            $start = strtotime($this->request->getPost('start_date'));
            $end = strtotime($this->request->getPost('end_date'));
            $data['start_time'] = $start;
            $data['end_time'] = $end;
            $data['customer_id'] = CUR_APP_ID;
            $data['code'] = 'wheel';
            $market = new AddonPromotion();
            $market->assign($data);
            $market->create();
            header('location:/panel/addon/wheel/default/index');


        }
        $this->assets->addCss('static/panel/css/module/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-timepicker.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');
        $this->assets->addCss('static/ace/css/datepicker.css');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->view->setVar('pageTitle', '大转盘');
        $this->view->setVar('description', '新增大转盘活动');
    }

    /*奖项列表*/
    public function awardList()
    {
        $id = $this->dispatcher->getParam(0);
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        $marketing = AddonPromotion::findFirst("code='wheel' AND customer_id='" . CUR_APP_ID . "' AND id=" . $id);
        if (!$marketing) {
            //该活动不存在
            $viewHelper = $this->view->getVar('viewHelper');
            $this->response->redirect(ltrim($viewHelper->getUrl('addAction'), '/'));
        }
        $this->assets->addCss('static/panel/css/module/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-timepicker.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');

        $this->view->setVar('pageTitle', '大转盘');
        $this->view->setVar('description', '大转盘游戏奖项');
        $this->view->setVar('id', $id);
    }

    /*添加奖项*/
    public function addAward()
    {

        $id = $_REQUEST['id'];
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        \Phalcon\Tag::setTitle('大转盘奖项设置');
        $this->assets->addJs('static/ace/js/jquery.validate.min.js');
        $this->assets->addJs('static/ace/js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('static/panel/modules/market.wheel.js');
        $request = $this->request;
        $optionId = $request->getQuery('option_id');
        $marketing = AddonPromotion::findFirst("code='wheel' AND customer_id='" . CUR_APP_ID . "' AND id=" . $id);
        if (!empty($optionId)) {
            $option = AddonPromotionOption::findFirst('option_id="' . $optionId . '"');
        } else {
            $option = new AddonPromotionOption();
        }


        $this->view->setVar('option', $option);
        $this->view->setVar('id', $id);
        if ($this->request->isPost()) {
            $optionName = $request->getPost('option_name');
            $optionDesc = $request->getPost('option_desc');
            $optionNumber = $request->getPost('option_number');
            $option_value = $request->getPost('option_value');
            $award_type = $request->getPost('award_type');

            $angle = $request->getPost('angle');
            $chance = $request->getPost('chance');

            if (empty($optionName)) {
                $this->flash->error('活动名称不能为空！');
                return false;
            }

            $this->db->begin();
            try {
                $option->option_name = $optionName;
                $option->option_desc = $optionDesc;
                $option->option_number = $optionNumber;
                $option->angle = $angle;
                $option->chance = $chance;
                $option->parent_id = $marketing->id;
                $option->option_value = $option_value;
                $option->award_type = $award_type;


                if ($option->save() == false) {
                    foreach ($option->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                    throw new \Phalcon\Exception("保存失败");
                }

                $this->db->commit();
                $viewHelper = $this->view->getVar('viewHelper');
                $this->flash->success('保存成功!');
                $this->response->redirect(ltrim($viewHelper->getUrl('awardList/' . $id), '/'));
            } catch (\Exception $e) {
                $this->db->rollback();
                $this->flash->error($e->getMessage());
            }
        }
    }

    public function edit()
    {
        $id = $_REQUEST['aid'];
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }

        \Phalcon\Tag::setTitle('大转盘奖项设置');
        $coupon = AddonCoupon::find("customer_id=" . CUR_APP_ID . "");
        $this->assets->addJs('static/ace/js/jquery.validate.min.js');
        $this->assets->addJs('static/ace/js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('static/panel/modules/market.wheel.js');
        $request = $this->request;
        $optionId = $request->getQuery('option_id');
        $marketing = AddonPromotion::findFirst("code='wheel' AND customer_id='" . CUR_APP_ID . "' AND id=" . $id);
        if (!empty($optionId)) {
            $option = AddonPromotionOption::findFirst('option_id="' . $optionId . '"');
        } else {
            $option = new AddonPromotionOption();
        }

        $this->view->setVar('option', $option);
        $this->view->setVar('aid', $id);
        $this->view->setVar('coupon', $coupon ? $coupon->toArray() : []);

        if ($request->isPost()) {
            $optionName = $request->getPost('option_name');
            $optionDesc = $request->getPost('option_desc');
            $optionNumber = $request->getPost('option_number');
            $angle = $request->getPost('angle');
            $chance = $request->getPost('chance');
            $option_value = $request->getPost('option_value');
            $award_type = $request->getPost('award_type');

            if (empty($optionName)) {
                $this->flash->error('活动名称不能为空！');
                return false;
            }

            $this->db->begin();
            try {
                $option->option_name = $optionName;
                $option->option_desc = $optionDesc;
                $option->option_number = $optionNumber;
                $option->angle = $angle;
                $option->chance = $chance;
                $option->parent_id = $marketing->id;
                $option->option_value = $option_value;
                $option->award_type = $award_type;


                if ($option->save() == false) {
                    foreach ($option->getMessages() as $message) {
                        $this->flash->error((string)$message);
                    }
                    throw new \Phalcon\Exception("保存失败");
                }

                $this->db->commit();
                $viewHelper = $this->view->getVar('viewHelper');
                $this->flash->success('保存成功!');
                $this->response->redirect(ltrim($viewHelper->getUrl('awardList/' . $id), '/'));
            } catch (\Exception $e) {
                $this->db->rollback();
                $this->flash->error($e->getMessage());
            }
        }
    }

    public function remove()
    {
        $request = $this->request;

        if ($request->isPost()) {

            $id = $request->getPost('id');

            if ($request->getPost('oper') == 'del') {

                $this->db->begin();
                try {

                    $ids = array();
                    if (!empty($id)) {
                        $ids = explode(',', $id);

                        foreach ($ids as $id) {
                            AddonPromotionOption::findFirst("option_id = $id")->delete();
                        }
                    }

                    $this->db->commit();

                } catch (\Exception $e) {

                    $this->db->rollback();
                    $this->flash->error($e->getMessage());

                }
            }
        }
        exit;
    }

    public function setting()
    {
        $id = $this->dispatcher->getParam(0);
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        \Phalcon\Tag::setTitle('大转盘设置');
        $this->assets->addCss('static/ace/css/datepicker.css');
        $this->assets->addJs('static/ace/js/jquery.validate.min.js');
        $this->assets->addJs('static/ace/js/fuelux/fuelux.spinner.min.js');
        $this->assets->addJs('static/ace/js/jquery.hotkeys.min.js');
        $this->assets->addJs('static/ace/js/bootstrap-wysiwyg.min.js');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->assets->addJs('static/panel/modules/market.wheel.js');
        $request = $this->request;
        $marketing = AddonPromotion::findFirst('code="wheel" AND customer_id="' . CUR_APP_ID . '" AND id=' . $id);
        $flag = true;
        if (empty($marketing)) {
            $marketing = new AddonPromotion;
            $marketing->code = "wheel";
        }
        if ($request->isPost()) {
            $name = $request->getPost('name');
            $description = $request->getPost('description');
            $times = $request->getPost('times');
            $exchange_time_limit = $request->getPost('exchange_time_limit');
            $file = $request->getPost('file');
            $startDate = strtotime($request->getPost('start_date'));
            $endDate = strtotime($request->getPost('end_date'));

            if (empty($name)) {
                $this->flash->error('活动名称不能为空！');
                $flag = false;
            }

            if (empty($file)) {
                $this->flash->error('活动图片不能为空！');
                $flag = false;
            }

            if (empty($startDate)) {
                $this->flash->error('活动开始时间不能为空！');
                $flag = false;
            }

            if (empty($endDate)) {
                $this->flash->error('活动结束时间不能为空！');
                $flag = false;
            }

            if ($endDate < $startDate) {
                $this->flash->error('活动结束时间不能小于开始时间！');
                $flag = false;
            }

            $this->db->begin();
            if ($flag) {
                try {
                    $marketing->name = $name;
                    $marketing->description = $description;
                    $marketing->customer_id = CUR_APP_ID;
                    $marketing->times = $times;
                    $marketing->exchange_time_limit=$exchange_time_limit;
                    $marketing->image = $file;
                    $marketing->start_time = $startDate;
                    $marketing->end_time = $endDate;

                    if ($marketing->save() == false) {
                        foreach ($marketing->getMessages() as $message) {
                            $this->flash->error((string)$message);
                        }
                        throw new \Phalcon\Exception("保存失败");
                    }

                    $this->db->commit();
                    $viewHelper = $this->view->getVar('viewHelper');
                    $this->flash->success('保存成功，现在你可以添加奖项啦。<a href="' . $viewHelper->getUrl('awardList/' . $id) . '">添加奖项</a>');
                } catch (\Exception $e) {
                    $this->db->rollback();
                    $this->flash->error($e->getMessage());
                }
            }
        }
        $this->view->setVar('marketing', $marketing);
    }

    /**
     * ajaxPrizesGrid
     *
     * @access public
     * @return void
     */
    public function ajaxPrizesGrid()
    {
        $this->view->disable();
        $id = $this->request->get('id');
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        // get post
        $order = $this->request->getPost('sidx', null, 'option_id');
        $sort = $this->request->getPost('sord');
        $page = $this->request->getPost('page', null, 1);
        $limit = $this->request->getPost('rows');
        $search = $this->request->getPost('_search');

        // search filters
        if ($search == true) {
            $filter = $this->request->getPost('filters');
            if (!empty($filter)) {
                $filter = json_decode($filter);
                foreach ($filter->rules as $rule) {

                    if ($rule->field == 'id') {
                        $cond = "option_id";
                    } else {
                        $cond = $rule->field;
                    }

                    switch ($rule->op) {
                        default:
                        case 'eq':
                            $cond .= " = $rule->data";
                            break;
                        case 'bw':
                            //$cond .= " like \"%$rule->data%\"";
                            $cond = "LOCATE('%$rule->data%', $cond)>0";
                            break;
                    }
                    $conditions[] = $cond;
                }
            }
        }

        // build conditions
        $conditions[] = "m.code = 'wheel'";
        $conditions[] = "m.customer_id = '" . CUR_APP_ID . "' AND id=" . $id;
        $conditions = implode(" AND ", $conditions);

        // get prizes
        $prizes = $builder = $this->modelsManager->createBuilder()
            ->from("Models\Modules\Promotion\AddonPromotionOption")
            ->where($conditions)
            ->leftJoin('Models\Modules\Promotion\AddonPromotion', "Models\Modules\Promotion\AddonPromotionOption.parent_id = m.id", 'm')
            ->orderBy($order . ' ' . $sort);

        // pagination
        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(
            array(
                "builder" => $prizes,
                "limit" => $limit,
                "page" => $page
            )
        );
        $page = @$paginator->getPaginate();

        // build rows for jqgrid
        $data['rows'] = array();
        if (count($page->items) > 0) {
            foreach ($page->items as $idx => $prize) {
                $data['rows'][$idx]['id'] = $prize->option_id;
                $data['rows'][$idx]['option_name'] = $prize->option_name;
                $data['rows'][$idx]['option_desc'] = $prize->option_desc;
                $data['rows'][$idx]['option_value'] = $prize->award_type == 'coupon' ? AddonCoupon::findFirst('id=' . $prize->option_value)->name : $prize->option_value;
                $data['rows'][$idx]['option_number'] = $prize->option_number;
                $data['rows'][$idx]['angle'] = $prize->angle;
                $data['rows'][$idx]['chance'] = $prize->chance;
                $data['rows'][$idx]['award_type'] = $prize->award_type == 'goods' ? '商品' : ($prize->award_type == 'points' ? '积分' : '优惠券');
            }

            $data['page'] = $page->current;
            $data['total'] = $page->total_pages;
//            $data['records'] = count($prizes->getQuery()->execute());
        }

        return $this->response->setJsonContent($data)->send();
    }

    public function removePic()
    {
        $id = $this->request->get('id');
        $marketing = AddonPromotion::findFirst("id = $id");
        $file = ROOT . DS . 'Public' . $marketing->image;
        if (!empty($marketing->image) && file_exists($file)) {

            $this->db->begin();
            try {
                $marketing->image = '';
                if ($marketing->save()) {
                    @unlink($file);
                } else {
                    throw new \Phalcon\Exception("删除失败");
                }

                $this->db->commit();
                $this->flash->success("删除图片成功！");
                $viewHelper = $this->view->getVar('viewHelper');
                $this->response->redirect(ltrim($viewHelper->getUrl('setting'), '/'));

            } catch (\Exception $e) {
                $this->db->rollback();
                $this->flash->error($e->getMessage());
            }
        }
        $this->view->disable();
    }

    public function stats()
    {
        $id = $this->dispatcher->getParam(0);
        if (!$id || !is_numeric($id)) {
            $this->flash->error('Illegal request parameters');
            $this->view->disable();
            exit;
        }
        $this->view->setVar('pageTitle', '结果统计');
        $this->view->setVar('description', '大转盘游戏中奖结果统计');

        $this->assets->addCss('static/panel/css/interation/guestbook.css');
        $this->assets->addCss('static/ace/css/jquery-ui-1.10.3.full.min.css');
        $this->assets->addCss('static/ace/css/bootstrap-datetimepicker.min.css');
        $this->assets->addCss('static/ace/css/ui.jqgrid.css');
        $this->assets->addCss('static/ace/css/ace.min.css');
        $this->assets->addCss('static/ace/css/ace-rtl.min.css');
        $this->assets->addCss('static/ace/css/ace-skins.min.css');

        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.full.min.js');
        $this->assets->addJs('static/ace/js/typeahead-bs2.min.js');
        $this->assets->addJs('static/ace/js/jquery-ui-1.10.3.full.min.js');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->assets->addJs('static/ace/js/jqGrid/jquery.jqGrid.min.js');
        $this->assets->addJs('static/ace/js/jqGrid/i18n/grid.locale-zh_CN.js');
        $this->assets->addJs('static/ace/js/jquery.easy-pie-chart.min.js');
        $this->assets->addJs('static/ace/js/flot/jquery.flot.min.js');
        $this->assets->addJs('static/ace/js/flot/jquery.flot.pie.min.js');


        $queryString = <<<EOF
    COUNT(*) AS times,
    SUM(IF(p.code IS NOT NULL AND LENGTH(p.code) > 0 , 1, 0)) AS win_times,
    SUM(IF(p.code IS NOT NULL AND LENGTH(p.code) > 0 , 0, 1)) AS lose_times,
    COUNT(*) AS participants,
    SUM(IF(p.code IS NOT NULL AND LENGTH(p.code) > 0 AND p.available = 0, 1, 0)) AS cashed_lottery,
    SUM(IF(p.code IS NOT NULL AND LENGTH(p.code) > 0 AND p.available = 1, 1, 0)) AS not_cashed_lottery
EOF;
        $statistic = $this->modelsManager->createBuilder()->addFrom("\\Models\\Modules\\Promotion\\AddonPromotionPrize", 'p')
            ->columns($queryString)
            ->where("p.activity_id = '{$id}'")
            ->limit(1)
            ->getQuery()
            ->execute()[0]
            ->toArray();

        $prize_stats = $this->modelsManager->createBuilder()->addFrom("\\Models\\Modules\\Promotion\\AddonPromotionOption", 'op')
            ->columns("COUNT(*) AS times, op.option_name")
            ->join("\\Models\\Modules\\Promotion\\AddonPromotionPrize", "p.prize_id = op.option_id", "p")
            ->where("p.activity_id = '{$id}'")
            ->groupBy("op.option_id")
            ->getQuery()
            ->execute()
            ->toArray();
        $statistic['prize_statisic'] = $prize_stats;
        $prizes = array();


        if (count($statistic['prize_statisic'])) {
            foreach ($statistic['prize_statisic'] as $prize) {
                $prizes[] = array('label' => $prize['option_name'] . " ({$prize['times']}次)", 'data' => $prize['times'], 'color' => '#' . sprintf("%02X", rand(0, 255)) . sprintf("%02X", rand(0, 255)) . sprintf("%02X", rand(0, 255)));
            }
        }


        $statistic['prizes'] = json_encode($prizes);


        $this->view->setVars($statistic);
        $this->view->setVar('id', $id);
    }

    public function ajaxPrizeResults() {
        // get post
        // get post
        $order = $this->request->getPost('sidx', null, 'option_id');
        $sort = $this->request->getPost('sord');
        $page = $this->request->getPost('page', null, 1);
        $limit = $this->request->getPost('rows');
        $search = $this->request->getPost('_search');

        $id = $this->request->get('id');
        if (empty($id)) {
            return $this->response->setJsonContent(array('page' => 0, 'records' => array(), 'total' => 0, 'rows' => array()))->send();
        }
        $conditions = array();
        // search filters
        if ($search == true) {
            $filter = $this->request->getPost('filters');
            if (!empty($filter)) {
                $filter = json_decode($filter);
                foreach ($filter->rules as $rule) {
                    if ($rule->field == 'id') {
                        $cond = "p.user_prize_id";

                    } elseif ($rule->field == 'user') {

                        $users = Users::find("LOCATE(username, '{$rule->data}') > -1");
                        if (count($users) > 0) {
                            $uids = array();
                            foreach ($users as $user) {
                                $uids[] = $user->id;
                            }
                            $cond = "P.user_id";
                            $rule->data = implode(',', $uids);
                        }
                    } else {
                        $cond = $rule->field;
                    }
                    switch ($rule->op) {
                        default:
                        case 'eq':
                            $cond .= " = $rule->data";
                            break;
                        case 'bw':
                            $cond = "LOCATE('{$rule->data}', $cond) > 0";
                            break;
                        case 'in':
                            $cond .= " IN ($rule->data)";
                            break;
                    }
                    $conditions[] = $cond;
                }
            }
        }

        $conditions[] = "p.activity_id = '{$id}'";

        // build conditions
        $conditions = implode(" AND ", $conditions);

        // get prizes
        $prizes = $this->modelsManager->createBuilder()
            ->columns("p.user_prize_id AS id, p.created, mo.option_name, u.username AS user, p.code, IF(p.available = 0, '已兑奖', '未兑奖') AS available, IF(p.is_delivery = 1, '已发货', '未发货') AS is_delivery, p.delivery_company, p.delivery_no")
            ->addFrom("Models\\Modules\\Promotion\\AddonPromotionPrize", 'p')
            ->leftJoin('Models\\Modules\\Promotion\\AddonPromotionOption', "mo.option_id = p.prize_id", 'mo')
            ->leftJoin('Models\\User\\Users', "p.user_id = u.id", 'u')
            ->where($conditions)
            ->orderBy('p.' . $order . ' ' . $sort);

        // pagination
        $paginator = new \Phalcon\Paginator\Adapter\QueryBuilder(array("builder" => $prizes, "limit" => $limit, "page" => $page));
        $page = $paginator->getPaginate();

        // build rows for jqgrid
        $data['rows'] = array();
        if (count($page->items) > 0) {
            $data['rows'] = $page->items->toArray();
            $data['page'] = $page->current;
            $data['total'] = $page->total_pages;
            $data['records'] = count($prizes->getQuery()->execute());
        }

        $this->view->disable();
        return $this->response->setJsonContent($data)->send();
    }

    public function editPrizes()
    {
        $post = $this->request->getPost();

        if ($post['oper'] == 'edit') {

            if (!empty($post['id'])) {
                $record = AddonPromotionPrize::findFirst("user_prize_id = {$post['id']}");
                $record->assign($post);
                $record->update();
            }

        } else if ($post['oper'] == 'del') {

            if (!empty($post['id'])) {
                $ids = explode(',', $post['id']);
                if (count($ids) > 0) {
                    foreach ($ids as $id) {
                        $record = AddonPromotionPrize::findFirst("user_prize_id = $id");
                        $record->delete();
                    }
                }
            }
        }
        exit;
    }

    public function beforeRender($event, $view)
    {

    }

    /*活动列表*/
    public function ajaxActionListGrid()
    {
        $this->view->disable();
        $page = $this->request->getPost('page', null, 1);
        $limit = $this->request->getPost('rows');
        $marketing = AddonPromotion::find("code = 'wheel' AND customer_id = " . CUR_APP_ID);
        // pagination
        $paginator = new \Phalcon\Paginator\Adapter\Model(array(
            "data" => $marketing,
            "limit" => $limit,
            "page" => $page
        ));
        $page = $paginator->getPaginate();

        // build rows for jqgrid
        $data['rows'] = array();
        if (count($page->items) > 0) {
            foreach ($page->items as $idx => $prize) {
                $data['rows'][$idx]['id'] = $prize->id;
                $data['rows'][$idx]['name'] = $prize->name;
                $data['rows'][$idx]['image'] = $prize->image;
                $data['rows'][$idx]['image'] = $prize->image;
                $data['rows'][$idx]['description'] = $prize->description;
                $data['rows'][$idx]['times'] = $prize->times;
                $data['rows'][$idx]['start_time'] = date('Y-m-d', $prize->start_time);
                $data['rows'][$idx]['end_time'] = date('Y-m-d', $prize->end_time);
            }

            $data['page'] = $page->current;
            $data['total'] = $page->total_pages;
            $data['records'] = $marketing ? count($marketing->toArray()) : 0;
        }
        return $this->response->setJsonContent($data)->send();
    }

    public function ajaxTypeChange()
    {
        $type = $this->request->getPost('type');
        $data = '';
        switch ($type) {
            case "goods":
                $data = "  <label for='angle' class='col-sm-4 control-label no-padding-right'> 商品名称： </label>" .
                    " <div class='col-sm-8'>" .
                    " <input type='text' class='col-xs-10 col-sm-5' id='option_value' required name='option_value' placeholder='商品名称''/>" .
                    " </div>";
                break;
            case "points":
                $data = "  <label for='angle' class='col-sm-4 control-label no-padding-right'> 积分数量： </label>" .
                    " <div class='col-sm-8'>" .
                    " <input type='text' class='col-xs-10 col-sm-5' id='option_value' name='option_value' required placeholder='积分数量''/>" .
                    " </div>";
                break;
            case "coupon":

                $res = AddonCoupon::find('customer_id=' . CUR_APP_ID . "");
                $coupon = $res ? $res->toArray() : [];
                $data = "  <label for='angle' class='col-sm-4 control-label no-padding-right'> 优惠券列表： </label>" .
                    " <div class='col-sm-8'>" .

                    "<select name='option_value' id='option_value' required>";
                foreach ($coupon as $val) {
                    $data .= "<option value='" . $val['id'] . "'>" . $val['name'] . "</option>";
                }
                $data .= "</select><a target='_blank' href='/panel/addon/coupon/default/add'>添加优惠券</a></div>";

                break;
            default:
                $data = "  <label for='angle' class='col-sm-4 control-label no-padding-right'> 角度： </label>" .
                    " <div class='col-sm-8'>" .
                    " <input type='text' class='col-xs-10 col-sm-5' id='angle' name='angle' placeholder='请根据设置的角度填写''/>" .
                    " </div>";
                break;

        }

        die(json_encode(array('data' => $data)));

    }
}
