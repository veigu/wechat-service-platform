<?php

namespace Modules\Panorama\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\AddonPanorama;
use Phalcon\Tag;
use Util\EasyEncrypt;
use Util\Pagination;
use Util\Uri;

class DefaultHandler extends ModuleHandler
{
    /**
     * initialize
     *
     * @access public
     * @return void
     */
    public function initialize()
    {
        $this->uri = new Uri();
        Tag::setTitle('360全景展示');
    }

    /**
     * list for messages of users
     *
     * @access public
     * @return void
     */
    public function index()
    {
        $page = $this->request->get('p');
        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 10;
        $key = $this->request->get('key');

        $where = 'customer_id=' . CUR_APP_ID;
        $where = $key ? $where . ' and title like "%' . $key . '%"' : $where;

        $list = AddonPanorama::find(array(
            $where,
            'order' => 'sort desc,created desc',
            'limit' => $curpage * $limit . "," . $limit
        ));

        $count = AddonPanorama::count($where);
        Pagination::instance($this->view)->showPage($page, $count, $limit);

        $this->view->setVar('list', $list ? $list->toArray() : '');
    }

    public function add()
    {
        Tag::setTitle('360全景展示-添加全景图');
        if ($this->request->isPost()) {
            $pano = new AddonPanorama();
            $pano->customer_id = CUR_APP_ID;
            $pano->title = $this->request->getPost('title');
            $pano->detail = $this->request->getPost('detail');
            $pano->front = $this->request->getPost('front');
            $pano->right = $this->request->getPost('right');
            $pano->back = $this->request->getPost('back');
            $pano->left = $this->request->getPost('left');
            $pano->top = $this->request->getPost('top');
            $pano->down = $this->request->getPost('down');
            $pano->published = $this->request->getPost('published');

            $pano->created = time();
            $pano->modified = time();

            if ($pano->save() == false) {
                foreach ($pano->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
            } else {
                $this->flash->success("全景图添加成功！ <a href='/panel/addon/panorama/default/update?id=" . EasyEncrypt::encode($pano->id) . "'>查看并修改</a>");
            }
        }
    }

    public function update()
    {
        Tag::setTitle('360全景展示-修改全景图');

        $id = $this->request->get('id');
        // app-id like 123456-1
        $id = EasyEncrypt::decode($id);

        if (!($id && is_numeric($id))) {
            // todo error
            return $this->err('404', 'panorama could not found');
        } else {

            $pano = AddonPanorama::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $id);
            if (!$pano) {
                // todo error
                return $this->err('404', 'panorama could not found');
            } else {
                if ($this->request->isPost()) {
                    $pano->customer_id = CUR_APP_ID;
                    $pano->title = $this->request->getPost('title');
                    $pano->detail = $this->request->getPost('detail');
                    $pano->front = $this->request->getPost('front');
                    $pano->right = $this->request->getPost('right');
                    $pano->back = $this->request->getPost('back');
                    $pano->left = $this->request->getPost('left');
                    $pano->top = $this->request->getPost('top');
                    $pano->down = $this->request->getPost('down');
                    $pano->published = $this->request->getPost('published');

                    $pano->created = time();
                    $pano->modified = time();

                    if ($pano->save() == false) {
                        foreach ($pano->getMessages() as $message) {
                            $this->flash->error((string)$message);
                        }
                    } else {
                        $this->flash->success("全景图更新成功！");
                    }
                }

                $this->view->setVar('item', $pano->toArray());
            }

        }
    }

    public function intro()
    {

    }


}
