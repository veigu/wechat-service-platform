<?php

return array(
    'name' => 'guestbook',
    'entry' => array(
        'handler' => 'list',
        'action' => 'index'
    ),
    'menus' => array(
        'fstMenu' => array(
            'name' => '360全景', 'c' => 'default', 'a' => 'index'
        ),
        'secMenu' => array(
            array('name' => '全景图列表', 'c' => 'default', 'a' => 'index'),
            array('name' => '添加全景图', 'c' => 'default', 'a' => 'add'),
            array('name' => '修改全景图', 'c' => 'default', 'a' => 'update', 'is_hide' => true),
            array('name' => '全景图制作教程', 'c' => 'default', 'a' => 'intro'),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'list',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
