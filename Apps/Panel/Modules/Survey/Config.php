<?php
return array(
    'name' => 'test',
    'menus' => array(
        'fstMenu' => array(
            'name' => '问卷调查管理', 'c' => 'default', 'a' => 'index'
        ),
        'secMenu' => array(
            array('name' => '问卷调查活动列表', 'c' => 'survey', 'a' => 'index'),
            array('name' => '新增问卷调查', 'c' => 'survey', 'a' => 'add'),
        )
    ),
    'render' => array(
        'template' => '',
        'layout' => 'list', //fixed enum items: "block", "list", "page"
        'style' => array(
            'current' => '',
            'items' => array(
                array(
                    'name' => 'default',
                    'thumb' => '/kkkkkkk/thumb.img',
                    'desc' => '默认'
                ),
                //more
            )
        ),
    ),
    'handlers' => array(
        'entry' => 'default',
        'items' => array(
            "default" => array(
                'main',
                //more
            ),
            //more
        )
    )
);
