<?php

namespace Modules\Survey\Handlers;

use Components\ModuleManager\ModuleHandler;
use Models\Modules\VoteSurvey\AddonVoteSurvey;

use Models\Modules\VoteSurvey\AddonVoteSurveyLog;
use Models\Modules\VoteSurvey\AddonVoteSurveyQuestion;
use Models\Modules\VoteSurvey\AddonVoteSurveyOption;
use Phalcon\Tag;
use Util\EasyEncrypt;
use Util\Pagination;


use Models\Modules\Promotion\AddonPromotion;
use Models\Modules\Promotion\AddonPromotionOption;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\User\Users;


class SurveyHandler extends ModuleHandler
{
    /**
     * initialize
     * @access public
     * @return void
     */
    public function initialize()
    {
        Tag::setTitle('模块管理-问卷调查管理');
        $this->view->setVar('pageTitle', '问卷调查管理');
    }

    /**
     * 问卷调查说明
     * @access public
     * @return void
     */
    public function index()
    {
        $survey = AddonVoteSurvey::find('customer_id=' . $this->customer->id . " AND type='survey'");
        $this->view->setVar('survey', $survey->toArray());

    }
    /**
     * 增加问卷调查
     **/
    public function add()
    {
        if (!empty($_POST))
        {
            $_POST['customer_id'] = CUR_APP_ID;
            $_POST['start_date'] = strtotime($_POST['start_date']);
            $_POST['end_date'] = strtotime($_POST['end_date']);
            $_POST['created'] = time();
            $_POST['type'] = 'survey';
            $record = new AddonVoteSurvey();
            $record->assign($_POST);
            if ($record->create()) {
                $this->response->redirect('panel/addon/survey/survey/index');
            } else $this->error();
            $this->view->disable();
        }
        else
        {
            $survey = AddonVoteSurvey::find('customer_id=' . $this->customer->id . " AND type='survey'");
            $this->view->setVar('survey', $survey->toArray());
            $this->assets->addJs('static/panel/js/tools/jscolor.js');
            $this->assets->addCss('static/ace/css/datepicker.css');
            $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        }

    }
    /**
     * 问卷调查信息编辑
     **/
    public function  surveyEdit()
    {
        if($this->request->getPost())
        {
            $survey_id=$this->request->getPost('survey_id');
            $survey= AddonVoteSurvey::findFirst('id='.$survey_id." AND customer_id=".$this->customer->id);
            $survey->bgcolor=$this->request->getPost('bgcolor');
            $survey->topic=$this->request->getPost('topic');
            $survey->desc=$this->request->getPost('desc');
            $survey->start_date=strtotime($this->request->getPost('start_date'));
            $survey->end_date=strtotime($this->request->getPost('end_date'));
            $survey->update();
            echo "<script>window.location.href='/panel/addon/survey/survey/index'</script>";
        }
        $survey_id=$this->dispatcher->getParam(0);
        $survey = AddonVoteSurvey::findFirst("id=" . $survey_id . " AND customer_id=" . $this->customer->id);
        $this->assets->addJs('static/panel/js/tools/jscolor.js');
        $this->assets->addCss('static/ace/css/datepicker.css');
        $this->assets->addJs('static/ace/js/date-time/bootstrap-datepicker.min.js');
        $this->view->setVar('survey',$survey ? $survey->toArray() :[] );

    }
    /**
     * 选题编辑
     **/
    public function questionList()
    {
        $survey_id=$this->dispatcher->getParam(0);
        if($survey_id && is_numeric(($survey_id)))
        {
            $question= AddonVoteSurveyQuestion::find('vote_id='.$survey_id.'  AND customer_id='.CUR_APP_ID.' order by id DESC');
            $this->view->setVar('survey_id', $survey_id);
            $this->view->setVar('survey',$question ? $question->toArray() : []);
        }
        else
        {
            $this->flash->error('未发现该问卷调查');
        }
    }
    /**
     * 增加选题
     **/
    public function addQuestion()
    {
        if($this->request->getPost())
        {
            $record = new AddonVoteSurveyQuestion();
            $_POST['customer_id']=CUR_APP_ID;
            $record->assign($_POST);
            if ($record->create()) {
                $this->response->redirect('panel/addon/survey/survey/questionList/'.$_POST['vote_id']);
            } else $this->error();
            $this->view->disable();
        }
        else
        {
            $survey_id=\Util\EasyEncrypt::decode($this->dispatcher->getParam(0));
            if($survey_id && is_numeric($survey_id))
            {
                $this->view->setVar('survey_id',$survey_id);
            }
            else
            {
                $this->flash->error('hacking attempt');
                $this->view->disable();
            }
        }


    }

    /**
     * 选项编辑
     **/
    public function optionEdit()
    {
        $question_id=\Util\EasyEncrypt::decode($this->dispatcher->getParam(0));

        if($question_id && is_numeric($question_id))
        {
            $option=AddonVoteSurveyOption::find('question_id='.$question_id.' order by id DESC');
            $this->view->setVar('option',$option ? $option->toArray(): []);
            $this->view->setVar('question_id',$question_id);
        }
        else{$this->flash->error('error');
            $this->view->disable();}
    }
    /**
     * 问卷调查统计汇总
     **/
    public function totalStatistics()
    {
        $survey_id=$this->dispatcher->getParam(0);
        $page=$this->request->get('page');
        $page=$page ? $page :1;
        $limit=$page-1;
        $option=array();
        $question=array();
        if(!$survey_id || !is_numeric($survey_id))
        {
            $this->flash->error('no that survey');
            $this->view->disable();
        }
        else{
            $survey= AddonVoteSurvey::count('customer_id='.CUR_APP_ID." AND id=".$survey_id);
            if($survey<=0)
            {
                $this->flash->error('no that survey');
                $this->view->disable();
            }
            else{
                $question_array=AddonVoteSurveyQuestion::find('vote_id='.$survey_id." AND  customer_id=".CUR_APP_ID." limit $limit, 1");
                if($question_array &&count($question_array->toArray())>0)
                {
                    $question_count=AddonVoteSurveyQuestion::count('vote_id='.$survey_id);
                    $question=$question_array->toArray()[0];
                    $option=AddonVoteSurveyOption::find('question_id='.$question['id']." AND customer_id= ".CUR_APP_ID);
                    $option = $option ? $option->toArray() :[];
                    $this->view->setVar('count',$question_count);
                }

            }

        }
        $this->view->setVar('question',$question);
        $this->view->setVar('option',$option);
        $this->view->setVar('page',$page);


    }
    /**
     * 问卷调查单个统计
     **/
    public function statistics()
    {
        $question_id=$this->dispatcher->getParam(0);
        $option=array();
        $question=array();
        if(!$question_id || !is_numeric($question_id))
        {
            $this->flash->error('no that question');
            $this->view->disable();
        }
        else{
                    $question=AddonVoteSurveyQuestion::findFirst('id='.$question_id." AND  customer_id=".CUR_APP_ID);
                    if($question)
                    {
                        $question=$question->toArray();
                        $option=AddonVoteSurveyOption::find('question_id='.$question['id']." AND  customer_id=".CUR_APP_ID);
                        $option = $option ? $option->toArray() :[];
                    }
                    else{
                        $this->flash->error('no that question');
                        $this->view->disable();
                    }
        }
        $this->view->setVar('question',$question);
        $this->view->setVar('option',$option);


    }
    //操作错误的返回信息
    public function error()
    {
        echo '操作失败!,点击<span onclick="history.go(-1);" style="color:red;">返回</span>重新操作';
        exit;
    }

}