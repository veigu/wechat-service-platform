<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/23
 * Time: 13:49
 */

namespace Multiple\Api\Controllers;


use Components\CustomerManager;

class AccountController extends ApiBase
{
    protected $_check_login = false;

    public function loginAction()
    {
        $account = $this->request->getPost('account');
        $password = $this->request->getPost('password');
        CustomerManager::init()->login($account, $password);
    }

    public function regAction()
    {
        $account = $this->request->getPost('account');
        $email = $this->request->getPost('email', 'email', false);
        $password = $this->request->getPost('password');

        CustomerManager::init()->reg($account, $email, $password);
    }

    public function forgotAction()
    {
        $email = $this->request->getPost('email', 'email', false);
        CustomerManager::init()->forgot($email);
    }
}