<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/23
 * Time: 13:49
 */

namespace Multiple\Api\Controllers;


use Components\CustomerManager;
use Util\Ajax;

class CustomerController extends ApiBase
{
    // 企业资料
    public function saveProfileAction()
    {
        $data = $this->request->get("data");
        CustomerManager::init()->saveProfile($data);
    }

    // 行业套餐
    public function pickPackageAction()
    {
        $package = $this->request->get("package");
        $industry = $this->request->get("industry");

        CustomerManager::init()->pickPackage($industry, $package);
    }

    // 自选套餐
    public function customPackageAction()
    {
        $all_func = $this->request->get("all_func");
        $all_func = json_decode(base64_decode($all_func), true);
        CustomerManager::init()->customPackage($all_func);
    }

    // 生成订单
    public function genOrderAction()
    {
        $duration = $this->request->get("duration");
        $duration = ltrim(base64_decode($duration), "estt");
        if (!is_numeric($duration)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "购买时长参数有误");
        }

        // 生成订单
        return CustomerManager::init()->genOrder($duration);
    }

    public function applyO2OAction()
    {
        return CustomerManager::init()->applyO2O();
    }
}