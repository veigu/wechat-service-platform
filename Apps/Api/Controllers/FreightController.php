<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-9-22
 * Time: 上午11:24
 */

namespace Multiple\Api\Controllers;


use Components\Product\FreightManager;
use Models\Shop\ShopFreightTpl;
use Models\Shop\ShopFreightTplItems;
use Util\Ajax;

class FreightController extends ApiBase
{
    public function saveAction()
    {
        FreightManager::init()->setFreight();
    }


    public function delAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = ShopFreightTpl::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        if (!$list->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }

        $listm = ShopFreightTplItems::find('customer_id=' . CUR_APP_ID . ' and freight_id in (' . implode(',', $data) . ')');

        if (!$listm->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }
        return Ajax::init()->outRight('');
    }


    public function deltAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }


        $listm = ShopFreightTplItems::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        if (!$listm->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }
        return Ajax::init()->outRight('');
    }


}