<?php
/**
 * Created by PhpStorm.
 * User: luguiwu
 * Date: 14-9-19
 * Time: 上午10:04
 */

namespace Multiple\Api\Controllers;

use Models\Shop\ShopOrderReturn;
use Models\Shop\ShopOrders;
use Util\Ajax;

class OrderController extends ApiBase
{
    /*
     * 退货/换货时卖家确认收货
     * */
    public function receiveGoodAction()
    {
       $id=$this->request->get('id');
       $return= ShopOrderReturn::findFirst('id='.$id." and customer_id=".$this->customer->id);
       if($return)
       {
           $return->seller_received_goods=1;
           if($return->update())
           {
               return Ajax::init()->outRight('');
           }
           return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"确认收货失败");
       }
        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"illegal params");
    }
    /*
    * 退货/换货时确认退款成功
    * */
    public function payAction()
    {
        $id=$this->request->get('id');
        $return= ShopOrderReturn::findFirst('id='.$id." and customer_id=".$this->customer->id);
        if($return)
        {
            $return->is_seller_refunded=1;//退款成功
            if($return->type==2)
            {
                $return->is_end=1;//处理结束
            }
            if($return->update())
            {
                return Ajax::init()->outRight('');
            }
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"确认退款失败");
        }
        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"illegal params");
    }
    /*
 * 退货/换货时拒绝申请
 * */
    public function refuseAction()
    {
        $id=$this->request->get('id');
        $reason=$this->request->get('reason');
        $return= ShopOrderReturn::findFirst('id='.$id." and customer_id=".$this->customer->id);
        if($return)
        {
            $return->seller_refused_reason=$reason;
            $return->is_seller_accept=2;
            $return->is_end=1;
            if($return->update())
            {
                return Ajax::init()->outRight('');
            }
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"操作失败");
        }
        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"illegal params");
    }
       /*
 * 退货/换货时通过申请
 * */
    public function passCheckAction()
    {
        $id=$this->request->get('id');
        $reason=$this->request->get('reason');
        $return= ShopOrderReturn::findFirst('id='.$id." and customer_id=".$this->customer->id);
        if($return)
        {
            $return->is_seller_accept=1;
            if($return->update())
            {
                return Ajax::init()->outRight('');
            }
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"操作失败");
        }
        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"illegal params");
    }
 /*
 * 换货时填写新订单号
 * */
    public function newOrderAction()
    {
        $id=$this->request->get('id');
        $order_number=$this->request->get('order_number');
        $order=ShopOrders::count("customer_id=".$this->customer->id." and order_number=".$order_number);
        if($order<=0)
        {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"该订单不存在");
        }
        $return= ShopOrderReturn::findFirst('id='.$id." and customer_id=".$this->customer->id);
        if($return)
        {
            $return->back_order_number=$order_number;
            if($return->update())
            {
                return Ajax::init()->outRight('');
            }
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"操作失败");
        }
        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"illegal params");
    }

    /*
* 换货完成
* */
    public function finishAction()
    {
        $id=$this->request->get('id');
        $return= ShopOrderReturn::findFirst('id='.$id." and customer_id=".$this->customer->id);
        if($return)
        {
            $return->is_end=1;
            if($return->update())
            {
                return Ajax::init()->outRight('');
            }
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"操作失败");
        }
        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"illegal params");
    }

    /*更改订单实付金额*/
    public function changePaidCashAction()
    {
           $order_no=$this->request->getPost('order');
           $paid_cash=$this->request->getPost('paid_cash');
           if(!($order_no && $paid_cash && is_numeric($order_no))){
               return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"illegal params");
           }
           $order=ShopOrders::findFirst('customer_id='.CUR_APP_ID." AND order_number=".$order_no);
           if(!$order)
           {
               return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"该订单不存在");
           }
           $order->paid_cash=$paid_cash;
           if($order->update())
           {
               return Ajax::init()->outRight('');
           }
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,"操作失败");

    }
} 