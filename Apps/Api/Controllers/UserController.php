<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-8-19
 * Time: 上午11:49
 */

namespace Multiple\Api\Controllers;

use Components\WeChat\RequestFactory;
use Download\Csv;
use Models\Modules\Promotion\AddonPromotionPrize;
use Models\Modules\Vipcard\AddonVipcardUsers;
use Models\User\UserForCustomers;
use Models\User\UserGroups;
use Models\User\UserPointLog;
use Models\User\UserPointRules;
use Models\User\Users as user;
use Models\User\Users;
use Models\User\UsersWechat;
use Models\User\UsersWeibo;
use Models\User\UserWechatGroup;
use Models\User\UserWeiboGroup;
use Util\Ajax;


class UserController extends ApiBase
{
    const GROUP_TYPE_LOCAL = 1;
    const GROUP_TYPE_WECHAT = 2;
    const GROUP_TYPE_WEIBO = 4;
    public function setPointRules()
    {
        // params
        $data = $this->request->get('data');
        if (!($data)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        // each do
        foreach ($data as $row) {
            $id = isset($row['id']) ? $row['id'] : null;
            unset($row['id']); // for update it
            $row['points'] = $row['quantity'];
            unset($row['quantity']);

            if ($id) {
                // update data
                $rule = UserPointRules::findFirst('id=' . $id);
                $rule->update($row);
            } else {
                $rule = new UserPointRules();
                $row['customer_id'] = CUR_APP_ID;
                $row['created'] = time();
                $rule->create($row);
            }
        }

        // log
        return Ajax::init()->outRight('操作成功', '');
    }

    /*删除用户*/
    public function delUserAction()
    {
        $data = $this->request->getPost('data');
        $data = implode(',', $data);
        if (!$data || empty($data)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $this->db->begin();
        try {
            $list = user::find('id in (' . $data . ')');
            //删除用户表数据
            foreach ($list as $item) {
                if(!$item->delete()) {
                    throw new \Phalcon\Exception("remove user table data failed");
                }
            }
            //删除user_for_customer表数据
            $list = UserForCustomers::find('user_id in ( ' . $data . ') AND customer_id=' . CUR_APP_ID);
            if($list) {
                foreach ($list as $item) {
                    if(!$item->delete()) {
                        throw new \Phalcon\Exception("remove UserForCustomers table data failed");
                    }
                }
            }

            //删除user_point_log表数据
            $list = UserPointLog::find('user_id in ( ' . $data . ') AND customer_id=' . CUR_APP_ID);
            if($list) {
                foreach ($list as $item) {
                    if(!$item->delete()) {
                        throw new \Phalcon\Exception("remove UserPointLog table data failed");
                    }
                }
            }
            //删除user_prize表数据
            $list = AddonPromotionPrize::find('user_id in ( ' . $data . ')');
            if($list) {
                foreach ($list as $item) {
                    if(!$item->delete()) {
                        throw new \Phalcon\Exception("remove AddonPromotionPrize table data failed");
                    }
                }
            }

            $list = AddonVipcardUsers::find('user_id in ( ' . $data . ')');
            if($list) {
                foreach ($list as $item) {
                    if(!$item->delete()) {
                        throw new \Phalcon\Exception("remove AddonVipcardUsers table data failed");
                    }
                }
            }

            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $e->getMessage());
        }

        return Ajax::init()->outRight('');

    }

    /*批量导出用户*/
    public function exportUserAction()
    {
        $data = $this->request->get('data');
        $header = "用户id,电话,邮箱,用户名,性别,生日,年龄,地址,微信,微博,支付宝,淘宝,拍拍,QQ,企业/个人,企业信息";
        $resp = array();
        $user = $this->db->query(
            "select u.id,u.phone,u.email,u.username,u.gender,u.birthday,u.age,u.province,u.city,u.town,u.weixin,u.weibo,u.alipay,u.taobao,u.paipai,u.qq,u.is_company,u.company_info
             FROM users as u  left join user_for_customers as ufc on u.id=ufc.user_id
             where u.id in(" . $data . ") and ufc.customer_id=" . CUR_APP_ID
        )->fetchAll();
        foreach ($user as $k => $v) {
            $resp[$k]['id'] = $v['id'];
            $resp[$k]['phone'] = $v['phone'];
            $resp[$k]['email'] = $v['email'];
            $resp[$k]['username'] = $v['username'];
            $resp[$k]['gender'] = $v['gender'] == 'm' ? '男' : ($v['gender'] == 'f' ? '女' : '保密');
            $resp[$k]['birthday'] = $v['phone'];
            $resp[$k]['age'] = $v['age'];
            $resp[$k]['address'] = $v['province'] . ' ' . $v['city'] . ' ' . $v['town'];
            $resp[$k]['weixin'] = $v['weixin'];
            $resp[$k]['weibo'] = $v['weibo'];
            $resp[$k]['alipay'] = $v['alipay'];
            $resp[$k]['taobao'] = $v['taobao'];
            $resp[$k]['paipai'] = $v['paipai'];
            $resp[$k]['qq'] = $v['qq'];
            $resp[$k]['is_company'] = $v['is_company'] == 1 ? '企业' : '个人';
            $resp[$k]['company_info'] = $v['company_info'];

        }
        Csv::init()->export_csv($header, $resp, time());
        exit;
    }

    /*一键导出所有用户*/
    public function exportAllUserAction()
    {
        $header = "用户id,电话,邮箱,用户名,性别,生日,年龄,地址,微信,微博,支付宝,淘宝,拍拍,QQ,企业/个人,企业信息";
        $resp = array();
        $user = $this->db->query(
            "select u.id,u.phone,u.email,u.username,u.gender,u.birthday,u.age,u.province,u.city,u.town,u.weixin,u.weibo,u.alipay,u.taobao,u.paipai,u.qq,u.is_company,u.company_info
             FROM users as u  left join user_for_customers as ufc on u.id=ufc.user_id
             where ufc.customer_id=" . CUR_APP_ID
        )->fetchAll();
        foreach ($user as $k => $v) {
            $resp[$k]['id'] = $v['id'];
            $resp[$k]['phone'] = $v['phone'];
            $resp[$k]['email'] = $v['email'];
            $resp[$k]['username'] = $v['username'];
            $resp[$k]['gender'] = $v['gender'] == 'm' ? '男' : ($v['gender'] == 'f' ? '女' : '保密');
            $resp[$k]['birthday'] = $v['phone'];
            $resp[$k]['age'] = $v['age'];
            $resp[$k]['address'] = $v['province'] . ' ' . $v['city'] . ' ' . $v['town'];
            $resp[$k]['weixin'] = $v['weixin'];
            $resp[$k]['weibo'] = $v['weibo'];
            $resp[$k]['alipay'] = $v['alipay'];
            $resp[$k]['taobao'] = $v['taobao'];
            $resp[$k]['paipai'] = $v['paipai'];
            $resp[$k]['qq'] = $v['qq'];
            $resp[$k]['is_company'] = $v['is_company'] == 1 ? '企业' : '个人';
            $resp[$k]['company_info'] = $v['company_info'];

        }
        Csv::init()->export_csv($header, $resp, time());
        exit;
    }

    /**
     * @param int $gid
     * @param string $name
     * @param string $desc
     * @param string $rules
     */
    public function groupAddAction()
    {
        if (!$this->request->isAjax()) {
            $this->response->setJsonContent(array(
                'code' => 1,
                'message' => '只接受AJAX请求!'
            ));
            $this->response->send();
        }
        $type = $this->request->getPost("type", "int");
        $gid = $this->request->getPost('gid', 'int');
        $name = $this->request->getPost('name', 'string');
        $desc = $this->request->getPost('desc', 'string');
        $rules = trim($this->request->getPost('rules', 'string'), ',');
        $data = array(
            'code' => 0,
            'result' => '',
            'message' => ''
        );

        if (empty($name)) {
            $data['code'] = 1;
            $data['message'][] = "等级名称不能为空！";
        }
        if (empty($desc)) {
            $data['code'] = 1;
            $data['message'][] = "等级描述不能为空！";
        }

        if ($data['code'] > 0) {
           return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $data['message']);
        } else {
            if($type == self::GROUP_TYPE_WECHAT) {
                if($gid) {
                    //todo rename group
                    $request = RequestFactory::create("GroupUpdate", $this->customer_id, $this->customer_wechat->app_id, $this->customer_wechat->app_secret);
                    $request->set('id', $gid);
                    $request->set('name', $name);
                    $request->run();
                    if($request->isFailed()) {
                        $this->di->get('wechatLogger')->error("change wechat user group name failed: customer - " . $this->customer_id . ' group - ' . $gid . ' new name - ' . $name);
                    }
                    $ug = UserGroups::findFirst("group_id='{$gid}'");
                    $ug->name = $name;

                    if (!$ug->save()) {
                        $data['code'] = 1;
                        foreach ($ug->getMessages() as $message) {
                            $data['message'][] = $message;
                        }
                    } else {
                        $data['result'] = $ug->id;
                    }
                }
                else {
                    //todo add new group
                    $request = RequestFactory::create("GroupCreate", $this->customer_id, $this->customer_wechat->app_id, $this->customer_wechat->app_secret);
                    $request->set('name', $name);
                    $request->run();
                    if($request->isFailed()) {
                        $data['code'] = 1;
                        $this->di->get('wechatLogger')->error("change wechat user group name failed: customer - " . $this->customer_id . ' group - ' . $gid . ' new name - ' . $name . ". failed reason:" . $request->getErrorMessage());
                        $data['message'] = $request->getErrorMessage();
                    }
                    else {
                        $result = $request->getResult();
                        $ug = new UserGroups();
                        $ug->count = $result->count;
                        $ug->customer_id = $this->customer_id;
                        $ug->name = $name;
                        $ug->group_id = $request->id;

                        if (!$ug->save()) {
                            $data['code'] = 1;
                            foreach ($ug->getMessages() as $message) {
                                $data['message'][] = $message;
                            }
                        } else {
                            $data['result'] = $ug->id;
                        }
                    }

                }
            }
            else if($type == self::GROUP_TYPE_WEIBO) {

            }
            else {
                if ($gid) {
                    $ug = UserGroups::findFirst("id='{$gid}'");
                } else {
                    $ug = new UserGroups();
                    $ug->created = time();
                }
                $rules = (array)explode(',', $rules);
                $ug->customer_id = CUR_APP_ID;
                $ug->name = $name;
                $ug->desc = $desc;
                $ug->rules = json_encode($rules);

                if (!$ug->save()) {
                    $data['code'] = 1;
                    foreach ($ug->getMessages() as $message) {
                        $data['message'][] = $message;
                    }
                } else {
                    $data['result'] = $ug->id;
                }
            }
            if($data['code'] > 0) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $data['message']);
            }
            else {
                return Ajax::init()->outRight('', $data);
            }
        }
    }

    public function groupRemoveAction()
    {
        if (!$this->request->isAjax()) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "只接受AJAX请求");
        }
        $gid = $this->request->getPost('gid', 'int');
        $type = $this->request->getPost('type', 'int');
        $data = array(
            'code' => 0,
            'result' => '',
            'message' => ''
        );


        if (empty($gid) || empty($type)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "请指定要删除的等级");
        } else {
            switch($type) {
                case self::GROUP_TYPE_LOCAL: {
                   $count= UserForCustomers::count("group_id=".$gid);
                   if($count>0)
                   {
                       return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "该分组下存在用户,请先移除该分组下的用户再进行此操作");
                   }

                    $ug=UserGroups::findFirst("id=".$gid);
                    break;
                }
                case self::GROUP_TYPE_WECHAT: {
                    $count= UsersWechat::count("group_id=".$gid);
                    if($count>0)
                    {
                        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "该分组下存在用户,请先移除该分组下的用户再进行此操作");
                    }
                    $ug=UserWechatGroup::findFirst("group_id=".$gid);
                    break;
                }
                case self::GROUP_TYPE_WEIBO: {
                    $count= UsersWeibo::count("group_id=".$gid);
                    if($count>0)
                    {
                        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "该分组下存在用户,请先移除该分组下的用户再进行此操作");
                    }
                    $ug=UserWeiboGroup::findFirst("group_id=".$gid);
                    break;
                }
            }

            if ($ug->delete()) {
                $data['message'] = '删除成功!';
            } else {
                $data['code'] = 1;
                foreach ($ug->getMessages() as $message) {
                    $data['message'][] = $message;
                }
            }

            if($type == self::GROUP_TYPE_WECHAT) {
                //todo
            }
            else if($type == self::GROUP_TYPE_WEIBO) {
                //todo
            }
            return Ajax::init()->outRight();
        }
    }

    public function setGroupAction() {
        $uid = $this->request->getPost('uid', 'int');
        $gid = $this->request->getPost("gid", 'int');

        $user = UserForCustomers::findFirst("customer_id = '" . CUR_APP_ID . "' AND user_id = '{$uid}'");
        if(!$user) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '用户不存在，或者您没有修改此用户信息的权限！');
        }

        $grade = UserGroups::findFirst("customer_id = '" .CUR_APP_ID . "' AND id='{$gid}'");
        if(!$grade) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您指定的分组不存在！');
        }

        if(!$user->update(array(
            'group_id' => $gid
        ))) {
            $errors = "";
            foreach($user->getMessages() as $err) {
                $errors .= (string)$err;
            }
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $errors);
        }

        return Ajax::init()->outRight();
    }

    public function checkUserAction() {
        $uid = $this->request->getPost('uid', 'int', 0);
        $active = $this->request->getPost('active', 'int', 0);
        if(empty($uid)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "没有指定要管理的用户");
        }

        $user = UserForCustomers::findFirst("customer_id=" . CUR_APP_ID . " AND user_id = " . $uid);
        if(!$user) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "没有找到您要管理的用户");
        }

        if(!$user->update(array(
            'active' => $active
        ))) {
            $errs = '';
            foreach($user->getMessages() as $err) {
                $errs .= (string)$err;
            }
            $this->di->get(errorLogger)->error("customer active user failed:" . $errs);
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $errs);
        }
        return Ajax::init()->outRight();
    }
} 