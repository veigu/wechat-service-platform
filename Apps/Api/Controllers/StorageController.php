<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 5/7/14
 * Time: 5:52 PM
 */

namespace Multiple\Api\Controllers;


use Models\Customer\CustomerStorage;
use Models\Customer\CustomerStorageCount;
use Phalcon\Mvc\Model\Query\Builder;
use Util\Ajax;
use Util\Config;

class StorageController extends ApiBase
{
    protected $_check_login = false;

    public function getAction()
    {
        CustomerStorage::find('');
    }

    public function getFolderAction()
    {
        $u = $this->request->get('u');

        if (!$u) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $params = array(
            'models' => array('Models\Customer\CustomerStorage'),
            'columns' => array('type', 'folder', 'count(folder) as count', 'sum(size) as total'),
            'group' => array('type', 'folder'),
        );

        $queryBuilder = new Builder($params);

        $res = $queryBuilder->where('customer_id=' . $u)->getQuery()->execute();
        $res = $res->toArray();

        $folder = [];
        foreach ($res as $v) {
            $folder [$v['type']][] = array('folder' => $v['folder'], 'count' => $v['count'], 'total' => $v['total']);
        }

        $count = CustomerStorageCount::findFirst('customer_id=' . $u);

        return Ajax::init()->outRight('', array('folder' => $folder, 'count' => $count));
    }

    public function getImgAction()
    {
        $this->get('img');
    }

    public function getVideoAction()
    {
        $this->get('video');
    }

    public function getVoiceAction()
    {
        $this->get('audio');
    }

    public function getFileAction()
    {
        $this->get('file');
    }

    private function get($type)
    {
        $q = $this->request->get('t');
        $u = $this->request->get('u');
        $page = $this->request->get('p');
        $curPage = $page <= 0 ? 0 : $page - 1;

        if (!$u) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $limit = 12;
        $param = 'customer_id="' . $u . '" AND type = "' . $type . '"';
        $param .= $q ? ' AND folder = "' . $q . '"' : '';
        $cusCount = CustomerStorageCount::findFirst('customer_id=' . $u);
        $files = CustomerStorage::find(array($param, 'order' => 'created desc', "limit" => $curPage * $limit . "," . $limit));
        // file base url
        $baseUrl = Config::getSite('upload', 'baseUrl');
        if ($files) {
            $files = $files->toArray();
            foreach ($files as &$file) {
                $file['url'] = rtrim($baseUrl, '/') . '/' . ltrim($file['url'], '/');
            }
        }
        $count = CustomerStorage::count($param);
        $res['list'] = $files;
        $res['count'] = $count;
        $res['limit'] = $limit;
        $res['used'] = $cusCount ? $cusCount->toArray() [$type] : 0;

        return Ajax::init()->outRight('', $res);
    }

}