<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/8/14
 * Time: 11:37 AM
 */

namespace Multiple\Api\Controllers;


use Models\Customer\CustomerStorage;
use Models\Customer\CustomerStorageCount;
use Upload\Fdfs;
use Upload\Upload;
use Util\Ajax;
use Util\Config;

class UploadController extends ApiBase
{
    /**
     * upload img
     */
    public function imgAction()
    {
        // 初始化配置
        $conf = Config::getSite('upload', 'pic');
        $upload = new Upload();
        $upload->upExt = $conf['ext'];
        $upload->maxAttachSize = $conf['maxAttachSize'];
        $buff = $upload->upOne();
        $this->checkFile($buff['md5']);
        if ($this->config->uploadSaveType == "normal" || $this->request->get('type') == "normal") {
            $file = $upload->saveFile($buff);
            $fileUrl = $file['url'];
            $url = $file['url'];
        } else {
            // save into dfs
            $file = Fdfs::getInstance($conf['fastDFS'])->upload_filebuff($buff['buff'], $buff['ext']);
            $url = '/' . $file['group_name'] . '/' . $file['filename'];
            $fileUrl = rtrim(Config::getSite('upload', 'baseUrl'), '/') . '/' . $file['group_name'] . '/' . $file['filename'];
        }

        // save into db
        $id = $this->syncDb('img', array('md5' => $buff['md5'], 'ext' => $buff['ext'], 'url' => $url, 'size' => $buff['size'], 'name' => $buff['name']));


        if ($this->request->get('from') == 'xheditor') {
            echo $msg = "{'msg':'" . $fileUrl . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
            exit;
        }

        return Ajax::init()->outRight('', array('url' => $fileUrl, 'id' => $id, 'name' => $buff['name'], 'ext' => $buff['ext']));
    }

    /**
     * upload img
     */
    public function imgLocalAction()
    {
        $upload = new Upload();
        $upload->upExt = 'jpg|jpeg|gif|png|bmp|ico';
        $upload->maxAttachSize = 2097152; // 2M
        $buff = $upload->upOne();
        $file = $upload->saveFile($buff);

        $id = $this->syncDb('img', array('md5' => $buff['md5'], 'ext' => $buff['ext'], 'url' => $file['url'], 'size' => $buff['size'], 'name' => $buff['name']));

        if ($this->request->get('from') == 'xheditor') {
            echo $msg = "{'msg':'" . $file['url'] . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
            exit;
        }

        return Ajax::init()->outRight('', array('url' => $file['url'], 'id' => $id, 'name' => $buff['name'], 'ext' => $buff['ext']));
    }

    /**
     * upload video
     */
    public function videoAction()
    {
        // 初始化配置
        $conf = Config::getSite('upload', 'video');
        $upload = new Upload();
        $upload->upExt = $conf['ext'];
        $upload->maxAttachSize = $conf['maxAttachSize'];
        $buff = $upload->upOne();
        $this->checkFile($buff['md5']);

        // save into dfs
        $file = Fdfs::getInstance($conf['fastDFS'])->upload_filebuff($buff['buff'], $buff['ext']);
        $fileUrl = rtrim(Config::getSite('upload', 'baseUrl'), '/') . '/' . $file['group_name'] . '/' . $file['filename'];

        // save into db
        $id = $this->syncDb('img', array('md5' => $buff['md5'], 'ext' => $buff['ext'], 'url' => '/' . $file['group_name'] . '/' . $file['filename'], 'size' => $buff['size'], 'name' => $buff['name']));
        if ($this->request->get('from') == 'xheditor') {
            echo $msg = "{'msg':'" . $fileUrl . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
            exit;
        }

        return Ajax::init()->outRight('', array('url' => $fileUrl, 'id' => $id, 'name' => $buff['name'], 'ext' => $buff['ext']));
    }

    /**
     * upload video
     */
    public function audioAction()
    {
        // 初始化配置
        $conf = Config::getSite('upload', 'audio');
        $upload = new Upload();
        $upload->upExt = $conf['ext'];
        $upload->maxAttachSize = $conf['maxAttachSize'];
        $buff = $upload->upOne();
        $this->checkFile($buff['md5']);

        // save into dfs
        $file = Fdfs::getInstance($conf['fastDFS'])->upload_filebuff($buff['buff'], $buff['ext']);
        $fileUrl = rtrim(Config::getSite('upload', 'baseUrl'), '/') . '/' . $file['group_name'] . '/' . $file['filename'];

        // save into db
        $id = $this->syncDb('img', array('md5' => $buff['md5'], 'ext' => $buff['ext'], 'url' => '/' . $file['group_name'] . '/' . $file['filename'], 'size' => $buff['size'], 'name' => $buff['name']));
        if ($this->request->get('from') == 'xheditor') {
            echo $msg = "{'msg':'" . $fileUrl . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
            exit;
        }

        return Ajax::init()->outRight('', array('url' => $fileUrl, 'id' => $id, 'name' => $buff['name'], 'ext' => $buff['ext']));
    }

    /**
     * upload video
     */
    public function fileAction()
    {
        // 初始化配置
        $conf = Config::getSite('upload', 'file');
        $upload = new Upload();
        $upload->upExt = $conf['ext'];
        $upload->maxAttachSize = $conf['maxAttachSize'];
        $buff = $upload->upOne();
        $this->checkFile($buff['md5']);

        // save into dfs
        $file = Fdfs::getInstance($conf['fastDFS'])->upload_filebuff($buff['buff'], $buff['ext']);
        $fileUrl = rtrim(Config::getSite('upload', 'baseUrl'), '/') . '/' . $file['group_name'] . '/' . $file['filename'];

        // save into db
        $id = $this->syncDb('img', array('md5' => $buff['md5'], 'ext' => $buff['ext'], 'url' => '/' . $file['group_name'] . '/' . $file['filename'], 'size' => $buff['size'], 'name' => $buff['name']));
        if ($this->request->get('from') == 'xheditor') {
            echo $msg = "{'msg':'" . $fileUrl . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
            exit;
        }

        return Ajax::init()->outRight('', array('url' => $fileUrl, 'id' => $id, 'name' => $buff['name'], 'ext' => $buff['ext']));
    }

    /**
     * save remote img
     */
    public function saveimgAction()
    {
        // 初始化配置
        $conf = Config::getSite('upload', 'pic');
        $arrUrls = explode('|', $_REQUEST['urls']);
        $urlCount = count($arrUrls);
        $upload = new Upload();
        $upload->upExt = 'jpg|jpeg|gif|png|bmp';
        for ($i = 0; $i < $urlCount; $i++) {
            $fileinfo = $upload->saveRemoteImg($arrUrls[$i]);
            if (!empty($fileinfo['buff']) && !empty($fileinfo['ext'])) {
                // save into dfs
                $file = Fdfs::getInstance($conf['fastDFS'])->upload_filebuff($fileinfo['buff'], $fileinfo['ext']);
                $fileUrl = $conf['baseUrl'] . $file['group_name'] . '/' . $file['filename'];

                // save into db
                $id = $this->syncDb('img', array('md5' => $fileinfo['md5'], 'ext' => $fileinfo['ext'], 'url' => $file['group_name'] . '/' . $file['filename'], 'size' => $fileinfo['size'], 'name' => $buff['name']));
                if ($this->request->get('from') == 'xheditor') {
                    echo $msg = "{'msg':'" . $fileUrl . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
                    exit;
                }

                $arrUrls[$i] = $file['url'];
            }
        }


//        echo 'http://' . Config::getItem('domain.img') . implode('http://' . Config::getItem('domain.img') . '|', $arrUrls);

    }

    /**
     * save to db
     *
     * @param $type //'img','video','audio','file','other'
     * @param $tmp
     * @return \Phalcon\Mvc\Model\Resultset
     */
    public function syncDb($type, $tmp)
    {
        $data = array(
            'customer_id' => CUR_APP_ID,
            'ext' => $tmp['ext'],
            'type' => $type,
            'md5' => $tmp['md5'],
            'size' => $tmp['size'],
            'name' => $tmp['name'],
            'url' => $tmp['url'],
            'folder' => date('Ym'),
            'created' => time()
        );

        // 插入存储
        $cus = CustomerStorage::findFirst('customer_id=' . CUR_APP_ID . ' and md5="' . $tmp['md5'] . '"');
        if (!$cus) {
            $cus = new CustomerStorage();
            if (!$cus->save($data)) {
//                print_r($store->getMessages());
            };

            // 更新存储统计
            $type_num = $type . '_num';
            $rs = CustomerStorageCount::findFirst('customer_id=' . CUR_APP_ID);

            if (!$rs) {
                $cou = new CustomerStorageCount();

                $row[$type] = $tmp['size'];
                $row['customer_id'] = CUR_APP_ID;
                $row['total'] = $tmp['size'];
                $row[$type_num] = 1;
                $row['total_num'] = 1;

                $cou->create($row);
            } else {
                $now = $rs->$type;
                $rs->update(array(
                    $type => $now + $tmp['size'],
                    'total' => $rs->total + $tmp['size'],
                    $type_num => $rs->$type_num + 1,
                    'total_num' => $rs->total_num + 1,
                ));
            }
        }

        return $cus->id;

    }

    private function checkFile($md5)
    {
        $file = CustomerStorage::findFirst('customer_id=' . CUR_APP_ID . ' and md5="' . $md5 . '"');
        if ($file) {
            $fileUrl = rtrim(Config::getSite('upload', 'baseUrl'), '/') . '/' . rtrim($file->url, '/');
            if ($this->request->get('from') == 'xheditor') {
                echo $msg = "{'msg':'" . $fileUrl . "','err':''}"; //id参数固定不变，仅供演示，实际项目中可以是数据库ID
                exit;
            }
            return Ajax::init()->outRight('', array('url' => $fileUrl, 'id' => $file->id, 'name' => $file->name, 'ext' => $file->ext));
        }
        return false;
    }

    private function cutCover($video_file, $url, $ext)
    {
        $img_file = rtrim($video_file, '.' . $ext) . '.jpg';
        $img_url = rtrim($url, '.' . $ext) . '.jpg';
        $cmd = "/usr/bin/ffmpeg -i '" . $video_file . "' -f image2 -ss 10 -vframes 1 '" . $img_file . "'";
        exec($cmd);
        return $img_url;
    }

}