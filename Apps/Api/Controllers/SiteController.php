<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/8/14
 * Time: 11:37 AM
 */

namespace Multiple\Api\Controllers;


use Components\TplManager;
use Models\Wap\SiteFocus;
use Models\Wap\SiteNavs;
use Models\Wap\SitePage;
use Models\Wap\SiteTpl;
use Util\Ajax;

class SiteController extends ApiBase
{
    public function getNavAction()
    {
        $id = $this->request->get('id');

        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $item = SiteNavs::findFirst('customer_id=' . CUR_APP_ID . ' and id =' . $id);

        if (!$item) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        return Ajax::init()->outRight('', $item->toArray());
    }

    public function saveNavAction()
    {
        $data = $this->request->get('data');
        $id = $this->request->get('id');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id > 0) {
            $nav = SiteNavs::findFirst('customer_id=' . CUR_APP_ID . ' and id = ' . $id);
            if (!$nav) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }
            // update
            $data['modified'] = time();
            if (!$nav->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        } else {
            $nav = new SiteNavs();

            // create
            $data['customer_id'] = CUR_APP_ID;
            $data['created'] = time();
            if (!$nav->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        }

        return Ajax::init()->outRight('');
    }

    public function delNavAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = SiteNavs::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        if (!$list->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }

        // 更新二级
        $sub_list = SiteNavs::find('customer_id=' . CUR_APP_ID . ' and parent_id in (' . implode(',', $data) . ')');
        if ($sub_list) {
            foreach ($sub_list as $item) {
                $item->parent_id = 0;
                $item->update();
            }
        }

        return Ajax::init()->outRight('');
    }

    public function getFocusAction()
    {
        $id = $this->request->get('id');

        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $item = SiteFocus::findFirst('customer_id=' . CUR_APP_ID . ' and id =' . $id);

        if (!$item) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        return Ajax::init()->outRight('', $item->toArray());
    }

    public function saveFocusAction()
    {
        $data = $this->request->get('data');
        $id = $this->request->get('id');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id > 0) {
            $nav = SiteFocus::findFirst('customer_id=' . CUR_APP_ID . ' and id = ' . $id);
            if (!$nav) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }
            // update
            $data['modified'] = time();
            if (!$nav->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        } else {
            $nav = new SiteFocus();

            // create
            $data['customer_id'] = CUR_APP_ID;
            $data['created'] = time();
            if (!$nav->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        }

        return Ajax::init()->outRight('');
    }

    public function delFocusAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = SiteFocus::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        if (!$list->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }

        return Ajax::init()->outRight('');
    }

    public function setTplAction()
    {
        $id = $this->request->get('id');
        $type = $this->request->get('type');
        $data = $this->request->get('data');

        if (!(in_array($type, array('kit', 'home', 'user', 'shop', 'menu', 'customize')) && $id && $data)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $item = SiteTpl::findFirst('type="' . $type . '" and id =' . $id);

        if (!$item) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        // set Tpl
        TplManager::init(CUR_APP_ID)->setTpl($type, $data);

        return Ajax::init()->outRight('');
    }

    public function setTplModeAction()
    {
        $mode = $this->request->get('mode');

        if (!(in_array($mode, array('kit', 'single', 'customize')))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        // set Tpl
        TplManager::init(CUR_APP_ID)->setTplMode($mode);

    }

    public function savePageAction()
    {
        $data = $this->request->get('data');
        $id = $this->request->get('id');
        if (!($data && !empty($data['name']))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id > 0) {
            $nav = SitePage::findFirst('customer_id=' . CUR_APP_ID . ' and id = ' . $id);
            if (!$nav) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }
            // update
            $data['modified'] = time();
            $data['name'] = trim($data['name']);
            if (!$nav->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        } else {
            $nav = new SitePage();

            // create
            $data['customer_id'] = CUR_APP_ID;

            if ($nav->findFirst('name="' . trim($data['name']) . '" and customer_id =' . CUR_APP_ID)) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_HAS_EXISTS);
            };

            $data['created'] = time();
            if (!$nav->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        }

        return Ajax::init()->outRight('');
    }

    public function delPageAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = SitePage::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        if (!$list->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }

        return Ajax::init()->outRight('');
    }
}