<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-8-28
 * Time: 下午3:31
 */

namespace Multiple\Api\Controllers;


use Components\Product\ProductManager;
use Models\Product\Product;
use Models\Product\ProductAttr;
use Models\Product\ProductBrand;
use Models\Product\ProductCat;
use Models\Product\ProductCatFeature;
use Models\Product\ProductSpec;
use Util\Ajax;

class ProductController extends ApiBase
{
    public function getCatAction()
    {
        $id = $this->request->get('id');

        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $item = ProductCat::findFirst('customer_id=' . CUR_APP_ID . ' and id =' . $id);

        if (!$item) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        return Ajax::init()->outRight('', $item->toArray());
    }

    public function saveCatAction()
    {
        $data = $this->request->get('data');
        $id = $this->request->get('id');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id > 0) {
            $nav = ProductCat::findFirst('customer_id=' . CUR_APP_ID . ' and id = ' . $id);
            if (!$nav) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }
            // update
            $data['modified'] = time();
            unset($data['parent_id']);
            unset($data['is_parent']);
            if (!$nav->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        } else {
            $nav = new ProductCat();

            if ($data['parent_id'] > 0) {
                $parent = ProductCat::findFirst('customer_id=' . CUR_APP_ID . ' and id = ' . $data['parent_id']);
                if ($parent) {
                    $parent->update(array('is_parent' => 1));
                }
            }
            // create
            $data['customer_id'] = CUR_APP_ID;

            $data['created'] = time();
            if (!$nav->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        }

        //update cache
        ProductManager::instance()->getCategories(CUR_APP_ID, true);

        return Ajax::init()->outRight('');
    }

    public function delCatAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = ProductCat::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();

            // 更新上级
            $parent_where = 'customer_id=' . CUR_APP_ID . ' and id = ' . $item->parent_id;
            $parent = ProductCat::findFirst($parent_where);
            if ($parent) {
                if (ProductCat::count('customer_id=' . CUR_APP_ID . ' and parent_id = ' . $item->parent_id) > 0) {
                    $parent->is_parent = 1;
                } else {
                    $parent->is_parent = 0;
                }
                $parent->update();
            }
        }

        // 更新二级
        $sub_list = ProductCat::find('customer_id=' . CUR_APP_ID . ' and parent_id in (' . implode(',', $data) . ')');
        if ($sub_list) {
            foreach ($sub_list as $item) {
                $item->parent_id = 0;
                $item->is_parent = 0;
                // 更新当前状态
                if (ProductCat::count('customer_id=' . CUR_APP_ID . ' and parent_id = ' . $item->id) > 0) {
                    $item->is_parent = 1;
                }

                $item->update();
            }
        }

        // 更新分类下
        $sub_list = Product::find('customer_id=' . CUR_APP_ID . ' and cid in (' . implode(',', $data) . ')');
        if ($sub_list) {
            foreach ($sub_list as $item) {
                $item->cid = 0;
                $item->update();
            }
        }

        // 删除商品分类属性
        $feature = ProductCatFeature::find(' cid in (' . implode(',', $data) . ')');
        if ($feature) {
            foreach ($feature as $fea) {
                $fea->delete();
            }
        }

        //update cache
        ProductManager::instance()->getCategories(CUR_APP_ID, true);

        return Ajax::init()->outRight('');
    }

    public function publishAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $id_in = is_array($id) && $id ? implode(',', $id) : $id;
        if (!$id_in) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $m = Product::find(' id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->is_active = $published;
            if (!$item->update()) {
            }
        }

        return Ajax::init()->outRight('');
    }

    public function pubAttrAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $id_in = is_array($id) && $id ? implode(',', $id) : $id;
        if (!$id_in) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $this->db->begin();

        try {
            $m = ProductCatFeature::find(' id in (' . $id_in . ')');
            foreach ($m as $item) {
                $item->is_active = $published;
                if (!$item->update()) {
                    $messages = array();
                    foreach ($item->getMessages() as $message) {
                        $messages[] = (string)$message;
                    }
                    throw new \Phalcon\Exception(join(',', $messages));
                }
            }
            $this->db->commit();
        }
        catch(\Exception $e) {
            $this->db->rollback();
            $messages = $e->getMessage();
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $messages);
        }

        return Ajax::init()->outRight('');
    }

    public function saveAttrAction()
    {
        $data = $this->request->getPost('data');
        $id = $this->request->getPost('id');

        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (empty($data['cid']) || $data['cid'] == 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '请选择商品分类');
        }

        if (is_numeric($id) && $id) {
            $column = ProductCatFeature::findFirst('id=' . $id);
            if (!$column) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }
            // 检查是否拥有
            $where = 'cid=' . $data['cid'] . ' and attr_name="' . $data['attr_name'] . '" and is_sale_prop=' . $data['is_sale_prop'] . ' and id!=' . $id;
            if (ProductCatFeature::count($where)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '名称已经存在，不能修改！');
            }

            // 单词设置
            $data['attr_key'] = '';
            if (in_array($data['attr_type'], array('single', 'multi'))) {
                // word
                $words = explode('-', $data['attr_value']);
                $attr_key = ProductManager::instance()->saveWord($words);
                $data['attr_key'] = implode('-', $attr_key);
            }

            if (!$column->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED);
            };
        } else {
            $column = new ProductCatFeature();

            // 检查是否拥有
            if (ProductCatFeature::count('cid=' . $data['cid'] . ' and attr_name="' . $data['attr_name'] . '" and is_sale_prop=' . $data['is_sale_prop'])) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '名称已经存在，请不要重复添加！');
            };

            $data['created'] = time();
            $data['is_active'] = 1;

            // 单词设置
            $data['attr_key'] = '';
            if (in_array($data['attr_type'], array('single', 'multi'))) {
                // word
                $words = explode('-', $data['attr_value']);
                $attr_key = ProductManager::instance()->saveWord($words);
                $data['attr_key'] = implode('-', $attr_key);
            }

            $column->create($data);
        }

        return Ajax::init()->outRight('');
    }

    public function delAttrAction()
    {
        $data = $this->request->get('data');
        if (!($data && is_array($data))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = ProductCatFeature::find('id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    public function saveAction()
    {
        $data_base = $this->request->getPost('data_base');
        $data_attr = json_decode($this->request->getPost('data_attr'), true);
        $data_spec = json_decode($this->request->getPost('data_spec'), true);
        $product_id = intval($this->request->getPost('product_id'));

        //
        ProductManager::instance()->saveProduct($data_base, $data_attr, $data_spec, $product_id);
    }

    public function saveBrandAction()
    {
        $data = $this->request->get('data');
        $id = $this->request->get('id');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id > 0) {
            $nav = ProductBrand::findFirst('customer_id=' . CUR_APP_ID . ' and id = ' . $id);
            if (!$nav) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }
            // update
            $data['modified'] = time();
            if (!$nav->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        } else {
            $nav = new ProductBrand();

            // create
            $data['customer_id'] = CUR_APP_ID;
            $data['created'] = time();
            if (!$nav->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        }

        return Ajax::init()->outRight('');
    }

    public function delBrandAction()
    {
        $data = $this->request->get('data');
        if (!($data && is_array($data))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = ProductBrand::find('id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        return Ajax::init()->outRight('');
    }

    // 删除产品
    public function delAction()
    {
        $data = $this->request->get('data');
        if (!($data && is_array($data))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        // 删除产品
        $list = Product::find('id in (' . implode(',', $data) . ')');

        foreach ($list as $item) {
            $item->delete();
        }

        // 删除属性
        $attr = ProductAttr::find('product_id in (' . implode(',', $data) . ')');
        if ($attr) {
            foreach ($attr as $item) {
                $item->delete();
            }
        }

        // 删除规格
        $spec = ProductSpec::find('product_id in (' . implode(',', $data) . ')');
        if ($spec) {
            foreach ($spec as $item) {
                $item->delete();
            }
        }

        return Ajax::init()->outRight('');
    }
} 