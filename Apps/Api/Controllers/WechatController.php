<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-8-19
 * Time: 上午11:49
 */

namespace Multiple\Api\Controllers;

use Components\UserManager;
use Components\WeChat\MenuManager;
use Components\WeChat\MessageManager;
use Components\WeChat\RequestFactory;
use Components\WeChat\ResourceManager;
use Models\Article\SiteArticles;
use Models\Customer\CustomerOpenInfo;
use Models\Product\Product;
use Models\WeChat\CustomerMenus;
use Models\WeChat\MessageSettingKeywords;
use Models\WeChat\MessageSettingMass;
use Models\WeChat\MessageSettings;
use Models\WeChat\MessageHistory;
use Util\Ajax;
use Util\EasyEncrypt;
use Util\Uri;


class WechatController extends ApiBase
{

    protected $_check_login = true;
    public function initialize() {
        parent::initialize();
        $this->uri = new Uri();
    }
    public function menuAddAction()
    {
        $this->view->disable();

        $data = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if (!$this->request->isAjax()) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "只支持AJAX请求");
        }
        $mid = $this->request->getPost('mid', 'int');
        $name = $this->request->getPost('name', 'string');
        $sort = $this->request->getPost("sort", 'int', 0);
        if (empty($name)) {
            $data['code'] = 1;
            $data['message'][] = "菜单名称必须填写。";
        }
        $pid = $this->request->getPost('pid', 'int');
        if (empty($mid)) {
            if (empty($pid)) {
                $pid = 0;
                $topMenuCount = CustomerMenus::count(array("customer_id=" . CUR_APP_ID . " AND pid='0' AND platform='" . MessageManager::PLATFORM_TYPE_WEIXIN . "'"));
                if ($topMenuCount >= 3) {
                    $data['code'] = 1;
                    $data['message'][] = '您已经添加了3个顶级菜单，不能再添加了！';
                }
            } else {
                $topMenuCount = CustomerMenus::count(array("customer_id=" . CUR_APP_ID . " AND pid='{$pid}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIXIN . "'"));
                if ($topMenuCount >= 5) {
                    $data['code'] = 1;
                    $data['message'][] = '您已经添加了5个子菜单，不能再添加了！';
                }
            }
        }
        if ($data['code'] > 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $data['message']);
        }
        $type = strtoupper($this->request->getPost('type', 'string'));
        if (empty($type)) {
            $data['code'] = 1;
            $data['message'][] = "菜单类型必须指定";
        }
        $target_value = $this->request->getPost('value', 'string');
        $defaultLink = $this->request->getPost('default_link', 'string');

        $eventKey = '';
        $messageType = '';
        if ($type == MenuManager::TYPE_VIEW) {
            if (empty($target_value)) {
                $data['code'] = 1;
                $data['message'][] = "没有指定菜单到达的目标链接。";
            } else if (!filter_var($target_value, FILTER_VALIDATE_URL)) {
                $data['code'] = 1;
                $data['message'][] = "目标链接格式不正确。";
            }
        } else if ($type == MenuManager::TYPE_CLICK) {
            $messageType = $this->request->getPost('messageType', 'string');
            if (empty($messageType)) {
                $messageType = ResourceManager::MESSAGE_TYPE_TEXT;
            }
            $eventKey = sha1($this->customer_id . time(). "event_key" . $type . $messageType);
            if (empty($target_value)) {
                $data['code'] = 1;
                $data['message'][] = "没有设置菜单的响应信息。";
            }
            if ($defaultLink && strlen($defaultLink) > 0 && !filter_var($defaultLink, FILTER_VALIDATE_URL)) {
                $data['code'] = 1;
                $data['message'][] = "默认链接的格式不正确。";
            }
        }
        if (empty($sort)) {
            $sort = 0;
        }

        if ($data['code'] > 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $data['message']);
        }

        if ($mid > 0) {
            $menuItem = CustomerMenus::findFirst("id='{$mid}'");
            if ($menuItem) {
                if (!$menuItem->update(array(
                    'name' => $name,
                    'pid' => $pid,
                    'type' => $type,
                    'platform' => MessageManager::PLATFORM_TYPE_WEIXIN,
                    'target_value' => $target_value,
                    'default_link' => $defaultLink,
                    'sort' => $sort,
                    'message_type' => $messageType,
                    'key' => $eventKey
                ))
                ) {
                    $data['code'] = 1;
                    foreach ($menuItem->getMessages() as $message) {
                        $data['message'][] = (string)$message;
                    }
                }
            }
        } else {
            $menuItem = new CustomerMenus();
            $menuItem->customer_id = $this->customer->id;
            $menuItem->name = $name;
            $menuItem->pid = $pid;
            $menuItem->type = $type;
            $menuItem->target_value = $target_value;
            $menuItem->sort = $sort;
            $menuItem->created = time();
            $menuItem->message_type = $messageType;
            $menuItem->key = $eventKey;
            $menuItem->platform = MessageManager::PLATFORM_TYPE_WEIXIN;
            if (!$menuItem->save()) {
                $data['code'] = 1;
                foreach ($menuItem->getMessages() as $message) {
                    $data['message'][] = (string)$message;
                }
            }
        }

        if ($data['code'] == 0 && $pid > 0) {
            $parentMenu = CustomerMenus::findFirst("id='{$pid}' AND customer_id=" . CUR_APP_ID . "");
            if ($parentMenu) {
                $parentMenu->update(array(
                    'type' => MenuManager::TYPE_TOP_BAR
                ));
            }
        }
        //Automatically sync to wechat server
        MenuManager::instance()->syncToWeChat(CUR_APP_ID, $this->customer_wechat->app_id, $this->customer_wechat->app_secret);

        Ajax::init()->outRight();
    }

    public function menuRemoveAction()
    {
        $this->view->disable();
        if (!$this->request->isAjax()) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "只支持AJAX请求");
        }

        $mid = $this->request->getPost('mid', 'int');

        if (!$mid || $mid <= 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "没有指定要删除的菜单");
        } else {
            $data = array(
                'code' => 0,
                'message' => '',
                'result' => ''
            );
            $menuItem = CustomerMenus::findFirst("id='{$mid}' AND customer_id=" . CUR_APP_ID . "");
            if (!$menuItem->delete()) {
                $data['code'] = 1;
                foreach ($menuItem->getMessages() as $message) {
                    $data['message'][] = $message;
                }
            } else {
                //Automatically sync to WeChat server
                MenuManager::instance()->syncToWeChat(CUR_APP_ID, $this->customer_wechat->app_id, $this->customer_wechat->app_secret);
            }
            if($data['code'] > 0) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $data['message']);
            }
            else {
                return Ajax::init()->outRight();
            }
        }
    }

    public function messageReplyAction()
    {
        $this->view->disable();

        $msgId = $this->request->getPost('msgId', 'string');
        $msgType = $this->request->getPost('msg_type', 'string');
        $msg = $this->request->getPost('message');
        $defaultLink = $this->request->get('default_link');

        $messageHistory = MessageHistory::findFirst($msgId);

        if (!$messageHistory) {
            $errors = "服务器没有找到该消息";
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $errors);
        } else {
            $toUser = $messageHistory->from;
            if($msgType == ResourceManager::MESSAGE_TYPE_NEWS) {
                $arcIds = explode(',', $msg);
                if($arcIds) {
                    $msg = array();
                    $articlesNum = 0;
                    foreach($arcIds as $k => $id) {
                        if ($articlesNum > 10) {
                            break;
                        }
                        $arc = SiteArticles::findFirst($id);
                        if(!$arc) {
                            continue;
                        }

                        if($k == 0 && empty($articlesNum) && is_string($defaultLink) && strlen($defaultLink) > 0) {
                            $articleUrl = $defaultLink;
                        }
                        else {
                            $articleUrl = $this->uri->appUrl(). '/article/detail/' . EasyEncrypt::encode($arc->id);
                        }
                        if(strpos($articleUrl, '?') === false) {
                            $articleUrl .=  '?from_user=' . EasyEncrypt::encode($toUser) . '&platform=' . MessageManager::PLATFORM_TYPE_WEIXIN;
                        }
                        else {
                            $articleUrl .= '&from_user=' . EasyEncrypt::encode($toUser). '&platform=' . MessageManager::PLATFORM_TYPE_WEIXIN;
                        }
                        $msg[] = array(
                            'title' => $arc->title,
                            'desc' => $arc->meta_desc,
                            'url' => $articleUrl,
                            'pic_url' => $arc->cover
                        );
                    }
                }
                else {
                    return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "没有找到对应的图文信息");
                }
            }
            else if($msgType == ResourceManager::MESSAGE_TYPE_PRODUCT) {
                $arcIds = explode(',', $msg);
                if($arcIds) {
                    $msg = array();
                    $articlesNum = 0;
                    foreach($arcIds as $k =>$id) {
                        $arc = Product::findFirst($id);
                        if(!$arc) {
                            continue;
                        }
                        if($k == 0 && empty($articlesNum) && is_string($defaultLink) && strlen($defaultLink) > 0) {
                            $productUrl = $defaultLink;
                        }
                        else {
                            $productUrl = $this->uri->appUrl(). '/shop/item/' . EasyEncrypt::encode($arc->id);
                        }
                        if(strpos($productUrl, '?') === false) {
                            $productUrl .=  '?from_user=' . EasyEncrypt::encode($toUser). '&platform=' . MessageManager::PLATFORM_TYPE_WEIXIN;
                        }
                        else {
                            $productUrl .= '&from_user=' . EasyEncrypt::encode($toUser). '&platform=' . MessageManager::PLATFORM_TYPE_WEIXIN;
                        }

                        $msg[] = array(
                            'title' => $arc->name,
                            'desc' => $arc->name,
                            'url' => $productUrl,
                            'pic_url' => $arc->thumb
                        );
                    }
                }
                else {
                    return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "没有找到对应商品信息");
                }
            }

            $request = RequestFactory::create("SendMessage", $this->customer_id, $this->customer_wechat->app_id, $this->customer_wechat->app_secret);
            if($msgType != ResourceManager::MESSAGE_TYPE_TEXT) {
                $msgType = ResourceManager::MESSAGE_TYPE_NEWS;
            }
            $request->set('msgType', $msgType);
            $request->set('toUser', $toUser);
            $request->set('message', $msg);

            $request->run();
            if($request->isFailed()) {
                $errors = $request->getErrorMessage();
                $this->di->get('wechatLogger')->error("send normal custom message failed: " . $errors);
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $errors);
            }
        }
        if(!$messageHistory->update(array(
            'is_replied' => 1,
            'reply' => json_encode(array(
                'type' => $msgType,
                'message' => $msg
            ))
        ))) {
            $errors = '';
            foreach ($messageHistory->getMessages() as $err) {
                $errors .= (string)$err;
            }
            $this->di->get('wechatLogger')->error("save reply content for meesage failed: " . $errors);
        }
        return Ajax::init()->outRight('操作成功', '');
    }

    public function messageRemoveAction()
    {
        $this->view->disable();
        $msgId = $this->request->get('msgId', 'string');
        $message = MessageHistory::findFirst($msgId);

        $result = 1;
        $errors = '';

        if (!$message) {
            $result = 0;
            $errors = "没有找到对应的数据";
        } else {
            if (!$message->delete()) {
                $result = 0;
                foreach ($message->getMessages() as $err) {
                    $errors .= (string)$err;
                }
                $this->di->get('errorLogger')->error("mongo message history delete failed, mongoId is " . $msgId . ". error message :" . $errors);
            }
        }

        if($result == 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $errors);
        }
        else {
            return Ajax::init()->outRight();
        }
    }

    public function saveAction()
    {
        $this->view->disable();
        $result = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if ($this->request->isPost()) {
            $respondType = $this->request->getPost('respondType', "string");
            $messageType = $this->request->getPost('messageType', "string");
            $message = $this->request->getPost('value');
            $defaultLink = $this->request->getPost('link', 'string');

            $messageSetting = MessageSettings::findFirst("customer_id=" . CUR_APP_ID . " AND event_type='{$respondType}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIXIN . "'");
            if (!$messageSetting) {
                $messageSetting = new MessageSettings();
                $messageSetting->customer_id = $this->customer->id;
                $messageSetting->platform = MessageManager::PLATFORM_TYPE_WEIXIN;
                $messageSetting->event_type = $respondType;
                $messageSetting->created = time();
            }
            $messageSetting->message_type = $messageType;
            $messageSetting->value = $message;
            $messageSetting->default_link = $defaultLink;

            if ($messageSetting->save() == false) {
                $result['code'] = 1;
                $result['message'] = $messageSetting->getMessages();
            }
            MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIXIN, $this->customer->id, $respondType, true);
        }
        if($result['code'] > 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $result['message']);
        }
        else {
            return Ajax::init()->outRight();
        }
    }

    public function saveKeywordAction()
    {
        $this->view->disable();
        $result = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if ($this->request->isPost()) {
            $kid = $this->request->getPost('kid', 'int');
            $ruleName = $this->request->getPost('ruleName', "string");
            $keywords = $this->request->getPost('keywords');
            $messageType = $this->request->getPost('messageType', 'string');
            $messageData = $this->request->getPost('messageData', "string");
            $messageLink = $this->request->getPost('messageLink', 'string');

            $count = MessageSettingMass::count("customer_id=" . CUR_APP_ID . "");

            if ($count > 10) {
                $result['code'] = 1;
                $result['message'] = "对不起，您已经设置了10个关键字规则，已经达到系统限制上限，您可以删除不常用的规则，然后再添加新规则";
            } else {
                if ($kid > 0) {
                    $messageSetting = MessageSettingKeywords::findFirst("customer_id=" . CUR_APP_ID . " AND id='{$kid}'");
                } else {
                    $messageSetting = new MessageSettingKeywords();
                    $messageSetting->customer_id = $this->customer->id;
                    $messageSetting->platform = MessageManager::PLATFORM_TYPE_WEIXIN;
                }
                $messageSetting->name = $ruleName;
                $messageSetting->keywords = json_encode($keywords);
                $messageSetting->message_type = $messageType;
                $messageSetting->respond_message = $messageData;
                $messageSetting->default_link = $messageLink;
                if ($messageSetting->save() == false) {
                    $result['code'] = 1;
                    $result['message'] = $errorMessage = $messageSetting->getMessages();
                } else {
                    $result['result'] = $messageSetting->id;
                }
                MessageManager::instance()->getMessageSettings($this->customer->id, MessageManager::EVENT_TYPE_KEYWORD, true);
            }
        }
        if($result['code'] > 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $result['message']);
        }
        else {
            return Ajax::init()->outRight('', $result['result']);
        }
    }

    public function removeKeywordAction()
    {
        $this->view->disable();
        $result = array(
            'code' => 0,
            'message' => '',
            'result' => ''
        );
        if ($this->request->isPost()) {
            $kid = $this->request->getPost('kid', 'int');
            if (empty($kid)) {
                $result['code'] = 1;
                $result['message'] = "没有指定要删除的关键词";
            } else {
                $messageSetting = MessageSettingKeywords::findFirst("customer_id=" . CUR_APP_ID . " AND id='{$kid}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIXIN . "'");
                if (!$messageSetting) {
                    $result['code'] = 1;
                    $result['message'] = "找不到指定的关键词规则";
                } else {
                    if ($messageSetting->delete() == false) {
                        $result['code'] = 1;
                        $result['message'] = $errorMessage = $messageSetting->getMessages();
                    } else {
                        $result['result'] = $messageSetting->id;
                    }
                }
            }
        }
        if($result['code'] > 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $result['message']);
        }
        else {
            return Ajax::init()->outRight();
        }
    }

    public function massAction()
    {
        $this->view->disable();
        $monthStartTime = strtotime('last month');
        $thisMonthMassCount = MessageSettingMass::count(array("customer_id=" . CUR_APP_ID . " AND time >= '{$monthStartTime}'"));
        $this->view->setVar('monthCount', $thisMonthMassCount);
//        $available = 4 - $thisMonthMassCount;
        if ($this->request->isAjax() || $this->request->isPost()) {
            $this->view->disable();
            /*if ($thisMonthMassCount > 4) {
                return $this->response->setJsonContent(array(
                    'code' => 1,
                    'message' => "您本月已经发送了4条群发，请等到下月再发吧",
                    'result' => ''
                ))->send();
            }*/
            $group = $this->request->getPost('group', "int", false);
            $gender = $this->request->getPost("gender", array('striptags', 'string'), false);
            $messageType = $this->request->getPost('messageType', 'striptags', "text");
            $message = $this->request->getPost('messages');
            $defaultLink = $this->request->getPost('default_link', 'string');
            $code = 0;
            $messages = '';
            try {
                $massMessage = new MessageSettingMass();
                $massMessage->customer_id = CUR_APP_ID;
                $massMessage->message_type = $messageType;
                $massMessage->message = $message;
                $massMessage->default_link = $defaultLink;
                $massMessage->group = $group;
                $massMessage->gender = $gender;
                $massMessage->is_send = 0;
                $massMessage->time = time();
                if ($massMessage->save()) {
                    //加入到消息队列
                    $beanstalk = $this->di->get('beanstalk');
                    $jobId = $beanstalk->put(array("mass-message-queue", array(
                        'customer_id' => CUR_APP_ID,
                        'group' => $group,
                        'gender' => $gender,
                        'messageType' => $messageType,
                        'messages' => $message,
                        'platform' => MessageManager::PLATFORM_TYPE_WEIXIN
                    )));
                    if ($jobId) {
                        $massMessage->update(array('is_send' => 1, 'queue_job_id' => $jobId));
                    }
                } else {
                    $messages = [];
                    foreach ($massMessage->getMessages() as $message) {
                        $messages[] = (string)$message;
                    }
                    throw new \Phalcon\Exception(join(',', $messages));
                }
            } catch (\Exception $e) {
                $code = 1;
                $messages = $e->getMessage();
            }

            if($code > 0) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $message);
            }
            else {
                return Ajax::init()->outRight();
            }
        }
    }


    public function syncAction()
    {
        $this->view->disable();
        set_time_limit(0);
//        if ($this->request->isPost()) {
        $cm = CustomerOpenInfo::findFirst('customer_id=' . CUR_APP_ID);

        // 获取用户及组
        $res = MenuManager::instance()->syncFromWeChat(CUR_APP_ID, $cm->app_id, $cm->app_secret);
        $this->out($res, '6. 同步菜单');

        $res = UserManager::instance()->syncGroupFromWeChat(CUR_APP_ID, $cm->app_id, $cm->app_secret);
        $this->out($res, '7. 同步组');

        $res = UserManager::instance()->syncUserFromWechat(CUR_APP_ID, $cm->app_id, $cm->app_secret);
        $this->out($res, '8. 同步用户');
//        }
    }


    private function out($status, $msg)
    {
        if ($status) {
            echo $msg . '成功</br>';
        } else {
            echo '！！' . $msg . '失败</br>';
        }
    }

    public function syncToWeChatAction() {
        $this->view->disable();
        $code = 0;
        $message = '';
        try {
            if(!MenuManager::instance()->syncToWeChat(CUR_APP_ID, $this->customer_wechat->app_id, $this->customer_wechat->app_secret)) {
                throw new \Phalcon\Exception("同步菜单失败，请确认：1，是否绑定微信成功；2，菜单设置是否符合右侧要求；3，顶级菜单为[弹出菜单]时，必须有子菜单方可同步成功！");
            }
        }
        catch(\Phalcon\Exception $e) {
            $code = 1;
            $message = $e->getMessage();
        }

        if($code > 0) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $message);
        }
        else {
            return Ajax::init()->outRight();
        }
    }
}