<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/8/14
 * Time: 11:37 AM
 */

namespace Multiple\Api\Controllers;


use Models\Article\SiteArticles;
use Models\Article\SiteArticlesCat;
use Phalcon\Tag;
use Util\Ajax;
use Util\EasyEncrypt;

class ArticleController extends ApiBase
{
    public function getCatAction()
    {
        $id = $this->request->get('id');

        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $item = SiteArticlesCat::findFirst('customer_id=' . CUR_APP_ID . ' and id =' . $id);

        if (!$item) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        return Ajax::init()->outRight('', $item->toArray());
    }

    public function saveCatAction()
    {
        $data = $this->request->get('data');
        $id = $this->request->get('id');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (is_numeric($id) && $id > 0) {
            $nav = SiteArticlesCat::findFirst('customer_id=' . CUR_APP_ID . ' and id = ' . $id);
            if (!$nav) {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
            }
            // update
            $data['modified'] = time();
            if (!$nav->update($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        } else {
            $nav = new SiteArticlesCat();

            // create
            $data['customer_id'] = CUR_APP_ID;
            $data['created'] = time();
            if (!$nav->create($data)) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }
        }

        return Ajax::init()->outRight('');
    }

    /**
     * get list
     */
    public function getAction()
    {
        $page = $this->request->get('p');
        $cid = $this->request->get('cid');
        $needCover = $this->request->get('needCover');

        $where = ' published=1 and customer_id=' . CUR_APP_ID;
        if (is_numeric($cid) && $cid >= 0) {
            $where .= ' and cid = ' . $cid;
        }

        if ($needCover) {
            $where .= ' and cover != ""';
        }

        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 6;

        $count = SiteArticles::count($where);
        $res = SiteArticles::find(array($where, "order" => "created desc", 'columns' => 'id,title,created,views,cover', "limit" => $curpage * $limit . "," . $limit));
        $list = $res ? $res->toArray() : null;
        if ($list) {
            foreach ($list as &$v) {
                $v['date'] = date('Y-m-d', $v['created']);
                $v['url'] = $this->uri->appUrl('article/detail/' . EasyEncrypt::encode($v['id']));
            }
        }

        return Ajax::init()->outRight('', array('count' => $count, 'list' => $list, 'limit' => $limit));
    }

    public function delCatAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = SiteArticlesCat::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        if (!$list->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }

        // 更新二级
        $sub_list = SiteArticlesCat::find('customer_id=' . CUR_APP_ID . ' and parent_id in (' . implode(',', $data) . ')');
        if ($sub_list) {
            foreach ($sub_list as $item) {
                $item->parent_id = 0;
                $item->update();
            }
        }

        // 更新分类下文章
        $sub_list = SiteArticles::find('customer_id=' . CUR_APP_ID . ' and cid in (' . implode(',', $data) . ')');
        if ($sub_list) {
            foreach ($sub_list as $item) {
                $item->cid = 0;
                $item->update();
            }
        }

        return Ajax::init()->outRight('');
    }

    public function delAction()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $list = SiteArticles::find('customer_id=' . CUR_APP_ID . ' and id in (' . implode(',', $data) . ')');

        if (!$list->delete()) {
            return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
        }

        return Ajax::init()->outRight('');
    }


    public function publishAction()
    {
        $id = $this->request->getPost('id');
        $published = intval($this->request->getPost('published'));
        if (!$id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $id_in = is_array($id) && $id ? implode(',', $id) : $id;
        if (!$id_in) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $m = SiteArticles::find("customer_id = '" . CUR_APP_ID . "' and id in (" . $id_in . ')');
        foreach ($m as $item) {
            $item->published = $published;
            $item->modified = time();
            $item->update();
        }

        return Ajax::init()->outRight('');
    }


    public function mvPostAction()
    {
        $cid = intval($this->request->getPost('cid'));
        $data = $this->request->getPost('data');
        if (!$cid && $data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        $id_in = is_array($data) && $data ? implode(',', $data) : $data;

        if (!$id_in) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $m = SiteArticles::find('customer_id = ' . CUR_APP_ID . ' and id in (' . $id_in . ')');
        foreach ($m as $item) {
            $item->cid = $cid;
            $item->update();
        }

        return Ajax::init()->outRight('');
    }
}