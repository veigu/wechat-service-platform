<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 4/8/14
 * Time: 11:37 AM
 */

namespace Multiple\Api\Controllers;


use Components\Product\FreightManager;
use Components\Product\ProductManager;
use Models\Product\Product;
use Util\EasyEncrypt;
use Models\Article\SiteArticles;
use Phalcon\Tag;
use Util\Ajax;

class ShopController extends ApiBase
{
    /**
     * get list
     */
    public function getItemAction()
    {
        $page = $this->request->get('p');
        $cid = $this->request->get('cid');
        $where = ' is_active=1 and customer_id=' . CUR_APP_ID . ' ';
        if ($cid) {
            $where .= ' and cid = ' . $cid . ' ';
        }

        $curpage = $page <= 0 ? 0 : $page - 1;
        $limit = 6;

        $count =Product::count($where);
        $res =Product::find(array($where, "order" => "created desc", 'columns' => 'id,name,sell_price,thumb,created', "limit" => $curpage * $limit . "," . $limit));
        $list = $res ? $res->toArray() : null;
        if ($list) {
            foreach ($list as &$v) {
                $v['date'] = date('Y-m-d', $v['created']);
                $v['url'] = $this->uri->appUrl('shop/item/' . EasyEncrypt::encode($v['id']));
            }
        }

        return Ajax::init()->outRight('', array('count' => $count, 'list' => $list, 'limit' => $limit));
    }
    public function  freightFreshAction()
    {
        $freight= FreightManager::init()->getFreight();
        foreach($freight as $k=>$val)
        {
            $freight[$k]['data-item']=base64_encode(json_encode($val));
        }
        return Ajax::init()->outRight('', $freight);
    }
    public function  brandFreshAction()
    {
        $brand = ProductManager::instance()->getAllBrand(CUR_APP_ID);
        return Ajax::init()->outRight('', $brand);
    }


}