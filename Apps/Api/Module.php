<?php

namespace Multiple\Api;

use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;

class Module
{

    public function registerAutoloaders()
    {

        $loader = new Loader();

        $loader->registerNamespaces(array(
            'Multiple\Api\Controllers' => 'Apps/Api/Controllers/',
            'Multiple\Api\ModuleApi' => 'Apps/Api/ModuleApi/',
        ));

        $loader->register();
    }

    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    public function registerServices($di)
    {

        //Registering a dispatcher
        $di->set('dispatcher', function () {

            $dispatcher = new Dispatcher();

            $dispatcher->setDefaultNamespace("Multiple\\Api\\Controllers\\");
            return $dispatcher;
        });

        //Registering the view component
        $di->set('view', function () {
            $view = new View();
            $view->setViewsDir('Apps/Api/Views/');
            return $view;
        });

    }

}