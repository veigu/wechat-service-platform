<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-6-27
 * Time: 上午10:51
 */

namespace Components\WeiBo;


use Components\UserManager;
use Components\WeChat\MessageManager;
use Components\WeChat\ResourceManager;
use Models\Customer\CustomerOpenInfo;
use Models\User\UserForCustomers;
use Models\User\UserLocations;
use Models\User\Users;
use Models\User\UsersWeibo;
use Models\WeChat\CustomerMenus;
use Models\WeiBo\EventHistory;
use Models\WeiBo\MessageHistory;
use Phalcon\Mvc\User\Plugin;

class FansService extends Plugin
{

    private $appKey = '';

    private $appSecret = "";

    private $accessToken = '';

    private $data = array();

    private $customer = 0;

    private $respondMessageType = ResourceManager::MESSAGE_TYPE_TEXT;

    private $respondMessage = "";

    private $respondDefaultLink = '';

    /**
     * @var \Phalcon\Logger\Adapter\File
     */
    private $logger = null;

    private $message_type = ResourceManager::MESSAGE_TYPE_TEXT;

    public function __construct($appKey, $appSecret, $accessToken)
    {
        $this->appKey = $appKey;
        $this->appSecret = $appSecret;
        $this->accessToken = $accessToken;
        $this->logger = $this->di->get("weiboLogger");
        $this->customer = CUR_APP_ID;
    }

    public function checkSign()
    {
        $signature = trim($this->request->get('signature'));
        $timestamp = trim($this->request->get('timestamp'));
        $nonce = trim($this->request->get('nonce'));
        $params = array($this->appSecret, $timestamp, $nonce);
        sort($params, SORT_STRING);
        $signatureNew = sha1(join('', $params));
        $this->logger->info("received query data:" . json_encode($this->request->getQuery()));
        $this->logger->info("received signature data:{ secret: " . $this->appSecret . " , sign:" . $signature . ", time: " . $timestamp . ", nonce: " . $nonce . ", new sign: " . $signatureNew . "}");
        if ($signature != $signatureNew) {
            return false;
        } else {
            return true;
        }
    }

    public function handle()
    {
        $returnStr = $this->request->get("echostr", 'string', '');
        if ($this->checkSign()) {
            $postData = $this->request->getRawBody();
            $this->logger->info('received post data:' . $postData);
            //首次接入
            if (empty($postData)) {
                $openInfo = CustomerOpenInfo::findFirst("customer_id='" . CUR_APP_ID . "' AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO . "'");
                if ($openInfo) {
                    $uid = UserManager::instance()->getWeiboUid(CUR_APP_ID);
                    $openInfo->update(array('is_binded' => 1, 'original_id' => $uid));
                } else {
                    $returnStr = '';
                }

                return $returnStr;
            } else {
                $data = json_decode($postData, true);
                if (!$data) {
                    return "";
                }
                $this->data = $data;

                UserManager::instance()->addWeiboUser($this->customer, $this->data["receiver_id"], $this->data["sender_id"],  $this->appKey, $this->appSecret, $this->accessToken);

                $this->message_type = $data['type'];
                if ($this->message_type == ResourceManager::MESSAGE_TYPE_EVENT) {
                    $eventHistory = EventHistory::findFirst(array("customer_id" => $this->customer, 'created_at' => $this->data['created_at'], 'from' => $this->data['sender_id']));
                    if(!$eventHistory) {
                        $eventHistory = new EventHistory();
                        $eventHistory->customer_id = $this->customer;
                        $eventHistory->from = (string)$this->data['sender_id'];
                        $eventHistory->type = ResourceManager::MESSAGE_TYPE_EVENT;
                        $eventHistory->received = time();
                        $eventHistory->created_at = $this->data['created_at'];
                        $eventHistory->message = (string)$this->data['text'];
                        $eventHistory->event = (string)$this->data['data']['subtype'];

                        $eventHistory->extra_data = [];
                        switch($eventHistory->event) {
                            case MessageManager::EVENT_TYPE_SCAN_FOLLOW: {
                                $eventHistory->extra_data = json_decode(array(
                                    'key' => (string)$this->data['key'],
                                    'ticket' => (string)$this->data['ticket']
                                ));
                                break;
                            }
                            case MessageManager::EVENT_TYPE_MENTION:
                            case MessageManager::EVENT_TYPE_VIEW:
                            case MessageManager::EVENT_TYPE_CLICK: {
                                $eventHistory->extra_data = json_decode(['key' => (string)$this->data['key']]);
                                break;
                            }
                        }

                        if(!$eventHistory->save()) {
                            $errorMsg = [];
                            foreach($eventHistory->getMessages() as $msg) {
                                $errorMsg[] = (string)$msg;
                            }
                            $this->di->get('errorLogger')->debug("save weibo event log failed:" . join(",", $errorMsg));
                        }
                    }
                    return $this->handleEvent();
                } else if ($this->message_type == ResourceManager::MESSAGE_TYPE_POSITION) {
                    return $this->handlePosition();
                } else {
                    $messageHistory = MessageHistory::findFirst(array("customer_id" => $this->customer, 'msg_id' => $this->data['sender_id']));
                    if(!$messageHistory) {
                        $messageHistory = new MessageHistory();
                        $messageHistory->customer_id = $this->customer;
                        $messageHistory->from = (string)$this->data['sender_id'];
                        $messageHistory->type = (string)$this->data['type'];
                        $messageHistory->created = (string)$this->data['created_at'];
                        $messageHistory->received = time();
                        $messageHistory->message = (string)$this->data['text'];
                        $messageHistory->is_replied = 0;
                        $messageHistory->replied = 0;
                        $messageHistory->reply = "";
                        $messageHistory->user_info = json_decode(array(
                            'nickname' => (string)$this->user_info->nickname,
                            'sex' => (string)$this->user_info->sex,
                            'avatar' => (string)$this->user_info->headimgurl
                        ));


                        switch($messageHistory->type) {
                            case ResourceManager::MESSAGE_TYPE_VOICE:
                            case ResourceManager::MESSAGE_TYPE_IMAGE: {
                                $messageHistory->extra_data = json_decode(['vfid' => (string)$this->data['vfid'], 'tovfid' => (string)$this->data['tovfid']]);
                                break;
                            }
                            case ResourceManager::MESSAGE_TYPE_LOCATION: {
                                $messageHistory->extra_data = json_decode(['x' => (string)$this->data['longitude'], 'y' => (string)$this->data['latitude']]);
                                break;
                            }
                        }
                        if(!$messageHistory->save()) {
                            $errorMsg = [];
                            foreach($messageHistory->getMessages() as $msg) {
                                $errorMsg[] = (string)$msg;
                            }
                            $this->di->get('errorLogger')->debug("save weibo event log failed:" . join(",", $errorMsg));
                        }
                    }
                    return $this->handleMessage();
                }
            }
        } else {
            $this->logger->info('signature verified failed');
            return '';
        }

    }

    public function handleMessage()
    {
        $msgType = $this->data['type'];

        $message = $this->data['text'];
        //keyword respond
        if ($msgType == ResourceManager::MESSAGE_TYPE_TEXT) {
            $respondSettings = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer, MessageManager::EVENT_TYPE_KEYWORD);
            if ($respondSettings) {
                $this->logger->info("keyword response rules:" . json_encode($respondSettings));
                $hasMatched = false;
                foreach ($respondSettings as $setting) {
                    $keywordSetting = json_decode($setting["keywords"], true);
                    foreach ($keywordSetting as $keyword) {
                        $this->logger->info("keyword：" . $keyword['keyword']);
                        if ($keyword['full_text'] > 0 && trim($message) == trim($keyword['keyword'])) {
                            $hasMatched = true;
                            $this->respondMessage = $setting['message'];
                            $this->respondMessageType = $setting['message_type'];
                            break;
                        } else if (strpos($message, $keyword['keyword']) !== false) {
                            $hasMatched = true;
                            $this->respondMessage = $setting['message'];
                            $this->respondMessageType = $setting['message_type'];
                            break;
                        }
                    }
                }
                if ($hasMatched) {
                    $this->logger->info("has matched one keyword rule");
                    $respondMessage = $this->responseMessage();
                    return $respondMessage;
                }
            }
        }

        //normal respond
        $respondSetting = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer, MessageManager::EVENT_TYPE_NORMAL);
        if ($respondSetting) {
            $this->logger->info(json_encode($respondSetting));
            if (strlen(trim($respondSetting['values'])) > 0) {
                $this->respondMessage = $respondSetting['values'];
                $this->respondMessageType = $respondSetting['message_type'];
                $respondMessage = $this->responseMessage();
                return $respondMessage;
            }
        }

        return "";
    }

    public function handleEvent()
    {
        $subType = $this->data['data']['subtype'];
        switch ($subType) {
            case MessageManager::EVENT_TYPE_FOLLOW:{break;}
            case MessageManager::EVENT_TYPE_SUBSCRIBE:
            {
                $respondMessage = '';
                $this->logger->info("----------log------------------");
                $respondSetting = MessageManager::instance()->getMessageSettings(MessageManager::PLATFORM_TYPE_WEIBO, $this->customer, MessageManager::EVENT_TYPE_SUBSCRIBE);
                $this->logger->info("设置结果：" . json_encode($respondSetting));
                if ($respondSetting) {
                    $this->respondMessage = $respondSetting['values'];
                    $this->respondMessageType = $respondSetting['message_type'];
                    $this->respondDefaultLink = $respondSetting['default_link'];
                    $respondMessage = $this->responseMessage();
                    return $respondMessage;
                }
                //log user info into system
                //log user into customer user list
                $this->db->begin();
                $this->logger->debug("开始处理用户订阅信息");
                $message = '';
                $errorMessages = [];
                $openId = (string)$this->data['sender_id'];

                $this->logger->info('openid:' . $openId);
                return $respondMessage;
                break;
            }
            case MessageManager::EVENT_TYPE_UN_SUBSCRIBE:
            case MessageManager::EVENT_TYPE_UN_FOLLOW:
            {
                //remove user from customer user list
                $this->logger->info("开始处理unsubscribe事件");
                $user = UserForCustomers::findFirst("open_id='{$this->data['sender_id']}' AND customer_id='{$this->customer}'");
                if ($user && $user->subscribe) {
                    if (!$user->update(array('subscribe' => 0, 'subscribe_time' => 0))) {
                        $messages = [];
                        foreach ($user->getMessages() as $message) {
                            $messages[] = (string)$message;
                        }
                        $this->logger->debug(join('\n', $messages));
                    }
                }
                break;
            }
            case MessageManager::EVENT_TYPE_MENTION:
            {
                break;
            }
            case MessageManager::EVENT_TYPE_CLICK:
            {
                //user click a menu item to get pre-set messages
                $eventKey = $this->data['EventKey'];
                $menuItem = CustomerMenus::findFirst("key='{$eventKey}'");
                if ($menuItem) {
                    $messageType = $menuItem->message_type;
                    $target_value = $menuItem->target_value;
                    $defaultLink = $menuItem->default_link;
                    $this->respondMessageType = $messageType;
                    $this->respondMessage = ResourceManager::instance()->getMessage($messageType, $target_value);
                    $this->respondDefaultLink = $defaultLink;
                    return $this->responseMessage();
                }
                break;
            }
        }
        return false;
    }

    public function handlePosition()
    {
        $location = new UserLocations();
        $location->customer_id = CUR_APP_ID;
        $location->platform = MessageManager::PLATFORM_TYPE_WEIBO;
        $location->open_id = $this->data['sender_id'];
        $location->latitude = $this->data['Latitude'];
        $location->longitude = $this->data['Longitude'];
        $location->precision = $this->data['Precision'];
        $location->created = $this->data['CreateTime'];
        if (!$location->save()) {
            $messages = [];
            foreach ($location->getMessages() as $message) {
                $messages[] = (string)$message;
            }
            $this->logger->debug("上报位置信息储存失败！" . join(',', $messages));
        }
        return '';
    }

    public function responseMessage($type = null, $message = null, $defaultLink = null)
    {
        if (!is_null($type)) {
            $this->respondMessageType = $type;
        }
        if (!is_null($message)) {
            $this->respondMessage = $message;
        }

        if (!is_null($defaultLink)) {
            $this->respondDefaultLink = $defaultLink;
        }

        $message = '';
        switch ($this->respondMessageType) {
            case ResourceManager::MESSAGE_TYPE_TEXT:
            {
                $message = MessageCreator::instance()->createText($this->data['sender_id'], $this->data['receiver_id'], $this->respondMessage);
                break;
            }
            case ResourceManager::MESSAGE_TYPE_NEWS:
            {
                if ($this->respondMessage) {
                    $message =  MessageCreator::instance()->createNewsMessage($this->data['receiver_id'], $this->data['sender_id'], $this->customer, $this->respondMessage, $this->respondDefaultLink);
                } else {
                    $message =  '';
                }
                break;
            }
            case ResourceManager::MESSAGE_TYPE_PRODUCT:
            {
                if ($this->respondMessage) {
                    $message =  MessageCreator::instance()->createProductMessage($this->data['receiver_id'], $this->data['sender_id'], $this->customer, $this->respondMessage, $this->respondDefaultLink);
                } else {
                    $message =  '';
                }
                break;
            }
        }
        return $message;
    }

    private function addUser(array $xmlData) {
        $this->db->begin();
        $this->logger->debug("开始处理用户订阅信息");
        $errorMessages = [];
        $openId = (string)$xmlData['sender_id'];
        $this->logger->info('openid:' . $openId);

        try {
            $userForWeibo = UsersWeibo::findFirst("uid='{$openId}'");
            if (!$userForWeibo) {
                $client = new FansClient($this->appKey, $this->appSecret, $this->accessToken);
                $userInfo = $client->get_user_info($openId);
                if ($client->isFailed()) {
                    $this->logger->debug("获取微信用户基本信息失败。" . $client->getErrorMessage());
                    $userInfo['nickname'] = "新用户" . rand(9999);
                    $userInfo['language'] = "zh_CN";
                    $userInfo['sex'] = "f";
                    $userInfo['country'] = '';
                    $userInfo['province'] = '';
                    $userInfo['city'] = '';
                    $userInfo['headimgurl'] = '';
                }

                $userGroupId = $client->get_user_group($this->customer_weibo->uid, $openId);
                if($client->isFailed()) {
                    $this->logger->debug("获取微信用户所属分组失败。" . $client->getErrorMessage());
                    $userGroupId = 0;
                }

                $this->logger->info("微信用户入库开始");
                $userForWeibo = new UsersWeibo();
                $userForWeibo->customer_id = $this->customer;
                $userForWeibo->open_id = "{$openId}";
                $userForWeibo->group_id = $userGroupId;
                $userForWeibo->nickname = $userInfo['nickname'];
                $userForWeibo->sex = $userInfo['sex'];
                $userForWeibo->city = $userInfo['city'];
                $userForWeibo->province = $userInfo['province'];
                $userForWeibo->country = $userInfo['country'];
                $userForWeibo->language = $userInfo['language'];
                $userForWeibo->headimgurl = $userInfo['headimgurl'];
                $userForWeibo->subscribe = $userInfo['subscribe'];
                $userForWeibo->subscribe_time = $userInfo['subscribe_time'];
                $userForWeibo->is_binded = 0;
                if (!$userForWeibo->save()) {
                    foreach ($userForWeibo->getMessages() as $message) {
                        $errorMessages[] = (string)$message;
                    }
                    throw new \Phalcon\Exception("微信用户信息入库失败。: " . implode(',', $errorMessages));
                }
                else {
                    $this->logger->info("微信用户入库成功！");
                }

            }
            $this->user_info = $userForWeibo;

            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
            $this->logger->debug("保存用户信息失败：" . $e->getMessage());
        }
    }

} 