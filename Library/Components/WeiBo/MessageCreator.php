<?php

namespace Components\WeiBo;

// use Components\JSON2XML;
use Components\WeChat\MessageManager;
use Util\EasyEncrypt;
use Phalcon\Mvc\User\Plugin;

class MessageCreator extends Plugin
{

    /**
     * @var MessageCreator
     */
    private static $instance = null;

    public $baseUrl = "";

    /**
     * @var \Phalcon\Logger\Adapter\File
     */
    public $logger = null;

    private function __construct()
    {
        $this->logger = $this->di->get("weiboLogger");
    }

    public static function  instance()
    {
        if (!self::$instance instanceof MessageCreator) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $receiver_id
     * @param $sender_id
     * @param $text
     * @return string
     */
    public function createText($receiver_id, $sender_id, $text)
    {
        $data = array(
            "result" => true,
            "receiver_id" => $receiver_id,
            "sender_id" => $sender_id,
            'type' => ResourceManager::RESOURCE_TYPE_TEXT,
            'data' => urlencode(json_encode(array('text' => $text)))
        );
        $data = ($data);
        return $data;
    }

    public function createNewsMessage($from, $to, $articles = array(), $defaultLink = null)
    {
        $this->logger->info("news message");
        $articlesArr = [];
        $articlesNum = 0;
        foreach ($articles as $article) {
            if ($articlesNum > 8) {
                break;
            }
            if (empty($articlesNum) && is_string($defaultLink) && strlen($defaultLink) > 0) {
                $articleUrl = $defaultLink;
            } else {
                $articleUrl = $this->uri->appUrl(). '/article/detail/' . EasyEncrypt::encode($article['value']);
            }
            if (strpos($articleUrl, '?') === false) {
                $articleUrl .= '?from_user=' . EasyEncrypt::encode($to) . '&platform=' . MessageManager::PLATFORM_TYPE_WEIBO;
            } else {
                $articleUrl .= '&from_user=' . EasyEncrypt::encode($to) . '&platform=' . MessageManager::PLATFORM_TYPE_WEIBO;
            }

            $image = $article['image'];
            if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
                $image = $this->uri->appUrl(). '/' . ltrim($image, '/');
            }

            $articlesArr[] = array(
                'display_name' => $article['title'],
                'summary' => $article['desc'],
                'image' => $image,
                'url' => $articleUrl
            );
            $articlesNum++;
        }
        $articlesArr = urlencode(json_encode(array('articles' => $articlesArr)));
        $data = array(
            'result' => true,
            'sender_id' => $from,
            'receiver_id' => $to,
            'type' => 'articles',
            'data' => $articlesArr
        );
        return $data;
    }

    public function createProductMessage($from, $to, $customerId, $products = array(), $defaultLink = null)
    {
        if ($customerId <= 0) {
            return false;
        }
        $articlesArr = "";
        $articlesNum = 0;
        foreach ($products as $article) {
            if ($articlesNum > 8) {
                break;
            }
            if (empty($articlesNum) && is_string($defaultLink) && strlen($defaultLink) > 0) {
                $productUrl = $defaultLink;
            } else {
                $productUrl = $this->uri->appUrl(). '/shop/item/' . EasyEncrypt::encode($article['value']);
            }
            if (strpos($productUrl, '?') === false) {
                $productUrl .= '?from_user=' . EasyEncrypt::encode($to) . '&platform=' . MessageManager::PLATFORM_TYPE_WEIBO;
            } else {
                $productUrl .= '&from_user=' . EasyEncrypt::encode($to) . '&platform=' . MessageManager::PLATFORM_TYPE_WEIBO;
            }

            $image = $article['image'];
            if (strpos($image, "http://") === false && strpos($image, "https://") === false) {
                $image = $this->uri->appUrl(). '/' . ltrim($image, '/');
            }
            $articlesArr[] = array(
                'display_name' => $article['title'],
                'summary' => $article['desc'],
                'image' => $image,
                'url' => $productUrl
            );
            $articlesNum++;
        }
        $articlesArr = urlencode(json_encode(array('articles' => $articlesArr)));
        $data = array(
            'result' => true,
            'sender_id' => $from,
            'receiver_id' => $to,
            'type' => 'articles',
            'data' => $articlesArr
        );
        return $data;
    }
}

?>