<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-8
 * Time: 下午3:22
 */

namespace Components\WeiBo;

use Models\Article\SiteArticles;
use Models\Product\Product;
use Phalcon\Mvc\User\Plugin;

class ResourceManager extends Plugin
{

    const RESOURCE_TYPE_TEXT = "text";
    const RESOURCE_TYPE_NEWS = "news";
    const RESOURCE_TYPE_PRODUCT = 'product';

    /**
     * @var ResourceManager
     */
    private static $instance = null;

    /**
     * @return ResourceManager
     */
    public static function instance()
    {
        if (!self::$instance instanceof ResourceManager) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param string $messageType
     * @param string $messageId
     * @return array
     */
    public function getMessage($messageType, $messageId)
    {
        $data = array();
        if ($messageType == self::RESOURCE_TYPE_TEXT) {
            $data = $messageId;
        } else if ($messageType == self::RESOURCE_TYPE_NEWS) {
            $msgIds = explode(',', $messageId);
            foreach ($msgIds as $id) {
                $message = SiteArticles::findFirst("id='{$id}'");
                if ($message) {
                    $message = $message->toArray();
                    $data[] = array(
                        'value' => $id,
                        'image' => $message['cover'],
                        'title' => $message['title'],
                        'desc' => $message['meta_desc']
                    );
                }
            }
        } else if ($messageType == self::RESOURCE_TYPE_PRODUCT) {
            $msgIds = explode(',', $messageId);
            foreach ($msgIds as $id) {
                $message =Product::findFirst("id='{$id}'");
                if ($message) {
                    $message = $message->toArray();
                    $data[] = array(
                        'value' => $id,
                        'image' => $message['thumb'],
                        'title' => $message['name'],
                        'desc' => $message['meta_description']
                    );
                }
            }
        }
        return $data;
    }

    /**
     * @param $customer
     * @param $type
     * @param int $page
     * @return \Phalcon\Paginator\Adapter\stdClass
     */
    public function getCustomerMessage($customer, $type, $page = 1)
    {
        $resource = array();
        switch ($type) {
            case ResourceManager::RESOURCE_TYPE_NEWS:
            {
                $queryBuilder = $this->modelsManager->createBuilder()
                    ->addFrom('\\Models\\Article\\SiteArticles', 'article')
                    ->where("article.customer_id='{$customer}' AND LENGTH(article.cover) > 0")
                    ->columns('article.id, article.cover, article.title');
                break;
            }
            case self::RESOURCE_TYPE_PRODUCT:
            {
                $queryBuilder = $this->modelsManager->createBuilder()
                    ->addFrom('\\Models\\Product\\Product', 'product')
                    ->where("product.customer_id='{$customer}' AND LENGTH(product.thumb) > 0");
                break;
            }
            default:
                $queryBuilder = $this->modelsManager->createBuilder()
                    ->addFrom('\\Models\\WeChat\\MessageResourceImages', 'image')
                    ->where("image.customer_id='{$customer}'");
        }

        $pagination = new \Phalcon\Paginator\Adapter\QueryBuilder(array(
            "builder" => $queryBuilder,
            "limit" => 10,
            "page" => $page
        ));
        $data = $pagination->getPaginate();
        return $data;
    }
} 