<?php
namespace Components\WeiBo;

/**
 * 新浪微博操作类V2
 *
 * 使用前需要先手工调用saetv2.ex.class.php <br />
 *
 * @package sae
 * @author Easy Chen, Elmer Zhang,Lazypeople
 * @version 1.0
 */
class FansClient
{

    private $result = array();

    private $isFailed = false;

    private $message = "";

    static $error_code = array(
        '284XX' => "",

    );

    /**
     * 构造函数
     *
     * @access public
     * @param mixed $akey 微博开放平台应用APP KEY
     * @param mixed $skey 微博开放平台应用APP SECRET
     * @param mixed $access_token OAuth认证返回的token
     * @param mixed $refresh_token OAuth认证返回的token secret
     * @return void
     */
    function __construct($akey, $skey, $access_token, $refresh_token = NULL)
    {
        $this->oauth = new SaeTOAuthV2($akey, $skey, $access_token, $refresh_token);
    }

    /**
     * 开启调试信息
     *
     * 开启调试信息后，SDK会将每次请求微博API所发送的POST Data、Headers以及请求信息、返回内容输出出来。
     *
     * @access public
     * @param bool $enable 是否开启调试信息
     * @return void
     */
    function set_debug($enable)
    {
        $this->oauth->debug = $enable;
    }

    /**
     * 设置用户IP
     *
     * SDK默认将会通过$_SERVER['REMOTE_ADDR']获取用户IP，在请求微博API时将用户IP附加到Request Header中。但某些情况下$_SERVER['REMOTE_ADDR']取到的IP并非用户IP，而是一个固定的IP（例如使用SAE的Cron或TaskQueue服务时），此时就有可能会造成该固定IP达到微博API调用频率限额，导致API调用失败。此时可使用本方法设置用户IP，以避免此问题。
     *
     * @access public
     * @param string $ip 用户IP
     * @return bool IP为非法IP字符串时，返回false，否则返回true
     */
    function set_remote_ip($ip)
    {
        if (ip2long($ip) !== false) {
            $this->oauth->remote_ip = $ip;
            return true;
        } else {
            return false;
        }
    }

    /**
     * OAuth授权之后，获取授权用户的UID
     *
     * 对应API：{@link http://open.weibo.com/wiki/2/account/get_uid account/get_uid}
     *
     * @access public
     * @return array
     */
    function get_uid()
    {
//        return $this->oauth->get( 'account/get_uid' );
        $this->result = $this->oauth->get('account/get_uid');
        if (!$this->result || isset($this->result['error'])) {
            $this->isFailed = true;
            $this->message = isset($this->result['errmsg']) ? $this->result['errmsg'] : "请求失败！";
        } else {
            return $this->result;
        }
    }

    public function get_user_uid($code) {
        $this->result = $this->oauth->get('https://api.weibo.com/oauth2/access_token', array('grant_type' => 'authorization_code', 'redirect_uri' => '', 'code' => $code));
        if (!$this->result || isset($this->result['error'])) {
            $this->isFailed = true;
            $this->message = isset($this->result['errmsg']) ? $this->result['errmsg'] : "请求失败！";
        } else {
            return $this->result;
        }

    }

    public function isFailed() {
        return boolval($this->isFailed);
    }

    public function getErrorMessage() {
        return $this->message;
    }


    public function get_user_info($uid)
    {
        $this->result = $this->oauth->get('https://m.api.weibo.com/2/eps/user/info.json', array('uid' => $uid));
        if (!$this->result || isset($this->result['result'])) {
            $this->isFailed = true;
            $this->message = isset($this->result['errmsg']) ? $this->result['errmsg'] : "请求失败！";
        } else {
            return $this->result;
        }
    }

    public function get_user_list($uid) {
        $this->result = $this->oauth->post("https://m.api.weibo.com/2/messages/subscribers/get.json", array('next_uid' => $uid));
        if($this->result && isset($this->result['result']) && boolval($this->result['result']) === true) {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['errmsg'];
            }
            return false;
        }
        else {
            $this->isFailed = false;
            return true;
        }
    }


    // =========================================

    /**
     * @ignore
     */
    protected function request_with_pager($url, $page = false, $count = false, $params = array())
    {
        if ($page) $params['page'] = $page;
        if ($count) $params['count'] = $count;

        return $this->oauth->get($url, $params);
    }

    /**
     * @ignore
     */
    protected function request_with_uid($url, $uid_or_name, $page = false, $count = false, $cursor = false, $post = false, $params = array())
    {
        if ($page) $params['page'] = $page;
        if ($count) $params['count'] = $count;
        if ($cursor) $params['cursor'] = $cursor;

        if ($post) $method = 'post';
        else $method = 'get';

        if ($uid_or_name !== NULL) {
            $this->id_format($uid_or_name);
            $params['id'] = $uid_or_name;
        }

        return $this->oauth->$method($url, $params);

    }

    /**
     * @ignore
     */
    protected function id_format(&$id)
    {
        if (is_float($id)) {
            $id = number_format($id, 0, '', '');
        } elseif (is_string($id)) {
            $id = trim($id);
        }
    }

    public function get_menus() {
        $this->result = $this->oauth->get("https://m.api.weibo.com/2/messages/menu/show.json", array());
        if($this->result && isset($this->result['menu'])) {
            $this->isFailed = false;
            return $this->result;
        }
        else {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['error'];
            }
            return false;
        }
    }

    public function del_menus() {
        $this->result = $this->oauth->post("https://m.api.weibo.com/2/messages/menu/delete.json");
        if($this->result && isset($this->result['result']) && boolval($this->result['result']) === true) {
            $this->isFailed = false;
            return true;
        }
        else {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['error'];
            }
            return false;
        }
    }

    public function set_menus($menus = array()) {
        $this->result = $this->oauth->post("https://m.api.weibo.com/2/messages/menu/create.json", array('menus' => (json_encode($menus))));
        if($this->result && isset($this->result['result']) && boolval($this->result['result']) === true) {
            $this->isFailed = false;
            return true;
        }
        else {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['error'];
            }
            return false;
        }
    }

    public function get_user_group($uid, $follower_id) {
        $this->result = $this->oauth->post("https://m.api.weibo.com/2/messages/custom_rule/getid.json", array('uid' => $uid, 'follower_id' => $follower_id));
        if($this->result && isset($this->result['result']) && boolval($this->result['result']) === true) {
            $this->isFailed = false;
            return $this->result['groupid'];
        }
        else {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['error'];
            }
            return false;
        }
    }

    public function update_user_group($uid, $followerId, $toGroupId) {
        $this->result = $this->oauth->post("https://m.api.weibo.com/2/messages/custom_rule/member/update.json", array('uid' => $uid, 'to_groupid' => $toGroupId, 'follower_id' => $followerId));
        if($this->result && isset($this->result['result']) && boolval($this->result['result']) === true) {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['errmsg'];
            }
            return false;
        }
        else {
            $this->isFailed = false;
            return true;
        }
    }

    public function get_groups() {

    }

    public function del_group($uid, $groupId) {
        $this->result = $this->oauth->post("https://m.api.weibo.com/2/messages/custom_rule/delete.json", array('uid' => $uid, 'id' => $groupId));
        if($this->result && isset($this->result['result']) && boolval($this->result['result']) === true) {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['errmsg'];
            }
            return false;
        }
        else {
            $this->isFailed = false;
            return true;
        }
    }

    public function create_group($uid, $name) {
        $this->result = $this->oauth->post("https://m.api.weibo.com/2/messages/custom_rule/create.json", array('uid' => $uid, 'name' => $name));
        if($this->result && isset($this->result['result']) && boolval($this->result['result']) === true) {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['errmsg'];
            }
            return false;
        }
        else {
            $this->isFailed = false;
            return  $this->result['group'];
        }
    }

    public function update_group($uid, $groupId,  $name) {
        $this->result = $this->oauth->post("https://m.api.weibo.com/2/messages/custom_rule/update.json", array('uid' => $uid, 'id' => $groupId, 'name' => $name));
        if($this->result && isset($this->result['result']) && boolval($this->result['result']) === true) {
            $this->isFailed = true;
            if(!$this->result) {
                $this->message = "请求失败！";
            }
            else {
                $this->message = $this->result['errmsg'];
            }
            return false;
        }
        else {
            $this->isFailed = false;
            return true;
        }
    }

}
