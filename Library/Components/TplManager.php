<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 3/31/14
 * Time: 11:15 AM
 */

namespace Components;


use Models\Wap\SiteTpl;
use Models\Wap\SiteTplForCustomer;
use Phalcon\Mvc\User\Component;
use Util\Ajax;

class TplManager extends Component
{
    static $app_id = null;
    static $instance = null;
    static $_config = array('hideHeader' => false, 'hideFooter' => false);

    public function __construct()
    {
    }

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param self ::$app_id
     * @param bool $refresh
     * @return array
     */
    public function getTpl($refresh = false)
    {
        $cache = $this->getDI()->get('memcached');
        $tpl = $cache->get('tpl_' . CUR_APP_ID);

        if ($refresh || !$tpl) {
            $tpl = $this->getTplFromDb(CUR_APP_ID);
            // cache it
            $cache->save('tpl_' . CUR_APP_ID, $tpl);
            return $tpl;
        }

        return $tpl;
    }

    /**
     * @param $type
     * @param $data
     */
    public function setTpl($type, $data)
    {
        $tpl_mode = in_array($type, array('kit', 'customize')) ? $type : 'single';

        // update data
        $row = array($type => json_encode($data), 'tpl_mode' => $tpl_mode);

        $tpl = SiteTplForCustomer::findFirst('customer_id=' . CUR_APP_ID);
        $tpl->update($row);

        // cache it
        $cache = $this->getDI()->get('memcached');
        $cache->save('tpl_' . CUR_APP_ID, $tpl->toArray());
    }

    public function setTplMode($mode)
    {
        if (!in_array($mode, array('kit', 'single', 'customize'))) return;
        // update data
        $row = array('tpl_mode' => $mode);
        if ($mode == 'customize') {
            $cus_tpl = SiteTpl::findFirst('customer_id=' . CUR_APP_ID . ' and type="customize"');
            if (!$cus_tpl) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '对不起，您没有定制化模板！');
            }
            $row['customize'] = json_encode(array("serial_number" => $cus_tpl->serial_number));
        }
        $tpl = SiteTplForCustomer::findFirst('customer_id=' . CUR_APP_ID);
        $tpl->update($row);

        // cache it
        $cache = $this->getDI()->get('memcached');
        $cache->save('tpl_' . CUR_APP_ID, $tpl->toArray());
        return Ajax::init()->outRight('');
    }

    /**
     *
     * @param self ::$app_id
     * @return array
     */
    private function getTplFromDb()
    {
        $tpl = SiteTplForCustomer::findFirst('customer_id=' . CUR_APP_ID);
        if (!$tpl) {
            $data = array(
                'customer_id' => CUR_APP_ID,
                'created' => time(),
            );

            $t = new SiteTplForCustomer();
            $t->create($data);

            // and get info again
            $tpl = SiteTplForCustomer::findFirst('customer_id=' . CUR_APP_ID);

        }

        return $tpl->toArray();
    }

    /**
     *
     * @param $page_type
     * @param string $industry
     * @return string
     */
    public function getViewTypeDir($page_type, $industry = '')
    {
        if ($page_type == 'customize') {
            // 定制化模板
            $page_type = $base_dir = 'customize';
        } else {
            if ($page_type == 'kit') {
                // 套装模式
                $base_dir = $page_type = 'kit';
            } else {

                $page_type = $page_type == 'menu' ? 'menu' : $this->router->getControllerName();

                $page_type_arr = array(
                    'user' => 'user',
                    'shop' => 'shop',
                    'menu' => 'menu',
                );

                // 默认为home下
                $page_type = isset($page_type_arr[$page_type]) ? $page_type_arr[$page_type] : 'home';
                $base_dir = $base_dir = 'tpl/' . $page_type;
            }
        }

        $serial_num = TplManager::init(CUR_APP_ID)->getTplSerialNumber($page_type);
        return $base_dir . '/' . $serial_num . '/';
    }

    /**
     * get css dir by page_type
     */
    public function getCss($tpl_mode, $page_type = null)
    {
        if ($tpl_mode == 'customize') {
            // 定制化模板
            $page_type_name = $base_dir = 'customize';

            $serial_num = TplManager::init(CUR_APP_ID)->getTplSerialNumber($page_type_name);

            return $base_dir . '/' . $serial_num . '.css';
        } else {
            if ($tpl_mode == 'kit') {
                // 套装模式
                $base_dir = 'kit';
                $page_type_name = 'kit';
            } else {
                // 单品模式
                $industry = isset($tpl['industry']) ? $tpl['industry'] : '';
                $page_type = $page_type ? $page_type : $this->router->getControllerName();

                $page_type_arr = array(
                    'index' => 'home',
                    'menu' => 'menu',
                    'user' => 'user',
                    'shop' => 'shop',
                );

                // 默认为home下
                $page_type_name = isset($page_type_arr[$page_type]) ? $page_type_arr[$page_type] : 'home';

                if ($industry) {
                    $base_dir = 'industry/' . $page_type_name . '/' . $industry; // 行业模板
                } else {
                    $base_dir = $page_type_name;
                }
            }

            $serial_num = TplManager::init(CUR_APP_ID)->getTplSerialNumber($page_type_name);

            return $base_dir . '/' . $serial_num . '/' . $page_type_name . '.css';
        }
    }

    /**
     *
     * @param $page_type
     * @param string $industry
     * @return string
     */
    public function getViewConfig($page_type, $industry = '')
    {
        $dir = $this->getViewTypeDir($page_type, $industry);

        return;
    }

    /**
     * parse tpl info
     *
     * @param self ::$app_id
     * @return array
     */
    private function parseTplInfo()
    {
        $tpl = $this->getTpl(CUR_APP_ID);
        $kit = json_decode(@$tpl['kit'], true);
        $home = json_decode(@$tpl['home'], true);
        $shop = json_decode(@$tpl['shop'], true);
        $user = json_decode(@$tpl['user'], true);
        $customize = json_decode(@$tpl['customize'], true);
        $menu = json_decode(@$tpl['menu'], true);

        $serial_number['kit'] = $kit['serial_number'];
        $serial_number['customize'] = $customize['serial_number'];
        $serial_number['home'] = $home['serial_number'];
        $serial_number['shop'] = $shop['serial_number'];
        $serial_number['user'] = $user['serial_number'];
        $serial_number['menu'] = $menu['serial_number'];

        return array('serial_numbers' => $serial_number);
    }

    /**
     * get tpl serial_number
     *
     * @param self ::$app_id
     * @param string $type
     * @return string
     */
    public function getTplSerialNumber($type = '')
    {
        $serial = $this->parseTplInfo();
        if ($type) {
            return isset($serial['serial_numbers'][$type]) ? $serial['serial_numbers'][$type] : '';
        }

        return $serial['serial_numbers'];
    }


    /**
     * get tpl id by type
     *
     * @param self ::$app_id
     * @param string $type
     * @return string
     */
    public function getTplIdByType($type = '')
    {
        $serial = $this->parseTplInfo();
        if ($type) {
            return isset($serial['ids'][$type]) ? $serial['ids'][$type] : '';
        }

        return $serial['ids'];
    }

    /**
     * render wap file
     *
     * @param self ::$app_id
     * @param $type
     * @param $file
     */
    public function render($type, $file)
    {
        $serial = $this->getTplSerialNumber($type);
        $dir = $this->getViewTypeDir($type, $serial);
        $basePath = MODULE_PATH . '/' . 'Views';
        $file = $basePath . '/' . trim($dir, '/') . '/' . $file . '.phtml';
        if (file_exists($file)) {
            include_once $file;
        }
    }

    public function baseTpl($layout = 'layout')
    {
        $basePath = MODULE_PATH . '/' . 'Views';
        $file = $basePath . '/base/' . $layout . '.phtml';
        if (file_exists($file)) {
            require_once $file;
        }
    }
}