<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-8-19
 * Time: 上午10:21
 */

namespace Components\Rules;

use Models\User\UserForCustomers;
use Models\User\UserPointLog;
use Models\User\UserPointRules;
use Models\User\Users;
use Multiple\Wap\Helper\EmoiUser;
use Util\Ajax;
use Util\Debug;


class PointRule extends AbstractRule
{
    static $instance = null;
    /**
     * behaviors list
     */

    const BEHAVIOR_SIGN_IN = 1000;
    const BEHAVIOR_REGISTER = 1001;
    const BEHAVIOR_BIND_VIPCARD = 1002;
    const BEHAVIOR_VIPCARD_CHECK_IN = 1003;
    const BEHAVIOR_TRADE_ONLINE = 1004;
    const BEHAVIOR_CONVERT_PRODUCT = 1005;
    const BEHAVIOR_LEAVE_MESSAGE = 1006;
    const BEHAVIOR_ADD_FAVORITE = 1007;
    const BEHAVIOR_DEL_FAVORITE = 1008;
    const BEHAVIOR_SHARE = 1009;
    const BEHAVIOR_VIPCARD_RECHARGE = 1010;
    const BEHAVIOR_SHARE_TRADE_SUCCESS = 1011;
    const BEHAVIOR_ADD_PRODUCT_COMMENT = 1012;
    const BEHAVIOR_INVITE_REGIEST_NEW_USER = 1013;
    const BEHAVIOR_WHEEL = 1030;
    const BEHAVIOR_SCRATCH = 1031;
    const BEHAVIOR_BIRTHDAY_GIFT = 1032;

    public static $behaviorNameMap = array(
        self::BEHAVIOR_SIGN_IN => "登陆",
        self::BEHAVIOR_REGISTER => "注册",
        self::BEHAVIOR_VIPCARD_CHECK_IN =>"VIP会员签到送积分",
        self::BEHAVIOR_BIND_VIPCARD => "初次绑定会员卡",
//        self::BEHAVIOR_TRADE_ONLINE => "在线购买商品成功",
        self::BEHAVIOR_LEAVE_MESSAGE => "留言板留言通过审核",
        self::BEHAVIOR_ADD_FAVORITE => "增加收藏条目", //收藏
        self::BEHAVIOR_DEL_FAVORITE => "删除收藏条目",
        self::BEHAVIOR_SHARE => "分享即送积分（跟购买后送积分不同）",
        self::BEHAVIOR_ADD_PRODUCT_COMMENT => "购买商品后评论",
        self::BEHAVIOR_INVITE_REGIEST_NEW_USER => "邀请朋友成功关注公众号",
    );

    public static function init($customer_id, $vip_grade)
    {
        if (!self::$instance) {
            self::$instance = new self($customer_id, $vip_grade);
        }
        return self::$instance;
    }

    public function executeRule($user_id, $behavior,$is_firm='0')
    {
        $rule = $this->getRule($behavior,$is_firm);
        return $this->execute($user_id, $rule);
    }

    public function execute($user_id, RuleStructure $rule)
    {
        if (!$rule->isValid()) {
            return false;
        }
        $userModel = UserForCustomers::findFirst('user_id = ' . $user_id . ' and customer_id=' . CUR_APP_ID);

        if (!($userModel && $this->checkLogUnique($user_id, $rule))) {
            return false;
        }
        $userPoints = $userModel->points;
        $userPointsAvailable = $userModel->points_available;
        if ($rule->action == self::ACTION_UP) {
            $userPoints += $rule->getValue();
            $userPointsAvailable += $rule->getValue();
        }

        if ($rule->action == self::ACTION_DOWN) {
            if (empty($userPointsAvailable) || $userPointsAvailable < $rule->getValue()) {
                return Ajax::ERROR_POINTS_NOT_ENOUGH;
            }
            $userPointsAvailable -= $rule->getValue();
        }
        $this->db->begin();
        try {
            // 更新积分
            $data = array(
                'points' => $userPoints,
                'points_available' => $userPointsAvailable
            );

            if (!$userModel->update($data)) {
                $messages = [];
                foreach ($userModel->getMessages() as $msg) {
                    $messages[] = (string)$msg;
                }
                throw new \Phalcon\Exception(join(',', $messages));
            }

            $rule->total = $userPointsAvailable;
            self::writeLog($user_id, $rule);
            $this->db->commit();
        } catch (\Exception $e) {
            $this->di->get('debugLogger')->error($e->getMessage());
            $this->db->rollback();
            return false;
        }
        return true;
    }

    public function getRule($behavior,$is_firm='0')
    {
        $rule = $this->getRuleData($behavior,$is_firm);
        $ruleStructure = new RuleStructure();
        if ($rule) {
            $ruleStructure->setAction($rule->action);
            $ruleStructure->setBehavior($rule->behavior);
            $ruleStructure->setTerm($rule->term);
            $ruleStructure->setValue($rule->points);
        }
        return $ruleStructure;
    }

    private function getRuleData($behavior,$is_firm='0')
    {
        // 默认按等级
        $where = 'behavior="' . $behavior . '" and customer_id=' . CUR_APP_ID . ' and vip_grade=' . intval($this->vip_grade);
        $firm='';
        if($is_firm==1)//是否企业用户
        {
           $firm=" and is_firm=1 ";
         }
        else
         {
            $firm=" and is_firm=0 ";
         }
        $rule = UserPointRules::findFirst($where.$firm);
        // 找不到则按默认0等级
        if (!$rule) {
            $where0 = 'behavior="' . $behavior . '" and customer_id=' . CUR_APP_ID . ' and vip_grade=0';
            $rule = UserPointRules::findFirst($where0.$firm);
        }
        return $rule;
    }

    public function getRulePoints($behavior,$is_firm='0')
    {
        $rule = $this->getRuleData($behavior,$is_firm);
        if (!$rule) {
            return 0;
        }
        return intval($rule->points);
    }

    protected function checkLogUnique($user, RuleStructure $rule)
    {
        if (!$rule->isValid()) {
            return false;
        }
        $behavior = $rule->getBehavior();

        $where = 'user_id=' . $user . ' AND action=' . $behavior . ' AND customer_id=' . CUR_APP_ID;
        switch ($rule->term) {
            case self::TERM_ONLY_ONE:
                $pointLog = UserPointLog::findFirst($where);
                if ($pointLog) {
                    return false;
                } else {
                    return true;
                }
                break;
            case self::TERM_ONCE_A_DAY:
                $pointLog = UserPointLog::findFirst(array($where, 'order' => 'logged DESC'));
                if ($pointLog) {
                    $timeStart = strtotime(date('Y-m-d'));
                    if ($pointLog->logged >= $timeStart) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
                break;
            case self::TERM_EVERY_BEHAVIOR:
            default:
                return true;
        }
    }

    /**
     * @param $user
     * @param RuleStructure $rule
     * @return bool|mixed
     * @throws \Phalcon\Exception
     */
    public function writeLog($user, RuleStructure $rule)
    {
        if ($rule->isValid()) {
            $data['user_id'] = $user;
            $data['customer_id'] = CUR_APP_ID;
            $data['vip_grade'] = $this->vip_grade;
            $data['in_out'] = $rule->getAction();
            $data['action'] = $rule->getBehavior();
            $data['total'] = $rule->total;
            $data['logged'] = $_SERVER['REQUEST_TIME'];
            $data['value'] = $rule->getValue();
            $data['action_desc'] = $this->getBehaviorName($rule->getBehavior());
            $pointLog = new UserPointLog();
            if (!$pointLog->create($data)) {
                $messages = [];
                foreach ($pointLog->getMessages() as $msg) {
                    $messages[] = (string)$msg;
                }
                $this->di->get('errorLogger')->error(json_encode($messages));
            }
        }
        return true;
    }

    /**
     * @param $behavior
     * @param $action
     * @param $value
     * @param $term
     * @return bool|mixed
     */
    public function setRule($behavior, $action, $value, $term)
    {
        if (!self::behaviorCheck($behavior)) {
            return false;
        }

        $rule = UserPointRules::findFirst('customer_id=' . CUR_APP_ID . ' and behavior=' . $behavior . ' and vip_grade= ' . $this->vip_grade);
        if ($rule) {
            if ($rule->action != $action || $value != $rule->points || $rule->term != $term) {
                return $rule->update(array(
                    'action' => $action,
                    'points' => $value,
                    'term' => $term
                ));
            } else {
                return true;
            }
        } else {
            $data = array(
                'customer_id' => CUR_APP_ID,
                'vip_grade' => $this->vip_grade,
                'behavior' => $behavior,
                'action' => $action,
                'points' => $value,
                'term' => $term,
                'created' => time()
            );

            $rule = new UserPointRules();

            return $rule->create($data);
        }
    }
} 