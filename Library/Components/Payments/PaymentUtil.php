<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-6-5
 * Time: 上午11:14
 */

namespace Components\Payments;


use Models\BaseModel;
use Models\Payments\PaymentConfigItems;
use Models\Payments\PaymentConfigMeta;
use Models\Payments\PaymentForCustomers;
use Models\Payments\PaymentForHosts;
use Models\Payments\Payments;
use Phalcon\Mvc\User\Plugin;

class PaymentUtil extends Plugin {

    const BELONG_TYPE_HOST = 'host';
    const BELONG_TYPE_CUSTOMER = 'customer';

    const META_VALUE_TYPE_TEXT = 'text';
    const META_VALUE_TYPE_RADIO = 'radio';
    const META_VALUE_TYPE_CHECKBOX = 'checkbocx';

    const ALIPAY_TYPE_DIRECT = 'direct';
    const ALIPAY_TYPE_ESCOW = 'escow';
    const ALIPAY_TYPE_BOTH = 'both';
    const ALIPAY_TYPE_WAP_DIRECT = 'wap';

    const ORDER_TYPE_PLATFORM = 'PO';
    const ORDER_TYPE_SHOP_ORDER = "SO";
    const ORDER_TYPE_VIP_CARD = 'CO';

    const PAYMENT_KEY_ALIPAY = 'alipay';
    const PAYMENT_KEY_CBPAY = 'cbpay';
    const PAYMENT_KEY_TENPAY = 'tenpay';
    const PAYMENT_KEY_WXPAY = 'wxpay';
    const PAYMENT_KEY_UNIONPAY = 'unionpay';
    const PAYMENT_KEY_COD = 'cod';

    /**
     * @var PaymentUtil
     */
    private static $instance;

    /**
     * @var \Phalcon\Cache\Backend\Libmemcached
     */
    private $cache = null;

    private function __construct() {
        $this->cache = $this->di->get('memcached');
    }

    public static function instance($host_key) {
        if(!static::$instance instanceof PaymentUtil) {
            static::$instance = new self();
        }

        static::$instance->host_key = HOST_KEY;
        return static::$instance;
    }

    /**
     * @param bool $refresh
     * @return array | \Models\BaseModel
     */
    public function getPaymentTypes($type = null, $isCustomer = true, $refresh = false) {
        $cacheKey = "payment_types_cache_key";
        if(!empty($type)) {
            $cacheKey .= "_" . $type;
        }
        else {
            $cacheKey .= "_all";
        }

        $data = $this->cache->get($cacheKey);
        if(!$data || $refresh) {
            $data = [];
            if(!empty($type)) {
				$filter = "key='{$type}'";
				if($isCustomer) {
                    $filter .= " AND customer_use='1'";
				}

                $payment = Payments::findFirst("key='{$type}'");
                if($payment) {
                    $payment->config_meta = json_decode($payment->config_meta, true);
                    $data = $payment->toArray();
                    $this->cache->save($cacheKey, $data);
                }
            }
            else {
				if($isCustomer) {
                    $payments = Payments::find("customer_use='1'");
				}
                else {
					$payments = Payments::find();
				}
                if($payments) {
                    foreach($payments as $payment) {
                        $payment->config_meta = json_decode($payment->config_meta, true);
                        $data[] = $payment->toArray();
                    }
                    $this->cache->save($cacheKey, $data);
                }
            }
        }
        return $data;
    }

    public function getPaymentConfigMeta($type, $refresh = false){
        $cacheKey = "payment_config_meta_list_" . $type;
        $data = $this->cache->get($cacheKey);
        if($refresh || !$data) {
            $metas = PaymentConfigMeta::find("payment_key='{$type}'");
            $data = [];
            if($metas) {
                foreach($metas as $meta) {
                    if(strtolower($meta->type) != self::META_VALUE_TYPE_TEXT) {
                        $meta->value_list = json_decode($meta->value_list, true);
                    }
                    $data[] = $meta->toArray();
                }
                $this->cache->save($cacheKey, $data);
            }
        }

        return $data;
    }

    public function getHostPayments($type = null, $refresh = false) {
        $cacheKey = "payments_for_host_" . HOST_KEY;
        if(!empty($type)) {
            $cacheKey .= "_" . $type;
        }
        else {
            $cacheKey .= "_all";
        }

        $data = $this->cache->get($cacheKey);
        if(!$data || $refresh) {
            $payments = $this->getPaymentTypes($type, false, true);
            $data = [];
            if($payments) {
                if($type) {
                    $payment = $payments;
                    $paymentForHost = PaymentForHosts::findFirst("type='{$payments['key']}' AND host_key='" . HOST_KEY . "'");
                    if($paymentForHost && $paymentForHost->is_active) {
                        $payment['is_active'] = 1;
                    }
                    else {
                        $payment['is_active'] = 0;
                    }
                    $data = $payment;
                }
                else {
                    foreach($payments as $payment) {
                        $paymentForHost = PaymentForHosts::findFirst("type='{$payment['key']}' AND host_key='" . HOST_KEY . "'");
                        if($paymentForHost && $paymentForHost->is_active) {
                            $payment['is_active'] = 1;
                        }
                        else {
                            $payment['is_active'] = 0;
                        }
                        $data[] = $payment;
                    }
                }

                unset($payments);
                $this->cache->save($cacheKey, $data);
            }
        }
        return $data;
    }

    public function getCustomerPayments($customer_id, $type = null, $refresh = true) {
        $cacheKey = "payments_for_customer_" . $customer_id;
        if(!empty($type)) {
            $cacheKey .= "_" . $type;
        }
        else {
            $cacheKey .= "_all";
        }

        $data = $this->cache->get($cacheKey);

        if(!$data || $refresh) {
            $payments = $this->getPaymentTypes($type, true, true);
            $data = [];
            if($payments) {
                $payments = $this->getPaymentTypes($type, true, true);
                $data = [];
                if($payments) {
                    if($type) {
                        $payment = $payments;
                        $paymentForCustomer = PaymentForCustomers::findFirst("type='{$payments['key']}' AND customer_id='{$customer_id}'");
                        if($paymentForCustomer && $paymentForCustomer->is_active > 0) {
                            $payment['is_active'] = 1;
                        }
                        else {
                            $payment['is_active'] = 0;
                        }
                        if($paymentForCustomer && $paymentForCustomer->use_platform > 0) {
                            $payment['use_platform'] = 1;
                        }
                        else {
                            $payment['use_platform'] = 0;
                        }
                        $data = $payment;
                    }
                    else {
                        $paymentsForCustomer = PaymentForCustomers::find("customer_id='{$customer_id}'");
                        foreach($payments as $payment) {
                            if($paymentsForCustomer) {
                                $hasMatched = false;
                                foreach($paymentsForCustomer as $paymentForCustomer) {
                                    if(strtolower(trim($paymentForCustomer->type)) == strtolower(trim($payment['key']))) {
                                        $hasMatched = true;
                                        if($paymentForCustomer->is_active > 0) {
                                            $payment['is_active'] = 1;
                                        }
                                        else {
                                            $payment['is_active'] = 0;
                                        }
                                        if($paymentForCustomer->use_platform > 0) {
                                            $payment['use_platform'] = 1;
                                        }
                                        else {
                                            $payment['use_platform'] = 0;
                                        }
                                        break 1;
                                    }
                                }
                                if(!$hasMatched) {
                                    $payment['is_active'] = 0;
                                    $payment['use_platform'] = 0;
                                }
                            }
                            else {
                                $payment['is_active'] = 0;
                                $payment['use_platform'] = 0;
                            }
                            $data[] = $payment;
                        }
                    }

                    unset($payments);
                    $this->cache->save($cacheKey, $data);
                }
            }
        }
        return $data;
    }

    public function getHostPaymentConfig($type, $refresh = true) {
        $cacheKey = "payment_config_for_host_" . HOST_KEY . "_" . $type;

        $data = $this->cache->get($cacheKey);

        if($refresh || !$data) {
            $data = [];
            $metas = $this->getPaymentConfigMeta($type, true);
            if($metas) {
                foreach($metas as $meta) {
                    $config = PaymentConfigItems::findFirst("config_key='{$meta['meta_name']}' AND belong_type='" . self::BELONG_TYPE_HOST . "' AND key_id='" . HOST_KEY . "' AND payment_type='{$type}'");
                    $meta['config_value'] = $config ? $config->config_value : "";
                    $data[] = $meta;
                }
                $this->cache->save($cacheKey, $data);
            }
        }
        return $data;
    }

    public function getCustomerPaymentConfig($customer_id, $type, $refresh = false) {
        $cacheKey = "payment_config_for_customer_" . $customer_id . "_" . $type;

        $data = $this->cache->get($cacheKey);

        if(!$data || $refresh) {
            $mainConf = PaymentForCustomers::findFirst("customer_id='{$customer_id}'");
            if(!$mainConf) {
                return null;
            }
            $metas = $this->getPaymentConfigMeta($type, true);
            if($metas) {
                $data = [
                    'is_active' => $mainConf->is_active,
                    'use_platform' => $mainConf->use_platform
                ];
                $belongType = self::BELONG_TYPE_CUSTOMER;
                $key_id = $customer_id;
                if($mainConf->use_platform) {
                    $belongType = self::BELONG_TYPE_HOST;
                    $key_id = HOST_KEY;;
                }
                foreach($metas as $meta) {
                    $config = PaymentConfigItems::findFirst("config_key='{$meta['meta_name']}' AND belong_type='" . $belongType . "' AND key_id='{$key_id}' AND payment_type='{$type}'");
                    $meta['config_value'] = $config ? $config->config_value : "";
                    $data[] = $meta;
                }
                $this->cache->save($cacheKey, $data);
            }
        }
        return $data;
    }

    /**
     * @param $belong
     * @param $belong_key
     * @param null $type
     * @return array
     */
    public function getPaymentConfig($belong, $belong_key, $type = null, $refresh = false) {
        if($belong == self::BELONG_TYPE_CUSTOMER) {
            $configs = $this->getCustomerPaymentConfig($belong_key, $type, $refresh);
        }
        else {
            $configs = $this->getHostPaymentConfig($type, $refresh);
        }

        $baseConfig = [];
        if($type) {
            $baseConfig = $this->di->get("config")->payment->{$type};
        }
        if(!$baseConfig) {
            $baseConfig = [];
        }
        foreach($configs as $item) {
            if(is_array($item)) {
                $baseConfig[$item['meta_name']] = $item['config_value'];
            }
        }

        return $baseConfig;
    }
}