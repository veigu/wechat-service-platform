<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:38
 */
namespace Components\ModuleManager;

interface RenderInterface
{
    public function setLayout($layout);

    public function getLayout();

    public function setTemplate($path);

    public function setStyle($style);

    public function getTemplate();

    public function getStyle();

    public function getConfig();

    public function setConfig(array $config);

    public function render();

    public function getUrl($handler, $action);
}

?>