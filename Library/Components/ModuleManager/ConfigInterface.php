<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:41
 */

namespace Components\ModuleManager;


interface ConfigInterface
{
    public function setConfig(array $config);

    public function getConfig();

    public function get($key);

    public function set($key, $val);
} 