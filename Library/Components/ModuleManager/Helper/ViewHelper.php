<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:38
 */
namespace Components\ModuleManager\Helper;

use Phalcon\Mvc\User\Plugin;

class ViewHelper extends Plugin
{

    protected $module;

    protected $handler;

    protected $action;

    protected $render;

    public function __construct($module, $handler)
    {
        $this->module = $module;
        $this->handler = $handler;
    }

    public function getUrl($action, array $params = null, $handler = null, $module = null)
    {
        if (empty($module)) {
            $module = $this->module;
        }
        if (empty($handler)) {
            $handler = $this->handler;
        }
        $baseUrl = "/panel/addon/" . $module . '/' . $handler . '/' . $action;
        if (is_array($params) && count($params) > 0) {
            $baseUrl .= '?';
            $paramStr = '';
            foreach ($params as $key => $val) {
                $paramStr .= '&' . $key . '=' . $val;
            }
            $paramStr = ltrim($paramStr, '&');
            $baseUrl .= $paramStr;
        }
        return $baseUrl;
    }
}

?>