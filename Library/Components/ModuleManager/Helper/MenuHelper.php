<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:38
 */
namespace Components\ModuleManager\Helper;

class MenuHelper
{

    public static $module = '';

    public static $handler = 'default';

    public static $action = "index";

    protected static $_fstMenu = array();
    protected static $_secMenu = array();

    public static function addFstMenu($menus)
    {
        self::$_fstMenu = $menus;
    }

    public static function addSecMenus($menus)
    {
        self::$_secMenu = $menus;
    }

    public static function getFstMenu()
    {
        return self::$_fstMenu;
    }

    public static function getSecMenus()
    {
        return self::$_secMenu;
    }

    public static function setModule($module)
    {
        self::$module = $module;
    }

    public static function setHandler($handler)
    {
        self::$handler = $handler;
    }

    public static function setAction($action)
    {
        self::$action = $action;
    }

    /**
     * @param $handler
     * @param $action
     * @return string
     */
    public static function renderMenu($handler = null, $action = null)
    {
        $curTop = '';
        $curSec = '';
        $curThr = '';

        if (empty($handler)) {
            $handler = self::$handler;
        }

        if (empty($action)) {
            $action = self::$action;
        }

        /*// 2级菜单找当前
        foreach (self::$_secMenu as $k => $sec) {
            if (isset($sec['thr']) && is_array($sec['thr'])) {
                $thrMenu = $sec['thr'];
                foreach ($thrMenu as $t => $thr) {
                    if ($thr['h'] == $handler && $thr['a'] == $action) {
                        $curTop = $sec['top'];
                        $curSec = $k;
                        $curThr = $t;
                        break;
                    }
                }
            } else {
                if ($sec['h'] == $handler && $sec['a'] == $action) {
                    $curTop = $sec['top'];
                    $curSec = $k;
                    $curThr = '';
                    break;
                }
            }
        }
        unset($t);
        unset($k);*/

        $menuString = <<<EOF
<style>
    .thr.active {
        background: #e6e6e6;
    }

    .sec.active .sec-a {
        background: #e6e6e6;
    }
</style>
EOF;
        foreach (self::$_fstMenu as $k => $top) {
            $topClass = '';

            if (isset($top['h']) && isset($top['a'])) {
                if ($handler == $top['h'] && $action == $top['a']) {
                    $topClass = 'fst active';
                }
                $menuString .= "<li class=\"{$topClass}\">"
                    . "<a href=\"/panel/addon?_m=" . self::$module . '&_h=' . $top['h'] . '&_a=' . $top['a'] . "\" class=\"dropdown-toggle\">"
                    . "<i class=\"fa fa-fw {$top['class']}\"></i>"
                    . "<span class=\"menu-text\">{$top['name']}</span>"
                    . "</a></li>";
            } else {
                $subUl = '';
                $isTopCurrent = false;
                if (isset(self::$_secMenu[$k])) {
                    $sec = self::$_secMenu[$k];
                    foreach ($sec as $secItem) {
                        $secName = $secItem['name'];
                        $isSecCurrent = false;
                        //判断当前页
                        if (isset($secItem['thr']) && is_array($secItem['thr'])) {
                            $thrMenu = $secItem['thr'];
                            foreach ($thrMenu as $t => $thr) {
                                if ($thr['h'] == $handler && $thr['a'] == $action) {
                                    $curTop = $k;
                                    $curSec = $secName;
                                    $curThr = $t;
                                    $isSecCurrent = true;
                                    $isTopCurrent = true;
                                }
                            }
                        } else {
                            if ($secItem['h'] == $handler && $secItem['a'] == $action) {
                                $curTop = $k;
                                $curSec = $secName;
                                $curThr = '';
                                $isSecCurrent = true;
                                $isTopCurrent = true;
                            }
                        }

                        $hasThr = isset($secItem['thr']) && is_array($secItem['thr']);
                        $thrDropIcon = $hasThr ? '<span class="arrow icon-angle-down"></span>' : "";
                        $secUrl = !$hasThr ? (isset($secItem['hide']) && $secItem['hide'] ? 'javascript:;' : '/panel/addon?_m=' . self::$module . '&_h=' . $secItem['h'] . '&_a=' . $secItem['a']) : 'javascript:;';
                        $thrClass = $isSecCurrent ? ' active' : '';
                        $subUl .= "<li class=\"{$thrClass}\">"
                            . "<a href=\"{$secUrl}\" class=\"dropdown-toggle\">"
                            . "    <i class=\"fa fa-fw {$secItem['class']}\"></i> {$secItem['name']}" . $thrDropIcon
                            . "</a>";
                        if ($hasThr) {
                            $fourClass = $isSecCurrent ? 'active open' : '';
                            $subUl .= '<ul class=" submenu {$fourClass}">';
                            foreach ($secItem['thr'] as $t => $thr) {
                                $thrUrl = (isset($thr['hide']) && $thr['hide'] ? 'javascript:;' : '/panel/addon?_m=' . self::$module . '&_h=' . $thr['h'] . '&_a=' . $thr['a']);
                                $fiveClass = $t == $curThr && $isSecCurrent ? 'thr active' : '';
                                $subUl .= "<li class=\"{$fiveClass}\""
                                    . "style=\"" . ($t == $curThr && $isSecCurrent ? '' : (isset($thr['hide']) && $thr['hide']) ? 'display:none' : '') . "\">"
                                    . "<a href=\"{$thrUrl}\">"
                                    . "   <i class=\"fa fa-fw {$thr['class']}\"></i> " . $thr['name']
                                    . '</a>'
                                    . '</li>';
                            }
                            $subUl .= '</ul>';
                        }
                    }
                    $topClass = $isTopCurrent ? "active open" : "";
                    $topLi = "<li class=\"{$topClass}\">"
                        . "<a href=\"#\" class=\"dropdown-toggle\">"
                        . "<i class=\"fa fa-fw {$top['class']}\"></i>"
                        . "<span class=\"menu-text\">{$top['name']}</span>"
                        . "<span class=\"arrow icon-angle-down\"></span>"
                        . "</a>";
                    $secClass = $k == $curTop ? 'active open' : '';
                    $topLi .= '<ul class="submenu {$secClass}">';
                    $topLi .= $subUl . '</ul>';
                    $menuString .= $topLi . '</li>';
                }
            }
        }
        return $menuString;
    }
}

?>