<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:37
 */

namespace Components\ModuleManager;


use Multiple\Panel\Plugins\PanelMenu;

class ModuleConfig implements ConfigInterface
{

    protected $config = array();

    protected $data;

    public function __construct($moduleName)
    {
        $path = ROOT . DIRECTORY_SEPARATOR . "Apps" . DIRECTORY_SEPARATOR . "Panel" . DIRECTORY_SEPARATOR . "Modules" . DIRECTORY_SEPARATOR . ucwords($moduleName) . DIRECTORY_SEPARATOR . 'Config.php';
        $config = include_once($path);
        $this->setConfig($config);
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param $key
     * @return array|null
     */
    public function get($key)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        } else {
            return null;
        }
    }

    public function set($key, $val)
    {
        if (isset($this->config[$key])) {
            $this->config[$key] = $val;
        }
        return $this;
    }

    public function setTemplate($template)
    {
        if (!empty($template)) {
            $this->config['render']['template'] = $template;
        }
        return $this;
    }

    public function setStyle($style)
    {
        if (empty($style)) {
            $this->config['render']['style']['current'] = $style;
        }
        return $this;
    }

    public function setPanelMenu($menus)
    {
        if ($menus) {
            PanelMenu::addSideFstMenu($menus['fstMenu']);
            PanelMenu::addSideSecMenu($menus['secMenu']);
        }
        return $this;
    }

    public function setLayout($layout)
    {
        $this->config['render']['layout'] = $layout;
    }

    public function getTemplate()
    {
        return $this->config['render']['template'];
    }

    public function getStyle()
    {
        return $this->config['render']['style']['current'];
    }

    public function getLayout()
    {
        return $this->config['render']['layout'];
    }

    public function getStyles()
    {
        return $this->config['render']['style']['list'];
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }
} 