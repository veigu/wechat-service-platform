<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:38
 */
namespace Components\ModuleManager;

use Components\ModuleManager\Helper\ViewHelper;
use Models\Customer\Customers;
use Phalcon\Mvc\User\Component;
use Phalcon\Tag;

abstract class ModuleHandler extends Component implements HandlerInterface
{
    /**
     * @var ModuleConfig
     */
    public $config = null;

    protected $_moduleName = '';

    protected $_render = null;

    protected $_moduleRoot = '';

    protected $isAuthed = false;

    /**
     * @var \Models\Customer\Customers
     */
    protected $customer = null;
    protected $customer_id = 0;
    /**
     * @var \Models\Customer\CustomerOpenInfo
     */
    protected $customer_wechat = null;

    /**
     * @var \Phalcon\Mvc\Controller
     */
    protected $context = null;

    protected $_handler = '';

    public function __construct($name, $params = null)
    {
        $this->_moduleName = $name;

        if (is_array($params) && count($params) > 0) {
            foreach ($params as $key => $val) {
                $this->$key = $val;
            }
        }

        $this->_moduleRoot = dirname(__FILE__); // . 'Modules' . DIRECTORY_SEPARATOR . ucwords($this->_moduleName) . DIRECTORY_SEPARATOR;

        if (empty($this->_handler)) {
            $selfReflection = new \ReflectionClass($this);
            $fullHandlerName = $selfReflection->getShortName();
            $this->_handler = strtolower(substr($fullHandlerName, 0, strlen($fullHandlerName) - strlen('Handler')));
        }

//        $this->view->setViewsDir($this->_moduleRoot . 'product');

        $this->customer = $this->session->get("customer_info");
        $this->customer_wechat = $this->session->get('customer_wechat');
        if ($this->customer instanceof Customers) {
            if (!defined('CUR_APP_ID')) define('CUR_APP_ID', $this->customer->id);
        }

        $this->view->setVar("customer_id", CUR_APP_ID);
        $customer_name = empty($this->customer->name) ? $this->customer->account : $this->customer->name;
        $this->view->setVar("customer_name", $customer_name);
        $viewHelper = new ViewHelper($this->_moduleName, $this->_handler);
        $this->view->setVar("viewHelper", $viewHelper);
    }

    public function initialize()
    {
//         $this->view->pick($controller->_handler . '/' . $controller->_action);

        return $this;
    }

    /**
     * @return string
     */
    public function getModuleName()
    {
        return $this->_moduleName;
    }

    public function getHandlerName()
    {
        return $this->handlerName;
    }

    public function setConfig(ModuleConfig $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @param bool $authed
     * @return ModuleHandler
     */
    public function setAuth($authed)
    {
        $this->isAuthed = $authed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAuthed()
    {
        return $this->isAuthed;
    }

    /**
     *
     */
    public function config()
    {
        // TODO: Implement config() method.
    }

    /**
     * @return ModuleConfig|void
     */
    public function getConfig()
    {
        // TODO: Implement getConfig() method.
    }

    public function err($code, $msg)
    {
        Tag::setTitle('运行时错误');
        $this->view->setViewsDir(MODULE_PATH . '/Views');
        $this->response->setHeader('content-type', 'text/html;charset=utf-8');
        $this->response->setStatusCode($code, $msg);

        $this->view->setVar('msg', $msg);

        return $this->view->pick('base/error');
    }
}

?>