<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:38
 */
namespace Components\ModuleManager;

use Phalcon\Mvc\User\Component;

abstract class ModuleRender extends Component implements RenderInterface
{

    protected $_moduleRoot = '';

    protected $_config = array();

    protected $_templateDir = '';

    protected $_template = 'default';

    protected $_layout = '';

    protected $_style = '';

    public function __construct($name, array $config)
    {
        $this->_moduleRoot = ROOT . DIRECTORY_SEPARATOR . 'Modules' . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR;
        $this->_config = $config;
    }

    public function setConfig(array $config)
    {

    }

    public function getConfig()
    {
        return $this->_config;
    }

    public function getTemplate()
    {
        return $this->_template;
    }

    public function setStyle($style)
    {
        $this->_style = $style;
        return $this;
    }

    public function getStyle()
    {

    }

    public function setTemplate($path)
    {

    }

    public function getUrl($handler, $action)
    {

    }

    public function setLayout($layout)
    {

    }

    public function getLayout()
    {

    }
}

?>