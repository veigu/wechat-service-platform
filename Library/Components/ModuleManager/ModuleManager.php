<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:38
 */
namespace Components\ModuleManager;

use Components\ModuleManager\Helper\MenuHelper;
use Models\Modules\ResourceForCustomers;
use Models\Modules\ResourceForHosts;
use Models\Modules\Resources;
use Phalcon\Mvc\User\Component;

class ModuleManager extends Component
{

    const ORDER_NUMBER_PREFIX_AUTO = 'SO_AUTO_';
    const ORDER_NUMBER_PREFIX_HANDLE = 'SO_HANDLE_';

    /**
     * @var ModuleManager
     */
    private static $instance;

    /**
     * @var \Phalcon\Cache\Backend\Libmemcached
     */
    private $cache;

    /**
     * @var \Phalcon\Logger\Adapter\File
     */
    private $logger;

    private $customer;

    private function __construct()
    {
        $this->cache = $this->di->get('memcached');
        $this->logger = $this->di->get('debugLogger');
    }

    /**
     * @param $customer
     * @return ModuleManager
     */
    public static function instance($host_key, $customer = null)
    {
        if (!self::$instance instanceof ModuleManager) {
            self::$instance = new self();
        }
        if ($customer > 0) {
            self::$instance->customer = $customer;
        }
        self::$instance->host_key = HOST_KEY;
        return self::$instance;
    }

    /**
     * @param int $customer
     * @param bool $refresh
     * @return \Phalcon\Mvc\Model[]
     */
    public function getCustomerModules($customer = null, $refresh = false)
    {
        if ($customer > 0) {
            $this->customer = $customer;
        }
        $cacheKey = "module_resource_list_for_customer_" . $this->customer;
        $data = $this->cache->get($cacheKey, 86400);
        if (!$data || $refresh) {
            $end_time = time() + 86400;
            $query = $this->modelsManager->createBuilder()
                ->addFrom('\\Models\\Modules\ResourceForCustomers', 'rc')
                ->leftJoin('\\Models\Modules\\Resources', 'r.id = rc.resource_id', 'r')
                ->columns("r.id as id, r.name as name, r.link as link, r.image, r.module_name as module_name, r.is_activity, r.year_price as year_price, r.month_price as month_price, r.type as type, r.description as description, r.created as created, r.image as images, r.belong as belong, r.free_period as free_period, rc.time_end as time_end")
                ->where("rc.customer_id='{$this->customer}' AND r.type='m' AND rc.time_end > {$end_time}");
            $data = $query->getQuery()->execute();
            $this->cache->save($cacheKey, $data, 86400);
        }
        return $data;
    }

    /**
     * @param string $name
     * @return bool|Resources
     */
    public function getModuleResource($name)
    {
        $modules = $this->getSystemModules(HOST_KEY);
        foreach ($modules as $item) {
            if ($item->module_name == $name) {
                return $item;
            }
        }
        return false;
    }

    /**
     * @param int $moduleId
     * @return bool|Resources
     */
    public function getModuleResourceById($moduleId)
    {
        $modules = $this->getSystemModules();
        foreach ($modules as $item) {
            if ($item->id == $moduleId) {
                return $item;
            }
        }
        return false;
    }

    /**
     * @param string $name
     * @param string $handler
     * @param array $params
     * @return int|ModuleHandler
     */
    public function getModuleHandler($name, $handler, $params = null)
    {
        $modules = $this->getSystemModules();
        $handlerClass = "\\Modules\\" . ucwords($name) . '\\Handlers\\' . ucwords($handler) . 'Handler';
        if (class_exists($handlerClass)) {
            foreach ($modules as $item) {
                if ($item->module_name == $name) {

                    $handler = new $handlerClass($name, $params);

                    $config = $this->checkAuth($name);

                    if ($config) {
                        $handler->setConfig($config);
                        $handler->setAuth(true);
                        $menus = $config->get('menus');
                        if (is_array($menus)) {
                            $topMenus = $menus['fstMenu'];
                            $secMenus = $menus['secMenu'];
                            MenuHelper::addFstMenu($topMenus);
                            MenuHelper::addSecMenus($secMenus);
                        }
                    } else {
                        $handler->setAuth(false);
                    }

                    return $handler;
                }

            }

            return 0;
        } else {

            return -1;
        }

    }

    /**
     * @param bool $refresh
     * @return Resources[]
     */
    public function getSystemModules($refresh = true)
    {
        $cacheKey = "module_resource_list_for_system_" . HOST_KEY;
        $data = $this->cache->get($cacheKey);
        if (!$data || $refresh) {
            $data = [];

            $modules = Resources::find("type='m'");
            if($modules) {
                foreach($modules as $module) {
                    $hostModule = ResourceForHosts::findFirst("host_key='" . HOST_KEY . "' AND resource_id='{$module->id}'");
                    if($hostModule) {
                        if(intval($hostModule->is_free)) {
                            $module->is_free = 1;
                        }
                        else {
                            $module->is_free = 0;
                        }
                        if(intval($hostModule->is_active)) {
                            $module->is_active = 1;
                        }
                        else {
                            $module->is_active = 0;
                        }

                        foreach($hostModule->toArray() as $key => $val) {
                            if($key != 'id' && $key != 'is_free' && $key != "is_active"&& (!is_null($val) && strlen($val) > 0)) {
                                $module->{$key} = $val;
                            }
                        }
                    }
                    else {
                        $module->is_active = 0;
                        $module->is_free = 0;
                    }
//                    if($module->is_active) {
                        $data[] = $module;
//                    }
                }
            }
            $this->cache->save($cacheKey, $data);
        }
        return $data;
    }

    /**
     * @param $name
     * @return int
     */
    public function getCustomerModuleDeadLine($name)
    {
        $modules = $this->getCustomerModules();
        foreach ($modules as $item) {
            if ($item->module_name == $name) {
                return $item->time_end;
            }
        }
        return time() - 86400;
    }

    public function getCustomerModuleInfo($name)
    {
        $modules = $this->getCustomerModules(CUR_APP_ID, true);
        foreach ($modules as $item) {
            if ($item->module_name == $name) {
                $item = $item->toArray();
                $customer_module = ResourceForCustomers::findFirst('customer_id=' . $this->customer . ' and resource_id=' . $item['id']);
                if (!$customer_module) {
                    return false;
                }
                $data = $customer_module->toArray();
                $data['resource'] = $item;
                return $data;
            }
        }
        return false;
    }

    /**
     * 获取
     * @param int $customer
     * @return ModuleRender[]
     */
    public function getModuleRenders($customer)
    {
        $modules = $this->getCustomerModules($customer);

    }

    /**
     * @param string $name
     * @return bool
     */
    public function register($name)
    {
        return true;
    }

    /**
     * @param string $module
     * @param int $customer
     * @return bool | ModuleConfig
     */
    public function checkAuth($module, $customer = null)
    {
        if(empty($customer)) {
            $customer = $this->customer;
        }
        $moduleModel = $this->getModuleResource($module);
        if(intval($moduleModel->is_free) == 1) {
            $config = new ModuleConfig($module);
            return $config;
        }
        if($customer > 0) {
            $moduleForCustomer = ResourceForCustomers::findFirst("customer_id='{$customer}' AND resource_id='{$moduleModel->id}'");
        }
        else {
            return false;
        }

        //模块权限控制已经转出
//        if($moduleForCustomer && $moduleForCustomer->time_end > time()) {
            $config = new ModuleConfig($module);
            return $config;
//        }
//        return false;
    }

    /**
     * @param string $module
     * @param int $customer
     * @return bool | ModuleConfig
     */
    public function checkEndTime($module, $customer = null)
    {
        $modules = $this->getCustomerModules($customer);
        foreach ($modules as $item) {
            if ($item->module_name == $module) {
                $end = $this->getCustomerModuleDeadLine($module);
                return $end;
            }
        }
        return false;
    }


    public function orderNumberGenerator($type = self::ORDER_NUMBER_PREFIX_AUTO)
    {
        if ($type != self::ORDER_NUMBER_PREFIX_AUTO) {
            $type = self::ORDER_NUMBER_PREFIX_HANDLE;
        }
        return $type . date("YmdHis") . "_" . strtoupper(substr(sha1(uniqid(microtime())), 0, 6));
    }
}

?>
