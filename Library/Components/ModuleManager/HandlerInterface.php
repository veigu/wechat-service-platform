<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-5-6
 * Time: 上午9:38
 */

namespace Components\ModuleManager;

interface HandlerInterface
{
    /**
     * @return HandlerInterface
     */
    public function initialize();

    /**
     * @param ModuleConfig $config
     * @return HandlerInterface
     */
    public function setConfig(ModuleConfig $config);

    /**
     * @return string
     */
    public function getModuleName();

    /**
     * @return ModuleConfig
     */
    public function getConfig();

    /**
     * @param bool $isAuth
     * @return mixed
     */
    public function setAuth($isAuth);

    /**
     * @return bool
     */
    public function isAuthed();
}

?>