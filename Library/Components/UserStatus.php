<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 6/17/14
 * Time: 2:24 PM
 */

namespace Components;

use Components\WeChat\MessageManager;
use Models\User\UserForCustomers;
use Models\User\Users;
use Models\User\UserSettings;
use Models\User\UsersWechat;
use Models\User\UsersWeibo;
use Phalcon\Mvc\User\Component;
use Util\Ajax;
use Util\Cookie;
use Util\Validator;

abstract class UserStatus extends Component
{
    const COOKIE_UID = '_APP_CUID';
    const COOKIE_USR = '_APP_CUSR';
    private  static  $instance=null;

    public static function init()
    {
    }

    abstract public function checkLogin();

    // login
    abstract public function ajaxLogin();

    // reg
    abstract public function ajaxReg();

    // 检查电话
    public function checkPhone($phone)
    {
        if (!Validator::validateCellPhone($phone)) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        $user = Users::findFirst('host_key="' . HOST_KEY . 'and phone="' . $phone . '"');
        if ($user) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_HAS_BEING_USED);
        }

        return Ajax::init()->outRight();
    }

    // 检查邮箱
    public function checkEmail($email)
    {
        if (!Validator::validEmail($email)) {
            return Ajax::init()->outError(Ajax::ERROR_EMAIL_IS_INVALID);
        }
        $user = Users::findFirst('host_key="' . HOST_KEY . 'and email="' . $email . '"');
        if ($user) {
            return Ajax::init()->outError(Ajax::ERROR_EMAIL_HAS_BEING_USED);
        }

        return Ajax::init()->outRight();
    }

    // 昵称检查
    public function checkNick($nickname)
    {
        if (!Validator::validateNick($nickname)) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_INVALID);
        }

        $user = Users::findFirst('host_key="' . HOST_KEY . 'and username="' . $nickname . '"');
        if ($user) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_BEING_USED);
        }

        return Ajax::init()->outRight();
    }

    // 绑定号码
    public function bindPhone($phone)
    {
        $uid = $this->getUid();
        if (!$uid) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }

        if (!Validator::validateCellPhone($phone)) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        if (Users::count('host_key="' . HOST_KEY . 'and phone="' . $phone . '" and id <> ' . $uid) > 0) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_HAS_BEING_USED);
        }

        // 更新数据
        $user = Users::findFirst('id=' . $uid);
        if (!$user->update(array('phone' => $phone))) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '更新号码失败！');
        }

        return Ajax::init()->outRight();
    }

    // 绑定邮箱
    public function bindEmail($email)
    {
        $uid = $this->getUid();
        if (!$uid) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }

        if (!Validator::validEmail($email)) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        if (Users::count('host_key="' . HOST_KEY . 'and email="' . $email . '" and id <> ' . $uid) > 0) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_HAS_BEING_USED);
        }

        // 更新数据
        $user = Users::findFirst('id=' . $uid);
        if (!$user->update(array('email' => $email))) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '更新邮箱失败！');
        }

        return Ajax::init()->outRight();
    }

    // 个人资料
    public function saveProfile()
    {
        if (!Cookie::exists(self::COOKIE_UID)) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN, '');
        }
        $user_id = Cookie::get(self::COOKIE_UID);

        $username = $this->request->getPost('username', array('string', 'striptags'));
        $email = $this->request->getPost('email');
        $gender = $this->request->getPost('gender');
        $province = $this->request->getPost('province');
        $city = $this->request->getPost('city');
        $town = $this->request->getPost('town');

        $data = array(
            'username' => $username,
            'email' => $email,
            'gender' => $gender,
            'province' => $province,
            'city' => $city,
            'town' => $town,
            'address' => '',
        );

        if (!$username) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_INVALID);
        }

        // 验证邮箱
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID);
        }

        if (Users::findFirst('host_key="' . HOST_KEY . '" and email = "' . $email . '" and id != ' . $user_id)) {
            return Ajax::init()->outError(Ajax::ERROR_EMAIL_HAS_BEING_USED);
        };

        $user = Users::findFirst('id=' . $user_id);
        if (!$user->save($data)) {
            foreach ($user->getMessages() as $message) {
                $msg[] = (string)$message;
            }
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, join(',', $msg));
        }

        return Ajax::init()->outRight('', '');
    }

    // 重置密码
    public function resetPass()
    {
        $oldPass = $this->request->getPost('oldPass');
        $pass = $this->request->getPost('pass');

        if (!($pass)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        if (!($oldPass && $pass)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if ((strlen($oldPass) < 6)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID, '请检查原始密码');
        }

        if ((strlen($pass) < 6)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID, '请检查新密码');
        }

        $user = Users::findFirst('id = ' . self::getUid());
        if (!$user) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_NOT_EXISTS);
        }

        if ($user->password != md5($oldPass)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_NOT_CORRECT, '原始密码不正确');
        }

        $user->password = md5($pass);

        if (!$user->update()) {
            foreach ($user->getMessages() as $message) {
                $msg [] = $message;
            }

            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '更新密码失败');
        }

        return Ajax::init()->outRight();
    }

    // 设置密码
    public function setPass()
    {
        $pass = $this->request->getPost('pass');

        if ((strlen($pass) < 6)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID, '请检查新密码');
        }

        $user = Users::findFirst('id = ' . self::getUid());
        if (!$user) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_NOT_EXISTS);
        }

        $user->password = md5($pass);
        if (!$user->update()) {
            foreach ($user->getMessages() as $message) {
                $msg [] = $message;
            }
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '设置密码失败');
        }

        return Ajax::init()->outRight();
    }

    // 获取用户信息
    public static function getUserInfo()
    {
        if (!Cookie::exists(self::COOKIE_UID)) {
            return false;
        }
        $user = Users::findFirst('id = ' . self::getUid() . ' and host_key="' . HOST_KEY . '"');
        return $user ? $user->toArray() : '';
    }

    // logout
    public static function logout()
    {
        Cookie::del(self::COOKIE_UID);
        Cookie::del(self::COOKIE_USR);
    }

    // 是否登陆
    public static function isLogged()
    {
        if (Cookie::exists(self::COOKIE_UID)) {
            return true;
        }
        return false;
    }

    // 用户id
    public static function getUid()
    {
        if (!Cookie::exists(self::COOKIE_UID)) {
            return false;
        }
        $uid = Cookie::get(self::COOKIE_UID);

        //防止用户已经在商家后台删除了，却能在前端继续使用
        $user = Users::findFirst($uid);
        if (!$user) {
            return false;
        }
        return $uid;
    }

    // cookie登陆
    protected function saveStatus($uid, $user)
    {
        Cookie::set(self::COOKIE_UID, $uid, Cookie::OneDay);
        Cookie::set(self::COOKIE_USR, $user, Cookie::OneDay);
    }

    // 绑定用户到当前商家
    protected function bindCustomerUser($uid, $platform = null, $openId = null)
    {
        $setting = UserSettings::findFirst("customer_id=" .CUR_APP_ID ."");
        $is_need_check = false;
        if($setting && $setting->need_check > 0){
            $is_need_check = true;
        }

        $cu = new UserForCustomers();
        $count = $cu->count('customer_id=' . CUR_APP_ID . ' and user_id=' . $uid);

        if ($count == 0) {
            $data = array(
                'id' => null,
                'host_key' => HOST_KEY,
                'customer_id' => CUR_APP_ID,
                'user_id' => $uid,
                'active' => $is_need_check ? 0 : 1
            );
            if (!$cu->create($data)) {
                $this->di->get("errorLogger")->error(json_encode($cu->getMessages()));
                return false;
            }
        }

        $this->bindThirdPartyUser($uid, $platform, $openId);

        if (empty($cu->wx_open_id)) {
            $this->wechat_binded = false;
        } else {
            $this->wechat_binded = true;
        }

        if (empty($cu->wb_uid)) {
            $this->weibo_binded = false;
        } else {
            $this->weibo_binded = true;
        }

        return true;
    }

    // 绑定微博微信
    private function bindThirdPartyUser($uid, $platform, $openId)
    {
        if ($platform && $platform == MessageManager::PLATFORM_TYPE_WEIXIN && $openId) {
            $userWechat = UsersWechat::findFirst("open_id='{$openId}'");
            if ($userWechat) {
                if (!$userWechat->update(array('user_id' => $uid, 'is_binded' => 1))) {
                    $this->di->get('errorLogger')->error(json_encode($userWechat->getMessages()));
                    return false;
                }
            }
        }
        if ($platform && $platform == MessageManager::PLATFORM_TYPE_WEIBO && $openId) {
            $userWeibo = UsersWeibo::findFirst("uid='{$openId}'");
            if ($userWeibo) {
                if (!$userWeibo->update(array('user_id' => $uid, 'is_binded' => 1))) {
                    $this->di->get('errorLogger')->error(json_encode($userWeibo->getMessages()));
                    return false;
                }
            }
        }
        return true;
    }

    // 来自微博
    public function isWeibo()
    {
        $userAgent = strtolower($this->request->getServer("HTTP_USER_AGENT"));
        if (strpos($userAgent, 'weibo') !== false) {
            return true;
        }
        return false;
    }

    // 是否来自微信
    public function isWechat()
    {
        $userAgent = strtolower($this->request->getServer("HTTP_USER_AGENT"));
        if (strpos($userAgent, 'micromessenger') !== false) {
            return true;
        }
        return false;
    }

    // 发生验证码
    public function sendVerifyCode($phone, $check_unique)
    {
        if (!Validator::validateCellPhone($phone)) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        if ($check_unique === 'true') {
            if (!Validator::validateCellPhone($phone)) {
                return false;
            }
            $user = Users::findFirst('phone="' . $phone . '"');
            if ($user) {
                return Ajax::init()->outError(Ajax::ERROR_PHONE_HAS_BEING_USED);
            }
        }

        $timeStart = Cookie::get("verify_code_send_start");
        if (!$timeStart) {
            $timeStart = time();
            Cookie::set("verify_code_send_start", $timeStart, 3600);
        }

        $totalSends = Cookie::get("verify_code_send_count");
        if (!$totalSends) {
            $totalSends = 1;
        } else {
            $totalSends += 1;
        }
        if ($totalSends > 8) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您请求过于频繁，请稍后再试！');
        }

        Cookie::set("verify_code_send_count", $totalSends, 3600);
        $lastSendTime = Cookie::get("verify_code_send_last");
        if ($lastSendTime && time() - $lastSendTime < 60) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '一分钟内只能请求一次');
        }

        Cookie::set("verify_code_send_last", time(), 3600);
        $code = rand(100000, 999999);
        Cookie::set("verify_code_send_number", $code, 3600);
        Cookie::set("verify_code_send_phone", $phone, 3600);
        $sender = $this->getDI()->get("messenger");
        $result = $sender->send($phone, "您已获得用户操作验证码，如果您没有请求这项操作，请您忽略此信息。验证有时间限制，请您及时使用： " . $code);
        if ($result['result'] == 1) {
            return Ajax::init()->outRight('发送成功', array('_rs_' => $result));
        }

        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, array('_rs_' => $result));
    }

    // 验证验证码
    public function checkVerifyCode($code)
    {
        if (empty($code) || strlen($code) != 6) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您没有正确填写验证码，系统验证码为6位数字');
        }

        $savedCode = Cookie::get("verify_code_send_number");
        if (empty($savedCode)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您还没有获取验证码');
        }

        if ($code != $savedCode) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '验证码不正确，请重新填写');
        }

        // 删除cookie
        Cookie::del("verify_code_send_start");
        Cookie::del("verify_code_send_count");
        Cookie::del("verify_code_send_last");
        Cookie::del("verify_code_send_number");
        Cookie::del("verify_code_send_phone");

        return Ajax::init()->outRight('恭喜您，验证通过');
    }


    public function resetPassByPhone($phone, $pass)
    {
        if (!$phone || !Validator::validateCellPhone($phone)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (!$pass || !Validator::validPassword($pass)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $savedPhone = Cookie::get('verify_code_send_phone');
        if ($savedPhone != $phone) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        // 删除cookie
        Cookie::del("verify_code_send_start");
        Cookie::del("verify_code_send_count");
        Cookie::del("verify_code_send_last");
        Cookie::del("verify_code_send_number");
        Cookie::del("verify_code_send_phone");

        $user = Users::findFirst('host_key="' . HOST_KEY . '" and phone="' . $phone . '"');
        if (!$user) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_NOT_EXISTS);
        }

        // 如果邮箱不存在，则保存到当前用户
        $user->password = md5($pass);
        if (!$user->update()) {
            foreach ($user->getMessages() as $message) {
                $msg[] = $message;
            };
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, $msg);
        }

        return Ajax::init()->outRight('密码修改成功！');
    }
}
