<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 8/5/14
 * Time: 10:45 AM
 */

namespace Components\Module;


use Models\Modules\Demand\AddonDemandReply;
use Models\Modules\Demand\AddonDemandType;
use Phalcon\Mvc\User\Plugin;

class DemandManager extends Plugin
{
    const DEMAND_STATUS_DEFAULT = 0;
    const DEMAND_STATUS_WAIT = 1;
    const DEMAND_STATUS_REPLAYED = 2;
    const DEMAND_STATUS_END = -1;

    public static $status_map = array(
        self::DEMAND_STATUS_DEFAULT => '尚未处理',
        self::DEMAND_STATUS_WAIT => '等待回复',
        self::DEMAND_STATUS_REPLAYED => '已经回复',
        self::DEMAND_STATUS_END => '已经结束',
    );

    public static function init()
    {
        return new self();
    }

    public function getType($type_id = null)
    {
        $types = AddonDemandType::find('customer_id=' . CUR_APP_ID)->toArray();
        $type_name = $types ? array_column($types, 'name', 'id') : array();
        if ($type_id && $types) {
            return isset($type_name[$type_id]) ? $type_name[$type_id] : '';
        }
        return $type_name;
    }

    public static function getStatus($status = null)
    {
        if (is_numeric($status)) {
            return isset(self::$status_map[$status]) ? self::$status_map[$status] : '未知';
        }

        return self::$status_map;
    }

    public static function getDemandReplies($demand_id)
    {
        $where = 'demand_id=' . $demand_id;
        return AddonDemandReply::find(array($where, 'order' => 'created asc', 'limit' => 30))->toArray();
    }
}