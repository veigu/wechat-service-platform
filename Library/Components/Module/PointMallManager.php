<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 2014/12/1
 * Time: 15:33
 */

namespace Library\Components\Module;


use Components\Product\ProductManager;
use Components\Product\TransactionManager;
use Components\UserStatus;
use Models\Modules\Pointmall\AddonPointMallItem;
use Models\User\UserAddress;
use Phalcon\Mvc\User\Component;

class PointMallManager extends Component
{
    public static function init()
    {
        return new self();
    }

    public function getSettleData()
    {
        $user_id = UserStatus::getUid();
        $point_item = AddonPointMallItem::findFirst('customer_id = ' . CUR_APP_ID . ' and item_id = ' . $cart['id']);
        if ($point_item) {
            $point_item = $point_item->toArray();
            $point_item['num'] = $cart['num'];
            if ($cart['spec']) {
                $point_item['spec_data'] = ProductManager::instance()->getProductSpecData($cart['id'], $cart['spec'], 'spec_data');
            }
            $point_item['subtotal'] = $point_item['cost_cash'] * $cart['num'];
        }

        $address = UserAddress::find('user_id=' . $user_id);
        $default_address = UserAddress::findFirst('is_default = 1 and user_id=' . $user_id);

        // 邮费
        $postage = TransactionManager::instance(HOST_KEY)->getCustomerLogistics(CUR_APP_ID);
    }
} 