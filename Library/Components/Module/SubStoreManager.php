<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/12
 * Time: 15:55
 */

namespace Components\Module;

use Phalcon\Mvc\User\Component;

class SubStoreManager extends Component{
    /**
     * @var SubStoreManager
     */
    private static $instance = null;

    private function __construct() {

    }

    /**
     * @return SubStoreManager
     */
    public static function getInstance() {
        if(!self::$instance instanceof SubStoreManager) {
            self::$instance = new self();
        }
        return self::$instance;
    }


} 