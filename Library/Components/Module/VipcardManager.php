<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 8/15/14
 * Time: 11:24 AM
 */

namespace Components\Module;

use Components\Rules\PointRule;
use Models\Modules\Vipcard\AddonVipcard;
use Models\Modules\Vipcard\AddonVipcardGrade;
use Models\Modules\Vipcard\AddonVipcardSettings;
use Models\Modules\Vipcard\AddonVipcardUsers;
use Models\User\UserForCustomers;
use Models\User\UserPointLog;
use Multiple\Wap\Helper\EmoiUser;
use Components\UserStatus;
use Phalcon\Mvc\User\Component;
use Util\Ajax;

class VipcardManager extends Component
{
    const CARD_RECEIVE_AUTO = 0; //默认卡号
    const CARD_RECEIVE_MANUAL = 1; // 手动卡号
    const AUTO_SEND=1;//默认发放,不需要审核
    const AUTO_SEND_NULL=0;//手动发放,需要审核
    static $instance = null;
    /**
     * @var AddonVipcard
     */
    protected $card_settings = null;

    protected $isActive = false;

    protected $auto_send = true;

    /**
     * 会员卡号生成方式 true 自动生成，false 手动生成
     * @var bool
     */
    protected $card_received = true;

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        self::$instance->card_settings = AddonVipcard::findFirst("customer_id = " . CUR_APP_ID);
        if(!self::$instance->card_settings instanceof AddonVipcard) {
            self::$instance->isActive = false;
        }
        else {
            self::$instance->isActive = true;
        }

        if(self::$instance->isActive && self::$instance->card_settings->auto_send == 1) {
            self::$instance->auto_send = true;
        }
        else {
            self::$instance->auto_send = false;
        }

        if(self::$instance->isActive && self::$instance->card_settings->card_receive == self::CARD_RECEIVE_AUTO) {
            self::$instance->card_received = true;
        }
        else {
            self::$instance->card_receive = false;
        }

        return self::$instance;
    }

    /**
     *       检查号
     * @param $user
     * @param $customer_id
     * @return bool|\Phalcon\Http\ResponseInterface
     */
    public static function checkNeedBindCard($user, $customer_id)
    {
        // 1     先检查是否有卡
        if (self::getCardInfoByUid($user, $customer_id, 1)) {
            return false;
        }
        // 2   检查商家是否设置卡（自动发放，需要审核）
        if (!(self::getCustomerSetting($customer_id, 'auto_send') == 1)) {
            //  商家未设置或需要审核
            return false;
        }
        return true;
    }

    public function addGrade($data)
    {
        $old = AddonVipcardGrade::findFirst(array('customer_id=' . CUR_APP_ID, 'order' => 'grade desc'));
        if ($old) {
            $old = $old->toArray();
            if ($data['amount_end'] < $old['amount_end']) return -1;

            $data['grade'] = $old['grade'] + 1;
            $data['amount_start'] = empty($old['amount_end']) ? 0 : $old['amount_end'] + 1;
        } else {
            $data['grade'] = 1;
            $data['amount_start'] = 0;
        }
        $grade = new AddonVipcardGrade();
        return $grade->create($data);
    }

    /**
     * @param $grade
     * @param $custoer_id
     * @param string $field
     * @return \Phalcon\Mvc\Model|\Phalcon\Mvc\Model\Resultset|string
     */
    public static function getCardGradeInfo($grade, $custoer_id, $field = '')
    {
        $grade_info = AddonVipcardGrade::findFirst('customer_id=' . $custoer_id . ' and grade=' . $grade);
        if (!$grade_info) {
            return '';
        }
        if (!empty($field)) {
            return isset($grade_info->$field) ? $grade_info->$field : '';
        }
        return $grade_info;
    }

    public static function getAllGrade($customer_id)
    {
        $grade = AddonVipcardGrade::find(array('customer_id=' . $customer_id, 'group' => 'grade'));
        return $grade ? $grade->toArray() : [];
    }

    /**
     *
     * @param $customer_id
     * @param string $field
     * @return array|bool|\Phalcon\Mvc\Model|string
     */
    public static function getCustomerSetting($customer_id, $field = '')
    {
        $setting = AddonVipcardSettings::findFirst('customer_id=' . $customer_id);
        if (!$setting) {
            return false;
        }
        $setting = $setting->toArray();
        if ($field) {
            return isset($setting[$field]) ? $setting[$field] : '';
        }
        return $setting;
    }

    public static function getCustomerCard($customer_id, $field = '')
    {
        $card = AddonVipcard::findFirst('customer_id=' . $customer_id);
        if (!$card) {
            return false;
        }
        $card = $card->toArray();
        if ($field) {
            return isset($card[$field]) ? $card[$field] : '';
        }
        return $card;
    }

    /**
     * @param $user_id
     * @param $customer_id
     * @return int|\Phalcon\Mvc\Model|\Phalcon\Mvc\Model\Resultset|string
     */
    public static function getCardDiscount($user_id, $customer_id)
    {
        $info = self::getCardInfoByUid($user_id, $customer_id);
        if ($info && $info['is_company'] == 1) {
            return self::getCardGradeInfo($info['card_grade'], CUR_APP_ID, 'discount');
        }
        return 100;
    }

    /**
     * 用户申请接口(wap)
     */
    public function userApply($data)
    {
//       // Debug::log("CARD:-start:data:" . var_export($data));

        $uid = UserStatus::getUid();
        if (!$uid) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }

        $card = VipcardManager::getCustomerCard(CUR_APP_ID);
        if (!$card) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '商家尚未设置会员卡!');
        }

        $has_card = AddonVipcardUsers::findFirst('user_id=' . $uid . ' and customer_id=' . CUR_APP_ID);
        if (isset($has_card->is_active) && $has_card->is_active == 1) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您申请的会员卡已经激活!');
        }

        // 自动获取卡号
        $data['card_no'] = self::generalCardNumber(CUR_APP_ID);

        // todo  获取积分
        $data['point'] = PointRule::init(CUR_APP_ID, 0)->getRulePoints(PointRule::BEHAVIOR_REGISTER, 0);

        // 其他信息
        $data['customer_id'] = CUR_APP_ID;
        $data['user_id'] = $uid;
        $data['balance'] = 0;
        $data['created'] = time();
        //会员卡设置
        $vipcard = AddonVipcardSettings::findFirst("customer_id=" . CUR_APP_ID . " and enable=1");
        // 自动发放会员卡
        if ($card['auto_send'] == self::AUTO_SEND) {
            $data['is_active'] = 1; // 已经激活
        }

        if ($card['auto_send'] == self::AUTO_SEND_NULL) {
            $data['is_active'] = 0; // 尚未激活
        }

        if ($has_card) {

            if (!$has_card->update($data)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '数据更新失败!');
            }
        } else {

            $user_cus = UserForCustomers::findFirst('user_id=' . $uid . '  and customer_id=' . CUR_APP_ID);
            $user_new = $user_cus;
            $user_cus = $user_cus ? $user_cus->toArray() : [];
            $cus_points_available = isset($user_cus['points_available']) ? $user_cus['points_available'] : 0;
            $grade = VipcardManager::init()->getGradeByPoint($cus_points_available, CUR_APP_ID);
            if (!$grade) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您的积分不足,还不满足申请会员卡的条件!');

            }
            $data['card_grade'] = $grade->grade;
            $data['card_grade_name'] = $grade->name;
            $card_user = new AddonVipcardUsers();
//           // Debug::log("CARD:-save:data:" . var_export($data) . ':time:' . date("Y-m-d H:i:s"));
            if (!$card_user->create($data)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '数据插入失败!');
            }

            //当会员卡不需要审核,绑定就送积分
            if ($card && $card['auto_send'] == 1) {


                //获取绑定会员卡应该赠送的积分
                $vipgrade = VipcardManager::getUserVipGrade($uid, CUR_APP_ID);
                PointRule::init(CUR_APP_ID, $vipgrade)->executeRule($uid, PointRule::BEHAVIOR_BIND_VIPCARD);
              //  $points = PointRule::init(CUR_APP_ID, 0)->getRulePoints(PointRule::BEHAVIOR_BIND_VIPCARD, 0);
              //  return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $points);
            }


        }
        // 初始绑定日志
        if (!$has_card && $card && $card['auto_send'] == 1) {

            // Debug::log("CARD:-save:point:" . PointRule::BEHAVIOR_BIND_VIPCARD);

            // todo 绑定日志
           // $vip_grade = VipcardManager::getUserVipGrade($uid, CUR_APP_ID);
        //    PointRule::init(CUR_APP_ID, $vip_grade)->executeRule($uid, PointRule::BEHAVIOR_BIND_VIPCARD, 0);
        }

        return true;
    }

    /**
     * 生成卡号
     */
    public static function generalCardNumber($customer_id)
    {
        $randStr1 = str_shuffle('123456789');
        $randStr2 = str_shuffle('0123456789');
        $randStr3 = str_shuffle(time());

        $rand = substr($randStr1, 0, 4) . substr($randStr2, 0, 4) . substr($randStr3, 0, 4);

        if (AddonVipcardUsers::count('customer_id=' . $customer_id . ' and card_no="' . $rand . '"')) {
            return self::generalCardNumber($customer_id);
        }

        return $rand;
    }

    public static function getCardInfoByUid($uid, $customer_id, $is_active = 1, $field = '')
    {
        $active = $is_active == 1 ? '  and is_active=1' : '';
        $card_user = AddonVipcardUsers::findFirst('user_id=' . $uid . ' and customer_id=' . $customer_id . $active);
        if ($card_user) {
            $card = $card_user->toArray();
            return $field && isset($card[$field]) ? $card[$field] : $card;
        }
        return false;
    }

    public static function getUserVipGrade($uid, $customer_id)
    {
        return intval(self::getCardInfoByUid($uid, $customer_id, 1, 'card_grade'));
    }

    public static function updateInfo($uid, $customer_id, $data)
    {
        $card_user = AddonVipcardUsers::findFirst('user_id=' . $uid . ' and customer_id=' . $customer_id);
        if (!$card_user->update($data)) {
            return false;
        }
        return true;
    }

    public static function getCardInfoByPhone($phone, $customer_id)
    {
        $card_user = AddonVipcardUsers::findFirst('phone=' . $phone . ' and customer_id=' . $customer_id);
        if ($card_user) {
            return $card_user->toArray();
        }
        return false;
    }

    public static function getCardInfoByCardNo($card_no, $customer_id)
    {
        $card_user = AddonVipcardUsers::findFirst('card_no="' . $card_no . '" and customer_id=' . $customer_id);
        if ($card_user) {
            return $card_user->toArray();
        }
        return false;
    }

    public function checkSignin()
    {

    }

    /**
     * get records
     *
     * @param $user_id
     * @param $customer_id
     * @param int $page
     * @param int $limit
     * @return bool|mixed
     */
    public static function getPointLog($user_id, $customer_id, $page = 0, $limit = 20)
    {
        $page = 0;
        $limit = $page * $limit . ',' . $limit;
        $log = UserPointLog::find(array('customer_id=' . $customer_id . ' and user_id=' . $user_id, 'order' => 'logged desc', 'limit' => $limit));
        return $log ? $log->toArray() : $log;
    }

    public static function getCheckInCount($user_id, $customer)
    {
        $where = 'customer_id=' . $customer . ' and user_id=' . $user_id . ' and action="' . PointRule::BEHAVIOR_VIPCARD_CHECK_IN . '"';
        $count = UserPointLog::sum(array($where, 'column' => 'value'));
        return $count;
    }

    /**
     * 是否已签到
     * @param $user_id
     * @param $customer
     * @return bool
     */
    public static function isCheckIn($user_id, $customer)
    {
        $today = strtotime(date('Y-m-d') . ' 00:00:00', time());
        $where = 'customer_id=' . $customer . ' and user_id=' . $user_id . ' and action="' . PointRule::BEHAVIOR_VIPCARD_CHECK_IN . '" and logged>' . $today;
        $count = UserPointLog::count($where);
        return $count;
    }

    /*
     * 通过积分获取会员卡等级
     * */
    public static function  getGradeByPoint($point, $customer)
    {
        return AddonVipcardGrade::findFirst("customer_id=" . $customer . " and amount_start<=" . $point . " and amount_end>=" . $point);
    }

} 