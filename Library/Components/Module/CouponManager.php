<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 8/6/14
 * Time: 9:23 AM
 */

namespace Components\Module;


use Components\UserStatus;
use Models\Modules\Coupon\AddonCoupon;
use Models\Modules\Coupon\AddonCouponOfflineLog;
use Models\Modules\Coupon\AddonCouponOrderLog;
use Models\Modules\Coupon\AddonCouponReceive;
use Models\Modules\Coupon\AddonCouponSettings;
use Phalcon\Mvc\User\Plugin;
use Util\Ajax;
use Util\EasyEncrypt;
use Util\QRcode;
use Util\Uri;

class CouponManager extends Plugin
{
    // 状态
    const COUPON_STATUS_RECEIVE_NOT_START = 1001;
    const COUPON_STATUS_RECEIVE_IS_END = 1002;
    const COUPON_STATUS_USE_NOT_START = 2002;
    const COUPON_STATUS_USE_IS_END = 2002;

    // 类型
    const COUPON_TYPE_CASH = 101;
    const COUPON_TYPE_DISCOUNT = 102;
    const COUPON_TYPE_SPECIAL = 103;

    static $count_type_name = array(
        self::COUPON_TYPE_CASH => '现金卷',
        self::COUPON_TYPE_DISCOUNT => '折扣卷',
        self::COUPON_TYPE_SPECIAL => '特价卷',
    );

    public static function getCouponType($type = null)
    {
        if (is_numeric($type)) {
            return isset(self::$count_type_name[$type]) ? self::$count_type_name[$type] : '';
        }
        return self::$count_type_name;
    }

    public static function init()
    {
        return new self();
    }

    public static function generateCouponSerial()
    {
        $randStr0 = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
        $randStr1 = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
        $randStr2 = str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');

        $rand = substr($randStr0, 0, 4) . '-' . substr($randStr1, 0, 5) . '-' . substr($randStr2, 0, 5);
        return $rand;
    }


    /**
     * 发放优惠劵
     */
    public function receiveCoupon($von_id)
    {
        $uid = UserStatus::getUid();
        if (!$uid) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }

        if (!$von_id) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        // 查找优惠劵
        $von = AddonCoupon::findFirst("customer_id = " . CUR_APP_ID . ' and published = 1 and deleted = 0 and id=' . $von_id);
        if (!$von) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '优惠劵不存在!');
        }

        if ($von->receive_end <= time()) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '优惠劵领取时间已经过期!');
        }

        if ($von->total <= $von->receive_count) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '优惠劵已发放完毕!');
        }

        // 查找是否领取过
        $count = AddonCouponReceive::count('customer_id=' . CUR_APP_ID . '  and user_id=' . $uid . ' and coupon_id=' . $von_id);
        if ($count) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '您已经领取过,不能重复领取!');
        }

        // 优惠码
        $serial_number = CouponManager::generateCouponSerial();

        $qrcode = self::generateCouponQRcode($serial_number);
        $data['serial_number'] = $serial_number;
        $data['customer_id'] = CUR_APP_ID;
        $data['coupon_id'] = $von_id;
        $data['user_id'] = $uid;
        $data['qrcode'] = $qrcode;
        $data['created'] = time();
        $data['used'] = 0;

        $receive = new AddonCouponReceive();
        if (!$receive->create($data)) {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, '数据插入失败!');
        }

        // 更新优惠劵发放数量
        $von->update(array('receive_count' => $von->receive_count + 1));

        return Ajax::init()->outRight('', EasyEncrypt::encode($receive->id));
    }

    public function generateCouponQRcode($serial_number)
    {
        $uri = new Uri();

        // 二维码
        $qrcode_path = '/uploads/coupon/' . CUR_APP_ID;
        $qrcode_path_full = ROOT . $qrcode_path;
        if (!is_dir($qrcode_path_full)) {
            @mkdir($qrcode_path_full, 0777, true);
        }
        $qrcode_url = $uri->wapModuleUrl('coupon/offline/' . $serial_number);
        $qrcode_img_name = $serial_number . '.png';
        $qrcode_img = ROOT . $qrcode_path . '/' . $qrcode_img_name;

        QRcode::png($qrcode_url, $qrcode_img);


        $qrcode_img_url = $qrcode_path . '/' . $qrcode_img_name;

        return $qrcode_img_url;
    }

    public function offlineUseConfirm($serial, $pass)
    {
        if (!$serial) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '优惠码不存在!');
        }

        if (!$serial) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '优惠劵确认密码不存在!');
        }

        $receive = AddonCouponReceive::findFirst('customer_id=' . CUR_APP_ID . ' and serial_number="' . $serial . '"');
        if (!$receive) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '发放的优惠卷不存在!');
        }

        if ($receive->used == 1) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '优惠劵已经被使用过了!');
        }

        $von = AddonCoupon::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $receive->coupon_id);
        if ($von->receive_end <= time()) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '优惠劵领取时间已经过期!');
        }

        // 验证密码
        $setting = AddonCouponSettings::findFirst('customer_id=' . CUR_APP_ID);
        if (!$setting) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '商家尚未设置密码,请到管理平台先设置!');
        }

        if ($setting->passwd != md5($pass)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '商家确认密码不正确!');
        }

        // 更新我的优惠劵使用状态
        if (!$receive->update(array('used' => 1))) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '更新优惠卷状态失败!');
        }

        // 添加使用日志
        $data['customer_id'] = CUR_APP_ID;
        $data['coupon_id'] = $receive->coupon_id;
        $data['user_id'] = $receive->user_id;
        $data['serial_number'] = $receive->serial_number;
        $data['created'] = time();

        $offline = new AddonCouponOfflineLog();
        $offline->create($data);

        // 更新优惠劵使用数量
        $von->update(array('use_count' => $von->use_count + 1));
        return Ajax::init()->outRight();
    }

    // 计算可以优惠券
    public static function getUsableCoupon($total_cash)
    {
        $res = AddonCouponReceive::query()->leftJoin('Models\Modules\Coupon\AddonCoupon', 'c.id = coupon_id', 'c')
            ->columns('serial_number,coupon_id,c.name,c.coupon_param,c.coupon_type,c.use_limit')
            ->where('used=0 and c.customer_id=' . CUR_APP_ID . ' and c.published=1 and c.deleted=0 and c.use_limit<' . $total_cash . ' and user_id=' . UserStatus::getUid())->execute();
        $list = [];
        $max_minus = 0;
        if ($res) {
            $list = $res->toArray();
            foreach ($list as &$v) {
                $minus = 0;
                $tip_str = "";
                if ($v['coupon_type'] == self::COUPON_TYPE_CASH) {
                    $tip_str = '订单满￥' . $v['use_limit'] . '元，减免￥' . $v['coupon_param'] . '元';
                    $minus = $v['coupon_param'];
                }

                if ($v['coupon_type'] == self::COUPON_TYPE_DISCOUNT) {
                    $tip_str = '订单满￥' . $v['use_limit'] . '元，打折' . $v['coupon_param'] . '%';
                    $minus = $total_cash * (100 - $v['coupon_param']) / 100;
                }

                if ($v['coupon_type'] == self::COUPON_TYPE_SPECIAL) {
                    $param = explode('|', $v['coupon_param']);
                    $tip_str = '现特价￥' . $param[0] . '元，可抵用￥' . $param[1] . '元';
                    $minus = $param[1] - $param[0];
                }

                // 避免减免值大于订单额
                if ($minus > $total_cash) {
                    $minus = $total_cash;
                }

                // 计算最大值
                if ($max_minus < $minus) {
                    $max_minus = $minus;
                };


                $v['coupon_minus'] = round($minus, 2);
                $v['coupon_tip'] = $tip_str;
            }
        }
        // 减免最大值
        return array('coupon' => $list, 'max_minus' => round($max_minus, 2));
    }


    /**
     * 获取优惠劵抵价
     *
     * @param $serial
     * @param $total_cash
     * @return array|bool|\Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function getCouponDiscountBySerial($serial, $total_cash)
    {
        $res = AddonCouponReceive::query()->leftJoin('Models\Modules\Coupon\AddonCoupon', 'c.id = coupon_id', 'c')
            ->columns('serial_number,coupon_id,c.name,c.coupon_param,c.coupon_type,c.use_limit')
            ->where('c.customer_id=' . CUR_APP_ID . ' and c.published=1 and serial_number="' . $serial . '" and c.deleted=0 and c.use_limit<' . $total_cash . ' and user_id=' . UserStatus::getUid())->execute();
        if (!$res) {
            return 0;
        }
        $minus = 0;
        $res = $res->toArray()[0];

        if ($res['coupon_type'] == self::COUPON_TYPE_CASH) {
            $minus = $res['coupon_param'];
        }

        if ($res['coupon_type'] == self::COUPON_TYPE_DISCOUNT) {
            $minus = $total_cash * (100 - $res['coupon_param']) / 100;
        }

        if ($res['coupon_type'] == self::COUPON_TYPE_SPECIAL) {
            $param = explode('|', $res['coupon_param']);
            $minus = $param[1] - $param[0];
        }

        return round($minus, 2);
    }

    /**
     * 生成优惠劵订单
     */
    public static function generateCouponOrder($coupon_serial, $order_number, $coupon_minus)
    {
        $coupon = AddonCouponReceive::findFirst('serial_number="' . $coupon_serial . '"');
        if (!$coupon) {
            return false;
        }

        // 更新状态为已经使用
        $coupon->update(array('used' => 1));

        $data['coupon_serial'] = $coupon_serial;
        $data['user_id'] = UserStatus::getUid();
        $data['coupon_id'] = $coupon->coupon_id;
        $data['amount'] = $coupon_minus;
        $data['order_number'] = $order_number;
        $data['created'] = time();
        $data['customer_id'] = CUR_APP_ID;

        $order = new AddonCouponOrderLog();
        return $order->create($data);
    }
}