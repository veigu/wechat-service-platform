<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-16
 * Time: 下午4:21
 */

namespace Components\Product;


use Models\Product\Distribution\ProductShareParam;
use Models\Product\Product;
use Models\Product\ProductAttr;
use Models\Product\ProductBrand;
use Models\Product\ProductCat;
use Models\Product\ProductCatFeature;
use Models\Product\ProductSpec;
use Models\Product\ProductWord;
use Phalcon\Cache\BackendInterface;
use Phalcon\Mvc\User\Plugin;
use Util\Ajax;

class ProductManager extends Plugin
{
    /**
     * @var BackendInterface
     */
    private $cache = null;

    /**
     * @var ProductManager
     */
    private static $instance = null;

    private function __construct()
    {
        $this->cache = $this->di->get('memcached');
    }

    public function saveWord(Array $words)
    {
        $attr_key = [];
        $cache = $this->cache->get('product_word_list');
        foreach ($words as $k => $word) {
            // 缓存里面
            $key = array_search($word, $cache);
            if ($key) {
                $attr_key[$k] = $key;
            } else {
                $res = ProductWord::findFirst('word="' . $word . '"');
                if ($res) {
                    $attr_key[$k] = $res->id;
                } else {
                    $pw = new ProductWord();
                    $pw->save(array('word' => $word));
                    $attr_key[$k] = $pw->id;
                }
            }
        }
        // save into cache
        $all = ProductWord::find();
        $this->cache->save('product_word_list', $all ? $all->toArray() : []);
        return $attr_key;
    }

    public function saveProduct($data_base, $data_attr, $data_spec, $product_id = null)
    {
        if (!$data_base) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM, '请填写完表单！');
        }

        $this->db->begin();
        try {
            if (!(is_numeric($product_id) && $product_id > 0)) {
                $data_base['modified'] = time();
                $data_base['created'] = time();
                $data_base['customer_id'] = CUR_APP_ID;

                $prod = new Product();
                if (!$prod->save($data_base)) {
                    $messages = [];
                    foreach ($prod->getMessages() as $m) {
                        $messages[] = (string)$m;
                    }
                    throw new \Phalcon\Exception("base data saving failed for product, messages: " . join(',', $messages));
                }
                $product_id = $prod->id;

            } else {
                $data_base['modified'] = time();
                $prod = Product::findFirst('id=' . $product_id);
                if (!$prod) {
                    throw new \Phalcon\Exception("product not found");
                }
                if (!$prod->update($data_base)) {
                    $messages = [];
                    foreach ($prod->getMessages() as $m) {
                        $messages[] = (string)$m;
                    }
                    throw new \Phalcon\Exception("base data updating failed for product, messages: " . join(',', $messages));
                }
            }

            // 推广分成
            $share_param = $data_base['share'];
            if (floatval($share_param['cash']) > 0) {
                $share_param['cash'] = floatval($share_param['cash']);
                $share_param['deadline'] = strtotime($share_param['deadline']);
                $share = ProductShareParam::findFirst("product_id=" . $product_id);
                if ($share) {
                    $share->save($share_param);
                } else {
                    $share_param['product_id'] = $product_id;
                    $share = new ProductShareParam();
                    $share->create($share_param);
                }
            }

            // 1. 商品属性
            $exists_attr = ProductAttr::find('product_id=' . $product_id);

            // 删除和更新属性
            if ($exists_attr) {
                foreach ($exists_attr as $old_attr) {
                    // 删除
                    if (!$old_attr->delete()) {
                        $messages = [];
                        foreach ($old_attr->getMessages() as $m) {
                            $messages[] = (string)$m;
                        }
                        throw new \Phalcon\Exception("delete product old attr failed for product, messages: " . join(',', $messages));
                    }
                }
            }

            // 添加属性
            if ($data_attr) {
                foreach ($data_attr as $attr) {
                    $attr['product_id'] = $product_id;
                    $attrModel = new ProductAttr();

                    if (!$attrModel->create($attr)) {
                        $messages = [];
                        foreach ($attrModel->getMessages() as $m) {
                            $messages[] = (string)$m;
                        }
                        throw new \Phalcon\Exception("add new product attr failed for product, messages: " . join(',', $messages));
                    }
                }
            }

            // 2. 商品规格
            $exists_spec = ProductSpec::find('product_id=' . $product_id);
            // 删除和更新规格
            if ($exists_spec) {
                foreach ($exists_spec as $old_spec) {
                    // 删除
                    if (!$old_spec->delete()) {
                        $messages = [];
                        foreach ($old_spec->getMessages() as $m) {
                            $messages[] = (string)$m;
                        }
                        throw new \Phalcon\Exception("delete old spec failed for product, messages: " . join(',', $messages));
                    }
                }
            }

            // 添加规格
            if ($data_spec) {
                foreach ($data_spec as $spec) {
                    $spec['product_id'] = $product_id;
                    $spec['spec_data'] = json_encode($spec['spec_data'], JSON_UNESCAPED_UNICODE);
                    $specModel = new ProductSpec();
                    if (!$specModel->create($spec)) {
                        $messages = [];
                        foreach ($specModel->getMessages() as $m) {
                            $messages[] = (string)$m;
                        }
                        throw new \Phalcon\Exception("add new spec failed failed for product, messages: " . join(',', $messages));
                    }
                }
            }

            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollback();
            $messages = $e->getMessage();
            $this->di->get('debugLogger')->info("product update failed. " . $messages);
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $messages);
        }

        return Ajax::init()->outRight('', $this->uri->baseUrl('/shop/product/edit?iid=' . $product_id));
    }

    /**
     * @return ProductManager
     */
    public static function instance()
    {
        if (!self::$instance instanceof ProductManager) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // 获取商品字段信息
    public function getItemInfo($product_id, $field)
    {
        if (!$product_id) {
            return [];
        }
        $prod = Product::findFirst('id=' . $product_id);
        if (!$prod) {
            return "";
        }
        return isset($prod->$field) ? $prod->$field : "";
    }

    public function getProduct($product_id, $customer_id)
    {
        if (!$product_id) {
            return [];
        }
        $prod = Product::findFirst('id=' . $product_id . ' and customer_id=' . $customer_id);
        return $prod ? $prod->toArray() : [];
    }

    public function getProductSpecData($product_id, $spec_key, $field = null)
    {
        if (!($product_id && $spec_key)) {
            return "";
        }
        // 线上
        $spec = ProductSpec::findFirst(array('product_id=' . $product_id . ' and spec_unique_key="' . $spec_key . '"'));

        if ($spec) {
            $spec = $spec->toArray();
            return $field && isset($spec[$field]) ? $spec[$field] : $spec;
        }
        return "";
    }

    // 获取子店
    public function getProductStoreBySpec($product_id, $spec_key)
    {
        $spec = ProductSpec::find(array('product_id=' . $product_id . ' and spec_unique_key="' . $spec_key . '"', 'columns' => 'store_id'));
        $data = [];
        if ($spec) {
            $spec = $spec->toArray();
            $data = array_values(array_column($spec, 'store_id'));
        }
        return $data;
    }

    // 获取所有有货的线下门店
    public function getProductStoreAllByCustomerID($customer_id)
    {
        $spec = ProductSpec::find(array('customer_id=' . $customer_id));
        $data = [];
        if ($spec) {
            $spec = $spec->toArray();
            $data = array_values(array_column($spec, 'store_id'));
        }
        return $data;
    }

    public function getProductShareParam($product_id)
    {
        if (!$product_id) {
            return "";
        }
        $share = ProductShareParam::findFirst('product_id=' . $product_id);
        return $share ? $share->toArray() : "";
    }

    public function getProductSpec($product_id)
    {
        if (!$product_id) {
            return [];
        }
        $specs = ProductSpec::find('product_id=' . $product_id);
        $data = [];
        if ($specs) {
            $specs = $specs->toArray();
            foreach ($specs as $spec) {
                $data[$spec['spec_unique_key']] = $spec;
            }
        }
        return $data;
    }

    public function getProductAttr($product_id)
    {
        if (!$product_id) {
            return [];
        }
        $attrs = ProductAttr::find('product_id=' . $product_id);
        $data = [];
        if ($attrs) {
            $attrs = $attrs->toArray();
            foreach ($attrs as $attr) {
                $data[$attr['attr_id']] = $attr;
            }
        }
        return $data;
    }

    public function getAllBrand($customer_id)
    {
        $all = ProductBrand::find('customer_id=' . $customer_id);
        return $all ? $all->toArray() : [];
    }

    public function getCatAttr($cid, $is_active = null)
    {
        if (!$cid) return [];

        $where = 'cid=' . $cid . ' and is_sale_prop=0 ';
        if (in_array($is_active, ["0", "1"])) {
            $where .= ' and is_active = ' . $is_active . ' ';
        }
        $attrs = ProductCatFeature::find(array($where, 'order' => 'sort desc,created asc'));
        return $attrs ? $attrs->toArray() : [];
    }

    public function getCatSpec($cid, $is_active = null)
    {
        if (!$cid) return [];

        $where = 'cid=' . $cid . ' and is_sale_prop=1 ';
        if (in_array($is_active, ["0", "1", true])) {
            $where .= '  and is_active = ' . $is_active . ' ';
        }
        $spec = ProductCatFeature::find(array($where, 'order' => 'sort desc,created asc'));
        return $spec ? $spec->toArray() : [];
    }

    public function getCats($customer_id, $refresh = false)
    {
        $data = $this->cache->get("customer_cat_list_" . $customer_id);
        if (!$data || $refresh) {
            $cats = ProductCat::find('customer_id = ' . $customer_id);
            $data = $cats ? $cats->toArray() : [];

            $this->cache->save("customer_cat_list_" . $customer_id, $data);
        }
        return $data;
    }

    public function getCat($cid, $customer_id)
    {
        $data = ProductCat::findFirst(array('id=' . $cid . ' and customer_id=' . $customer_id));
        return $data ? $data->toArray() : [];
    }

    /**
     * 可选的分类
     *
     * @param $customer_id
     * @param bool $refresh
     * @return array
     */
    public function getSelectableCats($customer_id, $refresh = true)
    {
        $cats = $this->getCats($customer_id, $refresh);
        $selectableCats = [];
        foreach ($cats as $cat) {
            if ($cat['parent_id'] == 0) {
                if (!$cat['is_parent'] == 1) {
                    $selectableCats[$cat['id']] = $cat['name'];
                }
            } else {
                $selectableCats[$cat['id']] = $cat['name'];
            }
        }
        return $selectableCats;
    }

    /*获取分类(一级及其子分类)*/
    public function getCategories($customer, $refresh = false)
    {
        $data = $this->cache->get("customer_categories_list_" . $customer);
        if (!$data || $refresh) {
            $topMenus = ProductCat::find(array("customer_id='{$customer}' AND parent_id='0'", 'order' => 'sort asc'));
            $topMenus = $topMenus ? $topMenus->toArray() : [];
            $data = array();
            if (count($topMenus) > 0) {
                foreach ($topMenus as $item) {
                    $subMenus = ProductCat::find(array("customer_id='{$customer}' AND parent_id=" . $item['id'], 'order' => 'sort asc'));
                    $item['subs'] = ($subMenus ? $subMenus->toArray() : []);
                    unset($subMenus);
                    $data[] = $item;
                }
                unset($topMenus);
            }
            $this->cache->save("customer_categories_list_" . $customer, $data);
        }
        return $data;
    }

    public function getSubCategoryById($customer, $cid)
    {
        $data = $this->cache->get("customer_categories_list_" . $customer);
        if (!$data) {
            $data = $this->getCategories($customer);
        }
        if ($data) {
            foreach ($data as $item) {
                if ($item['id'] == $cid) {
                    return $item;
                }
                if (count($item['subs']) > 0) {
                    foreach ($item['subs'] as $sub) {
                        if ($sub['id'] == $cid) {
                            return $sub;
                        }
                    }
                }
            }
        }

        return [];
    }

    /*获取某个分类的子分类 id包含自己
    return  array
  */
    public function getSubCategoryById2($customer, $cid)
    {
        $cat = array($cid);
        $res = ProductCat::find("customer_id=" . $customer . " AND parent_id=" . $cid);
        if ($res) {
            $res = $res->toArray();
            foreach ($res as $item) {
                array_push($cat, $item['id']);
            }
        }
        return $cat;
    }
}
