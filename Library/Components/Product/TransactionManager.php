<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-6-11
 * Time: 下午1:57
 */

namespace Components\Product;


use Models\Shop\Logistics;
use Models\Shop\LogisticsForCustomers;
use Models\Shop\Shop;
use Models\Shop\ShopOrderReturn;
use Phalcon\Mvc\User\Plugin;
use Models\Shop\FreightTpl;
use Models\Shop\FreightTplItems;


class TransactionManager extends Plugin
{
    /**
     * @var TransactionManager
     */
    private static $instance;

    /**
     * @var \Phalcon\Cache\Backend\Libmemcached
     */
    public $cache;

    /**
     * @var int
     */
    public $customer;

    /**
     * 订单状态定义
     */
    const ORDER_STATUS_WAIT_BUYER_PAY = 1000; //刚下单（如果是在线支付，表示待付款；如果是货到付款，表示待发货），等待用户付款
    const ORDER_STATUS_WAIT_SELLER_SEND_GOODS = 1001; //等待商家发货（在线支付用户已经(预)付款；或者是货到付款流程刚下单）
    const ORDER_STATUS_ESCOW_PAID = 1002; //在线付款的担保交易中用户已经预支付
    const ORDER_STATUS_TRADE_SUCCESS = 1003; //支付成功并到账，担保交易完成(用户已经确认到货)或者直接到账支付完成
    const ORDER_STATUS_WAIT_BUYER_CONFIRM_GOODS = 1004; //用户已经签收货物，担保交易等待用户确认到货
    const ORDER_STATUS_SELLER_DELIVERED = 1005; //商家已经发货等待用户签收
    const ORDER_STATUS_BUYER_CANCELED = 1007; //用户取消订单
    const ORDER_STATUS_BUYER_RETURNED_GOODS = 1008; //用户申请退货
    const ORDER_STATUS_BUYER_CHANGED_GOODS = 1009; //用户申请换货


    /**
     * 用户退货流程状态定义
     */
    const RETURNED_STATUS_WAIT_SELLER_CONFIRM = 2000; //等待商家确认
    const RETURNED_STATUS_WAIT_BUYER_SEND_GOODS = 2001; //等待买家发货
    const RETURNED_STATUS_WAIT_SELLER_CONFIRM_GOODS = 2002; //等待卖家确认到货
    const RETURNED_STATUS_WAIT_SELLER_REFUND = 2003; //等待卖家退款
    const RETURNED_STATUS_SUCCESS = 2004; //退货完成


    /**
     * 用户换货流程状态定义
     */
    const CHANGE_GOODS_STATUS_WAIT_SELLER_CONFIRM = 3000; //等待卖家确认
    const CHANGE_GOODS_STATUS_WAIT_BUYER_SEND_GOODS = 3001; //等待买家发货
    const CHANGE_GOODS_STATUS_WAIT_SELLER_SEND_GOODS = 3002; //等待卖家发货
    const CHANGE_GOODS_STATUS_WAIT_BUYER_CONFIRM_GOODS = 3003; //等待买家确认收货
    const CHANGE_GOODS_STATUS_SUCCESS = 3004; //换货完成

    public static $orderStatusMap = array(
        self::ORDER_STATUS_WAIT_BUYER_PAY => "待付款",
        self::ORDER_STATUS_WAIT_SELLER_SEND_GOODS => "待发货", //等待商家发货（在线支付用户已经(预)付款；或者是货到付款流程刚下单）
        self::ORDER_STATUS_ESCOW_PAID => "担保支付已付款",
        self::ORDER_STATUS_TRADE_SUCCESS => "交易完成", //支付成功并到账，担保交易完成(用户已经确认到货)或者直接到账支付完成
        self::ORDER_STATUS_WAIT_BUYER_CONFIRM_GOODS => "待确认到货", //用户已经签收货物，担保交易等待用户确认到货
        self::ORDER_STATUS_SELLER_DELIVERED => "已发货待签收", //商家已经发货等待用户签收
        self::ORDER_STATUS_BUYER_CANCELED => "买家取消", //用户取消订单
        self::ORDER_STATUS_BUYER_RETURNED_GOODS => "已申请退货", //由于用户申请退货
        self::ORDER_STATUS_BUYER_CHANGED_GOODS => "申请换货", //用户申请换货
    );

    public static $returnStatusMap = array(
        self::RETURNED_STATUS_WAIT_SELLER_CONFIRM => "已申请退货",
        self::RETURNED_STATUS_WAIT_BUYER_SEND_GOODS => "待买家发货",
        self::RETURNED_STATUS_WAIT_SELLER_CONFIRM_GOODS => "待卖家确认确认到货",
        self::RETURNED_STATUS_WAIT_SELLER_REFUND => "待卖家退款",
        self::RETURNED_STATUS_SUCCESS => "退货完成",
    );

    public static $changeStatusMap = array(
        self::CHANGE_GOODS_STATUS_WAIT_SELLER_CONFIRM => "待卖家确认",
        self::CHANGE_GOODS_STATUS_WAIT_BUYER_SEND_GOODS => "待买家发货",
        self::CHANGE_GOODS_STATUS_WAIT_SELLER_SEND_GOODS => "待卖家发货",
        self::CHANGE_GOODS_STATUS_WAIT_BUYER_CONFIRM_GOODS => "待卖家确认到货",
        self::CHANGE_GOODS_STATUS_SUCCESS => "换货结束",
    );


    private function __construct()
    {
        $this->cache = $this->di->get('memcached');
    }

    public static function instance()
    {
        if (!static::$instance instanceof TransactionManager) {
            static::$instance = new self();
        }
        static::$instance->host_key = HOST_KEY;
        return static::$instance;
    }

    public function getStoreSetting($customer, $refresh = true)
    {
        $cacheKey = "store_info_for_customer_" . $customer;
        $data = $this->cache->get($cacheKey);
        if ($refresh || !$data) {
            $data = [];
            $logistics = Shop::findFirst("customer_id='".$customer."'");

            if ($logistics) {
                $data = $logistics->toArray();
                $this->cache->save($cacheKey, $data);
            }
        }
        return $data;
    }

    /**
     * @param null $status
     * @return array|string
     */
    public static function getOrderStatus($status = null)
    {
        if (is_numeric($status)) {
            if (array_key_exists($status, self::$orderStatusMap)) {
                return self::$orderStatusMap[$status];
            } else {
                return "待确认";
            }
        } else {
            return self::$orderStatusMap;
        }
    }

    /**
     * @param null $status
     * @return array|string
     */
    public static function getChangeGoodsStatus($status = null)
    {
        if (is_numeric($status)) {
            if (array_key_exists($status, self::$changeStatusMap)) {
                return self::$changeStatusMap[$status];
            } else {
                return "待确认";
            }
        } else {
            return self::$changeStatusMap;
        }
    }

    /**
     * 获取退/换货商品的状态及可进行的操作
     *
     */
    public static function getRefundStatus($customer,$list)
    {
        foreach($list as $k=>$v)
        {
            $list[$k]=self::$instance->getItemReturn($customer,$v['id']);
        }
        return $list;
    }
    public  static function getItemReturn($customer,$id)
    {
        $item= self::$instance->db->query(
          "select sor.*,soi.created,soi.thumb,soi.item_spec_data,soi.item_name,soi.total_cash from
             shop_order_return as sor left join shop_order_item as soi on sor.order_item_id=soi.id
              where sor.id=".$id." and sor.customer_id=".$customer
       )->fetchAll();
       $item=$item ? $item[0]:[];
        if($item['type']==2)//退货
        {
            if($item['is_seller_accept']==0)//未处理
            {
                $item['status']="等待卖家审核";
                $item['button']="<span class='btn passCheck'>同意</span>
                <span class='btn refuse'>拒绝</span>";
            }
            else if($item['is_seller_accept']==2)//拒绝
            {
                $item['status']="申请被拒绝";
                $item['button']="";
            }
            else
            {
                if($item['is_end']==1)//流程已结束
                {
                    $item['status']="处理已结束";
                    $item['button']="";
                }
                else if($item['is_buyer_delivered']==0)//买家未发货
                {
                    $item['status']="等待买家寄回商品";
                    $item['button']="";
                }
                else if($item['seller_received_goods']==0)//卖家未收到货物
                {
                    $item['status']="买家已发货,等待卖家确认收货";
                    $item['button']="<span class='btn receiveGood'>确认收货</span>";
                }
                else if($item['is_seller_refunded']==0)//卖家未退款
                {
                    $item['status']="等待卖家退款";
                    $item['button']="<span class='btn pay btn-primary'>确认已退款</span>";
                }
                else if($item['is_send']==0)//流程未结束
                {
                    $item['status']="等待卖家审核";
                    $item['button']="";
                }
                else
                {
                    $item['status']="等待卖家审核";
                    $item['button']="";
                }
            }
        }
        if($item['type']==1)//换货
        {

            if($item['is_seller_accept']==0)//未处理
            {
                $item['status']="等待卖家审核";
                $item['button']="<span class='btn passCheck'>同意</span>
                <span class='btn refuse'>拒绝</span>";
            }
            else if($item['is_seller_accept']==2)//拒绝
            {
                $item['status']="申请被拒绝";
                $item['button']="";
            }
            else
            {
                if($item['is_end']==1)//流程已结束
                {
                    $item['status']="处理已结束";
                    $item['button']="";
                }
                else if($item['is_seller_refunded']==1 && $item['back_order_number']==0)
                {
                    $item['status']="退款成功";
                    $item['button']="<span class='btn newOrder btn-primary'>填写新订单号</span>";
                }
                else if($item['is_seller_refunded']==1 && $item['back_order_number']!=0)
                {
                    $item['status']="新订单已下";
                    $item['button']="<span class='btn finish '>换货完成</span>";
                }
                else if($item['is_buyer_delivered']==0)//买家未发货
                {
                    $item['status']="等待买家寄回商品";
                    $item['button']=$item['back_order_number']==0 ?
                        "<span class='btn newOrder btn-primary'>填写新订单号</span>":'';

                }
                else if($item['seller_received_goods']==0)//卖家未收到货物
                {
                    $item['status']="等待卖家确认收货";
                    $item['button']="<span class='btn receiveGood btn-primary'>确认收货</span>";
                    $item['button'].= $item['back_order_number']==0 ?
                        "<span class='btn newOrder btn-primary'>填写新订单号</span>":'';
                }
                else if($item['is_seller_refunded']==0)//卖家未退款
                {
                    $item['status']="等待卖家退款";
                    $item['button']="<span class='btn pay btn-primary'>确认退货成功</span>";
                    $item['button'].= $item['back_order_number']==0 ?
                        "<span class='btn newOrder btn-primary'>填写新订单号</span>":'';


                }
                else if($item['is_send']==0)//流程未结束
                {
                    $item['status']="等待卖家审核";
                    $item['button']="";
                }
                else
                {
                    $item['status']="等待卖家审核";
                    $item['button']="";
                }
            }
        }

        if($item)
        {
           $Logistics=Logistics::findFirst(array("key='".$item['buyer_deliver_logistics']."'",'columns'=>'name'));
            $item['logistic_name']=$Logistics ? $Logistics->name : '';
        }
        return $item;

    }

    /**
     * @param bool $refresh
     * @return array|mixed
     */
    public function getDefaultLogistics($refresh = false)
    {
        $cacheKey = "logistics_for_default";
        $data = $this->cache->get($cacheKey);

        if ($refresh || !$data) {
            $data = [];
            $logistics = Logistics::find();
            if ($logistics) {
                $data = $logistics->toArray();
                $this->cache->save($cacheKey, $data);
            }
        }

        return $data;
    }

    /**
     * @param $customer
     * @param bool $refresh
     * @return array|mixed
     */
    public function getCustomerLogistics($customer, $refresh = false)
    {
        $cacheKey = "logistics_for_customer_" . $customer;
        $data = $this->cache->get($cacheKey);

        if ($refresh || !$data) {
            $data = [];
            $logistics = Logistics::find();
            if ($logistics) {
                foreach ($logistics as $item) {
                    $lc = LogisticsForCustomers::findFirst("customer_id='{$customer}' AND type='{$item->key}'");
                    $item = $item->toArray();
                    if ($lc && intval($lc->is_active) > 0) {
                        $item['fee'] = $lc->fee;
                        $item['is_active'] = intval($lc->is_active);
                        $item['description'] = $lc->description;
                        $data[] = $item;
                    } else {
                        $item['fee'] = $item['default_fee'];
                        $item['description'] = '';
                        $item['is_active'] = 0;
                        $data[] = $item;
                    }
                }
            }

            if (count($data) > 0) {
                $this->cache->save($cacheKey, $data);
            }
        }

        return $data;
    }

    /**
     * @param $customer
     * @param bool $refresh
     * @return array|mixed
     */
    public function getCustomerFreightTpls($customer, $refresh = false)
    {
        $cacheKey = "freight_tpl_for_customer_" . $customer;
        $data = $this->cache->get($cacheKey);

        if ($refresh || !$data) {
            $data = [];
            $logistics = FreightTpl::find("customer_id='{$customer}'");
            if ($logistics) {
                foreach ($logistics as $item) {
                    $lc = FreightTplItems::find("tpl_id='{$item->id}'");
                    $item = $item->toArray();
                    $item['items'] = $lc->toArray();
                    $data[] = $item;
                }
            }

            if (count($data) > 0) {
                $this->cache->save($cacheKey, $data);
            }
        }

        return $data;
    }

    /**
     * @param $type
     * @return bool
     */
    public function getDefaultLogisticsByType($type)
    {
        $logistics = $this->getDefaultLogistics();
        if (!$logistics) {
            return false;
        }

        foreach ($logistics as $item) {
            if ($item['key'] == $type) {
                return $item;
            }
        }

        return false;
    }

    /**
     * @param $type
     * @return bool
     */
    public function getCustomerLogisticsByType($type, $customer)
    {
        $logistics = $this->getCustomerLogistics($customer);
        if (!$logistics) {
            return false;
        }

        foreach ($logistics as $item) {
            if ($item['key'] == $type) {
                return $item;
            }
        }

        return false;
    }
}