<?php
/**
 * Author: kuangyong
 * Date: 14-7-21
 * Time: 下午4:29
 */

namespace Components\Product;

use Models\Product\ProductComment;
use Models\Product\Product;
use Models\User\Users;
use Phalcon\Mvc\User\Plugin;

class CommentManger extends Plugin{


    private $cache = null;
    private static $instance = null;

    private function __construct()
    {
        $this->cache = $this->di->get('memcached');
    }

    public static function instance()
    {
        if (!self::$instance instanceof CommentManger) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /*获取商品评论数*/
    public function getCount($item_id)
    {
      return ProductComment::count("product_id='$item_id' and  is_show=1");
    }


} 