<?php
/**
 * Created by PhpStorm.
 * User: luguiwu
 * Date: 14-9-24
 * Time: 下午5:06
 */

namespace Components\Product;
use Phalcon\Mvc\User\Plugin;

class FavourableManager extends Plugin
{

    /**
     * 活动代码汇总
     */
    const ORDER_FREE_POSTAGE = 1000; //免邮费
    public static $favourableCodeMap = array(
        self::ORDER_FREE_POSTAGE => "全场包邮",

    );
} 