<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-9-26
 * Time: 下午6:37
 */

namespace Components\Product;

use Models\Shop\ShopFreightTpl;
use Models\Shop\ShopFreightTplItems;
use Phalcon\Mvc\User\Plugin;
use Util\Ajax;

class FreightManager extends Plugin
{
    // 计价方式
    const VALUATION_WAY_NUMBER = 'number';
    const VALUATION_WAY_VOLUME = 'volume';
    const VALUATION_WAY_WEIGHT = 'weight';

    static $_valuation_way = array(
        self::VALUATION_WAY_NUMBER => '按件数',
        self::VALUATION_WAY_VOLUME => '按重量',
        self::VALUATION_WAY_WEIGHT => '按体积',
    );

    private static $instance = null;

    public static function init()
    {
        if (!static::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getFreight()
    {
        $freight = ShopFreightTpl::find(array(" customer_id=" . CUR_APP_ID . " ", 'limit' => 20, 'order' => 'id desc'));
        return $freight ? $freight->toArray() : array();
    }

    public function getItems($id)
    {
        $freight = ShopFreightTplItems::find(array('customer_id=' . CUR_APP_ID . ' and freight_id =' . $id, 'order' => 'is_national desc'));
        $items = $freight ? $freight->toArray() : array();
        $data = [];
        if ($items) {
            foreach ($items as $item) {
                $data[$item['mail_mode']][] = $item;
            }
        }
        if (empty($data["express"])) {
            $data["express"][] = array(
                "first_num" => "",
                "first_cash" => "",
                "addon_num" => "",
                "addon_cash" => "",
                "is_national" => 1
            );
        }

        if (empty($data["ems"])) {
            $data["ems"][] = array(
                "first_num" => "",
                "first_cash" => "",
                "addon_num" => "",
                "addon_cash" => "",
                "is_national" => 1
            );
        }

        if (empty($data["mail"])) {
            $data["mail"][] = array(
                "first_num" => "",
                "first_cash" => "",
                "addon_num" => "",
                "addon_cash" => "",
                "is_national" => 1
            );
        }
        return $data;
    }

    public function getTpl($id)
    {
        $freight = ShopFreightTpl::findFirst(array('customer_id=' . CUR_APP_ID . ' and id =' . $id));
        return $freight ? $freight->toArray() : array();
    }

    public function setFreight()
    {
        $base = $this->request->getPost('base');
        $base['mail_mode'] = implode(',', $base['mail_mode']);

        $items = $this->request->getPost('items');
        $id = intval($base['id']);
        // 判断数据
        if (!($base && $items)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if ($id) {
            $tpl = ShopFreightTpl::findFirst('id=' . $id);
            $base['modified'] = time();
            $tpl->update($base);

            // 删除原有的运费项
            $old = ShopFreightTplItems::find('freight_id=' . $id);
            if ($old) {
                foreach ($old as $v) {
                    $v->delete();
                }
            }
        } else {
            $tpl = new ShopFreightTpl();
            $base['customer_id'] = CUR_APP_ID;
            $base['created'] = time();
            $base['modified'] = time();
            if (!$tpl->create($base)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "插入运费模板错误！");
            }
            $id = $tpl->id;
        }

        foreach ($items as $item) {
            $item['freight_id'] = $id;
            $item['customer_id'] = CUR_APP_ID;
            $obj = new ShopFreightTplItems();
            $obj->create($item);
        }

        return Ajax::init()->outRight('', $id);
    }
}