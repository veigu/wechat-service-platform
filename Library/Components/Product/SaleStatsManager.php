<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 10/11/2014
 * Time: 10:35 AM
 */

namespace Components\Product;

// 销售统计
use Phalcon\Mvc\User\Plugin;

class SaleStatsManager extends Plugin
{
    private static $instance = null;

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // 销量最多
    public function getTopSaleItem($endTime = 0)
    {
        $end = $endTime == 0 || $endTime >= time() ? '' : ' AND created>' . $endTime;
        $where = 'WHERE customer_id=' . CUR_APP_ID . $end;
        $sql = 'SELECT item_id,item_name,item_sell_price,COUNT(*) as count,sum(quantity) as sum  FROM shop_order_item ' . $where . ' GROUP BY item_name ORDER BY sum DESC LIMIT 10';
        $total = $this->db->query($sql)->fetchAll(\PDO::FETCH_ASSOC);
        return $total;
    }

    public function getOrderCount($endTime = 0)
    {
        $end = $endTime == 0 || $endTime >= time() ? '' : ' AND created>' . $endTime;
        $where = 'WHERE customer_id=' . CUR_APP_ID . $end;
        $sql = 'SELECT count(*) as count  FROM shop_orders ' . $where;
        $total = $this->db->query($sql)->fetch(\PDO::FETCH_ASSOC);
        return $total['count'];
    }
} 