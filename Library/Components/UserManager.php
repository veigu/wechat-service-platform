<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-8
 * Time: 下午3:05
 */

namespace Components;

use Components\WeChat\MessageManager;
use Components\WeChat\RequestFactory;
use Components\WeiBo\FansClient;
use Models\Customer\CustomerOpenInfo;
use Models\User\UserForCustomers;
use Models\User\UserGroups;
use Models\User\Users;
use Models\User\UsersWechat;
use Models\User\UsersWeibo;
use Models\User\UserWechatGroup;
use Models\User\UserWeiboGroup;
use Phalcon\Cache\BackendInterface;
use Phalcon\Logger\Adapter\File;
use Phalcon\Mvc\User\Plugin;

class UserManager extends Plugin
{
    const TYPE_CLICK = 'CLICK';
    const TYPE_VIEW = 'VIEW';
    const TYPE_TOP_BAR = 'TOP_BAR';

    private $nextOpenId = '';
    private $hasMore = true;

    /**
     * @var BackendInterface
     */
    private $cache = null;

    /**
     * @var File
     */
    private $logger = null;

    /**
     * @var UserManager
     */
    private static $instance = null;

    private function __construct()
    {
        $this->cache = $this->di->get('memcached');
        $this->logger = $this->di->get('wechatLogger');
    }

    /**
     * @return UserManager
     */
    public static function instance()
    {
        if (!self::$instance instanceof UserManager) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * get user grade list
     * @param int $customer
     * @param string $refresh
     * @return multitype:
     */
    public function getUserGroups($customer, $refresh = false)
    {
        $key = "customer_user_group_list_" . $customer;
        $data = $this->cache->get($key);
        if (!$data || $refresh) {
            $data = UserGroups::find("customer_id='{$customer}'")->toArray();
            $this->cache->save($key, $data);
        }
        return $data;
    }

    /**
     * get user grade list
     * @param int $customer
     * @param string $refresh
     * @return multitype:
     */
    public function getWechatGroups($customer, $refresh = false)
    {
        $data = $this->cache->get("customer_wechat_group_list_" . $customer);
        if (!$data || $refresh) {
            $data = UserWechatGroup::find("customer_id='{$customer}'")->toArray();
            $this->cache->save("customer_wechat_group_list_" . $customer, $data);
        }
        return $data;
    }

    /**
     * get user grade list
     * @param int $customer
     * @param string $refresh
     * @return multitype:
     */
    public function getWeiboGroups($customer, $refresh = false)
    {
        $data = $this->cache->get("customer_weibo_group_list_" . $customer);
        if (!$data || $refresh) {
            $data = UserWeiBoGroup::find("customer_id='{$customer}'")->toArray();
            $this->cache->save("customer_weibo_group_list_" . $customer, $data);
        }
        return $data;
    }

    /**
     * get user grade rule list
     * @param int $customer
     * @param string $refresh
     * @return Ambigous <\Phalcon\Cache\mixed, multitype:>
     */
    public function getUserGradeRules($customer, $refresh = false)
    {
        $data = $this->cache->get("customer_user_group_rule_list_" . $customer);
        if (!$data || $refresh) {
            $data = UserGradeRules::find("customer_id='{$customer}'")->toArray();
            $this->cache->save("customer_user_group_rule_list_" . $customer, $data);
        }
        return $data;
    }

    /**
     * sync use group from wechat setting
     * @param int $customer
     * @param string $appId
     * @param string $appSecret
     */
    public function syncGroupFromWeChat($customer, $appId, $appSecret)
    {
        $request = RequestFactory::create("GroupGet", $customer, $appId, $appSecret);
        $request->run();
        if ($request->isFailed()) {
            $this->di->get('wechatLogger')->error("获取用户分组失败！" . $request->getErrorMessage());
            return false;
        } else {
            $result = $request->getResult();
            $wechatGroups = $result['groups'];
            if (count($wechatGroups) > 0) {
                $this->db->begin();
                try {
                    $groups = UserWechatGroup::find("customer_id='{$customer}'");
                    if ($groups) {
                        foreach ($groups as $group) {
                            if (!$group->delete()) {
                                $messages = [];
                                foreach ($group->getMessages() as $message) {
                                    $messages[] = (string)$message;
                                }
                                throw new \Phalcon\Exception("微信用户组同步时删除用户组关联失败！" . join(',', $messages));
                            }
                        }
                    }

                    foreach ($wechatGroups as $item) {
                        $group = new UserWechatGroup();
                        $group->customer_id = $customer;
                        $group->group_id = $item['id'];
                        $group->count = $item['count'];
                        $group->name = $item['name'];

                        if (!$group->save()) {
                            $messages = [];
                            foreach ($group->getMessages() as $message) {
                                $messages[] = (string)$message;
                            }
                            throw new \Phalcon\Exception("微信用户组同步时保存用户组关联失败！" . join(',', $messages));
                        }
                    }

                    $this->db->commit();
                } catch (\Exception $e) {
                    $this->db->rollback();
                    $this->di->get('wechatLogger')->error("同步微信用户组失败！" . $e->getMessage());
                    return false;
                }
            }
            $this->getWeiboGroups($customer, true);
            return true;
        }
    }

    public function syncGroupToWeChat($customer, $appId, $appSecret)
    {

    }

    /**
     * sync use group from wechat setting
     * @param int $customer
     * @param string $appId
     * @param string $appSecret
     */
    public function syncGroupFromWeibo($customer, $appId, $appSecret)
    {
        $client = new FansClient($appId, $appSecret, $token);
        $request = RequestFactory::create("GroupGet", $customer, $appId, $appSecret);
        $request->run();
        if ($request->isFailed()) {
            $this->di->get('wechatLogger')->error("获取用户分组失败！" . $request->getErrorMessage());
            return false;
        } else {
            $result = $request->getResult();
            $wechatGroups = $result['groups'];
            if (count($wechatGroups) > 0) {
                $this->db->begin();
                try {
                    $groups = UserWechatGroup::find("customer_id='{$customer}'");
                    if ($groups) {
                        foreach ($groups as $group) {
                            if (!$group->delete()) {
                                $messages = [];
                                foreach ($group->getMessages() as $message) {
                                    $messages[] = (string)$message;
                                }
                                throw new \Phalcon\Exception("微信用户组同步时删除用户组关联失败！" . join(',', $messages));
                            }
                        }
                    }

                    foreach ($wechatGroups as $item) {
                        $group = new UserWechatGroup();
                        $group->customer_id = $customer;
                        $group->group_id = $item['id'];
                        $group->count = $item['count'];
                        $group->name = $item['name'];

                        if (!$group->save()) {
                            $messages = [];
                            foreach ($group->getMessages() as $message) {
                                $messages[] = (string)$message;
                            }
                            throw new \Phalcon\Exception("微信用户组同步时保存用户组关联失败！" . join(',', $messages));
                        }
                    }

                    $this->db->commit();
                } catch (\Exception $e) {
                    $this->db->rollback();
                    $this->di->get('wechatLogger')->error("同步微信用户组失败！" . $e->getMessage());
                    return false;
                }
            }
            $this->getWeiboGroups($customer, true);
            return true;
        }
    }

    /**
     *
     * @param int $customer
     * @param string $appId
     * @param string $appSecret
     */
    public function syncUserFromWechat($customer, $appId, $appSecret)
    {
        while ($this->hasMore) {
            $items = $this->getUserListFromWechat($customer, $appId, $appSecret);
            if ($items) {
                $request = RequestFactory::create("UserInfo", $customer, $appId, $appSecret);
                foreach ($items as $openId) {
                    $this->addWechatUser($customer, $openId, $appId, $appSecret);
                }
            }
        }
        return true;
    }

    public function addWechatUser($customer, $openId, $appId, $appSecret) {
        $this->di->get('weiboLogger')->error("微信用户入库开始！ customer: ". $customer );
        $this->db->begin();
        try {
            $errorMessages = [];
            $userForWechat = UsersWechat::findFirst("open_id='{$openId}'");
            if (!$userForWechat) {
                $request = RequestFactory::create("UserInfo", $customer, $appId, $appSecret);
                $request->set('openid', $openId);
                $request->run();
                if ($request->isFailed()) {
                    $this->di->get('wechatLogger')->error("微信用户同步获取用户信息失败！" . $request->getErrorMessage());
                    $userInfo['nickname'] = "新用户" . rand(1, 9999);
                    $userInfo['language'] = "zh_CN";
                    $userInfo['sex'] = "0";
                    $userInfo['country'] = '';
                    $userInfo['province'] = '';
                    $userInfo['city'] = '';
                    $userInfo['headimgurl'] = '';
                    $userInfo['subscribe'] = 0;
                    $userInfo['subscribe_time'] = null;
                }
                $userInfo = $request->getResult();

                $request = RequestFactory::create("GroupGetId", $customer, $appId, $appSecret);
                $request->run();
                if($request->isFailed()) {
                    $this->di->get('wechatLogger')->debug("获取微信用户所属分组失败。" . $request->getErrorMessage());
                    $userGroupId = 0;
                }
                else {
                    $userGroupId = $request->getResult()['groupid'];
                }
                $this->logger->info("微信用户入库开始");
                $userForWechat = new UsersWechat();
                $userForWechat->customer_id = $customer;
                $userForWechat->open_id = "{$openId}";
                $userForWechat->group_id = $userGroupId;
                $userForWechat->nickname = $userInfo['nickname'];
                $userForWechat->sex = $userInfo['sex'];
                $userForWechat->city = $userInfo['city'];
                $userForWechat->province = $userInfo['province'];
                $userForWechat->country = $userInfo['country'];
                $userForWechat->language = $userInfo['language'];
                $userForWechat->headimgurl = $userInfo['headimgurl'];
                $userForWechat->subscribe = $userInfo['subscribe'];
                $userForWechat->subscribe_time = $userInfo['subscribe_time'];
                $userForWechat->is_binded = 0;
                if (!$userForWechat->save()) {
                    foreach ($userForWechat->getMessages() as $message) {
                        $errorMessages[] = (string)$message;
                    }
                    throw new \Phalcon\Exception("微信用户信息入库失败。: " . implode(',', $errorMessages));
                }
            }
            else
            {
                $request = RequestFactory::create("UserInfo", $customer, $appId, $appSecret);
                $request->set('openid', $openId);
                $request->run();
                if ($request->isFailed()) {
                    $this->di->get('wechatLogger')->error("微信用户同步获取用户信息失败！" . $request->getErrorMessage());
                    $userInfo['subscribe'] = 0;
                    $userInfo['subscribe_time'] = null;
                }

                $userInfo = $request->getResult();
                $userForWechat->subscribe = $userInfo['subscribe'];
                $userForWechat->subscribe_time = $userInfo['subscribe_time'];
                if (!$userForWechat->update()) {
                    foreach ($userForWechat->getMessages() as $message) {
                        $errorMessages[] = (string)$message;
                    }
                    throw new \Phalcon\Exception("微信用户更新信息失败。: " . implode(',', $errorMessages));
                }
            }
            @$this->db->commit();
        } catch (\Phalcon\Exception $e) {
            $this->di->get('wechatLogger')->error("微信用户同步获取用户信息失败！" . $request->getErrorMessage());
            $this->db->rollback();
            return false;
        }
        $this->logger->info("微信用户入库成功！");
        return $userForWechat;
    }

    public function addWeiboUser($customer, $uid, $openId, $appId, $appSecret, $access_token) {
        $this->di->get('weiboLogger')->error("微博用户入库开始！ customer: ". $customer . " token：". $access_token);
        $this->db->begin();
        try {
            $errorMessages = [];
            $this->di->get('weiboLogger')->debug("--------  $openId ---------- " );
            $userForWeibo = UsersWeibo::findFirst("customer_id='{$customer}' AND uid='{$openId}'");
            $this->di->get('weiboLogger')->debug("------------------ " );
            if (!$userForWeibo) {
                $client = new FansClient($appId, $appSecret, $access_token);
                $userInfo = $client->get_user_info($openId);
                if ($client->isFailed()) {
                    $this->di->get('weiboLogger')->error("微博用户同步获取用户信息失败！" . $client->getErrorMessage());
                    $userInfo['nickname'] = "新用户" . rand(1, 9999);
                    $userInfo['language'] = "zh_CN";
                    $userInfo['sex'] = "0";
                    $userInfo['country'] = '';
                    $userInfo['province'] = '';
                    $userInfo['city'] = '';
                    $userInfo['headimgurl'] = '';
                    $userInfo['subscribe'] = 0;
                    $userInfo['subscribe_time'] = null;
                }

                if($uid) {
                    $userGroupId = $client->get_user_group($uid, $openId);
                    if($client->isFailed()) {
                        $this->di->get('weiboLogger')->debug("获取微博用户所属分组失败。" . $client->getErrorMessage());
                        $userGroupId = 0;
                    }
                }
                else {
                    $userGroupId = 0;
                }

                $this->logger->info("微信用户入库开始");
                $userForWeibo = new UsersWeibo();
                $userForWeibo->customer_id = $customer;
                $userForWeibo->open_id = (string)"{$openId}";
                $userForWeibo->group_id = $userGroupId;
                $userForWeibo->nickname = $userInfo['nickname'];
                $userForWeibo->sex = $userInfo['sex'];
                $userForWeibo->city = $userInfo['city'];
                $userForWeibo->province = $userInfo['province'];
                $userForWeibo->country = $userInfo['country'];
                $userForWeibo->language = $userInfo['language'];
                $userForWeibo->headimgurl = $userInfo['headimgurl'];
                $userForWeibo->subscribe = $userInfo['subscribe'];
                $userForWeibo->subscribe_time = $userInfo['subscribe_time'];
                $userForWeibo->is_binded = 0;
                if (!$userForWeibo->save()) {
                    foreach ($userForWeibo->getMessages() as $message) {
                        $errorMessages[] = (string)$message;
                    }
                    throw new \Phalcon\Exception("微博用户信息入库失败。: " . implode(',', $errorMessages));
                }
            }
            else {
                $this->di->get('weiboLogger')->info("微博用户已经同步！");
            }
            $this->db->commit();
        } catch (\Phalcon\Exception $e) {
            $this->di->get('weiboLogger')->error("微博用户同步获取用户信息失败！" . $e->getMessage());
            $this->db->rollback();
            return false;
        }
        $this->logger->info("微博用户入库结束！");
        return $userForWeibo;
    }

    public function getWeiboUid($customer, $refresh = true) {
        $cacheKey = HOST_KEY . "_customer_weibo_uid_" . $customer;
        $data = $this->cache->get($cacheKey);
        if(!$data || $refresh) {
            $this->di->get('weiboLogger')->error("get weibo uid for customer: ". $customer);
            try {
                $openInfo = CustomerOpenInfo::findFirst("customer_id='{$customer}' AND platform='" . MessageManager::PLATFORM_TYPE_WEIBO . "'");
                $this->di->get('weiboLogger')->debug("get customer weibo uid:" . join(',', $openInfo->toArray()));
                if($openInfo) {
                    if(!empty($openInfo->original_id)) {
                        $data = $openInfo->original_id;
                    }
                    else {
                        $client = new FansClient($openInfo->app_id, $openInfo->app_secret, $openInfo->token);
                        $data = $client->get_uid();
                        if($client->isFailed()) {
                            $this->di->get('weiboLogger')->debug("get customer weibo uid failed:" . $client->getErrorMessage());
                            $data = null;
                        }
                        else {
                            if(!$openInfo->update(array('original_id' => $data))) {
                                $errMsgs = [];
                                foreach($openInfo->getMessages() as $msg) {
                                    $errMsgs[] = (string)$msg;
                                }
                                throw new \Phalcon\Exception(join(',', $errMsgs));
                            }
                        }
                    }
                }
            }
            catch(\Phalcon\Exception $e) {
                $this->di->get('weiboLogger')->error("get weibo uid for customer-". $customer . " failed :" . $e->getMessage());
            }
            $this->cache->save($cacheKey, $data);
        }
        $this->di->get('weiboLogger')->debug("get customer weibo uid end.");
        return $data;
    }

    /**
     *
     * @param int $customer
     * @param string $appId
     * @param string $appSecret
     * @return array
     */
    private function getUserListFromWechat($customer, $appId, $appSecret)
    {
        $request = RequestFactory::create("UserGet", $customer, $appId, $appSecret);
        if (!empty($this->nextOpenId)) {
            $request->set('next_openid', $this->nextOpenId);
        }
        $request->run();
        if ($request->isFailed()) {
            $this->nextOpenId = '';
            $this->di->get('wechatLogger')->error($customer . "获取微信用户列表失败！" . $request->getErrorMessage());
            return false;
        }

        $result = $request->getResult();
        $this->nextOpenId = $result['next_openid'];
        if ($result['total'] > 0 && strlen($this->nextOpenId) > 0 && $result['count'] > 0) {
            $this->hasMore = true;
        } else {
            $this->hasMore = false;
        }
        return isset($result['data']) ? $result['data']['openid'] : array();
    }

    /**
     *
     * @param int $customer
     * @param string $appId
     * @param string $appSecret
     * @return array
     */
    private function getUserListFromWeibo($customer, $appId, $appSecret)
    {
        $request = RequestFactory::create("UserGet", $customer, $appId, $appSecret);
        if (!empty($this->nextOpenId)) {
            $request->set('next_openid', $this->nextOpenId);
        }
        $request->run();
        if ($request->isFailed()) {
            $this->nextOpenId = '';
            $this->di->get('wechatLogger')->error($customer . "获取微信用户列表失败！" . $request->getErrorMessage());
            return false;
        }

        $result = $request->getResult();
        $this->nextOpenId = $result['next_openid'];
        if ($result['total'] > 0 && strlen($this->nextOpenId) > 0 && $result['count'] > 0) {
            $this->hasMore = true;
        } else {
            $this->hasMore = false;
        }
        return isset($result['data']) ? $result['data']['openid'] : array();
    }


}