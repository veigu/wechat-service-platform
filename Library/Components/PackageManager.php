<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/25
 * Time: 14:38
 */

namespace Components;


class PackageManager
{
    const FUNC_BASE_PRICE = 2000;//基础功能价格/年
    const FUNC_SHOP_BASE_PRICE = 2000; //限制商城价格/年
    const FUNC_SHOP_ALL_PRICE = 5000; //完整商城价格/年
    const FUNC_ALL_PRICE = 12000; //完整功能版价格/年

    const PACKAGE_FULL = 'full';
    const PACKAGE_FREE = 'free';
    const PACKAGE_STANDARD = 'standard';
    const PACKAGE_SHOP = 'shop';
    const PACKAGE_DECORATION = 'decoration';
    const PACKAGE_CAR = 'car';
    const PACKAGE_ESTATE = 'estate';
    const PACKAGE_MEDICAL = 'medical';
    const PACKAGE_HOTEL = 'hotel';
    const PACKAGE_CATERING = 'catering';
    const PACKAGE_WEDDING = 'wedding';

    // 所有套餐
    public static $package_name = array(
        self::PACKAGE_FREE => "免费版",
        self::PACKAGE_STANDARD => "标准行业版",
        self::PACKAGE_SHOP => "电商行业版",
        self::PACKAGE_DECORATION => "家装行业版",
        self::PACKAGE_CAR => "汽车行业版",
        self::PACKAGE_ESTATE => "房产行业版",
        self::PACKAGE_MEDICAL => "医疗行业版",
        self::PACKAGE_HOTEL => "酒店行业版",
        self::PACKAGE_CATERING => "餐饮行业版",
        self::PACKAGE_WEDDING => "婚纱行业版",
        self::PACKAGE_FULL => "完整版",
    );

    // 套餐价格
    public static $package_price = array(
        self::PACKAGE_FREE => "0",
        self::PACKAGE_STANDARD => "5000",
        self::PACKAGE_SHOP => "8000",
        self::PACKAGE_DECORATION => "8000",
        self::PACKAGE_CAR => "8000",
        self::PACKAGE_ESTATE => "8000",
        self::PACKAGE_MEDICAL => "8000",
        self::PACKAGE_HOTEL => "9000",
        self::PACKAGE_CATERING => "9000",
        self::PACKAGE_WEDDING => "8000",
        self::PACKAGE_FULL => "12000",
    );

    // 套餐功能
    public static $package_func = array(
        "pic" => array("图片空间", "50M免费"),
        "wechat" => array("微信接入", "关键词、关注回复、自定义菜单"),
        "weibo" => array("微博接入", "关键词、关注回复、自定义菜单"),
        "site" => array("微网站", "快捷导航、栏目、文章、多模版选择"),
        "shop_base" => array("微商城基础版", "快捷导航、多模版、积分与分享、商品管理与展示"),
        "shop_full" => array("微商城完整版", "购物车、订单管理、退换货、多支付通道、多规格与库存价格关联、运费模板、积分支等"),
        "user" => array("微会员", "微信、微博粉丝管理，会员卡"),
        "pointmall" => array("积分商城", "微信、微博粉丝管理，会员卡"),
        "demand" => array("微询盘", "询盘类型管理、询盘内容跟踪"),
        "store" => array("微门店", "多门店管理、LBS一键导航、一键拨号"),
        "reserve" => array("微预约", "通用预约管理，可自定义表单"),
        "coupon" => array("优惠券", "支持现金券、折扣券、特价券，订单管理"),
        "panorama" => array("3D全景", "360全景展示"),
        "pictorial" => array("微画报", "画册展示，多图展示"),
        "vote" => array("微投票", "发起投票，投票结果统计"),
        "survey" => array("微调研", "发起调研活动，调研结果统计"),
        "act" => array("微活动", "大转盘、刮刮卡、砸金蛋、老虎机"),
        "serve" => array("微服务", "天气预报、彩票查询、火车查询、航班查询、小黄鸡等"),
        "game" => array("微游戏", "捕鱼、2048、神经猫、别踩白块"),
    );

    // 套餐功能受限
    public static $package_func_limit = array(
        "pic" => array(),
        "wechat" => array(),
        "weibo" => array(),
        "site" => array(),
        "shop_base" => array(self::PACKAGE_FREE, self::PACKAGE_CAR, self::PACKAGE_ESTATE, self::PACKAGE_MEDICAL),
        "shop_full" => array(self::PACKAGE_FREE, self::PACKAGE_DECORATION, self::PACKAGE_CAR, self::PACKAGE_ESTATE, self::PACKAGE_MEDICAL),
        "user" => array(),
        "pointmall" => array(self::PACKAGE_FREE, self::PACKAGE_DECORATION, self::PACKAGE_CAR, self::PACKAGE_ESTATE, self::PACKAGE_MEDICAL),
        "demand" => array(self::PACKAGE_FREE),
        "store" => array(self::PACKAGE_FREE, self::PACKAGE_STANDARD),
        "reserve" => array(self::PACKAGE_FREE, self::PACKAGE_STANDARD),
        "coupon" => array(self::PACKAGE_FREE, self::PACKAGE_STANDARD, self::PACKAGE_DECORATION),
        "panorama" => array(self::PACKAGE_FREE, self::PACKAGE_STANDARD),
        "pictorial" => array(self::PACKAGE_FREE),
        "vote" => array(self::PACKAGE_FREE),
        "survey" => array(self::PACKAGE_FREE),
        "act" => array(self::PACKAGE_FREE),
        "serve" => array(),
        "game" => array(),
    );

    // 尚未购买功能列表
    public static $package_limit_list = array(
        self::PACKAGE_STANDARD => array("store", "reserve", 'coupon', 'panorama'),
        self::PACKAGE_SHOP => array(),
        self::PACKAGE_DECORATION => array("shop_full"),
        self::PACKAGE_CAR => array("shop_base", "shop_full", "pointmall"),
        self::PACKAGE_ESTATE => array("shop_base", "shop_full", "pointmall"),
        self::PACKAGE_MEDICAL => array("shop_base", "shop_full", "pointmall"),
        self::PACKAGE_HOTEL => array(),
        self::PACKAGE_CATERING => array(),
        self::PACKAGE_WEDDING => array(),
        self::PACKAGE_FULL => array(),
    );
}