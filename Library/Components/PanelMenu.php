<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2014/10/27
 * Time: 16:03
 */

namespace Components;

// 说明
// 分别在shop和panel里面请controller+action不要重复
use Phalcon\Mvc\User\Component;

class PanelMenu extends Component
{
    protected static $_sideFstMenu = array(
        'info' => array('head' => 'web', 'name' => '商户信息', 'icon' => 'icon-magic'),
        'system' => array('head' => 'web', 'name' => '系统管理', 'icon' => 'icon-magic'),
        'web' => array('head' => 'web', 'name' => '微站管理', 'icon' => 'icon-adjust'),
        'shop' => array('head' => 'web', 'name' => '商城管理', 'icon' => 'icon-bell'),
        'user' => array('head' => 'web', 'name' => '用户管理', 'icon' => 'icon-lightbulb'),
        'func' => array('head' => 'web', 'name' => '功能模块', 'icon' => 'icon-cog'),
//        'stat' => array('head' => 'web', 'name' => '数据统计', 'icon' => 'icon-gift'),
    );

    protected static $_sideSecMenu = array(
        array('fst' => 'info', 'name' => '企业资料', 'icon' => 'icon-laptop', 'c' => 'setting', 'a' => 'index'),
        array('fst' => 'info', 'name' => '修改密码', 'icon' => 'icon-laptop', 'c' => 'setting', 'a' => 'password'),
        'wechat' => array('fst' => 'info', 'name' => '微信接入', 'icon' => 'icon-folder-open-alt', 'c' => 'wechat', 'a' => 'binding'),
        'web_base' => array('fst' => 'web', 'name' => '微站设置', 'icon' => 'icon-laptop', 'c' => 'site', 'a' => 'info'),
        'web_info' => array('fst' => 'web', 'name' => '公司信息', 'icon' => 'icon-laptop', 'c' => 'site', 'a' => 'page'),
        'web_content' => array('fst' => 'web', 'name' => '内容管理', 'icon' => 'icon-laptop', 'c' => 'article', 'a' => 'list'),
        //  'web_tpl' => array('fst' => 'web', 'name' => '微站模板', 'icon' => 'icon-laptop', 'c' => 'tpl', 'a' => 'index'),
        //系统管理
        'sys_setting' => array('fst' => 'system', 'name' => '系统设置', 'icon' => 'icon-laptop', 'c' => 'system', 'a' => 'focus'),
        'web_tpl' => array('fst' => 'system', 'name' => '模板设置', 'icon' => 'icon-laptop', 'c' => 'tpl', 'a' => 'index'),
        // 商城设置
        'shop_setting' => array('fst' => 'shop', 'name' => '店铺设置', 'icon' => 'icon-magic', 'm' => 'shop', 'c' => 'store', 'a' => 'index'),
        'shop_product' => array('fst' => 'shop', 'name' => '商品管理', 'icon' => 'icon-laptop', 'm' => 'shop', 'c' => 'product', 'a' => 'list'),
        'shop_order' => array('fst' => 'shop', 'noFree' => true, 'name' => '订单管理', 'icon' => 'icon-tablet', 'm' => 'shop', 'c' => 'transaction', 'a' => 'orders'),
        'shop_freight' => array('fst' => 'shop', 'noFree' => true, 'name' => '运费模板', 'icon' => 'icon-lightbulb', 'm' => 'shop', 'c' => 'freight', 'a' => 'index'),
//        'shop_promotion' => array('fst' => 'shop', 'noFree' => true, 'name' => '推广分成', 'icon' => 'icon-lightbulb', 'm' => 'shop', 'c' => 'promotion', 'a' => 'setting'),

        //用户管理
        'user' => array('fst' => 'user', 'name' => '用户设置', 'icon' => 'icon-screenshot', 'c' => 'users', 'a' => 'setting'),
        'user_list' => array('fst' => 'user', 'name' => '用户列表', 'icon' => 'icon-screenshot', 'c' => 'users', 'a' => 'index'),
        'user_group' => array('fst' => 'user', 'name' => '用户分组', 'icon' => 'icon-screenshot', 'c' => 'users', 'a' => 'group'),
        'point_rule' => array('fst' => 'user', 'name' => '积分规则', 'c' => 'users', 'a' => 'rules', 'icon' => "icon-asterisk"),
        'more' => array('fst' => 'func', 'name' => '所有功能', 'c' => 'module', 'a' => 'more', 'icon' => 'icon-globe'),
        'mine' => array('fst' => 'func', 'name' => '我的功能', 'c' => 'module', 'a' => 'mine', 'icon' => 'icon-gift'),

    );

    // 页面内部
    public static $_innerMenu = array(
        'web_base' => array(
            array('name' => '网站设置', 'c' => 'site', 'a' => 'info'),
        ),

        'web_info' => array(
            array('name' => '信息列表', 'c' => 'site', 'a' => 'page'),
            array('name' => '添加信息', 'c' => 'site', 'a' => 'pageAdd'),
            array('name' => '修改信息', 'c' => 'site', 'a' => 'pageUp', 'hide' => true),
        ),

        'web_content' => array(
            array('name' => '文章列表', 'c' => 'article', 'a' => 'list'),
            array('name' => '文章栏目', 'c' => 'article', 'a' => 'cat'),
            array('name' => '添加文章', 'c' => 'article', 'a' => 'add'),
            array('name' => '修改文章', 'c' => 'article', 'a' => 'update', 'hide' => true),
        ),

        'web_tpl' => array(
            array('name' => '我的设置', 'c' => 'tpl', 'a' => 'index'),
            array('name' => '模板套装', 'c' => 'tpl', 'a' => 'kit'),
            array('name' => '模板单品', 'c' => 'tpl', 'a' => 'single'),
        ),

        'wechat' => array(
            /* array('name' => '微信绑定', 'c' => 'wechat', 'a' => 'auto'),*/
            array('name' => '手动绑定', 'c' => 'wechat', 'a' => 'binding', 'hide' => true),
            array('name' => '微信菜单', 'c' => 'wechat', 'a' => 'menu'),
            array('name' => '自动回复', 'c' => 'wechat', 'a' => 'respond'),
            /*   array('name' => '消息群发', 'c' => 'wechat', 'a' => 'mass'),*/
            array('name' => '消息管理', 'c' => 'wechat', 'a' => 'message'),
        ),

        /*'user' => array(
            array('name' => '用户设置', 'c' => 'users', 'a' => 'setting'),
            array('name' => '用户列表', 'c' => 'users', 'a' => 'index'),
            array('name' => '微信用户', 'c' => 'users', 'a' => 'wechat'),
            array('name' => '微博用户', 'c' => 'users', 'a' => 'weibo'),
        ),*/

        'user_group' => array(
            array('name' => '自定义分组', 'c' => 'users', 'a' => 'group'),
            array('name' => '微信用户组', 'c' => 'users', 'a' => 'wechatGroup'),
            array('name' => '微博用户组', 'c' => 'users', 'a' => 'weiboGroup'),
        ),

        'point_rule' => array(
            array('name' => '普通用户积分规则', 'c' => 'users', 'a' => 'rules'),
            array('name' => '企业用户积分规则', 'c' => 'users', 'a' => 'firmRules'),
            array('name' => 'VIP用户积分规则', 'c' => 'card', 'a' => 'rules', 'm' => 'vipcard', 'is_module' => true),
        ),
        'sys_setting' => array(
            array('name' => '焦点图片', 'm' => 'sys', 'c' => 'system', 'a' => 'focus'),
            array('name' => '菜单导航', 'm' => 'sys', 'c' => 'system', 'a' => 'nav'),
            array('name' => '底部导航', 'm' => 'sys', 'c' => 'system', 'a' => 'foot'),
        ),
        'shop_setting' => array(
            array('name' => '店铺设置', 'm' => 'shop', 'c' => 'store', 'a' => 'index'),
            array('name' => '支付方式', 'm' => 'shop', 'c' => 'payment', 'a' => 'payment'),
        ),

        'shop_product' => array(
            array('name' => '商品列表', 'm' => 'shop', 'c' => 'product', 'a' => 'list'),
            array('name' => '商品品牌', 'm' => 'shop', 'c' => 'product', 'a' => 'brand'),
            array('name' => '商品类目', 'm' => 'shop', 'c' => 'product', 'a' => 'cat'),
            array('name' => '商品属性', 'm' => 'shop', 'c' => 'product', 'a' => 'attr'),
            array('name' => '型号规格', 'm' => 'shop', 'c' => 'product', 'a' => 'spec'),
            array('name' => '添加商品', 'm' => 'shop', 'c' => 'product', 'a' => 'edit'),
        ),

        'shop_order' => array(
            array('name' => '订单列表', 'm' => 'shop', 'c' => 'transaction', 'a' => 'orders'),
            array('name' => '退货订单', 'm' => 'shop', 'c' => 'transaction', 'a' => 'refund'),
        ),

        'shop_freight' => array(
            array('name' => '运费模板', 'm' => 'shop', 'c' => 'freight', 'a' => 'index'),
            array('name' => '添加模板', 'm' => 'shop', 'c' => 'freight', 'a' => 'add'),
            array('name' => '物流方式', 'm' => 'shop', 'c' => 'freight', 'a' => 'logistic'),
        ),

        'shop_promotion' => array(
            array('name' => '返现设置', 'm' => 'shop', 'c' => '', 'a' => 'index'),
            array('name' => '返现订单', 'm' => 'shop', 'c' => '', 'a' => 'add'),
            array('name' => '返现统计', 'm' => 'shop', 'c' => '', 'a' => 'logistic'),
        )
    );

    public static function init()
    {
        return new self();
    }

    public function getCurMenuKey()
    {
        $fstMenu = self::$_sideFstMenu;
        $secMenu = self::$_sideSecMenu;
        $innerMenu = self::$_innerMenu;

        $controller = $this->view->getControllerName();
        $action = $this->view->getActionName();
        $curFst = '';
        $curSec = '';

        $_isFound = false;
        foreach ($innerMenu as $secKey => $menus) {
            foreach ($menus as $inner) {
                if ($inner['c'] == $controller && $inner['a'] == $action) {
                    $curSec = $secKey;
                    $curFst = $secMenu[$curSec]['fst'];
                    $_isFound = true;
                    break;
                }
            }
        }
        // 2级菜单找当前
        if (!$_isFound) {
            foreach ($secMenu as $k => $sec) {
                if ($sec['c'] == $controller && $sec['a'] == $action) {
                    $curFst = $sec['fst'];
                    $curSec = $k;
                    break;
                }
            }
        }

        if (!$curFst) {
            foreach ($fstMenu as $k => $fst) {
                if (isset($fst['c']) && $fst['c'] == $controller && $fst['a'] == $action) {
                    $curFst = $k;
                    break;
                }
            }
        }

        // 模块
        if ($controller == 'module' && $action == 'run') {
            $curFst = 'func';
        }

        return array('side' => array('fst' => $curFst, 'sec' => $curSec));
    }

    public static function getMenu()
    {
        return array('side' => array('fst' => self::$_sideFstMenu, 'sec' => self::$_sideSecMenu, 'inner' => self::$_innerMenu));
    }
}