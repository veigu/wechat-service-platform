<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 2014/9/23
 * Time: 13:52
 */

namespace Components;

use Components\ModuleManager\ModuleManager;
use Components\WeChat\MessageManager;
use Library\PHPMailer\PHPMailer;
use Models\Customer\CustomerOpenInfo;
use Models\Customer\CustomerPackage;
use Models\Customer\CustomerProfile;
use Models\Customer\Customers;
use Models\Modules\Resources;
use Models\O2o\O2oCustomerApply;
use Models\Shop\Shop;
use Models\System\SystemOrders;
use Phalcon\Exception;
use Phalcon\Mvc\User\Plugin;
use Util\Ajax;
use Util\EasyEncrypt;
use Util\Validator;

class CustomerManager extends Plugin
{

    const ACCOUNT_STATUS_EXPIRED_TRIAL = '-1'; // 试用过期
    const ACCOUNT_STATUS_EXPIRED_PACKAGE = '-2'; // 套餐过期
    const ACCOUNT_STATUS_TRYOUT = '0';// 试用中
    const ACCOUNT_STATUS_IN_USE = '1';// 使用中

    const ACCOUNT_TRIAL_PERIOD = 2592000;//试用期限30天 30*3600*24

    public static $_active_name = array(
        self::ACCOUNT_STATUS_EXPIRED_TRIAL => '试用过期',
        self::ACCOUNT_STATUS_EXPIRED_PACKAGE => '套餐已过期',
        self::ACCOUNT_STATUS_TRYOUT => '试用中',
        self::ACCOUNT_STATUS_IN_USE => "使用中"
    );

    private static $instance = null;

    // 检查账号状态
    public function getAccountInfo()
    {

    }

    public static function init()
    {
        if (!static::$instance) {
            return new self();
        }

        return static::$instance;
    }

    // 登陆
    public function login($account, $password)
    {
        if (!Validator::validEmail($account)) {
            if (!Validator::validateAliasName($account)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "账号格式不准确，请填写正确的用户名或邮箱");
            }
        }

        if (!Validator::validPassword($password)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID);
        }

        $password = sha1($password);
        $sql = '(email="' . $account . '" or account="' . $account . '") AND password="' . $password . '" AND host_key="' . HOST_KEY . '"';
        $user = Customers::findFirst(array($sql));
        if (!$user) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '用户名或密码不正确！');
        }

        // 判断跳转
        $redirect = $this->checkJoinStepRedirect($user);
        $this->_registerSession($user);
        return Ajax::init()->outRight('', $redirect);
    }

    public function reg($account, $email, $password)
    {
        if (!Validator::validateAliasName($account)) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_INVALID);
        }

        if (!Validator::validEmail($email)) {
            return Ajax::init()->outError(Ajax::ERROR_EMAIL_IS_INVALID);
        }

        if (!Validator::validPassword($password)) {
            return Ajax::init()->outError(Ajax::ERROR_EMAIL_IS_INVALID);
        }

        $accountExists = Customers::count("account='" . $account . "' AND host_key='" . HOST_KEY . "'");
        if ($accountExists > 0) {
            return Ajax::init()->outError(Ajax::ERROR_ACCOUNT_HAS_BEING_USED);
        }

        $emailExists = Customers::count("email='" . $email . "' AND host_key='" . HOST_KEY . "'");
        if ($emailExists > 0) {
            return Ajax::init()->outError(Ajax::ERROR_EMAIL_HAS_BEING_USED);
        }

        $this->db->begin();
        $freeMode = $this->config->get('freeMode'); //是否免费模式（行指平台）

        try {
            $user = new Customers();
            $user->password = sha1($password);
            $user->account = $account;
            $user->name = "";
            $user->email = $email;
            $user->host_key = HOST_KEY;
            $user->created = time();
            $user->modified = time();
            $user->active = $freeMode ? self::ACCOUNT_STATUS_IN_USE : self::ACCOUNT_STATUS_TRYOUT; // 试用期
            $user->guide_steps = $freeMode ? '{"profile":true,"pick":true,"order":true}' : "";
            $user->qrcode = '';

            if (!$user->save()) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
                throw new Exception("");
            }

            $openInfo = new CustomerOpenInfo();
            $openInfo->customer_id = $user->id;
            $openInfo->token = uniqid();
            $openInfo->platform = 'wx';
            if (!$openInfo->save()) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string)$message);
                }
                throw new Exception("客户资料初始化失败！");
            }

            // 添加商家信息
            $profile = new CustomerProfile();
            $profile->create(array('customer_id' => $user->id));

            // 添加店铺信息
            $profile = new Shop();
            $profile->create(array('customer_id' => $user->id));

            // 添加套餐信息
            $pack = new CustomerPackage();
            if ($freeMode) {
                $package['customer_id'] = $user->id;
                $package['has_shop_full'] = 1;
                $package["package"] = 'full';
                $package['use_start'] = time();
                $package['use_end'] = strtotime("+2 year");
                $package['duration'] = 11; // 2年
                $package['limit_module'] = '[]';
                $package['own_module'] = '[]';
            } else {
                $package['customer_id'] = $user->id;
            }
            $pack->create($package);

            $this->db->commit();

            $this->_registerSession($user);

            // 判断跳转
            $redirect = $this->checkJoinStepRedirect($user);
            return Ajax::init()->outRight('', $redirect);
        } catch (\Exception $e) {
            $this->db->rollback();
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, "注册失败");
        }
    }

    public function forgot($email)
    {
        if (!Validator::validEmail($email)) {
            return Ajax::init()->outError(Ajax::ERROR_EMAIL_IS_INVALID);
        }

        $user = Customers::findFirst("email='" . $email . "' AND host_key='" . HOST_KEY . "'");
        if (!$user) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_NOT_EXISTS);
        }

        $pass = mt_rand(100000, 999999);
        // 如果邮箱不存在，则保存到当前用户
        $user->password = sha1($pass);
        if (!$user->update()) {
            foreach ($user->getMessages() as $message) {
                $msg[] = $message;
            };
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $msg);
        }

        // todo send mail
        // 邮箱注册用户，要进行邮件通知
        $loginUrl = $this->uri->baseUrl('/account/login');
        $this->session->set("current_request_url", $this->uri->baseUrl('/panel/setting/password'));
        $hostBrand = HOST_BRAND;
        $date = date('Y');
        $message = <<<EOF
                <html lang="zh">
                <head>
                <title>重置用户密码</title>
                <style type="text/css">
                span {
                    color: red;
                }
                </style>
                </head>
                <body>
                尊敬的客户您好：<br/>
                    您在我们网站平台进行了密码重置，以下是信息： <br/>
                    登录邮箱：<span style='color:red;'>{$email}</span><br />
                    重置后的密码：<span style='color:red;'>{$pass}</span>。</br>
                    请登陆后自行修改或妥善保管。祝您愉快！
                    <br/>
                    <br/>
                    登陆地址：<br/>
                    <a href="{$loginUrl}">{$loginUrl}</a>
                    <br/>
                    <br/>
                -----------------------<br/>
                {$date} @ <a href="{$hostBrand}">{$hostBrand}管理团队</a><br/>
                -----------------------<br/>
                系统邮件，请勿回复！
                </body>
                </html>
EOF;
        require_once(ROOT . '/Library/PHPMailer/PHPMailerAutoload.php');
        require_once(ROOT . '/Library/PHPMailer/PHPMailer.php');

        $mailer = new PHPMailer();
        $mailer->CharSet = "UTF-8";
        $mailer->Subject = "重置用户密码";
        //$mailer->From = "service@" . MAIN_DOMAIN;
        $mailer->From = "wwglfy@163.com";
        $mailer->FromName = HOST_BRAND;
        $mailer->msgHTML($message);
        $mailer->addAddress($email);

       // var_dump($mailer);
        if (!$mailer->send()) {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, $mailer->ErrorInfo);
        }
      //  return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG,'邮件发送失败！',1000);
        return Ajax::init()->outRight('邮件发送成功！');
    }

    public function checkJoinStepRedirect(Customers $user)
    {
        $current_request_url = $this->session->get("current_request_url");
        // todo
        // 判断用户步骤
        $user = $user->toArray();
        // 重置密码优先
        if (strpos('/panel/setting/password', $current_request_url)) {
            return $redirect = '/panel/setting/password';
        }

        if (!$user['guide_steps']) {
            // 需要填写企业资料
            return $redirect = '/customer/profile';
        }

        $guide_steps = json_decode($user['guide_steps'], true);
        if ($guide_steps) {
            // {"profile":true,"pick":true,"order":true}
            // 1. 填写资料先
            if (!(isset($guide_steps['profile']) && $guide_steps['profile'])) {
                return $redirect = '/customer/profile';
            }

            // 2. 选择套餐
            if (!(isset($guide_steps['pick']) && $guide_steps['pick'])) {
                return $redirect = '/customer/pick';
            }

            // 3. 选择套餐
            if (!(isset($guide_steps['order']) && $guide_steps['order'])) {
                return $redirect = '/customer/order';
            }

        }

        return $current_request_url && $current_request_url != $this->uri->baseUrl() ? $current_request_url : '/panel';
    }

    // 保存登陆信息
    private function _registerSession(Customers $user)
    {
        $wechat = CustomerOpenInfo::findFirst("customer_id='{$user->id}' AND platform = '" . MessageManager::PLATFORM_TYPE_WEIXIN . "'");
        $weibo = CustomerOpenInfo::findFirst("customer_id='{$user->id}' AND platform = '" . MessageManager::PLATFORM_TYPE_WEIBO . "'");

        $package = CustomerPackage::findFirst('customer_id = ' . $user->id);
        if ($package) {
            // 判断是否购买，是否过期
            if ($package->use_end == 0) {
                // 尚未支付,而过了试用期，转为免费版
                if ((time() - $user->created) > self::ACCOUNT_TRIAL_PERIOD) {
                    $user->update(array("active" => self::ACCOUNT_STATUS_EXPIRED_TRIAL));
                }
            } else if ($package->use_end <= time()) {
                // 套餐使用已过期
                $user->update(array("active" => self::ACCOUNT_STATUS_EXPIRED_PACKAGE));
            }
        }
        $this->session->set('customer_auth', true);
        $this->session->set('customer_package', $package ? $package->toArray() : []);
        $this->session->set('customer_info', $user);
        $this->session->set('customer_wechat', $wechat);
        $this->session->set('customer_weibo', $weibo);
    }

    public function setGuideSteps($guide_steps, $type, $val = true)
    {
        $guide = $guide_steps ? json_decode($guide_steps, true) : [];

        if ($type == "profile") {
            $guide['profile'] = true;
        }

        if ($type == "pick") {
            $guide['pick'] = true;
        }

        if ($type == "order") {
            $guide['order'] = true;
        }

        return json_encode($guide, JSON_UNESCAPED_UNICODE);
    }

    public function saveProfile($data)
    {
        if (!$data) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $customer = Customers::findFirst('id=' . CUR_APP_ID);

        $custom['name'] = $data['company_name'];// 复制名称到主表
        $custom['industry'] = $data['industry'];
        // 更新进度
        $custom['guide_steps'] = $this->setGuideSteps($customer->guide_steps, "profile");

        // 更新主表
        $customer->update($custom);

        // 更新信息
        $profile = CustomerProfile::findFirst('customer_id=' . CUR_APP_ID);
        if ($profile) {
            $profile->update($data);
        } else {
            $profile = new CustomerProfile();
            $data['customer_id'] = CUR_APP_ID;
            $profile->create($data);
        }

        // 更新缓存
        $this->session->set('customer_info', $customer);

        $redicret = $this->checkJoinStepRedirect($customer);
        return Ajax::init()->outRight('', $redicret);
    }

    // 选择行业套餐
    public function pickPackage($industry, $package)
    {
        if (!($industry && $package)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        // 更新套餐
        $data['package'] = $package;
        $data['price_month'] = 0;
        $data['price_year'] = PackageManager::$package_price[$package];
        $data['has_shop_base'] = !in_array('has_shop_base', PackageManager::$package_limit_list[$package]);
        $data['has_shop_full'] = !in_array('has_shop_full', PackageManager::$package_limit_list[$package]);
        $data['limit_module'] = json_encode(PackageManager::$package_limit_list[$package], JSON_UNESCAPED_UNICODE);
        $data['duration'] = 10;
        $data['customer_id'] = CUR_APP_ID;

        $pack = CustomerPackage::findFirst('customer_id=' . CUR_APP_ID);
        if ($pack) {
            $pack->update($data);
        } else {
            $pack = new CustomerPackage();
            $pack->create($data);
        }

        $customer = Customers::findFirst('id=' . CUR_APP_ID);
        $custom['industry'] = $industry;
        // 更新进度
        $custom['guide_steps'] = $this->setGuideSteps($customer->guide_steps, "pick");
        // 更新主表
        $customer->update($custom);
        // 更新缓存
        $this->session->set('customer_info', $customer);
        return Ajax::init()->outRight();
    }

    // 自选套餐
    public function customPackage($all_func)
    {
        if (!$all_func) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "请选择您所需要的功能！");
        }
        $total_year_price = PackageManager::FUNC_BASE_PRICE;// 按年
        $total_month_price = PackageManager::FUNC_BASE_PRICE / 10; // 按月(10月算一年)
        $has_shop_base = false;
        $has_shop_full = false;

        // 获取购买的模块
        $module_func = array_filter($all_func, function ($val) {
            if (!in_array($val, array("base", "shop_base", "shop_full"))) {
                return $val;
            }
        });

        // 查找所有模块
        $modules = Resources::find('module_name in ("' . implode('","', $module_func) . '")');
        if ($modules) {
            $modules = $modules->toArray();
            foreach ($modules as $mod) {
                $total_year_price += floatval($mod['year_price']);
                $total_month_price += floatval($mod['month_price']);
            }
        }

        // 完整版商城
        if (in_array('shop_full', $all_func)) {
            $has_shop_full = true;
            $total_year_price += PackageManager::FUNC_SHOP_ALL_PRICE;
            $total_month_price += PackageManager::FUNC_SHOP_ALL_PRICE / 10;
        } else if (in_array('shop_base', $all_func)) {
            // 商城基础版
            $has_shop_base = true;
            $total_year_price += PackageManager::FUNC_SHOP_BASE_PRICE;
            $total_month_price += PackageManager::FUNC_SHOP_BASE_PRICE / 10;
        }

        $pack_infota = array(
            "customer_id" => CUR_APP_ID,
            "package" => 'custom',
            "price_month" => $total_month_price,
            "price_year" => $total_year_price,
            "has_shop_base" => $has_shop_base,
            "has_shop_full" => $has_shop_full,
            "duration" => 10,
            "own_module" => json_encode(array_values($all_func), true)
        );

        $pack = CustomerPackage::findFirst('customer_id=' . CUR_APP_ID);
        if ($pack) {
            $pack->update($pack_infota);
        } else {
            $pack = new CustomerPackage();
            $pack->create($pack_infota);
        }
        $customer = Customers::findFirst('id=' . CUR_APP_ID);
        // 更新进度
        $custom['guide_steps'] = $this->setGuideSteps($customer->guide_steps, "pick");
        // 更新主表
        $customer->update($custom);
        // 更新缓存
        $this->session->set('customer_info', $customer);
        return Ajax::init()->outRight();
    }

    // 生成订单
    public function genOrder($duration)
    {
        if ($duration < 1 || $duration > 12) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "购买时长参数有误！");
        }

        $pack = CustomerPackage::findFirst('customer_id=' . CUR_APP_ID);
        if (!$pack) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "对不起，您尚未选择适合您的套餐咯！");
        }
        $pack_info = $pack->toArray();
        if ($duration < 9) {
            if (!$pack_info['price_month']) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "对不起，该套餐不支持月付！");
            }
            $data['total_money'] = $duration * $pack_info['price_month'];
        } else {
            $data['total_money'] = ($duration - 9) * $pack_info['price_year'];
        }

        // 生成订单
        $unpaid_order = SystemOrders::findFirst(array('customer_id=' . CUR_APP_ID . ' and is_paid=0', 'order' => "ordered desc"));
        $data['package'] = $pack_info['package']; //自定义套餐
        $data['limit_module'] = $pack_info['limit_module']; //行业套餐（限制的模块）
        $data['own_module'] = $pack_info['own_module']; //自选套餐（购买的模块）
        $data['customer_id'] = CUR_APP_ID;
        $data['total_duration'] = $duration;
        $data['ordered'] = time();
        if ($unpaid_order) {
            $order_number = $unpaid_order->order_number;
            if (!$unpaid_order->save($data)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "订单生成失败！");
            }
        } else {
            $order_number = ModuleManager::instance(HOST_KEY)->orderNumberGenerator(ModuleManager::ORDER_NUMBER_PREFIX_AUTO);
            $data['order_number'] = $order_number;

            $order = new SystemOrders();
            if (!$order->create($data)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "订单生成失败！");
            }
        }
        // 更新套餐时长
        $pack->update(array("duration" => $duration));

        // 更新进度
        $customer = Customers::findFirst('id=' . CUR_APP_ID);
        $custom['guide_steps'] = $this->setGuideSteps($customer->guide_steps, "order");
        $customer->update($custom);

        // 更新缓存
        $this->session->set('customer_info', $customer);
        $this->session->set('customer_package', $pack->toArray());
        return Ajax::init()->outRight("", EasyEncrypt::encode($order_number));
    }

    // 生成自定义套餐
    public function genCustomOrder($duration, $all_func)
    {
        if ($duration < 1 || $duration > 12) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "购买时长参数有误！");
        }
        $total_year_price = PackageManager::FUNC_BASE_PRICE;// 按年
        $total_month_price = PackageManager::FUNC_BASE_PRICE / 10; // 按月(10月算一年)
        $has_shop_base = false;
        $has_shop_full = false;
        // 获取购买的模块
        $module_func = array_filter($all_func, function ($val) {
            if (!in_array($val, array("base", "has_shop_base", "has_shop_full"))) {
                return $val;
            }
        });

        // 查找所有模块
        $modules = Resources::find('module_name in ("' . implode('","', $module_func) . '")');
        if ($modules) {
            $modules = $modules->toArray();
            foreach ($modules as $mod) {
                $total_year_price += floatval($mod['year_price']);
                $total_month_price += floatval($mod['month_price']);
            }
        }

        // 完整版商城
        if (in_array('has_shop_full', $all_func)) {
            $has_shop_full = true;
            $total_year_price += PackageManager::FUNC_SHOP_ALL_PRICE;
            $total_month_price += PackageManager::FUNC_SHOP_ALL_PRICE / 10;
        } else if (in_array('has_shop_base', $all_func)) {
            // 商城基础版
            $has_shop_base = true;
            $total_year_price += PackageManager::FUNC_SHOP_BASE_PRICE;
            $total_month_price += PackageManager::FUNC_SHOP_BASE_PRICE / 10;
        }

        // 计算总价
        if ($duration < 9) {
            $data['total_money'] = $duration * $total_month_price;
        } else {
            $data['total_money'] = ($duration - 9) * $total_year_price;
        }

        // 生成订单
        $unpaid_order = SystemOrders::findFirst(array('customer_id=' . CUR_APP_ID . ' and is_paid=0 and package="custom"', 'order' => "ordered desc"));
        $data['package'] = "custom"; //自定义套餐
        $data['customer_id'] = CUR_APP_ID;
        $data['total_duration'] = $duration;
        $data['ordered'] = time();
        if ($unpaid_order) {
            $order_number = $unpaid_order->order_number;
            if (!$unpaid_order->save($data)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "订单生成失败！");
            }
        } else {
            $order_number = ModuleManager::instance(HOST_KEY)->orderNumberGenerator(ModuleManager::ORDER_NUMBER_PREFIX_AUTO);
            $data['order_number'] = $order_number;

            $order = new SystemOrders();
            if (!$order->create($data)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "订单生成失败！");
            }
        }
        $pack = CustomerPackage::findFirst('customer_id=' . CUR_APP_ID);
        $pack_infota = array(
            "customer_id" => CUR_APP_ID,
            "package" => 'custom',
            "price_month" => $total_month_price,
            "price_year" => $total_year_price,
            "has_shop_base" => $has_shop_base,
            "has_shop_full" => $has_shop_full,
            "duration" => $duration,
            "own_module" => json_encode(array_values($all_func), true)
        );

        if ($pack) {
            $pack->update($pack_infota);
        } else {
            $pack = new CustomerPackage();
            $pack->create($pack_infota);
        }

        // 更新进度
        $customer = Customers::findFirst('id=' . CUR_APP_ID);
        $customer->guide_steps = $this->setGuideSteps($customer->guide_steps, "pick");
        $custom['guide_steps'] = $this->setGuideSteps($customer->guide_steps, "order");

        $customer->update($custom);
        // 更新缓存
        $this->session->set('customer_info', $customer);
        return Ajax::init()->outRight("", EasyEncrypt::encode($order_number));
    }

    public function applyO2O()
    {
        $apply = O2oCustomerApply::findFirst('customer_id=' . CUR_APP_ID);

        if (!$apply) {
            $apply = new O2oCustomerApply();
            $data['customer_id'] = CUR_APP_ID;
            $data['created'] = time();
            $apply->create($data);
        } else {
            // 审核而且通过
            if ($apply->status == 1) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '你已经的申请已经通过，无需再次提交！');
            } else {
                $data['status'] = 0;
                $data['modified'] = time();
                $apply->save($data);
            }
        }

        return Ajax::init()->outRight();
    }

    // 登出
    public function logout()
    {
        $this->session->remove('customer_auth');
        $this->session->remove('customer_info');
        $this->session->remove('customer_wechat');
        $this->session->remove('customer_weibo');
        $this->session->destroy();
    }
} 