<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 2014/11/24
 * Time: 17:36
 */

namespace Components\Order;


use Components\Module\CouponManager;
use Components\Module\VipcardManager;
use Components\Product\ProductManager;
use Components\Product\TransactionManager;
use Components\UserStatus;
use Components\UserStatusWap;
use Library\O2O\O2oShareManager;
use Models\Modules\Pointmall\AddonPointMallItem;
use Models\Modules\Pointmall\AddonPointMallOrder;
use Models\Product\Product;
use Models\Product\ProductSpec;
use Models\Shop\Shop;
use Models\Shop\ShopCart;
use Models\Shop\ShopOrderCombine;
use Models\Shop\ShopOrderItem;
use Models\Shop\ShopOrders;
use Models\User\UserForCustomers;
use Models\User\UserPointLog;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use Util\Ajax;
use Util\Cookie;

class OrderGenerator extends Component
{
    const ERROR_FAIL_CREATE_ORDER = '创建订单信息失败！';
    const ERROR_FAIL_CREATE_ORDER_ITEM = '创建订单数据失败！';
    const ERROR_FAIL_PRODUCT_NO_FOUND = '商品未找到！';
    const ERROR_FAIL_INVALID_PARAM = '错误的参数！';
    const ERROR_FAIL_NOT_ENOUGH_INVENTORY = '库存不足！';
    const ERROR_FAIL_UPDATE_INVENTORY = '更新库存失败！';

    private static $instance = null;
    private static $_last_err = '';
    private static $_total_cash = 0;
    private static $_paid_cash = 0;
    private static $_total_freight_fee = 0;
    private static $_share_item_list = []; // 用于分享返现商品列表
    private static $_combine_order_number = "";//组合订单号
    private static $_final_order_number = ""; //最终订单号

    public static function init()
    {
        self::$_last_err = '';
        self::$_total_cash = 0;
        self::$_paid_cash = 0;
        self::$_total_freight_fee = 0;
        self::$_share_item_list = [];
        self::$_combine_order_number = '';
        self::$_final_order_number = '';

        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // 生成订单，保存到session
    public function generateOrderNumber()
    {
        $t = explode(' ', microtime());
        $strtime = $t[1];

        # 时间戳后四位+micortime+时间戳前6位随随机
        $o = substr($strtime, 6) . substr($t[0], 2, 6) . mt_rand(substr($strtime, 0, 6), 999999);

        return $o;
    }

    // 生成组合订单号
    public function genCombineNumber($frieght_arr)
    {
        // 单个商家多种邮寄方式，生成多订单
        $single_customer_amount = @count($frieght_arr);

        if ($single_customer_amount > 1) {
            self::$_combine_order_number = 'P' . self::generateOrderNumber();
            $order_count = ShopOrderCombine::count("combine_order_number='" . self::$_combine_order_number . "'");
            if ($order_count >= 1) // 订单号不能重复
            {
                self::$_combine_order_number = 'P' . self::generateOrderNumber();
            }
        }
    }

    // 结算
    public function settle()
    {
        $user_id = UserStatus::getUid();
        if (!$user_id) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }
        $all_item = $this->request->getPost('t');
        $addr = $this->request->getPost('addr');
        $remark = $this->request->getPost('remark');
        $freight = $this->request->getPost('freight'); // 组合订单运费模式（有序的）
        $coupon_serial = $this->request->getPost('coupon');
        $use_point = $this->request->getPost('use_point');
        $pick_store_id = $this->request->getPost('pick_store_id');
        $all_item = json_decode(base64_decode($all_item), true);
        if (!($all_item && $addr)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单里无商品，无法生成订单！');
        }
        $flag_order_number = $this->session->get('_order_user_' . $user_id);
        if (!$flag_order_number) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单已经失效，请不要重复提交订单！');
        }

        // 自取订单
        if ($pick_store_id) {
            // 自取订单
            if (!$this->genPickOrder($pick_store_id, $all_item, $coupon_serial, $remark, $use_point)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, self::$_last_err);
            }
        }

        // 生成组合订单
        $this->genCombineNumber($freight);

        // 1. 不存在组合订单，则生成单个订单
        if (!self::$_combine_order_number) {

            // 单个订单
            if (!$this->genSingleOrder($all_item, $addr, $freight, $coupon_serial, $remark, $use_point)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, self::$_last_err);
            }

            return self::$_final_order_number;
        }

        # 多订单模式
        $this->db->begin();
        try {
            // 网络订单
            $res = $this->genCustomerOrder($all_item, $addr, $remark, $freight);
            if (!$res) {
                throw new Exception(self::$_last_err);
            }

            // 最后生成组合订单
            if (!$this->genCombineOrder($addr, $use_point, $coupon_serial)) {
                throw new Exception(self::$_last_err);
            };

            $this->db->commit();

            # 最终订单号
            self::$_final_order_number = self::$_combine_order_number;
            // 删除订单号session信息 避免重复提交
            $this->session->remove('_order_user_' . $user_id);
            // 清除信息避免重复
            Cookie::del('_cart_settle_page');
            unset($_SERVER['HTTP_REFERER']);
        } catch (\Exception $e) {
            $messages = $e->getMessage();
            $this->db->rollback();
            $this->di->get("errorLogger")->error($messages);
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $messages);
        }

        return Ajax::init()->outRight('', self::$_final_order_number);
    }

    // 按商家生成订单
    private function genCustomerOrder($all_item, $addr, $remark, $_freight)
    {
        $store = Shop::findFirst('customer_id=' . CUR_APP_ID)->toArray();

        $user_id = UserStatusWap::getUid();
        # todo 会员卡功能
        $vip_discount = VipcardManager::init()->getCardDiscount($user_id, CUR_APP_ID);
        $all_item = array_values($all_item);
        // 按邮寄方式生成单独订单
        foreach ($all_item as $k => $_data) {
            $order_number = self::generateOrderNumber();
            $order_count = ShopOrderItem::count('order_number="' . $order_number . '"');
            if ($order_count >= 1) {
                //订单号不能重复
                $order_number = self::generateOrderNumber();
            }

            $temp_paid_cash = 0;//订单支付金额(当前物流订单)
            $temp_total_cash = 0;//订单总金额(当前物流订单)

            // 1. 生成订单商品列表
            foreach ($_data['goods'] as $goods) {
                if (!$this->createOrderItem($order_number, $goods, $vip_discount, $temp_total_cash, $temp_paid_cash)) {
                    return false;
                }
            }

            // 邮费
            $_freight_type = $_freight[$k];
            $_freight_fee = $store['free_postage'] > 0 && $store['free_postage'] > $temp_paid_cash ? 0 : floatval($_data['freight_tpl'][$_freight_type]['total_freight_money']);
            // 获取订单奖励积分
            $orders_proportion = $store['orders_proportion'];
            $award_points = 0;
            if ($orders_proportion > 0) {
                $award_points = floor($temp_paid_cash / $orders_proportion);
            }

            // 2 生成订单
            $orderData = array(
                'order_number' => $order_number,
                'customer_id' => CUR_APP_ID,
                'user_id' => $user_id,
                'created' => time(),
                'remark' => $remark,
                'address_id' => $addr,
                'is_paid' => 0,
                'total_cash' => $temp_total_cash,
                'paid_cash' => $temp_paid_cash, // 最终支付的价格
                'logistics_fee' => $_freight_fee,
                'freight_type' => $_freight_type,
                'order_award_points' => $award_points,
                'use_coupon' => '',
                'use_point' => 0,
                'combine_order_number' => self::$_combine_order_number,
                'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY
            );

            $order = new ShopOrders();
            if (!$order->create($orderData)) {
                $messages = '';
                foreach ($order->getMessages() as $message) {
                    $messages[] = $message;
                }
                self::$_last_err = self::ERROR_FAIL_CREATE_ORDER;
                return false;
            }

            // 总计
            self::$_total_cash += $temp_total_cash; // 总额
            self::$_paid_cash += $temp_paid_cash; // 总支付
            self::$_total_freight_fee += $_freight_fee;// 总邮费

            // 清空购物车
            $this->clearCart();
            // 分享返积分记录
            O2oShareManager::init()->addOrderLog(self::$_share_item_list, $order_number, "app");
        }

        return true;
    }


    // 生成组合订单
    public function genCombineOrder($addr, $use_point, $coupon_serial)
    {
        // 优惠劵
        $coupon_minus = 0;

        if (strlen(trim($coupon_serial)) == 16) {
            $coupon_minus = CouponManager::getCouponDiscountBySerial(trim($coupon_serial), self::$_paid_cash);
            if (!$coupon_minus) {
                $coupon_serial = '';
            }
        } else {
            $coupon_serial = '';
        }
        // 使用优惠劵后
        self::$_paid_cash = self::$_paid_cash - $coupon_minus;

        $store = Shop::findFirst('customer_id=' . CUR_APP_ID)->toArray();
        $redeem_proportion = $store['redeem_proportion'];
        // 使用积分
        $point = UserStatusWap::init()->getPoint();
        $pay_point = 0;
        if (self::$_paid_cash > 0 && $use_point > 0) {
            //输入的积分数大于需要扣除的积分数
            if ($point > 0 && $redeem_proportion > 0 && ($use_point / $redeem_proportion) >= (self::$_paid_cash + self::$_total_freight_fee)) {
                $pay_point = ceil((self::$_paid_cash + self::$_total_freight_fee) * $redeem_proportion);
                self::$_paid_cash = 0;
            } else {
                self::$_paid_cash = self::$_paid_cash - round($use_point / $redeem_proportion, 2);
                $pay_point = $use_point;
            }
        }
        $user_id = UserStatusWap::getUid();
        $shopOrderCombine = new ShopOrderCombine();
        $shopOrderCombine->customer_id = CUR_APP_ID;
        $shopOrderCombine->combine_order_number = self::$_combine_order_number;
        $shopOrderCombine->logistics_fee = self::$_total_freight_fee;
        $shopOrderCombine->user_id = $user_id;
        $shopOrderCombine->createed = time();
        $shopOrderCombine->total_cash = self::$_total_cash;
        $shopOrderCombine->paid_cash = self::$_paid_cash;
        $shopOrderCombine->address_id = $addr;
        $shopOrderCombine->use_point = $pay_point;
        $shopOrderCombine->logistics_fee = self::$_total_freight_fee;
        $shopOrderCombine->coupon_serial = $coupon_serial;

        // 无需支付情况
        if ((self::$_paid_cash + self::$_total_freight_fee) == 0) {
            $shopOrderCombine->is_paid = 1;
            $shopOrderCombine->status = TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS;
        } else {
            $shopOrderCombine->is_paid = 0;
            $shopOrderCombine->status = TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY;
        }

        if (!$shopOrderCombine->create()) {
            $messages = '';
            foreach ($shopOrderCombine->getMessages() as $message) {
                $messages[] = $message;
            }
            self::$_last_err = self::ERROR_FAIL_CREATE_ORDER;
            return false;
        }

        // 优惠券订单
        CouponManager::init()->generateCouponOrder($coupon_serial, self::$_combine_order_number, $coupon_minus);
        // 清空购物车
        $this->clearCart();
        //更新用户积分数
        if (!$this->updateUserPoints($user_id, $use_point)) {
            return false;
        };
        return true;
    }

    // 自取订单
    public function genPickOrder($pick_store_id, $all_item, $coupon_serial, $remark, $use_point)
    {
        return true;
    }

    // 单独订单
    public function genSingleOrder($all_item, $addr, $_freight, $coupon_serial, $remark, $use_point)
    {
        $store = Shop::findFirst('customer_id=' . CUR_APP_ID)->toArray();
        $redeem_proportion = $store['redeem_proportion'];
        $user_id = UserStatusWap::getUid();
        $vip_discount = VipcardManager::init()->getCardDiscount($user_id, CUR_APP_ID);
        $all_item = array_values($all_item);
        // 开始生成订单
        $this->db->begin();
        try {
            // 按邮寄方式生成单独订单
            foreach ($all_item as $k => $_data) {
                $order_number = self::generateOrderNumber();
                $order_count = ShopOrders::count('order_number="' . $order_number . '"');
                if ($order_count >= 1) {
                    //订单号不能重复
                    $order_number = self::generateOrderNumber();
                }
                $temp_paid_cash = 0;//订单支付金额(当前物流订单)
                $temp_total_cash = 0;//订单总金额(当前物流订单)
                $temp_vip_preferential_amount = 0;//订单vip优惠总金额(当前物流订单)

                // 1. 生成订单商品列表
                foreach ($_data['goods'] as $k1 => $goods) {
                    if (!$this->createOrderItem($order_number, $goods, $vip_discount, $temp_total_cash, $temp_paid_cash)) {
                        throw new Exception(self::$_last_err);
                    }
                }

                // 邮费
                $_freight_type = $_freight[$k];
                $_freight_fee = $store['free_postage'] > 0 && $store['free_postage'] > $temp_paid_cash ? 0 : floatval($_data['freight_tpl'][$_freight_type]['total_freight_money']);

                // 获取订单奖励积分
                $orders_proportion = $store['orders_proportion'];
                $award_points = 0;
                if ($orders_proportion > 0) {
                    $award_points = floor($temp_paid_cash / $orders_proportion);
                }

                // 优惠劵
                $coupon_minus = 0;
                if (strlen(trim($coupon_serial)) == 16) {
                    $coupon_minus = CouponManager::getCouponDiscountBySerial(trim($coupon_serial), $temp_paid_cash);
                    if (!$coupon_minus) {
                        $coupon_serial = '';
                    }
                } else {
                    $coupon_serial = '';
                }
                // 使用优惠劵后
                $temp_paid_cash = $temp_paid_cash - $coupon_minus;

                // 使用积分
                $point = UserStatusWap::init()->getPoint();
                $pay_point = 0;
                if ($temp_paid_cash > 0 && $use_point > 0) {
                    //输入的积分数大于需要扣除的积分数
                    if ($point > 0 && $redeem_proportion > 0 && ($use_point / $redeem_proportion) >= ($temp_paid_cash + $_freight_fee)) {
                        $pay_point = ceil(($temp_paid_cash + $_freight_fee) * $redeem_proportion);
                        $temp_paid_cash = 0;
                        $post_fee = 0;
                        $point_pay_all = true;
                    } else {
                        $temp_paid_cash = $temp_paid_cash - round($use_point / $redeem_proportion, 2);
                        $pay_point = $use_point;
                    }
                }

                // 总计
                self::$_total_cash += $temp_total_cash; // 总额
                self::$_paid_cash += $temp_paid_cash; // 总支付
                self::$_total_freight_fee += $_freight_fee;// 总邮费

                // 2 生成订单
                $orderData = array(
                    'order_number' => $order_number,
                    'customer_id' => CUR_APP_ID,
                    'user_id' => $user_id,
                    'created' => time(),
                    'remark' => $remark,
                    'address_id' => $addr,
                    'total_cash' => $temp_total_cash,
                    'paid_cash' => $temp_paid_cash, // 最终支付的价格
                    'logistics_fee' => $_freight_fee,
                    'freight_type' => $_freight_type,
                    'discount_cash' => $temp_vip_preferential_amount, //优惠多少
                    'order_award_points' => $award_points,
                    'use_coupon' => $coupon_serial,
                    'use_point' => $pay_point,
                    'combine_order_number' => self::$_combine_order_number,
                    'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY
                );

                // 无需支付情况
                if ((self::$_paid_cash + $_freight_fee) == 0) {
                    $orderData['is_paid'] = 1;
                    $orderData['status'] = TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS;
                } else {
                    $orderData['is_paid'] = 0;
                    $orderData['status'] = TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY;
                }

                $order = new ShopOrders();
                if (!$order->create($orderData)) {
                    $messages = '';
                    foreach ($order->getMessages() as $message) {
                        $messages[] = $message;
                    }
                    self::$_last_err = self::ERROR_FAIL_CREATE_ORDER;
                    throw new Exception(self::$_last_err);
                }

                // 优惠券订单
                CouponManager::init()->generateCouponOrder($coupon_serial, $order_number, $coupon_minus);
                // 扣除积分
                if ($pay_point) {
                    $this->updateUserPoints($user_id, $pay_point);
                }

                // 最终订单号
                self::$_final_order_number = $order_number;
                // 分享返积分记录
                O2oShareManager::init()->addOrderLog(self::$_share_item_list, $order_number, "wap");
            }

            // 清空购物车
            $this->clearCart();

            $this->db->commit();

            // 4 删除订单号session信息 避免重复提交
            $this->session->remove('_order_user_' . $user_id);

            // 清除信息避免重复
            Cookie::del('_cart_settle_page');
            unset($_SERVER['HTTP_REFERER']);
        } catch (\Exception $e) {
            $messages = $e->getMessage();
            $this->db->rollback();
            $this->di->get("errorLogger")->error($messages);
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, self::$_last_err);
        }

        return Ajax::init()->outRight('', self::$_final_order_number);
    }

    // 创建订单商品数据
    private function createOrderItem($order_number, $goods, $vip_discount, &$total_cash, &$paid_cash)
    {
        $product = ProductManager::instance()->getProduct($goods['item_id'], CUR_APP_ID);
        if (!$product) {
            self::$_last_err = self::ERROR_FAIL_PRODUCT_NO_FOUND;
            return false;
        }

        // 返现信息列表
        self::$_share_item_list[$goods['item_id']] = array('num' => $goods['num'], 'sell_price' => $product['sell_price']);
        // 售价
        $sell_price = $product['sell_price'];
        // 原价
        $original_price = $product['original_price'];
        // 最终进行计算的价格
        $item_new['item_sell_price'] = $sell_price;
        $item_new['total_preferential'] = ($product['original_price'] - $sell_price) * $goods['num'];


        // 商品规格 vip价格（原始价vip折扣与售价取最低）
        if ($goods['spec']) {
            $spec = isset($goods['spec']) ? ProductManager::instance()->getProductSpecData($goods['item_id'], $goods['spec']) : [];
            $item_new['item_sell_price'] = $spec['sell_price'];
            $original_price = $spec['original_price'];

            if (($vip_discount * $spec['original_price'] / 100) < $spec['sell_price']) //折扣后的价格比优惠价格更低
            {
                $item_new['item_sell_price'] = $vip_discount * $spec['original_price'] / 100;
                $item_new['total_preferential'] = ($spec['original_price'] - $sell_price) * $goods['num'];
            }
        } else if (($vip_discount * $product['original_price'] / 100) < $product['sell_price']) {
            //折扣后的价格比优惠价格更低
            $item_new['item_sell_price'] = $vip_discount * $product['original_price'] / 100;
            $item_new['total_preferential'] = ($product['original_price'] - $sell_price) * $goods['num'];
        }

        // 数据信息
        $item_new['user_id'] = UserStatusWap::getUid();
        $item_new['customer_id'] = CUR_APP_ID;
        $item_new['order_number'] = $order_number;
        $item_new['combine_order_number'] = self::$_combine_order_number;
        $item_new['item_id'] = $goods['item_id'];
        $item_new['item_name'] = $goods['name'];
        $item_new['item_price'] = $sell_price;
        $item_new['item_original_price'] = $original_price;
        $item_new['item_spec'] = $goods['spec'];
        $item_new['item_spec_data'] = $goods['spec_data'];
        $item_new['quantity'] = $goods['num'];
        $item_new['thumb'] = $goods['thumb'];
        $item_new['total_cash'] = $goods['num'] * $sell_price;
        $item_new['created'] = $goods['created'];
        $orderItem = new ShopOrderItem();
        if (!$orderItem->create($item_new)) {
            $messages = [];
            foreach ($orderItem->getMessages() as $message) {
                $messages[] = $message;
            }
            self::$_last_err = self::ERROR_FAIL_CREATE_ORDER_ITEM;
            return false;
        };

        // 减库存
        if (!$this->minusInventory($goods['item_id'], $goods['spec'], $goods['num'])) {
            return false;
        };

        // 累计
        $total_cash += $item_new['quantity'] * $product['original_price'];
        $paid_cash += $item_new['quantity'] * $item_new['item_sell_price'];

        return true;
    }

    // 减库存
    public function minusInventory($item_id, $spec, $num)
    {
        // 更新当前规格库存
        $_prodSpec = ProductSpec::findFirst('spec_unique_key="' . $spec . '" and product_id=' . $item_id);
        if ($_prodSpec) {
            if ($_prodSpec->num < $num) {
                self::$_last_err = self::ERROR_FAIL_NOT_ENOUGH_INVENTORY;
                return false;
            }
            if (!$_prodSpec->update(array('num' => $_prodSpec->num - $num))) {
                self::$_last_err = self::ERROR_FAIL_UPDATE_INVENTORY;
                return false;
            }
        }
        // 更新商品总库存
        $prod = Product::findFirst('id=' . $item_id);
        if ($prod->quantity < $num) {
            self::$_last_err = self::ERROR_FAIL_NOT_ENOUGH_INVENTORY;
            return false;
        }
        if (!$prod->update(array('quantity' => $prod->quantity - $num))) {
            self::$_last_err = self::ERROR_FAIL_UPDATE_INVENTORY;
            return false;
        }
        return true;
    }

    // 清空购物车
    public function clearCart()
    {
        $user_id = UserStatusWap::getUid();
        $_ids = array_keys(self::$_share_item_list);
        $list = ShopCart::find('item_id in (' . implode(',', $_ids) . ' ) and user_id=' . $user_id);
        if (!$list) {
            return;
        }
        foreach ($list as $item) {
            $item->delete();
        }
    }

    // 积分商城订单
    public function pointMallSettle()
    {
        $user_id = UserStatus::getUid();
        if (!$user_id) {
            return Ajax::init()->outError(Ajax::ERROR_USER_HAS_NOT_LOGIN);
        }
        $item_info = $this->request->getPost('t');
        $addr = $this->request->getPost('addr');

        $this->ajax = new Ajax();

        $buy_item = json_decode(base64_decode($item_info), true);
        if (!($buy_item && $addr)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM, '订单里无商品，无法生成订单！');
        }

        $order_number = $this->session->get('_order_user_' . $user_id);
        if (!$order_number) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单已经失效，请不要重复提交订单！');
        }

        $product = Product::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $buy_item['id']);
        if (!$product) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '未找到对应商品，订单失效！');
        }

        $pointMallItem = AddonPointMallItem::findFirst('item_id=' . $buy_item['id']);
        if (!$pointMallItem) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '未找到积分换购商品，订单失效！');
        }

        // 再次判断积分
        $user = UserForCustomers::findFirst('customer_id=' . CUR_APP_ID . ' and user_id=' . $user_id);
        if (!$user) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '无法找到用户信息');
        }

        // 计算
        $total_point = $pointMallItem->cost_point * $buy_item['num'];
        $total_cash = $pointMallItem->cost_cash * $buy_item['num'];

        if ($user->points_available < $total_point) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '对不起！您的积分不够，不能生成订单');
        }

        $post_fee = $pointMallItem->postage_fee;
        $paid = 0;
        // 开始生成订单
        $this->db->begin();
        try {
            $item['order_number'] = $order_number;
            $item['item_id'] = $buy_item['id'];
            $item['item_name'] = $pointMallItem->item_name;
            $item['item_price'] = $pointMallItem->cost_cash; // 最终价格（可能被改价）
            $item['item_sell_price'] = $product->sell_price;
            $item['item_original_price'] = $product->original_price;
            $item['quantity'] = $buy_item['num'];
            $item['thumb'] = $product->thumb;
            $item['total_cash'] = $total_cash;
            $item['item_spec'] = $buy_item['spec'];
            $item['item_spec_data'] = ProductManager::instance()->getProductSpec($buy_item['id']);
            $item['total_preferential'] = 0;
            // 商品型号
            $item['spec'] = json_encode($buy_item['spec'], JSON_UNESCAPED_UNICODE);
            $item['created'] = time();

            // 加入下单列表
            $orderItem = new ShopOrderItem();
            if (!$orderItem->save($item)) {
                self::$_last_err = self::ERROR_FAIL_CREATE_ORDER_ITEM;
                throw new Exception(self::$_last_err);
            };

            // 更新原始商品库存
            // 减库存
            if (!$this->minusInventory($buy_item['id'], $buy_item['spec'], $buy_item['num'])) {
                throw new Exception(self::$_last_err);
            };

            // 更新换购商品库存
            if (!$pointMallItem->update(array('item_quantity' => $pointMallItem->item_quantity - $buy_item['num']))) {
                throw new Exception(self::ERROR_FAIL_UPDATE_INVENTORY);
            }

            // 2 生成原始订单
            $orderData = array(
                'order_number' => $order_number,
                'customer_id' => CUR_APP_ID,
                'user_id' => $user_id,
                'created' => time(),
                'address_id' => $addr,
                'total_cash' => $product->sell_price * $buy_item['num'],
                'paid_cash' => $total_cash, // 最终优惠后的价格
                'logistics_fee' => $post_fee,
                'discount_cash' => 0, //优惠多少
                'back_coin' => 0, // 返回积分
                'is_pointmall' => 1,
                'use_point' => $total_point,
            );

            // 是否等待付款
            if ($total_cash + $post_fee == 0) {
                $orderData['is_paid'] = 1;
                $orderData['status'] = TransactionManager::ORDER_STATUS_ESCOW_PAID;
                // 输出
                $paid = 1;
            } else {
                $orderData['is_paid'] = 0;
                $orderData['status'] = TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY;
            }

            $order = new ShopOrders();
            if (!$order->save($orderData)) {
                throw new Exception(self::ERROR_FAIL_CREATE_ORDER);
            }

            // 3 生成积分商城订单
            $pointData = array(
                'order_number' => $order_number,
                'customer_id' => CUR_APP_ID,
                'user_id' => $user_id,
                'item_id' => $buy_item['id'],
                'num' => $buy_item['num'],
                'total_point' => $total_point,
                'total_cash' => $total_cash,
                'created' => time()
            );

            $order = new AddonPointMallOrder();
            if (!$order->save($pointData)) {
                throw new Exception(self::ERROR_FAIL_CREATE_ORDER);
            }

            // 扣除用户积分
            $this->updateUserPoints($user_id, $total_point);

            // todo 积分日志
            $pointLog = new UserPointLog();
            $data = array(
                "customer_id" => CUR_APP_ID,
                "user_id" => $user_id,
                "in_out" => "out",
                "action" => "积分换购",
                "action_desc" => "积分换购",
                "logged" => time(),
                "value" => $total_point
            );
            $pointLog->create($data);

            // 删除订单号session信息 避免重复提交
            $this->session->remove('_order_user_' . $user_id);

            $this->db->commit();

            // 清除信息避免重复
            Cookie::del('_cart_settle_page');
            unset($_SERVER['HTTP_REFERER']);
        } catch (\Exception $e) {
            $this->db->rollback();
            $messages[] = $e->getMessage();
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, join("<br>", $messages));
        }

        return Ajax::init()->outRight('', array('order_number' => $order_number, 'paid' => $paid));
    }

    /*积分支付后更新用户的可用积分*/
    public function  updateUserPoints($user_id, $points)
    {
        if ($points > 0) {
            $user = UserForCustomers::findFirst('customer_id=' . CUR_APP_ID . " AND user_id=" . $user_id);
            $user->points_available = $user->points_available - $points >= 0 ? $user->points_available - $points : 0;
            $user->points = $user->points - $points >= 0 ? $user->points - $points : 0;
            return $user->update();
        }
        return true;
    }

} 