<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-12
 * Time: 下午2:55
 */

namespace Components\Order;


use Components\Module\VipcardManager;
use Components\Product\TransactionManager;
use Components\Rules\PointRule;
use Components\UserStatus;
use Models\Modules\Pointmall\AddonPointMallOrder;
use Models\Product\Product;
use Models\Product\ProductComment;
use Models\Product\ProductSpec;
use Models\Shop\ShopOrderItem;
use Models\Shop\ShopOrderReturn;
use Models\Shop\ShopOrders;
use Models\User\UserForCustomers;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use Util\Ajax;
use Util\EasyEncrypt;

// wap版下单
class OrderManager extends Component
{
    private static $instance = null;

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    //确认收货
    public function confirmReceipt()
    {
        $uid = UserStatus::getUid();
        $order_number = EasyEncrypt::decode($this->request->getPost('order'));
        if (!$order_number) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        // 没有被删除的订单
        $where = 'is_deleted = 0 and customer_id = ' . CUR_APP_ID . ' and user_id=' . $uid . ' and order_number="' . $order_number . '"';
        $order = ShopOrders::findFirst($where);
        if (!$order) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '订单未找到！');
        }

        // todo 判断订单状态
        if (!in_array($order->status, array(TransactionManager::ORDER_STATUS_WAIT_BUYER_CONFIRM_GOODS, TransactionManager::ORDER_STATUS_SELLER_DELIVERED))) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单已经完结，订单状态：' . TransactionManager::getOrderStatus($order->status));
        }

        $this->db->begin();
        try {
            // 更新订单状态
            if (!$order->update(array('status' => TransactionManager::ORDER_STATUS_TRADE_SUCCESS, 'received_time' => time()))) {
                $messages = array();
                foreach ($order->getMessages() as $message) {
                    $messages[] = $message;
                }
                throw new Exception(join(',', $messages));
            }

            // todo 担保交易处理

            // 返现
            /*  if (!UserWalletManager::init($uid)->settleCashBack($order_number)) {
                  throw new Exception(join(',', [UserWalletManager::$_err_msg]));
              }*/

            // 订单送积分+记录一下
            $ruleMng = PointRule::init(CUR_APP_ID, $uid);
            $ruleMng->executeRule($uid, PointRule::BEHAVIOR_TRADE_ONLINE);
            // ...
            $this->db->commit();

        } catch (Exception $e) {
            $this->db->rollback();
            $messages[] = $e->getMessage();
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, join("<br>", $messages));
        }

        return Ajax::init()->outRight('');
    }

    /*获取订单里的商品信息*/
    public function getOrderItem($order)
    {
        $list = $this->db->query("Select soi.*,ifnull(sor.type,'0') as order_type,
              sor.is_seller_accept,sor.is_end
              from shop_order_item as soi left join  shop_order_return as sor ON soi.id=sor.order_item_id WHERE soi.order_number=" . $order['order_number'])
            ->fetchAll();
        foreach ($list as $k => $val) {
            if (($order['status'] == TransactionManager::ORDER_STATUS_TRADE_SUCCESS) && (((time() - $order['received_time']) / (60 * 60 * 24)) < 7) && ($order['use_point'] <= 0) && ($val['order_type'] == 0)) {
                $list[$k]['return_show'] = true;
            } else {
                $list[$k]['return_show'] = false;
            }
        }
        return $list;
    }

    /*
     * 检查商品是否符合退换货条件
     * @item_id int shop_order_item表的主键id
      @type int 0-退货或者换货,1-换货,2-退货
    */
    public function checkReturnProduct($item_id, $type = 0)
    {
        $item = ShopOrderItem::findFirst('id=' . $item_id);
        if (!$item) {
            return null;
        } else {
            $order = ShopOrders::findFirst('order_number=' . $item->order_number);
            if ($order && $order->status == TransactionManager::ORDER_STATUS_TRADE_SUCCESS && ((time() - $order->received_time) / (60 * 60 * 24)) < 7 && $order->use_point <= 0) {
                return $item->toArray();
            }
            return null;
        }
    }

    /*获取退/换货商品列表*/
    public function getReturnGoods($customer_id, $user_id)
    {
        $list = $this->db->query(
            "select sor.*,soi.created,soi.thumb,soi.item_spec_data,soi.item_name,soi.total_cash,soi.item_price from
              shop_order_return as sor left join shop_order_item as soi on sor.order_item_id=soi.id
               where sor.user_id=" . $user_id . " and sor.customer_id=" . $customer_id
        )->fetchAll();
        //  $list=ShopOrderReturn::find("user_id=".$user_id." and customer_id=".$customer_id);
        //  $list =$list ? $list->toArray() :[];
        foreach ($list as $k => $v) {
            if ($v['type'] == 2) //退货
            {
                if ($v['is_seller_accept'] == 0) //未处理
                {
                    $list[$k]['button'] = "<span class='cancelBack btn'>取消退货</span><span class='btn waitCheck'>待审核</span>";
                } else if ($v['is_seller_accept'] == 2) //拒绝
                {
                    $list[$k]['button'] = "<span class='btn refuseBack'>卖家拒绝退货</span>";
                } else {
                    if ($v['is_end'] == 1) //流程已结束
                    {
                        if ($v['is_seller_refunded'] == 1) {
                            $list[$k]['button'] = "<span class='handleOver btn'>退款成功</span>";
                        }

                    } else if ($v['is_buyer_delivered'] == 0) //买家未发货
                    {

                        $list[$k]['button'] = "<span class='cancelBack btn'>取消退货</span><span class='backGood btn'>回寄商品</span>";
                    } else if ($v['seller_received_goods'] == 0) //卖家未收到货物
                    {
                        $list[$k]['button'] = "<span class='waitReceive btn'>等待卖家收货</span>";
                    } else if ($v['is_seller_refunded'] == 0) //卖家未退款
                    {
                        $list[$k]['button'] = "<span class='waitPay btn'>等待卖家退款</span>";
                    } else if ($v['is_send'] == 0) //流程未结束
                    {
                        $list[$k]['button'] = "<span class='waitCheck btn'>待审核</span>";
                    } else {
                        $list[$k]['button'] = "<span class='waitCheck btn'>待审核</span>";
                    }
                }
            }
            if ($v['type'] == 1) //换货
            {

                if ($v['is_seller_accept'] == 0) //未处理
                {
                    $list[$k]['button'] = "<span class='cancelBack btn'>取消换货</span><span class='btn waitCheck'>待审核</span>";
                } else if ($v['is_seller_accept'] == 2) //拒绝
                {
                    $list[$k]['button'] = "<span class='btn refuseBack'>卖家拒绝换货</span>";
                } else {
                    if ($v['is_end'] == 1) //流程已结束
                    {
                        if ($v['back_order_number'] != 0) {
                            $list[$k]['button'] = "<span class='handleOver btn'>换货完成</span>";
                        } else if ($v['is_seller_refunded'] == 1) {
                            $list[$k]['button'] = "<span class='handleOver btn'>退款成功</span>";
                        }

                    } else if ($v['is_buyer_delivered'] == 0) //买家未发货
                    {
                        $list[$k]['button'] = "<span class='cancelBack btn'>取消换货</span><span class='backGood btn'>回寄商品</span>";
                    } else if ($v['seller_received_goods'] == 0) //卖家未收到货物
                    {
                        $list[$k]['button'] = "<span class='waitReceive btn'>等待卖家收货</span>";
                    } else if ($v['is_seller_refunded'] == 0) //卖家未退款
                    {
                        $list[$k]['button'] = "<span class='waitPay btn'>等待卖家退款</span>";
                    } else if ($v['is_seller_refunded'] == 1 && $v['is_send'] == 0) //已退款,但是还没有结束
                    {
                        $list[$k]['button'] = "<span class='waitPay btn'>换货处理中</span>";
                    } else {
                        $list[$k]['button'] = "<span class='waitCheck btn'>待审核</span>";
                    }
                }

            }

        }
        // echo "<pre>";var_dump($list);exit;
        return $list;
    }

    // 取消订单
    public function cancalOrder()
    {
        $user_id = UserStatus::getUid();
        $order_number = $this->request->get('order');
        if (!$order_number) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $where = 'customer_id=' . CUR_APP_ID . ' and user_id=' . $user_id . ' and order_number = "' . $order_number . '"';
        $order = ShopOrders::findFirst($where);

        if (!$order) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        if (($order->is_cod && $order->status == TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS) || (!$order->is_cod && $order->status == TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY)) {
            $order->status = TransactionManager::ORDER_STATUS_BUYER_CANCELED; // 订单作废，不在前台显示

            if (!$order->update()) {
                return Ajax::init()->outError(Ajax::ERROR_NOTHING_HAS_CHANGED);
            }

            // 商品库存回滚
            $list = ShopOrderItem::find('order_number="' . $order_number . '"');
            foreach ($list as $row) {
                // 商品总数
                $item = Product::findFirst('id=' . $row->item_id);
                if ($item) {
                    $item->update(array('quantity' => $item->quantity + $row->quantity));
                }

                // 商品规格
                if ($row->item_spec && strlen(($row->item_spec) > 0)) {
                    $spec = ProductSpec::findFirst('product_id=' . $row->item_id . ' and spec_unique_key="' . $row->item_spec . '"');
                    if ($spec) {
                        $spec->update(array('quantity' => $spec->quantity + $row->quantity));
                    }
                }
            }

            // 积分商城（换购订单处理）
            if ($order->is_pointmall) {
                $point_item = AddonPointMallOrder::findFirst($where);
                if ($point_item) {
                    $point = $point_item->cost_point;
                    $user = UserForCustomers::findFirst('user_id=' . $user_id . ' and customer_id=' . CUR_APP_ID);
                    if ($user) {
                        $user->update(array('points_available' => $user->points_available + $point));
                    }
                    // todo 积分日志
                }
            }
        } else {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单正在进行中，无法取消！');
        }

        return Ajax::init()->outRight('');
    }

    // 删除订单
    public function delOrder()
    {
        $user_id = UserStatus::getUid();
        $order = $this->request->get('order');

        if (!$order) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $order = ShopOrders::findFirst('user_id=' . $user_id . ' and order_number = "' . $order . '"');

        if (!$order) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }

        if ($order->status != TransactionManager::ORDER_STATUS_BUYER_CANCELED && $order->status != TransactionManager::ORDER_STATUS_TRADE_SUCCESS) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单正在进行中，无法删除！');
        }

        if (!$order->update([
            'is_deleted' => 1
        ])
        ) {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED);
        }

        return Ajax::init()->outRight('');
    }

    // 退货申请
    public function returnApply()
    {
        $type = $this->request->get('type');
        $id = $this->request->get('id');
        $reason_option = $this->request->get('reason_option');
        $user_id = UserStatus::getUid();
        if (!is_numeric($id)) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, 'illegal params！');
        }
        $item = OrderManager::init()->checkReturnProduct($id, $type);
        if (!$item) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '该订单商品不存在！');
        }
        $reason = $this->request->get('reason');
        $shopOrderReturn = new ShopOrderReturn();
        $shopOrderReturn->type = $type;
        $shopOrderReturn->reason = $reason_option;
        $shopOrderReturn->reason_detail = $reason;
        $shopOrderReturn->customer_id = CUR_APP_ID;
        $shopOrderReturn->user_id = $user_id;
        $shopOrderReturn->order_item_id = $id;
        $shopOrderReturn->product_id = $item['item_id'];
        $shopOrderReturn->order_number = $item['order_number'];
        $shopOrderReturn->quantity = $item['quantity'];
        $shopOrderReturn->is_seller_accept = 0;
        $shopOrderReturn->is_send = 0;
        $shopOrderReturn->recharge_money = $item['total_cash'];
        $shopOrderReturn->is_seller_refunded = 0;
        $shopOrderReturn->seller_received_goods = 0;
        $shopOrderReturn->created = time();
        if ($shopOrderReturn->create()) {
            return Ajax::init()->outRight('');
        } else {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '提交失败！');
        }
    }

    // 取消退换货
    public function cancelBack()
    {
        $user_id = UserStatus::getUid();
        $id = $this->request->get('id');
        if (!is_numeric($id)) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, 'illegal params！');
        }
        $return = ShopOrderReturn::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $user_id . " and id=" . $id);
        if ($return && $return->delete()) {
            return Ajax::init()->outRight('');
        } else {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '请求失败！');
        }
    }

    // 取消物流信息
    public function backGood()
    {
        $user_id = UserStatus::getUid();
        $id = $this->request->get('id');
        if (!is_numeric($id)) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, 'illegal params！');
        }
        $return = ShopOrderReturn::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $user_id . " and id=" . $id);

        if ($return) {
            $return->buyer_deliver_no = $this->request->get('deliver_no');
            $return->buyer_deliver_logistics = $this->request->get('deliver_logistics');
            $return->is_buyer_delivered = 1;
            $return->buyer_delivered = time();
            if ($return->update()) {
                return Ajax::init()->outRight('');
            } else {
                return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '请求失败！');
            }
        } else {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '请求失败！');
        }
    }

    // 改变订单为货到付款
    public function orderCod()
    {
        $order_number = EasyEncrypt::decode($this->request->getPost('order'));
        $is_cod = intval($this->request->getPost('is_cod'));

        if (!($order_number && ($is_cod == 0 || $is_cod == 1))) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $order = ShopOrders::findFirst('order_number="' . $order_number . '"');

        if (!$order) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '订单未找到！');
        }

        if (!$order->update(array('is_cod' => $is_cod))) {
            foreach ($order->getMessages() as $message) {
                $msg[] = $message;
            }
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, join(',', $msg));
        }

        return Ajax::init()->outRight();
    }

    //评论提交
    public function  commentSubmit()
    {
        $data = $this->request->get("data");
        $order_number = $data['order_number'];
        $goods_list = $data['goodsList'];

        if (!$data) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, 'illegal params');
        }
        $uid = UserStatus::getUid();
        if (!$uid) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "请先登录再评价");
        }
        $goodsList = explode(",", $goods_list);
        $sql = "";
        $time = time();
        if ($goodsList) {
            $count = ProductComment::count("parent_id=0 and order_number=" . $order_number . " and user_id=" . $uid . " and product_id in(" . $goods_list . ")");
            if ($count > 0) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, "您已经评价过了");
            }
            $sql = " insert into product_comment (parent_id,product_id,order_number,user_id,rank,content,created,is_show) values ";
        }
        foreach ($goodsList as $item) {
            $sql .= "(0," . $item . "," . $order_number . "," . $uid . "," . ($data['rank' . $item]) . ",'" . ($data['content' . $item]) . "'," . $time . ",1) ,";
        }
        if ($sql == "") {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, 'illegal params');
        }
        $sql = rtrim($sql, ',');
        if (!$this->db->query($sql)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '评价失败');
        }
        /*更新订单是否评论字段评论*/
        $order = ShopOrders::findFirst("order_number=" . $order_number . " and user_id=" . $uid);
        $order->is_rate = 1;
        $order->update();
        /*送积分*/
        $vipgrade = VipcardManager::getUserVipGrade($uid, CUR_APP_ID);
        PointRule::init(CUR_APP_ID, $vipgrade)->executeRule($uid, PointRule::BEHAVIOR_ADD_PRODUCT_COMMENT);


        return Ajax::init()->outRight();
    }
}
