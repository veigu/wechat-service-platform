<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-12
 * Time: 下午2:55
 */

namespace Components\Order;


use Components\Product\ProductManager;
use Components\UserStatus;
use Components\UserStatusWap;
use Models\Product\Product;
use Models\Shop\ShopCart;
use Phalcon\Mvc\User\Component;
use Util\Ajax;
use Util\Cookie;

// wap版购物车
class CartManager extends Component
{
    private static $instance = null;

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * get cart item list
     */
    public function getList()
    {
        $user_id = UserStatus::getUid();
        $cookie_cart_str = Cookie::get('_my_cart_all');
        // save cookie cart data into db
        if ($cookie_cart_str) {
            $cookie_cart = json_decode(base64_decode($cookie_cart_str), true);
            foreach ($cookie_cart as $val) {
                $item = Product::findFirst('customer_id=' . CUR_APP_ID . ' and id=' . $val['id']);
                if ($item) {
                    $data = array('num' => $val['num'], 'modified' => time());
                    // 商品型号
                    $data['spec'] = $val['spec'];
                    // 型号数据
                    $data['spec_data'] = ProductManager::instance()->getProductSpecData($val['id'], $val['spec'], 'spec_data');

                    $shopcart = ShopCart::findFirst('customer_id=' . CUR_APP_ID . ' and item_id=' . $val['id'] . ' and user_id=' . $user_id . " and spec= '" . $data['spec'] . "'");
                    if ($shopcart) {
                        $shopcart->update($data);
                    }

                    if (!$shopcart) {
                        $shopcart = new ShopCart();
                        $data['created'] = time();
                        $data['customer_id'] = CUR_APP_ID;
                        $data['user_id'] = $user_id;
                        $data['item_id'] = $val['id'];
                        $shopcart->create($data);
                    }
                }
            }
        }
        // delete cart cookie
        Cookie::del('_my_cart_all');

        // read cart info for db
        $db_cart = ShopCart::find('customer_id=' . CUR_APP_ID . ' and user_id=' . UserStatus::getUid());

        $list = '';

        if ($db_cart) {
            $cart_all = $db_cart->toArray();
            foreach ($cart_all as $cart) {
                $v = Product::findFirst('customer_id = ' . CUR_APP_ID . ' and id =' . $cart['item_id']);
                $item = $v->toArray();
                // 以购物车id为主键
                $item['item_id'] = $cart['item_id'];
                $item['id'] = $cart['id'];
                $item['num'] = $cart['num'];
                $item['spec'] = isset($cart['spec']) ? $cart['spec'] : "";
                $item['spec_data'] = isset($cart['spec_data']) ? $cart['spec_data'] : "";
                if ($cart['spec']) {
                    $inventory = ProductManager::instance()->getProductSpecData($v->id, $cart['spec']);
                    $item['spec_num'] = $inventory['num'];
                    $item['total'] = $cart['num'] * $inventory['sell_price'];

                } else {
                    $item['spec_num'] = $item['quantity'];
                    $item['total'] = $cart['num'] * $item['sell_price'];
                }

                $list[] = $item;
            }
        }

        return $list;
    }

    /**
     * get cart item count
     *
     * @return int
     */
    public function getCount()
    {
        // 计算购物车商品数量
        $cookie_cart_str = Cookie::get('_my_cart_all');
        $cart_count = 0;
        if ($cookie_cart_str) {
            $cookie_cart = json_decode(base64_decode($cookie_cart_str), true);
            if ($cookie_cart) {
                $cookieArr = array_column($cookie_cart, 'id');
                $cookieArr = array_unique($cookieArr);
                $cart_count = count($cookieArr);
            }
        }

        if (UserStatus::getUid() > 0) {
            $shopcart = ShopCart::find('customer_id=' . CUR_APP_ID . ' and user_id=' . UserStatus::getUid());
            if ($shopcart) {
                $dbcart = $shopcart->toArray();
                $dbArr = array_column($dbcart, 'item_id');

                if (!empty($cookieArr)) {
                    $cook = array_merge($dbArr, $cookieArr);
                    $cook = array_unique($cook);
                    $cart_count = count($cook);
                } else {
                    $cart_count = count($dbArr);
                }

            }
        }

        return $cart_count;
    }

    //删除购物车商品
    public function delById()
    {
        $user_id = UserStatus::getUid();
        $item_id = intval($this->request->get('id'));

        if (!($item_id)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        $item = ShopCart::findFirst('user_id=' . $user_id . ' and id = ' . $item_id);
        if (!$item) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS);
        }
        if (!$item->delete()) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '删除失败！');
        }
        return Ajax::init()->outRight('');
    }

    // 更新购物车商品信息
    public function updateCart()
    {
        $user_id = UserStatus::getUid();
        $data = json_decode($this->request->get('data'), true);
        if (!($data)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }
        foreach ($data as $item) {
            $m = ShopCart::findFirst('user_id = ' . $user_id . ' and id = ' . $item['id']);
            if ($m) {
                $m->num = $item['num'];
                $m->modified = time();
                $m->update();
            }
        }

        return Ajax::init()->outRight('');
    }

    // 删除购物车商品
    public function delProduct()
    {
        $data = $this->request->get('data');
        if (!$data) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG);
        }
        $cart = ShopCart::find("customer_id=" . CUR_APP_ID . " AND user_id=" . UserStatusWap::getUid() . " and  id in (" . implode(',', $data) . ")");
        if (!$cart) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG);
        }

        foreach ($cart as $_c) {
            $_c->delete();
        }

        return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '删除失败！');
    }
}