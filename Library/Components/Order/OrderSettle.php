<?php
/**
 * Created by PhpStorm.
 * User: yanue
 * Date: 2014/11/25
 * Time: 9:57
 */

namespace Components\Order;


use Components\Product\ProductManager;
use Components\UserStatus;
use Models\Product\Product;
use Models\Shop\Shop;
use Models\Shop\ShopCart;
use Models\Shop\ShopFreightTpl;
use Models\Shop\ShopFreightTplItems;
use Models\User\UserAddress;
use Phalcon\Mvc\User\Plugin;
use Util\Cookie;

class OrderSettle extends Plugin
{
    private static $instance = null;
    private static $_total_cash = 0;
    private static $_paid_cash = 0;
    private static $_freight_fee = 0;
    private static $_total_preferential = 0;
    private static $_vip_preferential = 0;

    public static function init()
    {
        self::$_total_cash = 0;
        self::$_paid_cash = 0;
        self::$_freight_fee = 0;
        self::$_total_preferential = 0;
        self::$_vip_preferential = 0;

        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // 直接购买（缓冲到购物车，获取数据直接从购物车删除）
    public function getSettleItem($_cart_data, $province_name, $vip_discount)
    {
        // 1. 先插入购物车
        $user_id = UserStatus::getUid();
        $data = array('num' => $_cart_data['num'], 'modified' => time());
        // 商品型号
        $data['spec'] = $_cart_data['spec'];
        // 型号数据
        $data['spec_data'] = ProductManager::instance()->getProductSpecData($_cart_data['id'], $_cart_data['spec'], 'spec_data');

        $shopcart = ShopCart::findFirst('customer_id=' . CUR_APP_ID . ' and item_id=' . $_cart_data['id'] . ' and user_id=' . $user_id . " and spec= '" . $_cart_data['spec'] . "'");
        if ($shopcart) {
            $shopcart->update($data);
        }

        if (!$shopcart) {
            $shopcart = new ShopCart();
            $data['created'] = time();
            $data['customer_id'] = CUR_APP_ID;
            $data['user_id'] = $user_id;
            $data['item_id'] = $_cart_data['id'];
            $shopcart->create($data);
        }

        if (!$shopcart->id) {
            return false;
        }

        // 2 按照购物车模式结算（公用购物车模式）
        $settleData = $this->getSettleData([$shopcart->id], $province_name, $vip_discount);

        // 3 删除购物车（避免无故加入购物车）
        $shopcart->delete();

        return $settleData;
    }

    // 获取购物车里面结算的商品
    public function getSettleData($_cart_data, $province_name, $vip_discount)
    {
        $user_id = UserStatus::getUid();

        $res = ShopCart::find(' user_id = ' . $user_id . ' and  id in (' . implode(',', array_values($_cart_data)) . ')');
        $data = [];
        if ($res) {
            $res = $res->toArray();
            // 获取商品
            $data = $this->getItemData($res, $province_name, $vip_discount);
        }
        return $data;
    }

    // 获取商品信息
    private function getItemData($_cartData, $province_name, $vip_discount)
    {
        // 所有支持自取的门店
        $_pick_store = [];
        if (strtolower(MODULE_NAME) != 'wap') {
            $_pick_store = ProductManager::instance()->getProductStoreAllByCustomerID(CUR_APP_ID);
        }
        // 商品列表(按物流模板组合)
        $_item_list = [];
        foreach ($_cartData as $k => $_cart) {
            // 查找已经发布的商品
            $item = Product::findFirst(array('id=' . $_cart['item_id'] . ' and is_active=1', 'columns' => 'name,thumb,freight_tpl,weight,volume,quantity,sell_price,original_price'));
            // 商品不存在！
            if (!$item) {
                continue;
            }

            // 库存不足
            if ($item->quantity < $_cart['num']) {
                continue;
            }

            $item = $item->toArray();
            // vip折扣价
            $item['vip_sell_price'] = $item['sell_price'];

            // 如果存在规格
            if (!empty($_cart['spec'])) {
                $spec_data = ProductManager::instance()->getProductSpecData($_cart['item_id'], $_cart['spec']);
//                $store_data = ProductManager::instance()->getProductStoreBySpec($_cart['item_id'], $_cart['spec']);
                $store_data = [];
                // 库存不足
                if (($spec_data['num'] < $_cart['num']) || $_cart['num'] < 1) {
                    continue;
                }

                // vip折扣价
                $item['vip_sell_price'] = $spec_data['sell_price'];

                // vip打折比较
                if ($spec_data['original_price'] && ($vip_discount * $spec_data['original_price'] / 100) < $spec_data['sell_price']) {
                    //折扣后的价格比优惠价格更低
                    $item['vip_sell_price'] = $vip_discount * $spec_data['original_price'] / 100;
                    // 优惠
                    self::$_total_preferential += ($spec_data['original_price'] - $spec_data['sell_price']) * $_cart['num'];
                    self::$_vip_preferential += ($item['vip_sell_price'] - $spec_data['sell_price']) * $_cart['num'];
                }

                $item['thumb'] = $spec_data['thumb'] ? $spec_data['thumb'] : $item['thumb'];
                $item['store'] = $store_data;

                // 自取门店
                $_pick_store = array_diff($_pick_store, $store_data);

                // 总价
                self::$_total_cash += $spec_data['original_price'] * $_cart['num'];
            } else {
                // 库存不足
                if (($item['quantity'] < $_cart['num']) || $_cart['num'] < 1) {
                    continue;
                }


                //折扣后的价格比优惠价格更低
                if ($item['original_price'] && ($vip_discount * $item['original_price'] / 100) < $item['sell_price']) {
                    $item['vip_sell_price'] = $vip_discount * $item['original_price'] / 100;
                    // 优惠
                    self::$_total_preferential += ($item['original_price'] - $item['sell_price']) * $_cart['num'];
                    self::$_vip_preferential += ($item['sell_price'] - $item['vip_sell_price']) * $_cart['num'];
                }

                // 计算价格
                $item['store'] = [];
                // 总价
                self::$_total_cash += $item['original_price'] * $_cart['num'];
                // 自取门店
                $_pick_store = array_diff($_pick_store, []);
            }

            // 计算价格
            $item['subtotal'] = $item['vip_sell_price'] * $_cart['num'];

            $_cart['cart_id'] = $_cart['id'];
            unset($_cart['id']);
            // 所有符合的商品(按物流模板组合)
            $_item_list[$_cart['cart_id']] = array_merge($_cart, $item);

            // 总价
            self::$_paid_cash += $item['subtotal'];
        }
        // 线上运费
        $goods = $this->getFreight($_item_list, $province_name);
        // 免邮基数
        $shop = Shop::findFirst(array("customer_id='" . CUR_APP_ID . "'", 'columns' => 'name,free_postage,orders_proportion'));
        $order_award_points = $shop->orders_proportion > 0 ? floor(self::$_paid_cash / $shop->orders_proportion) : 0;
        $data = array(
            'list' => $goods,
            'free_postage' => $shop->free_postage,
            'freight_fee' => self::$_freight_fee,
            'total_paid' => self::$_paid_cash,
            'total_cash' => self::$_total_cash,
            'total_preferential' => self::$_total_preferential,
            'vip_preferential' => self::$_vip_preferential,
            'order_award_points' => $order_award_points,
        );

        return $data;
    }

    /*
     *
     * 获取当前可选物流及当前运费总和
     * @parm list array  购物车里的商品列表
     *
     */
    public function getFreight($list, $province_name)
    {
        // 各种运费情况下商品合集
        $res = array(
            'mail' => array(),
            'express' => array(),
            'ems' => array(),
            'express_mail' => array(),
            'express_ems' => array(),
            'mail_ems' => array(),
            'express_mail_ems' => array(),
        );

        //第一步 : 获取每个购物车商品可以使用的配送方式
        foreach ($list as $k => & $val) {
            $val['freight_tpl_id'] = $val['freight_tpl'];
            $val['valuation_way'] = $this->get_valuation_way($val['freight_tpl']);
            $freight = $this->getFreightTpl($val['freight_tpl'], $province_name);
            $val['freight_tpl'] = $freight;
            /*三种物流都支持*/
            if ($freight['express'] && $freight['mail'] && $freight['ems']) {
                $res['express_mail_ems'][] = $val;
            } /*支持快递和ems*/
            else if ($freight['express'] && $freight['ems'] && !$freight['mail']) {
                $res['express_ems'][] = $val;
            } /*支持快递和平邮*/
            else if ($freight['express'] && $freight['mail'] && !$freight['ems']) {
                $res['express_mail'][] = $val;
            } /*支持平邮和EMS*/
            else if ($freight['mail'] && $freight['ems'] && !$freight['express']) {
                $res['mail_ems'][] = $val;
            } /*支持快递*/
            else if ($freight['express'] && !$freight['mail'] && !$freight['ems']) {
                $res['express'][] = $val;
            } /*支持快递*/
            else if ($freight['ems'] && !$freight['mail'] && !$freight['express']) {
                $res['ems'][] = $val;
            } /*支持快递*/
            else if ($freight['mail'] && !$freight['express'] && !$freight['ems']) {
                $res['mail'][] = $val;
            }
        }

        //第2步:根据优先级合并精简配送方式(express>mail>EMS)
        if ($res) {
            if (!empty($res['express'])) {
                $res['express'] = array_merge($res['express'], $res['express_ems']);
                $res['express'] = array_merge($res['express'], $res['express_mail']);
                $res['express'] = array_merge($res['express'], $res['express_mail_ems']);
                unset($res['express_ems']);
                unset($res['express_mail']);
                unset($res['express_mail_ems']);
            } else {
                if ((!empty($res['express_mail']) && !empty($res['express_ems'])) || (!empty($res['express_ems']) && !empty($res['express_mail_ems']))) {
                    $res['express'] = array_merge($res['express'], $res['express_ems']);
                    $res['express'] = array_merge($res['express'], $res['express_mail']);
                    $res['express'] = array_merge($res['express'], $res['express_mail_ems']);
                    unset($res['express_ems']);
                    unset($res['express_mail']);
                    unset($res['express_mail_ems']);
                } else {
                    if (!empty($res['express_mail'])) {
                        $res['express_mail'] = array_merge($res['express_mail'], $res['express_mail_ems']);
                        unset($res['express_mail_ems']);
                    } else if (!empty($res['express_ems'])) {
                        $res['express_ems'] = array_merge($res['express_ems'], $res['express_mail_ems']);
                        unset($res['express_mail_ems']);
                    }
                }
            }
            $res['mail'] = array_merge($res['mail'], $res['mail_ems']);
            unset($res['mail_ems']);
        }
        //第3步 : 去空合并
        $result = array();
        foreach ($res as $k => $val) {
            if (empty($val)) {
                unset($res[$k]);
            } else {
                $result[$k]['goods'] = $val;
            }
        }

        //第4步 : 计算运费
        foreach ($result as $k => $val) {
            $type = explode('_', $k); //
            foreach ($type as $k2 => $val2) {
                $fr_one_money = 0; //首件最划算的价钱
                $fr_tpl_id = 0; //选为首件的运费模板id
                $total_freight_money = 0; //最终运费
                $tpl_array = array();
                //获取以哪个商品做首件
                foreach ($val['goods'] as $k1 => $val1) {
                    if (array_key_exists($val1['freight_tpl_id'], $tpl_array)) {
                        $tpl_array[$val1['freight_tpl_id']]['count'] += ($val1['valuation_way'] == 'number' ? $val1['num'] : $val1[$val1['valuation_way']]);
                    } else {
                        $tpl_array[$val1['freight_tpl_id']]['count'] = ($val1['valuation_way'] == 'number' ? $val1['num'] : $val1[$val1['valuation_way']]);
                        $tpl_array[$val1['freight_tpl_id']]['info'] = $val1['freight_tpl'][$val2][0];
                    }
                    if ($fr_one_money == 0 || ($val1['freight_tpl'][$val2][0]['first_cash'] < $fr_one_money)) {
                        $fr_one_money = $val1['freight_tpl'][$val2][0]['first_cash'];
                        $fr_tpl_id = $val1['freight_tpl_id'];
                    }
                }
                foreach ($tpl_array as $k1 => $val1) {
                    if ($k1 == $fr_tpl_id) //首件计费模板
                    {
                        if ($val1['info']['first_num'] > $val1['count']) {
                            $total_freight_money += $val1['info']['first_cash'];
                        } else {
                            $co = @ceil($val1['count'] - $val1['info']['first_num']) / $val1['info']['addon_num'];
                            $total_freight_money += ($val1['info']['first_cash'] + $co * $val1['info']['addon_cash']);
                        }
                    } else //续件计费模板
                    {
                        $co = ceil($val1['count'] / $val1['info']['addon_num']);
                        $total_freight_money += $co * $val1['info']['addon_cash'];
                    }
                }
                $result[$k]['freight_tpl'][$val2]['total_freight_money'] = $total_freight_money;
            }
        }

        // 循环排序最小运费并统计总邮费
        if ($result) {
            foreach ($result as $_data) {
                $freight_tpl = array_values($_data['freight_tpl']);
                self::$_freight_fee += $freight_tpl[0]['total_freight_money'];
            }
        }

        return $result;
    }

    // 获取物流方式
    public function getFreightTpl($id, $address)
    {

        $res = array(
            'mail' => array(),
            'express' => array(),
            'ems' => array()
        );
        $freight = ShopFreightTplItems::find("customer_id=" . CUR_APP_ID . " AND freight_id=" . $id);

        if ($freight) {
            $freight = $freight->toArray();
            foreach ($freight as $k => $v) {
                if (($v['is_national'] == 1) || ($v['is_national'] == 0 && strpos($v['district_cn'], $address['province']))) {
                    $res[$v['mail_mode']][] = $v;
                }
            }
        }
        return $res;
    }

    // 获取运费模板计费方式
    public function  get_valuation_way($freight_id)
    {
        $shopFreightTpl = ShopFreightTpl::findFirst("customer_id= '" . CUR_APP_ID . "' AND id= '" . $freight_id . "'");
        if ($shopFreightTpl) {
            return $shopFreightTpl->valuation_way;
        } else {
            return false;
        }

    }

    // 获取选中的地址（或默认）
    public function getUserAddress()
    {
        $addr = Cookie::get('_user_addr');
        if ($addr) {
            $address = UserAddress::findFirst('id = ' . $addr . ' and user_id=' . UserStatus::getUid());
        } else {
            $address = UserAddress::findFirst('is_default = 1 and user_id=' . UserStatus::getUid());
        }

        $address = $address ? $address->toArray() : []; //用户默认收货地址
        return $address;
    }

}