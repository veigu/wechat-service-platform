<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2014/11/12
 * Time: 11:08
 */

namespace Components;

use Components\Module\VipcardManager;
use Components\Rules\PointRule;
use Library\PHPMailer\PHPMailer;
use Models\User\UserForCustomers;
use Models\User\Users;
use Util\Ajax;
use Util\Cookie;

final class UserStatusStore extends UserStatus
{
    public static function init()
    {
        return new self();
    }

    // 检查登陆
    public function checkLogin()
    {
        $uid = $this->getUid();

        if (!$uid) {
            $cur_url = $this->uri->fullUrl();
            if ($this->isWechat() && $this->dispatcher->getActionName() != 'bindWechat') {
                $redirect = 'user/bindWechat?go=' . urlencode($cur_url);
            } else {
                $redirect = 'user/login?go=' . urlencode($cur_url);
            }
            return $this->response->redirect($redirect)->send();
        }

        return $uid;
    }

    public function ajaxLogin()
    {
        $phone = $this->request->getPost('phone');
        $password = $this->request->getPost('pass');
        if (!$phone) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        if ((strlen($password) < 6)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID);
        }

        $user = Users::findFirst('phone = "' . $phone . '" and host_key="' . HOST_KEY . '"');
        if (!$user) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_NOT_EXISTS);
        }

        if ($user->password != md5($password)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_NOT_CORRECT);
        }

        $this->saveStatus($user->id, $user->name);

        // 绑定用户到当前商家
        $this->bindCustomerUser($user->id);

        //赠送积分
        $ruleMng = PointRule::init(CUR_APP_ID, $user->id);
        $ruleMng->executeRule($user->id, PointRule::BEHAVIOR_SIGN_IN);

        //   vip card binded
        $card_binded = VipcardManager::checkNeedBindCard($user->id, CUR_APP_ID);

        // res
        $res = array(
            'uid' => $this->getUid(),
            'username' => $user->username,
            'point' => $this->getPoint(),
            'need_card_bind' => $card_binded,
            'wechat_binded' => $this->wechat_binded,
            'weibo_binded' => $this->weibo_binded
        );

        return Ajax::init()->outRight('登陆成功', $res);
    }

    // 获取用户积分
    public function getPoint()
    {
        if (!Cookie::exists(self::COOKIE_UID)) {
            return false;
        }

        $uf = UserForCustomers::findFirst('customer_id=' . CUR_APP_ID . ' and user_id=' . Cookie::get(self::COOKIE_UID));
        return isset($uf->points_available) ? intval($uf->points_available) : 0;
    }

    /**
     * ajax user reg
     */
    public function ajaxReg()
    {
        // Debug::log("REG:-start::");

        $username = $this->request->getPost('username', array('string', 'striptags'));
        $phone = $this->request->getPost('phone', array('alphanum', 'striptags'));
        $password = $this->request->getPost('pass');

        $data = array(
            'username' => $username,
            'phone' => $phone,
            'password' => md5($password),
            'created' => time(),
            'host_key' => HOST_KEY,
        );

        if (!$username) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_INVALID);
        }

        if (!($phone && preg_match('/^(1\d{10})$/', $phone))) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        if ((strlen($password) < 6)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID);
        }

        // todo why exit?
        $hasUser = Users::findFirst('host_key="' . HOST_KEY . '" and phone = "' . $phone . '"');
        if ($hasUser) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_HAS_BEING_USED);
        }

        $user = new Users();

        $res = $user->create($data);

        if (!$res) {
            foreach ($user->getMessages() as $message) {
                $msg[] = (string)$message;
            }
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, join(',', $msg), false);
        }

        // 绑定用户到当前商家
        $this->bindCustomerUser($user->id);

        // 登陆状态
        $this->saveStatus($user->id, $user->username);

        // 注册积分
        $vip_grade = VipcardManager::getUserVipGrade($user->id, CUR_APP_ID);
        $ruleMng = PointRule::init(CUR_APP_ID, $vip_grade);

        $ruleMng->executeRule($user->id, PointRule::BEHAVIOR_REGISTER);

        //绑定卡
        $card_binded = VipcardManager::checkNeedBindCard($user->id, CUR_APP_ID);

        // res
        $res = array(
            'uid' => $this->getUid(),
            'username' => $username,
            'need_card_bind' => 1,
            'point' => $this->getPoint()
        );

        return Ajax::init()->outRight('注册成功', $res, false);
    }

    // 邮箱重置密码
    public function sendPassMail($email, $phone, $siteName)
    {
        $pass = mt_rand(100000, 999999);

        if (!($email && $phone)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        if (!(preg_match('/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/', $email))) {
            return Ajax::init()->outError(Ajax::ERROR_EMAIL_IS_INVALID);
        }

        if (!preg_match('/^(1[\d]{10})$/', $phone)) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        $user = Users::findFirst('host_key="' . HOST_KEY . '" and phone="' . $phone . '"');
        if (!$user) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_NOT_EXISTS);
        }

        // 如果邮箱不存在，则保存到当前用户
        $user->password = md5($pass);
        if (!Users::findFirst('host_key="' . HOST_KEY . '" and email="' . $email . '"')) {
            $user->email = $email;
        };
        if (!$user->update()) {
            foreach ($user->getMessages() as $message) {
                $msg[] = $message;
            };
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, $msg);

        }

        $loginUrl = $this->uri->appUrl('user/login');
        $domain = $this->uri->appUrl();

        $date = date('Y');
        // todo send mail
        // 邮箱注册用户，要进行邮件通知
        $message = <<<EOF
                <html lang="zh">
                <head>
                <title>重置用户密码</title>
                <style type="text/css">
                span {
                    color: red;
                }
                </style>
                </head>
                <body>
                尊敬的用户您好：<br/>
                    您在我们网站平台进行了密码重置，以下是信息： <br/>
                    登录手机号：<span style='color:red;'>{$phone}</span><br />
                    重置后的密码：<span style='color:red;'>{$pass}</span>。</br>
                    请登陆后自行修改或妥善保管。祝您愉快！
                    <br/>
                    <br/>
                    登陆地址：<br/>
                    <a href="{$loginUrl}">{$loginUrl}</a>
                    <br/>
                    <br/>
                -----------------------<br/>
                点此进入微网站: <a href="{$domain}">{$domain}</a><br/>
                -----------------------<br/>
                {$date} @ <a href="{$domain}">{$siteName}</a><br/>
                -----------------------<br/>
                系统邮件，请勿回复！
                </body>
                </html>
EOF;
        require_once(ROOT . '/Library/PHPMailer/PHPMailerAutoload.php');
        require_once(ROOT . '/Library/PHPMailer/PHPMailer.php');

        $mailer = new PHPMailer();
        $mailer->CharSet = "UTF-8";
        $mailer->Subject = "重置用户密码";
        $mailer->From = "service@" . MAIN_DOMAIN;
        $mailer->FromName = $siteName;
        $mailer->msgHTML($message);
        $mailer->addAddress($email);
        if (!$mailer->send()) {
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, $mailer->ErrorInfo);
        }
        return Ajax::init()->outRight('邮件发送成功！');
    }
} 