<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2014/11/12
 * Time: 11:08
 */

namespace Components;

use Components\Module\VipcardManager;
use Components\Rules\PointRule;
use Components\WeChat\MessageManager;
use Models\User\Users;
use Util\Ajax;
use Util\Validator;

class UserStatusApp extends UserStatus
{
    public static function init()
    {
        return new self();
    }

    // 检查登陆
    public function checkLogin()
    {
        $uid = $this->getUid();

        if (!$uid) {
            $cur_url = $this->uri->fullUrl();
            $redirect = 'o2o_app/user/login?go=' . urlencode($cur_url);
            return $this->response->redirect($redirect)->send();
        }

        return $uid;
    }

    public function ajaxLogin()
    {
        $phone = $this->request->getPost('phone');
        $password = $this->request->getPost('pass');
        if (!Validator::validateCellPhone($phone)) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        if ((strlen($password) < 6)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID);
        }

        $user = Users::findFirst('phone = "' . $phone . '" and host_key="' . HOST_KEY . '"');
        if (!$user) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_NOT_EXISTS);
        }

        if ($user->password != md5($password)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_NOT_CORRECT);
        }

        $this->saveStatus($user->id, $user->username);

        // todo 赠送积分
//        $ruleMng = PointRule::init(CUR_APP_ID, $user->id);
//        $ruleMng->executeRule($user->id, PointRule::BEHAVIOR_SIGN_IN);

        //   vip card binded
//        $card_binded = VipcardManager::checkNeedBindCard($user->id, CUR_APP_ID);

        // res
        $res = array(
            'uid' => $this->getUid(),
            'username' => $user->username,
//            'point' => $this->getPoint(),
//            'need_card_bind' => $card_binded,
//            'wechat_binded' => $this->wechat_binded,
//            'weibo_binded' => $this->weibo_binded
        );


        return Ajax::init()->outRight('登陆成功', $res);
    }

    /**
     * ajax user reg
     */
    public function ajaxReg()
    {
        // Debug::log("REG:-start::");

        $username = $this->request->getPost('username', array('string', 'striptags'));
        $phone = $this->request->getPost('phone', array('alphanum', 'striptags'));
        $password = $this->request->getPost('pass');

        $data = array(
            'username' => $username,
            'phone' => $phone,
            'password' => md5($password),
            'created' => time(),
            'host_key' => HOST_KEY,
        );

        if (!$username) {
            return Ajax::init()->outError(Ajax::ERROR_USER_IS_INVALID);
        }

        if (!($phone && preg_match('/^(1\d{10})$/', $phone))) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_IS_INVALID);
        }

        if ((strlen($password) < 6)) {
            return Ajax::init()->outError(Ajax::ERROR_PASSWD_IS_INVALID);
        }

        // todo why exit?
        $hasUser = Users::findFirst('host_key="' . HOST_KEY . '" and phone = "' . $phone . '"');
        if ($hasUser) {
            return Ajax::init()->outError(Ajax::ERROR_PHONE_HAS_BEING_USED);
        }

        $user = new Users();

        $res = $user->create($data);

        if (!$res) {
            foreach ($user->getMessages() as $message) {
                $msg[] = (string)$message;
            }
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, join(',', $msg), false);
        }
        // Debug::log("REG:-after-create:res:ok");

        // 绑定用户到当前商家
        $this->bindCustomerUser($user->id);

        // 登陆状态
        $this->saveStatus($user->id,$user->username);

        // 注册积分
        $vip_grade = VipcardManager::getUserVipGrade($user->id, CUR_APP_ID);
        $ruleMng = PointRule::init(CUR_APP_ID, $vip_grade);
        // Debug::log("REG:-before-rule:uid:" . $user->id);

        $ruleMng->executeRule($user->id, PointRule::BEHAVIOR_REGISTER);
        // Debug::log("REG:-after-rule:uid:" . $user->id . ':' . PointRule::BEHAVIOR_REGISTER);

        //绑定卡
//        $card_binded = VipcardManager::checkNeedBindCard($user->id, CUR_APP_ID);

        //sync emoi
        // res
        $res = array(
            'uid' => $this->getUid(),
            'username' => $username,
            'need_card_bind' => 1,
            'point' => $this->getPoint()
        );
        // Debug::log("REG:-end:data:" . var_export($res));
        return Ajax::init()->outRight('注册成功', $res, false);
    }
} 