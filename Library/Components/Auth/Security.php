<?php
namespace Components\Auth;

use Phalcon\Acl;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;
use Models\Acl\AclRoleResource;
use Models\Acl\AclResource;
use Models\Acl\AclRole;
use Phalcon\Acl\Adapter\Memory;

/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class Security extends Plugin
{

    public function __construct($dependencyInjector)
    {
        $this->_dependencyInjector = $dependencyInjector;
    }

    public function getAcl()
    {
        if (!isset($this->persistent->acl)) {
            
            $acl = new Memory();
            //The default action is DENY access
            $acl->setDefaultAction(\Phalcon\Acl::DENY);
             
            //Register two roles, Users is registered users
            //and guests are users without a defined identity
            $roles = AclRole::find("module = " . ACL_MODULE);
            
            foreach ($roles as $role) {
                $acl->addRole($role);
            }
            
            $resources = AclResource:: find("module=" . ACL_MODULE);
            foreach ($resources as $rs) {
                $resource = new \Phalcon\Acl\Resource($rs->controller);
                $acl->addResource($resource, json_decode($rs->actions));
            }
            
            $roleRsourece = AclRoleResource::find("module = " . ACL_MODULE);
            foreach ($roleRsourece as $rr) {
                if($rr->auth_type == self::AUTH_TYPE_ALLOW)
                {
                    $acl->allow($rr->role, $rr->controller, $rr->action);
                }
                else {
                    $acl->deny($rr->role, $rr->controller, $rr->action);
                }
            }

            //The acl is stored in session, APC would be useful here too
            $this->persistent->acl = $acl;
        }

        return $this->persistent->acl;
    }

    /**
     * This action is executed before execute any action in the application
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth_role');
        if (!$auth) {
            $role = 'Guests';
        } else {
            $role = 'Users';
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $acl = $this->getAcl();

        $allowed = $acl->isAllowed($role, $controller, $action);
        if ($allowed != Acl::ALLOW) {
            $this->flash->error("You don't have access to this service");
            $dispatcher->forward(
                array(
                    'module' => $dispatcher->getModuleName(),
                    'controller' => 'index',
                    'action' => 'index'
                )
            );
            return false;
        }

    }

}
