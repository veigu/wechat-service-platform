<?php
namespace Components\Auth;

use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory;
use Phalcon\Acl;
use Models\Acl\AclRole;
use Models\Acl\AclResource;
use Models\Acl\AclRoleResource;

class AclListener extends \Phalcon\Mvc\User\Component
{

    /**
     * @var string
     */
    protected $_module;
    /**
     * @var Acl
     */
    protected $_acl;
    
    protected $_acl_cache_file_path = "/Data/";
    
    const AUTH_TYPE_ALLOW = "allow";
    const AUTH_TYPE_DENY = "deny";

    public function __construct($module)
    {
        $this->_module = $module;
    }

    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $resource = $this->_module . '-' . $dispatcher->getControllerName(); // frontend-dashboard
        $access = $dispatcher->getActionName(); // null
    }
    
    public function initRoleResourceSettings() {
        $aclCacheFile = ROOT . $this->_acl_cache_file_path . ACL_MODULE . "acl.data";
        //Check whether acl data already exist
        if (!is_file($aclCacheFile)) {
        
            $acl = new Memory();
            //The default action is DENY access
            $acl->setDefaultAction(\Phalcon\Acl::DENY);
             
            //Register two roles, Users is registered users
            //and guests are users without a defined identity
            $roles = AclRole::find("module = " . ACL_MODULE);
            
            foreach ($roles as $role) {
                $acl->addRole($role);
            }
            
            $resources = AclResource:: find("module=" . ACL_MODULE);
            foreach ($resources as $rs) {
                $resource = new \Phalcon\Acl\Resource($rs->controller);
                $acl->addResource($resource, json_decode($rs->actions));
            }
            
            $roleRsourece = AclRoleResource::find("module = " . ACL_MODULE);
            foreach ($roleRsourece as $rr) {
                if($rr->auth_type == self::AUTH_TYPE_ALLOW)
                {
                    $acl->allow($rr->role, $rr->controller, $rr->action);
                }
                else {
                    $acl->deny($rr->role, $rr->controller, $rr->action);
                }
            }
            
            //... Define roles, resources, access, etc
            // Store serialized list into plain file
            file_put_contents($aclCacheFile, serialize($acl));
        
        } else {
        
            //Restore acl object from serialized file
            $acl = unserialize(file_get_contents($aclCacheFile));
        }
        
        $this->acl = $acl;
    }

}