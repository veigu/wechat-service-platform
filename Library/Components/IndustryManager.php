<?php
/**
 * Created by PhpStorm.
 * User: wgwang
 * Date: 14-4-8
 * Time: 下午3:05
 */

namespace Components;

use Models\System\SystemIndustry;
use Phalcon\Cache\BackendInterface;
use Phalcon\Mvc\User\Plugin;

class IndustryManager extends Plugin
{

    /**
     * @var BackendInterface
     */
    private $cache = null;

    /**
     * @var IndustryManager
     */
    private static $instance = null;

    private function __construct()
    {
        $this->cache = $this->di->get('memcached');
    }

    public static function instance()
    {
        if (!self::$instance instanceof IndustryManager) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getTreeData($refresh = false)
    {
        $cacheKey = "industries_list_for_tree_" . HOST_KEY;
        $data = $this->cache->get($cacheKey);
        if (!$data || $refresh) {
            $data = SystemIndustry::find("host_key='" . HOST_KEY . "'")->toArray();
            $this->cache->save($cacheKey, $data);
        }
        return $data;
    }

    public function getIndustryById($cid)
    {
        return [];
    }
}