<?php
namespace Util;

use Phalcon\Mvc\User\Plugin;

/**
 * ajax handle for web api
 *
 * @author     yanue <yanue@outlook.com>
 * @link     http://stephp.yanue.net/
 * @package  lib/util
 * @time     2013-07-11
 */
class Ajax extends Plugin
{
    const ERROR_USER_IS_NOT_EXISTS = 1001;
    const ERROR_USER_HAS_NOT_LOGIN = 1002;
    const ERROR_USER_HAS_NO_PERMISSION = 1003;
    const ERROR_USER_HAS_BEING_USED = 1004;
    const ERROR_USER_IS_INVALID = 1005;
    const ERROR_USER_BIND_FAILED = 1006;
    const ERROR_PASSWD_IS_INVALID = 1007;
    const ERROR_PASSWD_IS_NOT_CORRECT = 1008;
    const ERROR_EMAIL_IS_INVALID = 1009;
    const ERROR_EMAIL_HAS_BEING_USED = 1010;
    const ERROR_INVALID_REQUEST_PARAM = 1011;
    const ERROR_ILLEGAL_API_SIGNATURE = 1012;
    const ERROR_NOTHING_HAS_CHANGED = 1013;
    const ERROR_RUN_TIME_ERROR_OCCURRED = 1014;
    const ERROR_PHONE_IS_INVALID = 1015;
    const ERROR_PHONE_HAS_BEING_USED = 1016;
    const ERROR_USER_IS_NOT_ACTIVE = 1017;
    const ERROR_TOKEN_INVALID = 1018;
    const ERROR_TOKEN_EXPIRES = 1019;
    const ERROR_USER_NOT_BINDED = 1020;
    const ERROR_REFRESH_TOKEN_INVALID = 1021;
    const ERROR_SIGN_EXPIRES = 1022;
    const ERROR_DATA_NOT_EXISTS = 1023;
    const ERROR_DATA_HAS_EXISTS = 1024;
    const ERROR_POINTS_NOT_ENOUGH = 1025;
    const ERROR_EXP_NOT_ENOUGH = 1026;
    const ERROR_USER_PROFILE_IMCOMPLETE = 1027;
    const ERROR_TOKEN_HAS_REFRESHED = 1028;
    const ERROR_USER_HAS_BEING_LOCKED = 1029;
    const ERROR_TITLE_IS_NOT_EXISTS = 1030;
    const ERROR_TITLE_IS_EXISTS = 1031;
    const ERROR_ACCOUNT_IS_NOT_EXISTS = 1031;
    const ERROR_ACCOUNT_HAS_BEING_USED = 1033;
    #2 文件上传相关
    const UPLOAD_ERR_TMP_NAME_NOT_EXIST = 2011;
    const UPLOAD_ERR_FILE_FIELD_NOT_RECEIVED = 2012;
    const UPLOAD_ERR_FILE_EXT_ONLY_ALLOWED = 2013;
    const UPLOAD_ERR_UPLOAD_FILE_IS_TOO_LARGE = 2014;
    const UPLOAD_ERR_BATCH_IS_NOT_ALLOWED = 2015;
    const UPLOAD_ERR_ONLY_SUPPORT_BATCH_UPLOAD = 2016;
    const UPLOAD_ERR_FASTDFS_SAVE_ERROR_OCCURRED = 2017;
    const UPLOAD_ERR_MASTER_FILE_NOT_EXIST = 2018;

    # custom error msg
    const CUSTOM_ERROR_MSG = 3001;

    #4 活动相关

    const ACT_WHEEL_HAS_EXCHANGE = 4001;
    const ACT_WHEEL_CODE_NOT_EXIST = 4002;
    const ACT_WHEEL_CODE_NO_LOGIN = 4003;


    public static $errmsg = array(
        self::ERROR_USER_IS_NOT_EXISTS => '用户不存在',
        self::ERROR_USER_HAS_NOT_LOGIN => '用户尚未登陆',
        self::ERROR_USER_HAS_NO_PERMISSION => '用户没有权限',
        self::ERROR_USER_HAS_BEING_USED => '本手机号已注册',
        self::ERROR_USER_IS_INVALID => '用户名格式不正确',
        self::ERROR_USER_BIND_FAILED => '用户绑定失败',
        self::ERROR_PASSWD_IS_INVALID => '密码长度为6-16位字符',
        self::ERROR_PASSWD_IS_NOT_CORRECT => '密码不正确',
        self::ERROR_EMAIL_IS_INVALID => '邮箱格式不正确',
        self::ERROR_EMAIL_HAS_BEING_USED => '邮箱已被使用了',
        self::ERROR_INVALID_REQUEST_PARAM => '缺少请求参数',
        self::ERROR_ILLEGAL_API_SIGNATURE => '非法的API签名',
        self::ERROR_NOTHING_HAS_CHANGED => '修改或插入不成功',
        self::ERROR_RUN_TIME_ERROR_OCCURRED => '服务器错误',
        self::ERROR_PHONE_IS_INVALID => '非法的手机号',
        self::ERROR_PHONE_HAS_BEING_USED => '本手机号已注册',
        self::ERROR_USER_IS_NOT_ACTIVE => '用户未激活',
        self::ERROR_TOKEN_INVALID => '非法的token（令牌）',
        self::ERROR_TOKEN_EXPIRES => 'token（令牌）已经过期',
        self::ERROR_USER_NOT_BINDED => '用户未做本地化绑定',
        self::ERROR_REFRESH_TOKEN_INVALID => 'refresh_token非法',
        self::ERROR_SIGN_EXPIRES => '签名已经过期',
        self::ERROR_DATA_NOT_EXISTS => '数据不存在',
        self::ERROR_DATA_HAS_EXISTS => '数据已经存在',
        self::ERROR_POINTS_NOT_ENOUGH => '用户可用积分不够',
        self::ERROR_EXP_NOT_ENOUGH => '用户可用经验值不够',
        self::ERROR_USER_PROFILE_IMCOMPLETE => '用户基本资料不完整',
        self::ERROR_TOKEN_HAS_REFRESHED => 'token（令牌）已经被刷新过',
        self::ERROR_USER_HAS_BEING_LOCKED => '用户已被锁定',
        self::ERROR_TITLE_IS_NOT_EXISTS => '标题不存在',
        self::ERROR_TITLE_IS_EXISTS => '标题已经存在',
        self::ERROR_ACCOUNT_HAS_BEING_USED => '用户名已被使用',
        self::ERROR_ACCOUNT_IS_NOT_EXISTS => '账号不存在',

        UPLOAD_ERR_INI_SIZE => '文件大小超过了php.ini定义的upload_max_filesize值',
        UPLOAD_ERR_FORM_SIZE => '文件大小超过了HTML定义的MAX_FILE_SIZE值',
        UPLOAD_ERR_PARTIAL => '文件只有部分被上传',
        UPLOAD_ERR_NO_FILE => '没有文件被上传',
        UPLOAD_ERR_NO_TMP_DIR => '缺少临时文件夹',
        UPLOAD_ERR_CANT_WRITE => '文件写入失败',

        self::UPLOAD_ERR_TMP_NAME_NOT_EXIST => '无文件上传',
        self::UPLOAD_ERR_FILE_FIELD_NOT_RECEIVED => '未接收到数据',
        self::UPLOAD_ERR_FILE_EXT_ONLY_ALLOWED => '文件类型不支持',
        self::UPLOAD_ERR_UPLOAD_FILE_IS_TOO_LARGE => '文件太大',
        self::UPLOAD_ERR_BATCH_IS_NOT_ALLOWED => '不允许批量上传',
        self::UPLOAD_ERR_ONLY_SUPPORT_BATCH_UPLOAD => '仅支持批量上传',
        self::UPLOAD_ERR_FASTDFS_SAVE_ERROR_OCCURRED => '文件保存失败',

        self::ACT_WHEEL_HAS_EXCHANGE => "此兑奖码已经兑换过了",
        self::ACT_WHEEL_CODE_NOT_EXIST => "兑奖码不存在",
        self::ACT_WHEEL_CODE_NO_LOGIN => '你还没有登陆,请先登录再抽奖'
    );

    const AJAX_RETURN_RIGHT = 'ajax_return_right';
    const AJAX_RETURN_ERROR = 'ajax_return_error';

    public static function init()
    {
        return new self();
    }

    public static function errorResult()
    {

    }

    public function outRight($msg = '', $data = '')
    {
        $result = array(
            'error' => array('code' => 0, 'msg' => $msg, 'more' => $msg),
            'result' => 1,
            'data' => $data
        );

        $this->view->setVar('data', $result);

        return self::AJAX_RETURN_RIGHT;
    }

    public function outError($code, $msg = '')
    {
        $result = array(
            'error' => array('code' => $code, 'msg' => self::getErrorMsg($code), 'more' => $msg),
            'result' => 0,
        );

        $this->view->setVar('data', $result);

        return self::AJAX_RETURN_ERROR;
    }

    /**
     * get error msg by defined code
     * @param $code
     * @return string
     */
    public static function getErrorMsg($code)
    {
        return isset($code) && isset(self::$errmsg[$code]) ? self::$errmsg[$code] : '';
    }

    // 来自微博
    public static  function isWeibo()
    {
        $userAgent = strtolower($_SERVER["HTTP_USER_AGENT"]);
        if (strpos($userAgent, 'weibo') !== false) {
            return true;
        }
        return false;
    }

    // 是否来自微信
    public static function isWechat()
    {
        $userAgent = strtolower($_SERVER["HTTP_USER_AGENT"]);
        if (strpos($userAgent, 'micromessenger') !== false) {
            return true;
        }
        return false;
    }
    public static function isMobile() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $mobile_agents = Array("240x320","acer","acoon","acs-","abacho","ahong","airness","alcatel","amoi","android","anywhereyougo.com","applewebkit/525","applewebkit/532","asus","audio","au-mic","avantogo","becker","benq","bilbo","bird","blackberry","blazer","bleu","cdm-","compal","coolpad","danger","dbtel","dopod","elaine","eric","etouch","fly ","fly_","fly-","go.web","goodaccess","gradiente","grundig","haier","hedy","hitachi","htc","huawei","hutchison","inno","ipad","ipaq","ipod","jbrowser","kddi","kgt","kwc","lenovo","lg ","lg2","lg3","lg4","lg5","lg7","lg8","lg9","lg-","lge-","lge9","longcos","maemo","mercator","meridian","micromax","midp","mini","mitsu","mmm","mmp","mobi","mot-","moto","nec-","netfront","newgen","nexian","nf-browser","nintendo","nitro","nokia","nook","novarra","obigo","palm","panasonic","pantech","philips","phone","pg-","playstation","pocket","pt-","qc-","qtek","rover","sagem","sama","samu","sanyo","samsung","sch-","scooter","sec-","sendo","sgh-","sharp","siemens","sie-","softbank","sony","spice","sprint","spv","symbian","tablet","talkabout","tcl-","teleca","telit","tianyu","tim-","toshiba","tsm","up.browser","utec","utstar","verykool","virgin","vk-","voda","voxtel","vx","wap","wellco","wig browser","wii","windows ce","wireless","xda","xde","zte");
        $is_mobile = false;
        foreach ($mobile_agents as $device) {
            if (stristr($user_agent, $device)) {
                $is_mobile = true;
                break;
            }
        }
        return $is_mobile;
    }
}