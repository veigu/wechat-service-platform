<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-12
 * Time: 下午2:55
 */

namespace Library\O2O\Order;


use Components\Product\ProductManager;
use Components\UserStatusApp;
use Models\O2O\Order\O2oShopCart;
use Models\Product\Product;
use Util\Cookie;

class O2oAppCartManager
{
    private static $instance = null;

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * get cart item list
     */
    public function getList()
    {
        $user_id = UserStatusApp::init()->getUid();
        $cookie_cart_str = Cookie::get('_my_cart_all');
        // save cookie cart data into db
        if ($cookie_cart_str) {
            $cookie_cart = json_decode(base64_decode($cookie_cart_str), true);
            foreach ($cookie_cart as $val) {
                $item = Product::findFirst('id=' . $val['id']);
                if ($item) {
                    $data = array('num' => $val['num'], 'modified' => time());
                    // 商品型号
                    $data['spec'] = $val['spec'];
                    // 型号数据
                    $data['spec_data'] = ProductManager::instance()->getProductSpecData($val['id'], $val['spec'], 'spec_data');

                    $shopcart = O2oShopCart::findFirst('item_id=' . $val['id'] . ' and user_id=' . $user_id . " and spec= '" . $data['spec'] . "'");
                    if ($shopcart) {
                        $shopcart->update($data);
                    }

                    if (!$shopcart) {
                        $shopcart = new O2oShopCart();
                        $data['created'] = time();
                        $data['user_id'] = $user_id;
                        $data['item_id'] = $val['id'];
                        $data['param'] = "[]";
                        $data['customer_id'] = $item->customer_id;

                        $shopcart->create($data);
                    }
                }
            }
        }
        //delete cart cookie
        Cookie::del('_my_cart_all');

        // read cart info for db
        $db_cart = O2oShopCart::find('user_id=' . UserStatusApp::init()->getUid());

        $list = '';

        if ($db_cart) {
            $cart_all = $db_cart->toArray();
            foreach ($cart_all as $cart) {
                $v = Product::findFirst('id =' . $cart['item_id']);
                $item = $v->toArray();
                // 这里是string类型的param
                $item['num'] = $cart['num'];
                $item['spec'] = isset($cart['spec']) ? $cart['spec'] : "";
                $item['spec_data'] = isset($cart['spec_data']) ? $cart['spec_data'] : "";
                $item['item_id'] = $cart['item_id'];
                if ($cart['spec']) {
                    $inventory = ProductManager::instance()->getProductSpecData($v->id, $cart['spec']);
                    $item['spec_num'] = $inventory['num'];
                    $item['total'] = $cart['num'] * $inventory['sell_price'];
                } else {
                    $item['spec_num'] = $item['quantity'];
                    $item['total'] = $cart['num'] * $item['sell_price'];
                }

                $list[] = $item;
            }
        }

        return $list;
    }

    /**
     * get cart item count
     *
     * @return int
     */
    public function getCount()
    {
        // 计算购物车商品数量
        $cookie_cart_str = Cookie::get('_my_cart_all');
        $cart_count = 0;
        if ($cookie_cart_str) {
            $cookie_cart = json_decode(base64_decode($cookie_cart_str), true);
            if ($cookie_cart) {
                $cookieArr = array_column($cookie_cart, 'id');
                $cookieArr = array_unique($cookieArr);
                $cart_count = count($cookieArr);
            }
        }

        if (UserStatusApp::init()->getUid() > 0) {
            $shopcart = O2oShopCart::find('customer_id=' . CUR_APP_ID . ' and user_id=' . UserStatusApp::init()->getUid());
            if ($shopcart) {
                $dbcart = $shopcart->toArray();
                $dbArr = array_column($dbcart, 'item_id');

                if (!empty($cookieArr)) {
                    $cook = array_merge($dbArr, $cookieArr);
                    $cook = array_unique($cook);
                    $cart_count = count($cook);
                } else {
                    $cart_count = count($dbArr);
                }

            }
        }

        return $cart_count;
    }

} 