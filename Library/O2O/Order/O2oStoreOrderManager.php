<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-12
 * Time: 下午2:55
 */

namespace Library\O2O\Order;

use Components\Module\CouponManager;
use Components\Module\VipcardManager;
use Components\Product\ProductManager;
use Components\Product\TransactionManager;
use Components\Rules\PointRule;
use Components\UserStatusStore;
use Library\O2O\Financial\O2oUserWalletManager;
use Library\O2O\O2oShareManager;
use Models\Modules\Pointmall\AddonPointMallItem;
use Models\Modules\Pointmall\AddonPointMallOrder;
use Models\Modules\Vipcard\AddonVipcardUsers;
use Models\Product\Product;
use Models\Shop\ShopCart;
use Models\Shop\ShopFreightTpl;
use Models\Shop\ShopFreightTplItems;
use Models\Shop\ShopOrderCombine;
use Models\Shop\ShopOrderItem;
use Models\Shop\ShopOrders;
use Models\User\UserForCustomers;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use Util\Ajax;
use Util\Cookie;
use Util\EasyEncrypt;

class O2oStoreOrderManager extends Component
{
    private static $instance = null;

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // 获取需要支付的商品
    public function getSettleData($discount_rate)
    {
        // vip卡折扣率
        $discount_rate = intval($discount_rate) >= 0 && intval($discount_rate) <= 100 ? intval($discount_rate) : 100;
        $discount_rate = $discount_rate / 100;
        $all = $this->dispatcher->getParam(0);
        $from = $this->request->get('f');
        $cart = json_decode(base64_decode($all), true);
        $user_id = UserStatusStore::getUid();
        $list = [];
        $total_cash = 0;
        $paid_cash = 0;
        $order_preferential = 0; //订单优惠
        $discount_amount = 0; //vip折扣优惠
        $return_points = 0; //返回积分数
        $shop = TransactionManager::instance(HOST_KEY)->getStoreSetting(CUR_APP_ID, true);

        // 来自商品详情页面
        $settle_data = [];
        if ($from == 'item') {
            $item = Product::findFirst('id=' . $cart['id']);
            $item = $item ? $item->toArray() : [];
            $item['spec_data'] = "";
            $sell_price = 0;
            if (($discount_rate * $item['original_price']) < $item['sell_price']) //折扣后的价格比优惠价格更低
            {
                $sell_price = $discount_rate * $item['original_price'];
                $discount_amount = ($item['original_price'] - $sell_price) * $cart['num'];
            } else {
                $sell_price = $item['sell_price'];
                $order_preferential = ($item['original_price'] - $sell_price) * $cart['num'];
            }
            $total_cash = $item['original_price'] * $cart['num']; //原始总价
            $item['subtotal'] = $item['original_price'] * $cart['num'];
            // 有规格则替换
            if ($cart['spec']) {
                $spec_data = ProductManager::instance()->getProductSpecData($cart['item_id'], $cart['spec']);
                $item['spec_data'] = $spec_data['spec_data'];

                // 规格下数量
                if ($spec_data['num'] < $cart['num']) {
                    $cart['num'] = $spec_data['num'];
                }

                // todo 数量为空
                if ($cart['num'] == 0) {
                }
                if (($discount_rate * $spec_data['original_price']) < $spec_data['sell_price']) //折扣后的价格比优惠价格更低
                {
                    $sell_price = $discount_rate * $spec_data['original_price'];
                    $discount_amount = ($spec_data['original_price'] - $sell_price) * $cart['num'];

                } else {
                    $sell_price = $spec_data['sell_price'];
                    $order_preferential = ($spec_data['original_price'] - $sell_price) * $cart['num'];


                }
                $total_cash = $spec_data['original_price'] * $cart['num'];
                $item['subtotal'] = $spec_data['original_price'] * $cart['num'];
            }

            // 计算支付金额
            $paid_cash = $sell_price * $cart['num']; //最后售价

            $item['num'] = $cart['num'];
            $list[$cart['id']] = $item;
            //
            $settle_data[] = $cart;
        }

        // 购物车页面
        if ($from == 'cart') {
            $res = ShopCart::find('customer_id=' . CUR_APP_ID . '  and user_id = ' . $user_id . ' and  item_id in (' . implode(',', array_values($cart)) . ')');
            if ($res) {
                $res = $res->toArray();
                foreach ($res as $shop_cart) {
                    $item = Product::findFirst('id=' . $shop_cart['item_id']);
                    if ($item) {
                        $item = $item->toArray();
                        $sell_price = 0;

                        //  $total_cash = $item['original_price'] * $shop_cart['num'];//原始总价
                        // 获取规格信息（替换原有）
                        $spec_data = ProductManager::instance()->getProductSpecData($shop_cart['id'], $shop_cart['spec']);

                        if ($shop_cart['spec'] && $spec_data) {
                            // $shop_cart['spec_data'] = $spec_data['spec_data'];

                            // 规格下数量
                            if ($spec_data['num'] < $shop_cart['num']) {
                                $shop_cart['num'] = $spec_data['num'];
                            }
                            if (($discount_rate * $spec_data['original_price']) < $spec_data['sell_price']) //折扣后的价格比优惠价格更低
                            {
                                $sell_price = $discount_rate * $spec_data['original_price'];
                                $discount_amount += ($spec_data['original_price'] - $sell_price) * $shop_cart['num'];
                            } else {
                                $sell_price = $spec_data['sell_price'];
                                $order_preferential += ($spec_data['original_price'] - $sell_price) * $shop_cart['num'];
                            }
                            // 价格信息


                            $shop_cart['sell_price'] = $sell_price;
                            $shop_cart['original_price'] = $spec_data['original_price'];
                            $shop_cart['subtotal'] = $shop_cart['original_price'] * $shop_cart['num'];

                            // 计算支付金额
                            $paid_cash += $sell_price * $shop_cart['num'];

                            $total_cash += $spec_data['original_price'] * $shop_cart['num'];

                        } else {
                            if (($discount_rate * $item['original_price']) < $item['sell_price']) //折扣后的价格比优惠价格更低
                            {
                                $sell_price = $discount_rate * $item['original_price'];
                                $discount_amount += ($item['original_price'] - $sell_price) * $shop_cart['num'];
                            } else {
                                $sell_price = $item['sell_price'];
                                $order_preferential += ($item['original_price'] - $sell_price) * $shop_cart['num'];
                            }
                            $shop_cart['sell_price'] = $sell_price;
                            $shop_cart['original_price'] = $item['original_price'];
                            $shop_cart['subtotal'] = $shop_cart['original_price'] * $shop_cart['num'];

                            // 计算支付金额
                            $paid_cash += $sell_price * $shop_cart['num'];
                            $total_cash += $item['original_price'] * $shop_cart['num'];

                        }

                        $shop_cart['name'] = $item['name'];
                        $shop_cart['thumb'] = $item['thumb'];
                        $shop_cart['freight_tpl'] = $item['freight_tpl'];
                        $shop_cart['volume'] = $item['volume'];
                        $shop_cart['weight'] = $item['weight'];

                        $list[] = $shop_cart;

                        $settle_data[] = array('id' => $item['id'], 'num' => $shop_cart['num'], 'spec' => $shop_cart['spec']);
                    }
                }
            }
        }
        //计算返回积分数
        $vipcard_user = AddonVipcardUsers::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $user_id);
        if ($vipcard_user && $vipcard_user->is_company == 1 && $shop['firm_orders_proportion'] > 0) {
            $return_points = floor($paid_cash / $shop['firm_orders_proportion']);
        } else if ($shop['orders_proportion'] > 0) {
            $return_points = floor($paid_cash / $shop['orders_proportion']);
        }
        return array('list' => $list, 'total_cash' => $total_cash, 'order_preferential' => $order_preferential, 'discount_amount' => $discount_amount, 'paid_cash' => $paid_cash, 'all_data' => $settle_data, 'return_points' => $return_points);
    }

    // 生成订单，保存到session
    public function generateOrderNumber()
    {
        $t = explode(' ', microtime());
        $strtime = $t[1];

        # 时间戳后四位+micortime+时间戳前6位随随机
        $o = substr($strtime, 6) . substr($t[0], 2, 6) . mt_rand(substr($strtime, 0, 6), 999999);

        return $o;
    }

    /**
     * 确认收货
     */
    public function confirmReceipt()
    {
        $uid = UserStatusStore::init()->getUid();
        $order_number = EasyEncrypt::decode($this->request->getPost('order'));
        if (!$order_number) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM);
        }

        // 没有被删除的订单
        $where = 'is_deleted = 0 and customer_id = ' . CUR_APP_ID . ' and user_id=' . $uid . ' and order_number="' . $order_number . '"';
        $order = ShopOrders::findFirst($where);
        if (!$order) {
            return Ajax::init()->outError(Ajax::ERROR_DATA_NOT_EXISTS, '订单未找到！');
        }

        // todo 判断订单状态
        if (!in_array($order->status, array(TransactionManager::ORDER_STATUS_WAIT_BUYER_CONFIRM_GOODS, TransactionManager::ORDER_STATUS_SELLER_DELIVERED))) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单已经完结，订单状态：' . TransactionManager::getOrderStatus($order->status));
        }

        $this->db->begin();
        try {
            // 更新订单状态
            if (!$order->update(array('status' => TransactionManager::ORDER_STATUS_TRADE_SUCCESS, 'received_time' => time()))) {
                $messages = array();
                foreach ($order->getMessages() as $message) {
                    $messages[] = $message;
                }
                throw new Exception(join(',', $messages));
            }

            // todo 担保交易处理

            // 返现
            if (!O2oUserWalletManager::init($uid)->settleCashBack($order_number)) {
                throw new Exception(join(',', [O2oUserWalletManager::$_err_msg]));
            }

            // 订单送积分+记录一下
            $ruleMng = PointRule::init(CUR_APP_ID, $uid);
            $ruleMng->executeRule($uid, PointRule::BEHAVIOR_TRADE_ONLINE);
            // ...
            $this->db->commit();

        } catch (Exception $e) {
            $this->db->rollback();
            $messages[] = $e->getMessage();
            return Ajax::init()->outError(Ajax::ERROR_RUN_TIME_ERROR_OCCURRED, join("<br>", $messages));
        }

        return Ajax::init()->outRight('');
    }

    /**
     * 结算
     */
    public function settle()
    {
        $user_id = UserStatusStore::getUid();
        $shop = TransactionManager::instance(HOST_KEY)->getStoreSetting(CUR_APP_ID, true);
        $all_item = $this->request->getPost('t');
        $addr = $this->request->getPost('addr');
        $remark = $this->request->getPost('remark');
        $freight = $this->request->getPost('freight');
        $coupon_serial = $this->request->getPost('coupon');
        $use_point = $this->request->getPost('use_point');
        $final_order_number = 0;
        $all_item = json_decode(base64_decode($all_item), true);
        if (!($all_item && $addr)) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单里无商品，无法生成订单！');
        }
        $flag_order_number = $this->session->get('_order_user_' . $user_id);
        if (!$flag_order_number) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单已经失效，请不要重复提交订单！');
        }

        $total_cash = 0;
        $paid_cash = 0;
        $item_share_list = array();

        // 开始生成订单
        $this->db->begin();

        $discount = VipcardManager::init()->getCardDiscount($user_id, CUR_APP_ID);
        $discount = $discount / 100;
        $vip_preferential_amount = 0; //vip打折节省的金额数
        $total_freight_price = 0;//总运费
        try {
            $start = 0;
            $combine_order_number = 0;
            /*不止一种物流方式,不止一个订单*/
            $order_amount = count($all_item);
            if ($order_amount > 1) {

                $combine_order_number = 'P' . self::generateOrderNumber($user_id);
                $order_count = ShopOrderCombine::count("combine_order_number='" . $combine_order_number . "'");
                while ($order_count >= 1)//订单号不能重复
                {
                    $combine_order_number = 'P' . self::generateOrderNumber($user_id);
                }
                $final_order_number = $combine_order_number;

            }

            foreach ($all_item as $k => $item) {
                foreach ($item['goods'] as $k1 => $goods) {
                    $spec = isset($goods['spec_info']['spec']) ? ProductManager::instance()->getProductSpecData($goods['id'], $goods['spec_info']['spec']) : [];
                    if ($goods['spec_info']['spec'] && $spec) {
                        if (($discount * $spec['original_price']) < $spec['sell_price']) //折扣后的价格比优惠价格更低
                        {
                            $item_sell_price = $discount * $spec['original_price'];
                        } else {
                            $item_sell_price = $spec['sell_price'];
                        }
                    } else {
                        if (($discount * $goods['original_price']) < $goods['sell_price']) //折扣后的价格比优惠价格更低
                        {
                            $item_sell_price = $discount * $goods['original_price'];
                        } else {
                            $item_sell_price = $goods['sell_price'];
                        }
                    }
                    $paid_cash += $goods['num'] * $item_sell_price;
                }
            }

            foreach ($all_item as $k => $item) {
                $order_number = self::generateOrderNumber($user_id);
                $order_count = ShopOrderItem::count("order_number=" . $order_number);
                while ($order_count >= 1)//订单号不能重复
                {
                    $order_number = self::generateOrderNumber($user_id);
                }
                $temp_paid_cash = 0;//订单支付金额(当前物流订单)
                $temp_total_cash = 0;//订单总金额(当前物流订单)
                $temp_vip_preferential_amount = 0;//订单vip优惠总金额(当前物流订单)
                foreach ($item['goods'] as $k1 => $goods) {
                    $item_new = array();
                    $item_share_list[$goods['id']] = array('num' => $goods['num'], 'sell_price' => $goods['sell_price']);
                    $sell_price = $goods['sell_price'];
                    $original_price = $goods['original_price'];
                    $spec = isset($goods['spec_info']['spec']) ? ProductManager::instance()->getProductSpecData($goods['id'], $goods['spec_info']['spec']) : [];
                    if ($goods['spec_info']['spec'] && $spec) {
                        if (($discount * $spec['original_price']) < $spec['sell_price']) //折扣后的价格比优惠价格更低
                        {
                            $item_new['item_sell_price'] = $discount * $spec['original_price'];
                            $vip_preferential_amount += ($spec['original_price'] - $sell_price) * $goods['num'];
                            $temp_vip_preferential_amount += ($spec['original_price'] - $sell_price) * $goods['num'];
                            $item_new['total_preferential'] = ($spec['original_price'] - $sell_price) * $goods['num'];
                        } else {
                            $item_new['item_sell_price'] = $spec['sell_price'];
                            $item_new['total_preferential'] = ($spec['original_price'] - $sell_price) * $goods['num'];
                        }
                        $original_price = $spec['original_price'];
                    } else {
                        if (($discount * $goods['original_price']) < $goods['sell_price']) //折扣后的价格比优惠价格更低
                        {
                            $item_new['item_sell_price'] = $discount * $goods['original_price'];
                            $vip_preferential_amount += ($goods['original_price'] - $sell_price) * $goods['num'];
                            $temp_vip_preferential_amount += ($goods['original_price'] - $sell_price) * $goods['num'];
                            $item_new['total_preferential'] = ($goods['original_price'] - $sell_price) * $goods['num'];
                        } else {
                            $item_new['item_sell_price'] = $goods['sell_price'];
                            $item_new['total_preferential'] = ($goods['original_price'] - $sell_price) * $goods['num'];
                        }
                    }

                    $item_new['user_id'] = $user_id;
                    $item_new['order_number'] = $order_number;
                    $item_new['item_id'] = $goods['id'];
                    $item_new['item_name'] = $goods['name'];
                    $item_new['item_price'] = $sell_price;
                    $item_new['item_original_price'] = $original_price;
                    $item_new['item_spec'] = $goods['spec_info']['spec'];
                    $item_new['item_spec_data'] = isset($goods['spec_info']['spec']) ? ProductManager::instance()->getProductSpecData($goods['id'], $goods['spec_info']['spec'], 'spec_data') : '';
                    $item_new['quantity'] = $goods['num'];
                    $item_new['thumb'] = $goods['thumb'];
                    $item_new['total_cash'] = $goods['num'] * $sell_price;
                    $item_new['created'] = $goods['created'];
                    $orderItem = new ShopOrderItem();
                    if (!$orderItem->create($item_new)) {
                        $messages = [];
                        foreach ($orderItem->getMessages() as $message) {
                            $messages[] = $message;
                        }
                        throw new \Phalcon\Exception(join(',', $messages));
                    };
                    $total_cash += $item_new['quantity'] * $goods['original_price'];
                    $temp_total_cash += $item_new['quantity'] * $goods['original_price'];
                    $temp_paid_cash += $item_new['quantity'] * $sell_price;

                }

                //包邮
                if ($shop['free_postage'] > $paid_cash) {
                    $total_freight_price += $item['freight_tpl'][$freight[$start]]['total_freight_money'];
                }

                /*一种物流方式,一个订单*/
                if ($order_amount == 1) {
                    $discount_paid_cash = $paid_cash;
                    $final_order_number = $order_number;
                    // 优惠劵
                    $coupon_minus = 0;
                    if (strlen(trim($coupon_serial)) == 16) {
                        $coupon_minus = CouponManager::getCouponDiscountBySerial(trim($coupon_serial), $discount_paid_cash);
                        if (!$coupon_minus) {
                            $coupon_serial = '';
                        }
                    } else {
                        $coupon_serial = '';
                    }

                    // 使用优惠劵后
                    $final_paid_cash = $discount_paid_cash - $coupon_minus;

                    //---积分支付相关---
                    $pay_point = 0;
                    $point_pay_all = false; //积分全部支付
                    $vipcard_user = AddonVipcardUsers::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $user_id);
                    if ($use_point > 0) //使用了积分支付
                    {
                        $user_info = UserForCustomers::findFirst('user_id=' . $user_id . " AND customer_id=" . CUR_APP_ID);
                        $vipcard_user = AddonVipcardUsers::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $user_id);
                        $redeem_proportion = $shop['redeem_proportion'];

                        if ($vipcard_user && $vipcard_user->is_company == 1 && $shop['firm_redeem_proportion'] > 0) {
                            $redeem_proportion = $shop['firm_redeem_proportion'];
                        }


                        $point = UserStatusStore::init()->getPoint();
                        if ($final_paid_cash > 0) {

                            //输入的积分数大于需要扣除的积分数
                            if ($point > 0 && ($use_point / $redeem_proportion) >= ($final_paid_cash + $total_freight_price)) {
                                $pay_point = ceil(($final_paid_cash + $total_freight_price) * $redeem_proportion);
                                $final_paid_cash = 0;
                                $post_fee = 0;
                                $point_pay_all = true;
                            } else {
                                $final_paid_cash = $final_paid_cash - round($use_point / $redeem_proportion, 0);
                                $pay_point = $use_point;
                            }

                        }
                    }
                    $orders_proportion = $shop['orders_proportion'];
                    if ($vipcard_user && $vipcard_user->is_company == 1 && $shop['firm_orders_proportion'] > 0) {
                        $orders_proportion = $shop['firm_orders_proportion'];
                    }

                    // 获取订单奖励积分
                    $award_points = 0;
                    if ($orders_proportion > 0) {
                        $award_points = floor($paid_cash / $orders_proportion);
                    }
                    // 2 生成订单
                    $orderData = array(
                        'order_number' => $order_number,
                        'customer_id' => CUR_APP_ID,
                        'user_id' => $user_id,
                        'created' => time(),
                        'remark' => $remark,
                        'address_id' => $addr,
                        'is_paid' => 0,
                        'total_cash' => $total_cash,
                        'paid_cash' => $final_paid_cash, // 最终支付的价格
                        //   'logistics_type' => $post_key,
                        'freight_type' => $freight[$start],
                        'logistics_fee' => $shop['free_postage'] > $paid_cash ? floatval($item['freight_tpl'][$freight[$start]]['total_freight_money']) : 0,
                        'discount_cash' => $vip_preferential_amount, //优惠多少
                        'order_award_points' => $award_points,
                        'use_coupon' => $coupon_serial,
                        'use_point' => $use_point,
                        'combine_order_number' => $combine_order_number,
                        'status' => $point_pay_all ? TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS : TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY
                    );

                    // log
                    /*                   $orderData_log = var_export($orderData, true);
                                       Debug::log('ORDER:- order data: ' . $orderData_log);*/

                    $order = new ShopOrders();
                    if (!$order->create($orderData)) {
                        $messages = '';
                        foreach ($order->getMessages() as $message) {
                            $messages[] = $message;
                        }
                        throw new \Phalcon\Exception(join(',', $messages));
                    }

                    // 生成优惠劵订单
                    if ($coupon_minus && !empty($coupon_minus)) {
                        CouponManager::generateCouponOrder($coupon_serial, $order_number, $coupon_minus);
                    }

                    //更新用户积分数
                    $this->updateUserPoints($user_id, $pay_point);
                    // 3 清除购物车
                    $cart = ShopCart::find('customer_id=' . CUR_APP_ID . ' and user_id=' . $user_id);
                    if (!$cart->delete()) {
                        $messages = '';
                        foreach ($cart->getMessages() as $message) {
                            $messages[] = $message;
                        }
                        throw new \Phalcon\Exception(join(',', $messages));
                    }


                } else {

                    $vipcard_user = AddonVipcardUsers::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $user_id);
                    $orders_proportion = $shop['orders_proportion'];
                    if ($vipcard_user && $vipcard_user->is_company == 1 && $shop['firm_orders_proportion'] > 0) {
                        $orders_proportion = $shop['firm_orders_proportion'];
                    }

                    // 获取订单奖励积分
                    $award_points = 0;
                    if ($orders_proportion > 0) {
                        $award_points = floor($temp_paid_cash / $orders_proportion);
                    }
                    // 2 生成订单
                    $orderData = array(
                        'order_number' => $order_number,
                        'customer_id' => CUR_APP_ID,
                        'user_id' => $user_id,
                        'created' => time(),
                        'remark' => $remark,
                        'address_id' => $addr,
                        'is_paid' => 0,
                        'total_cash' => $temp_total_cash,
                        'paid_cash' => $temp_paid_cash, // 最终支付的价格
                        //   'logistics_type' => $post_key,
                        'logistics_fee' => $shop['free_postage'] > $paid_cash ? floatval($item['freight_tpl'][$freight[$start]]['total_freight_money']) : 0,
                        'freight_type' => $freight[$start],
                        'discount_cash' => $temp_vip_preferential_amount, //优惠多少
                        'order_award_points' => $award_points,
                        'use_coupon' => '',
                        'use_point' => 0,
                        'combine_order_number' => $combine_order_number,
                        'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY
                    );

                    $order = new ShopOrders();
                    if (!$order->create($orderData)) {
                        $messages = '';
                        foreach ($order->getMessages() as $message) {
                            $messages[] = $message;
                        }
                        throw new \Phalcon\Exception(join(',', $messages));
                    }
                }
                $start++;
            }
            if (count($all_item) > 1) {
                $discount_paid_cash = $paid_cash;

                // 优惠劵
                $coupon_minus = 0;
                if (strlen(trim($coupon_serial)) == 16) {
                    $coupon_minus = CouponManager::getCouponDiscountBySerial(trim($coupon_serial), $discount_paid_cash);
                    if (!$coupon_minus) {
                        $coupon_serial = '';
                    }
                } else {
                    $coupon_serial = '';
                }

                // 使用优惠劵后
                $final_paid_cash = $discount_paid_cash - $coupon_minus;

                //---积分支付相关---
                $pay_point = 0;
                $point_pay_all = false; //积分全部支付
                $vipcard_user = AddonVipcardUsers::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $user_id);
                if ($use_point > 0) //使用了积分支付
                {
                    $user_info = UserForCustomers::findFirst('user_id=' . $user_id . " AND customer_id=" . CUR_APP_ID);
                    $vipcard_user = AddonVipcardUsers::findFirst("customer_id=" . CUR_APP_ID . " AND user_id=" . $user_id);
                    $redeem_proportion = $shop['redeem_proportion'];

                    if ($vipcard_user && $vipcard_user->is_company == 1 && $shop['firm_redeem_proportion'] > 0) {
                        $redeem_proportion = $shop['firm_redeem_proportion'];
                    }


                    $point = UserStatusStore::init()->getPoint();
                    if ($final_paid_cash > 0) {

                        //输入的积分数大于需要扣除的积分数
                        if ($point > 0 && ($use_point / $redeem_proportion) >= ($final_paid_cash + $total_freight_price)) {
                            $pay_point = ceil(($final_paid_cash + $total_freight_price) * $redeem_proportion);
                            $final_paid_cash = 0;
                            $post_fee = 0;
                            $point_pay_all = true;
                        } else {
                            $final_paid_cash = $final_paid_cash - round($use_point / $redeem_proportion, 0);
                            $pay_point = $use_point;
                        }

                    }
                }
                $orders_proportion = $shop['orders_proportion'];
                if ($vipcard_user && $vipcard_user->is_company == 1 && $shop['firm_orders_proportion'] > 0) {
                    $orders_proportion = $shop['firm_orders_proportion'];
                }

                // 获取订单奖励积分
                $award_points = 0;
                if ($orders_proportion > 0) {
                    $award_points = floor($paid_cash / $orders_proportion);
                }
                $shopOrderCombine = new ShopOrderCombine();
                $shopOrderCombine->combine_order_number = $combine_order_number;
                $shopOrderCombine->customer_id = CUR_APP_ID;
                $shopOrderCombine->user_id = $user_id;
                $shopOrderCombine->createed = time();
                $shopOrderCombine->total_cash = $total_cash;
                $shopOrderCombine->paid_cash = $final_paid_cash;
                $shopOrderCombine->discount_cash = $vip_preferential_amount;
                $shopOrderCombine->back_coin = $award_points;
                $shopOrderCombine->order_award_points = $award_points;
                $shopOrderCombine->address_id = $addr;
                $shopOrderCombine->is_paid = 0;
                $shopOrderCombine->status = TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY;
                $shopOrderCombine->remark = $remark;
                $shopOrderCombine->use_coupon = $coupon_serial;
                $shopOrderCombine->use_point = $use_point;
                $shopOrderCombine->logistics_fee = $total_freight_price;
                if (!$shopOrderCombine->create()) {
                    $messages = '';
                    foreach ($shopOrderCombine->getMessages() as $message) {
                        $messages[] = $message;
                    }
                    throw new \Phalcon\Exception(join(',', $messages));
                }
                //更新用户积分数
                $this->updateUserPoints($user_id, $pay_point);
                // 3 清除购物车
                $cart = ShopCart::find('customer_id=' . CUR_APP_ID . ' and user_id=' . $user_id);
                if (!$cart->delete()) {
                    $messages = '';
                    foreach ($cart->getMessages() as $message) {
                        $messages[] = $message;
                    }
                    throw new \Phalcon\Exception(join(',', $messages));
                }


            }
            // 分享返积分记录
            O2oShareManager::init()->addOrderLog($item_share_list, $order_number, "store");
            // 4 删除订单号session信息 避免重复提交
            $this->session->remove('_order_user_' . $user_id);

            $this->db->commit();

            // 清除信息避免重复
            Cookie::del('_cart_settle_page');
            unset($_SERVER['HTTP_REFERER']);

        } catch (\Exception $e) {
            $messages = $e->getMessage();
            $this->db->rollback();
            $this->db->get("errorLogger")->error($messages);
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $messages);
        }
<<<<<<< HEAD:Library/Components/Product/OrderManager.php
        // Debug::log('ORDER:- end: ');
        return $final_order_number;
=======

        return Ajax::init()->outRight('', $final_order_number);
>>>>>>> d260ed2ab179806bf8f18c6d53a79b63660a343c:Library/O2O/Order/O2oStoreOrderManager.php
    }

    // 积分商城订单
    public function pointMallSettle()
    {
        $item_info = $this->request->getPost('t');
        $addr = $this->request->getPost('addr');
        $remark = $this->request->getPost('remark');
        $post_key = $this->request->getPost('postage');

        $this->ajax = new Ajax();

        $buy_item = json_decode(base64_decode($item_info), true);
        if (!($buy_item && $addr)) {
            return Ajax::init()->outError(Ajax::ERROR_INVALID_REQUEST_PARAM, '订单里无商品，无法生成订单！');
        }

        $order_number = $this->session->get('_order_user_' . $user_id);
        if (!$order_number) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单已经失效，请不要重复提交订单！');
        }

        $product = Product::findFirst('customer_id = ' . CUR_APP_ID . ' and id = ' . $buy_item['id']);
        if (!$product) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '未找到对应商品，订单失效！');
        }

        $pointMallItem = AddonPointMallItem::findFirst('item_id=' . $buy_item['id']);
        if (!$pointMallItem) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '未找到积分换购商品，订单失效！');
        }

        // 再次判断积分
        $user = UserForCustomers::findFirst('customer_id=' . CUR_APP_ID . ' and user_id=' . $user_id);
        if (!$user) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '无法找到用户信息');
        }

        // 计算
        $total_point = $pointMallItem->cost_point * $buy_item['num'];
        $total_cash = $pointMallItem->cost_cash * $buy_item['num'];

        if ($user->points_available < $total_point) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '对不起！您的积分不够，不能生成订单');
        }

        // todo 计算邮费
        $postage = TransactionManager::instance(HOST_KEY)->getCustomerLogisticsByType($post_key, CUR_APP_ID);
        $post_fee = isset($postage['fee']) ? $postage['fee'] : 0.00;

        // 开始生成订单
        $this->db->begin();
        // Debug::log('ORDER:-begin transtion');
        try {
            $item['order_number'] = $order_number;
            $item['item_id'] = $buy_item['id'];
            $item['item_name'] = $pointMallItem->item_name;
            $item['item_price'] = $pointMallItem->cost_cash; // 最终价格（可能被改价）
            $item['item_sell_price'] = $product->sell_price;
            $item['item_original_price'] = $product->original_price;
            $item['quantity'] = $buy_item['num'];
            $item['thumb'] = $product->thumb;
            $item['total_cash'] = $total_cash;
            $item['total_preferential'] = 0;
            // 商品型号
            $item['spec'] = json_encode($buy_item['spec'], JSON_UNESCAPED_UNICODE);
            $item['created'] = time();

            // log
            $item_log = var_export($item, true);
            // Debug::log('ORDER:- item data: ' . $item_log);

            // 加入下单列表
            $orderItem = new ShopOrderItem();
            if (!$orderItem->save($item)) {
                $messages = [];
                foreach ($orderItem->getMessages() as $message) {
                    $messages[] = $message;
                }
                throw new Exception(join(',', $messages));
            };

            // 更新原始商品库存
            if (!$product->update(array('quantity' => $product->quantity - $buy_item['num']))) {
                $messages = [];
                foreach ($product->getMessages() as $message) {
                    $messages[] = $message;
                }
                throw new Exception(join(',', $messages));
            };

            // 更新换购商品库存
            if (!$pointMallItem->update(array('item_quantity' => $pointMallItem->item_quantity - $buy_item['num']))) {
                foreach ($product->getMessages() as $message) {
                    $messages[] = $message;
                }
                throw new Exception(join(',', $messages));
            }

            // 2 生成原始订单
            $orderData = array(
                'order_number' => $order_number,
                'customer_id' => CUR_APP_ID,
                'user_id' => $user_id,
                'created' => time(),
                'remark1' => $remark,
                'address_id' => $addr,
                'is_paid' => 0,
                'total_cash' => $product->sell_price * $buy_item['num'],
                'paid_cash' => $total_cash, // 最终优惠后的价格
                'logistics_type' => $post_key,
                'logistics_fee' => $post_fee,
                'discount_cash' => 0, //优惠多少
                'back_coin' => 0, // 返回积分
                'is_pointmall' => 1,
                'status' => TransactionManager::ORDER_STATUS_WAIT_SELLER_SEND_GOODS
            );

            // log
            $orderData_log = var_export($orderData, true);
            // Debug::log('ORDER:- order data: ' . $orderData_log);

            $order = new ShopOrders();
            if (!$order->save($orderData)) {
                foreach ($order->getMessages() as $message) {
                    $messages[] = $message;
                }
                throw new Exception(join(',', $messages));
            }

            // 3 生成积分商城订单
            $pointData = array(
                'order_number' => $order_number,
                'customer_id' => CUR_APP_ID,
                'user_id' => $user_id,
                'item_id' => $buy_item['id'],
                'num' => $buy_item['num'],
                'total_point' => $total_point,
                'total_cash' => $total_cash,
                'created' => time()
            );
            // log
            $pointData_log = var_export($pointData, true);
            // Debug::log('ORDER:- $pointData: ' . $pointData_log);

            $order = new AddonPointMallOrder();
            if (!$order->save($pointData)) {
                foreach ($order->getMessages() as $message) {
                    $messages[] = $message;
                }
                throw new Exception(join(',', $messages));
            }

            // 删除订单号session信息 避免重复提交
            $this->session->remove('_order_user_' . $user_id);

            $this->db->commit();

            // 清除信息避免重复
            Cookie::del('_cart_settle_page');
            unset($_SERVER['HTTP_REFERER']);
        } catch (\Exception $e) {
            $this->db->rollback();
            $messages[] = $e->getMessage();
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, join("<br>", $messages));
        }

        // Debug::log('ORDER:- end: ');

        return $order_number;
    }

    /*积分支付后更新用户的可用积分*/
    public function  updateUserPoints($user_id, $points)
    {
        if ($points > 0) {
            $user = UserForCustomers::findFirst('customer_id=' . CUR_APP_ID . " AND user_id=" . $user_id);
            $user->points_available = $user->points_available - $points >= 0 ? $user->points_available - $points : 0;
            $user->points = $user->points - $points >= 0 ? $user->points - $points : 0;
            $user->update();
        }

    }

    /*获取订单里的商品信息*/
    public function getOrderItem($order)
    {
        $list = $this->db->query("Select soi.*,ifnull(sor.type,'0') as order_type,
              sor.is_seller_accept,sor.is_end
              from shop_order_item as soi left join  shop_order_return as sor ON soi.id=sor.order_item_id WHERE soi.order_number=" . $order['order_number'])
            ->fetchAll();
        foreach ($list as $k => $val) {
            if (($order['status'] == TransactionManager::ORDER_STATUS_TRADE_SUCCESS) && (((time() - $order['received_time']) / (60 * 60 * 24)) < 7) && ($order['use_point'] <= 0) && ($val['order_type'] == 0)) {
                $list[$k]['return_show'] = true;
            } else {
                $list[$k]['return_show'] = false;
            }
        }
        return $list;
    }

    /*
     * 检查商品是否符合退换货条件
     * @item_id int shop_order_item表的主键id
      @type int 0-退货或者换货,1-换货,2-退货
    */
    public function checkReturnProduct($item_id, $type = 0)
    {
        $item = ShopOrderItem::findFirst('id=' . $item_id);
        if (!$item) {
            return null;
        } else {
            $order = ShopOrders::findFirst('order_number=' . $item->order_number);
            if ($order && $order->status == TransactionManager::ORDER_STATUS_TRADE_SUCCESS && ((time() - $order->received_time) / (60 * 60 * 24)) < 7 && $order->use_point <= 0) {
                return $item->toArray();
            }
            return null;
        }
    }

    /*获取退/换货商品列表*/
    public function getReturnGoods($customer_id, $user_id)
    {
        $list = $this->db->query(
            "select sor.*,soi.created,soi.thumb,soi.item_spec_data,soi.item_name,soi.total_cash,soi.item_price from
              shop_order_return as sor left join shop_order_item as soi on sor.order_item_id=soi.id
               where sor.user_id=" . $user_id . " and sor.customer_id=" . $customer_id
        )->fetchAll();
        //  $list=ShopOrderReturn::find("user_id=".$user_id." and customer_id=".$customer_id);
        //  $list =$list ? $list->toArray() :[];
        foreach ($list as $k => $v) {
            if ($v['type'] == 2) //退货
            {
                if ($v['is_seller_accept'] == 0) //未处理
                {
                    $list[$k]['button'] = "<span class='cancelBack btn'>取消退货</span><span class='btn waitCheck'>待审核</span>";
                } else if ($v['is_seller_accept'] == 2) //拒绝
                {
                    $list[$k]['button'] = "<span class='btn refuseBack'>卖家拒绝退货</span>";
                } else {
                    if ($v['is_end'] == 1) //流程已结束
                    {
                        if ($v['is_seller_refunded'] == 1) {
                            $list[$k]['button'] = "<span class='handleOver btn'>退款成功</span>";
                        }

                    } else if ($v['is_buyer_delivered'] == 0) //买家未发货
                    {

                        $list[$k]['button'] = "<span class='cancelBack btn'>取消退货</span><span class='backGood btn'>回寄商品</span>";
                    } else if ($v['seller_received_goods'] == 0) //卖家未收到货物
                    {
                        $list[$k]['button'] = "<span class='waitReceive btn'>等待卖家收货</span>";
                    } else if ($v['is_seller_refunded'] == 0) //卖家未退款
                    {
                        $list[$k]['button'] = "<span class='waitPay btn'>等待卖家退款</span>";
                    } else if ($v['is_send'] == 0) //流程未结束
                    {
                        $list[$k]['button'] = "<span class='waitCheck btn'>待审核</span>";
                    } else {
                        $list[$k]['button'] = "<span class='waitCheck btn'>待审核</span>";
                    }
                }
            }
            if ($v['type'] == 1) //换货
            {

                if ($v['is_seller_accept'] == 0) //未处理
                {
                    $list[$k]['button'] = "<span class='cancelBack btn'>取消换货</span><span class='btn waitCheck'>待审核</span>";
                } else if ($v['is_seller_accept'] == 2) //拒绝
                {
                    $list[$k]['button'] = "<span class='btn refuseBack'>卖家拒绝换货</span>";
                } else {
                    if ($v['is_end'] == 1) //流程已结束
                    {
                        if ($v['back_order_number'] != 0) {
                            $list[$k]['button'] = "<span class='handleOver btn'>换货完成</span>";
                        } else if ($v['is_seller_refunded'] == 1) {
                            $list[$k]['button'] = "<span class='handleOver btn'>退款成功</span>";
                        }

                    } else if ($v['is_buyer_delivered'] == 0) //买家未发货
                    {
                        $list[$k]['button'] = "<span class='cancelBack btn'>取消换货</span><span class='backGood btn'>回寄商品</span>";
                    } else if ($v['seller_received_goods'] == 0) //卖家未收到货物
                    {
                        $list[$k]['button'] = "<span class='waitReceive btn'>等待卖家收货</span>";
                    } else if ($v['is_seller_refunded'] == 0) //卖家未退款
                    {
                        $list[$k]['button'] = "<span class='waitPay btn'>等待卖家退款</span>";
                    } else if ($v['is_seller_refunded'] == 1 && $v['is_send'] == 0) //已退款,但是还没有结束
                    {
                        $list[$k]['button'] = "<span class='waitPay btn'>换货处理中</span>";
                    } else {
                        $list[$k]['button'] = "<span class='waitCheck btn'>待审核</span>";
                    }
                }

            }

        }
        // echo "<pre>";var_dump($list);exit;
        return $list;
    }

    /*
     *
     * 获取当前可选物流及当前运费总和
     * @parm list array  购物车里的商品列表
     *
     */
    public function getFreight($list, $spec_info, $address)
    {
        $res = array(
            'mail' => array(),
            'express' => array(),
            'ems' => array(),
            'express_mail' => array(),
            'express_ems' => array(),
            'mail_ems' => array(),
            'express_mail_ems' => array(),
        );
        //第一步 : 获取每个商品可以使用的配送方式
        $spec_start = 0;
        foreach ($list as $k => $val) {
            $list[$k]['valuation_way'] = $this->get_valuation_way($val['freight_tpl']);
            $list[$k]['spec_info'] = isset($spec_info[$spec_start]) ? $spec_info[$spec_start] : [];
            $list[$k]['freight_tpl_id'] = $val['freight_tpl'];
            $list[$k]['freight_tpl'] = $this->getFreightTpl($val['freight_tpl'], $address);
            $spec_start++;
        }
        //第二步 : 合并配送方式(相同的合并)
        foreach ($list as $k => $val) {
            /*三种物流都支持*/
            if ($val['freight_tpl']['express'] && $val['freight_tpl']['mail'] && $val['freight_tpl']['ems']) {
                $res['express_mail_ems'][] = $val;
            } /*支持快递和ems*/
            else if ($val['freight_tpl']['express'] && $val['freight_tpl']['ems'] && !$val['freight_tpl']['mail']) {
                $res['express_ems'][] = $val;
            } /*支持快递和平邮*/
            else if ($val['freight_tpl']['express'] && $val['freight_tpl']['mail'] && !$val['freight_tpl']['ems']) {
                $res['express_mail'][] = $val;
            } /*支持平邮和EMS*/
            else if ($val['freight_tpl']['mail'] && $val['freight_tpl']['ems'] && !$val['freight_tpl']['express']) {
                $res['mail_ems'][] = $val;
            } /*支持快递*/
            else if ($val['freight_tpl']['express'] && !$val['freight_tpl']['mail'] && !$val['freight_tpl']['ems']) {
                $res['express'][] = $val;
            } /*支持快递*/
            else if ($val['freight_tpl']['ems'] && !$val['freight_tpl']['mail'] && !$val['freight_tpl']['express']) {
                $res['ems'][] = $val;
            } /*支持快递*/
            else if ($val['freight_tpl']['mail'] && !$val['freight_tpl']['express'] && !$val['freight_tpl']['ems']) {
                $res['mail'][] = $val;
            }
        }

        //第三步:根据优先级合并精简配送方式(express>mail>EMS)
        if ($res) {
            if (!empty($res['express'])) {
                $res['express'] = array_merge($res['express'], $res['express_ems']);
                $res['express'] = array_merge($res['express'], $res['express_mail']);
                $res['express'] = array_merge($res['express'], $res['express_mail_ems']);
                unset($res['express_ems']);
                unset($res['express_mail']);
                unset($res['express_mail_ems']);
                if (!empty($res['mail'])) {
                    $res['mail'] = array_merge($res['ems'], $res['mail_ems']);
                    unset($res['mail_ems']);
                }
            } else {
                if ((!empty($res['express_mail']) && !empty($res['express_ems'])) || (!empty($res['express_ems']) && !empty($res['express_mail_ems']))) {
                    $res['express'] = array_merge($res['express'], $res['express_ems']);
                    $res['express'] = array_merge($res['express'], $res['express_mail']);
                    $res['express'] = array_merge($res['express'], $res['express_mail_ems']);
                    unset($res['express_ems']);
                    unset($res['express_mail']);
                    unset($res['express_mail_ems']);
                } else {
                    if (!empty($res['express_mail'])) {
                        $res['express_mail'] = array_merge($res['express_mail'], $res['express_mail_ems']);
                        unset($res['express_mail_ems']);
                    } else if (!empty($res['express_ems'])) {
                        $res['express_ems'] = array_merge($res['express_ems'], $res['express_mail_ems']);
                        unset($res['express_mail_ems']);

                    }
                }
                $res['mail'] = array_merge($res['mail'], $res['mail_ems']);
                unset($res['mail_ems']);
            }
        }
        //第四步 : 去空合并
        $result = array();
        foreach ($res as $k => $val) {
            if (empty($val)) {
                unset($res[$k]);
            } else {
                $result[$k]['goods'] = $val;
            }
        }

        //第五步 : 计算运费
        foreach ($result as $k => $val) {
            $type = explode('_', $k); //
            foreach ($type as $k2 => $val2) {
                $fr_one_money = 0; //首件最划算的价钱
                $fr_tpl_id = 0; //选为首件的运费模板id
                $total_freight_money = 0; //最终运费
                $tpl_array = array();
                //获取以哪个商品做首件
                foreach ($val['goods'] as $k1 => $val1) {
                    if (array_key_exists($val1['freight_tpl_id'], $tpl_array)) {
                        $tpl_array[$val1['freight_tpl_id']]['count'] += ($val1['valuation_way'] == 'number' ? $val1['num'] : $val1[$val1['valuation_way']]);
                    } else {
                        $tpl_array[$val1['freight_tpl_id']]['count'] = ($val1['valuation_way'] == 'number' ? $val1['num'] : $val1[$val1['valuation_way']]);
                        $tpl_array[$val1['freight_tpl_id']]['info'] = $val1['freight_tpl'][$val2][0];
                    }
                    if ($fr_one_money == 0 || ($val1['freight_tpl'][$val2][0]['first_cash'] < $fr_one_money)) {
                        $fr_one_money = $val1['freight_tpl'][$val2][0]['first_cash'];
                        $fr_tpl_id = $val1['freight_tpl_id'];
                    }
                }
                foreach ($tpl_array as $k1 => $val1) {
                    if ($k1 == $fr_tpl_id) //首件计费模板
                    {
                        if ($val1['info']['first_num'] > $val1['count']) {
                            $total_freight_money += $val1['info']['first_cash'];
                        } else {
                            $co = ceil($val1['count'] - $val1['info']['first_num']) / $val1['info']['addon_num'];
                            $total_freight_money += ($val1['info']['first_cash'] + $co * $val1['info']['addon_cash']);
                        }
                    } else //续件计费模板
                    {
                        $co = ceil($val1['count'] / $val1['info']['addon_num']);
                        $total_freight_money += $co * $val1['info']['addon_cash'];
                    }
                }
                $result[$k]['freight_tpl'][$val2]['total_freight_money'] = $total_freight_money;
            }
        }
        return $result;
    }

    /*
     * 获取物流方式
     * */
    public function  getFreightTpl($id, $address)
    {

        $res = array(
            'mail' => array(),
            'express' => array(),
            'ems' => array()
        );
        $freight = ShopFreightTplItems::find("customer_id=" . CUR_APP_ID . " AND freight_id=" . $id);

        if ($freight) {
            $freight = $freight->toArray();
            foreach ($freight as $k => $v) {
                if (($v['is_national'] == 1) || ($v['is_national'] == 0 && strpos($v['district_cn'], $address['province']))) {
                    $res[$v['mail_mode']][] = $v;
                }
            }
        }
        return $res;
    }

    /*
     * 获取运费模板计费方式
     *
     * */
    public function  get_valuation_way($freight_id)
    {
        $shopFreightTpl = ShopFreightTpl::findFirst("customer_id= '" . CUR_APP_ID . "' AND id= '" . $freight_id . "'");
        if ($shopFreightTpl) {
            return $shopFreightTpl->valuation_way;
        } else {
            return false;
        }

    }
}
