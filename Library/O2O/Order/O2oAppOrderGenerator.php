<?php
/**
 * Created by PhpStorm.
 * User: yanue-mi
 * Date: 14-9-12
 * Time: 下午2:55
 */

namespace Library\O2O\Order;

use Components\Product\ProductManager;
use Components\Product\TransactionManager;
use Components\UserStatusApp;
use Library\O2O\O2oShareManager;
use Models\O2O\Order\O2oShopCart;
use Models\O2O\Order\O2oShopOrder;
use Models\O2O\Order\O2oShopOrderCombine;
use Models\O2O\Order\O2oShopOrderItem;
use Models\O2O\Order\O2oShopOrderPick;
use Models\Shop\Shop;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use Util\Ajax;
use Util\Cookie;

class O2oAppOrderGenerator extends Component
{
    const ERROR_FAIL_CREATE_ORDER = '创建订单信息失败';
    const ERROR_FAIL_CREATE_ORDER_ITEM = '创建订单数据失败';
    const ERROR_FAIL_PRODUCT_NO_FOUND = '商品未找到！';
    const ERROR_FAIL_INVALID_PARAM = '错误的参数！';

    private static $_last_err = '';
    private static $instance = null;
    private static $_total_cash = 0;
    private static $_paid_cash = 0;
    private static $_total_freight_fee = 0;
    private static $_share_item_list = array(); // 用于分享返现商品列表
    private static $_combine_order_number = "";//组合订单号
    private static $_final_order_number = ""; //最终订单号

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // 生成订单，保存到session
    public function generateOrderNumber()
    {
        $t = explode(' ', microtime());
        $strtime = $t[1];

        # 时间戳后四位+micortime+时间戳前6位随随机
        $o = substr($strtime, 6) . substr($t[0], 2, 6) . mt_rand(substr($strtime, 0, 6), 999999);

        return $o;
    }

    /**
     * 结算
     */
    public function settle()
    {
        $user_id = UserStatusApp::getUid();
        $list_customer_data = $this->request->getPost('list');
        $addr = $this->request->getPost('address_id');
        $use_point = $this->request->getPost('use_point');
        $use_wallet = $this->request->getPost('use_wallet');

        if (!$list_customer_data) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单里无商品，无法生成订单！');
        }

        $flag_order_number = $this->session->get('_order_user_' . $user_id);
        if (!$flag_order_number) {
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, '订单已经失效，请不要重复提交订单！');
        }

        // 生成组合订单
        $this->genCombineNumber($list_customer_data);
        // 1. 不存在组合订单，则生成单个订单
        if (!self::$_combine_order_number) {
            $customer_id = $list_customer_data[0]['customer_id'];
            $remark = $list_customer_data[0]['remark'];
            $freight = $list_customer_data[0]['freight'];
            $coupon_serial = $list_customer_data[0]['coupon'];
            $pick_store_id = $list_customer_data[0]['pick_store_id']; // 自取id
            $all_item = json_decode(base64_decode($list_customer_data[0]['all_item']), true);

            if ($pick_store_id) {
                // 自取订单
                if (!$this->genPickOrder($customer_id, $pick_store_id, $all_item, $coupon_serial, $remark, $use_point, $use_wallet)) {
                    return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, self::$_last_err);
                }
            }

            // 单个订单
            if (!$this->genSingleOrder($customer_id, $all_item, $addr, $coupon_serial, $remark, $use_point, $use_wallet)) {
                return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, self::$_last_err);
            }

            return self::$_final_order_number;
        }

        # 多订单模式
        $this->db->begin();
        try {
            // 2. 根据每个商家生成对应订单
            foreach ($list_customer_data as $_data) {
                $customer_id = $_data['customer_id'];
                $remark = $_data['remark'];
                $freight = $_data['freight'];
                $coupon_serial = $_data['coupon'];
                $pick_store_id = $_data['pick_store_id']; // 自取id
                $all_item = json_decode(base64_decode($_data['all_item']), true);
                if (!($customer_id && $all_item && $freight)) {
                    self::$_last_err = self::ERROR_FAIL_INVALID_PARAM;
                    throw new Exception(self::$_last_err);
                }

                if ($pick_store_id) {
                    // 自取订单
                    $res = $this->genPickOrder($customer_id, $pick_store_id, $all_item, $coupon_serial, $remark);
                } else {
                    // 网络订单
                    $res = $this->genCustomerOrder($customer_id, $all_item, $addr, $coupon_serial, $remark);
                }

                if (!$res) {
                    throw new Exception(self::$_last_err);
                }
            }

            // 最后生成组合订单
            if (!$this->genCombineOrder($addr, $use_point)) {
                throw new Exception(self::$_last_err);
            };

            $this->db->commit();

            # 最终订单号
            self::$_final_order_number = self::$_combine_order_number;
            // 删除订单号session信息 避免重复提交
            $this->session->remove('_order_user_' . $user_id);
            // 清除信息避免重复
            Cookie::del('_cart_settle_page');
            unset($_SERVER['HTTP_REFERER']);
        } catch (\Exception $e) {
            $messages = $e->getMessage();
            $this->db->rollback();
            $this->di->get("errorLogger")->error($messages);
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $messages);
        }

        return Ajax::init()->outRight('', self::$_final_order_number);
    }

    // 清空购物车
    public function clearCart()
    {
        $user_id = UserStatusApp::getUid();
        $_ids = array_keys(self::$_share_item_list);
        $list = O2oShopCart::find('item_id in (' . implode(',', $_ids) . ' ) and user_id=' . $user_id);
        if (!$list) {
            return;
        }
        foreach ($list as $item) {
            $item->delete();
        }
    }

    // 生成组合订单
    public function genCombineOrder($addr, $user_point)
    {
        $user_id = UserStatusApp::getUid();
        $shopOrderCombine = new O2oShopOrderCombine();
        $shopOrderCombine->combine_order_number = self::$_combine_order_number;
        $shopOrderCombine->user_id = $user_id;
        $shopOrderCombine->createed = time();
        $shopOrderCombine->total_cash = self::$_total_cash;
        $shopOrderCombine->paid_cash = self::$_paid_cash;
        $shopOrderCombine->address_id = $addr;
        $shopOrderCombine->is_paid = 0;
        $shopOrderCombine->status = TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY;
        $shopOrderCombine->use_point = $user_point;
        $shopOrderCombine->logistics_fee = self::$_total_freight_fee;

        if (!$shopOrderCombine->create()) {
            $messages = '';
            foreach ($shopOrderCombine->getMessages() as $message) {
                $messages[] = $message;
            }
            self::$_last_err = self::ERROR_FAIL_CREATE_ORDER;
            return false;
        }

        // 清空购物车
        $this->clearCart();

        //更新用户积分数
        if (!$this->updateUserPoints($user_id, $user_point)) {
            return false;
        };
        return true;
    }

    // 生成组合订单号
    public function genCombineNumber($list_customer_data)
    {
        // 多商家，生成多订单
        $multi_customer_amount = count($list_customer_data);
        // 单个商家多种邮寄方式，生成多订单
        $single_customer_amount = @count($list_customer_data[0]['freight']);

        if ($multi_customer_amount > 1 || $single_customer_amount > 1) {
            self::$_combine_order_number = 'P' . self::generateOrderNumber();
            $order_count = O2oShopOrderCombine::count("combine_order_number='" . self::$_combine_order_number . "'");
            if ($order_count >= 1) // 订单号不能重复
            {
                self::$_combine_order_number = 'P' . self::generateOrderNumber();
            }
        }
    }

    // 门店自取订单
    public function genPickOrder($customer_id, $store_id, $all_item, $coupon_serial, $remark)
    {
        $store = Shop::findFirst('customer_id=' . $customer_id)->toArray();

        $user_id = UserStatusApp::getUid();
        $order_number = self::generateOrderNumber();

        # todo 会员卡功能
        #$discount = VipcardManager::init()->getCardDiscount($user_id, $customer_id);
        $discount = 1;
        // 按邮寄方式生成单独订单
        foreach ($all_item as $_freight_type => $_data) {
            $temp_paid_cash = 0;//订单支付金额(当前物流订单)
            $temp_total_cash = 0;//订单总金额(当前物流订单)

            // 1. 生成订单商品列表
            foreach ($_data['goods'] as $goods) {
                if (!$this->createOrderItem($customer_id, $order_number, $goods, $discount, $temp_total_cash, $temp_paid_cash)) {
                    return false;
                }
            }

            // 邮费
            $_freight_fee = 0;

            // 获取订单奖励积分
            $orders_proportion = $store['orders_proportion'];
            $award_points = 0;
            if ($orders_proportion > 0) {
                $award_points = floor($temp_paid_cash / $orders_proportion);
            }

            // 2 生成订单
            $orderData = array(
                'order_number' => $order_number,
                'customer_id' => $customer_id,
                'store_id' => $store_id,
                'user_id' => $user_id,
                'created' => time(),
                'remark' => $remark,
                'is_paid' => 0,
                'total_cash' => $temp_total_cash,
                'paid_cash' => $temp_paid_cash, // 最终支付的价格
                'order_award_points' => $award_points,
                'use_coupon' => '',
                'use_point' => 0,
                'combine_order_number' => self::$_combine_order_number,
                'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY
            );

            $order = new O2oShopOrderPick();
            if (!$order->create($orderData)) {
                $messages = '';
                foreach ($order->getMessages() as $message) {
                    $messages[] = $message;
                }
                self::$_last_err = self::ERROR_FAIL_CREATE_ORDER;
                return false;
            }

            // 总计
            self::$_total_cash += $temp_total_cash; // 总额
            self::$_paid_cash += $temp_paid_cash; // 总支付
            self::$_total_freight_fee += $_freight_fee;// 总邮费

            // 清空购物车
            $this->clearCart();
            // 分享返积分记录
            O2oShareManager::init()->addOrderLog(self::$_share_item_list, $order_number, "app");
        }

        return true;
    }

    // 按商家生成订单
    private function genCustomerOrder($customer_id, $all_item, $addr, $coupon_serial, $remark)
    {
        $store = Shop::findFirst('customer_id=' . $customer_id)->toArray();

        $user_id = UserStatusApp::getUid();
        # todo 会员卡功能
        #$discount = VipcardManager::init()->getCardDiscount($user_id, $customer_id);
        $discount = 1;
        // 按邮寄方式生成单独订单
        foreach ($all_item as $_freight_type => $_data) {
            $order_number = self::generateOrderNumber();
            $order_count = O2oShopOrderItem::count('order_number="' . $order_number . '"');
            if ($order_count >= 1) {
                //订单号不能重复
                $order_number = self::generateOrderNumber();
            }

            $temp_paid_cash = 0;//订单支付金额(当前物流订单)
            $temp_total_cash = 0;//订单总金额(当前物流订单)

            // 1. 生成订单商品列表
            foreach ($_data['goods'] as $goods) {
                if (!$this->createOrderItem($customer_id, $order_number, $goods, $discount, $temp_total_cash, $temp_paid_cash)) {
                    return false;
                }
            }

            // 邮费
            $_freight_fee = $store['free_postage'] > 0 && $store['free_postage'] > $temp_paid_cash ? 0 : floatval($_data['freight_tpl'][$_freight_type]['total_freight_money']);

            // 获取订单奖励积分
            $orders_proportion = $store['orders_proportion'];
            $award_points = 0;
            if ($orders_proportion > 0) {
                $award_points = floor($temp_paid_cash / $orders_proportion);
            }

            // 2 生成订单
            $orderData = array(
                'order_number' => $order_number,
                'customer_id' => $customer_id,
                'user_id' => $user_id,
                'created' => time(),
                'remark' => $remark,
                'address_id' => $addr,
                'is_paid' => 0,
                'total_cash' => $temp_total_cash,
                'paid_cash' => $temp_paid_cash, // 最终支付的价格
                'logistics_fee' => $_freight_fee,
                'freight_type' => $_freight_type,
                'order_award_points' => $award_points,
                'use_coupon' => '',
                'use_point' => 0,
                'combine_order_number' => self::$_combine_order_number,
                'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY
            );

            $order = new O2oShopOrder();
            if (!$order->create($orderData)) {
                $messages = '';
                foreach ($order->getMessages() as $message) {
                    $messages[] = $message;
                }
                self::$_last_err = self::ERROR_FAIL_CREATE_ORDER;
                return false;
            }

            // 总计
            self::$_total_cash += $temp_total_cash; // 总额
            self::$_paid_cash += $temp_paid_cash; // 总支付
            self::$_total_freight_fee += $_freight_fee;// 总邮费

            // 清空购物车
            $this->clearCart();
            // 分享返积分记录
            O2oShareManager::init()->addOrderLog(self::$_share_item_list, $order_number, "app");
        }

        return true;
    }

    // 创建订单商品数据
    private function createOrderItem($customer_id, $order_number, $goods, $discount, &$total_cash, &$paid_cash)
    {
        $product = ProductManager::instance()->getProduct($goods['item_id'], $customer_id);
        if (!$product) {
            self::$_last_err = self::ERROR_FAIL_PRODUCT_NO_FOUND;
            return false;
        }
        // 返现信息列表
        self::$_share_item_list[$goods['item_id']] = array('num' => $goods['num'], 'sell_price' => $product['sell_price']);
        // 价格
        $sell_price = $product['sell_price'];
        // 原价
        $original_price = $product['original_price'];
        $item_new['item_sell_price'] = $product['sell_price'];
        $item_new['total_preferential'] = ($product['original_price'] - $sell_price) * $goods['num'];

        $spec = isset($goods['spec']) ? ProductManager::instance()->getProductSpecData($goods['item_id'], $goods['spec']) : [];
        // 商品规格
        if ($goods['spec'] && $spec) {
            $original_price = $spec['original_price'];
            if (($discount * $spec['original_price']) < $spec['sell_price']) //折扣后的价格比优惠价格更低
            {
                $item_new['item_sell_price'] = $discount * $spec['original_price'];
                $item_new['total_preferential'] = ($spec['original_price'] - $sell_price) * $goods['num'];
            }
        } else if (($discount * $product['original_price']) < $product['sell_price']) //折扣后的价格比优惠价格更低
        {
            $item_new['item_sell_price'] = $discount * $product['original_price'];
            $item_new['total_preferential'] = ($product['original_price'] - $sell_price) * $goods['num'];
        }

        // 数据信息
        $item_new['user_id'] = UserStatusApp::getUid();
        $item_new['customer_id'] = $customer_id;
        $item_new['order_number'] = $order_number;
        $item_new['combine_order_number'] = self::$_combine_order_number;
        $item_new['item_id'] = $goods['item_id'];
        $item_new['item_name'] = $goods['name'];
        $item_new['item_price'] = $sell_price;
        $item_new['item_original_price'] = $original_price;
        $item_new['item_spec'] = $goods['spec'];
        $item_new['item_spec_data'] = $goods['spec_data'];
        $item_new['quantity'] = $goods['num'];
        $item_new['thumb'] = $goods['thumb'];
        $item_new['total_cash'] = $goods['num'] * $sell_price;
        $item_new['created'] = $goods['created'];
        $orderItem = new O2oShopOrderItem();
        if (!$orderItem->create($item_new)) {
            $messages = [];
            foreach ($orderItem->getMessages() as $message) {
                $messages[] = $message;
            }
            self::$_last_err = self::ERROR_FAIL_CREATE_ORDER_ITEM;
            return false;
        };
        // 累计
        $total_cash += $item_new['quantity'] * $product['original_price'];
        $paid_cash += $item_new['quantity'] * $sell_price;
        return true;
    }

    // 生成单个订单
    public function genSingleOrder($customer_id, $all_item, $addr, $coupon_serial, $remark, $use_point, $use_wallet)
    {
        $store = Shop::findFirst('customer_id=' . $customer_id)->toArray();

        $user_id = UserStatusApp::getUid();
//        $discount = VipcardManager::init()->getCardDiscount($user_id, $customer_id);# todo 会员卡打折
        $discount = 1;

        // 开始生成订单
        $this->db->begin();
        try {
            // 按邮寄方式生成单独订单
            foreach ($all_item as $_freight_type => $_data) {
                $order_number = self::generateOrderNumber();
                $order_count = O2oShopOrder::count('order_number="' . $order_number . '"');
                if ($order_count >= 1) {
                    //订单号不能重复
                    $order_number = self::generateOrderNumber();
                }
                $temp_paid_cash = 0;//订单支付金额(当前物流订单)
                $temp_total_cash = 0;//订单总金额(当前物流订单)
                $temp_vip_preferential_amount = 0;//订单vip优惠总金额(当前物流订单)
                // 1. 生成订单商品列表
                foreach ($_data['goods'] as $k1 => $goods) {
                    if (!$this->createOrderItem($customer_id, $order_number, $goods, $discount, $temp_total_cash, $temp_paid_cash)) {
                        throw new Exception(self::$_last_err);
                    }
                }

                // 邮费
                $_freight_fee = $store['free_postage'] > 0 && $store['free_postage'] > $temp_paid_cash ? 0 : floatval($_data['freight_tpl'][$_freight_type]['total_freight_money']);

                // 获取订单奖励积分
                $orders_proportion = $store['orders_proportion'];
                $award_points = 0;
                if ($orders_proportion > 0) {
                    $award_points = floor($temp_paid_cash / $orders_proportion);
                }

                // 2 生成订单
                $orderData = array(
                    'order_number' => $order_number,
                    'customer_id' => $customer_id,
                    'user_id' => $user_id,
                    'created' => time(),
                    'remark' => $remark,
                    'address_id' => $addr,
                    'is_paid' => 0,
                    'total_cash' => $temp_total_cash,
                    'paid_cash' => $temp_paid_cash, // 最终支付的价格
                    'logistics_fee' => $_freight_fee,
                    'freight_type' => $_freight_type,
                    'discount_cash' => $temp_vip_preferential_amount, //优惠多少
                    'order_award_points' => $award_points,
                    'use_coupon' => '',
                    'use_point' => 0,
                    'combine_order_number' => self::$_combine_order_number,
                    'status' => TransactionManager::ORDER_STATUS_WAIT_BUYER_PAY
                );

                $order = new O2oShopOrder();
                if (!$order->create($orderData)) {
                    $messages = '';
                    foreach ($order->getMessages() as $message) {
                        $messages[] = $message;
                    }
                    self::$_last_err = self::ERROR_FAIL_CREATE_ORDER;
                    throw new Exception(self::$_last_err);
                }

                // 总计
                self::$_total_cash += $temp_total_cash; // 总额
                self::$_paid_cash += $temp_paid_cash; // 总支付
                self::$_total_freight_fee += $_freight_fee;// 总邮费
                // 最终订单号
                self::$_final_order_number = $order_number;
                // 分享返积分记录
                O2oShareManager::init()->addOrderLog(self::$_share_item_list, $order_number);
            }

            // 清空购物车
            $this->clearCart();

            $this->db->commit();

            // 4 删除订单号session信息 避免重复提交
            $this->session->remove('_order_user_' . $user_id);

            // 清除信息避免重复
            Cookie::del('_cart_settle_page');
            unset($_SERVER['HTTP_REFERER']);
        } catch (\Exception $e) {
            $messages = $e->getMessage();
            $this->db->rollback();
            $this->di->get("errorLogger")->error($messages);
            return Ajax::init()->outError(Ajax::CUSTOM_ERROR_MSG, $messages);
        }
        return true;
    }


    /*积分支付后更新用户的可用积分*/
    public function  updateUserPoints($user_id, $points)
    {
        if ($points > 0) {
        }
        return true;
    }

    /*获取订单里的商品信息*/
    public function getOrderItem($order)
    {
        $list = $this->db->query("Select soi.*,ifnull(sor.type,'0') as order_type,
              sor.is_seller_accept,sor.is_end
              from shop_order_item as soi left join  shop_order_return as sor ON soi.id=sor.order_item_id WHERE soi.order_number=" . $order['order_number'])
            ->fetchAll();
        foreach ($list as $k => $val) {
            if (($order['status'] == TransactionManager::ORDER_STATUS_TRADE_SUCCESS) && (((time() - $order['received_time']) / (60 * 60 * 24)) < 7) && ($order['use_point'] <= 0) && ($val['order_type'] == 0)) {
                $list[$k]['return_show'] = true;
            } else {
                $list[$k]['return_show'] = false;
            }
        }
        return $list;
    }
}
