<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/10
 * Time: 13:52
 */

namespace Library\O2O\Financial;


use Models\O2O\O2oCustomerBalance;
use Models\O2O\O2oCustomerBalanceLog;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;

class CustomerBalanceManager extends Component
{
    /**
     * @var CustomerBalanceManager
     */
    private static $instance = null;

    /**
     * @var int
     */
    private $customer_id = 0;

    /**
     * @var int
     */
    private $balance_status = 0;
    public static $_err_msg = '';

    /**
     * @var \Models\O2o\O2oCustomerBalance
     */
    private $balance = null;

    // 流水类型
    const ACTION_TYPE_IN = '1'; // 收入
    const ACTION_TYPE_OUT = '2';// 支出
    // 流水代码
    const BALANCE_ACTION_DIRECT_PAY = 1001;
    const BALANCE_ACTION_PRE_PAY = 1002; // 预支付
    const BALANCE_ACTION_PRE_PAY_DONE = 1003; // 预支发确认
    const BALANCE_ACTION_PRE_PAY_RETURN = 1004; // 预支付取消
    const BALANCE_ACTION_FROZE = 1005;// 冻结
    const BALANCE_ACTION_UNFROZE = 1006;// 解冻
    const BALANCE_ACTION_RECHARGE = 1007;// 充值
    const BALANCE_ACTION_WITHDRAW = 1008;// 取现
    const BALANCE_ACTION_TRANSFER = 1009;// 转账
    const BALANCE_ACTION_BACK_CASH = 1010; //返现

    // 交易成功
    const BALANCE_STATUS_SUCCESS_DIRECT_PAY = 2001;
    const BALANCE_STATUS_SUCCESS_PRE_PAY = 2002;
    const BALANCE_STATUS_SUCCESS_PRE_PAY_DONE = 2003;
    const BALANCE_STATUS_SUCCESS_PRE_PAY_RETURN = 2004;
    const BALANCE_STATUS_SUCCESS_FROZE = 2005;
    const BALANCE_STATUS_SUCCESS_UNFROZE = 2006;
    const BALANCE_STATUS_SUCCESS_RECHARGE = 2007;
    const BALANCE_STATUS_SUCCESS_WITHDRAW = 2008;
    const BALANCE_STATUS_SUCCESS_TRANSFER = 2009;
    const BALANCE_STATUS_SUCCESS_BACK_CASH = 2010;

    // 交易失败
    const BALANCE_STATUS_FAIL_DIRECT_PAY = 3001;
    const BALANCE_STATUS_FAIL_PRE_PAY = 3002;
    const BALANCE_STATUS_FAIL_PRE_PAY_DONE = 3003;
    const BALANCE_STATUS_FAIL_PRE_PAY_RETURN = 3004;
    const BALANCE_STATUS_FAIL_FROZE = 3005;
    const BALANCE_STATUS_FAIL_UNFROZE = 3006;
    const BALANCE_STATUS_FAIL_RECHARGE = 3007;
    const BALANCE_STATUS_FAIL_WITHDRAW = 3008;
    const BALANCE_STATUS_FAIL_TRANSFER = 3009;
    const BALANCE_STATUS_FAIL_BACK_CASH = 3010;

    // 操作失败
    const BALANCE_STATUS_PARAM_ILLEGAL = 4001;
    const BALANCE_STATUS_INSTANCE_CASH_ACCOUNT_FAIL = 4002;
    const BALANCE_STATUS_AVAILABLE_CASH_NOT_ENOUGH = 4003;
    const BALANCE_STATUS_LOG_FAIL = 4004;

    const BALANCE_BEHAVIOR_BUY_INTEGRATE = 5001;
    const BALANCE_BEHAVIOR_BUY_INTEGRATE_SUCCESS = 5002;
    const BALANCE_BEHAVIOR_BUY_INTEGRATE_FAIL = 5003;
    const BALANCE_BEHAVIOR_BUY_SECKILL = 5004;
    const BALANCE_BEHAVIOR_BUY_SECKILL_SUCCESS = 5005;
    const BALANCE_BEHAVIOR_BUY_SECKILL_FAIL = 5006;
    const BALANCE_BEHAVIOR_PAY_COMMISSION = 5007;

    // 流水信息
    private static $_behavior_name = array(
        self::BALANCE_ACTION_DIRECT_PAY => "直接支付",
        self::BALANCE_ACTION_PRE_PAY => "预支付",
        self::BALANCE_ACTION_PRE_PAY_DONE => "支付已确认",
        self::BALANCE_ACTION_PRE_PAY_RETURN => "支付已取消",
        self::BALANCE_ACTION_FROZE => "冻结",
        self::BALANCE_ACTION_UNFROZE => "解冻",
        self::BALANCE_ACTION_RECHARGE => "充值",
        self::BALANCE_ACTION_WITHDRAW => "取现",
        self::BALANCE_ACTION_TRANSFER => "转账",
        self::BALANCE_ACTION_BACK_CASH => "返现",
    );

    private static $_status_name = array(
        // 交易失败
        self::BALANCE_STATUS_FAIL_DIRECT_PAY => "直接支付失败",
        self::BALANCE_STATUS_FAIL_PRE_PAY => "预支付失败",
        self::BALANCE_STATUS_FAIL_PRE_PAY_DONE => "支付已确认失败",
        self::BALANCE_STATUS_FAIL_PRE_PAY_RETURN => "支付已取消失败",
        self::BALANCE_STATUS_FAIL_FROZE => "冻结失败",
        self::BALANCE_STATUS_FAIL_UNFROZE => "解冻失败",
        self::BALANCE_STATUS_FAIL_RECHARGE => "充值失败",
        self::BALANCE_STATUS_FAIL_WITHDRAW => "取现失败",
        self::BALANCE_STATUS_FAIL_TRANSFER => "转账失败",
        self::BALANCE_STATUS_FAIL_BACK_CASH => "返现失败",

        // 操作失败
        self::BALANCE_STATUS_PARAM_ILLEGAL => "非法参数",
        self::BALANCE_STATUS_INSTANCE_CASH_ACCOUNT_FAIL => "系统初始化商户账户失败",
        self::BALANCE_STATUS_AVAILABLE_CASH_NOT_ENOUGH => "商家资金账户余额不足",
        self::BALANCE_STATUS_LOG_FAIL => "记录资金变动日志失败",
    );

    private function __construct()
    {
    }

    // 获取失败信息
    public static function getErrMsg($err_code = null)
    {
        $err = $err_code ? self::$instance->balance_status : $err_code;
        return isset(self::$_status_name[$err]) ? self::$_status_name[$err] : "未知的操作！";
    }

    public static function getErrCode()
    {
        return self::$instance->balance_status;
    }

    /**
     * @param int $customer_id
     * @return CustomerBalanceManager
     */
    public static function getInstance($customer_id)
    {
        if (!self::$instance instanceof CustomerBalanceManager) {
            self::$instance = new self();
        }
        self::$instance->customer_id = $customer_id;
        self::$instance->initBalance();
        return self::$instance;
    }

    // 初始
    public function initBalance()
    {
        $balance = O2oCustomerBalance::findFirst("customer_id=" . $this->customer_id);
        if (!$balance instanceof O2oCustomerBalance) {
            $balance = new O2oCustomerBalance();
            $balance->customer_id = $this->customer_id;
            $balance->balance = 0;
            $balance->frozen = 0;
            $balance->available = 0;
            if (!$balance->save()) {
                $err = "";
                foreach ($balance->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get('errorLogger')->error("customer balance init failed: " . $err);
                throw new Exception(self::$_status_name[self::BALANCE_STATUS_INSTANCE_CASH_ACCOUNT_FAIL]);
            }
        }
        self::$instance->balance = $balance;
        return self::$instance;
    }

    /**
     * @param null $filter
     * @param int $start
     * @param int $limit
     * @param string $sort
     * @return array
     */
    public function getBalanceLog($filter = null, $start = 0, $limit = 10, $sort = "created desc")
    {
        $logs = O2oCustomerBalanceLog::find(array($filter, 'order' => $sort, 'limit' => $limit, 'skip' => $start));
        return $logs ? $logs->toArray() : array();
    }

    /**
     * @param int $cash
     * @return bool
     */
    public function frozeCash($cash = 0)
    {

        return true;
    }

    /**
     * @param $logId
     * @return bool
     */
    public function unfrozeCash($logId)
    {

        return true;
    }

    /**
     * @return array
     */
    public function getBalanceInfo()
    {
        return $this->balance->toArray();
    }

    //直接支付
    public function directPay($cash = 0)
    {
        if ($cash > 0) {
            if ($this->balance->available < $cash) {
                $this->balance_status = self::BALANCE_STATUS_AVAILABLE_CASH_NOT_ENOUGH;
                return false;
            }
            $this->balance->balance -= $cash;
            $this->balance->available -= $cash;
            if (!$this->balance->save()) {
                $err = "";
                foreach ($this->balance->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get("errorLogger")->error("customer balance direct pay failed: " . $err);
                $this->balance_status = self::BALANCE_STATUS_FAIL_DIRECT_PAY;
                return false;
            }
            $this->balance_status = self::BALANCE_STATUS_SUCCESS_DIRECT_PAY;
            return true;
        }
        return false;
    }

    //向账户内充值
    public function recharge($cash = 0)
    {
        if ($cash > 0) {
            $this->balance->balance += $cash;
            $this->balance->available += $cash;
            if (!$this->balance->save()) {
                $err = "";
                foreach ($this->balance->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get("errorLogger")->error("customer balance direct pay failed: " . $err);
                $this->balance_status = self::BALANCE_STATUS_FAIL_RECHARGE;
                return false;
            }
            $this->balance_status = self::BALANCE_STATUS_SUCCESS_RECHARGE;
            return true;
        }
        return false;
    }

    /**
     * @param $from
     * @param $cash
     * @return bool
     */
    public function transferInto($from, $cash)
    {

        return true;
    }

    /**
     * @param $to
     * @param $cash
     * @return bool
     */
    public function transferFrom($to, $cash)
    {

        return true;
    }

    /**
     * @param int $behavior
     * @param int $cash
     * @param int $status
     * @return bool
     */
    public function prePay($behavior, $cash, $status)
    {

        return true;
    }

    /**
     * @param $cash
     * @return bool
     */
    public function prePayForIntegrate($cash)
    {

        return true;
    }

    /**
     * @param $cash
     * @return bool
     */
    public function prePayForSeckill($cash)
    {

        return true;
    }

    // 返现处理
    public function settleBackCash($customer_id, $user_id, $total_cash)
    {
        if ($this->balance->available - $total_cash < 0) {
            $this->balance_status = self::BALANCE_STATUS_AVAILABLE_CASH_NOT_ENOUGH;
            self::$_err_msg = self::getErrMsg(self::BALANCE_STATUS_AVAILABLE_CASH_NOT_ENOUGH);
            return false;
        }
        // 更新金额
        $data = array(
            'available' => $this->balance->available - $total_cash,
            'balance' => $this->balance->balance - $total_cash,
            'last_output' => $total_cash
        );
        if (!$this->balance->update($data)) {
            $err = "";
            foreach ($this->balance->getMessages() as $m) {
                $err .= (string)$m;
            }
            $this->di->get("errorLogger")->error("customer balance direct pay failed: " . $err);
            // 更新失败
            $this->balance_status = self::BALANCE_STATUS_FAIL_BACK_CASH;
            self::$_err_msg = self::getErrMsg(self::BALANCE_STATUS_FAIL_BACK_CASH);
            return false;
        }

        // 记录日志
        if (!$this->writeLog($customer_id, self::BALANCE_ACTION_DIRECT_PAY, self::ACTION_TYPE_OUT, $total_cash, $user_id)) {
            $this->balance_status = self::BALANCE_STATUS_LOG_FAIL;
            self::$_err_msg = self::getErrMsg(self::BALANCE_STATUS_LOG_FAIL);
            return false;
        }

        return true;
    }

    // 记录日志
    public function writeLog($customer_id, $behavior, $in_out, $value, $param = '')
    {
        // 现金日志
        $log = new O2oCustomerBalanceLog();
        $data = array(
            'customer_id' => $customer_id,
            'behavior' => $behavior,
            'in_out' => $in_out,
            'action' => @self::$_behavior_name[$behavior],
            'value' => $value,
            'param' => $param,
            'created' => time()
        );

        if (!$log->create($data)) {
            $err = "";
            foreach ($this->balance->getMessages() as $m) {
                $err .= (string)$m;
            }
            $this->di->get("errorLogger")->error("customer balance direct pay failed: " . $err);
        }

        return true;
    }

} 