<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/10
 * Time: 13:52
 */

namespace Library\O2O\Financial;


use Models\O2O\O2oUserWallet;
use Models\O2O\O2oUserWalletLog;
use Models\Product\Distribution\ProductShareOrderLog;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;

class O2oUserWalletManager extends Component
{
    /**
     * @var O2oUserWalletManager
     */
    private static $instance = null;

    /**
     * @var int
     */
    private $uid = 0;

    /**
     * @var int
     */
    private $wallet_status = 0;
    public static $_err_msg = '';
    /**
     * @var \Models\O2o\O2oUserWallet
     */
    private $wallet = null;
// 流水类型
    const ACTION_TYPE_IN = '1'; // 收入
    const ACTION_TYPE_OUT = '2';// 支出
    // 流水代码
    const WALLET_ACTION_DIRECT_PAY = 1001;
    const WALLET_ACTION_PRE_PAY = 1002; // 预支付
    const WALLET_ACTION_PRE_PAY_DONE = 1003; // 预支发确认
    const WALLET_ACTION_PRE_PAY_RETURN = 1004; // 预支付取消
    const WALLET_ACTION_FROZE = 1005;// 冻结
    const WALLET_ACTION_UNFROZE = 1006;// 解冻
    const WALLET_ACTION_RECHARGE = 1007;// 充值
    const WALLET_ACTION_WITHDRAW = 1008;// 取现
    const WALLET_ACTION_TRANSFER = 1009;// 转账
    const WALLET_ACTION_BACK_CASH = 1010; //返现

    // 交易成功
    const WALLET_STATUS_SUCCESS_DIRECT_PAY = 2001;
    const WALLET_STATUS_SUCCESS_PRE_PAY = 2002;
    const WALLET_STATUS_SUCCESS_PRE_PAY_DONE = 2003;
    const WALLET_STATUS_SUCCESS_PRE_PAY_RETURN = 2004;
    const WALLET_STATUS_SUCCESS_FROZE = 2005;
    const WALLET_STATUS_SUCCESS_UNFROZE = 2006;
    const WALLET_STATUS_SUCCESS_RECHARGE = 2007;
    const WALLET_STATUS_SUCCESS_WITHDRAW = 2008;
    const WALLET_STATUS_SUCCESS_TRANSFER = 2009;
    const WALLET_STATUS_SUCCESS_BACK_CASH = 2010;

    // 交易失败
    const WALLET_STATUS_FAIL_DIRECT_PAY = 3001;
    const WALLET_STATUS_FAIL_PRE_PAY = 3002;
    const WALLET_STATUS_FAIL_PRE_PAY_DONE = 3003;
    const WALLET_STATUS_FAIL_PRE_PAY_RETURN = 3004;
    const WALLET_STATUS_FAIL_FROZE = 3005;
    const WALLET_STATUS_FAIL_UNFROZE = 3006;
    const WALLET_STATUS_FAIL_RECHARGE = 3007;
    const WALLET_STATUS_FAIL_WITHDRAW = 3008;
    const WALLET_STATUS_FAIL_TRANSFER = 3009;
    const WALLET_STATUS_FAIL_BACK_CASH = 3010;

    // 操作失败
    const WALLET_STATUS_PARAM_ILLEGAL = 4001;
    const WALLET_STATUS_INSTANCE_CASH_ACCOUNT_FAIL = 4002;
    const WALLET_STATUS_AVAILABLE_CASH_NOT_ENOUGH = 4003;
    const WALLET_STATUS_SAVE_CASH_FAIL = 4004;
    const WALLET_STATUS_LOG_FAIL = 4005;
    const WALLET_STATUS_UPDATE_BACK_CASH_ORDER_FAIL = 4006;

    const WALLET_BEHAVIOR_BUY_INTEGRATE = 5001;
    const WALLET_BEHAVIOR_BUY_INTEGRATE_SUCCESS = 5002;
    const WALLET_BEHAVIOR_BUY_INTEGRATE_FAIL = 5003;
    const WALLET_BEHAVIOR_BUY_SECKILL = 5004;
    const WALLET_BEHAVIOR_BUY_SECKILL_SUCCESS = 5005;
    const WALLET_BEHAVIOR_BUY_SECKILL_FAIL = 5006;
    const WALLET_BEHAVIOR_PAY_COMMISSION = 5007;


    // 流水信息
    private static $_behavior_name = array(
        self::WALLET_ACTION_DIRECT_PAY => "直接支付",
        self::WALLET_ACTION_PRE_PAY => "预支付",
        self::WALLET_ACTION_PRE_PAY_DONE => "支付已确认",
        self::WALLET_ACTION_PRE_PAY_RETURN => "支付已取消",
        self::WALLET_ACTION_FROZE => "冻结",
        self::WALLET_ACTION_UNFROZE => "解冻",
        self::WALLET_ACTION_RECHARGE => "充值",
        self::WALLET_ACTION_WITHDRAW => "取现",
        self::WALLET_ACTION_TRANSFER => "转账",
        self::WALLET_ACTION_BACK_CASH => "返现",
    );

    private static $_status_name = array(
        // 交易失败
        self::WALLET_STATUS_FAIL_DIRECT_PAY => "直接支付失败",
        self::WALLET_STATUS_FAIL_PRE_PAY => "预支付失败",
        self::WALLET_STATUS_FAIL_PRE_PAY_DONE => "支付已确认失败",
        self::WALLET_STATUS_FAIL_PRE_PAY_RETURN => "支付已取消失败",
        self::WALLET_STATUS_FAIL_FROZE => "冻结失败",
        self::WALLET_STATUS_FAIL_UNFROZE => "解冻失败",
        self::WALLET_STATUS_FAIL_RECHARGE => "充值失败",
        self::WALLET_STATUS_FAIL_WITHDRAW => "取现失败",
        self::WALLET_STATUS_FAIL_TRANSFER => "转账失败",
        self::WALLET_STATUS_FAIL_BACK_CASH => "返现失败",
        self::WALLET_STATUS_FAIL_BACK_CASH => "更新返现订单失败！",

        // 操作失败
        self::WALLET_STATUS_PARAM_ILLEGAL => "非法参数",
        self::WALLET_STATUS_INSTANCE_CASH_ACCOUNT_FAIL => "初始化用户钱包失败",
        self::WALLET_STATUS_SAVE_CASH_FAIL => "钱包变更保存失败~！",
        self::WALLET_STATUS_AVAILABLE_CASH_NOT_ENOUGH => "钱包余额不足",
        self::WALLET_STATUS_LOG_FAIL => "记录钱包变动日志失败",
        self::WALLET_STATUS_UPDATE_BACK_CASH_ORDER_FAIL => "更新返现订单失败",
    );

    private function __construct()
    {
    }

    /**
     * @param int $uid
     * @return UserWalletManager
     */
    public static function getInstance($uid)
    {
        if (!self::$instance instanceof O2oUserWalletManager) {
            self::$instance = new self();
        }

        self::$instance->uid = $uid;
        if (!self::$instance->wallet instanceof O2oUserWallet || self::$instance->wallet->user_id != self::$instance->uid) {
            self::$instance->initBalance();
        }

        return self::$instance;
    }

    public static function init()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // 获取失败信息
    public function getErrMsg($err_code)
    {
        return isset(self::$_status_name[$err_code]) ? self::$_status_name[$err_code] : "未知的操作！";
    }

    public function initBalance()
    {
        $wallet = O2oUserWallet::findFirst("user_id=" . $this->uid);
        if (!$wallet instanceof O2oUserWallet) {
            $wallet = new O2oUserWallet();
            $wallet->user_id = $this->uid;
            $wallet->wallet = 0;
            $wallet->frozen = 0;
            $wallet->available = 0;
            if (!$wallet->save()) {
                $err = "";
                foreach ($wallet->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get('errorLogger')->error("customer wallet init failed: " . $err);
                throw new Exception();
            }
        }

        self::$instance->wallet = $wallet;
        return self::$instance;
    }

    /**
     * @param null $filter
     * @param int $start
     * @param int $limit
     * @param string $sort
     * @return array
     */
    public function getBalanceLog($filter = null, $start = 0, $limit = 10, $sort = "created desc")
    {

        return array();
    }

    /**
     * @param int $cash
     * @return bool
     */
    public function frozeCash($cash = 0)
    {

        return true;
    }

    /**
     * @param $logId
     * @return bool
     */
    public function unfrozeCash($logId)
    {

        return true;
    }

    /**
     * @return array
     */
    public function getBalanceInfo()
    {
        return $this->wallet->toArray();
    }

    //直接支付
    public function directPay($cash = 0)
    {
        if ($cash > 0) {
            if ($this->wallet->available < $cash) {
                $this->wallet_status = self::WALLET_STATUS_AVAILABLE_CASH_NOT_ENOUGH;
                return false;
            }
            $this->wallet->wallet -= $cash;
            $this->wallet->available -= $cash;
            if (!$this->wallet->save()) {
                $err = "";
                foreach ($this->wallet->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get("errorLogger")->error("customer wallet direct pay failed: " . $err);
                $this->wallet_status = self::WALLET_STATUS_DIRECT_PAY_FAILED;
                return false;
            }
            $this->wallet_status = self::WALLET_STATUS_DIRECT_PAY_SUCCESS;
            return true;
        }
        return false;
    }

    //向账户内充值
    public function recharge($cash = 0)
    {
        if ($cash > 0) {
            $this->wallet->wallet += $cash;
            $this->wallet->available += $cash;
            if (!$this->wallet->save()) {
                $err = "";
                foreach ($this->wallet->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get("errorLogger")->error("customer wallet direct pay failed: " . $err);
                $this->wallet_status = self::WALLET_STATUS_RECHARGE_FAILED;
                return false;
            }
            $this->wallet_status = self::WALLET_STATUS_RECHARGE_SUCCESS;
            return true;
        }
        return false;
    }

    /**
     * @param $from
     * @param $cash
     * @return bool
     */
    public function transferInto($from, $cash)
    {

        return true;
    }

    /**
     * @param $to
     * @param $cash
     * @return bool
     */
    public function transferFrom($to, $cash)
    {

        return true;
    }

    /**
     * @param int $behavior
     * @param int $cash
     * @param int $status
     * @return bool
     */
    public function prePay($behavior, $cash, $status = self::WALLET_STATUS_PRE_PAY_SUCCESS)
    {

        return true;
    }

    /**
     * @param $cash
     * @return bool
     */
    public function prePayForIntegrate($cash)
    {

        return true;
    }

    /**
     * @param $cash
     * @return bool
     */
    public function prePayForSeckill($cash)
    {

        return true;
    }

    // 返现过程
    public function settleCashBack($order_number)
    {
        if (!$order_number) {
            $this->wallet_status = self::WALLET_STATUS_PARAM_ILLEGAL;
            self::$_err_msg = self::getErrMsg(self::WALLET_STATUS_PARAM_ILLEGAL);
            return false;
        }

        $order_list = ProductShareOrderLog::find(array('order_number="' . $order_number . '"'));
        if (!$order_list) {
            // ** 直接返回true，避免回滚
            return true;
        }

        // 更新返现订单状态
        foreach ($order_list as $item) {
            $item->status = 1;

            if (!$item->update()) {
                $this->wallet_status = self::WALLET_STATUS_UPDATE_BACK_CASH_ORDER_FAIL;
                self::$_err_msg = self::getErrMsg(self::WALLET_STATUS_UPDATE_BACK_CASH_ORDER_FAIL);
                return false;
            }
        }

        $order_arr = $order_list->toArray();
        $from_user = 0;
        $_backcash = []; //tmp

        // 循环计算商家对应需要返现数目：针对app情况
        foreach ($order_arr as $order) {
            $from_user = $order['from_user'];
            if (!isset($_backcash[$order['customer_id']])) {
                $_backcash[$order['customer_id']] = $order['cash'];
            } else {
                $_backcash[$order['customer_id']] += $order['cash'];
            }
        }

        // 返现过程
        foreach ($_backcash as $customer_id => $total_cash) {
            // 返现给用户
            if (!$this->upWallet($customer_id, $from_user, $total_cash, self::ACTION_TYPE_IN)) {
                $this->wallet_status = self::WALLET_STATUS_FAIL_BACK_CASH;
                self::$_err_msg = self::getErrMsg(self::WALLET_STATUS_FAIL_BACK_CASH);
                return false;
            }

            // 返现日志
            if (!$this->writeLog($from_user, self::WALLET_ACTION_BACK_CASH, self::ACTION_TYPE_IN, $total_cash, $customer_id)) {
                $this->wallet_status = self::WALLET_STATUS_LOG_FAIL;
                self::$_err_msg = self::getErrMsg(self::WALLET_STATUS_LOG_FAIL);
                return false;
            };

            // 扣除商家金额
            if (!CustomerBalanceManager::getInstance($customer_id)->settleBackCash($customer_id, $from_user, $total_cash)) {
                $this->wallet_status = CustomerBalanceManager::getErrCode();
                self::$_err_msg = CustomerBalanceManager::$_err_msg;
                return false;
            }
        }

        return true;
    }


    // 更新钱包
    public function upWallet($uid, $customer_id, $total_cash, $in_out)
    {
        $wallet = O2oUserWallet::findFirst('customer_id=' . $customer_id . ' and user_id=' . $uid);
        if ($wallet) {
            // 增加
            if ($in_out == self::ACTION_TYPE_IN) {
                $available = $wallet->available + $total_cash;
                $balance = $wallet->balance + $total_cash;
            } else {
                // 减少
                $available = $wallet->available - $total_cash;
                $balance = $wallet->balance - $total_cash;
            }
            return $wallet->update(array("available" => $available, "balance" => $balance));
        } else {
            // 增加
            if ($in_out == self::ACTION_TYPE_IN) {
                $wallet = new O2oUserWallet();
                return $wallet->create(array('user_id' => $uid, 'customer_id' => $customer_id, "available" => $total_cash, "balance" => $total_cash));
            }
        }
        return false;
    }

    // 记录日志
    public function writeLog($user_id, $behavior, $in_out, $value, $param = '')
    {
        // 现金日志
        $log = new O2oUserWalletLog();
        $data = array(
            'customer_id' => CUR_APP_ID,
            'user_id' => $user_id,
            'behavior' => $behavior,
            'in_out' => $in_out,
            'action' => @self::$_behavior_name[$behavior],
            'value' => $value,
            'param' => $param,
            'created' => time()
        );

        if (!$log->create($data)) {
            $err = "";
            foreach ($this->balance->getMessages() as $m) {
                $err .= (string)$m;
            }
            $this->di->get("errorLogger")->error("customer balance direct pay failed: " . $err);
        }

        return true;
    }
} 