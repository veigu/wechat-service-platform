<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/10
 * Time: 13:52
 */

namespace Library\O2O\Financial;


use Models\O2O\O2oCustomerBalance;
use Models\O2O\O2oCustomerBalanceLog;
use Phalcon\Mvc\User\Component;

class UserWalletManager extends Component{
    /**
     * @var UserWalletManager
     */
    private static $instance = null;

    /**
     * @var int
     */
    private $customer = 0;

    /**
     * @var int
     */
    private $balance_status = 0;

    /**
     * @var \Models\O2o\UserWallet
     */
    private $balance = null;

    const BALANCE_ACTION_DIRECT_PAY = 1000;
    const BALANCE_ACTION_PRE_PAY = 1001;
    const BALANCE_ACTION_PRE_PAY_DONE = 1002;
    const BALANCE_ACTION_PRE_PAY_RETURN = 1003;
    const BALANCE_ACTION_FROZE = 1004;
    const BALANCE_ACTION_UNFROZE = 1005;
    const BALANCE_ACTION_RECHARGE = 1006;
    const BALANCE_ACTION_WITHDWAW = 1007;
    const BALANCE_ACTION_TRANSFER = 1008;

    const BALANCE_STATUS_AVAILABLE_CASH_NOT_ENOUGH = 2000; //可用余额不够
    const BALANCE_STATUS_DIRECT_PAY_SUCCESS = 2001;
    const BALANCE_STATUS_PRE_PAY_SUCCESS = 2002;
    const BALANCE_STATUS_FROZE_SUCCESS = 2003;
    const BALANCE_STATUS_UNFROZE_SUCCESS = 2004;
    const BALANCE_STATUS_PRE_PAY_DONE_SUCCESS = 2005;
    const BALANCE_STATUS_PRE_PAY_RETURN_SUCCESS = 2006;
    const BALANCE_STATUS_DIRECT_PAY_FAILED = 2007;
    const BALANCE_STATUS_PRE_PAY_FAILED = 2008;
    const BALANCE_STATUS_FROZE_FAILED = 2008;
    const BALANCE_STATUS_UNFROZE_FAILED = 2009;
    const BALANCE_STATUS_PRE_PAY_DONE_FAILED = 2010;
    const BALANCE_STATUS_PRE_PAY_RETURN_FAILED = 2011;
    const BALANCE_STATUS_PARAM_ILLEGAL = 2012;
    const BALANCE_STATUS_RECHARGE_SUCCESS = 2013;
    const BALANCE_STATUS_RECHARGE_FAILED = 2014;
    const BALANCE_STATUS_TRANSFER_SUCCESS = 2015;
    const BALANCE_STATUS_TRANSFER_FAILED = 2016;

    const BALANCE_BEHAVIOR_BUY_INTEGRATE = 3001;
    const BALANCE_BEHAVIOR_BUY_INTEGRATE_SUCCESS = 3002;
    const BALANCE_BEHAVIOR_BUY_INTEGRATE_FAILED = 3003;
    const BALANCE_BEHAVIOR_BUY_SECKILL = 3004;
    const BALANCE_BEHAVIOR_BUY_SECKILL_SUCCESS = 3005;
    const BALANCE_BEHAVIOR_BUY_SECKILL_FAILED = 3006;
    const BALANCE_BEHAVIOR_PAY_COMMISSION = 3007;

    private function __construct() {

    }

    /**
     * @param int $customer
     * @return UserWalletManager
     */
    public static function getInstance($customer) {
        if(!self::$instance instanceof UserWalletManager) {
            self::$instance = new self();
        }
        self::$instance->customer = $customer;
        self::$instance->initBalance();
        return self::$instance;
    }


    public function initBalance() {
        $balance = O2oCustomerBalance::findFirst("customer_id=" . $this->customer);
        if(!$balance instanceof O2oCustomerBalance) {
            $balance = new O2oCustomerBalance();
            $balance->customer_id = $this->customer;
            $balance->balance = 0;
            $balance->frozen = 0;
            $balance->available = 0;
            if(!$balance->save()) {
                $err = "";
                foreach($balance->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get('errorLogger')->error("customer balance init failed: " . $err);
                throw new \Phalcon\Exception("系统初始化商户账户失败！");
            }
        }
        self::$instance->balance = $balance;
        return self::$instance;
    }

    /**
     * @param null $filter
     * @param int $start
     * @param int $limit
     * @param string $sort
     * @return array
     */
    public function getBalanceLog($filter = null, $start = 0, $limit = 10, $sort = "created desc") {

        return array();
    }

    public function writeLog($customer, $inOut, $action, $behavior, $value, $param) {
        $log = O2oCustomerBalanceLog::findFirst("customer_id=" . $this->customer . " AND action=" . self::BALANCE_ACTION_PRE_PAY);
    }

    /**
     * @param int $cash
     * @return bool
     */
    public function frozeCash($cash = 0) {

        return true;
    }

    /**
     * @param $logId
     * @return bool
     */
    public function unfrozeCash($logId) {

        return true;
    }

    /**
     * @return array
     */
    public function getBalanceInfo() {
        return $this->balance->toArray();
    }

    //直接支付
    public function directPay($cash = 0) {
        if($cash > 0) {
            if($this->balance->available < $cash) {
                $this->balance_status = self::BALANCE_STATUS_AVAILABLE_CASH_NOT_ENOUGH;
                return false;
            }
            $this->balance->balance -= $cash;
            $this->balance->available -= $cash;
            if(!$this->balance->save()) {
                $err = "";
                foreach($this->balance->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get("errorLogger")->error("customer balance direct pay failed: " . $err);
                $this->balance_status = self::BALANCE_STATUS_DIRECT_PAY_FAILED;
                return false;
            }
            $this->balance_status = self::BALANCE_STATUS_DIRECT_PAY_SUCCESS;
            return true;
        }
        return false;
    }

    //向账户内充值
    public function recharge($cash = 0) {
        if($cash > 0) {
            $this->balance->balance += $cash;
            $this->balance->available += $cash;
            if(!$this->balance->save()) {
                $err = "";
                foreach($this->balance->getMessages() as $m) {
                    $err .= (string)$m;
                }
                $this->di->get("errorLogger")->error("customer balance direct pay failed: " . $err);
                $this->balance_status = self::BALANCE_STATUS_RECHARGE_FAILED;
                return false;
            }
            $this->balance_status = self::BALANCE_STATUS_RECHARGE_SUCCESS;
            return true;
        }
        return false;
    }

    /**
     * @param $from
     * @param $cash
     * @return bool
     */
    public function transferInto($from, $cash) {

        return true;
    }

    /**
     * @param $to
     * @param $cash
     * @return bool
     */
    public function transferFrom($to, $cash) {

        return true;
    }

    /**
     * @param int $behavior
     * @param int $cash
     * @param int $status
     * @return bool
     */
    public function prePay($behavior, $cash, $status = self::BALANCE_STATUS_PRE_PAY_SUCCESS) {

        return true;
    }

    /**
     * @param $cash
     * @return bool
     */
    public function prePayForIntegrate($cash) {

        return true;
    }

    /**
     * @param $cash
     * @return bool
     */
    public function prePayForSeckill($cash) {

        return true;
    }
} 