<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2014/11/11
 * Time: 10:26
 */

namespace Library\O2O\Financial;


use Models\Shop\ShopOrderStatMonth;
use Models\Shop\ShopOrderStatTotal;

class O2oFinanceStat
{
    public static function getMonthData()
    {
        $month_data = ShopOrderStatMonth::find('customer_id=' . CUR_APP_ID);
        if ($month_data) {
            $month_data = $month_data->toArray();
        }
        return $month_data;
    }

    public static function getTotalData()
    {
        $_data = ShopOrderStatTotal::find('customer_id=' . CUR_APP_ID);
        if ($_data) {
            $_data = $_data->toArray();
        }
        return $_data;
    }
} 