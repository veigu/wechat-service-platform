<?php
namespace Library\O2O\Marketing\Tools;
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/10
 * Time: 19:18
 */

class OneBuck extends \Phalcon\Mvc\User\Component{

    /**
     * @var OneBuck
     */
    private static $instance = null;

    private function __construct() {

    }

    /**
     * @return OneBuck
     */
    public static function  getInstance() {
        if(!self::$instance instanceof OneBuck) {
            self::$instance = new self();
        }
        return self::$instance;
    }
} 