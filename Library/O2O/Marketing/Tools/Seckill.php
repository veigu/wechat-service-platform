<?php
namespace Library\O2O\Marketing\Tools;
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/10
 * Time: 19:18
 */

class Seckill extends \Phalcon\Mvc\User\Component{

    /**
     * @var Seckill
     */
    private static $instance = null;

    private function __construct() {

    }

    /**
     * @return Seckill
     */
    public static function  getInstance() {
        if(!self::$instance instanceof Seckill) {
            self::$instance = new self();
        }
        return self::$instance;
    }
} 