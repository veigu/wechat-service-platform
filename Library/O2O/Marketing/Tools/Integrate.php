<?php
namespace Library\O2O\Marketing\Tools;
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/10
 * Time: 19:18
 */

class Integrate extends \Phalcon\Mvc\User\Component{

    /**
     * @var Integrate
     */
    private static $instance = null;

    private function __construct() {

    }

    /**
     * @return Integrate
     */
    public static function  getInstance() {
        if(!self::$instance instanceof Integrate) {
            self::$instance = new self();
        }
        return self::$instance;
    }
} 