<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/12
 * Time: 13:46
 */

namespace Library\O2O\Marketing;


use Library\O2O\Marketing\Tools\Integrate;
use Library\O2O\Marketing\Tools\Reserve;
use Library\O2O\Marketing\Tools\Seckill;
use Models\O2O\O2oMarketing;

class MarketingTool {
    /**
     * @var MarketingTool
     */
    private static $instance = null;

    /**
     * @var int
     */
    private $customer = null;

    /**
     * @var array
     */
    private $config = null;

    private function __construct() {

    }

    /**
     * @return MarketingTool
     */
    public static function getInstance() {
        if(!self::$instance instanceof MarketingTool) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getCustomerConfig($customer) {
        if($this->customer == $customer && is_array($this->config)) {
            return $this->config;
        }

        $this->customer = $customer;
        $config = O2oMarketing::findFirst("customer_id = '{$customer}'");
        if(!$config) {
            $config = new O2oMarketing();
            $this->config = $config->toArray();
        }
        else {
            $this->config = $config->toArray();
        }

        return $this->config;
    }

    /**
     * @return Integrate
     */
    public function getIntegrate() {
        return Integrate::getInstance();
    }

    /**
     * @return Seckill
     */
    public function getSeckill() {
        return Seckill::getInstance();
    }

    /**
     * @return Reserve
     */
    public function getReserve() {
        return Reserve::getInstance();
    }

    /**
     * @param $customer
     * @return bool
     */
    public function isAuthed($customer) {
        $config = $this->getCustomerConfig($customer);
        if($config['is_active'] == 1 && (
            (empty($config['time_start']) && empty($config['time_end'])) ||
            ($config['time_start'] > 0 && $config['time_start'] <= time() && empty($config['time_end'])) ||
            ($config['time_start'] > 0 && $config['time_start'] <= time() && $config['time_end'] > 0 && $config['time_end'] > time())
            )) {
            return true;
        }
        return false;
    }
} 