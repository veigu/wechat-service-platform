<?php
namespace Library\O2O\Marketing;
use Models\O2O\O2oAds;
use Models\O2O\O2oAdsItem;

/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 2014/11/10
 * Time: 19:18
 */

class Advertise extends \Phalcon\Mvc\User\Component{

    /**
     * @var Advertise
     */
    private static $instance = null;

    /**
     * @var int
     */
    private $area = 0;

    const ADS_TYPE_IMAGE_AND_TEXT = 1001;
    const ADS_TYPE_NORMAL_MERCHANT = 1002;
    const ADS_TYPE_STAR_MERCHANT = 1003;
    const ADS_TYPE_RESERVE_PRODUCT = 1004;
    const ADS_TYPE_INTEGRATE_PRODUCT = 1005;
    const ADS_TYPE_SECKILL_PRODUCT = 1006;
    const ADS_TYPE_NORMAL_PRODUCT = 1007;

    const ADS_PLATFORM_PC = 'pc';
    const ADS_PLATFORM_APP = 'app';
    const ADS_PLATFORM_WECHAT = 'wechat';

    const ADS_TERM_LAST = 'last';
    const ADS_TERM_CURRENT = 'this';
    const ADS_TERM_NEXT = 'next';

    const ADS_DATA_CACHE_KEY_PREFIX = "ads_list_cache_key_prefix_";

    private function __construct() {

    }

    /**
     * @param int $area
     * @return Advertise
     */
    public static function getInstance($area) {
        if(!self::$instance instanceof Advertise) {
            self::$instance = new self();
        }
        self::$instance->area = $area;
        return self::$instance;
    }

    /**
     * @return int
     */
    public function getArea() {
        return $this->area;
    }

    /**
     * @param int $area
     * @return $this
     */
    public function setArea($area) {
        $this->area = $area;
        return $this;
    }

    /**
     * @param $key
     * @param int $time_start
     * @param string $platform ['pc', 'app']
     * @return array
     */
    public function getAdvertise($key = null, $platform = null) {

        if(is_string($key)) {
            $data = O2oAds::findFirst("key = '{$key}'");
            return $data ? $data->toArray() : array();
        }
        else {
            $cache_key = self::ADS_DATA_CACHE_KEY_PREFIX;
            if(is_string($platform) && strlen($platform) > 0) {
                $cache_key .= $platform;
            }
            else {
                $cache_key .= "all";
            }
            $data = $this->di->get('memcached')->get($cache_key);
            if(!$data) {
                $condition = "";
                if(is_string($platform) && strlen($platform) > 0) {
                    $condition .= " platform = '{$platform}'";
                }
                $data = O2oAds::find($condition)->toArray();
                if($data) {
                    $this->di->get('memcached')->save($cache_key, $data);
                }
            }
            return $data ? $data : array();
        }
    }

    /**
     * @param $filter
     * @param int $start
     * @param int $limit
     * @param string $sort
     * @return array
     */
    public function getItemList($filter, $start = 0, $limit = 10, $sort = "created  desc") {

        return array();
    }

    /**
     * @param int $ads_key
     * @param int $time_start
     * @param int $time_end
     * @return array
     */
    public function getDetail($ads_key, $time_start, $time_end) {

        return array();
    }

    /**
     * @param $ads_key
     * @param $time_start
     * @param $time_end
     * @return int
     */
    public function getAdsItemCount($ads_key, $time_start, $time_end) {
        return O2oAdsItem::count("ads_key = '{$ads_key}' AND start_time >= {$time_start} AND end_time <= {$time_end}");
    }

    /**
     * @param string $key
     * @param string $platform
     * @param string $name
     * @param int $content_type
     * @param int $size
     * @param float $price
     * @return $this
     */
    public function addAdvertise($key, $platform, $name, $content_type, $size = 1, $price = 0.00) {

        return $this;
    }

    /**
     * @param $key
     * @param $position
     * @param $content_type
     * @param $content
     * @param $start
     * @param $end
     * @param $applicant
     * @param $applied_time
     * @return $this
     */
    public function addItem($key, $position, $content_type, $content, $start, $end, $applicant, $applied_time) {

        return $this;
    }

    /**
     * @param int $customer
     * @param int $time_start
     * @param int $time_end
     * @param int $start
     * @param int $limit
     * @param bool|null $is_success
     * @return array
     */
    public function getCustomerApplyHistory($customer, $time_start = 0, $time_end = 0, $start = 0, $limit = 10, $is_success = null) {

        return array();
    }

    /**
     * @param string $customer
     * @param string $ads_key
     * @param int $time_start
     * @param int $time_end
     * @return array
     */
    public function getCustomerSucceedAds($customer, $ads_key, $time_start = 0, $time_end = 0) {
        if(empty($customer)) {
            return array();
        }
        $condition = "customer_id = " . $customer;
        if(is_string($ads_key) && strlen($ads_key) > 0) {
            $condition .= " AND ads_key = '{$ads_key}'";
        }
        if($time_start > 0 && $time_end > 0 && $time_end > $time_start) {
            $condition .= " AND start_time >= {$time_start} AND end_time <= {$time_end}";
        }
        $adsItems = O2oAdsItem::find($condition);
        return $adsItems ? $adsItems->toArray() : array();
    }

    /**
     * @param int $frequency
     * @param string $term ['last', 'this', 'next']
     * @return array
     */
    public  function getTermTimeRange($frequency = 1, $term = 'this') {
        $date = new \DateTime();
        $start_time = "";
        $end_time = "";
        $interval = "";

        if(!in_array($term, array(
            self::ADS_TERM_LAST,
            self::ADS_TERM_CURRENT,
            self::ADS_TERM_NEXT
        ))) {
            $term = self::ADS_TERM_CURRENT;
        }

        switch($frequency) {
            case 0: {
                $start_time = strtotime(date("Y-m-d") . " 00:00:00");
                $end_time = $start_time + 76800;
                break;
            }
            case 1: {
                $start_key = $term . " week";
                $interval = 3600 * 24 * 7;
                $start_date = date("Y-m-d", strtotime($start_key));
                $start_time = strtotime($start_date . " 00:00:00");
                $end_time = $start_time + $interval;
                break;
            }
            case 2: {
                $start_key = $term . " month";
                $start_date = $date->modify($start_key)->format("Y-m-d");
                $start_time = strtotime($start_date . " 00:00:00");
                $t = date("t", $start_time);
                $interval = 3600 * 24 * $t;
                $end_time = $start_time + $interval;
                break;
            }
            case 3: {
                $range = $this->getSeasonTimeRange($term);
                list($start_time, $end_time) = array_values($range);
                break;
            }
            case 4: {
                $start_time = strtotime(date("Y" . "-01-01 00:00:00"));
                $end_time = strtotime(date("Y" . "-12-31 00:00:00"));;
                break;
            }
        }

        return array(
            'start' => $start_time,
            'end' => $end_time
        );
    }

    /**
     * @param string $term
     * @return array
     */
    private function getSeasonTimeRange($term = 'this') {
        $time = time();
        if($term == self::ADS_TERM_LAST) {
            $time = strtotime("-3 month");
        }
        else if($term == self::ADS_TERM_NEXT) {
            $time = strtotime('+3 month');
        }
        $m = date('m', $time);
        $y = date('Y', $time);
        if($m <= 3) {
            $start = strtotime($y . "-01-01 00:00:00");
            $end = strtotime($y . "-03-31 23:59:59");
        }
        else if($m <= 6) {
            $start =  strtotime($y . "-04-01 00:00:00");
            $end = strtotime($y . "-06-30 23:59:59");
        }
        else if($m <= 9) {
            $start = strtotime($y . "-07-01 00:00:00");
            $end = strtotime($y . "-09-30 23:59:59");
        }
        else {
            $start = strtotime($y . "-10-01 00:00:00");
            $end = strtotime($y . "-12-31 23:59:59");
        }
        return array(
            'start' => $start,
            'end' => $end
        );
    }
}