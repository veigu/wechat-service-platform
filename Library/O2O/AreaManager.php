<?php
/**
 * Created by PhpStorm.
 * User: Arimis
 * Date: 14-9-12
 * Time: 下午2:55
 */

namespace Library\O2O;


use Models\Product\Product;
use Models\Shop\ShopCart;
use Components\UserStatusWap;
use Phalcon\Mvc\User\Component;
use Util\Cookie;

class AreaManager extends Component
{
    /**
     * @var AreaManager
     */
     
    private static $instance = null;

	const AREA_CACHE_KEY_PREFIX_PROVINCE = "area_cache_key_prefix_for_province_";

	const AREA_CACHE_KEY_PREFIX_CITY = "area_cache_key_prefix_for_city_";

	const AREA_CACHE_KEY_PREFIX_COUNTRY = "area_cache_key_prefix_for_country_";

    const AREA_CACHE_KEY_PREFIX_TOWN = "area_cache_key_prefix_for_town_";

    const AREA_CACHE_KEY_PREFIX_VILLAGE = "area_cache_key_prefix_for_village_";

    private function __construct() {}

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param bool $refresh
	 * @return array
     */
    public function getProvinces($refresh = false)
    {
        $cacheKey = self::AREA_CACHE_KEY_PREFIX_PROVINCE . "_zh";
		$data = $this->di->get("memcached")->get($cacheKey);
		if(!$data || $refresh) {
			$province = \Models\O2O\AreaProvince::find();
			if($province) {
				$data = $province->toArray();
				$this->di->get("memcached")->save($cacheKey, $data);
			}
			else {
				$data = array();
			}
		}
		return $data;
    }

	/**
     * @param bool $refresh
	 * @return array
     */
    public function getCities($province, $refresh = false)
    {
        if(empty($province)) {
            return array();
        }
        $cacheKey = self::AREA_CACHE_KEY_PREFIX_CITY . $province;
		$data = $this->di->get("memcached")->get($cacheKey);
		if(!$data || $refresh) {
			$data = \Models\O2O\AreaCity::find("province_id = {$province}");
			if($data) {
                $data = $data->toArray();
				$this->di->get("memcached")->save($cacheKey, $data);
			}
			else {
				$data = array();
			}
		}
		return $data;
    }

    /**
     * @param bool $refresh
     * @return array
     */
    public function getCountries($city, $refresh = false)
    {
        if(empty($city)) {
            return array();
        }
        $cacheKey = self::AREA_CACHE_KEY_PREFIX_COUNTRY . $city;
        $data = $this->di->get("memcached")->get($cacheKey);
        if(!$data || $refresh) {
            $data = \Models\O2O\AreaCity::find("city_id = {$city}");
            if($data) {
                $data = $data->toArray();
                $this->di->get("memcached")->save($cacheKey, $data);
            }
            else {
                $data = array();
            }
        }
        return $data;
    }

    /**
     * @param bool $refresh
     * @return array
     */
    public function getTowns($country, $refresh = false)
    {
        if(empty($country)) {
            return array();
        }
        $cacheKey = self::AREA_CACHE_KEY_PREFIX_TOWN . $country;
        $data = $this->di->get("memcached")->get($cacheKey);
        if(!$data || $refresh) {
            $data = \Models\O2O\AreaCity::find("country_id = {$country}");
            if($data) {
                $data = $data->toArray();
                $this->di->get("memcached")->save($cacheKey, $data);
            }
            else {
                $data = array();
            }
        }
        return $data;
    }

    /**
     * @param bool $refresh
     * @return array
     */
    public function getVillages($town, $refresh = false)
    {
        if(empty($town)) {
            return array();
        }
        $cacheKey = self::AREA_CACHE_KEY_PREFIX_VILLAGE . $town;
        $data = $this->di->get("memcached")->get($cacheKey);
        if(!$data || $refresh) {
            $data = \Models\O2O\AreaCity::find("town_id = {$town}");
            if($data) {
                $data = $data->toArray();
                $this->di->get("memcached")->save($cacheKey, $data);
            }
            else {
                $data = array();
            }
        }
        return $data;
    }
}