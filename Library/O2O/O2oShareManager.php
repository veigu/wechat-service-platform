<?php
/**
 * Created by PhpStorm.
 * User: mi
 * Date: 2014/10/29
 * Time: 10:13
 */

namespace Library\O2O;


use Components\Product\ProductManager;
use Components\UserStatusWap;
use Models\O2O\O2oUserShareProduct;
use Models\Product\Distribution\ProductShareBackLog;
use Models\Product\Distribution\ProductShareOrderLog;
use Models\Product\Distribution\ProductShareParam;
use Models\Product\Product;
use Phalcon\Mvc\User\Plugin;
use Util\Cookie;
use Util\EasyEncrypt;
use Util\GetClient;

class O2oShareManager extends Plugin
{
    const SHARE_TYPE_ITEM = 'item';
    const SHARE_TYPE_SHOP = 'shop';
    const SHARE_TYPE_COLLECT = 'collect';//用户收藏

    private $plate = null;// 平台[app|store|wap]
    private $plate_base_uri = ['app' => 'o2o_app/', 'store' => 'o2o_store/', 'wap' => ''];
    private static $_instance = null;

    private static $customer_id = null;

    public static function init()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        self::$_instance->plate = strtolower(MODULE_NAME);
        return self::$_instance;
    }

    /**
     * 解析分享链接
     *
     * @param string $spm
     * @return array|bool
     */
    public function parseShareSpm($spm)
    {
        // 判断spm
        if ($spm) {
            $spm_de = EasyEncrypt::decode($spm);
            $spm_arr = explode('-', $spm_de);
            if (count($spm_arr) == 4) {
                $uid = $spm_arr[0];
                $type = $spm_arr[1];
                $item = $spm_arr[2];
                $time = $spm_arr[3];
                if (!(is_numeric($uid) && $uid > 0)) {
                    return false;
                }

                if (!in_array($type, array('shop', 'item', 'collect'))) {
                    return false;
                }

                if (!(is_numeric($item) && $item > 0)) {
                    return false;
                }

                return array('uid' => $uid, 'type' => $type, 'data' => $item, 'time' => $time);
            }
        }

        return false;
    }

    // 生成分享链接
    public function generateShareUrl($type = self::SHARE_TYPE_ITEM, $item_id = null, $customer_id = null)
    {
        $customer_id = $customer_id ? $customer_id : CUR_APP_ID;
        $uid = UserStatusWap::getUid();
        $share_detail = ['app' => 'share/detail', 'store' => 'share/detail', 'wap' => 'shop/share'];
        $url = $this->uri->baseUrl($this->plate_base_uri[$this->plate] . $share_detail[$this->plate]);
        $spm = '';
        if ($uid) {
            if ($type == self::SHARE_TYPE_ITEM && $item_id) {
                // 商品分享
                $spm = '?spm=' . EasyEncrypt::encode($uid . '-item-' . $item_id . '-' . time());
            } else if ($type == self::SHARE_TYPE_SHOP) {
                // 店铺分享
                $spm = '?spm=' . EasyEncrypt::encode($uid . '-shop-' . $customer_id . '-' . time());
            } else {
                // 个人收藏分享
                $spm = '?spm=' . EasyEncrypt::encode($uid . '-collect-' . $customer_id . '-' . time());
            }
        }

        return $url . $spm;
    }

    public function getShareParam($item_id)
    {
        $pointModel = ProductShareParam::findFirst('product_id=' . $item_id);
        $point = 0;
        $cash = 0;
        $deadline = 0;
        if ($pointModel) {
            $cash = $pointModel->cash;
            $deadline = $pointModel->deadline;
        }

        return array('point' => $point, 'cash' => $cash, 'deadline' => $deadline);
    }

    /**
     * 分享链接回调下单记录
     *
     * @param array $item_list
     * @param $order_number
     * @return bool
     */
    public function addOrderLog(Array $item_list, $order_number)
    {
        $uid = UserStatusWap::getUid();
        $share_spm = Cookie::get('share_spm');
        if (!$share_spm) {
            return false;
        }

        $share_spm_arr = $this->parseShareSpm($share_spm);
        $from_user = $share_spm_arr['uid'];
        if (!$share_spm_arr) {
            return false;
        }

        if ($from_user == $uid) {
            return false;
        }

        // 返利（现金或积分）
        $_rebate = [];

        if ($share_spm_arr['type'] == self::SHARE_TYPE_ITEM) {
            $_rebate = [$share_spm_arr['data']];
        }

        // 查找收藏商品
        if ($share_spm_arr['type'] == self::SHARE_TYPE_COLLECT) {
            $share_data = O2oUserShareProduct::find('user_id=' . $from_user);
            if ($share_data) {
                $share_data = $share_data->toArray();
                $_rebate = array_column($share_data, 'product_id');
            }
        }

        // 查找店铺商品
        if ($share_spm_arr['type'] == self::SHARE_TYPE_COLLECT) {
            $prod = Product::find(array('customer_id=' . $share_spm_arr['data'] . ' item_id in (' . implode(',', array_keys($item_list)) . ')', 'columns' => 'id'));
            if ($prod) {
                $prods = $prod->toArray();
                $_rebate = array_column($prods, 'id');
            }
        }

        // 添加日志
        foreach ($item_list as $item_id => $item) {
            if (!in_array($item_id, $_rebate)) {
                continue;
            }

            // 获取返利
            $item_share_param = $this->getShareParam($item_id);
            if (!($item_share_param['point'] > 0 || $item_share_param['cash'] > 0)) {
                continue;
            }

            if ($item_share_param['deadline'] && $item_share_param['deadline'] < time()) {
                continue;
            }

            $where = ' from_item= ' . $item_id . ' and from_user=' . $from_user . ' and order_user=' . $uid;
            if (ProductShareOrderLog::count($where) == 0) {
                $dealog = new ProductShareOrderLog();
                $dealog->customer_id = CUR_APP_ID;
                $dealog->order_number = $order_number;
                $dealog->from_item = $item_id;
                $dealog->from_user = $from_user;
                $dealog->order_user = $uid;
                $dealog->back_id = Cookie::get('share_id');
                $dealog->spm = $share_spm;
                $dealog->spm_type = $share_spm_arr['type'];
                $dealog->point = $item_share_param['point'];
                $dealog->cash = $item_share_param['cash'] * $item['num']; // todo 是否需要累计
                $dealog->created = time();
                $dealog->status = 0;
                $dealog->plate = $this->plate;
                $dealog->ymd = date('ymd');
                $dealog->total_paid = $item['sell_price'] * $item['num'];

                //
                if (!$dealog->save()) {
                    continue;
                }
            }
        }

        // 删除cookie
        Cookie::del("share_id");
        Cookie::del("share_spm");
        return false;
    }

    /**
     * todo 分享链接访问统计
     *
     */
    public function visitCount()
    {
        $from_url = isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : '';
        $ip = $this->request->getClientAddress();
        $client = new GetClient();
        $domain = $from_url ? parse_url($from_url, PHP_URL_HOST) : '';
        if ($from_url && $domain && strpos($domain, MAIN_DOMAIN) === false) {
            //
            $spm = $this->request->get('spm');
            if (!$spm) {
                return;
            }

            $spm_info = $this->parseShareSpm($spm);
            if (!$spm_info) {
                return;
            }

            $ipinfo = file_get_contents('http://ip.taobao.com/service/getIpInfo.php?ip=' . $ip);
            $ipinfo = json_decode($ipinfo, true);

            $province = @$ipinfo["data"]['region'];
            $city = @$ipinfo["data"]['city'];
            $data['ip'] = $ip;
            $data['province'] = $province;
            $data['city'] = $city;
            $data['from_domain'] = $domain;
            $data['from_url'] = $from_url;
            $data['spm'] = $spm;
            $data['spm_uid'] = $spm_info['uid'];
            $data['spm_type'] = $spm_info['type'];
            $data['spm_item'] = $spm_info['data'];
            $data['spm_time'] = $spm_info['time'];
            $data['ymd'] = date('ymd');
            $data['plate'] = $this->plate;
            $data['system'] = $client->GetOs();
            $data['browser'] = $client->GetBrowser();
            $data['created'] = time();

            if ($spm_info['type'] == self::SHARE_TYPE_ITEM) {
                $data['customer_id'] = ProductManager::instance()->getItemInfo($spm_info['data'], 'customer_id');
            } else {
                $data['customer_id'] = $spm_info['data'];
            }

            //
            $log = new ProductShareBackLog();
            $log->create($data);

            // redirect
            Cookie::set('share_id', $log->id, Cookie::OneDay);
            Cookie::set('share_spm', $spm, Cookie::OneDay);

            if ($data['spm_type'] == self::SHARE_TYPE_ITEM) {
                // 单个商品分享
                $link_module = ['app' => 'item/detail/', 'store' => 'item/detail/', 'wap' => 'shop/item/'];
                $item_id = $this->plate == "wap" ? EasyEncrypt::encode($data['spm_item']) : $data['spm_item'];
                $url = $this->plate_base_uri[$this->plate] . $link_module[$this->plate] . $item_id;
            } else if ($data['spm_type'] == self::SHARE_TYPE_SHOP) {
                // 店铺分享
                $url = $this->plate_base_uri[$this->plate] . 'shop';
            } else {
                // 个人收藏分享
                $url = $this->plate_base_uri[$this->plate] . 'user/collection/' . $data['spm_uid'];
            }
            $this->response->redirect($url)->send();

            return;
        }
    }
}