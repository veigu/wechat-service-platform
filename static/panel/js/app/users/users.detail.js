/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();

    exports.init = function() {
        $('#user-profile-2 a[data-toggle="tab"]').on("show.bs.tab", function(e) {
            var target = $(this).attr('data-url');
            if(target && target.length > 0) {
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
                window.location.href = target;
            }
        });
        this.setGroup("#user_group");
        this.setGrade("#card_grade");
    };

    /**
     * set user group
     * @param btn
     */
    exports.setGroup = function (btn) {
        $(".user-profile").on('change', btn, function (e) {
            // params
            var uid = $(this).attr('data-uid');
            var data = {uid:uid};
            var gid = $(this).val();
            data.gid = gid;
            // confirm
            var cm = window.confirm('你确定需要修改该用户会员卡等级吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/user/setGroup', data, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '设置成功！', 3000);
                }
                else {
                    base.showTip("err", res.message);
                }
            });
            e.stopImmediatePropagation();
        });
    };
    /**
     * set user vipcard grade
     * @param btn
     */
    exports.setGrade = function (btn) {
        $(".user-profile").on('change', btn, function (e) {
            // params
            var uid = $(this).attr('data-card-no');
            var data = {card_no:uid};
            var gid = $(this).val();
            var gname = $(this).find(':selected').attr('data-name');
            data.gid = gid;
            data.gname = gname;
            // confirm
            var cm = window.confirm('你确定需要修改该用户会员卡等级吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/vipcard/setUserGrade', data, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '设置成功！', 3000);
                }
                else {
                    base.showTip("err", res.message);
                }
            });
            e.stopImmediatePropagation();
        });
    };
    exports.exportsUsers = function (btn) {
        $(".user-list").on('click', btn, function (e) {
            var data = '';
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data+=$(this).attr('data-id')+',';
                }
            });

            //  has no selected
            if (data == '') {
                base.showTip('err', '请选择需要导出的用户', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要导出选中的数据吗？');
            if (!cm) {
                return;
            }
            window.location.href='/api/user/exportUser?data='+data.substring(0,data.length-1);


        });
    };
    exports.exportsAllUsers = function (btn) {
        $(".user-list").on('click', btn, function (e) {
            var cm = window.confirm('你确定需要导出所有用户数据吗？');
            if (!cm) {
                return;
            }
            window.location.href='/api/user/exportAllUser';


        });
    };
});