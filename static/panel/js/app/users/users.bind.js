/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();

    /**
     * publish panorama or not
     *
     * @param btn
     * @param referer
     */

    /**
     * del panorama
     * @param btn
     */
    exports.delUsers = function (btn) {
        $(" .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要删除该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/user/delUser', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                    window.location.reload();
                }
            });
            e.stopImmediatePropagation();
        });
    };
    /**
     * del panorama
     * @param btn
     */
    exports.delAlldelUsers = function (btn) {
        $(".user-list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/user/delUser', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！', 3000);window.location.reload();

                }
            });
            e.stopImmediatePropagation();
        });
    };
    exports.exportsUsers = function (btn) {
        $(".user-list").on('click', btn, function (e) {
            var data = '';
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data+=$(this).attr('data-id')+',';
                }
            });

            //  has no selected
            if (data == '') {
                base.showTip('err', '请选择需要导出的用户', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要导出选中的数据吗？');
            if (!cm) {
                return;
            }
            window.location.href='/api/user/exportUser?data='+data.substring(0,data.length-1);


        });
    };
    exports.exportsAllUsers = function (btn) {
        $(".user-list").on('click', btn, function (e) {
            var cm = window.confirm('你确定需要导出所有用户数据吗？');
            if (!cm) {
                return;
            }
            window.location.href='/api/user/exportAllUser';

        });
    };
    exports.checkUser = function(btn) {
        $(".user-list").on('click', btn, function (e) {
            if(!window.confirm('确定要对该用户进行此操作吗？')) {
                return false;
            }
            var uid = $(this).attr('data-id');
            var active = $(this).attr('data-active');
            base.requestApi('/api/user/checkUser', {uid: uid, active: active}, function(rs) {
                if(rs.result && rs.result == 1) {
                    tip.showTip('ok', "操作成功！", 2000);
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000);
                }
                else {
                    tip.showTip('err', rs.message, 3000);
                }
            });
        });
    };
});