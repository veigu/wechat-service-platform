/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();
    /**
     * publish reserve or not
     *
     * @param btn
     * @param referer
     */
    exports.pubReserve = function (btn) {
        $('.list .listData').on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish
            };

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/addon/reserve/publish', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * publish reserve or not
     *
     * @param btn
     * @param referer
     */
    exports.pubColumn = function (btn) {
        $('.list .listData').on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish
            };

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/addon/reserve/pubColumn', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * publish reserve or not
     *
     * @param btn
     * @param referer
     */
    exports.setOrderStatus = function (btn) {
        $('.list .listData').on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var status = $(this).attr('data-status');
            var data = {
                'id': id,
                'status': status
            };

            var cm = window.confirm('您确认审核 ' + (status == 1 ? '通过' : '不通过') + ' 吗？');
            if (!cm) {
                return;
            }

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/addon/reserve/audit', data, function (res) {
                if (res.result == 1) {
                    var sta_html = [];
                    sta_html['sta-1'] = '<span class="grey">预约不通过</span>';
                    sta_html['sta1'] = '<span class="green">客服确认通过</span>';
                    $('.list .listData .item[data-id="' + id + '"] .show_status').html(sta_html['sta' + status]);
                    $('.list .listData .item[data-id="' + id + '"] .show_btn').html('已审核');

                    base.showTip('ok', '操作成功！', 1000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    /**
     * del reserve
     * @param btn
     */
    exports.delReserve = function (btn) {
        $(".list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/reserve/delReserve', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del reserve
     * @param btn
     */
    exports.delColumn = function (btn) {
        $(".list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/reserve/delColumn', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });

            e.stopImmediatePropagation();
        });
    };


    /**
     * del reserve
     * @param btn
     */
    exports.delOrder = function (btn) {
        $(".list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/reserve/delOrder', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del reserve
     * @param btn
     */
    exports.delAllReserve = function (btn) {
        $(".list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/reserve/delReserve', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del reserve
     * @param btn
     */
    exports.delAllOrder = function (btn) {
        $(".list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/reserve/delOrder', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del reserve
     * @param btn
     */
    exports.saveColumn = function () {
        // add click
        $('.addOptionBtn').click(function (e) {
            var len = $('.list .listData .item').length;
            if (len > 10) {
                base.showTip('err', '最多可以创建10个选项', 2000);
                return;
            }
            // reset
            $('#optionWidget .res-btn').removeAttr('data-id');
            $('.choose-type-selc').show();
            $('#optionWidget .column_name').val('')
            $('#optionWidget .column_data').val('')

            base.showPop('#optionPopup');

            e.stopImmediatePropagation();
        });

        // switch
        $('#optionWidget').on('change', '.select-option-type', function () {
            var val = $(this).val();

            $('.option').hide();
            $('#option-' + val).show();

        });

        // confirm
        $('#optionWidget').on('click', '.res-btn', function () {
            var reserve_id = $(this).attr('data-reserve_id');
            var id = $(this).attr('data-id');
            var obj = $('#optionWidget .option:visible');
            var column_type = obj.attr('data-type');
            var column_name = obj.find('.column_name').val();
            var column_data = obj.find('.column_data').val();

            if (!checkField(obj.find('.column_name'), '请填写正确的字段名', /^[\w\u4E00-\u9FA5]{2,18}$/)) {
                obj.find('.column_name').focus();
                base.showTip('err', '请填写正确的字段名', 3000);
                return false;
            }

            if (column_type == 'single') {
                if (!column_data) {
                    base.showTip('err', '请填写单选选项', 3000);
                    return false;
                }
            }

            if (column_type == 'multi') {
                if (!column_data) {
                    base.showTip('err', '请填写多选选项', 3000);
                    return false;
                }
            }

            var data = {
                column_type: column_type,
                column_name: column_name,
                column_data: column_data
            };

            base.requestApi('/api/addon/reserve/saveColumn', {reserve_id: reserve_id, data: data, id: id}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '预约选项添加成功！', 3000);
                    window.location.reload();
                    base.hidePop('#optionPopup');
                }
            });

        });

        // update
        $('.list .listData .upBtn').click(function (e) {
            var id = $(this).attr('data-id');
            var column_type = $(this).attr('data-column_type');
            var column_name = $(this).attr('data-column_name');
            var column_data = $(this).attr('data-column_data');
            if (id) {
                base.showPop('#optionPopup');

                $('#optionWidget .res-btn').attr('data-id', id);
                $('.choose-type-selc').hide();
                $('.option').hide();
                var obj = $('#option-' + column_type);
                obj.find('.column_name').val(column_name);
                obj.find('.column_data').val(column_data);
                obj.show();
            }

            e.stopImmediatePropagation();
        });

    };

    function checkField(elem, msg, regx) {
        var val = $(elem).val();
        if ($(elem).val() == '') {
            $(elem).siblings('.tip').html('不能为空');
            return false;
        } else {
            if (regx) {
                if (!new RegExp(regx).test(val)) {
                    $(elem).siblings('.tip').html(msg || '格式不正确');
                    return false;
                } else {
                    $(elem).siblings('.tip').html('');
                }
            }
            $(elem).siblings('.tip').html('');
        }

        return true;
    }
});