/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数

    base.selectNone();
    base.selectCheckbox();

    exports.reply = function () {
        $('#replyForm').on('click', '#smbBtn', function (e) {
                var detail = $('#txtDetail').val();
                var demand_id = $(this).attr('data-demand_id');
                if (!detail) {
                    tip.showTip('err', '请填写回复内容～！', 3000);
                    $('#txtDetail').focus();
                    return;
                }

                var data = {
                    demand_id: demand_id, detail: detail, is_customer: 1
                };

                var cm = window.confirm('您填写完善并回复吗？');

                if (!cm) return;

                base.requestApi('/api/addon/demand/reply', {data: data}, function (res) {
                    if (res.result == 1) {
                        tip.showTip('ok', '恭喜您，回复需求成功～！', 3000);
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    }
                });

                e.stopImmediatePropagation();
            }
        )
    };

    exports.endReply = function () {
        $('#endReply').on('click', function (e) {
                var demand_id = $(this).attr('data-id');

                var data = {
                    id: demand_id
                };

                var cm = window.confirm('您确认需要结束吗，结束后将不能回复？');

                if (!cm) return;

                base.requestApi('/api/addon/demand/endReply', data, function (res) {
                    if (res.result == 1) {
                        tip.showTip('ok', '恭喜您，需求设置结束成功～！', 3000);
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    }
                });

                e.stopImmediatePropagation();
            }
        )
    };

    exports.delReply = function () {
        $(".reply").on('click', '.delReply', function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            base.requestApi('/api/addon/demand/delReply', {data: data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.replies .reply[data-id="' + data[i] + '"]').fadeOut(function () {
                            $(this).remove();
                        });
                    }
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del
     */
    exports.del = function (uri) {
        $(".list .listData").on('click', '.delBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            del(data);
            e.stopImmediatePropagation();
        });

        $(".list").on('click', '.delAllSelected', function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            del(data);

            e.stopImmediatePropagation();
        });

        function del(data) {
            uri = uri == undefined ? '' : uri;
            // api request
            base.requestApi('/api/addon/demand/del' + uri, {data: data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.list .listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
        }
    };

    /**
     * set type for diary, interest, requirement etc.
     *
     * @param uri :  last api url param
     */
    exports.setType = function () {
        // add menu row
        $(".addRowBtn").live('click', function (e) {
            var catId = $(this).attr('data-cid');
            // append empty row
            var str = '<tr class="item">';
            str += '   <th class="name"></th>';
            str += '   <td></td>';
            str += '   <td><input class="txt txtName" type="text"/></td>';
            str += '   <td><input class="txt txtDesc" type="text" style="width: 400px"/></td>';
            str += '   <td></td>';
            str += '   <td></td>';
            str += '</tr>';
            // append
            $('#interestForm .addOptionRow').after(str);

            e.stopImmediatePropagation();
        });

        var btn = '#interestForm .setBtn';
        $(btn).live('click', function (e) {
            var data = [];
            $('#interestForm .item').each(function () {
                // change data
                var id = $(this).attr('data-id');
                var name = $(this).find('.txtName').val().trim();
                var desc = $(this).find('.txtDesc').val().trim();
                //old data
                var old_name = $(this).find('.txtName').attr('data-old');
                var old_desc = $(this).find('.txtDesc').attr('data-old');
                // change or not
                if (!(name == old_name && desc == old_desc) && name) {
                    var tmp = {
                        id: id,
                        name: name,
                        desc: desc
                    };
                    data.push(tmp);
                }
            });

            if (data.length == 0) {
                base.showTip('err', '您未作任何的修改', 3000);
                return false;
            }

            // console.log(a);
            var data = {'data': data}
            // disable the button
            // api request
            base.requestApi('/api/addon/demand/setType', data, function (res) {
                base.showTip('ok', '设置成功！即将跳转');
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
                // cancel to disable the btn;
            });
            e.stopImmediatePropagation();
        });
    };
})