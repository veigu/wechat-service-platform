/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();

    /**
     * publish vote or not
     *
     * @param btn
     * @param referer
     */
    exports.pubVote = function (btn) {
        $('.list .listData').on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish
            };

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/addon/vote/publish', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del vote
     * @param btn
     */
    exports.delVote = function (btn) {
        $(".list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/vote/del', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };
    /**
     * del vote
     * @param btn
     */
    exports.delAllVote = function (btn) {
        $("#article-list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/vote/del', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };
});