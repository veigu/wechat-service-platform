/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();

    exports.setStore = function () {
        // 初始化上传
        store.getImg('#upPicBtn', function (res) {
            $(res.url).each(function (index, node) {
                var isAdded = $('.picShow').find('img[src="' + node + '"]');
                if (isAdded && isAdded.length > 0) {
                    return false;
                }
                $('.picShow').append('<a href="javascript:;" class="rmImg"  data-url="' + node + '" title="点击移除"><img src="' +
                    node + '" class="pic" alt="" style="height: 60px;vertical-align: middle;"/> x</a>'
                );
                var productImages = $('#pics').val();
                productImages += node + ';';
                $('#pics').val(productImages);
            });


        }, true);
        rmImg();
        function rmImg() {
            $('.rmImg').live('click', function () {
                var src = $(this).attr('data-url');
                var allImg = $('#pics').val();
                var imgArr = allImg.split(';');

                var str = '';
                for (var i = 0; i < imgArr.length; i++) {

                    if (imgArr[i] != src && imgArr[i]) {
                        str += imgArr[i] + ';';
                    }
                }
                $('#pics').val(str);
                $(this).remove();
            });
        }

        $('.smbBtn').live('click', function (e) {
            // params
            var data = $('form#StoreForm').serializeObject();

            if (!checkField($('.form-control[name="name"]'))) {
                return;
            }

            if (!data.group_id) {
                tip.showTip('err', '请选择分组，如果没有请创建！', 3000);
                return;
            }

            if (!data.pics) {
                tip.showTip('err', '请上传至少一张图片！', 3000);
                return;
            }

            if (!data.address) {
                tip.showTip('err', '请填写分店地址！', 3000);
                $("#txtAddress").focus();
                return;
            }

            if (!data.contact) {
                tip.showTip('err', '请填写分店联系电话！', 3000);
                $("#contact").focus();
                return;
            }

            // api request
            base.requestApi('/api/addon/store/setStore', {data: data}, function (res) {
                if (res.error.code == 0) {
                    if (data.id) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    } else {
                        setTimeout(function () {
                            window.location.href = '/panel/addon/store/default/index';
                        }, 3000);

                        base.showTip('ok', '数据插入成功，即将跳转！', 2000);
                    }
                }
            });
            e.stopImmediatePropagation();
        });

    };


    /**
     * set type for diary, interest, requirement etc.
     *
     * @param uri :  last api url param
     */
    exports.setGroup = function () {
        // add menu row
        $(".addRowBtn").live('click', function (e) {
            var catId = $(this).attr('data-cid');
            // append empty row
            var str = '<tr class="item">';
            str += '   <th class="name"></th>';
            str += '   <td></td>';
            str += '   <td><input class="txt txtName" type="text"/></td>';
            str += '   <td><input class="txt txtDesc" type="text" style="width: 400px"/></td>';
            str += '   <td></td>';
            str += '   <td></td>';
            str += '</tr>';
            // append
            $('#interestForm .addOptionRow').after(str);

            e.stopImmediatePropagation();
        });

        var btn = '#interestForm .setBtn';
        $(btn).live('click', function (e) {
            var data = [];
            $('#interestForm .item').each(function () {
                // change data
                var id = $(this).attr('data-id');
                var name = $(this).find('.txtName').val().trim();
                var desc = $(this).find('.txtDesc').val().trim();
                //old data
                var old_name = $(this).find('.txtName').attr('data-old');
                var old_desc = $(this).find('.txtDesc').attr('data-old');
                // change or not
                if (!(name == old_name && desc == old_desc) && name) {
                    var tmp = {
                        id: id,
                        name: name,
                        desc: desc
                    };
                    data.push(tmp);
                }
            });

            if (data.length == 0) {
                base.showTip('err', '您未作任何的修改', 3000);
                return false;
            }

            // console.log(a);
            var data = {'data': data}
            // disable the button
            // api request
            base.requestApi('/api/addon/store/setGroup', data, function (res) {
                base.showTip('ok', '设置成功！即将跳转');
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
                // cancel to disable the btn;
            });
            e.stopImmediatePropagation();
        });
    };

    exports.toLatLng = function () {
        $(function () {
            // 百度地图API功能
            var map = new BMap.Map("map_canvas");
            map.enableScrollWheelZoom();
            map.enableInertialDragging();
            map.addControl(new BMap.NavigationControl());
            window.map = map;

            var myGeo = new BMap.Geocoder();
            var marker = null;
            var lat = $('#txtLat').val();
            var lng = $('#txtLng').val();
            if (lat && lng) {
                var point = new BMap.Point(lng, lat);
                setMarker(point);
            } else {
                // init lat lng
                var addr = $('#txtAddress').val();
                if (addr) {
                    // 将地址解析结果显示在地图上,并调整地图视野re
                    myGeo.getPoint(addr, function (point) {
                        if (point) {
                            setMarker(point);
                        }
                    });
                } else {
                    // 获取城市信息
                    var city = new BMap.LocalCity();
                    city.get(function (res) {
                        var lng = res.center.lng;
                        var lat = res.center.lat;
                        $('#txtLat').val(lng);
                        $('#txtLng').val(lat);
                        setMarker(res.center);
                    });
                }
            }

            $('#txtAddress').on('change', function () {
                var addr = $(this).val();
                if (addr) {
                    // 将地址解析结果显示在地图上,并调整地图视野
                    myGeo.getPoint(addr, function (point) {
                        if (point) {
                            setMarker(point);
                        }
                    });
                }
            });

            function setMarker(point) {
                map.centerAndZoom(point, 18);

                $('#txtLat').val(point.lat);
                $('#txtLng').val(point.lng);

                if (marker) {
                    marker.setPosition(point);
                } else {
                    marker = new BMap.Marker(point);
                    marker.enableDragging();
                    map.addOverlay(marker);
                }

                marker.addEventListener("dragend", function () {
                    var cp = this.getPosition();
                    $('#txtLat').val(cp.lat);
                    $('#txtLng').val(cp.lng);
                });

                marker.addEventListener("dragstart", function () {
                    $('#txtLat').val('拖动中..');
                    $('#txtLng').val('拖动中..');
                });
            }
        })
    };

    /**
     * publish or not
     *
     */
    exports.publish = function () {
        $('.list .listData').on('click', '.pubBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var published = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': published
            };

            // api request
            base.requestApi('/api/addon/store/publish', data, function (res) {
                if (res.result == 1) {
                    var btnObj = $('.pubBtn[data-id="' + id + '"]');
                    if (published == 1) {
                        btnObj.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btnObj.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del
     */
    exports.del = function (uri) {
        $(".list .listData").on('click', '.delBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            del(data);
            e.stopImmediatePropagation();
        });

        $(".list").on('click', '.delAllSelected', function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            del(data);

            e.stopImmediatePropagation();
        });

        function del(data) {
            uri = uri == undefined ? '' : uri;
            // api request
            base.requestApi('/api/addon/store/del' + uri, {data: data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.list .listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
        }
    };
});