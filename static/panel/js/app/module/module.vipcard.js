/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();

    exports.setCard = function () {
        seajs.use('app/panel/panel.storage', function (s) {
            s.getImg('#upload-logo', function (res) {
                $('#card_logo').val(res.url);
                $('#logo-preview').attr('src', res.url);
                $('#cardlogo').attr('src', res.url);
            }, false);
        });

        seajs.use('app/panel/panel.storage', function (s) {
            s.getImg('#upload-bg', function (res) {
                $('#card_bg').val(res.url);
                $('#bg-preview').attr('src', res.url);
                $('#cardbg').attr('src', res.url);
            }, false);
        });

        seajs.use('app/app.editor', function (editor) {
            editor.init("#card_detail"); //选择器作为参数
            editor.init("#card_point_detail"); //选择器作为参数
        });

    };


    /**
     * del vipcard
     * @param btn
     */
    exports.delField = function (btn) {
        $(".list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/vipcard/delField', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    /**
     * publish vipcard or not
     *
     * @param btn
     * @param referer
     */
    exports.pubField = function (btn) {
        $('.list .listData').on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish,
                'grade': grade,
                'grade_name': grade_name
            };

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/addon/vipcard/pubField', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };
    /**
     * del vipcard
     * @param btn
     */
    exports.saveField = function () {
        // add click
        $('.addOptionBtn').click(function (e) {
            var len = $('.list .listData .item').length;
            if (len > 10) {
                base.showTip('err', '最多可以创建10个选项', 2000);
                return;
            }
            // reset
            $('#optionWidget .res-btn').removeAttr('data-id');
            $('.choose-type-selc').show();
            $('#optionWidget .column_name').val('')
            $('#optionWidget .column_data').val('')

            base.showPop('#optionPopup');

            e.stopImmediatePropagation();
        });

        // switch
        $('#optionWidget').on('change', '.select-option-type', function () {
            var val = $(this).val();

            $('.option').hide();
            $('#option-' + val).show();

        });

        // confirm
        $('#optionWidget').on('click', '.res-btn', function () {
            var vipcard_id = $(this).attr('data-vipcard_id');
            var id = $(this).attr('data-id');
            var obj = $('#optionWidget .option:visible');
            var column_type = obj.attr('data-type');
            var column_name = obj.find('.column_name').val();
            var column_data = obj.find('.column_data').val();

            if (!checkField(obj.find('.column_name'), '请填写正确的字段名', /^[\w\u4E00-\u9FA5]{2,18}$/)) {
                obj.find('.column_name').focus();
                base.showTip('err', '请填写正确的字段名', 3000);
                return false;
            }

            if (column_type == 'single') {
                if (!column_data) {
                    base.showTip('err', '请填写单选选项', 3000);
                    return false;
                }
            }

            if (column_type == 'multi') {
                if (!column_data) {
                    base.showTip('err', '请填写多选选项', 3000);
                    return false;
                }
            }

            var data = {
                column_type: column_type,
                column_name: column_name,
                column_data: column_data
            };

            base.requestApi('/api/addon/vipcard/saveField', {
                vipcard_id: vipcard_id,
                data: data,
                id: id
            }, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '预约选项添加成功！', 3000);
                    window.location.reload();
                    base.hidePop('#optionPopup');
                }
            });

        });

        // update
        $('.list .listData .upBtn').click(function (e) {
            var id = $(this).attr('data-id');
            var column_type = $(this).attr('data-column_type');
            var column_name = $(this).attr('data-column_name');
            var column_data = $(this).attr('data-column_data');
            if (id) {
                base.showPop('#optionPopup');

                $('#optionWidget .res-btn').attr('data-id', id);
                $('.choose-type-selc').hide();
                $('.option').hide();
                var obj = $('#option-' + column_type);
                obj.find('.column_name').val(column_name);
                obj.find('.column_data').val(column_data);
                obj.show();
            }

            e.stopImmediatePropagation();
        });

    };
    /**
     * update grade
     *
     * @param btn
     */
    exports.setGrade = function (btn) {
        // check if has changed
        $('.intPointEnd').on('change', function () {
            // get data
            var id = $(this).attr('data-id');
            var amount_start = parseInt($(this).attr('data-amount-start'));
            var amount_end = $(this).val();
            var grade = $(this).attr('data-grade');

            // check itself
            if (!(!isNaN(amount_end) && ( parseInt(amount_end) > amount_start ))) {
                $(this).addClass('err');
                base.showTip('err', '请设置当前结束额度并且大于开始额度！', 3000);
                return false;
            } else {
                // set own data
                $(this).removeClass('err');
            }

            // check next
            // set next start
            var next_grade = 1 + parseInt(grade);
            next_grade = next_grade < 10 ? '0' + next_grade : next_grade;
            var next_amount_start = 1 + parseInt(amount_end);
            // next obj . fuck here.
            var next_obj = $('.list').find('.item[data-grade="' + next_grade + '"]');
            // next grade is not exists
            if (next_obj.length == 0) {
                // just do add
                if (grade) {
                    // for new add
                    $('.addRow .add-amount-start').text(next_amount_start);
                    $('.addRow .addIntPointEnd').attr('data-amount-start', next_amount_start);
                }
                return false;
            }
            // change count
            next_obj.find('.amount-start').text(next_amount_start);
            next_obj.find('.intPointEnd').attr('data-amount-start', next_amount_start);

            var next_amount_end = next_obj.find('.intPointEnd').val();

            // check next
            if (!(!isNaN(next_amount_end) && (next_amount_end > next_amount_start ))) {
                next_obj.find('.intPointEnd').focus().addClass('err');
                base.showTip('err', '请设置结束额度并且大于开始额度！', 3000);
                return false;
            } else {
                next_obj.find('.intPointEnd').removeClass('err');
                $(this).removeClass('err');
            }
        });

        // submit to update
        $(btn).on('click', function (e) {
            var data = [];
            var err = false;
            $('.list .listData .item').each(function () {
                if ($(this).find('.intPointEnd').hasClass('err')) {
                    err = true;
                } else {
                    err = false;
                }
                // change data
                var id = $(this).attr('data-id');
                var name = $(this).find('.txtName').val().trim();
                var discount = $(this).find('.txtdiscount').val().trim();
                var amount_end = $(this).find('.intPointEnd').val().trim();
                var amount_start = $(this).find('.intPointEnd').attr('data-amount-start');
                //old data
                var old_name = $(this).find('.txtName').attr('data-old');
                var old_start = $(this).find('.intPointEnd').attr('data-old-start');
                var old_end = $(this).find('.intPointEnd').attr('data-old-end');
                var old_discount = $(this).find('.txtdiscount').attr('data-old');

                // change or not
                if (!(name == old_name
                    && discount == old_discount
                    && amount_start == old_start
                    && amount_end == old_end
                    ) && (name)) {
                    var tmp = {
                        id: id,
                        name: name,
                        amount_start: amount_start,
                        amount_end: amount_end,
                        discount: discount
                    };
                    data.push(tmp);
                }
            });

            if (err) {
                base.showTip('err', '填写的数据有误，请检查～！', 3000);
                return false;
            }

            if (data.length == 0) {
                base.showTip('err', '您未作任何的修改', 3000);
                return false;
            }

            // console.log(a);
            var data = {'data': data}
            // disable the button
            // api request
            base.requestApi('/api/addon/vipcard/setGrade', data, function (res) {
                base.showTip('ok', '等级设置成功！即将跳转');
                setTimeout(function () {
//                    window.location.reload();
                }, 1000)                // cancel to disable the btn
            });

            e.stopImmediatePropagation();

        });
    };

    /**
     * update grade
     *
     * @param btn
     */
    exports.addGrade = function (btn) {
        // submit to update
        $(btn).on('click', function (e) {
            var name = $('.addGradeForm .txtName').val().trim();
            var discount = $('.addGradeForm .txtdiscount').val().trim();
            var amount_end = $('.addGradeForm .addIntPointEnd').val().trim();
            // var amount_start = $('.addGradeForm .addIntPointEnd').attr('data-amount-start');

            var amount_start = 0;

            if (!(!isNaN(amount_end) && ( parseInt(amount_end) > amount_start ))) {
                base.showTip('err', '填写的额度数据有误，请检查～！', 3000);
                return false;
            }

            //    if (!(!isNaN(discount) && ( parseInt(discount) <= 100 && parseInt(discount) >= 0  ))) {
            if (isNaN(discount) || parseInt(discount) > 100 || parseInt(discount) < 0) {
                base.showTip('err', '填写的打折数据有误，请检查～！', 3000);
                return false;
            }

            if (!name) {
                base.showTip('err', '等级名称必填！', 3000);
                return false;
            }

            // console.log(a);
            var data = {
                'name': name,
                'amount': amount_end,
                'discount': discount
            };
            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/addon/vipcard/addGrade', data, function (res) {
                if (res.error.code == 0) {
                    base.showTip('ok', '等级设置成功！即将跳转');
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000)
                }
                // cancel to disable the btn
                $(btn).attr('disabled', false);
            });

            e.stopImmediatePropagation();

        });
    };

    exports.setRule = function (vip_grade) {
        // edit
        $('.editRowBtn').on('click', function (e) {
            var behavior = $(this).attr('data-behavior');
            $('.item-show[data-behavior="' + behavior + '"]').hide();
            $('.item-edit[data-behavior="' + behavior + '"]').fadeIn();

            e.stopImmediatePropagation();
        });
        // save item
        $('.saveRowBtn').on('click', function (e) {
            var type = $("#ruleForm").attr('data-type');
            type = (type == 'normal' ? '0' : "1");
            var id = $(this).attr('data-id');
            var behavior = $(this).attr('data-behavior');
            var obj = $(this).parents('.item-edit[data-behavior="' + behavior + '"]');
            var action = obj.find('.pointType').val();
            var term = obj.find('.term').val();
            var quantity = obj.find('.quantity').val();

            var txt_action = obj.find('.pointType option:selected').text();
            var txt_term = obj.find('.term option:selected').text();
            var txt_points = obj.find('.quantity').val();

            var data = {
                'id': id,
                'behavior': behavior,
                'vip_grade': vip_grade,
                'action': action,
                'quantity': quantity,
                'term': term,
                'is_firm': type
            };
            // api request
            base.requestApi('/api/addon/vipcard/setPoint', {data: [data]}, function (res) {
                if (res.result == 1) {
                    window.location.reload();
                    base.showTip('ok', '操作成功！');
                }
            });

            $('.item-edit[data-behavior="' + behavior + '"]').hide();
            $('.item-show[data-behavior="' + behavior + '"]').fadeIn();

            e.stopImmediatePropagation();
        });

        // edit all
        $('.editRuleBtn').on('click', function (e) {
            var flag = $(this).attr('data-flag');
            if (flag == 1) {
                $('.ruleForm p.item-edit').hide();
                $('.ruleForm p.item-show').fadeIn();
                $(this).attr('data-flag', 0).text('编辑');
                $('.saveRuleBtn').hide();
            } else {
                $('.ruleForm p.item-show').hide();
                $('.ruleForm p.item-edit').fadeIn();
                $(this).attr('data-flag', 1).text('取消');
                $('.saveRuleBtn').fadeIn();
            }
            e.stopImmediatePropagation();
        });

        // item show edit
        $('.item-show').on({
            mouseenter: function () {
                $(this).find('.editRowBtn').show();
            },
            mouseleave: function () {
                $(this).find('.editRowBtn').hide();
            }
        });

        // save all
        $('.saveRuleBtn').on('click', function (e) {
            var tmp = [];
            $('.ruleForm .item-edit:visible').each(function () {
                var id = $(this).attr('data-id');
                var behavior = $(this).attr('data-behavior');
                var action = $(this).find('.pointType').val();
                var term = $(this).find('.term').val();
                var quantity = $(this).find('.quantity').val();

                var data = {
                    'id': id,
                    'behavior': behavior,
                    'action': action,
                    'vip_grade': vip_grade,
                    'quantity': quantity,
                    'term': term
                };
                tmp.push(data);
            });

            // api request
            base.requestApi('/api/addon/vipcard/setPoint', {data: tmp}, function (res) {
                if (res.error.code == 0) {
                    base.showTip('ok', '操作成功！');
                }
                window.location.reload();
            });
            e.stopImmediatePropagation();
        });
    };
    exports.lookApply = function () {
        $('.look').on('click', function () {
            var data = eval('(' + $(this).attr('data-data') + ')');
            $("#item_id").val(data.id);
            $(".realname").html(data.realname);
            $("#card_no").val(data.card_no);
            $("#card_grade_id").val(data.card_grade);
            $("#card_grade_name").val(data.card_grade_name);
            $(".phone").html(data.phone);
            $(".email").html(data.email);
            $(".birthday").html(data.birthday);
            $("#gender").val(data.gender);
            $(".gender_text").html(data.gender == 'm' ? '男' : (data.gender == 'f' ? '女' : '保密'));
            $("#province").val(data.province);
            $("#city").val(data.city);
            $("#town").val(data.town);
            $(".area").html(data.province + data.city + data.town);
            $("input[name=is_active][value='" + data.is_active + "']").attr('checked', 'checked');
            if ($('#card_receive').val() == 0) {
                $("#card_no").attr('readonly', 'readonly');
            }
        });

        $('#card_grade_id').change(function () {
            var op = $(this).find(':selected');
            var name = $(op).attr('data-name');
            $('#card_grade_name').val(name);
        });

        $('.saveBtn').on('click', function () {
            var item_id = $("#item_id").val();
            var is_active = $("input[name=is_active]:checked").val();
            var card_no = $.trim($("#card_no").val());
            var grade = $('#card_grade_id').val();
            var grade_name = $('#card_grade_name').val();
            var parm = /[0-9a-zA-Z- ]{6}/;
            if (!parm.test(card_no)) {
                base.showTip('err', '卡号填写不规范～！', 3000);
                return false;
            }
            var data = {
                'id': item_id,
                'is_active': is_active,
                'card_no': card_no,
                'grade': grade,
                'grade_name': grade_name
            };
            base.requestApi('/api/addon/vipcard/vipcardCheck', data, function (res) {
                if (res.error.code == 0) {
                    base.showTip('ok', '操作成功！');
                    window.location.reload();
                }
            })
        });
    };

    exports.setStatus = function() {
        $('.status-change').click(function(e) {
            e.stopImmediatePropagation();
            if(e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }

            var id = $(this).attr('data-id');
            if(!id || id <= 0) {
                return false;
            }

            base.requestApi('/api/addon/vipcard/setStatus', {id: id, status: 0}, function(res) {
                if(res.code && res.code == 0) {
                    tip.showTip('ok', "操作成功！");
                }
                else {
                    tip.showTip('err', res.message);
                }
            });
        });
    };
});