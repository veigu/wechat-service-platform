/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var editor = require('app/app.editor');//公共函数
    require('tools/datetimepicker/bootstrap-datetimepicker.min.js');

    base.selectNone();
    base.selectCheckbox();

    exports.set = function () {
        editor.init('#detail');

        // 日期选择
        var use_start_val;
        $('#receive_end').datetimepicker({
            language: 'zh-CN',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).on('changeDate', function (ev) {
        });

        $('#use_start').datetimepicker({
            language: 'zh-CN',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).on('changeDate', function (ev) {
            $('#use_end').focus();
            use_start_val = ev.date.valueOf();
        });

        $('#use_end').datetimepicker({
            language: 'zh-CN',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).on('changeDate', function (ev) {
            if (!use_start_val) {
                $('#use_start').focus();
            }
            if (ev.date.valueOf() < use_start_val) {
                $('#use_start').focus();
            }
        });

        // 添加
        $('#CouponForm').on('change', '#coupon_type', function (e) {
            var val = $(this).val();
            if (!val) {
                tip.showTip('err', '请填写选择优惠卷类型！', 3000);
                return;
            }
            var html = $('.form-group[data-type="' + val + '"]').html();
            $('#coupon_param_area').html(html);
            e.stopImmediatePropagation();
        });

        // 提交
        $('#CouponForm').on('click', '.smbBtn', function (e) {
            var data = $('form#CouponForm').serializeObject();
            if (!data.coupon_type) {
                tip.showTip('err', '请填写选择优惠卷类型！', 3000);
                return;
            }

            if (!checkField($('.form-control[name="name"]'))) {
                return;
            }


            if (!checkField($('#amount'))) {
                return;
            }

            // 现金卷:COUPON_TYPE_CASH
            if (data.coupon_type == 101) {
                if (isNaN(parseInt(data.coupon_param))) {
                    tip.showTip('err', '请填写正确的优惠金额！', 3000);
                    return;
                }

                if (parseInt(data.coupon_param) == 0) {
                    tip.showTip('err', '优惠金额必须大于0！', 3000);
                    return;
                }
            }

            // 折扣卷:COUPON_TYPE_DISCOUNT
            if (data.coupon_type == 102) {
                if (isNaN(parseInt(data.coupon_param))) {
                    tip.showTip('err', '请填写正确的优惠折扣！', 3000);
                    return;
                }

                if (parseInt(data.coupon_param) <= 0 || parseInt(data.coupon_param) >= 100) {
                    tip.showTip('err', '优惠折扣必须在0-99范围内！', 3000);
                    return;
                }
            }

            // 折扣卷:COUPON_TYPE_SPECIAL
            if (data.coupon_type == 103) {

                if (isNaN(parseInt(data.param_now))) {
                    $('#param_now').focus();
                    tip.showTip('err', '请填写正确的优惠特价！', 3000);
                    return;
                }

                if (isNaN(parseInt(data.param_original))) {
                    $('#param_original').focus();
                    tip.showTip('err', '请填写正确的优惠特价可抵用现金！', 3000);
                    return;
                }

                if (parseInt(data.param_now) >= parseInt(data.param_original)) {
                    $('#param_original').focus();
                    tip.showTip('err', '优惠特价必须小于可抵用现金！', 3000);
                    return;
                }

                data.coupon_param = parseInt(data.param_now) + '|' + parseInt(data.param_original);
                // 删除数据
                data.param_now = undefined;
                data.param_original = undefined;
            }

            if (!checkField($('#total'))) {
                return;
            }

            if (isNaN(parseInt(data.use_limit))) {
                $('#use_limit').focus();
                tip.showTip('err', '请填写正确的金额数量！', 3000);
                return;
            }

            if (isNaN(parseInt(data.total))) {
                tip.showTip('err', '请填写正确的发放数量！', 3000);
                return;
            }

            if (parseInt(data.total) == 0) {
                tip.showTip('err', '发放数量必须大于0！', 3000);
                return;
            }
            if (!checkField($('#receive_end'))) {
                return;
            }
            if (!checkField($('#use_start'))) {
                return;
            }
            if (!checkField($('#use_end'))) {
                return;
            }
            console.log(data);
            base.requestApi('/api/addon/coupon/set', {data: data}, function (res) {
                if (res.result == 1) {
                    if (data.id) {
                        window.location.reload();
                    } else {
                        setTimeout(function () {
                            window.location.href = '/panel/addon/coupon/default/index';
                        }, 3000);

                        base.showTip('ok', '数据插入成功，即将跳转！', 2000);
                    }
                }
            });

            e.stopImmediatePropagation();
        });
    };

    /**
     * publish coupon or not
     *
     * @param btn
     * @param referer
     */
    exports.pub = function () {
        $('.list .listData').on('click', '.pubBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish
            };

            // api request
            base.requestApi('/api/addon/coupon/publish', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del
     */
    exports.del = function (uri) {
        $(".list .listData").on('click', '.delBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            del(data);
            e.stopImmediatePropagation();
        });

        $(".list").on('click', '.delAllSelected', function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            del(data);

            e.stopImmediatePropagation();
        });

        function del(data) {
            uri = uri == undefined ? '' : uri;
            // api request
            base.requestApi('/api/addon/coupon/del' + uri, {data: data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.list .listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
        }
    };

});