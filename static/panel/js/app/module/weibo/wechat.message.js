/**
 * Created by Arimis on 14-8-4.
 */
define(function (require, exports) {

    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var appIcon = require('app/app.icon');
    var color = require('app/app.color');
    var source = require('app/panel/panel.resource');
    var link = require('app/panel/panel.linkChoose');

    exports.init = function () {
        //图文设置 + 链接设置
        source.getPost("#addKeywordNewsBtn", function (res) {
            var htmlStr = '';
            $(res).each(function (index, item) {
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
            });
            $('#keywordRespondNewsItems').html(htmlStr);
        }, true);
        source.getItem("#addKeywordProductBtn", function (res) {
            var htmlStr = '';
            $(res).each(function (index, item) {
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
            });
            $('#keywordRespondProductItems').html(htmlStr);
        }, true);

        // set link
        link.getLink('#keywordNewsLinkBtn', function (link, type) {
            $('#keywordDefaultNewsLink').val(link);
        });
        link.getLink('#keywordProductLinkBtn', function (link, type) {
            $('#keywordDefaultProductLink').val(link);
        });
    };

    exports.reply = function (eventTarget) {
        $(eventTarget).click(function (e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }

            requestApi("/panel/wechat/respondReply", {
                msg_id: msgId,
                reply: reply
            }, function (res) {

            });
        });
    }

});