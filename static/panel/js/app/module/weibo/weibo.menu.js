/**
 * Created by wgwang on 14-4-8.
 */
"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;

$(document).ready(function () {
    $('#nestable').delegate('.tree-add', 'click', function () {
        var pid = $(this).attr('data-id');
        $('#pid').val(pid);
        $('#mid').val('');
        $('#name').val('');
        $('#clickValBox').hide();
        $('#linkValBox').show();
        $('#type').val('VIEW');
        if (pid > 0) {
            $('#type').find('option[value=TOP_BAR]').attr('disabled', true);
        }
        else {
            $('#type').find('option[value=TOP_BAR]').attr('disabled', false);
        }
        $('#menuInfoModal').modal('show');
    });

    $('#nestable').delegate('.tree-edit', "click", function (e) {
        var data = JSON.parse($(this).attr('data-data'));
        if (data) {
            $('#mid').val(data.id);
            $('#pid').val(data.pid);
            $('#name').val(data.name);
            $('#sort').val(data.sort);
            $('#type').val(data.type);
            if (data.pid > 0) {
                $('#type').find('option[value=TOP_BAR]').attr('disabled', true);
            }
            else {
                $('#type').find('option[value=TOP_BAR]').attr('disabled', false);
            }
            if (data.type == "VIEW") {
                $('#linkValBox').show();
                $('#link').val(data.target_value);
                $('#clickValBox').hide();
                $('#message').val('');
                setMessage();
            }
            else if (data.type == "CLICK") {
                $('#linkValBox').hide();
                $('#clickValBox').show();
                setMessage(data);
            }
            else {
                $('#linkValBox').hide();
                $('#clickValBox').hide();
//                $('#type').find('option').attr('disabled', true);
                $('#type').find('option[value=TOP_BAR]').attr('disabled', false);
                setMessage();
            }
            $('#menuInfoModal').modal('show');
        }
        else {
            alert("请指定要修改的菜单！", function () {
                $('#menuInfoModal').modal('hide');
            });
        }
    });

    $('#nestable').delegate('.tree-remove', "click", function (e) {
        var mid = $(this).attr('data-id');
        confirm("<div class='widget-header'><h4 class='smaller'><i class='icon-warning-sign red'></i> 确定删除此分类?</h4></div>",
            [
                {
                    label: "放弃",
                    cssClass: "btn btn-sm"
                },
                {
                    label: "确定",
                    cssClass: "btn btn-danger btn-sm",
                    onclick: function () {
                        $.ajax({
                            url: '/panel/weibo/menuRemove',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                mid: mid
                            }
                        }).done(function (data) {
                            if (data.code > 1) {
                                alert("删除失败!");
                            }
                            else {
                                $('li.dd-item[data-cat-id=' + mid + ']').remove();

                                window.location.reload();
                            }
                        });
                    }
                }
            ]
        );
    });

    $('#type').bind('change', function () {
        var val = this.value;
        if (val == "VIEW") {
            $('#linkValBox').show();
            $('#clickValBox').hide();
        }
        else if (val == 'CLICK') {
            $('#linkValBox').hide();
            $('#clickValBox').show();
        }
        else {
            $('#linkValBox').hide();
            $('#clickValBox').hide();
        }
    });


    $('li[data-type]').on('show.bs.tab', function (e) {
        var type = $(this).attr('data-type');
        if(type && type.length > 0) {
            $('#messageType').val(type);
        }
    });
    //链接选择器
    seajs.use(['app/panel/panel.linkChoose','app/panel/panel.resource'], function(link, src) {
        link.getLink('#jumpLinkBtn', function (link, type) {
            $('#link').val(link);
        });

        link.getLink('#newsLinkBtn', function (link, type) {
            $('#defaultNewsLink').val(link);
        });

        link.getLink('#productLinkBtn', function (link, type) {
            $('#defaultProductLink').val(link);
        });

        src.getPost("#addNewsBtn", function(res) {
            var htmlStr = '';
            $(res).each(function(index, item) {
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
            });
            $('#newsItems').html(htmlStr);
        }, true);
        src.getItem("#addProductBtn", function(res) {
            var htmlStr = '';
            $(res).each(function(index, item) {
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
            });
            $('#productItems').html(htmlStr);
        }, true);
    });

    //保存菜单
    $('#saveMenu').bind("click", function () {
        var mid = $('#mid').val();
        var name = $('#name').val();
        var sort = $('#sort').val();
        var type = $('#type').val();
        var pid = $('#pid').val();

        if (!pid) {
            pid = 0;
            data.pid = pid;
        }

        if (!name || name.length == 0) {
            alert("菜单名称不能为空!", function () {
                $('#name').focus();
            });
            return false;
        }

        if (type != 'CLICK' && type != "VIEW") {
            type = 'TOP_BAR';
        }

        var data = {};
        if (mid && mid > 0) {
            data.mid = mid;
        }
        data.pid = pid;
        data.name = name;
        data.sort = sort;
        data.type = type;

        if (type == 'VIEW') {
            var link = $('#link').val();
            var regUrl = /http:\/\/([\w\-]+\.)+[\w\-]+(\/[\w\-\.\/?%&=]*)?/gi;
            if (!link || link.length == 0 || !regUrl.test(link)) {
                alert("链接不能为空或者格式不正确!", function () {
                    $('#link').focus();
                });
                return false;
            }
            data.value = link;
        }
        else if (type == 'CLICK') {
            var messageType = $('#messageType').val();
            if (!messageType) {
                messageType = 'text';
            }
            data.messageType = messageType;
            var message = '';
            var messageItems = [];
            var defaultLink = '';
            switch (messageType) {
                case "text" : {
                    message = $('#respondText').val();
                    break;
                }
                case 'news': {
                    messageItems = $('#newsItems li');
                    if(!messageItems || messageItems.length == 0) {
                        BSDialog.alert("对不起，您没有设置图文消息内容！");
                        return false;
                    }
                    defaultLink = $('#defaultNewsLink').val();
                    break;
                }
                case 'product': {
                    messageItems = $('#productItems li');
                    if(!messageItems || messageItems.length == 0) {
                        BSDialog.alert("对不起，您没有设置商品消息内容！");
                        return false;
                    }
                    defaultLink = $('#defaultProductLink').val();
                    break;
                }
            }

            data.default_link = defaultLink;

            if(messageType != 'text' && messageItems.length > 0) {
                message = [];
                $(messageItems).each(function(i, news) {
                    message.push($(news).attr('data-id'));
                });
                message = message.join(',');
            }

            if (!message || message.length == 0) {
                alert("必须设置响应的信息！");
                return false;
            }
            data.value = message;
        }

        $.ajax({
            url: '/panel/weibo/menuAdd',
            type: 'post',
            dataType: 'json',
            data: data
        }).done(function (data) {
            if (data.code > 0) {
                alert("菜单保存失败！" + data.message);
            } else {
                window.location.reload();
            }
        });
    });

    //已经改为自动同步
    $('#syncToWeixin').click(function () {
        confirm("同步数据将会把您微信的菜单更新为您现有的本地设置，确定要进行此操作？", [
            {
                label: '放弃',
                cssClass: 'btn-warning'
            },
            {
                label: '确定',
                cssClass: 'btn-danger',
                onclick: function () {
                    $.ajax({
                        url: "/panel/weibo/syncToWeiBo"
                    }).done(function (data) {
                        if (data.code > 0) {
                            alert("同步数据失败" + data.message);
                            return false;
                        }
                        else {
                            alert('同步数据成功!', function() {
                                window.location.href = window.location.href;
                            });
                            return true;
                        }
                    });
                    return true;
                }
            }
        ]);
    });
});

function setMessage(data) {
    if (data) {
        $('#clickValBox [data-type=' + data.message_type + '] a').tab("show");
        $('#message').val(data.target_value);
        $('#messageType').val(data.message_type);
        if (data.message_type == 'text') {
            $('#respondText').html(data.target_value);
        }
        else if (data.message_type == 'product' || data.message_type == 'news') {
            var messages = data.message;
            $('#clickValBox div[data-tab=' + data.message_type + '] .edit_area ul').html("");
            $(messages).each(function (index, node) {
                $('#clickValBox div[data-tab=' + data.message_type + '] .edit_area ul').append('<li data-id="' + node.value + '"><span class="list-thumb"><img src="' + node.image + '"/></span><span class="list-title">' + node.title + '</span></li>');
            });

            if(data.message_type == 'news') {
                $('#defaultNewsLink').val(data.default_link);
            }
            else {
                $('#defaultProductLink').val(data.default_link);
            }
        }
    }
    else {
        $('div[data-tab=text] .edit_area').html('');
        $('div[data-tab=image] .edit_area').html('');
        $('div[data-tab=voice] .edit_area').html('');
        $('div[data-tab=video] .edit_area').html('');
        $('[data-type=text] a').tab("show");
        $('#message').val('');
        $('#messageType').val('text');
        $('#clickValBox .edit_area ul').html("");
        $('#defaultNewsLink').val('');
        $('#defaultProductLink').val('');
    }
}
