/**
 * Created by Arimis on 14-8-4.
 */
define(function (require, exports) {

    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var appIcon = require('app/app.icon');
    var source = require('app/panel/panel.resource');
    var link = require('app/panel/panel.linkChoose');

    exports.init = function () {
        //图文设置 + 链接设置
        source.getPost("#addKeywordNewsBtn", function (res) {
            var htmlStr = '';
            $(res).each(function (index, item) {
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
            });
            $('#keywordRespondNewsItems').html(htmlStr);
        }, true);
        source.getItem("#addKeywordProductBtn", function (res) {
            var htmlStr = '';
            $(res).each(function (index, item) {
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
            });
            $('#keywordRespondProductItems').html(htmlStr);
        }, true);

        // set link
        link.getLink('#keywordNewsLinkBtn', function (link, type) {
            $('#keywordDefaultNewsLink').val(link);
        });
        link.getLink('#keywordProductLinkBtn', function (link, type) {
            $('#keywordDefaultProductLink').val(link);
        });

        $('.item-respond').click(function() {
            var mid = $(this).attr('data-id');
            $('#keywordRuleOK').attr('data-mid', mid);
        });

        this.reply('#keywordRuleOK');
    };

    exports.reply = function(eventTarget) {
        $(eventTarget).click(function(e) {
            if(e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }

            var msg_type = $('#keywordRespondContent li.active').attr('data-type');
            var data = "";
            var defaultLink = '';
            switch (msg_type) {
                case 'text':
                {
                    data = $('#keywordRespondText').val();
                    break;
                }
                case 'news':
                {
                    var listItems = $('#keywordRespondContent div[data-tab=' + msg_type + '] .edit_area li');
                    defaultLink = $('#keywordRespondContent div[data-tab=' + msg_type + '] #keywordDefaultNewsLink').val();
                    break;
                }
                case 'product':
                {
                    var listItems = $('#keywordRespondContent div[data-tab=' + msg_type + '] .edit_area li');
                    defaultLink = $('#keywordRespondContent div[data-tab=' + msg_type + '] #keywordDefaultProductLink').val();
                    break;
                }
            }

            if(msg_type != 'text' && listItems && listItems.length > 0) {
                data = [];
                $(listItems).each(function(i, item) {
                    data.push($(item).attr('data-id'));
                });
                data = data.join(',');
            }

            var r_data = {
                msgId: $(this).attr('data-mid'),
                msg_type: msg_type,
                message: data,
                default_link: defaultLink
            };

            base.requestApi("/api/wechat/messageReply", r_data, function(res) {
                if(res.result == 1) {
                    base.showTip('ok', '回复消息成功！');
                }
                else {
                    base.showTip('err', res.message);
                }
            });
        });
    }

});