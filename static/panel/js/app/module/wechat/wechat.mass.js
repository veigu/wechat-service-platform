"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;
//页面初始化
define(function (require, exports) {

    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var appIcon = require('app/app.icon');
    var source = require('app/panel/panel.resource');
    var link = require('app/panel/panel.linkChoose');

    exports.init = function() {
        $('[data-toggle="tooltip"]').tooltip();

        this.messagePanel();

        this.sendMessage();
    };

    exports.messagePanel = function() {
        $('#resourceListBox').delegate('li', 'click', function () {
            $('#resourceListBox').find('li').removeClass('btn-success');
            $(this).addClass('btn-success');
        });

        //subscribe/normal/keywords event message setting type switcher
        $('li[data-type]').on('click', function () {
            var _type = $(this).attr("data-type");
            $('#messageType').val(_type);
            if (!$(this).hasClass('active')) {
                $('.tab_content[data-tab=' + _type + '] .edit_area').text('');
            }
        });

        //链接选择器
        link.getLink('#jumpLinkBtn', function (link, type) {
            $('#link').val(link);
        });

        link.getLink('#newsLinkBtn', function (link, type) {
            $('#defaultNewsLink').val(link);
        });

        link.getLink('#productLinkBtn', function (link, type) {
            $('#defaultProductLink').val(link);
        });

        //资源选择器
        source.getPost("#addNewsBtn", function(res) {
            var htmlStr = '';
            var total = 0;
            $(res).each(function(index, item) {
                if(total >= 10) {
                    return;
                }
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
            });
            $('#newsItems').html(htmlStr);
        }, true);
        source.getItem("#addProductBtn", function(res) {
            var htmlStr = '';
            var total = 0;
            $(res).each(function(index, item) {
                if(total >= 10) {
                    return;
                }
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
            });
            $('#productItems').html(htmlStr);
        }, true);
    };

    /**
     * save message settings
     * @return {[type]} [description]
     */
    exports.sendMessage = function() {
        $('#saveMsgBtn').unbind();
        $('#saveMsgBtn').bind('click', function () {
            var group = $('#group').val();
            var gender = $('#gender').val();
            var msg_type = $('#messageType').val();
            if (!msg_type) {
                msg_type = 'text';
            }
            var data = "";
            if (msg_type == 'text') {
                data = $('#respondText').val();
            }
            else {
                var messageType = $('#messageType').val();
                if (!messageType) {
                    messageType = 'text';
                }

                var message = '';
                var messageItems = [];
                var defaultLink = '';
                switch (messageType) {
                    case "text" : {
                        message = $('#respondText').val();
                        break;
                    }
                    case 'news': {
                        messageItems = $('#newsItems li');
                        if(!messageItems || messageItems.length == 0) {
                            BSDialog.alert("对不起，您没有设置图文消息内容！");
                            return false;
                        }
                        defaultLink = $('#defaultNewsLink').val();
                        break;
                    }
                    case 'product': {
                        messageItems = $('#productItems li');
                        if(!messageItems || messageItems.length == 0) {
                            BSDialog.alert("对不起，您没有设置商品消息内容！");
                            return false;
                        }
                        defaultLink = $('#defaultProductLink').val();
                        break;
                    }
                }

                if(messageType != 'text' && messageItems.length > 0) {
                    message = [];
                    $(messageItems).each(function(i, news) {
                        message.push($(news).attr('data-id'));
                    });
                    message = message.join(',');
                }

                if (!message || message.length == 0) {
                    alert("必须设置响应的信息！");
                    return false;
                }
                data = message;
            }
            base.requestApi("/api/wechat/mass", {
                group: group,
                gender: gender,
                messageType: msg_type,
                messages: data,
                default_link: defaultLink
                },
                function(data) {
                    if (data.code > 1) {
                        tip.showTip('err', data.message);
                    }
                    else {
                        tip.showTip('ok');
                    }
            });
        });
    };
});
