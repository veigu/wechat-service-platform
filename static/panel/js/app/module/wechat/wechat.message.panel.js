"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;
define(function (require, exports) {

    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var appIcon = require('app/app.icon');
    var source = require('app/panel/panel.resource');
    var link = require('app/panel/panel.linkChoose');

    $('li[data-type]').on('show.bs.tab', function (e) {
        var type = $(this).attr('data-type');
        if(type && type.length > 0) {
            $('#messageType').val(type);
        }
    });

    //图文设置 + 链接设置
    seajs.use(['app/panel/panel.resource', 'app/panel/panel.linkChoose'], function(src, link) {
        src.getPost("#addNewsBtn", function(res) {
            var htmlStr = '';
            var total = 0;
            $(res).each(function(index, item) {
                if(total >= 10) {
                    return;
                }
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
                total ++;
            });
            $('#newsItems').html(htmlStr);
        }, true);
        src.getItem("#addProductBtn", function(res) {
            var htmlStr = '';
            var total = 0;
            $(res).each(function(index, item) {
                if(total >= 10) {
                    return;
                }
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
                total ++;
            });
            $('#productItems').html(htmlStr);
        }, true);
        src.getPost("#addKeywordNewsBtn", function(res) {
            var htmlStr = '';
            var total = 0;
            $(res).each(function(index, item) {
                if(total >= 10) {
                    return;
                }
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
                total ++;
            });
            $('#keywordRespondNewsItems').html(htmlStr);
        }, true);
        src.getItem("#addKeywordProductBtn", function(res) {
            var htmlStr = '';
            var total = 0;
            $(res).each(function(index, item) {
                if(total >= 10) {
                    return;
                }
                htmlStr += '<li data-id="' + item.id + '"><span class="list-thumb"><img src="' + item.cover + '"/></span><span class="list-title">' + item.title + '</span></li>';
                total ++;
            });
            $('#keywordRespondProductItems').html(htmlStr);
        }, true);

        // set link
        link.getLink('#newsLinkBtn', function (link, type) {
            $('#defaultNewsLink').val(link);
        });
        link.getLink('#productLinkBtn', function (link, type) {
            $('#defaultProductLink').val(link);
        });
        link.getLink('#keywordNewsLinkBtn', function (link, type) {
            $('#keywordDefaultNewsLink').val(link);
        });
        link.getLink('#keywordProductLinkBtn', function (link, type) {
            $('#keywordDefaultProductLink').val(link);
        });
    });

    $('#keywordRuleModal').delegate("#addKeyword", "click", function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        var id = Math.round(Math.random() * 100000, 0);
        $("#keywordList").append($('<div class="keywordItem" id="keywordItem_' + id + '"><div class="col-sm-8">' +
            '<input type="text" class="form-control" id="keyword_0" data-keyword="1" placeholder="关键词"></div>' +
            '<div class="checkbox col-sm-4">' +
            '<label><input type="checkbox" id="fullText_0" value="full_text"> 全文匹配</label>' +
            '<a href="#" class="alert-danger remove-keyword" data-id="' + id + '" title="删除关键词"><i class="icon icon-minus" "=""></i></a>' +
            '</div><div class="help-block col-sm-8">字数不得多于30个汉字</div></div>'));
    });

    $("#keywordList").delegate('.remove-keyword', 'click', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        var id = $(this).attr('data-id');
        confirm("确定删除吗", [
            {
                'label': "取消",
                'cssClass': "btn-warning"
            },
            {
                'label': "确定",
                'cssClass': "btn-danger",
                'onclick': function () {
                    $("#keywordItem_" + id).remove();
                }
            }
        ]);
    });

    /**
     * save message settings
     * @return {[type]} [description]
     */
    $('#saveMsgBtn').unbind();
    $('#saveMsgBtn').bind('click', function () {
        var respondType = $('#respondType').val();
        if (!respondType) {
            respondType = 'subscribe';
        }
        var msg_type = $('#messageType').val();
        if (!msg_type) {
            msg_type = 'text';
        }
        var data = "";
        if (msg_type == 'text') {
            data = $('#subscribeText').val();
        }
        else {
            data = $('#messageData').val();
        }
        var defaultLink = '';
        switch (msg_type) {
            case 'text': {
                data = $('#subscribeText').val();
                break;
            }
            case 'news': {
                var msgItems = $('#newsItems li');
                if(!msgItems || msgItems.length == 0) {
                    BSDialog.alert("对不起，您没有设置图文消息内容！");
                    return false;
                }
                defaultLink = $('#defaultNewsLink').val();
                break;
            }
            case 'product': {
                var msgItems = $('#productItems li');
                if(!msgItems || msgItems.length == 0) {
                    BSDialog.alert("对不起，您没有设置商品消息内容！");
                    return false;
                }
                defaultLink = $('#defaultProductLink').val();
                break;
            }
        }

        if(msg_type != 'text') {
            data = [];
            if(msgItems) {
                $(msgItems).each(function(i, news) {
                    data.push($(news).attr('data-id'));
                });
            }
            data = data.join(',');
        }

        $.ajax({
            url: "/api/wechat/save",
            data: {
                respondType: respondType,
                messageType: msg_type,
                value: data,
                link: defaultLink
            },
            dataType: 'json',
            type: 'post'
        }).done(function (data) {
            if (data.code > 1) {
                alert(data.message);
            }
            alert('保存成功！');
        });
    });

    $('#keywordRuleList').delegate('.keyword-item-edit', 'click', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        var data = $(this).attr('data-data');
        if (!data || data.length <= 0) {
            alert('没有指定要编辑的关键词设置');
            return false;
        }
        data = JSON.parse(data);
        setKeywordMessage(data);
    });

    $('#addKeywordRule').bind("click", function () {
        setKeywordMessage();
    });

    /**
     * save message settings
     * @return {[type]} [description]
     */
    $('#keywordRuleOK').unbind();
    $('#keywordRuleOK').bind('click', function () {
        var kid = $('#kid').val();
        var ruleName = $('#ruleName').val();
        if (!ruleName || ruleName.length == 0) {
            alert("规则名称不能为空", function () {
                $('#ruleName').focus();
            });
            return false;
        }
        var keywords = {};
        var keywordElements = $('[data-keyword]');
        $.each(keywordElements, function (index, node) {
            var keyword = $(node).val();
            var fullText = $(':checkbox[value=full_text]')[index];
            if (fullText.checked) {
                fullText = 1;
            }
            else {
                fullText = 0;
            }
            keywords[index] = {'keyword': keyword, 'full_text': parseInt(fullText)};
        });

        if (keywords.length == 0) {
            alert("至少需要一个关键词", function () {
                $('#keyword_0').focus();
            });
            return false;
        }

        var keywordsData = keywords;

        var msg_type = $('#keywordRuleModal .nav-tabs li.active a').attr('data-type');
        var data = "";
        var defaultLink = '';
        switch (msg_type) {
            case 'text':
            {
                data = $('#keywordRespondText').val();
                break;
            }
            case 'news':
            {
                var listItems = $('#keywordRuleModal div[data-tab=' + msg_type + '] .edit_area li');
                defaultLink = $('#keywordDefaultNewsLink').val();
                break;
            }
            case 'product':
            {
                var listItems = $('#keywordRuleModal div[data-tab=' + msg_type + '] .edit_area li');
                defaultLink = $('#keywordDefaultProductLink').val();
                break;
            }
        }

        if(msg_type != 'text' && listItems && listItems.length > 0) {
            data = [];
            $(listItems).each(function(i, item) {
                data.push($(item).attr('data-id'));
            });
            data = data.join(',');
        }

        $.ajax({
            url: "/api/wechat/saveKeyword",
            data: {
                kid: kid,
                ruleName: ruleName,
                messageType: msg_type,
                keywords: keywordsData,
                messageData: data,
                messageLink: defaultLink
            },
            dataType: 'json',
            type: 'post'
        }).done(function (data) {
            if (data.code > 1) {
                alert(data.message);
            }
            addRenderedRule(data.result);
            alert('保存成功！', function () {
                $('#keywordRuleModal').modal('hide');
            });
        });
    });

    $('#keywordRuleList').delegate('a[data-remove]', 'click', function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        var kid = $(this).attr('data-remove');
        confirm("确定删除该关键词设置？", [
            {
                label: '放弃',
                cssClass: 'btn-warning'
            },
            {
                label: "确定",
                cssClass: 'btn-danger',
                onclick: function () {
                    $.ajax({
                        url: '/api/wechat/removeKeyword',
//                        url: '/panel/addon/wechat/default/removeKeyword',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            kid: kid
                        }
                    }).done(function (data) {
                        if (data.code > 0) {
                            alert(data.message);
                        }
                        else {
                            alert("删除成功！", function () {
                                $('#keywordRuleList li[id=' + kid + ']').remove();
                            });
                        }
                    });
                }
            }
        ]);
    });
});

function setKeywordMessage(data) {
    /*if(!data) {
     var initData = JSON.parse($('#messageSettingJson').val());
     data = initData.keyword;
     }*/
    if (data) {
        $('#kid').val(data.id);
        $('#ruleName').val(data.name);
        var keywords = data.keywords;
        if (typeof keywords == 'string') {
            keywords = JSON.parse(keywords);
        }
        var keywordDefaultBox = $('#keywordDefaultBox');
        $('#keywordList').empty().append(keywordDefaultBox);
        $('#keywordDefault').val('');
        $('#fullTextDefault').attr('checked', false);
        if (keywords && keywords.length > 0) {
            var item = {};
            var firstItem = keywords.shift();
            $('#keywordDefault').val(firstItem.keyword);
            if (firstItem.full_text > 0) {
                $('#fullTextDefault').attr('checked', true);
            }
            $(keywords).each(function (index, item) {
                if (item.full_text > 0) {
                    itemHtml += ' checked';
                }
                var itemHtml = '<div class="keywordItem" id="keywordItem_' + item.keyword + '">' +
                    '<div class="col-sm-8">' +
                    '<input type="text" class="form-control" value="' + item.keyword + '" data-keyword="1" placeholder="关键词"/>' +
                    '</div>' +
                    '<div class="checkbox col-sm-4">' +
                    '<label><input type="checkbox" id="fullText_' + item.id + '" value="full_text" ' + itemHtml + '> 全文匹配</label>' +
                    '<a href="#" class="alert-danger remove-keyword" data-id="' + item.keyword + '" title="删除关键词">' +
                    '<i class="icon icon-minus"></i></a>' +
                    '</div>' +
                    '<div class="help-block col-sm-8">字数不得多于30个汉字</div></div>';
                $('#keywordList').append(itemHtml);
            });
        }
        $('#keywordRespondContent  [data-type=' + data.message_type + ']').tab('show');
        if (data.message_type == 'text') {
            $('#keywordRespondText').val(data.respond_message);
        }
        else {
            var messages = data.message;
            $('div[data-tab=' + data.message_type + '] .edit_area ul').html('');
            $(messages).each(function (index, node) {
                var itemHtml = '<li data-id="' + node.value + '"><span class="list-thumb"><img src="' + node.image + '"/></span><span class="list-title">' + node.title + '</span></li>';
                $('div[data-tab=' + data.message_type + '] .edit_area ul').append(itemHtml);
            });
            if(data.message_type == 'news') {
                $('#keywordDefaultNewsLink').val(data.default_link);
            }
            else {
                $('#keywordDefaultProductLink').val(data.default_link);
            }
        }
    }
    else {
        $('#kid').val('');
        $('#ruleName').val('');
        var keywordDefaultBox = $('#keywordDefaultBox');
        $('#keywordList').empty().append(keywordDefaultBox);
        $('#keywordDefault').val('');
        $('#fullTextDefault').attr('checked', false);

        $('#keywordRespondText').val('');
        $('#keywordRespondNewsItems').html('');
        $('#keywordRespondProductItems').html('');
        $('#keywordDefaultNewsLink').val('');
        $('#keywordDefaultProductLink').val('');
        $('#keywordRespondContent li [data-type=text]').tab('show');
    }
}

function addRenderedRule(ruleId) {
    if (!ruleId) {
        ruleId = $('#kid').val();
    }
    $('#keywordRuleList li[id=' + ruleId + ']').remove();
    var ruleData = {
        id: ruleId
    };
    var ruleName = $('#ruleName').val();
    ruleData.name = ruleName;
    var keywords = [];

    var keywordList = $('input[data-keyword]');
    var keywordsHtml = '';
    $(keywordList).each(function (ki, node) {
        var keyword = node.value;
        var fullText = $(':checkbox[value=full_text]')[ki];
        var fullTextTag = "是";
        if (fullText.checked) {
            fullText = 1;
        }
        else {
            fullText = 0;
            fullTextTag = '否';
        }
        keywords.push({
            keyword: keyword,
            full_text: fullText
        });
        keywordsHtml += '<span>关键词：<code>' + keyword + '</code> 全文匹配:' + fullTextTag + '</span>';
        if (ki < keywordList.length - 1) {
            keywordsHtml += " | ";
        }
    });

    ruleData.keywords = keywords;

    var msg_type = $('#keywordRuleModal .nav-tabs li.active a').attr('data-type');
    ruleData.message_type = msg_type;

    var respond_message = "";
    if (msg_type == 'text') {
        respond_message = $('#keywordRespondText').val();
        ruleData.default_link = '';
    }
    else {
        var respondList = $('#keywordRuleModal div[data-tab=' + msg_type + '] .edit_area li');
        respond_message = [];
        $(respondList).each(function (index, node) {
            respond_message.push($(node).attr('data-id'));
        });

        if(msg_type == 'news') {
            ruleData.default_link = $('#keywordDefaultNewsLink').val();
        }
        else {
            ruleData.default_link = $('#keywordDefaultProductLink').val();
        }
    }
    ruleData.respond_message = respond_message;

    var messageType = '文本';
    switch (ruleData.message_type) {
        case 'image':
            messageType = '图片';
            break;
        case 'voice':
            messageType = "音频";
            break;
        case 'video':
            messageType = '视频';
            break;
        case 'news':
            messageType = '图文';
            break;
        case 'product':
            messageType = '商品';
            break;
    }

    var respond_message_html = '';
    var message = [];
    if (msg_type == 'text') {
        respond_message_html = '<pre>' + respond_message + '</pre>';
    }
    else if (msg_type == 'news') {
        respond_message_html = '<ul class="list list-unstyled">';

        $(respond_message).each(function (index, node) {
            var imageUrl = $('li[data-id=' + node + ']').find('img').attr('src');
            var title = $('li[data-id=' + node + ']').find('span.list-title').text();
            respond_message_html += "<li data-id='" + node + "'><div class='row col-sm-12'><span class='col-sm-4'><img src='" + imageUrl + "' style='width: 50px; height: 40px;'/></span><span class='col-sm-8'>" + title + "</span></div></li>";
            message.push({
                message_type: 'news',
                value: node,
                url: imageUrl,
                title: title
            });
        });
        respond_message_html += '</ul>';
        ruleData.message = message;
    }
    else if (msg_type == 'product') {
        respond_message_html = '<ul class="list list-unstyled">';
        $(respond_message).each(function (index, node) {
            var imageUrl = $('li[data-id=' + node + ']').find('img').attr('src');
            var title = $('li[data-id=' + node + ']').find('span.list-title').text();
            respond_message_html += "<li data-id='" + node + "'><div class='row col-sm-12'><span class='col-sm-4'><img src='" + imageUrl + "' style='width: 50px; height: 40px;'/></span><span class='col-sm-8'>" + title + "</span></div></li>";
            message.push({
                message_type: 'product',
                value: node,
                url: imageUrl,
                title: title
            });
        });
        respond_message_html += '</ul>';
        ruleData.message = message;
    }

    var returnHtml =
        '<li id="' + ruleId + '"><div class="panel panel-default">' +
            '<div class="panel-header">' +
            '<span class="pull-left col-sm-10"><h4>' + ruleName + '</h4></span>' +
            '<span class="pull-right col-sm-2">' +
            '<a href="#" data-remove="' + ruleData.id + '" class="pull-right"><i class="icon icon-trash"></i></a>' +
            '<a href="#" data-id="' + ruleData.id + '" data-data=\'' + JSON.stringify(ruleData) + '\' class="pull-right keyword-item-edit" data-toggle="modal" data-target="#keywordRuleModal"><i class="icon icon-pencil"></i></a>' +
            '</span>' +
            '</div>' +
            '<div class="panel-body">' +
            '<div class="pull-left col-sm-12">' + keywordsHtml + '</div>' +
            '<div class="pull-left col-sm-12">' +
            '<h4>回复设置</h4>' +
            '<label>消息类型：</label>' + messageType +
            '<label>具体内容：</label>' + respond_message_html +
            '</div>' +
            '<div class="pull-left col-sm-12">' +
            '<h4>默认链接</h4>' +
            '<label>' + ruleData.default_link + '</label>' +
            '</div>' +
            '</div>' +
            '</div></li>';
    $('#keywordRuleList').append(returnHtml);
}
