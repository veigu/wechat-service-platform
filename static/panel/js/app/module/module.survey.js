/**
 * Created by yanue on 6/25/14.
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();

    /**
     * publish survey or not
     *
     * @param btn
     * @param referer
     */
    exports.pubSurvey = function (btn) {
        $('.list .listData').on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish
            };

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/addon/survey/publish', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del survey
     * @param btn
     */
    exports.delSurvey = function (btn) {
        $(".list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/survey/delSurvey', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };
    /**
     * del survey
     * @param btn
     */
    exports.delAllSurvey = function (btn) {
        $("#article-list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/addon/survey/del', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };
    exports.addOption=function(btn)
    {
        $("#optionlist").on('click', btn, function (e) {

            var data=$("#option_name").val();
            var question_id=$("#question_id").val();
            if ($.trim(data) == '') {
                base.showTip('err', '请输入选项值', 3000);
                return;
            }
            base.requestApi('/api/addon/survey/addOption', {'data': data,'question_id':question_id}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '添加成功！', 3000);
                    window.location.href=window.location.href;
                }
            });
            e.stopImmediatePropagation();


        })
    }
    exports.delOption=function(btn)
    {
        $("#optionlist").on('click', btn, function (e) {

            var data=$(this).attr('data-id');
            base.requestApi('/api/addon/survey/delOption', {'data': data}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '删除成功！', 3000);
                    window.location.href=window.location.href;
                }
            });
            e.stopImmediatePropagation();


        })
    }
    exports.delQuestion=function(btn)
    {
        $(".questionlist").on('click', btn, function (e) {

            var data=$(this).attr('data-id');
            base.requestApi('/api/addon/survey/delQuestion', {'data': data}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '删除成功！', 3000);
                    window.location.href=window.location.href;
                }
            });
            e.stopImmediatePropagation();


        })
    }
    exports.updateQuestion=function(btn){
        $("#questionInfoModal").on('click', btn, function (e) {
            var type=$("input[name='type']:checked").val();
            var desc=$("#question_desc").val();
            var id=$("#qid").val();

            base.requestApi('/api/addon/survey/updateQuestion', {'desc': desc,'type':type,'id':id}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '更新成功！', 3000);
                    window.location.href=window.location.href;
                }
            });
            e.stopImmediatePropagation();

        })


    }
});