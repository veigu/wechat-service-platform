/**
 */
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();

    exports.addOption=function(btn){
        $("#questionInfoModal").on('click', btn, function (e) {


            var id=$("#pictorial_id").val();
            var name= $.trim($("#option_name").val());
            var thumb= $.trim($("#infoPic").val());
            if(id=='' || name=='' ||  thumb=='')
            {
                 alert('请填全信息再提交');return false;
            }

            base.requestApi('/api/addon/pictorial/addOption', {'option_name': name,'thumb':thumb,'id':id}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '添加成功！', 3000);
                    window.location.href=window.location.href;
                }
            });
            e.stopImmediatePropagation();

        })


    }
    exports.delOption=function(btn)
    {
        $('.listData').on('click',btn,function(){

            var id=$(this).attr('data-id');
            if(!id)
            {
                return false ;
            }
            base.requestApi('/api/addon/pictorial/delOption',{'id':id},function (res){
                if (res.result == 1) {
                    base.showTip('ok', '删除成功！', 3000);
                    window.location.href=window.location.href;
                }
            })
            e.stopImmediatePropagation();


        } )


    }
    exports.delAllOption=function(btn)
    {
        $('.listData').on('click',btn,function(){
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return false;
            }
            // api request
            base.requestApi('/api/addon/pictorial/delAllOption', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！', 3000);
                }
            });
            e.stopImmediatePropagation();

        })
    }
    exports.del=function(btn){
        $('.listData').on('click',btn,function(){
        var id = $(this).attr('data-id');
        var data = [id];
        // confirm
        var cm = window.confirm('你确定需要该条数据吗？');
        if (!cm) {
            return;
        }

        // api request
        base.requestApi('/api/addon/pictorial/del', {data: data}, function (res) {
            if (res.result == 1) {
                $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                setTimeout(function () {
                    $('.list .listData .item[data-id="' + id + '"]').remove();
                }, 1000);
                base.showTip('ok', '删除成功！', 3000);
            }
        });
        e.stopImmediatePropagation();
        })
    }
    exports.delAllPictorial=function(btn)
    {
        $('.listData').on('click',btn,function(){
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return false;
            }
            // api request
            base.requestApi('/api/addon/pictorial/del', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！', 3000);
                }
            });
            e.stopImmediatePropagation();

        })
    }
});