define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    base.selectNone();
    base.selectCheckbox();

    // number check
    $.fn.number = function () {
        $(this).css("ime-mode", "disabled");
        this.bind("keypress", function (e) {
            var code = (e.keyCode ? e.keyCode : e.which); //兼容火狐 IE
            if (!$.browser.msie && (e.keyCode == 0x8)) { //火狐下不能使用退格键
                return;
            }
            if (this.value.indexOf(".") == -1) {
                return (code >= 48 && code <= 57) || (code == 46);
            } else {
                return code >= 48 && code <= 57
            }
        });
        this.bind("paste", function () {
            return false;
        });
        this.bind("keyup", function () {
            if (this.value.slice(0, 1) == ".") {
                this.value = "";
            }
        });
        this.bind("blur", function () {
            if (this.value.slice(-1) == ".") {
                this.value = this.value.slice(0, this.value.length - 1);
            }
        });
    };

    function setRegion(regions) {
        var strHtml = '';
        for (var i in regions) {
            strHtml += '<label for=""><input type="checkbox" class="region" name="region[]" value="' + regions[i] + '" id=""/>' + regions[i] + '</label>';
        }
        $("#regionPopup #region_area").html(strHtml);
    }

    function getHtml(type, item_id) {
        var strHtml = '';
        strHtml += '<tr data-id="' + item_id + '" data-type="' + type + '"   class="item" data-is_new="true">';
        strHtml += '<td>';
        strHtml += '    指定地区';
        strHtml += '<span>';
        strHtml += '<a href="javascript:;" data-item_id="' + item_id + '" data-type="' + type + '" class="RegionPop">编辑</a>';
        strHtml += '</span>';
        strHtml += '&nbsp;';
        strHtml += '<span>';
        strHtml += '<a href="javascript:;" data-id="' + item_id + '" class="delItem"   data-type="' + type + '" data-is_new="true">删除</a></span>';
        strHtml += '<br>';
        strHtml += '<span data-item_id="' + item_id + '" style="color:#a3a3a3" class="showDist ' + type + '"></span>';
        strHtml += '<input type="hidden" data-type="' + type + '" class="txtDistrict ' + type + '" data-item_id="' + item_id + '">';
        strHtml += '</td>';
        strHtml += '<td><input type="text" class="first_num"  data-item_id="' + item_id + '"     ></td>';
        strHtml += '<td><input type="text" class="first_cash" data-item_id="' + item_id + '"></td>';
        strHtml += '<td><input type="text" class="addon_num" data-item_id="' + item_id + '"></td>';
        strHtml += '<td><input type="text" class="addon_cash" data-item_id="' + item_id + '"></td>';
        strHtml += '<input type="hidden" class="append" value="append">';
        strHtml += '</tr>';
        return strHtml;
    }


    // 弹一个
    function openRegionDialog(all_region) {
        // open
        $(".RegionPop").live('click', function (e) {
            base.showPop("#regionPopup");
            var type = $(this).attr("data-type"); // ems|epr
            var item_id = $(this).attr("data-item_id");

            $("#regionPopup .getRegionBtn").attr("data-type", type).attr("data-item_id", item_id);

            var used = [];
            $(".txtDistrict[data-type=" + type + "]").each(function () {
                var region = $(this).val();
                if (region) {
                    var region_arr = region.split(',');
                    for (var i in region_arr) {
                        used.push(region_arr[i]);
                    }
                }
            });

            var lastLeft = [];//剩下的
            // 去除已经存在的
            var all_region_str = all_region.join(',') + ',';
            for (var i in used) {
                console.log(used[i]);
                all_region_str = all_region_str.replace(used[i] + ",", '');
            }


            all_region_str = all_region_str.substr(0, all_region_str.length - 1);

            lastLeft = all_region_str.split(',');

            // 获取本身已经存在的
            var exists_arr = $(".txtDistrict." + type + "[data-item_id='" + item_id + "']").val().split(",");
            if (exists_arr) {
                for (var i in exists_arr) {
                    if (exists_arr[i]) {
                        lastLeft.push(exists_arr[i]);
                    }
                }
            }
            // 重新设置
            setRegion(lastLeft);
            if (exists_arr) {
                // 显示已经选的
                for (var i in exists_arr) {
                    $(".region[value=" + exists_arr[i] + "]").prop("checked", true);
                }
            }
            e.stopImmediatePropagation();
        });


        // confirm
        $(".getRegionBtn").click(function () {
            var all = [];
            var type = $(this).attr("data-type");
            var item_id = $(this).attr("data-item_id");
            console.log(type);
            $('#regionPopup .region:checked').each(function () {
                var region = $(this).val();
                all.push(region);
            });

            if (all.length == 0) {
                tip.showTip('err', "请选择至少一个省份", 3000);
                return;
            }

            $(".showDist." + type + "[data-item_id='" + item_id + "']").html(all.join(","));
            $(".txtDistrict." + type + "[data-item_id='" + item_id + "']").val(all.join(","));

            $('#regionPopup .region:checked').each(function () {
                $(this).prop("checked", false);
            });

            base.hidePop("#regionPopup");
        });
    }


    exports.setFreight = function () {
        var all_region = ["北京", "天津", "河北省", "山西省", "内蒙古自治区", "辽宁省", "吉林省", "黑龙江省", "上海市", "江苏省", "浙江省", "安徽省", "福建省", "江西省", "山东省", "河南省", "湖北省", "湖南省", "广东省", "广西壮族自治区", "海南省", "重庆市", "四川省", "贵州省", "云南省", "西藏自治区", "陕西省", "甘肃省", "青海省", "宁夏回族自治区", "新疆维吾尔自治区", "香港特别行政区", "澳门特别行政区", "台湾省"];
        // 设置区域
        setRegion(all_region);
        // 打开区域
        openRegionDialog(all_region);
        // 数字过滤
        $(".first_num").number();
        $(".first_cash").number();
        $(".addon_num").number();
        $(".addon_cash").number();


        $(".addAreaBtn").on('click', function () {

            var size = $(this).attr('data-size');
            var mode = $(this).attr('data-mode');
            if (!isNaN(size) && size > 0) {
                size++;
            } else {
                size = 1;
            }
            console.log(size);
            console.log(mode);

            $(this).attr('data-size', size);
            console.log('.listFreight[data-mode=' + mode + '] .listData');
            $('.listFreight[data-mode=' + mode + '] .listData').append(getHtml(mode, size));
        });

        // 切换计价方式
        $(".valuation_way").on('change', function () {
            if (confirm('确定要改变计价方式吗?')) {
                var way = $(this).val();
                if (way == 'number') {
                    $(".x_one").html("首件(个)");
                    $(".x_co").html("续件(个)");
                }
                else if (way == 'volume') {
                    $(".x_one").html("首体积(大小)");
                    $(".x_co").html("续体积(大小)");
                }
                else if (way == 'weight') {
                    $(".x_one").html("首重(重量)");
                    $(".x_co").html("续重(重量)");
                }
            }
        });

        // 增加或删除运输方式
        $('.deliverMethod').on('click', function (e) {
            var choose = [];
            $(".deliverMethod:checked").each(function () {
                console.log(11);
                choose.push($(this).val());
            });
            if (choose.length == 0) {
                base.showTip('err', "请选择至少一项运送方式", 3000);
                $(this).prop("checked", "checked");
                return;
            }
            var type = $(this).val();
            var typeObj = $('.listFreight[data-mode="' + type + '"]');
            typeObj.fadeIn();
            if ($(this).prop('checked') === false) {
                if (confirm("您确认需要取消这种运送模式么？")) {
                    typeObj.fadeOut(500);
                } else {
                    $(this).prop("checked", "checked");
                }
            }
            e.stopImmediatePropagation();
        });

        // 保存数据
        $('.saveFreightBtn').on('click', function (e) {
            var data_base = $("#freightForm").serializeObject();

            var valid = true;
            var data_items = [];
            // 基本信息
            if (!data_base.tpl_name) {
                tip.showTip('err', "请填写模板名称！", 3000);
                $("#tplName").focus();
                return;
            }
            if (!data_base.deliver_address) {
                tip.showTip('err', "请填写发货地址！", 3000);
                $("#deliverAddr").focus();
                return;
            }
            // 运费设置
            $('.listFreight:visible .listData tr').each(function (e) {
                var type = $(this).attr("data-type");
                var is_national = $(this).attr('data-is_national');
                var is_new = $(this).attr('data-is_new');
                var first_num = $(this).find('.first_num').val();
                var first_cash = $(this).find('.first_cash').val();
                var addon_num = $(this).find('.addon_num').val();
                var addon_cash = $(this).find('.addon_cash').val();
                var district_cn = $(this).find('.txtDistrict').val();

                // 新增加， 需要选择省份
                if (!is_national) {
                    is_national = 0;
                    if (!district_cn) {
                        tip.showTip('err', "请选择指定地区！", 3000);
                        $(this).find('.first_num').focus();
                        valid = false;
                        return false;
                    }
                }

                // 判断
                if (!first_num) {
                    tip.showTip('err', "请填写表单！", 3000);
                    $(this).find('.first_num').focus();
                    valid = false;
                    return false;
                }
                if (!first_cash) {
                    tip.showTip('err', "请填写表单！", 3000);
                    $(this).find('.first_cash').focus();
                    valid = false;
                    return false;
                }
                if (!addon_num) {
                    tip.showTip('err', "请填写表单！", 3000);
                    $(this).find('.addon_num').focus();
                    valid = false;
                    return false;
                }
                if (!addon_cash) {
                    tip.showTip('err', "请填写表单！", 3000);
                    $(this).find('.addon_cash').focus();
                    valid = false;
                    return false;
                }

                // data
                var tmp = {
                    mail_mode: type,
                    first_num: first_num,
                    first_cash: first_cash,
                    addon_num: addon_num,
                    addon_cash: addon_cash,
                    is_national: is_national,
                    district_cn: district_cn
                };

                data_items.push(tmp);
            });

            if (!valid) {
                return false;
            }

            base.requestApi('/api/freight/save', {base: data_base, items: data_items}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '恭喜您，设置成功！', 3000);
                    window.location.href = '/shop/freight/add?id=' + res.data;
                }
            });
            e.stopImmediatePropagation();
        });


    };

    /**
     * del nav
     * @param btn
     */
    exports.delfPost = function (btn) {
        $(".listFreight").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var isNew = $(this).attr('data-is_new');
            var data = [id];
            console.log(isNew);
            // 直接删除新添加的
            if (isNew == "true") {
                $(' .listFreight .item[data-id="' + id + '"]').fadeOut();
                setTimeout(function () {
                    $(' .listFreight .item[data-id="' + id + '"]').remove();
                }, 1000);
            }

            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/freight/del', {data: data}, function (res) {
                if (res.result == 1) {
                    $(' .listFreight .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $(' .listFreight .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    exports.delItem = function (btn) {
        $(".listFreight").on('click', btn, function (e) {
            var id = $(this).attr('data-id');
            var type = $(this).attr('data-type');

            var data_new = $(this).attr('data-is_new');

            if (data_new == "true") {
                $('.item[data-id="' + id + '"][data-type="' + type + '" ]').remove();
            } else {
                var data = [id];
                // confirm
                var cm = window.confirm('你确定需要该条数据吗？');
                if (!cm) {
                    return;
                }

                // api request
                base.requestApi('/api/freight/delt', {data: data}, function (res) {
                    if (res.result == 1) {
                        $(' .listFreight .item[data-id="' + id + '"]').fadeOut();
                        setTimeout(function () {
                            $(' .listFreight .item[data-id="' + id + '"]').remove();
                        }, 1000);
                        base.showTip('ok', '删除成功！', 3000);
                    }
                });
                e.stopImmediatePropagation();
            }
        });

    };
});