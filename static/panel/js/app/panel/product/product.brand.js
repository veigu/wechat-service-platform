define(function (require, exports) {
    var base = require('app/panel/panel.base');
    base.selectNone();
    base.selectCheckbox();
    var storage = require('app/panel/panel.storage');

    /**
     * del nav
     * @param btn
     */
    exports.del = function () {
        $(".list .listData").on('click', '.delBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            del(data);
            e.stopImmediatePropagation();
        });

        $(".list").on('click', '.delAllSelected', function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            del(data);

            e.stopImmediatePropagation();
        });

        function del(data) {
            // api request
            base.requestApi('/api/product/delBrand', {data: data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.list .listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
        }
    };

    /**
     * set nav
     * -- update or add
     */
    exports.setBrand = function () {

        storage.getImg('.getCoverBtn', function func(res) {
            $('#navModal #image').val(res.url);
            $('.preview_img').attr('src', res.url);
        }, false);

        // update event
        $('.upBtn').click(function (e) {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            var logo = $(this).attr('data-logo');
            // api request
            setModal({id: id, name: name, image: logo});
            $('#navModal').modal('show');

            e.stopImmediatePropagation();
        });

        // add event
        $('.addBtn').click(function (e) {
            resetModal();
        });
        // save event
        save();
    };


    function setModal(data) {
        // set icon
        $('#navModal').find('#image').val(data.image);
        $('#navModal').find('.preview_img').attr('src', data.image);
        $('#navModal').find('#name').val(data.name);
        $('#navModal').find('.saveBtn').attr('data-id', data.id);
    }

    function resetModal() {
        $('#navModal').find('#name').val('');
        $('#navModal').find('.saveBtn').removeAttr('data-id');
    }

    function save() {
        $('#navModal .saveBtn').click(function (e) {
            var name = $('#navModal').find('#name').val();
            var image = $('#navModal').find('#image').val();

            var id = $(this).attr('data-id');

            if (!checkNull('#navModal #name', '品牌名称不能为空')) {
                return false;
            }
            var data = {
                name: name,
                logo: image
            };

            // api request
            base.requestApi('/api/product/saveBrand', {'data': data, id: id}, function (res) {
                if (res.result == 1) {
                    $('#navModal').modal('hide');
                    window.location.reload();
                }
            });

            e.stopImmediatePropagation();
        });
    }

    function checkNull(elem, msg) {
        if ($(elem).val() == '') {
            $(elem).siblings('.help-inline').html(msg);

            return false;
        } else {
            $(elem).siblings('.help-inline').html('');
        }

        return true;
    }
});