define(function (require, exports) {
    var base = require('app/panel/panel.base');
    base.selectNone();
    base.selectCheckbox();
    var link = require('app/panel/panel.linkChoose');
    var storage = require('app/panel/panel.storage');

    var appIcon = require('app/app.icon');
    var color = require('app/app.color');

    /**
     * del nav
     * @param btn
     */
    exports.del = function () {
        $(".list .listData").on('click', '.delBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('警告：\n删除分类后，会导致商品错乱！！\n你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            del(data);
            e.stopImmediatePropagation();
        });

        $(".list").on('click', '.delAllSelected', function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            del(data);

            e.stopImmediatePropagation();
        });

        function del(data) {
            // api request
            base.requestApi('/api/product/delCat', {data: data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.list .listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
        }
    };

    /**
     * set nav
     * -- update or add
     */
    exports.setCat = function () {

        storage.getImg('.getCoverBtn', function func(res) {
            $('#navModal #image').val(res.url);
            $('.preview_img').attr('src', res.url);
        }, false);


        // update event
        $('.upBtn').click(function (e) {
            var id = $(this).attr('data-id');
            // api request
            base.requestApi('/api/product/getCat', {'id': id}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '获取数据成功！', 1000);
                    $('#navModal').modal('show');
                    setModal(res.data);
                }
            });

            e.stopImmediatePropagation();
        });

        // add event
        $('.addBtn').click(function (e) {
            resetModal();
        });

        // set icon
        appIcon.setIcon('.icon-preview', '.icon-wrap', function (icon) {
            $('#navModal #icon').val(icon);
        });

        // set link
        link.getLink('.getLinkUrl', function (link, type) {
            $('#navModal #link').val(link);
            $('#navModal #link_type').val(type);
        });

        // save event
        save();
    };


    function setModal(data) {
        // set icon
        var icon = data.icon;
        $('.icon-preview').removeClass().addClass('icon-preview ' + icon);
        $('#navModal').find('#icon').val(data.icon);

        $('#navModal').find('#image').val(data.image);
        $('#navModal').find('.preview_img').attr('src', data.image);
        // 二级分类
        $('#navModal').find('.select_cid').hide();

        $('#navModal').find('#name').val(data.name);
        $('#navModal').find('#sub_name').val(data.sub_name);
        $('#navModal').find('#detail').val(data.detail);
        $('#navModal').find('#sort').val(data.sort);
        $('#navModal').find('.saveBtn').attr('data-id', data.id);

        // radio
        $('#navModal').find('.published').attr("checked", false);
        $('#navModal').find('.published[value="' + data.published + '"]').attr("checked", true).parent('label').addClass('active');
    }

    function resetModal() {
        // set icon
        $('.icon-preview').removeClass().addClass('icon-preview icon-sun');
        $('#navModal').find('#icon').val('icon-sun');
        $('#navModal').find('.select_cid').show();

        $('#navModal').find('#name').val('');
        $('#navModal').find('#sub_name').val('');
        $('#navModal').find('#detail').val('');
        $('#navModal').find('#sort').val(0);
        $('#navModal').find('.saveBtn').removeAttr('data-id');

        // radio
        $('#navModal').find('.published').attr("checked", false);
        $('#navModal').find('.published[value="1"]').attr("checked", true).parent('label').addClass('active');
    }

    function save() {
        $('#navModal .saveBtn').click(function (e) {
            var name = $('#navModal').find('#name').val();
            var sub_name = $('#navModal').find('#sub_name').val();
            var detail = $('#navModal').find('#detail').val();
            var icon = $('#navModal').find('#icon').val();
            var image = $('#navModal').find('#image').val();
            var sort = $('#navModal').find('#sort').val();
            var published = $('#navModal').find('.published:checked').val();
            var parent_id = $('#navModal').find('#parent_id option:selected').val();


            var id = $(this).attr('data-id');

            if (!checkNull('#navModal #name', '分类名称不能为空')) {
                return false;
            }

            if (!checkNull('#navModal #link', '链接不能为空')) {
                return false;
            }

            if (!checkNull('#navModal #icon', '请选择图标')) {
                return false;
            }

            var data = {
                name: name,
                sub_name: sub_name,
                icon: icon,
                image: image,
                sort: sort,
                parent_id: parent_id,
                published: published
            };

            // api request
            base.requestApi('/api/product/saveCat', {'data': data, id: id}, function (res) {
                if (res.result == 1) {
                    $('#navModal').modal('hide');
                    window.location.reload();
                }
            });

            e.stopImmediatePropagation();
        });
    }

    function checkNull(elem, msg) {
        if ($(elem).val() == '') {
            $(elem).siblings('.help-inline').html(msg);

            return false;
        } else {
            $(elem).siblings('.help-inline').html('');
        }

        return true;
    }
});