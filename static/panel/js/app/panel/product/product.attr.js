define(function (require, exports) {
    var base = require('app/panel/panel.base');
    base.selectNone();
    base.selectCheckbox();


    /**
     * del vipcard
     * @param btn
     */
    exports.delAttr = function (btn) {
        $(".list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/product/delAttr', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    /**
     * publish vipcard or not
     *
     * @param btn
     * @param referer
     */
    exports.pubAttr = function (btn) {
        $('.list .listData').on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish
            };

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/product/pubAttr', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    exports.saveAttr = function (max_len) {
        // add click
        $('.addOptionBtn').click(function (e) {
            var len = $('.list .listData .item').length;
            if (len > max_len) {
                base.showTip('err', '最多可以创建' + ( max_len + 1) + '个选项', 2000);
                return;
            }
            // reset
            $('#optionWidget .res-btn').removeAttr('data-id');
            $('.choose-type-selc').show();
            $('#optionWidget .attr_name').val('');
            $('#optionWidget .attr_value').val('');

            base.showPop('#optionPopup');

            e.stopImmediatePropagation();
        });

        // switch
        $('#optionWidget').on('change', '.select-option-type', function () {
            var val = $(this).val();

            $('.option').hide();
            $('#option-' + val).show();
        });

        // confirm
        $('#optionWidget').on('click', '.res-btn', function () {
            var cid = $(this).attr('data-cid');
            var id = $(this).attr('data-id');
            var obj = $('#optionWidget .option:visible');
            var attr_type = obj.attr('data-type');
            var attr_name = obj.find('.attr_name').val();
            var attr_value = obj.find('.attr_value').val();
            var is_active = $('#optionWidget .is_active:checked').val();
            var must = $('#optionWidget .must:checked').val();
            var sort = $('#optionWidget .sort').val();
            var is_sale_prop = $('#optionWidget .is_sale_prop').val();

            if (!checkField(obj.find('.attr_name'), '请填写正确的属性名', /^[\w\u4E00-\u9FA5]{2,18}$/)) {
                obj.find('.attr_name').focus();
                base.showTip('err', '请填写正确的字段名', 3000);
                return false;
            }
            var attr_tmp;
            if (attr_type == 'single') {
                attr_tmp = attr_value.replace(/\s/g, '').split('-');
                attr_tmp = getUniqueArray(attr_tmp);

                if (attr_tmp.length <= 1) {
                    base.showTip('err', '请填写单选选项,并用-隔开', 3000);
                    return false;
                }
                attr_value = attr_tmp.join('-');
            }

            if (attr_type == 'multi') {
                attr_tmp = attr_value.replace(/\s/g, '').split('-');
                attr_tmp = getUniqueArray(attr_tmp);

                if (attr_tmp.length <= 1) {
                    base.showTip('err', '请填写多选选项,并用-隔开', 3000);
                    return false;
                }
                attr_value = attr_tmp.join('-');
            }

            var data = {
                cid: cid,
                attr_type: attr_type,
                attr_name: attr_name,
                attr_value: attr_value,
                sort: sort,
                is_active: is_active,
                is_sale_prop: is_sale_prop,
                must: must
            };

            base.requestApi('/api/product/saveAttr', {data: data, id: id}, function (res) {
                if (res.result == 1) {
                    window.location.reload();
                    base.hidePop('#optionPopup');
                }
            });

        });

        function getUniqueArray(a) {
            var hash = {},
                len = a.length,
                result = [];

            for (var i = 0; i < len; i++) {
                if (!hash[a[i]]) {
                    hash[a[i]] = true;
                    result.push(a[i]);
                }
            }
            return result;
        }

        // update
        $('.list .listData .upBtn').click(function (e) {
            var id = $(this).attr('data-id');
            var attr_type = $(this).attr('data-attr_type');
            var attr_name = $(this).attr('data-attr_name');
            var attr_value = $(this).attr('data-attr_value');
            if (id) {
                base.showPop('#optionPopup');

                $('#optionWidget .res-btn').attr('data-id', id);
                $('.choose-type-selc').hide();
                $('.option').hide();
                var obj = $('#option-' + attr_type);
                obj.find('.attr_name').val(attr_name);
                obj.find('.attr_value').val(attr_value);
                obj.show();
            }

            e.stopImmediatePropagation();
        });

    };
});