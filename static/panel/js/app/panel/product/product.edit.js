define(function (require, exports) {
    var base = require('app/panel/panel.base');
    base.selectNone();
    base.selectCheckbox();
    var editor = require('app/app.editor');
    var store = require('app/panel/panel.storage');
    require('jquery/jquery.dragsort');
    require('jquery/jquery.stickyNavbar.min');
    require('jquery/jquery.easing.min');

    exports.pub = function () {
        $(".list .listData").on('click', '.pubBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish
            };

            // disable the button
            $(this).attr('disabled', true);
            // api request
            base.requestApi('/api/product/publish', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！', 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del post
     * @param btn
     */
    exports.del = function () {
        $(".list .listData").on('click', '.delBtn', function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            del(data);
            e.stopImmediatePropagation();
        });

        $(".list").on('click', '.delAllSelected', function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            del(data);

            e.stopImmediatePropagation();
        });

        function del(data) {
            // api request
            base.requestApi('/api/product/del', {data: data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.list .listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
        }
    };

    // 商品规格设置
    function ItemSpecTable() {

    }

    ItemSpecTable.spec_data = {};
    ItemSpecTable.prototype = {
        init: function (data_spec) {
            var _this = this;
            $('#product-spec').on('click', '.spec-chk', function () {
                _this.genTable();
            });

            if (data_spec) {
                ItemSpecTable.spec_data = data_spec;
                _this.genTable();
            }
        },
        genTable: function () {
            var _this = this;
            // 循环获取
            var data_tmp = [];
            $('.spec-field').each(function () {
                var spec_id = $(this).attr('data-spec_id');
                var spec_index = $(this).attr('data-index');
                var spec_name = $(this).attr('data-spec_name');
                var spec_arr = [];

                $(this).find('.spec-chk:checked').each(function () {
                    var spec_key = $(this).attr('data-spec_key');
                    var spec_val = $(this).attr('data-spec_val');
                    var tmp = {"key": spec_key, "val": spec_val, "spec_id": spec_id, "spec_name": spec_name};
                    spec_arr.push(tmp);
                });

                if (spec_arr.length > 0) {
                    // 排序作用
                    data_tmp[spec_index] = {
                        "spec_id": spec_id,
                        "spec_name": spec_name,
                        "spec_data": spec_arr,
                        "spec_count": spec_arr.length
                    };
                }
            });

            if (data_tmp.length > 0) {
                var data = [];
                // 去除索引，去除空数组。
                for (var n in data_tmp) {
                    if (data_tmp[n]) {
                        data.push(data_tmp[n]);
                    }
                }
                _this.genTHead(data);
                _this.genTBody(data);
            } else {
                $('.spec-list .listHead').html("");
                $('.spec-list .listData').html("");
            }
        },
        genTBody: function (data) {
            var len = data.length;
            var tableColRowData = [];
            var fullColRowData = [];
            var g = 0;
            var sell_price = $('#sell_price').val();
            var original_price = $('#original_price').val();

            if (len == 1) {
                var fst = data[0].spec_data;
                for (var i = 0; i < fst.length; i++) {
                    fullColRowData[g] = [fst[i]];

                    tableColRowData.push([fst[i]]);

                    g++;
                }
            }

            if (len == 2) {
                var sec0 = data[0].spec_data;
                var sec1 = data[1].spec_data;
                var row_flag = 0;

                for (var i0 = 0; i0 < sec0.length; i0++) {
                    for (var i1 = 0; i1 < sec1.length; i1++) {
                        var b0 = sec0[i0];
                        var b1 = sec1[i1];
                        fullColRowData[g] = [b0, b1];

                        b0.rowspan = sec1.length;

                        // 当前行号+spec_id
                        if (row_flag == b0.spec_id + '' + i0) {
                            b0 = '-';
                        }

                        // 设置flag
                        row_flag = b0 == '-' ? row_flag : b0.spec_id + '' + i0;

                        tableColRowData.push([b0, b1]);

                        g++;
                    }
                }
            }

            if (len == 3) {
                var thr0 = data[0].spec_data;
                var thr1 = data[1].spec_data;
                var thr2 = data[2].spec_data;

                var row0_flag = 0;
                var row1_flag = 0;

                for (var j0 = 0; j0 < thr0.length; j0++) {
                    for (var j1 = 0; j1 < thr1.length; j1++) {
                        for (var j2 = 0; j2 < thr2.length; j2++) {
                            var c0 = thr0[j0];
                            var c1 = thr1[j1];
                            var c2 = thr2[j2];
                            fullColRowData[g] = [c0, c1, c2];

                            c1.rowspan = thr2.length;
                            c0.rowspan = thr1.length * thr2.length;

                            // 第一列：当前行号+spec_id
                            if (row0_flag == c0.spec_id + '' + j0) {
                                c0 = '-';
                            }

                            // 第二列：
                            if (row1_flag == c1.spec_id + '' + j0 + '' + j1) {
                                c1 = '-';
                                c1.skip_row = true;
                            }

                            // 设置flag
                            row0_flag = c0 == '-' ? row0_flag : c0.spec_id + '' + j0;
                            row1_flag = c1 == '-' ? row1_flag : c1.spec_id + '' + j0 + '' + j1;

                            tableColRowData.push([c0, c1, c2]);

                            g++;
                        }
                    }
                }
            }

            var strTbody = "";
            for (var n in tableColRowData) {
                var columns = tableColRowData[n];
                var all = fullColRowData[n];
                var strColumn = "";
                var spec_data = []; // spec data group
                var spec_uniq_key = ''; // spec data group

                for (var m in columns) {
                    // 循环
                    spec_data.push({
                        "spec_id": all[m].spec_id,
                        "spec_name": all[m].spec_name,
                        "key": all[m].key,
                        "val": all[m].val
                    });
                    spec_uniq_key += all[m].spec_id + ':' + all[m].key + ';';
                    var spec = columns[m];
                    if (spec != '-') {
                        var rowspan = !isNaN(spec.rowspan) ? 'rowspan="' + spec.rowspan + '"' : "";

                        strColumn += '<td ' + rowspan + ' data-spec_key="' + spec.key + '" data-spec_id="' + spec.spec_id
                        + '" data-spec_val="' + spec.val + '">' + tableColRowData[n][m].val + '</td>';
                    } else {
                        strColumn += '';
                    }
                }

                strTbody += '<tr ' +
                'data-spec_data="' + $.base64.encode(JSON.stringify(spec_data)) + '"  ' +
                'data-spec_uniq_key="' + spec_uniq_key + '"  ' +
                'class="spec">';
                strTbody += strColumn;

                // 初始化原有数据
                var num = '', serial = '', barcode = '';
                if (ItemSpecTable.spec_data) {
                    var spec_item = ItemSpecTable.spec_data[spec_uniq_key];
                    if (spec_item && spec_item != undefined) {
                        sell_price = spec_item.sell_price;
                        original_price = spec_item.original_price;
                        num = spec_item.num;
                        serial = spec_item.serial;
                        barcode = spec_item.barcode;
                    }
                }

                // 最后默认数据
                strTbody += '<td><input type="text" value="' + sell_price + '" class="sell_price" /></td>';
                strTbody += '<td><input type="text" value="' + original_price + '" class="original_price" /></td>';
                strTbody += '<td><input type="text" value="' + num + '" class="num"/></td>';
                strTbody += '<td><input type="text" value="' + serial + '" class="serial"/></td>';
                strTbody += '<td><input type="text" value="' + barcode + '" class="barcode"/></td>';
                strTbody += '</tr>';
            }

            $('.spec-list .listData').html(strTbody);
            this.setChange();
        },

        genTHead: function (data) {
            var strTHead = '';
            for (var i in data) {
                var spec_info = data[i];
                // 表格头
                strTHead += '<th>' + spec_info.spec_name + '</th>';
            }

            strTHead += '<th style="width: 50px">出售价格<span class="red">*</span><th style="width: 50px">原始价格 <span class="red">*</span></th><th style="width: 72px">数量 <span class="red">*</span></th><th style="width: 132px">商家编码</th><th style="width: 132px">商品条形码</th>';
            $('.spec-list .listHead').html(strTHead);
        },
        setChange: function () {
            // sell_price
            $('#specDataList .sell_price').on('change', function (e) {
                var val = $(this).val();

                if (isNaN(val) || val < 0) {
                    $(this).focus();
                    tip.showTip('err', '请正确填写该商品规格的出售价格！', 3000);
                }

                var OPrice = $(this).parent().parent().find('.original_price');
                if (OPrice.val() < val) {
                    OPrice.focus();
                }

                e.stopImmediatePropagation();
            });
            // 原始价格
            $('#specDataList .original_price').on('change', function (e) {
                var val = $(this).val();

                if (isNaN(val) || val < 0) {
                    $(this).focus();
                    tip.showTip('err', '请正确填写该商品规格的原始价格！', 3000);
                }

                var SPrice = $(this).parent().parent().find('.sell_price');
                if (SPrice.val() > val) {
                    SPrice.focus();
                }

                e.stopImmediatePropagation();
            });
            // num
            $('#specDataList .num').on('change', function (e) {

                var val = $(this).val();

                if (isNaN(val) || val < 0) {
                    $(this).focus();
                    tip.showTip('err', '请正确填写该商品规格的数量！', 3000);
                    return;
                }

                var total = 0;
                $('.spec-list .spec .num').each(function () {
                    var num = $(this).val();
                    if (!isNaN(num) && num > 0) {
                        total += parseInt(num);
                    }
                });

                $('#quantity').val(total);

                e.stopImmediatePropagation();
            });
        }
    };

    exports.selectCat = function (product_id) {
        $('#product-area').on('change', '#prod-cat', function (e) {
            var cid = $(this).val();
            if (cid) {
                if (product_id > 0) {
                    var cm = window.confirm("警告：\n更换分类后，所有属性和规格将会消失!!\n您确定需要更换商品分类吗？");
                    if (cm) {
                        window.location.href = '/shop/product/edit?iid=' + product_id + '&cid=' + cid;
                    }
                } else {
                    window.location.href = '/shop/product/edit?cid=' + cid;
                }
            }
            e.stopImmediatePropagation();
        });
    };


    exports.setSpec = function (data_spec) {
        var spec = new ItemSpecTable();
        spec.init(data_spec);
    };

    exports.setPic = function () {
        // 主图
        store.getImg('#uploadMainPic', function (res) {
            $('#thumb').val(res.url);
            $('#thumbPreview').attr('src', res.url);
        });
        // 多图
        store.getImg('#uploadProductPictures', function (res) {

            $('#picturesPreview .clearfix').remove();

            $(res.url).each(function (index, node) {
                var isAdded = $('#picturesPreview').find('img[src="' + node + '"]');
                if (isAdded && isAdded.length > 0) {
                    return false;
                }
                var str = '<p class="pic"><a href="javascript:;" class="setMainPic">设为主图</a><img src="' + node + '"><a href="javascript:;" class="rmBtn">删除</a></p>';
                $('#picturesPreview').append(str);
            });

            $('#picturesPreview').append("<div class='clearfix'></div>");

            reVal();

            $('#picturesPreview').dragsort({
                dragEnd: function () {
                    reVal();
                }
            });

        }, true);

        $('#picturesPreview').dragsort({
            dragEnd: function () {
                reVal();
            }
        });

        // 移除图片
        $("#picturesPreview").on('click', '.rmBtn', function (e) {
            $(this).parent().remove();
            reVal();
            e.stopImmediatePropagation();
        });

        // 设置主图
        $("#picturesPreview").on('click', '.setMainPic', function (e) {
            var src = $(this).parent().find('img').attr('src');
            $('#thumbPreview').attr('src', src);
            $('#thumb').val(src);
            e.stopImmediatePropagation();
        });

        function reVal() {
            var picVal = "";
            $('#picturesPreview .pic img').each(function () {
                picVal += $(this).attr("src") + ';';
            });
            $('#productImages').val(picVal);
        }
    };

    exports.asideNav = function () {
        $('.asideNav').stickyNavbar({
            activeClass: "active", // Class to be added to highlight nav elements
            sectionSelector: "product-widget", // Class of the section that is interconnected with nav links
            animDuration: 350, // Duration of jQuery animation as well as jQuery scrolling duration
            startAt: 0, // Stick the menu at XXXpx from the top of the this() (nav container)
            easing: "swing", // Easing type if jqueryEffects = true, use jQuery Easing plugin to extend easing types - gsgd.co.uk/sandbox/jquery/easing
            animateCSS: true, // AnimateCSS effect on/off
            animateCSSRepeat: false, // Repeat animation everytime user scrolls
            cssAnimation: "fadeInDown", // AnimateCSS class that will be added to selector
            jqueryEffects: false, // jQuery animation on/off
            jqueryAnim: "slideDown", // jQuery animation type: fadeIn, show or slideDown
            selector: "a", // Selector to which activeClass will be added, either "a" or "li"
            mobile: false, // If false, nav will not stick under viewport width of 480px (default) or user defined mobileWidth
            mobileWidth: 480, // The viewport width (without scrollbar) under which stickyNavbar will not be applied (due user usability on mobile)
            zindex: 1, // The zindex value to apply to the element: default 9999, other option is "auto"
            stickyModeClass: "sticky", // Class that will be applied to 'this' in sticky mode
            unstickyModeClass: "unsticky" // Class that will be applied to 'this' in non-sticky mode
        });
    };

    exports.saveProduct = function () {

        editor.init('#txtContent');

        // price
        $('#sell_price').on('change', function (e) {
            var price = $(this).val();
            var cashback_proportion = $(this).attr('data-cashback_proportion');
            if (isNaN(price) || price < 0) {
                $('#product-form #sell_price').focus();
                tip.showTip('err', '请填写正确的商品出售价格', 3000);
                return;
            }

            var original_price = $("#original_price").val();
            if (isNaN(original_price) || price > original_price) {
                $("#original_price").val(price);
            }
            // 商品推广信息
            var cash = price * cashback_proportion / 100;
            $('.tip-share-cash').html(cash);
            if (!$('#cashback_proportion').val()) {
                $('#cashback_proportion').val(cash);
            }
            e.stopImmediatePropagation();
        });

        // price
        $('#original_price').on('change', function (e) {
            var original_price = $(this).val();
            if (isNaN(original_price) || original_price < 0) {
                $('#product-form #sell_price').focus();
                tip.showTip('err', '请填写正确的原始价格', 3000);
                return;
            }

            var price = $("#sell_price").val();
            if (!isNaN(price) && (price > original_price)) {
                tip.showTip('err', '原始价格不得小于出售价格！', 3000);
                $("#original_price").val(price);
            }

            e.stopImmediatePropagation();
        });
        $("#freight").on('change', function () {
            var item = $("#freight option[value=" + $(this).val() + "]").attr('data-item');
            return false;
        });

        $(document).on('click', '.saveBtn', function (e) {
                var data_base = $('#product-form').serializeObject();

                // 验证base
                if (!data_base.name) {
                    $('#product-form input[name="name"]').focus();
                    tip.showTip('err', '请填写商品名称', 3000);
                    return;
                }

                if (!data_base.sell_price) {
                    $('#product-form #sell_price').focus();
                    tip.showTip('err', '请填写正确的商品出售价格', 3000);
                    return;
                }
                if (data_base.freight_tpl == 0) {
                    $('#product-form #freight').focus();
                    tip.showTip('err', '请选择运费模板', 3000);
                    return;
                }
                var valuation_way = $("#freight option[value=" + data_base.freight_tpl + "]").attr('data-type');
                var volumn_weight = $("#" + valuation_way);
                if (valuation_way != 'number' && !(/^[0-9]+(\.[0-9]+)?$/.test(volumn_weight.val()))) {
                    tip.showTip('err', '请根据您选择物流模板的计费方式填写正确的物流参数信息', 3000);
                    volumn_weight.val(volumn_weight.attr('data-val'));
                    volumn_weight.focus();
                    return;

                }


                // 商品属性
                var data_attr = [];
                var attr_checked = true;
                var attr_checked_obj = {};
                $('#product-attr .input').each(function () {
                    var id = $(this).attr("data-id");
                    var type = $(this).attr("data-type");
                    var name = $(this).attr("data-name");
                    var must = $(this).attr("data-must");
                    var attr_key = '';// 单选和多选情况存在
                    // 多选
                    if (type == 'multi') {
                        var chk_data = [];
                        var chk_keys = [];
                        $(this).find('.chk').each(function () {
                            if ($(this).attr('checked')) {
                                chk_data.push($(this).val());
                                chk_keys.push($(this).attr('data-key'))
                            }
                        });

                        if (chk_data.length == 0 && must == 1) {
                            $(this).find('.chk').focus();

                            attr_checked = false;
                            attr_checked_obj = {"name": name, "type": type};
                            return;
                        }

                        if (chk_data.length > 0) {
                            data_attr.push({
                                "attr_id": id,
                                "attr_name": name,
                                "attr_key": chk_keys.join(","),
                                "attr_val": chk_data.join(' '),
                                "attr_type": type
                            });
                        }
                    } else {
                        var val = $(this).val().trim();
                        if (must == 1 && (val == "" || val == null || val == undefined)) {
                            attr_checked = false;
                            $(this).focus();
                            attr_checked_obj = {"name": name, "type": type};
                            return;
                        }

                        if (val != "" && val != null && val != undefined) {
                            if (type == 'single') {
                                attr_key = $(this).find('option:selected').attr('data-attr_key');
                            }
                            data_attr.push({
                                "attr_id": id,
                                "attr_name": name,
                                "attr_key": attr_key,
                                "attr_val": val,
                                "attr_type": type
                            });
                        }
                    }
                });
                // check
                if (attr_checked == false) {
                    tip.showTip('err', '请选择并填写商品属性[ <span class="red">' + attr_checked_obj.name + '</span> ]', 3000);
                    return;
                }

                // 是否商品规格
                var data_spec = [];

                var has_spec = $('#product-form #quantity').attr("data-has_spec");
                if (has_spec == "true") {
                    // 商品规格
                    var spec_checked = true;
                    var spec_checked_obj = {};
                    $('#product-spec .listData .spec').each(function () {
                        var spec_data = JSON.parse($.base64.decode($(this).attr('data-spec_data')));
                        var spec_unique_key = $(this).attr('data-spec_uniq_key');
                        var sell_price = $(this).find('.sell_price').val();
                        var original_price = $(this).find('.original_price').val();
                        var num = $(this).find('.num').val();
                        var serial = $(this).find('.serial').val();
                        var barcode = $(this).find('.barcode').val();

                        if (isNaN(sell_price) || sell_price < 0) {
                            $(this).find('.sell_price').focus();
                            spec_checked_obj = {"name": "出售价格"};
                            spec_checked = false;
                            return;
                        }

                        if (isNaN(original_price) || original_price < 0) {
                            $(this).find('.original_price').focus();
                            spec_checked_obj = {"name": "原始价格"};
                            spec_checked = false;
                            return;
                        }

                        if (parseFloat(sell_price) > parseFloat(original_price)) {
                            $(this).find('.original_price').focus();
                            spec_checked_obj = {"name": "出售价格不得大于原始价格"};
                            spec_checked = false;
                            return;
                        }

                        if (isNaN(num) || num <= 0) {
                            $(this).find('.num').focus();
                            spec_checked_obj = {"name": "数量"};
                            spec_checked = false;
                            return;
                        }

                        var row = {
                            "sell_price": sell_price,
                            "original_price": original_price,
                            "num": num,
                            "serial": serial,
                            "barcode": barcode,
                            "spec_data": spec_data,
                            "spec_unique_key": spec_unique_key
                        };

                        data_spec.push(row);
                    });

                    if (spec_checked == false) {
                        tip.showTip('err', '请设置商品规格属性[ <span class="red">' + spec_checked_obj.name + '</span> ]', 3000);
                        return;
                    }

                    if (data_spec.length == 0) {
                        $(this).find('.num').focus();
                        tip.showTip('err', '请选择商品规格，并进行规格数量和价格设置！', 3000);
                        return;
                    }
                }

                if (!data_base.quantity) {
                    $('#product-form #quantity').focus();
                    tip.showTip('err', '请正确设置商品数量！', 3000);
                    return;
                }

                if (!data_base.thumb) {
                    $('#product-form #thumb').focus();
                    tip.showTip('err', '请填写上传商品图片', 3000);
                    return;
                }

                var product_id = $('#product-form .product_id').val();
                var data = {
                    product_id: product_id,
                    data_base: data_base,
                    data_attr: JSON.stringify(data_attr),
                    data_spec: JSON.stringify(data_spec)
                };

                base.requestApi('/api/product/save', data, function (res) {
                    if (res.result == 1) {
                        window.location.href = res.data;
                    }
                });

                e.stopImmediatePropagation();
            }
        )
        ;
    };
})
;