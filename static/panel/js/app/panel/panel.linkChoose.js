/**
 * 链接选择和设置工具 link setter & choose
 * -- 已支持url链接，电话，微网站选择
 *    -- 微网站选择支持固定页面，栏目导航，文章链接
 * -- 自定义popup实现方式
 *
 *
 * ====== usage ========================================

 *   -- 1. import view tpl
 <?php $this->partial('base/widget/linkChoose'); ?>


 *   -- 2. seajs.use
 seajs.use('app/panel/panel.linkChoose', function (sea) {
    sea.getLink('.getLinkUrl', function (res) {
        console.log(res);
    });
 });

 * @author yanue
 * @time 2014-05-15
 * @version 1.0
 */
define(function (require, exports) {
    require('jquery/jquery.pagination.js');
    require('tools/draggabilly.min.js');
    // 拖拽窗口
    new Draggabilly(document.querySelector('#linkWidget'), { handle: '#linkWidget .popup-head'});

    var base = require('app/panel/panel.base');//公共函数


    var chooseType = '';

    /**
     * 获取或设置链接
     *
     * */
    exports.getLink = function (btn, func) {

        $(btn).click(function () {
            base.showPop('#linkPopup');
        });

        // switch
        $('#linkWidget').on('change', '.select-link-type', function () {
            var val = $(this).val();
            chooseType = val;

            $('.link-module').hide();
            $('#module-' + val).show();
            $('#linkWidget .res-val').val('');

            // 改变窗口大小
            if (chooseType == 'link' || chooseType == 'tel') {
                $('#linkWidget').css({'height': '220px', 'margin-top': '-110px'});
            } else {
                $('#linkWidget').css({'height': '416px', 'margin-top': '-208px'});
            }

        });

        // confirm
        $('#linkWidget').on('click', '.res-btn', function () {
            var val = $('#linkWidget .res-val').val();
            if (!val) {
                $('.res-area .help-inline').show().html('<span class="text-danger"><i class="icon icon-warning-sign"></i>请选择链接类型并填写或选择链接</span>');
                return false;
            } else {
                $('.res-area .help-inline').hide().html('');
            }

            // callback
            if (typeof func == 'function') {
                func(val, chooseType);

                // hide pop
                base.hidePop('#linkPopup');
            }
        });

        typeChooseLink();
        typeChooseWeb();
        typeChooseTel();
        typeChooseShop();
        typeChooseActivity();
        typeChooseBusiness();
    };

    /**
     * 设置普通链接
     *
     */
    function typeChooseLink() {
        $('#module-link').on('change', '#source_url', function () {
            var val = $(this).val();
            if (!new RegExp(/[http|https]:\/\/[^\s]*/).test(val)) {
                $('#module-link .help-inline').html('url地址格式不正确！');
                $(this).focus();
                return false;
            } else {
                $('#linkWidget .res-val').val(val);

                $('#module-link .help-inline').html('<i class="icon icon-ok success"></i>');
            }
        });
    }

    /**
     * 电话功能
     */
    function typeChooseTel() {
        $('#module-tel').on('change', '#tel_number', function () {
            var val = $(this).val();
            if (!new RegExp(/^(1[\d]{10})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{3,7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$/).test(val)) {
                $('#module-tel .help-inline').html('电话号码格式不对！');
                $(this).focus();
                return false;
            } else {
                $('#linkWidget .res-val').val(val);

                $('#module-tel .help-inline').html('<i class="icon icon-ok success"></i>');
            }
        });
    }


    /**
     * 活动
     */
    function typeChooseActivity() {
        $('#module-activity').on('click', '.choose_link', function (e) {
            var val = $(this).val();
            $('#linkWidget .res-val').val(val);

            e.stopImmediatePropagation();
        });

    }

    /**
     * 业务模块
     */
    function typeChooseBusiness() {
        $('#module-business').on('click', '.choose_link', function (e) {
            var val = $(this).val();
            $('#linkWidget .res-val').val(val);

            e.stopImmediatePropagation();
        });

    }

    /**
     * 从微网站选择链接
     *
     */
    function typeChooseWeb() {
        var totalCount = null;

        $('#module-web').on('click', '.choose_web_type', function (e) {
            var val = $(this).attr('data-type');

            if (val == 'article') {
                // get data         * @param type

                getArticleData({p: 0});
                $('#module-web .pagination').show();
            } else {
                $('#module-web .pagination').hide();
            }

            // clean data
            $('#linkWidget .res-val').val('');
            $('#module-web .web-content .nav-list').hide();
            $('#module-web .web-content #' + val).show();

            e.stopImmediatePropagation();
        });

        $('#module-web').on('click', '.choose_link', function (e) {
            var val = $(this).val();
            $('#linkWidget .res-val').val(val);

            e.stopImmediatePropagation();
        });


        /**
         * get article data
         * @param data
         */
        function getArticleData(data) {
            // api request
            base.requestApi('/api/article/get', data, function (res) {
                if (res.error.code == 0) {
                    base.showTip('ok', '获取成功！',1000);

                    var list = res.data.list;
                    var count = res.data.count;
                    var limit = res.data.limit;

                    if (totalCount != count) {
                        pagination(Math.ceil(count / limit), data);
                        totalCount = count;
                    }

                    var str = '';
                    if (list) {
                        for (var i in list) {
                            var item = list[i];
                            str += '<li>';
                            str += '<label>';
                            str += '  <input type="radio" name="single-radio" class="ace choose_link" value="' + item.url + '">';
                            str += '      <span class="lbl">';
                            str += '      ' + item.title + '<small class="right">' + item.date + '</small>';
                            str += '       </span>';
                            str += '  </label>';
                            str += '</li>';
                        }
                    }

                    $('#module-web .nav-list#article').html(str);
                }
            });

        }

        /**
         * 通过Ajax加载分页元素
         *
         * @param total
         * @param data
         */
        function pagination(total, data) {
            // 创建分页
            $('.pagination').pagination(total, {
                num_edge_entries: 1, //边缘页数
                num_display_entries: 6, //主体页数
                items_per_page: 1, //每页显示1项
                link_to: 'javascript:;',
                prev_text: '<i class="icon-double-angle-left"></i>',
                next_text: '<i class="icon-double-angle-right"></i>',
                callback: function (page_index) {
                    data.p = page_index + 1;
                    getArticleData(data);
                }
            });

        }

    }

    /**
     * 从微商城选择链接
     *
     */
    function typeChooseShop() {
        var totalCount = null;

        $('#module-shop').on('click', '.choose_shop_type', function (e) {
            var val = $(this).attr('data-type');

            if (val == 'article') {
                // get data         * @param type

                getItemData({p: 0});
                $('#module-shop .pagination').show();
            } else {
                $('#module-shop .pagination').hide();
            }

            // clean data
            $('#linkWidget .res-val').val('');
            $('#module-shop .shop-content .nav-list').hide();
            $('#module-shop .shop-content #' + val).show();

            e.stopImmediatePropagation();
        });

        $('#module-shop').on('click', '.choose_link', function (e) {
            var val = $(this).val();
            $('#linkWidget .res-val').val(val);

            e.stopImmediatePropagation();
        });


        /**
         * get article data
         * @param data
         */
        function getItemData(data) {
            // api request
            base.requestApi('/api/shop/getItem', data, function (res) {
                if (res.error.code == 0) {
                    base.showTip('ok', '获取成功！',1000);
                    var list = res.data.list;
                    var count = res.data.count;
                    var limit = res.data.limit;

                    if (totalCount != count) {
                        pagination(Math.ceil(count / limit), data);
                        totalCount = count;
                    }

                    var str = '';
                    if (list) {
                        for (var i in list) {
                            var item = list[i];
                            str += '<li>';
                            str += '<label>';
                            str += '  <input type="radio" name="single-radio" class="ace choose_link" value="' + item.url + '">';
                            str += '      <span class="lbl">';
                            str += '      ' + item.name + '<small class="right">' + item.date + '</small>';
                            str += '       </span>';
                            str += '  </label>';
                            str += '</li>';
                        }
                    }

                    $('#module-shop .nav-list#article').html(str);
                }
            });

        }

        /**
         * 通过Ajax加载分页元素
         *
         * @param total
         * @param data
         */
        function pagination(total, data) {
            // 创建分页
            $('.pagination').pagination(total, {
                num_edge_entries: 1, //边缘页数
                num_display_entries: 6, //主体页数
                items_per_page: 1, //每页显示1项
                link_to: 'javascript:;',
                prev_text: '<i class="icon-double-angle-left"></i>',
                next_text: '<i class="icon-double-angle-right"></i>',
                callback: function (page_index) {
                    data.p = page_index + 1;
                    getItemData(data);
                }
            });

        }

    }
});