define(function (require, exports) {

    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var appIcon = require('app/app.icon');
    var color = require('app/app.color');


    exports.backGood = function () {

        $(".receiveGood").on('click', function (e) {
            if (confirm("确实要确认收货吗?")) {
                var id = $(".apply-id").attr('data-id');
                base.requestApi('/api/order/receiveGood', {id: id}, function (res) {

                    if (res.result == 1) {
                        tip.showTip('ok', '确认收货成功', 2000);
                        window.location=window.location;
                    }

                });
                e.stopImmediatePropagation();
            }
        })
        $(".pay").on('click', function (e) {
            if (confirm("确定已经退款了吗?")) {
                var id = $(".apply-id").attr('data-id');
                base.requestApi('/api/order/pay', {id: id}, function (res) {

                    if (res.result == 1) {
                        tip.showTip('ok', '确认退款成功', 2000);
                        window.location=window.location;
                    }

                });
                e.stopImmediatePropagation();
            }
        });
        $(".refuse").on('click', function (e) {
               $("#focusModal").show();
        })
        $(".close").on('click', function (e) {
            $("#focusModal").hide();
        })
        $(".closeBtn").on('click', function (e) {
            $("#focusModal").hide();
        })
        $(".saveBtn").on('click', function (e) {
          var seller_refused_reason= $.trim($("#seller_refused_reason").val());
          var id = $(".apply-id").attr('data-id');
          if(seller_refused_reason=='')
          {
              base.showTip('err','请输入拒绝原因',2000);
              return false;
          }
          base.requestApi('/api/order/refuse', {id: id,reason:seller_refused_reason}, function (res) {

                if (res.result == 1) {
                    window.location=window.location;
                }

            });

        })

        $(".passCheck").on('click', function (e) {
            var id = $(".apply-id").attr('data-id');
            if (confirm("确定同意申请吗?")) {
                base.requestApi('/api/order/passCheck', {id: id}, function (res) {

                    if (res.result == 1) {
                        window.location=window.location;
                    }

                });
            }

        })

        $(".newOrder").on('click', function (e) {
            $(".reason-field").css('display','none');
            $(".saveBtn").css('display','none');
            $(".order-field").css('display','block');
            $(".modal-title").text('新订单号填写');
            $(".saveOrderBtn").css('display','inline-block');
            $("#focusModal").show();
        })
        $(".saveOrderBtn").on('click', function (e) {
          var order_number= $.trim($("#back_order_number").val());
          var id = $(".apply-id").attr('data-id');
           if(order_number=='')
           {
               base.tip('err','订单号不能为空',2000);return false;
           }
            base.requestApi('/api/order/newOrder', {id: id,'order_number':order_number}, function (res) {

                if (res.result == 1) {
                    window.location=window.location;
                }

            });
        })
        $(".finish").on('click', function (e) {
            var id = $(".apply-id").attr('data-id');
            base.requestApi('/api/order/finish', {id: id}, function (res) {

                if (res.result == 1) {
                    window.location=window.location;
                }

            });



        })
    };

});