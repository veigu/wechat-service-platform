define(function (require, exports) {

    window.inAjaxProcess = false;

    exports.requestApi = function (uri, data, func) {
        var param = $.extend({}, ajaxparam(), {
            url: uri,
            async: true,
            data: data,
            success: function (res) {
                if (typeof func == 'function') {
                    func(res);
                }
                if (res.result == 0) {
                    showErrorCode(res.error);
                }
            }
        });

        $.ajax(param);
    };

    // status : err, ok, wait, warming
    exports.showTip = function (status, tip, time) {
        var t = new ajaxTip();
        t.showTip(status, tip, time);
    };

    exports.hideTip = function () {
        var t = new ajaxTip();
        t.hideTip();
    };

    /**
     * ajax tip
     *
     * @constructor
     */

    function ajaxTip() {

    }

    ajaxTip.ajaxTimer = null;
    ajaxTip.tip = null;
    ajaxTip.time = 0;
    ajaxTip.status = null;
    ajaxTip.prototype = {
        showTip: function (status, tip, time) {
            ajaxTip.status = status;
            ajaxTip.tip = tip;
            ajaxTip.time = time;

            $('#ajaxStatus').show();
            $('#ajaxStatus #ajaxTip').html(ajaxTip.tip).removeClass().addClass(ajaxTip.status);


            if (ajaxTip.time) {
                if (ajaxTip.ajaxTimer) {
                    clearTimeout(ajaxTip.ajaxTimer);
                }
                ajaxTip.ajaxTimer = setTimeout(function () {
                    $('#ajaxStatus').hide();
                    ajaxTip.inProcess = true;
                }, ajaxTip.time);
            }
        },
        hideTip: function () {
            $('#ajaxStatus').hide();
        }
    };
    window.tip = new ajaxTip();

    // popup basic show hide
    exports.showPop = function (popElem) {

        var pop = new popup(popElem);
        pop.show();


    };


    // popup basic show hide
    exports.hidePop = function (popElem) {
        var pop = new popup(popElem);
        pop.hide();
    };


    // popup
    function popup(popElem) {

        this.widget = popElem ? popElem : '.popup-wrap';

        var _this = this;
        // close click
        $('.popup-widget .popup-close').on('click', function () {
            _this.hide();
        });

        // bg click
        $('.popup-wrap').on('click', function (e) {
            if (e.target !== this) return;

            _this.hide();

            e.stopImmediatePropagation();
        });
    }

    popup.prototype = {
        'show': function () {
            $(this.widget).fadeIn();
        },
        'hide': function () {
            $('.popup-wrap').hide();
        }
    };

    //ajax 请求公用参数
    function ajaxparam(btn) {

        var param = {
            dataType: 'json',
            type: 'post',
            beforeSend: function () {
                tip.showTip('wait', '处理请求...');
                if (inAjaxProcess == true) {
                    tip.showTip('wait', '正在请求...');
                    return false;
                }
                // 正在处理状态
                inAjaxProcess = true;
            },
            timeout: function () {
                tip.showTip('err', '请求超时,请重试！', 3000);
            },
            abort: function () {
                tip.showTip('err', '网路连接被中断！', 3000);
            },
            parsererror: function () {
                tip.showTip('err', '运行时发生错误！', 3000);
            },
            complete: function () {
                setTimeout(function () {
                    if ($('#ajaxStatus').css('display') !== 'none') {
                        tip.showTip('ok', '操作成功！', 3000);
                    }
                    // 清除处理状态
                    inAjaxProcess = false;
                }, ajaxTip.time);// 最后一次tip时间
            },
            error: function () {
                $(btn).attr('disabled', false);
                tip.showTip('err', '内部程序错误！', 3000);
            }
        };
        return param;
    }

    function showErrorCode(err) {
        var tip = new ajaxTip();
        tip.showTip('err', err.msg || err.more, 3000);
    }


    // all select status
    exports.selectCheckbox = function () {
        // tr click
        $('.list .listData .item .chk').live('click', function (e) {
            var chk_id = $(this).attr('data-id');

            if ($(this).prop('checked') == true || $(this).prop('checked') == 'checked') {
                $(this).parents('.item[data-id=' + chk_id + ']').addClass('selected');
            } else {
                $(this).parents('.item[data-id=' + chk_id + ']').removeClass('selected');
            }

            e.stopImmediatePropagation();
        });

        // checkbox click
        $('.list .listData .item').live('click', function (e) {
            var chk_id = $(this).find('.chk').attr('data-id');
            if ($(this).find('.chk').prop('checked') == true || $(this).find('.chk').prop('checked') == 'checked') {
                $(this).removeClass('selected');
                $(this).find('.chk').attr('checked', false);
            } else {
                $(this).addClass('selected');
                $(this).find('.chk').attr('checked', true);
            }
        });

        // 全选
        $(".selectAll").live('click', function () {
            $(this).addClass('current');
            $(this).siblings('.btn-light').removeClass('current');
            $(".list .listData .item input.chk").attr("checked", true);
            $('.list .listData .item').addClass('selected');
        });
        // 全不选
        $(".selectNone").live('click', function () {
            $(this).siblings('.btn-light').removeClass('current');
            $(".list .listData .item input.chk").attr("checked", false);
            $('.list .listData .item').removeClass('selected');
        });
        // 反选
        $(".selectInvert").live('click', function () {
            $(this).addClass('current');
            $(this).siblings('.btn-light').removeClass('current');
            $(".list .listData .item input.chk").each(function () {
                $(this).attr("checked", !this.checked);//反选
                var chk_id = $(this).attr('data-id')
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    $(this).parents('.item[data-id=' + chk_id + ']').addClass('selected');
                } else {
                    $(this).parents('.item[data-id=' + chk_id + ']').removeClass('selected');
                }
            });
        });
    };

    // all select status
    exports.selectNone = function () {
        // 全不选
        $(".list .listData input.chk").attr("checked", false);
        $('.list .listData .item').removeClass('selected');
    };

    // go to the input page
    exports.goPage = function (btn) {
        $(btn).live('click', function () {
            var page = parseInt($(this).siblings('.pageVal').val());
            var requestSting = $(this).attr('data-string');
            var suffix = $(this).attr('data-suffix');
            var total = $(this).attr('data-total');
            page = isNaN(page) ? 1 : page;
            page = page >= total ? total : page;
            page = page <= 1 ? 1 : page;
            var url = $(this).attr('data-url') + '/p/' + page + suffix;
            url = requestSting ? url + '?' + requestSting : url;
            window.location.href = url;
        });
    };

    window.checkField = function (elem, msg, regx) {
        var val = $(elem).val();
        var dmsg = $(elem).attr('placeholder');

        if ($(elem).val() == '') {
            $(elem).siblings('.tip').show().addClass('err');
            $(elem).siblings('.tip').html(dmsg);
            return false;
        } else {
            if (regx) {
                if (!new RegExp(regx).test(val)) {
                    $(elem).siblings('.tip').show().addClass('err');
                    $(elem).siblings('.tip').html(msg || dmsg);
                    return false;
                } else {
                    $(elem).siblings('.tip').hide().removeClass('err');
                    $(elem).siblings('.tip').html('');
                }
            }
            $(elem).siblings('.tip').hide().removeClass('err');
            $(elem).siblings('.tip').html('');
        }

        return true;
    };


});

// 序列化插件
(function ($) {
    $.fn.serializeObject = function () {

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push": /^$/,
                "fixed": /^\d+$/,
                "named": /^[a-zA-Z0-9_]+$/
            };


        this.build = function (base, key, value) {
            base[key] = value;
            return base;
        };

        this.push_counter = function (key) {
            if (push_counters[key] === undefined) {
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function () {

            // skip invalid keys
            if (!patterns.validate.test(this.name)) {
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while ((k = keys.pop()) !== undefined) {

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if (k.match(patterns.push)) {
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if (k.match(patterns.fixed)) {
                    merge = self.build([], k, merge);
                }

                // named
                else if (k.match(patterns.named)) {
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);
/**
 * Implements JSON stringify and parse functions
 * v1.0
 *
 * By Craig Buckler, Optimalworks.net
 *
 * As featured on SitePoint.com
 * Please use as you wish at your own risk.
 *
 * Usage:
 *
 * // serialize a JavaScript object to a JSON string
 * var str = JSON.stringify(object);
 *
 * // de-serialize a JSON string to a JavaScript object
 * var obj = JSON.parse(str);
 */

var JSON = JSON || {};

// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {

    var t = typeof (obj);
    if (t != "object" || obj === null) {

        // simple data type
        if (t == "string") obj = '"' + obj + '"';
        return String(obj);

    }
    else {

        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);

        for (n in obj) {
            v = obj[n];
            t = typeof(v);

            if (t == "string") v = '"' + v + '"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);

            json.push((arr ? "" : '"' + n + '":') + String(v));
        }

        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};


// implement JSON.parse de-serialization
JSON.parse = JSON.parse || function (str) {
    if (str === "") str = '""';
    eval("var p=" + str + ";");
    return p;
};

/**
 * base64
 * Usage
 $.base64.encode( "this is a test" ) returns "dGhpcyBpcyBhIHRlc3Q="
 $.base64.decode( "dGhpcyBpcyBhIHRlc3Q=" ) returns "this is a test"
 */
!function(a){"use strict";var d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,b=a.base64,c="2.1.4";"undefined"!=typeof module&&module.exports&&(d=require("buffer").Buffer),e="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",f=function(a){var c,d,b={};for(c=0,d=a.length;d>c;c++)b[a.charAt(c)]=c;return b}(e),g=String.fromCharCode,h=function(a){var b;return a.length<2?(b=a.charCodeAt(0),128>b?a:2048>b?g(192|b>>>6)+g(128|63&b):g(224|15&b>>>12)+g(128|63&b>>>6)+g(128|63&b)):(b=65536+1024*(a.charCodeAt(0)-55296)+(a.charCodeAt(1)-56320),g(240|7&b>>>18)+g(128|63&b>>>12)+g(128|63&b>>>6)+g(128|63&b))},i=/[\uD800-\uDBFF][\uDC00-\uDFFFF]|[^\x00-\x7F]/g,j=function(a){return a.replace(i,h)},k=function(a){var b=[0,2,1][a.length%3],c=a.charCodeAt(0)<<16|(a.length>1?a.charCodeAt(1):0)<<8|(a.length>2?a.charCodeAt(2):0),d=[e.charAt(c>>>18),e.charAt(63&c>>>12),b>=2?"=":e.charAt(63&c>>>6),b>=1?"=":e.charAt(63&c)];return d.join("")},l=a.btoa?function(b){return a.btoa(b)}:function(a){return a.replace(/[\s\S]{1,3}/g,k)},m=d?function(a){return new d(a).toString("base64")}:function(a){return l(j(a))},n=function(a,b){return b?m(a).replace(/[+\/]/g,function(a){return"+"==a?"-":"_"}).replace(/=/g,""):m(a)},o=function(a){return n(a,!0)},p=new RegExp(["[À-ß][-¿]","[à-ï][-¿]{2}","[ð-÷][-¿]{3}"].join("|"),"g"),q=function(a){switch(a.length){case 4:var b=(7&a.charCodeAt(0))<<18|(63&a.charCodeAt(1))<<12|(63&a.charCodeAt(2))<<6|63&a.charCodeAt(3),c=b-65536;return g((c>>>10)+55296)+g((1023&c)+56320);case 3:return g((15&a.charCodeAt(0))<<12|(63&a.charCodeAt(1))<<6|63&a.charCodeAt(2));default:return g((31&a.charCodeAt(0))<<6|63&a.charCodeAt(1))}},r=function(a){return a.replace(p,q)},s=function(a){var b=a.length,c=b%4,d=(b>0?f[a.charAt(0)]<<18:0)|(b>1?f[a.charAt(1)]<<12:0)|(b>2?f[a.charAt(2)]<<6:0)|(b>3?f[a.charAt(3)]:0),e=[g(d>>>16),g(255&d>>>8),g(255&d)];return e.length-=[0,0,2,1][c],e.join("")},t=a.atob?function(b){return a.atob(b)}:function(a){return a.replace(/[\s\S]{1,4}/g,s)},u=d?function(a){return new d(a,"base64").toString()}:function(a){return r(t(a))},v=function(a){return u(a.replace(/[-_]/g,function(a){return"-"==a?"+":"/"}).replace(/[^A-Za-z0-9\+\/]/g,""))},w=function(){var c=a.base64;return a.base64=b,c},a.base64={VERSION:c,atob:t,btoa:l,fromBase64:v,toBase64:n,utob:j,encode:n,encodeURI:o,btou:r,decode:v,noConflict:w},"function"==typeof Object.defineProperty&&(x=function(a){return{value:a,enumerable:!1,writable:!0,configurable:!0}},a.base64.extendString=function(){Object.defineProperty(String.prototype,"fromBase64",x(function(){return v(this)})),Object.defineProperty(String.prototype,"toBase64",x(function(a){return n(this,a)})),Object.defineProperty(String.prototype,"toBase64URI",x(function(){return n(this,!0)}))})}($);
