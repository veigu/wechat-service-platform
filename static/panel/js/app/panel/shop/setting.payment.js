//BUI.use('common/page');

$(document).ready(function (e) {

    $('.item-edit').click(function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }

        var data = JSON.parse($(this).attr('data-data'));

        $('#payment_name').html(data.name);

        $('#payment_key').val(data.key);

        if (data.is_active > 0) {
            $('#is_active').attr('checked', true);
            $('#is_not_active').attr('checked', false);
        }
        else {
            $('#is_active').attr('checked', false);
            $('#is_not_active').attr('checked', true);
        }

        if (data.use_platform > 0) {
            $('#is_use_platform').attr('checked', true);
            $('#is_not_use_platform').attr('checked', false);
            $('#config-div').hide();
        }
        else {
            $('#is_use_platform').attr('checked', false);
            $('#is_not_use_platform').attr('checked', true);
            loadConfig(data.key);
            $('#config-div').show();
        }
        $('#paymentModal').modal("show");
    });

    function loadConfig(type) {
        $('#ajaxTip').html("数据加载中，请稍候……！");
        $('#ajaxStatus').show();
        $.ajax({
            url: "/shop/payment/paymentDetail",
            dataType: 'json',
            type: 'post',
            data: {
                type: type
            }
        }).done(function (data) {
            $('#ajaxTip').html("数据加载成功！");
            setTimeout(function () {
                $('#ajaxStatus').hide();
            }, 1000);
            var config_items = data.result;
            if (config_items && config_items.length > 0) {
                $('#config-div').html('');
                $(config_items).each(function (index, item) {
                    var itemHtml = '<div class="form-group">' +
                        '<label for="' + item.meta_name + '" class="col-sm-2 control-label no-padding-right">' + item.meta_label + '</label>' +
                        '<span class="col-sm-10">';
                    if (item.type == 'radio') {
                        $(item.value_list).each(function (i, n) {
                            var is_checked = "";
                            if (n.value == item.config_value || ((!item.config_value || item.config_value.length == 0) && n.value == item.default)) {
                                is_checked = "checked"
                            }
                            itemHtml += '<label class="inline"><input name="' + item.meta_name + '" id="' + item.meta_name + '" type="radio" required="" value="' + n.value + '" data-error="参数必须填写" placeholder="' + n.label + '" ' + is_checked + '>' + n.label + '</label>';
                        });
                    }
                    else if (item.type == 'checkbox') {
                        $(item.value_list).each(function (i, n) {
                            var is_checked = "";
                            if (n.value == item.config_value || ((!item.config_value || item.config_value.length == 0) && n.value == item.default)) {
                                is_checked = "checked"
                            }
                            itemHtml += '<label class="inline" ><input name="' + item.meta_name + '" id="' + item.meta_name + '" type="checkbox" required="" value="' + n.value + '" data-error="参数必须填写" placeholder="' + n.label + '" ' + is_checked + '>' + n.label + '</label>';
                        });
                    }
                    else {
                        itemHtml += '<input name="' + item.meta_name + '" id="' + item.meta_name + '" class="col-xs-10 col-sm-5 form-control" required="" value="' + item.config_value + '" data-error="参数必须填写" placeholder="' + item.meta_label + '">';
                    }

                    itemHtml += '<span class="help-block with-errors"></span>' +
                    '</span></div>';

                    $('#config-div').append($(itemHtml));
                });
                $('#config-div').show();
            }
        }).error(function () {
            $('#ajaxTip').html("数据加载失败！");
            setTimeout(function () {
                $('#ajaxStatus').hide();
            }, 1000);
        });
    }

    $('#is_use_platform').click(function () {
        $('#config-div').hide();
    });
    $('#is_not_use_platform').click(function () {
        loadConfig($('#payment_key').val())
    });

    $('#saveConfig').click(function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }

        $('#ajaxTip').html("设置保存中，请稍候……！");
        $('#ajaxStatus').show();
        $('#payment_form').ajaxSubmit({
            url: '/shop/payment/paymentConfigSave',
            type: 'post',
            dataType: 'json',
            success: function (data, statusText) {
                if (data.code && data.code > 0) {
                    $('#ajaxTip').html(data.message);
                    setTimeout(function () {
                        $('#ajaxStatus').hide();
                    }, 3000);
                }
                else {
                    $('#ajaxTip').html("保存设置成功！");
                    setTimeout(function () {
                        $('#ajaxStatus').hide();
                    }, 1000);
                    window.location.href = window.location.href;
                }
            },
            error: function () {
                $('#ajaxTip').html("保存设置失败，请联系官方客服解决，为此给您带来的不便，我们深表歉意！谢谢您的理解和支持。");
                setTimeout(function () {
                    $('#ajaxStatus').hide();
                }, 1000);
            }
        });

        $('#paymentModal').modal('hide');
    });
});