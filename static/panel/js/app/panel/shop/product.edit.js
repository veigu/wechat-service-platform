/**
 * Created by wgwang on 14-4-15.
 */
"use strict";

define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');
    var editor = require('app/app.editor');
    base.selectNone();
    base.selectCheckbox();
    exports.init = function() {
        $('#base').find(".chosen-select").chosen();
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // activated tab
            e.relatedTarget // previous tab
            var target = $(e.target).attr('href');
            if(target) {
                $(target).find(".chosen-select").chosen();
                /*$(target).find(".chosen-select").css('width','200px').select2({allowClear:true})
                 .on('change', function(){
                 $(this).closest('form').validate().element($(this));
                 });*/
            }
        });

        store.getImg('#uploadThumb', function (res) {
            $('.img').val(res.url);
            $('.preview').attr('src', res.url);
        });
        store.getImg('#uploadMorePictures', function (res) {
//            $('.img').val(res.url);
//            $('.preview').attr('src', res.url);
            $(res.url).each(function(index, node) {
                var isAdded = $('#picturesPreview').find('img[src="' + node + '"]');
                if(isAdded && isAdded.length > 0) {
                    return false;
                }
                $('#picturesPreview').append($("<li/>").append($('<img/>').attr('src', node)).append($('<a/>').attr('href', '#').addClass('pic-remove').text('删除')));
                var productImages = $('#productImages').val();
                productImages += node + ',';
                $('#productImages').val(productImages);
            });

        }, true);
        editor.init("#txtContent");

        exports.attributeSettings();
        exports.optionSetting();
        exports.saveData();
        exports.gallery();
        initialize();
    };

    exports.attributeSettings = function() {
        $('#addAttributeBtn').click(function(e){
            if(e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }
            var attrId = $('#addAttributeItem').val();
            var data = $('body').data('attribute_data_json');
            $(data).each(function(index, node) {
                if(node.id == attrId) {
                    var isAdded = $('tr[data-attr-id="' + attrId + '"]');
                    if(isAdded && isAdded.length > 0) {
                        return false;
                    }

                    $('#attributeListTable').append(
                        $('<tr/>')
                            .attr('data-attr-id', attrId)
                            .append($('<td/>')
                                .text(node.name)
                            )
                            .append(
                                $('<td/>')
                                    .append($('<input/>')
                                        .attr('id', 'attribute_value_' + attrId)
                                        .attr('data-attr-id', attrId)
                                        .addClass('col-sm-6')
                                    )
                            )
                            .append($('<td/>')
                                .append($('<a/>')
                                    .addClass('attribute-remove btn btn-warning btn-sm')
                                    .attr('href', '#')
                                    .attr('data-attr-id', attrId)
                                    .append($('<i/>')
                                        .addClass('icon-trash')
                                    )
                                    .attr('title', "删除")
                                    .append("删除")
                                )
                            )
                    );
                    $('#addAttributeItem').find('[value=' + attrId + ']').attr('disabled', true);
                    $('#addAttributeItem_chosen').find('.chosen_results li');
                    return true;
                }
            });
            $('#addAttributeItem').val('');
        });

        $('#attributeListTable').delegate('.attribute-remove', 'click', function(e) {
            if(e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }
            var attrId = $(this).attr('data-attr-id');
            if(attrId) {
                $('tr[data-attr-id=' + attrId + ']').remove();
                $('#attrDataList').find('[value='+attrId+']').attr('disabled', false);
            }
        });
        //属性设置 end

        exports.optionSetting = function() {
            // 选项设置 start
            $('#addOptionBtn').click(function(e){
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
                var optionId = $('#addOptionItem').val();
                var isAdded = $('#optionListTable').find("tr[data-option-id='" + optionId + "']");
                if(isAdded && isAdded.length > 0) {
                    return false;
                }
                $('#addOptionItem').val('');
                var data = $('body').data('option_data_json');
                $(data).each(function(index, node) {
                    if(node.id == optionId) {
                        $('#optionListTable').append(
                            $('<tr/>')
                                .attr('data-option-id', optionId)
                                .append($('<td/>')
                                    .text(node.name)
                                )
                                /*.append(
                                    $('<td/>')
                                        .append($('<input/>')
                                            .attr('type', 'checkbox')
                                            .attr('checked', true)
                                            .attr('id', 'option_required_' + optionId)
                                            .addClass('ace ace-switch ace-switch-6 col-sm-2')
                                        )
                                        .append('<span class="lbl"></span>')
                                )*/
                                .append(
                                    $('<td/>')
                                        .attr('data-option-id', optionId)
                                        .append($('<ul/>')
                                            .attr('id', 'option_values_item_list_' + optionId)
                                            .addClass('list list-inline list-unstyled')
                                            .html("无")
                                        )
                                )
                                .append($('<td/>')
                                    .append($("<span/>")
                                        .addClass("btn-group")
                                        .append($('<a/>')
                                            .addClass('option-remove btn btn-danger btn-sm')
                                            .attr('href', '#')
                                            .attr('data-option-id', optionId)
                                            .append($('<i/>')
                                                .addClass('icon-trash')
                                            )
                                            .attr('title', "删除")
                                        )
                                        .append($('<a/>')
                                            .addClass('option-values  btn btn-primary btn-sm')
                                            .attr('href', '#')
                                            .attr('data-toggle', 'modal')
                                            .attr('data-target', '#optionValuesModal')
                                            .attr('data-option-id', optionId)
                                            .append($('<i/>')
                                                .addClass('icon-pencil')
                                            )
                                            .attr('title', "编辑选项")
                                        )
                                    )
                                )
                        );
                        $('#optionDataList').find('[value=' + optionId + ']').attr('disabled', true);
                        return true;
                    }
                });
                $('#addOptionItem').val('');
            });

            $('#optionListTable').delegate('.option-remove', 'click', function(e) {
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
                var attrId = $(this).attr('data-option-id');
                if(attrId) {
                    $('tr[data-option-id=' + attrId + ']').remove();
                    $('#optionDataList').find('[value='+attrId+']').attr('disabled', false);
                }
            });

            $('#optionListTable').delegate('.option-values', 'click', function(e) {
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
                var optionId = $(this).attr('data-option-id');
                $('body').data('current_option', optionId);
                var productOptionValues = $('body').data('product_option_values_' + optionId);
                var renderedValueIds = [];
                //render
                if(productOptionValues && productOptionValues.length > 0) {
                    var productOptionValuesHtml = "";
                    $(productOptionValues).each(function(index, node) {
                        var detailData = node.detail;
                        var subtracted = detailData.subtract == 'on' ? 'checked="true"' : "";
                        productOptionValuesHtml += "<tr data-option-value-id='" + node.id + "'>"
                            + "<td>" + node.value +"</td>"
//                            + "<td><input id='quantity_" + optionId + "_" + node.id + "' class='col-sm-6' value='" + detailData.quantity + "'/></td>"
//                            + "<td><input type='checkbox' class='ace ace-switch ace-switch-6 col-sm-6' id='subtract_" + optionId + "_" + node.id + "' " + subtracted + "/><span class=\"lbl\"></span></td>"
//                            + "<td>"
//                            + "<select class='col-sm-6' id='price_prefix_" + optionId + "_" + node.id + "'>" +
//                            "<option value='+' " + (detailData.price_prefix == '+' ? "selected" : "") + ">+</option>" +
//                            "<option value='-' " + (detailData.price_prefix == '-' ? "selected" : "") + ">-</option></select>"
//                            + "<input id='price_" + optionId + "_" + node.id + "' class='col-sm-6' value='" + detailData.price + "'/>"
//                            + "</td>"
                            + "<td>"
                            + "<a class='option-value-remove btn btn-danger btn-sm' data-option-value-id='" + node.id + "'><i class='icon-trash'></i></a>"
                            + "</td>"
                            + "</tr>";
                        renderedValueIds.push(node.id);
                    });
                    $('#option_values_list_table').empty().html(productOptionValuesHtml);
                }
                else {
                    $('#option_values_list_table').empty()
                }

                var optionValues = $('body').data('option_values_' + optionId);
                if(!optionValues) {
                    var optionData = $('body').data('option_data_json');
                    if(optionData) {
                        $(optionData).each(function(index, node) {
                            if(node.id == optionId) {
                                optionValues = node.values;
                                $('body').data('option_values_' + optionId, optionValues);
                                return true;
                            }
                        });
                    }
                }
                if(optionValues) {
                    var optionValueDataListHtml = "";
                    $(optionValues).each(function(index, node) {
                        optionValueDataListHtml += "<option value='" + node.id + "'";
                        if(renderedValueIds && renderedValueIds.length > 0 && jQuery.inArray(node.id, renderedValueIds) > -1) {
                            optionValueDataListHtml += " disabled ";
                        }
                        optionValueDataListHtml += ">" + node.value + "</option>";
                    });
                    $('#optionValueSelected').html(optionValueDataListHtml);
                }
            });
            $('#addOptionValueBtn').click(function(e) {
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }

                var currentOption = $('body').data('current_option');
                var valueId = $('#optionValueSelected').val();
                $('#optionValueSelected').val('');
                if(currentOption && valueId) {
                    var values = $('body').data('option_values_' + currentOption);
                    if(values) {
                        var isAdded = $('tr[data-option-value-id="' + valueId + '"]')
                        if(isAdded && isAdded.length > 0) {
                            return false;
                        }
                        var selectOptions = "";
                        $(values).each(function(index, node) {
                            if(node.id == valueId) {
                                selectOptions = node.value;
                                var productOptionValues = $('body').data('product_option_values_' + valueId);
                                if(!productOptionValues) {
                                    productOptionValues = [];
                                }
                                productOptionValues.push(node);
                            }
                        });
                        var returnHtml = "<tr data-option-value-id='" + valueId + "'>"
                            + "<td>" + selectOptions +"</td>"
//                            + "<td><input id='quantity_" + currentOption + "_" + valueId + "' class='col-sm-6' value='0'/></td>"
//                            + "<td><input type='checkbox' class='ace ace-switch ace-switch-6 col-sm-6' id='subtract_" + currentOption + "_" + valueId + "' checked=true/><span class='lbl'></span></td>"
//                            + "<td>"
//                            + "<select class='col-sm-6' id='price_prefix_" + currentOption + "_" + valueId + "'><option value='+'>+</option><option value='-'>-</option></select>"
//                            + "<input id='price_" + currentOption + "_" + valueId + "' class='col-sm-6' value='0'/>"
//                            + "</td>"
                            + "<td>"
                            + "<a class='option-value-remove btn btn-danger btn-sm' data-option-value-id='" + valueId + "'><i class='icon-trash'></i></a>"
                            + "</td>"
                            + "</tr>";

                        $('#option_values_list_table').append($(returnHtml));
                        $('#optionValueSelected').find('[value='+ valueId +']').attr('disabled', true);
                    }
                    else {
                        $('#option_values_list_table').html('');
                    }
                }
            });

            $('#option_values_list_table').delegate('.option-value-remove', 'click', function(e) {
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
                var optionValueId = $(this).attr('data-option-value-id');
                if(optionValueId) {
                    $('#option_values_list_table tr[data-option-value-id='+ optionValueId +']').remove();
                    var currentOption = $('body').data('current_option');
                    var optionValues = $('body').data('product_option_values_' + currentOption);
                    $(optionValues).each(function(index, node) {
                        if(node.id == optionValueId) {
                            delete optionValues[index];
                            $('body').data('product_option_values_' + currentOption, optionValues);
                            $('#optionValueSelected').find("option[value=" + optionValueId + "]").attr('disabled', false);
                            return true;
                        }
                    });
                }
                $('#optionValueDataList').find('[value='+ optionValueId +']').attr('disabled', false);
            });

            //save the data into memory
            $('#saveOptionValuesBtn').click(function(e) {
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }

                var currentOption = $('body').data('current_option');
                var optionValues = $('body').data('option_values_' + currentOption);
                var valueList = $('#optionValueSelected').find('option');
                var productOptionValues = [];
                var listItemHtml = "";

                if(valueList && valueList.length > 0) {
                    $(valueList).each(function(index, node) {
                        var disabled = $(this).attr('disabled');
                        if(disabled && disabled != false) {
                            var valueId = $(this).attr('value');
                            var valueItem = {};
                            for(var i in optionValues) {
                                var n = optionValues[i];
                                if(n.id == valueId) {
                                    valueItem = n;
                                    break;
                                }
                            }

                            var subtract_status = $('#subtract_' + currentOption + "_" + valueId).attr('checked');

                            var itemData = {};
                            itemData.id = valueId;
                            itemData.quantity = $('#quantity_' + currentOption + "_" + valueId).val();
                            itemData.subtract = subtract_status ? 'on' : '';
                            itemData.price = $('#price_' + currentOption + "_" + valueId).val();
                            itemData.price_prefix = $('#price_prefix_' + currentOption + "_" + valueId).val();
                            itemData.points = $('#points_' + currentOption + "_" + valueId).val();
                            itemData.points_prefix = $('#points_prefix_' + currentOption + "_" + valueId).val();
                            itemData.weight = $('#weight_' + currentOption + "_" + valueId).val();
                            itemData.weight_prefix = $('#weight_prefix_' + currentOption + "_" + valueId).val();

                            valueItem.detail = itemData;
                            productOptionValues.push(valueItem);

                            listItemHtml += "<li>" + valueItem.value + "</li>";
                        }
                    });
                }
                $('body').data('product_option_values_' + currentOption, productOptionValues);
                $('#option_values_item_list_' + currentOption).html(listItemHtml);
                $('#optionValuesModal').modal('hide');
            });

            //选项设置 end
        };

        exports.pointSetting = function() {
            //积分设置 start
            $('#addUserPointsBtn').click(function(e) {
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
                var gradeId = $('#selectedUserGrade').val();
                var gradesData = $('body').data('user_grade_data_json');
                var gradeData = {};
                for(var index in gradesData) {
                    var grade = gradesData[index];
                    if(grade.id == gradeId) {
                        gradeData = grade;
                        break;
                    }
                }
                $('#selectedUserGrade').val('');
                $('#userGradeDataList').find('[value=' + gradeId + ']').attr('disabled', true);

                $('#user_grade_points_table').append($('<tr/>')
                    .attr('data-points-grade-id', gradeId)
                    .append($('<td/>')
                        .append(gradeData.name)
                    )
                    .append($('<td/>')
                        .append($('<input/>')
                            .attr('id', 'product_user_points_' + gradeId)
                            .attr('value', 0)
                        )
                    )
                    .append($('<td/>')
                        .append($('<a/>')
                            .attr('data-points-grade-id', gradeId)
                            .attr('href', '#')
                            .addClass('point-item-remove')
                            .append($("<i/>")
                                .addClass('icon-trash')
                            )
                        )
                    )
                );
            });

            $('#user_grade_points_table').delegate('.point-item-remove', 'click', function() {
                var gradeId = $(this).attr('data-points-grade-id');
                if(gradeId) {
                    $('#user_grade_points_table').find('tr[data-points-grade-id=' + gradeId + ']').remove();
                    $('#userGradeDataList').find('[value=' + gradeId + ']').attr('disabled', false);
                }
            });

            //积分设置 end
        };

        exports.gallery = function() {
            //商品相册 start
            $('#picturesPreview').delegate('.pic-remove', 'click', function(e) {
                if(e.preventDefault) {
                    e.preventDefault();
                }
                else {
                    e.returnValue = false;
                }
                var image = $(this).siblings().first().attr('src');
                $(this).parent().remove();
                var productImages = $('#productImages').val();
                var suffixChar = productImages.charAt(productImages.length - 1);
                if(suffixChar == ',') {
                    productImages = productImages.substr(0, productImages.length - 1);
                }
                var tempPicArr = productImages.split(',');
                productImages = [];
                for(var index in tempPicArr) {
                    var item = tempPicArr[index];
                    if(item && item != image && item.length > 0) {
                        productImages.push(item);
                    }
                }
                if(productImages.length > 0) {
                    productImages = productImages.join(',') + ',';
                }
                else {
                    productImages = '';
                }
                $('#productImages').val(productImages);
            });

            // 图片管理 end
        };

        exports.saveData = function() {
            // 保存数据到服务器
            $('#saveProduct').click(function(e) {
                var data = {};
                var errTargetTab = "";
                var errTargetInput = "";
                var errMessage = '';
                var isValid = true;

                var pid = parseFloat($('#pid').val());
                if(pid && pid > 0) {
                    data.id = pid;
                }

                //base data
                data.name = $('#name').val();
                data.system_category = $('#systemCategory').val();
                var subCatObj = $('#sub_sys_cats_' + data.system_category + ' select');
                if(subCatObj && subCatObj.length > 0) {
                    data.system_category = $(subCatObj).val();

                }
                data.custom_category = $('#category').val();
                subCatObj = $('#sub_cats_' + data.custom_category + ' select');
                if(subCatObj && subCatObj.length > 0) {
                    data.custom_category = $(subCatObj).val();
                }
                data.brand = $('#brand').val();
                data.thumb = $('#thumb').val();
                data.desc = $('#txtContent').val();

                //base attr
                data.import_price = parseFloat($('#import_price').val());
                data.original_price = parseFloat($('#original_price').val());
                data.sell_price = parseFloat($('#sell_price').val());
                data.isRecommended = $('#isRecommended').val();
                data.isHot = $('#isHot').val();
                data.isNew = $('#isNew').val();
                data.quantity = parseFloat($('#quantity').val());
                data.sku = $('#sku').val();
                data.share_point = $('#share_point').val();
                data.share_deadline = $('#share_deadline').val();

                if(data.original_price < data.sell_price) {
                    BSDialog.alert("预售价格不得低于折后价格！");
                    return false;
                }

                //custom attr
                var customAttr = $('#attributeListTable').find('input');
                data.attributes = [];

                if(customAttr) {
                    var isValidAttr = true;
                    $(customAttr).each(function(index, node) {
                        var id = $(node).attr('data-attr-id');
                        var val = $(node).val();
                        if(!val || val.length == 0) {
                            BSDialog.alert("对不起，参数属性值不能为空");
                            isValidAttr = false;
                            return false;
                        }
                        data.attributes.push({
                            id: id,
                            value: val
                        });
                    });
                    if(!isValidAttr) {
                        return false;
                    }
                }
                //custom option
                var customOptions = $('#optionListTable').find('tr');
                var noSetOption='';//哪些添加的属性没有设置值
                data.options = [];
                if(customOptions) {
                    $(customOptions).each(function(index, node) {
                        var id = $(node).attr('data-option-id');
                        var required = $('#option_required_' + id).attr('checked') ? '1' : '0';
                        var valuesArr = $('body').data('product_option_values_' + id);
                        console.log(node);
                        if(!valuesArr || valuesArr.length == 0) {
                          noSetOption += $(node).find('td').eq(0).html()+ ',';
                        }
                        data.options.push({
                            id: id,
                            required: required,
                            values: valuesArr
                        });
                    });
                }
                if(noSetOption!='')
                {
                    BSDialog.alert("对不起，规格参数【"+noSetOption.substring(0,noSetOption.length-1)+"】等的选项值不能为空");
                    return false;
                }

                //images
                var imgList = $('#picturesPreview li img');
                data.images = [];
                if(imgList) {
                    $(imgList).each(function(index, node) {
                        var imgUrl = $(node).attr('src');
                        data.images.push(imgUrl);
                    });
                }

                //points
                var pointsTrList = $('#user_grade_points_table tr');
                data.points = [];
                if(pointsTrList) {
                    $(pointsTrList).each(function(index, node) {
                        var grade_id = $(node).attr('data-points-grade-id');
                        var points = $('#product_user_points_' + grade_id).val();
                        data.points.push({
                            grade_id: grade_id,
                            points: points
                        });
                    });
                }

                //seo
                data.meta_keyword = $('#meta_keywords').val();
                data.meta_description = $('#meta_description').val();

                //related

                //marketing
                $('#ajaxTip').html("数据保存中，请稍候……");
                $('#ajaxStatus').show();
                $.ajax({
                    url: '/panel/products/add',
                    type: 'post',
                    dataType: 'json',
                    data: data
                }).done(function(data) {
                    $('#ajaxStatus').hide();
                    if(data.code == 0) {
                        BSDialog.alert("商品保存成功！点击确定跳转至商品列表页", function() {
                           //window.location.href = "/panel/products/index";
                            self.location=document.referrer;
                        });
                    }
                    else {
                        BSDialog.alert(data.message);
                    }
                });
            });
        }

    };

    function initialize() {
        $('body').data('attribute_data_json', JSON.parse($('#attributeDataJson').val()));
        $('body').data('option_data_json', JSON.parse($('#optionDataJson').val()));
        $('body').data('user_grade_data_json', JSON.parse($('#userGradeDataJson').val()));
        $('#systemCategory').trigger("change");
        $('#category').trigger("change");
        /*seajs.use('app/panel/panel.article', function (api) {
         api.setArticle();
         });
         seajs.use('app/app.upload', function (api) {
         api.upload('.upload-picture', {'type': 'pic'}, function (res) {
         $('#picturesPreview').append($("<li/>").append($('<img/>').attr('src', res.url)).append($('<a/>').attr('href', '#').addClass('pic-remove').text('删除')));
         var productImages = $('#productImages').val();
         productImages += res.url + ',';
         $('#productImages').val(productImages);
         });
         });*/

        //initial attributes
        var productAttributes = JSON.parse($('#productAttributeDataJson').val());
        var atrrData = $('body').data('attribute_data_json');
        if(productAttributes && productAttributes.length > 0) {
            for(var index in productAttributes) {
                var pa = productAttributes[index];
                var aid = pa.attribute_id;
                var paValue = pa.value;
                var name = "";
                for(var i in atrrData) {
                    var attr = atrrData[i];
                    if(attr.id == aid) {
                        name = attr.name;
                        break;
                    }
                }
                $('#attributeListTable').append('<tr data-attr-id="' + aid + '"><td>'
                    + name + '</td><td><input type="text" id="attribute_value_' + aid + '" data-attr-id="' + aid + '" value="' + paValue + '"'
                    + ' class="form-control" /></td><td><a class="attribute-remove btn btn-warning btn-sm" href="#" data-attr-id="' + aid
                    + '" title="删除"><i class="icon-trash"></i>删除</a></td></tr>');
                $('#attrDataList option[value=' + aid + ']').attr('disabled', true);
            }
        }

        //initial options
        var productOptions = JSON.parse($('#productOptionDataJson').val());
        var optionData = $('body').data('option_data_json');
        if(productOptions && productOptions.length > 0) {
            for(var i in productOptions) {
                var po = productOptions[i];
                var oid = po.option_id;
                var option = {};
                for(var j in optionData) {
                    option = optionData[j];
                    if(option.id == oid) {
                        break;
                    }
                }
                var values = po.values;
                var valueHtml = '';
                var product_option_values = [];
                if(values && values.length) {
                    for(var k in values) {
                        var pov = values[k];
                        var vid = pov.option_value_id;
                        var value = {};
                        for(var h in option.values) {
                            if(vid == option.values[h].id) {
                                value = option.values[h];
                                break;
                            }
                        }
                        value.detail = {
                            id: vid,
                            points: pov.points,
                            points_prefix: pov.points_prefix,
                            price: pov.price,
                            price_prefix: pov.price_prefix,
                            quantity: pov.quantity,
                            subtract: parseInt(pov.subtract) == 1 ? "on" : "",
                            weight: pov.weight,
                            weight_prefix: pov.weight_prefix
                        };
                        valueHtml += "<li>" + value.value + "</li>";
                        product_option_values.push(value);
                    }
                }

                $('body').data('product_option_values_' + oid, product_option_values);

                var po_required = parseInt(po.required) == 1 ? 'checked=true':'';
                $('#optionListTable').append('<tr data-option-id="' + oid + '"><td>' + option.name
                    + '</td>' +
//                    '<td><input type="checkbox" id="option_required_' + oid
//                    + '" class="ace ace-switch ace-switch-6 col-sm-2" ' + po_required + '/><span class="lbl"></span> </td>' +
                    '<td data-option-id="' + oid
                    + '"><ul id="option_values_item_list_' + oid
                    + '" class="list list-inline list-unstyled">' + valueHtml + '</ul></td><td><span class="btn-group"><a class="option-remove btn btn-danger btn-sm" href="#" data-option-id="' + oid
                    + '" title="删除"><i class="icon-trash"></i></a>'
                    + '<a class="option-values btn btn-primary btn-sm" href="#" data-toggle="modal" data-target="#optionValuesModal" data-option-id="' + oid
                    + '" title="编辑选项"><i class="icon-pencil"></i></a></span></td></tr>');

                $('#optionDataList option[value=' + oid + ']').attr('disabled', true);
            }
        }

        //initial points
        var productRewardsData = JSON.parse($('#productRewardsDataJson').val());
        var userGradeData = $('body').data('user_grade_data_json');
        if(productRewardsData && productRewardsData.length > 0) {
            for(var i in productRewardsData) {
                var pr = productRewardsData[i];
                var gid = pr.user_grade_id;
                var userGrade = {};
                for(var j in userGradeData) {
                    userGrade = userGradeData[j];
                    if(gid == userGrade.id) {
                        break;
                    }
                }
                $('#user_grade_points_table').append('<tr data-points-grade-id="' + gid
                    + '"><td>' + userGrade.name + '</td><td><input id="product_user_points_' + gid
                    + '" value="0"></td><td><a data-points-grade-id="' + gid
                    + '" href="#" class="point-item-remove"><i class="glyphicon glyphicon-trash"></i></a></td></tr>');
                $('#userGradeDataList option[value=' + gid + ']').attr('disabled', true);
            }
        }

        $('form').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                systemCategory : {
                    required: true
                },
                category: {
                    required: true
                },
                brand: {
                    required: true
                },
                url: {
                    required: true,
                    url: true
                },
                txtContent: {
                    required: true
                },
                state: {
                    required: true
                },
                platform: {
                    required: true
                },
                subscription: {
                    required: true
                },
                gender: 'required',
                agree: 'required'
            },

            messages: {
                email: {
                    required: "Please provide a valid email.",
                    email: "Please provide a valid email."
                },
                password: {
                    required: "Please specify a password.",
                    minlength: "Please specify a secure password."
                },
                subscription: "Please choose at least one option",
                gender: "Please choose gender",
                agree: "Please accept our policy"
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
            },

            success: function (e) {
                $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(e).remove();
            },

            errorPlacement: function (error, element) {
                if(element.is(':checkbox') || element.is(':radio')) {
                    var controls = element.closest('div[class*="col-"]');
                    if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
                    else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
                else if(element.is('.select2')) {
                    error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                }
                else if(element.is('.chosen-select')) {
                    error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                }
                else error.insertAfter(element.parent());
            },

            submitHandler: function (form) {
            }
        });
    }
});


