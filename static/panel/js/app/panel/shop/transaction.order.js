/**
 * Created by Arimis on 14-6-10.
 */


$(document).ready(function() {
    //tooltips
    $('[data-rel=tooltip]').tooltip();
    $('[data-rel=popover]').popover({html:true});

    //products tool tips
    $('a[data-product]').on('show.bs.popover', function(event) {
        var pulled = $(this).attr('data-pulled');
        if(pulled && pulled > 0) {
            $(currentPopover).popover('toggle');
            return true;
        }
        else {
            $(currentPopover).popover('hide');
            var currentPopover = this;
            $('#ajaxTip').html("获取商品数据中，请稍候……");
            $('#ajaxStatus').show();
            $.ajax({
                url: '/shop/transaction/getOrderProduct',
                type: 'post',
                dataType: 'json',
                data: {
                    order: $(currentPopover).attr('data-order')
                }
            }).done(function(data) {
                if(data.code && data.code > 0) {
                    $('#ajaxTip').html("获取订单商品信息失败！");
                    setTimeout(function() {
                        $('#ajaxStatus').hide();
                    }, 1000);
                }
                else {
                    $('#ajaxStatus').hide();
                    $(currentPopover).attr('data-pulled', 1);
                    var content = '';
                    if(data.result.length > 0) {
                        content = "<table class='table table-stipped'><thead><tr><th>名称</th><th>原售价</th><th>实售价</th><th>数量</th><th>总价</th></tr></thead>";
                        $(data.result).each(function(i, item) {
                            content += "<tr>"
                                + "<td>" + item.item_name + "</td>"
                                + "<td>" + item.item_sell_price + "</td>"
                                + "<td>" + item.item_price + "</td>"
                                + "<td>" + item.quantity + "</td>"
                                + "<td>" + item.total_cash + "</td>"
                                + "</tr>";
                        });
                        content += "</table>";
                    }
                    else {
                        content = "此订单没有商品信息";
                    }

                    $(currentPopover).attr('data-content', content);
                    $(currentPopover).popover('show');
                }
            }).error(function() {
                $('#ajaxTip').html("获取订单商品信息失败！");
                setTimeout(function() {
                    $('#ajaxStatus').hide();
                }, 1000);
                $(currentPopover).popover('hide');
            })
        }
    });

    var cur_mode = parseInt($('#pay_mode').val());
    if(cur_mode == 1) {
        $('#codStatus').show();
        $('#nonCodStatus').hide();
    }
    else if(cur_mode == 0) {
        $('#codStatus').hide();
        $('#nonCodStatus').show();
    }
    else {
        $('#codStatus').hide();
        $('#nonCodStatus').hide();
    }

    $('#pay_mode').change(function() {
        var mode = parseInt($(this).val());
        if(mode == 1) {
            $('#codStatus').show();
            $('#nonCodStatus').hide();
        }
        else if(mode == 0) {
            $('#codStatus').hide();
            $('#nonCodStatus').show();
        }
        else {
            $('#codStatus').hide();
            $('#nonCodStatus').hide();
        }
    });


    //order status change event
    $('select[data-trigger=status]').change(function(event) {
        if(event.preventDefault) {
            event.preventDefault();
        }
        else {
            event.returnValue = false;
        }

        var changedStatus = this;

        BSDialog.confirm("确定要修改订单状态吗？",  [{
            label: "放弃",
            cssClass: "btn btn-sm btn-warning",
            onclick: function() {
                $(changedStatus).val($(changedStatus).attr('data-status'));
                return false;
            }
        },{
            label: "确定",
            cssClass: "btn btn-sm btn-primary",
            onclick: function() {
                $('#ajaxTip').html("修改订单状态中……");
                $('#ajaxStatus').show();
                $.ajax({
                    url: '/shop/transaction/setOrderStatus',
                    type: "post",
                    dataType: 'json',
                    data: {
                        order: $(changedStatus).attr('data-order'),
                        status: $(changedStatus).val()
                    }
                }).done(function(data){
                    if(data.code && data.code > 0) {
                        $('#ajaxTip').html("修改订单状态失败：" + data.message);
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                        }, 1000);
                        $(changedStatus).val($(changedStatus).attr('data-status'));
                        return false;
                    }
                    else {$('#ajaxTip').html("修改订单状态成功！");
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                        }, 1000);
                        $(changedStatus).attr('data-status', $(changedStatus).val());
                    }
                }).error(function() {
                    $('#ajaxTip').html("修改订单状态失败！");
                    setTimeout(function() {
                        $('#ajaxStatus').hide();
                    }, 1000);
                });
            }
        }]);
    });

    //deliver products for order
    $('#deliver_box').on('show.bs.modal', function(e) {
        var target = e.relatedTarget;
        var type = $(target).attr('data-logistics-type');
        var order = $(target).attr('data-logistics-order');

        $('#logistics_type').val(type)
        $('#logistics_order').val(order);
    });

    $('#saveDeliverBtn').click(function() {
        var logistics_type_text = $('#logistics_type').find("option:selected").text();
        var logistics_type = $('#logistics_type').val();
        var logistics_order = $('#logistics_order').val();
        if(!logistics_type || logistics_type.length == 0 || !logistics_order || logistics_order.length == 0) {
            $('#ajaxTip').html("物流方式和物流单号都要设置！");
            $('#ajaxStatus').show();
            setTimeout(function() {
                $('#ajaxStatus').hide();
            }, 2000);
            return false;
        }
        else {
            $('#ajaxTip').html("请求发送中，请稍候……！");
            $('#ajaxStatus').show();
            $('#deliver_form').ajaxSubmit({
                url: '/shop/transaction/deliver',
                type: 'post',
                dataType: 'json',
                success: function(data, status) {
                    if(data.code && data.code > 1) {
                        $('#ajaxTip').html(data.message);
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                        }, 1000);
                    }
                    else {
                        $('#ajaxTip').html("订单发货成功！");
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                        }, 1000);
                        window.location.reload();
/*
                        $('#logistics_info').html("<ul class='list list-unstyled'>" +
                            "<li>快递公司：" + logistics_type_text + "</li>"+
                            "<li>发货时间：刚刚</li>"+
                            "<li>快递单号：" + logistics_order + "</li>"+
                            "<li>快递跟踪：<span id='logistics_log'></span></li>"+
                            "</ul>");
                        $('#deliver_box').modal('hide');
                        $("#deliveredBtn").html("更改物流信息");*/
                    }
                },
                error: function() {
                    $('#ajaxTip').html("订单发货失败，请联系官方客服解决，为此给您带来的不便，我们深表歉意！谢谢您的理解和支持。");
                    setTimeout(function() {
                        $('#ajaxStatus').hide();
                    }, 1000);
                }
            });
        }
    });


    //set receive address
    $('#address_box').on('show.bs.modal', function(e) {
		var data = $(e.relatedTarget).attr('data-data');
		if(data) {
			var data = JSON.parse(data);
			$('#name').val(data.name);
			$('#province').val(data.province);
			$('#city').val();
			$('#town').val();
			new PCAS('province', 'city', 'town',data.province, data.city, data.town);
			var address = $('#address').val(data.address);
			$("#zip_code").val(data.zip_code);
			$("#phone").val(data.phone);
		}
    });

    $('#saveAddressBtn').click(function() {
        var name = $('#name').val();
        var province = $('#province').val();
        var city = $('#city').val();
        var town = $('#town').val();
        var address = $('#address').val();
        var zipcode = $('#zip_code').val();
        var phone = $('#phone').val();
        if(!name || name.length == 0 || !province || province.length == 0 || !city || city.length == 0 || !town || town.length == 0 || !address || address.length == 0 || !zipcode || zipcode.length == 0) {
            $('#ajaxTip').html("所有选择都必须填写！");
            $('#ajaxStatus').show();
            setTimeout(function() {
                $('#ajaxStatus').hide();
            }, 2000);
            return false;
        }
        else {
            $('#ajaxTip').html("请求发送中，请稍候……！");
            $('#ajaxStatus').show();
            $('#address_form').ajaxSubmit({
                url: '/shop/transaction/address',
                type: 'post',
                dataType: 'json',
                success: function(data, status) {
                    if(data.code && data.code > 1) {
                        $('#ajaxTip').html(data.message);
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                        }, 1000);
                    }
                    else {
                        $('#ajaxTip').html("收货地址更新成功！");
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                        }, 1000);

                        $('#address_box').modal('hide');
                        window.location.href = window.location.href;
                    }
                },
                error: function() {
                    $('#ajaxTip').html("订单发货失败，请联系官方客服解决，为此给您带来的不便，我们深表歉意！谢谢您的理解和支持。");
                    setTimeout(function() {
                        $('#ajaxStatus').hide();
                    }, 1000);
                }
            });
        }
    });

    if($( "#product_name" )) {
        $( "#product_name" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "/shop/transaction/autoProduct",
                    dataType: "json",
                    type: 'post',
                    data: {
                        keyword: $( "#product_name").val()
                    },
                    success: function( data ) {
                        if(data.code && data.code > 0) {

                        }
                        else {
                            response( $.map( data.result, function( item ) {
                                return {
                                    label: item.name + "[库存:" + item.quantity + ", 售价: ￥" + item.sell_price + ']',
                                    value: item.name + "[库存:" + item.quantity + ", 售价: ￥" + item.sell_price + ']',
                                    sell_price: item.sell_price,
                                    product_id: item.id
                                }
                            }));
                        }
                    }
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                $('#product_id').val(ui.item.product_id);
                $('#price').val(ui.item.sell_price);
                $('#quantity').val(1);
                $('#total_price').val(ui.item.sell_price);
            },
            open: function() {
                $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
            },
            close: function() {
                $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
            }
        });
    }


    //初始化添加/修改订单商品的信息
    $('#product_box').on('show.bs.modal', function(e) {
        var data = $(e.relatedTarget).attr('data-data');
		if(data) {
			var data = JSON.parse(data);
			$('#product_name').val(data.item_name + "[售价：" + data.item_sell_price + "]");
			$('#product_id').val(data.item_id);
			$('#price').val(data.item_price);
			$('#quantity').val(data.quantity);
			$("#total_price").val(data.total_cash);
		}
        else {
            $('#product_name').val("");
            $('#product_id').val("");
            $('#price').val("");
            $('#quantity').val(1);
            $("#total_price").val("");
        }
    });
    $('#saveProductBtn').click(function() {
        var product_id = $('#product_id').val();
        var price = parseFloat($('#price').val());
        var total_price = parseFloat($('#total_price').val());
        var quantity = parseInt($('#quantity').val());

        if(!product_id || quantity <= 0) {
            BSDialog.alert("您填写的数据不正确，请更正后重试！");
            return false;
        }

        $('#ajaxTip').html("数据提交中，请稍候……");
        $('#ajaxStatus').show();
        $('#product_form').ajaxSubmit({
            url: '/shop/transaction/saveProduct',
            type: 'post',
            dataType: 'json',
            success: function(data, status) {
                if(data.code && data.code > 1) {
                    $('#ajaxTip').html("添加商品成功！");
                    setTimeout(function() {
                        $('#ajaxStatus').hide();
                    }, 1000);
                }
                else {
                    $('#ajaxTip').html("订单发货成功！");
                    setTimeout(function() {
                        $('#ajaxStatus').hide();
                    }, 1000);
                    $('#product_box').modal("hide");
                    window.location.href = window.location.href;
                }
            },
            error: function() {
                $('#ajaxTip').html("订单发货失败，请联系官方客服解决，为此给您带来的不便，我们深表歉意！谢谢您的理解和支持。");
                setTimeout(function() {
                    $('#ajaxStatus').hide();
                }, 1000);
            }
        });
    });

    $('#quantity').change(function() {
        var price = parseInt($('#price').val());
        var quantity = parseInt($('#quantity').val());
        $('#total_price').val(price * quantity);
    });
    $('#price').change(function() {
        var price = parseInt($('#price').val());
        var quantity = parseInt($('#quantity').val());
        $('#total_price').val(price * quantity);
    });

    $('a[data-remove]').click(function(event) {
        if(event.preventDefault) {
            event.preventDefault();
        }
        else {
            event.returnValue = false;
        }
        var order_number = $(this).attr('data-order');
        var product_id = $(this).attr("data-product");
        BSDialog.confirm("确定删除此商品吗？", [{
            label: "放弃",
            cssClass: "btn btn-sm btn-default"
        },
            {
                label:"确定",
                cssClass:"btn btn-sm btn-primary",
                onclick: function() {
                    $('#ajaxTip').html("请求发送中，请稍候……");
                    $('#ajaxStatus').show();
                    $.ajax({
                        url: "/shop/transaction/removeProduct",
                        type: 'post',
                        dataType: 'json',
                        data: {
                            order_number:order_number,
                            product_id: product_id
                        }
                    }).done(function(data) {
                        if(data.code && data.code > 0) {
                            $('#ajaxTip').html(data.message);
                            setTimeout(function() {
                                $('#ajaxStatus').hide();
                            }, 2000);
                        }
                        else {
                            $('#ajaxTip').html("删除成功！");
                            setTimeout(function() {
                                $('#ajaxStatus').hide();
                            }, 2000);
                            window.location.href = window.location.href;
                        }
                    }).error(function() {
                        $('#ajaxTip').html("请求发送失败！");
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                        }, 2000);
                    });
                }
            }]);
    })


});
