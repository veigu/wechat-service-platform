"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;

$(document).ready(function() {
    $('#saveAttribute').bind('click', function () {
        var bid = $('#bid').val();
        var name = $('#name').val();
        var desc = $('#desc').val();
        if(!name || name.length == 0) {
            alert('属性名称不能为空', function() {
                $('#name').focus();
            });
            return false;
        }
        if(!desc || desc.length == 0) {
            alert('描述不能为空', function() {
                $('#creator').focus();
            });
            return false;
        }

        var data = {};
        if(bid && bid > 0) {
            data.bid = bid;
        }
        data.name = name;
        data.desc = desc;

        $.ajax({
            url: '/panel/attribute/add',
            dataType: 'json',
            type: 'post',
            data: data
        }).done(function(data) {
            if(data.code > 0) {
                alert("属性保存失败！" + data.message);
            }
            else {
                alert('属性保存成功', function() {
                    $('#attrInfoModal').modal('hide');
                    window.location.href = window.location.href;
                });
            }
        });
    });
    $('i[data-toggle="tooltip"]').tooltip();
    $('.item-edit').click(function() {
        $('body').data('currentAttr', JSON.parse($(this).attr('data-data')));
    });
    $('#attrInfoModal').bind('show.bs.modal', function() {
        initModalFormData();
    }).bind('hide.bs.modal', function() {
        $('body').data('currentAttr', false);
    });

    $('.item-remove').click(function(e) {
        if(e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        var targetId = $(this).attr('data-id');
        confirm("确定删除", [{
            label: '放弃',
            cssClass: 'btn-warning'
        },{
            label: "确定",
            cssClass: 'btn-danger',
            onclick: function() {
                $.ajax({
                    url: '/panel/attribute/remove',
                    type: "post",
                    dataType: 'json',
                    data: {
                        aid: targetId
                    }
                }).done(function(data) {
                    if(data.code == 0) {
                        alert('删除成功!', function() {
                            window.location.href = window.location.href;
                        });
                    }
                    else {
                        alert("删除失败！" + data.message);
                    }
                });
            }
        }]);
    });
});


function initModalFormData() {
    var data = $('body').data('currentAttr');
    if(!data) {
        $('#aid').val('');
        $('#name').val('');
        $('#desc').val('');
    }
    else {
        $('#aid').val(data.id);
        $('#name').val(data.name);
        $('#desc').val(data.desc);
    }
}
