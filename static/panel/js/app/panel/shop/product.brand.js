"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;

$(document).ready(function() {
    seajs.use('app/panel/panel.storage', function(store) {
        store.getImg('#fileUpload', function (res) {
            $('#logo').val(res.url);
            $('#picReviewer img').attr('src', res.url);
        });
    });

    $('#saveBrand').bind('click', function () {
        var bid = $('#bid').val();
        var name = $('#name').val();
        var creator = $('#creator').val();
        var logo = $('#logo').val();
        var story = $('#story').val();
        var ruleElements = $(':checkbox[name=ruleIds]');
        var rules = '';
        $(ruleElements).each(function(index, node) {
            var ruleId = $(node).val();
            rules += ',' + ruleId;
        });
        if(rules.length > 0) {
            rules = rules.substring(1);
        }
        if(!name || name.length == 0) {
            alert('品牌名称不能为空', function() {
                $('#name').focus();
            });
            return false;
        }
        if(!creator || creator.length == 0) {
            alert('创始人不能为空', function() {
                $('#creator').focus();
            });
            return false;
        }
        if(!logo || logo.length == 0) {
            alert('品牌logo不能为空', function() {
                $('#logo').focus();
            });
            return false;
        }

        var data = {};
        if(bid && bid > 0) {
            data.bid = bid;
        }
        data.name = name;
        data.creator = creator;
        data.logo = logo;
        data.story = story;

        $.ajax({
            url: '/panel/brand/add',
            dataType: 'json',
            type: 'post',
            data: data
        }).done(function(data) {
            if(data.code > 0) {
                alert("品牌添加失败！" + data.message);
            }
            else {
                alert('品牌添加成功', function() {
                    $('#brandInfoModal').modal('hide');
                    window.location.href = window.location.href;
                });
            }
        });
    });
    $('i[data-toggle="tooltip"]').tooltip();
    $('.item-edit').click(function() {
        $('body').data('currentBrand', JSON.parse($(this).attr('data-data')));
    });
    $('#brandInfoModal').bind('show.bs.modal', function() {
        initModalFormData();
    }).bind('hide.bs.modal', function() {
        $('body').data('currentBrand', false);
    });

    $('.item-remove').bind('click', function(e) {
        if(e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        var itemId = $(this).attr('data-id');
        confirm("确定要删除吗？", [{
            label: "放弃",
            cssClass: 'btn-warning'
        },{
            label: '确定',
            cssClass: 'btn-danger',
            onclick: function(e) {
                $.ajax({
                    url: '/panel/brand/remove',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        bid: itemId
                    }
                }).done(function(data) {
                    if(data.code == 0) {
                        alert("删除成功", function() {
                            window.location.href = window.location.href;
                        });
                    }
                    else {
                        alert("删除失败！" + data.message);
                    }
                });
            }
        }]);
    });
});


function initModalFormData() {
    var data = $('body').data('currentBrand');
    if(!data) {
        $('#bid').val('');
        $('#name').val('');
        $('#creator').val('');
        $('#logo').val('');
        $('#story').val();
    }
    else {
        $('#bid').val(data.id);
        $('#name').val(data.name);
        $('#creator').val(data.creator);
        $('#logo').val(data.image);
        $('#story').val(data.story);
        $('#picReviewer').html('').append($('<span class="col-xs-10"/>').attr('data-list-item-id', data.id).data(data)
            .append($('<span/>').addClass('.col-xs-6').append($('<img/>').attr('src', data.image).css('with', 100).css('height', 50)))
            );
    }
}
