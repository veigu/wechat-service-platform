/**
 * Created by wgwang on 14-4-15.
 */
"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;

$(document).ready(function() {
    $('.item-remove').click(function(e) {
        if(e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        var pid = $(this).attr('data-id');
        if(!pid) {
            alert("请选择要删除的商品");
            return false;
        }
        confirm("确定要删除此商品吗？", [{
            label: "放弃",
            cssClass: 'btn-warning'
        },{
            label: "确定",
            cssClass: "btn-danger",
            onclick: function(e) {
                $.ajax({
                    url: "/panel/products/remove",
                    type: 'post',
                    dataType: 'json',
                    data: {
                        pid: pid
                    }
                }).done(function(data) {
                    if(data.code == 0) {
                        window.location.href = window.location.href;
                    }
                    else {
                        alert(data.message);
                    }
                });
            }
        }]);
    });
});
