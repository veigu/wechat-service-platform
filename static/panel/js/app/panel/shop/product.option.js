"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;

$(document).ready(function() {
    $('#saveOption').click(function () {
        var oid = $('#oid').val();
        var name = $('#name').val();
        var type = $('#type').val();
        var desc = $('#desc').val();

        if(!name || name.length == 0) {
            alert('列表名称不能为空', function() {
                $('#name').focus();
            });
            return false;
        }
        if(!type || type.length == 0) {
            alert('描述不能为空', function() {
                $('#creator').focus();
            });
            return false;
        }

        var optionItems = $('#values .option-item');
        var values = [];
        if(!optionItems || optionItems.length == 0) {
            alert("至少设置一个选项值", function() {
                renderOptionItem(type);
                $('#values .option-item').first().focus();
            });
            return false;
        }
        $(optionItems).each(function(index, node) {
            var itemVal = this.value;
            if(!itemVal || itemVal.length == 0) {
                var _targetOgj = this;
                alert("选项值不能为空", function() {
                    $(_targetOgj).focus();
                });
                return false;
            }
            else {
                values.push(itemVal);
            }
        });

        var data = {};
        if(oid && oid > 0) {
            data.oid = oid;
        }
        data.name = name;
        data.type = type;
        data.desc = desc;
        data.values = values;

        $.ajax({
            url: '/panel/option/add',
            dataType: 'json',
            type: 'post',
            data: data
        }).done(function(data) {
            if(data.code > 0) {
                alert("列表保存失败！" + data.message);
            }
            else {
                alert('列表保存成功', function() {
                    $('#optionInfoModal').modal('hide');
                    window.location.href = window.location.href;
                });
            }
        });
    });
    $('i[data-toggle="tooltip"]').tooltip();
    $('.item-edit').click(function(e) {
        $('body').data('currentList', JSON.parse($(this).attr('data-data')));
    });
    $('#optionInfoModal').bind('show.bs.modal', function() {
        initModalFormData();
    }).bind('hide.bs.modal', function() {
        $('body').data('currentList', false);
    });

    $('.item-remove').click(function(e) {
        if(e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }

        var optionId = $(this).attr('data-id');
        if(!optionId) {
            alert("没有指定要删除的选项");
            return false;
        }

        confirm("确定要删除吗?", [{
            label: "放弃",
            cssClass: "btn-warning"
        }, {
            label: "确定",
            cssClass: "btn-danger",
            onclick: function() {
                $.ajax({
                    url: '/panel/option/remove',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        oid: optionId
                    }
                }).done(function(data) {
                    if(data.code == 0) {
                        alert("删除成功！", function() {
                            window.location.href = window.location.href;
                        });
                    }
                    else {
                        alert("对不起，删除失败了！" + data.message);
                    }
                });
            }
        }]);
    });

    //绑定添加选项值按钮
    $('#addItem').bind('click', function(e) {
        if(e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        var type = $('#type').val();
        renderOptionItem(type);
    });

    $('#values').delegate('.option-item-remove', 'click', function(e) {
        if(e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }

        var target = $(this).parent();

        confirm("确定删除？", [{
            label: '放弃',
            cssClass: 'btn-warning'
        },{
            label: '确定',
            cssClass: "btn-danger",
            onclick: function() {
                target.remove();
            }
        }]);
    });

    $('#type').change(function(e) {
        if(e.preventDefault) {
            e.preventDefault()
        }
        else {
            e.returnValue = false;
        }
        var currentType = $('body').data('currentType');
        if(!currentType) {
            currentType = "text";
        }
        var newType = this.value;
        var optionItems = $('#values .option-item');
        if(optionItems && optionItems.length > 0) {
            confirm("已经有选项，改变此属性将清空所有选项值，您确定要改变？", [{
                label: '取消',
                cssClass: 'btn-warning',
                onclick: function(e) {
                    $('#type').val(currentType);
                }
            },{
                label: '确定',
                cssClass: 'btn-danger',
                onclick: function() {
                    $('#type').val(newType);
                    $('body').data('currentType', newType);
                    $('#values').empty();
                }
            }]);
        }
        else {
            $('body').data('currentType', newType);
        }
    });
});


function initModalFormData() {
    var data = $('body').data('currentList');
    if(!data) {
        $('#oid').val('');
        $('#name').val('');
        $('#type').val('text');
        $('#desc').val('');
        $('#values').empty();
        $('body').data('currentType', 'text');
    }
    else {
        $('#oid').val(data.id);
        $('#type').val(data.type);
        $('body').data('currentType', data.type);
        $('#name').val(data.name);
        $('#desc').val(data.desc);
        var values = $('body').data('option-values-' + data.id);
        if(values) {
            renderOptionList(data.type, data.values);
        }
        else {
            var optionId = data.id;
            var optionType = data.type;
            $.ajax({
                url: '/panel/option/values',
                type: 'post',
                dataType: 'json',
                data: {
                    oid: optionId
                }
            }).done(function(data) {
                if(data.code > 0) {
                    alert("获取选项值失败" + data.message);
                }
                else {
                    $('body').data('option-values-' + optionId);
                    renderOptionList(optionType, data.result);
                }
            });
        }
    }
}


/**
 * render data list for option
 * @param type
 * @param data
 */
function renderOptionList(type, data) {
    $('#values').empty()
    $(data).each(function(index, node) {
        renderOptionItem(type, node.value);
    });
}

/**
 * add a empty element for option item
 * @param type
 * @param value
 */
function renderOptionItem(type, value) {
    if(!value) {
        value = '';
    }
    var renderedHtml = "";
    switch (type) {
        case 'textarea' : {
            renderedHtml = "<span class='input-group'><textarea class='option-item form-control'>" + value + "</textarea><a class='input-group-addon glyphicon glyphicon-minus option-item-remove'></a></span>";
            break;
        }
        case 'date': {
            renderedHtml = "<span class='input-group'><input type='date' class='option-item form-control' value='" + value + "'/><a class='input-group-addon glyphicon glyphicon-minus option-item-remove'></a></span>";
            break;
        }
        case 'time': {
            renderedHtml = "<span class='input-group'><input type='time' class='option-item form-control' value='" + value + "'/><a class='input-group-addon glyphicon glyphicon-minus option-item-remove'></a></span>";
            break;
        }
        case 'datetime': {
            renderedHtml = "<span class='input-group'><input type='datetime-local' class='option-item form-control' value='" + value + "'/><a class='input-group-addon glyphicon glyphicon-minus option-item-remove'></a></span>";
            break;
        }
        default: {
            renderedHtml = "<span class='input-group'><input type='text' class='option-item form-control' value='" + value + "'/><a class='input-group-addon glyphicon glyphicon-minus option-item-remove'></a></span>";
        }
    }
    $('#values').append(renderedHtml);
}
