//BUI.use('common/page');

$(document).ready(function (e) {
    $('.item-edit').click(function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }

        var data = JSON.parse($(this).attr('data-data'));

        $('#logistics_name').html(data.name);
        $('#logistics_key').val(data.key);
        if (!data.fee_editable || parseInt(data.fee_editable) == 0) {
            $('#fee').attr('readonly', true);
        }
        else {
            $('#fee').attr('readonly', false);
        }

        if (data.is_active > 0) {
            $('#is_active').attr('checked', true);
            $('#is_not_active').attr('checked', false);
        }
        else {
            $('#is_active').attr('checked', false);
            $('#is_not_active').attr('checked', true);
        }

        $('#fee').val(data.fee);
        $('#description').val(data.description);

        $('#paymentModal').modal("show");
    });

    $('#saveConfig').click(function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }

        $('#ajaxTip').html("设置保存中，请稍候……！");
        $('#ajaxStatus').show();
        $('#payment_form').ajaxSubmit({
            url: '/shop/logistics/configSave',
            type: 'post',
            dataType: 'json',
            success: function (data, statusText) {
                if (data.code && data.code > 1) {
                    $('#ajaxTip').html(data.message);
                    setTimeout(function () {
                        $('#ajaxStatus').hide();
                    }, 1000);
                }
                else {
                    $('#ajaxTip').html("保存设置成功！");
                    setTimeout(function () {
                        $('#ajaxStatus').hide();
                    }, 1000);
                    window.location.href = window.location.href;
                }
            },
            error: function () {
                $('#ajaxTip').html("保存设置失败，请联系官方客服解决，为此给您带来的不便，我们深表歉意！谢谢您的理解和支持。");
                setTimeout(function () {
                    $('#ajaxStatus').hide();
                }, 1000);
            }
        });

        $('#paymentModal').modal('hide');
    });
});