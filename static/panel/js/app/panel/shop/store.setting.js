/**
 * Created by Arimis on 14-5-23.
 */

$(document).ready(function() {
    //上传缩略图
    seajs.use(['app/panel/panel.storage', 'app/app.editor'], function(store, editor) {
        store.getImg('#fileUpload', function (res) {
            $('#logo').val(res.url);
            $('#picReviewer img').attr('src', res.url);
        });

        store.getImg('#upSiteBg', function (res) {
            $('#background').val(res.url);
            $('#background-preview').attr('src', res.url);
        });
        editor.init(".txtContent");
    });
});
