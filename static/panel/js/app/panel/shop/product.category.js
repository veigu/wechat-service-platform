/**
 * Created by wgwang on 14-4-8.
 */
"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;

$(document).ready(function() {
    $('[data-rel="tooltip"]').tooltip();

    $('.tree-list').delegate('.tree-add', 'click', function() {
        var pid = $(this).attr('data-id');
        $('#cid').val('');
        $('#pid').val(pid);
        $('#resourceListBox').html('');
        $('#clickValBox').hide();
        $('#linkValBox').show();
        $('#type').val('VIEW');
        if(pid > 0) {
            $('#type').find('option[value=TOP_BAR]').attr('disabled', true);
        }
        else {
            $('#type').find('option[value=TOP_BAR]').attr('disabled', false);
        }
        $('#menuInfoModal').modal('show');
    });

    $('.tree-list').delegate('.tree-edit', "click", function(e) {
        var data = JSON.parse($(this).attr('data-data'));
        if(data) {
            $('#cid').val(data.id);
            $('#pid').val(data.pid);
            $('#name').val(data.name);
            $('#sort').val(data.sort);
            $('#image').val(data.image);
            $('#description').val(data.description);
            $('#meta_keyword').val(data.meta_keyword);
            $('#meta_description').val(data.meta_description);
            if(data.pid > 0) {
                $('#type').find('option[value=TOP_BAR]').attr('disabled', true);
            }
            else {
                $('#type').find('option[value=TOP_BAR]').attr('disabled', false);
            }
            if(data.image && data.image.length > 0) {
                $('#resourceListBox').html($('<span/>').addClass('.col-xs-6').append($('<img width="100%"/>').attr('src', data.image)));
            }
            else {
                $('#resourceListBox').html('');
            }
            if(data.type == "VIEW") {
                $('#linkValBox').show();
                $('#link').val(data.target_value);
                $('#clickValBox').hide();
                $('#message').val('');
                setMessage();
            }
            else if(data.type == "CLICK") {
                $('#linkValBox').hide();
                $('#clickValBox').show();
                setMessage(data);
            }
            else {
                $('#linkValBox').hide();
                $('#clickValBox').hide();
//                $('#type').find('option').attr('disabled', true);
                $('#type').find('option[value=TOP_BAR]').attr('disabled', false);
                setMessage();
            }
            $('#menuInfoModal').modal('show');
        }
        else {
            alert("请指定要修改的菜单！", function() {
                $('#menuInfoModal').modal('hide');
            });
        }
    });

    $('.tree-list').delegate('.tree-remove', "click", function(e) {
        var mid = $(this).attr('data-id');
        confirm("确定删除？", [{
            label: "取消",
            cssClass:'btn-default'
        },{
            label: "确定",
            cssClass: 'btn-danger',
            onclick: function(e) {
                $.ajax({
                    url: '/panel/category/remove',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        cid: mid
                    }
                }).done(function(data) {
                    if(data.code > 0) {
                        $("#ajaxTip").html("删除失败!" + data.message);
                        $('#ajaxStatus').show();
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                        }, 2000);
                    }
                    else {
                        $("#ajaxTip").html("删除成功!" + data.message);
                        $('#ajaxStatus').show();
                        setTimeout(function() {
                            $('#ajaxStatus').hide();
                            $('li[data-cat-id=' + mid +']').remove();
                        }, 2000);
                    }

                });
            }
        }]);
    });


    $('#menuInfoModal').bind('shown.bs.modal', function() {
        var _type = $('#type').val();
    });

    seajs.use('app/panel/panel.storage', function(store) {
        store.getImg('#thumbUploader', function (res) {
            $('#image').val(res.url);
            $('#resourceListBox').html($('<span/>').addClass('.col-xs-6').append($('<img/>').attr('src', res.url)));
        });
    });

    //subscribe/normal event message setting type switcher
    $('li[data-type]').on('click', function() {
        var _type = $(this).attr("data-type");
        $('#messageType').val(_type);
        if(!$(this).hasClass('active')) {
            $('.tab_content[data-tab='+_type+'] .edit_area').text('');
        }
    });


    //保存菜单
    $('#saveCategory').bind("click", function() {
        var cid = $('#cid').val();
        var name = $('#name').val();
        var image = $('#image').val();
        var pid = $('#pid').val();
        var sort = $('#sort').val();
        var desc = $('#description').val();
        var metaKeyword = $('#meta_keyword').val();
        var metaDesc = $('#meta_description').val();

        if(!pid) {
            pid = 0;
            data.pid = pid;
        }

        if(!name || name.length == 0) {
            alert("分类名称不能为空!", function() {
                $('#name').focus();
            });
            return false;
        }
        if(!desc || desc.length == 0) {
            alert("描述不能为空!", function() {
                $('#description').focus();
            });
            return false;
        }
        if(!metaKeyword || metaKeyword.length == 0) {
            alert("META关键字不能为空!", function() {
                $('#meta_description').focus();
            });
            return false;
        }
        if(isNaN(sort)){
            alert("分类排序不能为非数字!", function() {
                $('#sort').focus();
            });
            return false;
        }
        if(!metaDesc || metaDesc.length == 0) {
            alert("META描述不能为空!", function() {
                $('#meta_description').focus();
            });
            return false;
        }

        var data = {};
        if(cid && cid > 0) {
            data.cid = cid;
        }

        data.pid = pid;
        data.name = name;
        data.sort = sort;
        data.image = image;
        data.description = desc;
        data.meta_keyword = metaKeyword;
        data.meta_description = metaDesc;
        $.ajax({
            url: '/panel/category/add',
            type: 'post',
            dataType: 'json',
            data: data
        }).done(function(data) {
            if(data.code > 0) {
                alert("分类保存失败！" + data.message);
            }
            else {
                alert("分类保存成功！", function() {
                	$('#menuInfoModal').modal('hide');
                	window.location.href = window.location.href;
                });
            }
        });
    });
});

function setMessage(data) {
    if(data) {
        $('#clickValBox [data-type='+ data.message_type +'] a').tab("show");
        $('#message').val(data.target_value);
        $('#messageType').val(data.message_type);
        if(data.message_type == 'text') {
            $('div[data-tab=text] .edit_area').html(data.target_value);
        }
        else if(data.message_type == 'image') {
            data = data.message;
            $('div[data-tab=image] .edit_area').html($('<span/>').addClass('.col-xs-6').append($('<img/>').attr('src', data.url)));
        }
        else if(data.message_type == 'voice') {
            data = data.message;
            $('div[data-tab=voice] .edit_area').html($("<span/>").append($('<video/>').attr('src', data.url)));
        }
        else if(data.message_type == 'video') {
            data = data.message;
            $('div[data-tab=video] .edit_area').html($("<span/>").append($('<video/>').attr('src', data.url)));
        }
        else {
            data = data.message;
            $('div[data-tab=video] .edit_area').html($("<span/>").append($('<video/>').attr('src', data.url)));
        }
    }
    else {
        $('div[data-tab=text] .edit_area').html('');
        $('div[data-tab=image] .edit_area').html('');
        $('div[data-tab=voice] .edit_area').html('');
        $('div[data-tab=video] .edit_area').html('');
        $('#messageBox [data-type=text] a').tab("show");
        $('#message').val('');
        $('#messageType').val('text');
    }
}