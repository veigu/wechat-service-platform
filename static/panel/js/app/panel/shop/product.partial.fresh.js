/**
 * Created by luguiwu on 14-12-2.
 */

"use strict";
define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    exports.fresh = function () {
        $(".freight_fresh").on('click', function () {
            var freight_id = $(this).attr('data-val');
            base.requestApi('/api/shop/freightFresh', '', function (res) {
                if (res.result == 1) {
                    var data = res.data;
                    var select = "";

                    select += "<option value='0'>请选择运费模板</option>";
                    for (var i in data) {

                            select +=
                                "<option  value='" +
                                    data[i]['id'] + "'" +
                                    " data-item='" +
                                    data[i]['data-item'] +
                                    "'data-type='" +
                                    data[i]['valuation_way'] +"'>"+
                                    data[i]['tpl_name']+
                                    "</option>";
                        }
                   $("#freight").html(select);
                   $("#freight").attr('value',freight_id);
                  tip.showTip('ok','运费模板列表刷新成功',1000);

                }


            })

        });
        $(".brand_fresh").on('click',function(){
            var brand_id = $(this).attr('data-val');
            base.requestApi('/api/shop/brandFresh', '', function (res) {
                if (res.result == 1) {
                    var data = res.data;
                    var select = "";

                    select += "<option value='0'>选择品牌</option>";
                    for (var i in data) {
                        select +=
                            "<option  value='" +
                                data[i]['id'] + "'"
                                +"'>"+
                                data[i]['name']+
                                "</option>";
                    }
                    $("#brand_id").html(select);
                    $("#brand_id").attr('value',brand_id);
                    tip.showTip('ok','品牌列表刷新成功',1000);

                }


            })
        })

    }

})
