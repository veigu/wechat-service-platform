"use sxtrict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;

//页面初始化
$(document).ready(function() {
//    setMessage('subscribe');

    $('#topTabs').bind('shown.bs.tab', function(e) {
    	var url = $(e.target).attr('data-url');
    	if(url && url.length > 0) {
    		window.location.href = url;
    	}
    });
});

function setMessage(rtype) {
    var initData = JSON.parse($('#messageSettingJson').val());
    var data = initData[rtype];
    if(data) {
        $('#messageBox [data-type='+ data.message_type +'] a').tab("show");
        if(data.message_type == 'text') {
            $('div[data-tab=text] .edit_area').html(data.value);
        }
        else if(data.message_type == 'image') {
//            $('div[data-tab=image] .edit_area').append($("<span/>").append($('<img/>').attr('src', data.url)));
            $('div[data-tab=image] .edit_area').html($('<span/>').addClass('.col-xs-6').append($('<img/>').attr('src', data.url)));
        }
        else if(data.message_type == 'voice') {
            $('div[data-tab=voice] .edit_area').html($("<span/>").append($('<video/>').attr('src', data.url)));
        }
        else if(data.message_type == 'video') {
            $('div[data-tab=video] .edit_area').html($("<span/>").append($('<video/>').attr('src', data.url)));
        }
        else {

        }
    }
    else {
        $('div[data-tab=text] .edit_area').html('');
        $('div[data-tab=image] .edit_area').html('');
        $('div[data-tab=voice] .edit_area').html('');
        $('div[data-tab=video] .edit_area').html('');
        $('#messageBox [data-type=text] a').tab("show");
    }
}

function setKeywordMessage(data) {
    var initData = JSON.parse($('#messageSettingJson').val());
    var data = initData.keyword;
    if(data) {
        if(data.message_type == 'text') {
            $('div[data-tab=text] .edit_area').html(data.value);
        }
        else if(data.message_type == 'image') {
//            $('div[data-tab=image] .edit_area').append($("<span/>").append($('<img/>').attr('src', data.url)));
            $('div[data-tab=image] .edit_area').html($('<span/>').addClass('.col-xs-6').append($('<img/>').attr('src', data.url)));
        }
        else if(data.message_type == 'voice') {
            $('div[data-tab=voice] .edit_area').html($("<span/>").append($('<video/>').attr('src', data.url)));
        }
        else if(data.message_type == 'video') {
            $('div[data-tab=video] .edit_area').html($("<span/>").append($('<video/>').attr('src', data.url)));
        }
        else if(data.message_type == 'news') {

        }
    }
    else {
        $('div[data-tab=text] .edit_area').html('');
        $('div[data-tab=image] .edit_area').html('');
        $('div[data-tab=voice] .edit_area').html('');
        $('div[data-tab=video] .edit_area').html('');
    }
}
//事件类型切换事件绑定
$('#subscribe').on('show.bs.tab', function (e) {
    e.target // activated tab
    e.relatedTarget // previous tab
    $('#warningMsg').hide();
    $('#respondType').val('subscribe');
    setMessage('subscribe');
});
$('#normal').on('show.bs.tab', function (e) {
	e.target // activated tab
    e.relatedTarget // previous tab
    $('#warningMsg').text("如果已经开启客服功能，则此功能自动失效！").show();
    $('#respondType').val('normal');
    setMessage('normal');
});
$('#keywords').on('show.bs.tab', function (e) {
	e.target // activated tab
    e.relatedTarget // previous tab
    $('#warningMsg').hide();
	$('#respondType').val('keyword');
});

//subscribe/normal event message setting type switcher
$('li[data-type]').on('click', function() {
    var _type = $(this).attr("data-type");
    $('#messageType').val(_type);
    if(!$(this).hasClass('active')) {
        $('.tab_content[data-tab='+_type+'] .edit_area').text('');
    }
});

//文件上传按钮事件
$('#trigerFileUpload').on('click', function() {
    $('#fileUploadModal').modal("show");
});

//资源选择器modal
$('#resourceViewer').unbind();
$('#resourceViewer')
.on('show.bs.modal', function (e) {
    var _type = $('#messageType').val();
    $.ajax({
        url: '/home/resource/get',
        data: {
            type: _type
        },
        type: 'post',
        dataType:'json'
    }).done(function(data) {
        if(data.code) {
          alert("server error");
        }
        $('#resourceListBox').empty();
        $('#progress .progress-bar').css(
          'width', 0
        );
        if(data.result.total) {
            $(data.result.list).each(function (i, node) {
                $('body').data('boxTag', 'video');
                switch(_type) {
                    //image
                    case 'image': {
                        var listItem = $('<li/>').attr('data-list-item-id', node.id).data(node)
                            .append($('<span/>').addClass('.col-xs-6').append($('<img/>').attr('src', node.url))).append($('<span/>').addClass('.col-xs-6').text(node.name));
                        $('body').data('boxTag', 'img');
                        break;
                    }
                    //voice
                    case "voice": {
                        var listItem = $('<li/>').attr('data-list-item-id', node.id).data(node)
                            .append($('<span/>').addClass('.col-xs-6').append($('<video/>').attr('src', node.url))).append($('<span/>').addClass('.col-xs-6').text(node.time));
                        break;
                    }
                    //video
                    case 'video': {
                        var listItem = $('<li/>').attr('data-list-item-id', node.id).data(node)
                            .append($('<span/>').addClass('.col-xs-6').append($('<video/>').attr('src', node.url))).append($('<span/>').addClass('.col-xs-6').text(node.title));
                        break;
                    }
                }
                $('#resourceListBox').append(listItem);
            });
            var url = '/home/resource/upload/' + _type;
            $('#fileupload').unbind();
            $('#fileupload').fileupload({
                url: url,
                dataType: 'json',
                done: function (e, data) {
                    if(data.result.code > 0) {

                    }
                    else {
                        var item = data.result.results;
                        var boxTag = $('body').data('boxTag');
                        $('<li/>').attr('data-list-item-id', item.id).data(item)
                            .append($('<span/>').addClass('.col-xs-6').append($('<' + boxTag + '/>').attr('src', item.url)))
                            .append($('<span/>').addClass('.col-xs-6').text(item.name))
                            .appendTo('#resourceListBox');
                    }
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
        }
        else {
            listBox = "没有数据，请先上传些吧！";
        }
        $('#resourceListBox').delegate('li', 'click', function() {
            $('#resourceListBox').find('li').removeClass('btn-success');
            $(this).addClass('btn-success');
        });
    });
  });

//资源选择器确定按钮事件
  $('#resourcePickOK').bind('click', function (e) {
      var _type = $('#messageType').val();
      var rType = $('#respondType').val();
      var selectedItems = $('#resourceListBox').find('.btn-success');
      if (selectedItems.length <= 0) {
      } else {
          var item = selectedItems[0];
          var id = $(item).attr('data-list-item-id');
          $('#messageData').val(id);
          var initData = JSON.parse($('#messageSettingJson').val());
          if(initData.length == 0) {
              initData = {};
          }
          var typeData = {};
          if(initData[rType]) {
              typeData = initData[rType];
          }
          if(rType != 'keyword') {
              typeData.message_type = _type;
              typeData.value = id;
              typeData.url = $(item).find('img').attr('src');
              initData[rType] = typeData;
              var dataStr = JSON.stringify(initData);
              $('#messageSettingJson').val(dataStr);
              //noinspection JSValidateTypes
              $('div[data-tab=' + _type + '] .edit_area').html($(item).children().first());
          }
          else {
              $('div[data-tab=' + _type + '] .edit_area').html($(item).children().first());
          }
      }
  });

/**
 * emotion settings
 * @return {[type]} [description]
 */
$('.emotion_switch .js_switch').on('click', function() {

});

$('#addKeyword').bind("click", function(e) {
    var id = Math.round(Math.random() * 100000, 0);
    $("#keywordList").append($('<div class="keywordItem" id="keywordItem_'+id+'"><div class="col-sm-8">'+
        '<input type="text" class="form-control" id="keyword_0" data-keyword="1" placeholder="关键词"></div>'+
        '<div class="checkbox col-sm-4">'+
        '<label><input type="checkbox" id="fullText_0" value="full_text"> 全文匹配</label>'+
        '<a href="#" class="alert-danger remove-keyword" data-id="'+id+'" title="删除关键词"><i class="glyphicon glyphicon-minus" "=""></i></a>'+
        '</div><div class="help-block col-sm-8">字数不得多于30个汉字</div></div>'));
});

$("#keywordList").delegate('.remove-keyword', 'click', function(e) {
    if(e.preventDefault) {
        e.preventDefault();
    }
    else {
        e.returnValue = false;
    }
    var id = $(this).attr('data-id');
    confirm("确定删除吗", [{
            'label': "取消",
            'cssClass': "btn-warning",
        },
        {
            'label': "确定",
            'cssClass': "btn-danger",
            'onclick': function() {
                $("#keywordItem_" + id).remove();
            }
        }]);
});

/*$('#keywordsBox').delegate('a[data-edit]', function() {

});

$('#keywordsBox').delegate('a[data-remove]', function() {

});*/

/**
 * save message settings
 * @return {[type]} [description]
 */
$('#saveMsgBtn').unbind();
$('#saveMsgBtn').bind('click', function() {
    var respondType = $('#respondType').val();
    if(!respondType) {
      respondType = 'subscribe';
    }
    var msg_type = $('#messageType').val();
    if(!msg_type) {
      msg_type = 'text';
    }
    var data = "";
    if(msg_type == 'text') {
    	data = $('div[data-tab=text] .edit_area').text();
    }
    else {
    	data = $('#messageData').val();
    }
    $.ajax({
        url: "/home/we-chat/save",
        data: {
            respondType: respondType,
            messageType: msg_type,
            value: data
        },
        dataType: 'json',
        type: 'post'
    }).done(function(data) {
        if(data.code > 1) {
            alert(data.message);
        }
        alert('保存成功！');
    });
});

/**
 * save message settings
 * @return {[type]} [description]
 */
$('#keywordRuleOK').unbind();
$('#keywordRuleOK').bind('click', function() {
    var ruleName = $('#ruleName').val();
    if(!ruleName || ruleName.length == 0) {
        alert("规则名称不能为空", function() {
            $('#ruleName').focus();
        });
        return false;
    }
    var keywords = {};
    var keywordElements = $('[data-keyword]');
    $.each(keywordElements, function(index, node) {
        var keyword = $(node).val();
        var fullText = $('#fullText_'+index).attr('checked');
        if(fullText) {
            fullText = 1;
        }
        else {
            fullText = 0;
        }

        keywords[index] = {'keyword':keyword, 'full_text':parseInt(fullText)};
    });

    if(keywords.length == 0) {
        alert("至少需要一个关键词", function() {
            $('#keyword_0').focus();
        });
        return false;
    }

    var keywordsData =  encodeURIComponent(JSON.stringify(keywords));

    var msg_type = $('#keywordRuleModal .nav-tabs li.active a').attr('data-type');
    var data = "";
    if(msg_type == 'text') {
        data = $('#keywordRuleModal div[data-tab=text] .edit_area').text();
    }
    else if(msg_type != 'news') {
        data = $('#keywordRuleModal div[data-tab='+msg_type+'] .edit_area li').attr('data-id');
    }
    else {
        data = $('#keywordRuleModal div[data-tab=news] .edit_area li').attr('data-id');
    }

    $.ajax({
        url: "/home/we-chat/save-keyword",
        data: {
            ruleName: ruleName,
            messageType: msg_type,
            keywords: keywordsData,
            messageData: data
        },
        dataType: 'json',
        type: 'post'
    }).done(function(data) {
        if(data.code > 1) {
            alert(data.message);
        }
        addRenderedRule(data);
        alert('保存成功！', function() {

        });
    });
});

function addRenderedRule(ruleData) {
    var ruleName = $('#ruleName').val();
    var keywords = {};
    var keyword = $('#keyword_0').val();
    var fullText = $('#fullText_0').attr('checked');
    if(fullText) {
        fullText = 1;
    }
    else {
        fullText = 0;
    }

    keywords[keyword] = parseInt(fullText);

    var keywordsData = JSON.stringify(keywords);

    var msg_type = $('#keywordRuleModal .nav-tabs li.active a').attr('data-type');
    var data = "";
    if(msg_type == 'text') {
        data = $('#keywordRuleModal div[data-tab=text] .edit_area').text();
    }
    else if(msg_type != 'news') {
        data = $('#keywordRuleModal div[data-tab='+msg_type+'] .edit_area li').attr('data-id');
    }
    else {
        data = $('#keywordRuleModal div[data-tab=news] .edit_area li').attr('data-id');
    }
    var returnHtml = '<li id="rule_'+ruleData.id+'"><div class="panel panel-default">'+
                    '<div class="panel-header">'+
                        '"<span>'+ruleName+'</span><span>'+keywords+'</span>'+
                        '<span class="pull-right">'+
                            '<a href="#" data-remove="'+ruleData.id+'" data-toggle="modal" data-target="#keywordRuleModal"><i class="fa fa-trash-o fa-fw"></i></a>'+
                            '<a href="#" data-edit="'+ruleData.id+'"><i class="fa fa-pencil fa-fw"></i></a>'+
                        '</span>'+
                    '</div>'+
                    '<div class="panel-body">'+

                    '</div>'+
                '</div>'+
            '</li>';
    $('#keywordsBox').append(returnHtml);
}