/**
 * 资源数据管理器 resource manager
 * -- 浏览,上传,选择
 * -- 文章，产品两种种类型
 * -- 多选功能
 * -- 自定义popup实现方式

 * ====== usage ========================================

 *   -- 1. import view tpl
 <?php $this->partial('base/widget/resource'); ?>

 *   -- 2. seajs.use
 seajs.use('app/panel/panel.resource', function (a) {
    a.getItem('#upSiteIcon', func, true);
    function func(res) {
        $('#site_icon').val(res.url);
        $('.preview-icon').attr('src', res.url);
    }
 });

 * @author yanue
 * @time 2014-06-29
 * @version 1.0
 */
define(function (require, exports) {
    require('jquery/jquery.pagination.js');
    require('jquery/jquery.stringify.js');
    require('tools/draggabilly.min.js');
    // 拖拽窗口
//    new Draggabilly(document.querySelector('#wxResourceWidget'), { handle: '#wxResourceWidget .popup-head'});

    var base = require('app/panel/panel.base');//公共函数
    var upload = require('app/app.upload');

    var callbacks = []; //所有回调
    var curBtn = null;// 当前按钮
    var tmpType = null; //记录上次打开的类型
    var curMulti = false; //记录上次打开的类型
    var tmpMultiFiles = [];//记录选择文件(去除重复)
    var tmpMultiFilesCover = [];//记录选择文件封面
    var hasData = false;

    /**
     * storage class
     *
     * @constructor
     */
    function Resource() {
        this.confirm();
    }
    Resource.typeName = {
        'item': '商品',
        'post': '文章'
    };
    Resource.totalCount = null;
    Resource.prototype = {

        /**
         * init
         * @param btn
         * @param type
         * @param multi
         */
        init: function (btn, type, multi) {
            var _this = this;

            $(btn).click(function (e) {

                // 重置窗口
                if (type != tmpType || curMulti != multi) {
                    _this.reset(type);
                }

                curMulti = multi;
                tmpType = type;
                curBtn = btn; //记录当前按钮

                base.showPop('#wxResourcePopup');

                if (type == 'post') {
                    _this.initPost();
                }

                if (type == 'item') {
                    _this.initItem();
                }

                if (curMulti == true) {
                    $('#wxResourcePopup .multiData').show();
                    $('#wxResourcePopup .singleData').hide();

                    var key = eval($('.multiData-tmp').attr('data-key'));
                    var title = eval($('.multiData-tmp').attr('data-title'));
                    var cover = eval($('.multiData-tmp').attr('data-cover'));

                    for (var i in key) {
                        tmpMultiFiles[key[i]] = title[i];
                        tmpMultiFilesCover[key[i]] = cover[i];
                    }

                    // fileChoose
                    _this.multiChoose();
                } else {
                    $('#wxResourcePopup .singleData').show();
                    $('#wxResourcePopup .multiData').hide();

                    // oneChoose
                    _this.oneChoose();
                }

                e.stopImmediatePropagation();
            });
        },

        initPost: function () {
            var _this = this;
            // 隐藏
            $('#wxResourcePopup .nav-tabs .tab').hide()
            $('#wxResourcePopup .nav-tabs .tab[data-type="post"]').show();


            if (curMulti == true) {
                $('#wxResourcePopup .multiData').show();
                $('#wxResourcePopup .singleData').hide();

                var key = eval($('.multiData-tmp').attr('data-key'));
                var title = eval($('.multiData-tmp').attr('data-title'));
                var cover = eval($('.multiData-tmp').attr('data-cover'));

                for (var i in key) {
                    tmpMultiFiles[key[i]] = title[i];
                    tmpMultiFilesCover[key[i]] = cover[i];
                }

                // fileChoose
                _this.multiChoose();
            } else {
                $('#wxResourcePopup .singleData').show();
                $('#wxResourcePopup .multiData').hide();

                // oneChoose
                _this.oneChoose();
            }

            if (!hasData) {
                _this.getArticleData({'needCover': 1});
            }
        },

        initItem: function () {
            var _this = this;
            // 隐藏
            $('#wxResourcePopup .nav-tabs .tab').hide()
            $('#wxResourcePopup .nav-tabs .tab[data-type="item"]').show().addClass('active');

            if (!hasData) {
                _this.getItemData({});
            }
        },

        confirm: function () {
            $('#wxResourcePopup').on('click', '.confirmChooseBtn', function (e) {
                var args = '';
                // 多选
                if (curMulti == true) {
                    // 验证
                    if (!$('#wxResourcePopup .multiData').val()) {
                        $('.res-area .res-err-tip').addClass('brown').text('(请选择一个及以上' + Resource.typeName[tmpType] + ')');
                        return false;
                    } else {
                        $('.res-area .res-err-tip').removeClass('brown').text('(请选择一个及以上' + Resource.typeName[tmpType] + ')');
                    }

                    var objData = $('#wxResourcePopup .multiData-tmp').val();

                    // return data
                    args = objData;
                } else {
                    var id = $('#wxResourcePopup .singleData').attr('data-id');
                    var title = $('#wxResourcePopup .singleData').val();
                    var cover = $('#wxResourcePopup .singleData').attr('data-cover');
                    if (!title) {
                        $('.res-area .res-err-tip').removeClass('brown').text('(请选择' + Resource.typeName[tmpType] + ')');
                        return false;
                    }

                    args = {'id': id, 'title': title, 'cover': cover};
                }

                base.hidePop('#wxResourcePopup');
                var func = callbacks['"' + curBtn + '"'];
                if (typeof  func == 'function') {
                    func(eval(args));
                }

                e.stopImmediatePropagation();
            });
        },

        oneChoose: function () {
            $('#wxResourcePopup .tab-pane.active .data-list').on('click', '.choose_link:radio', function (e) {
                var id = $(this).attr('data-id');
                var title = $(this).attr('data-title');
                var cover = $(this).attr('data-cover');

                var url = $(this).attr('data-url');

                $('.res-area .singleData').val(title).attr('data-title', title).attr('data-id', id).attr('data-cover', cover);


                $(this).siblings('.oneChoose').removeClass('current').find('.chooseIcon').remove();
                $(this).addClass('current').append('<button class="btn btn-xs btn-success chooseIcon"><i class="icon-ok icon"></i></button>');

                e.stopImmediatePropagation();
            });

            return this;
        },

        multiChoose: function () {
            var _this = this;
            $('#wxResourcePopup .tab-pane.active .data-list').on('click', '.choose_link:checkbox', function (e) {
                var id = $(this).attr('data-id');
                var cover = $(this).attr('data-cover');
                var title = $(this).attr('data-title');
                // choose or not
                if ($(this).hasClass('current')) {
                    $(this).removeClass('current');
                    title = undefined; // 禁用，去重复
                    $(this).removeAttr('checked');
                } else {
                    $(this).addClass('current');
                    $(this).prop('checked', true);
                }

                _this.saveMulti(id, title, cover);
                e.stopImmediatePropagation();
            });

            return this;
        },

        saveMulti: function (id, title, cover) {
            // 去重
            tmpMultiFiles[id] = title;
            tmpMultiFilesCover[id] = cover;
            var filesKey = [];
            var filesTitle = [];
            var filesCover = [];

            var data = [];
            for (var i in tmpMultiFiles) {
                // 去除重复
                if (tmpMultiFiles[i] != undefined) {
                    filesKey.push(i);
                    filesTitle.push(tmpMultiFiles[i]);
                    filesCover.push(tmpMultiFilesCover[i]);
                    data.push({'id': i, 'title': tmpMultiFiles[i], 'cover': tmpMultiFilesCover[i]});
                }
            }

            $('.multiData-tmp').val(JSON.stringify(data)).attr('data-key', JSON.stringify(filesKey))
                .attr('data-title', JSON.stringify(filesTitle))
                .attr('data-cover', JSON.stringify(filesCover));

            $('.multiData').val(filesTitle.join('\n'));
        },

        /**
         * get article data
         * @param data
         */
        getArticleData: function (data) {
            var _this = this;
            // api request
            base.requestApi('/api/article/get', data, function (res) {
                if (res.result == 1) {
                    hasData = 1;

                    var list = res.data.list;
                    var count = res.data.count;
                    var limit = res.data.limit;

                    if (Resource.totalCount != count) {
                        _this.pagination(Math.ceil(count / limit), data, 'post');
                        Resource.totalCount = count;
                    }

                    var str = '';
                    var inputType = curMulti ? 'checkbox' : 'radio';
                    var inputName = curMulti ? 'multi-radio' : 'single-radio';
                    if (list) {
                        for (var i in list) {
                            var item = list[i];
                            str += '<li class="item">' + '<small class="right">' + item.date + '</small>';
                            str += '<label>';
                            str += '  <input type="' + inputType + '" name="' + inputName + '" class="ace choose_link" value="' + item.url + '" ' +
                                'data-title="' + item.title + '" data-id="' + item.id + '" data-cover="' + item.cover + '">';
                            str += '    <span class="lbl">';
                            str += '    <img src="' + item.cover + '" alt=""/>' + item.title;
                            str += '    </span>';
                            str += '  </label>';
                            str += '</li>';
                        }
                    }

                    $('#wxResourcePopup .tab-pane.active .data-list').html(str);

                    if (curMulti) {
                        _this.setMultiView();
                    }

                    base.showTip('ok', '获取成功！', 2000);
                }
            });
        },

        /**
         * get item data
         * @param data
         */
        getItemData: function (data) {
            var _this = this;
            // api request
            base.requestApi('/api/shop/getItem', data, function (res) {
                if (res.result == 1) {
                    hasData = 1;

                    var list = res.data.list;
                    var count = res.data.count;
                    var limit = res.data.limit;

                    if (Resource.totalCount != count) {
                        _this.pagination(Math.ceil(count / limit), data, 'item');
                        Resource.totalCount = count;
                    }

                    var str = '';
                    var inputType = curMulti ? 'checkbox' : 'radio';
                    var inputName = curMulti ? 'multi-radio' : 'single-radio';
                    if (list) {
                        for (var i in list) {
                            var item = list[i];
                            str += '<li class="item">' + '<small class="right">￥ ' + item.sell_price + '</small>';
                            str += '<label>';
                            str += '  <input type="' + inputType + '" name="' + inputName + '" class="ace choose_link" value="' + item.url + '" ' +
                                'data-title="' + item.name + '" data-id="' + item.id + '" data-cover="' + item.thumb + '">';
                            str += '    <span class="lbl">';
                            str += '    <img src="' + item.thumb + '" alt=""/>' + item.name;
                            str += '    </span>';
                            str += '  </label>';
                            str += '</li>';
                        }
                    }

                    $('#wxResourcePopup .tab-pane.active .data-list').html(str);

                    if (curMulti) {
                        _this.setMultiView();
                    }

                    base.showTip('ok', '获取成功！', 1000);
                }
            });
        },

        /**
         * 通过Ajax加载分页元素
         *
         * @param total
         * @param data
         */
        pagination: function (total, data, type) {
            var _this = this;

            if (type == 'post') {
                // 创建分页
                $('.pagination.post-page').pagination(total, {
                    num_edge_entries: 1, //边缘页数
                    num_display_entries: 6, //主体页数
                    items_per_page: 1, //每页显示1项
                    link_to: 'javascript:;',
                    prev_text: '<i class="icon-double-angle-left"></i>',
                    next_text: '<i class="icon-double-angle-right"></i>',
                    callback: function (page_index) {
                        data.p = page_index + 1;
                        data.needCover = 1;
                        _this.getArticleData(data);
                    }
                });
            }
            if (type == 'item') {
                // 创建分页
                $('.pagination.item-page').pagination(total, {
                    num_edge_entries: 1, //边缘页数
                    num_display_entries: 6, //主体页数
                    items_per_page: 1, //每页显示1项
                    link_to: 'javascript:;',
                    prev_text: '<i class="icon-double-angle-left"></i>',
                    next_text: '<i class="icon-double-angle-right"></i>',
                    callback: function (page_index) {
                        data.p = page_index + 1;
                        _this.getItemData(data);
                    }
                });
            }
        },

        /**
         * set choose file status
         */
        setMultiView: function () {
            $('#wxResourcePopup .tab-pane.active .data-list .choose_link:checkbox').each(function () {
                var id = $(this).attr('data-id');
                if (tmpMultiFiles[id]) {
                    $(this).addClass('current').prop('checked', true);
                }
            });
        },

        reset: function (type) {
            $('#wxResourceWidget .nav-tabs .tab').removeClass('active');
            $('#wxResourceWidget .nav-tabs .tab[data-type="' + type + '"]').addClass('active');
            $('#wxResourceWidget .tab-content .tab-pane').removeClass('active');
            $('#wxResourceWidget .tab-content #' + type + '-list-resource').addClass('active');
            $('#wxResourceWidget .tab-content .data-list').html('');
            hasData = 0;
            // reset
            $('#wxResourceWidget .singleData').val('');
            $('#wxResourceWidget .multiData').val('');
            $('#wxResourceWidget .multiData-tmp').val('').removeAttr('data-key').removeAttr('data-title').removeAttr('data-cover');

            // 清空
            tmpMultiFiles = [];//记录选择文件(去除重复)
            tmpMultiFilesCover = [];//记录选择文件封面
        }
    };

    var store = new Resource();

    /**
     * 获取商品
     *
     * @param btn
     * @param func
     * @param multi
     */
    exports.getItem = function (btn, func, multi) {
        callbacks['"' + btn + '"'] = func; // 记录所有回调函数
        store.init(btn, 'item', multi);
    };

    /**
     * 获取文章
     *
     * @param btn
     * @param func
     * @param multi
     */
    exports.getPost = function (btn, func, multi) {
        callbacks['"' + btn + '"'] = func; // 记录所有回调函数
        store.init(btn, 'post', multi);
    };

});
