/**
 * Created by Arimis on 14-5-19.
 */
$( "#years-slider" ).slider({
    value:1,
    range: "min",
    min: 0,
    max: 5,
    step: 1,
    slide: function( event, ui ) {
        var val = parseInt(ui.value);
        $('#years').val(val);
        calculateCash();
    }
});
$( "#months-slider").slider({
    value:0,
    range: "min",
    min: 0,
    max: 9,
    step: 1,
    slide: function( event, ui ) {
        var val = parseInt(ui.value);
        $('#months').val(val);
        calculateCash();
    }
});

function calculateCash() {
    var year = parseInt($('#years').val());
    var month = parseInt($('#months').val());
    var year_price = parseFloat($('#year_price').val());
    var month_price = parseFloat($('#month_price').val());
    var total_cash = year * year_price + month * month_price;
    $('#total_cash').val(total_cash);
}
