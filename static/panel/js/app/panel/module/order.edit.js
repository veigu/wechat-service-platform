jQuery(function ($) {

    $.mask.definitions['~'] = '[+-]';
    $('#cellphone').mask('19999999999');

    $('[data-rel=tooltip]').tooltip();

    $('.check-item').bind('click', function (e) {
        var checked = $(this).hasClass('checked');
        if (checked) {
            $(this).removeClass('checked');
        }
        else {
            $(this).addClass('checked');
        }
    });

    $('#serve_type').change(function (e) {
        var type = $(this).val();
        if (type != 'year') {
            $('#year').hide();
            $('#month').show();
        }
        else {
            $('#year').show();
            $('#month').hide();
        }
    });

    $('#year').change(function (e) {
        calculateCash()
    });
    $('#month').change(function (e) {
        calculateCash()
    });
    $('#serve_type').change(function (e) {
        calculateCash()
    });

    $( "#years-slider" ).slider({
        value:1,
        range: "min",
        min: 0,
        max: 5,
        step: 1,
        slide: function( event, ui ) {
            var val = parseInt(ui.value);
            $('#years').val(val);
            calculateCash();
        }
    });
    $( "#months-slider").slider({
        value:0,
        range: "min",
        min: 0,
        max: 9,
        step: 1,
        slide: function( event, ui ) {
            var val = parseInt(ui.value);
            $('#months').val(val);
            calculateCash();
        }
    });

    function calculateCash(type, terms) {
        var years = $('#years').val();
        var months = $('#months').val();
        var price_items = $('.checked .item-year-price');
        var total_price = 0.00;
        var total_cash = 0.00;
        var price = 0.00;
        $(price_items).each(function (index, item) {
            price = parseFloat($(item).text());
            total_price += price;
        });
        total_cash = (total_price * years);
        price_items = $('.checked .item-month-price');

        total_price = 0.00;
        $(price_items).each(function (index, item) {
            price = parseFloat($(item).text());
            total_price += price;
        });

        total_cash += (total_price * months);
        total_cash += '.00';
        $('#total_cash').text(total_cash);
    }

    var order_type = parseInt($('#orderType').val());
    var start_step = 1;
    if (order_type > 0) {
        start_step = 1;
        var resources = JSON.parse($('#orderResources').val());
        if (resources) {
            $(resources).each(function (index, resource) {
                var id = resource.resource_id;
                $('div.check-item[data-id=' + id + ']').addClass('checked');
            });
        }
    }
    //initialize wizard
    var wizard = $('#wizard-header').ace_wizard({step: start_step});
    wizard.on('change',function (e, info) {
        if(info.direction == "previous") {
            $('.btn-next').attr('disabled', false);
            return true;
        }
        if (info.step == 1) {
            if (info.direction == 'previous') {
                var order_type = $('#orderType').val();
                if (order_type) {
                    return false;
                }
            }
            var checkedItems = $('.check-item.checked');
//            if (!checkedItems.length) {
//                bootbox.dialog({
//                    message: "您需要至少选择一项服务!",
//                    buttons: {
//                        "success": {
//                            "label": "确定",
//                            "className": "btn-sm btn-primary"
//                        }
//                    }
//                });
//                return false;
//            }
            calculateCash('year', 1);
        }
        else if (info.step == 2) {
            if (info.direction === 'next') {
                var price_items = $('.checked');
                var modules = [];
                $(price_items).each(function (index, item) {
                    modules.push(parseFloat($(item).attr('data-id')));
                });
                var years = parseInt($('#years').val());
                var months =parseInt( $('#months').val());
                if(!years && !months) {
                    return false;
                }
                var order_number = $('#order_number').val();
                $('#step3').html("<div class='center'><h4 class='blue'>订单保存中，请稍后……</h4></div>");
//            $.blockUI();
                $.ajax({
                    url: '/account/saveOrder',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        order_number: order_number,
                        modules: modules,
                        years: years,
                        months: months,
                        total_cash: parseFloat($('#total_cash').text())
                    }
                }).done(function (data) {
                    if (data.code == 0) {
                        $('#order_number').val(data.result.order_number);
                        var price_items = $('.checked');
                        var modules = '';
                        $(price_items).each(function (index, item) {
                            var mName = $(item).find('.item-title').text();
                            modules += mName + ' ';
                        });
                        var total_cash = parseFloat($('#total_cash').text());
                        var type = $('#serve_type').val();
                        var serve_time = '';
                        serve_time += $('#years').val() + ' 年';
                        serve_time += $('#months').val() + ' 月';

                        $('#step3').html('<div class="center">'
                            + '<h3 class="green">恭喜您!已经为您生成订单，详情如下</h3>'
                            + '<dl><dt>购买ESTT网服务</dt><dd>包含服务：' + modules + '</dd><dd>总金额：&yen;' + total_cash + '</dd><dd>时限：' + serve_time + '</dd></dl>'
                            + '<div class="pull-right col-md-offset-3 col-md-9">' +
                            '<a href="/payment/alipay/pay/PO/' + data.result.encoded_number + '" target="_blank"><img class="icon bigger-10" src="/static/panel/images/payment/alipay.gif" title="支付宝付款" align="middle" alt="" border="0"></a>' +
//                            '<a href="/panel/payment/cbsend" target="_blank"><img src="/static/panel/images/payment/cbpay.png" title="网银在线支付" align="middle"></a>' +
//                            '<a href="/panel/payment/tenpay" target="_blank"><img src="/static/panel/images/payment/tenpay.png" title="财付通支付"></a>' +
//                            '<a href="/account/trial" class="btn btn-default" target="_self">试用7天</a>' +
                            '</div>'
                            + '</div>');
                        $('.btn-next').attr('disabled', false);
                    }
                    else if (data.code == 1) {
                        bootbox.dialog({
                            message: data.message,
                            buttons: {
                                "success": {
                                    "label": "确定",
                                    "className": "btn-sm btn-primary",
                                    "click": function () {
                                        window.location.href = "/account/reg";
                                    }
                                }
                            }
                        });
                    }
                    else {
                        $('#step3').html("<div class='center'><h2 class='red'>" + data.message + "</h2></div>");
                        $('.btn-next').attr('disabled', true);
                    }
                });
            }
        }
    }).on('finished',function (e) {
        window.location.href = '/panel/module/trial';
        bootbox.dialog({
            message: "<div class='center'>马上支付，即可享受各项服务！</div>",
            buttons: {
                "success": {
                    "label": "支付完成",
                    "className": "btn-sm btn-primary btn-lg",
                    "callback": function (e) {
                        $.ajax({
                            url: '/module/payCheck',
                            type: 'post',
                            dataType: 'json',
                            data: {

                            }
                        }).done(function (data) {
                            if (data.code == 1) {
                                bootbox.dialog({
                                    message: "<div class='center'>马上支付，即可享受各项服务！</div>",
                                    buttons: {
                                        "success": {
                                            "label": "支付完成",
                                            "className": "btn-sm btn-primary btn-lg"
                                        }
                                    }
                                });
                            }
                            else {
                                window.location.href = "/panel";
                            }
                        });
                    }
                },
                "failed": {
                    "label": "支付失败",
                    "className": "btn-sm btn-warning btn-lg"
                }
            }
        });
    }).on('stepclick', function (e) {
        console.log(e);
        //return false;//prevent clicking on steps
    });

    jQuery.validator.addMethod("phone", function (value, element) {
        return this.optional(element) || /^1[0-9]{10}$/.test(value);
    }, "Enter a valid phone number.");

    $('#first_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            name: {
                required: true,
                minlength: 2,
                maxlength: 100
            },
            cellphone: {
                required: true,
                phone: 'required'
            },
            address: {
                required: true,
                minlength: 10
            },
            province: {
                required: true
            },
            city: {
                required: true
            },
            town: {
                required: true
            }
        },

        messages: {
            name: {
                required: "企业名称必须填写.",
                minlength: "企业名称长度不得小于2个字符.",
                maxlength: "企业名称不得大于100个字符"
            },
            cellphone: {
                required: "手机号码必须填写.",
                phone: "手机号码格式不正确."
            },
            address: {
                required:"详细地址必须填写",
                minlength: "企业详细地址长度不得小于10个字符."
            },
            province: "请指定企业所在省份",
            city: "请指定企业所在城市",
            town: "请指定企业所在区域"
        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            $('.alert-danger', $('.login-form')).show();
        },

        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },

        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },

        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
            }
            else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            }
            else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            }
            else error.insertAfter(element.parent());
        },

        submitHandler: function (form) {
        },
        invalidHandler: function (form) {
        }
    });
});