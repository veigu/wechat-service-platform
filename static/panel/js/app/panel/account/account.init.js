define(function (require, exports) {
    var base = require("app/panel/panel.base");
    require("jquery/jquery.dragdrop");
    require('jquery/jquery.stickyNavbar.min');
    require('jquery/jquery.easing.min');

    exports.forgot = function () {
        $('.forgotForm').on('click', '.loginBtn', function (e) {
            var email = $('#email').val();
            if (!new RegExp(/(\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)/).test(email)) {
                tip.showTip('err', "账号格式不准确，请填写用户名或邮箱！", 2000);
                return;
            }

            base.requestApi('/api/account/forgot', {email: email}, function (res) {
                if (res.result == 1) {
                    setTimeout(function () {
                         window.location.href = '/account/login';
                    }, 1500);
                    tip.showTip("ok", '重置后的密码已经发送到您的油箱，请注意查收！', 5000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    exports.login = function () {
        $('.loginForm').on('click', '.loginBtn', function (e) {
            var data = $('.loginForm').serializeObject();

            if (!new RegExp(/(\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)/).test(data.account)) {
                if (!new RegExp(/[0-9a-zA-Z-+]{4,16}/).test(data.account)) {
                    tip.showTip('err', "账号格式不准确，请填写用户名或邮箱！", 2000);
                    $('.forgotForm #account').focus();
                    return;
                }
            }

            if (data.password.length < 5 || data.password.length > 16) {
                tip.showTip('err', "密码长度为6-16位", 2000);
                return;
            }


            base.requestApi('/api/account/login', data, function (res) {
                if (res.result == 1) {
                    setTimeout(function () {
                        window.location.href = res.data;
                    }, 1500);
                    tip.showTip("ok", '恭喜您，登陆成功！', 2000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    exports.reg = function () {
        $('#regForm').on('click', '.regBtn', function (e) {
            var data = $('.regForm').serializeObject();

            if (!checkField('#regForm #account', /^[a-zA-Z][0-9a-zA-Z_]{4,15}$/)) {
                $('.regForm #account').focus();
                return;
            }
            if (!checkField('#regForm #email', /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/)) {
                $('.regForm #email').focus();
                return;
            }

            if (!checkField('#regForm #password', /(.*){6,16}/)) {
                $('.regForm #password').focus();
                return;
            }

            if (data.password != $("#repassword").val()) {
                tip.showTip("err", "两次输入的密码不一致！", 2000);
                $('.regForm #repassword').focus();
                return;
            }

            if ($("#agree:checked").attr("data-checked") != "true") {
                tip.showTip("err", "请接受用户协议！", 2000);
                return;
            }


            base.requestApi('/api/account/reg', data, function (res) {
                if (res.result == 1) {
                    setTimeout(function () {
                        window.location.href = res.data;
                    }, 1500);
                    tip.showTip("ok", '恭喜您，注册成功！', 2000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    exports.profile = function (fromPage) {
        require("/static/panel/js/tools/region.select");

        $("#profileForm").on("click", '.saveProfileBtn', function () {
            var data = $("#profileForm").serializeObject();

            if (!data.company_name) {
                $("#company_name").focus();
                tip.showTip('err', "请填写企业名称！", 2000);
                return;
            }

            if (!checkField('#industry')) {
                $('#industry').focus();
                return;
            }

            if (!checkField('#province')) {
                $('#province').focus();
                return;
            }

            if (data.town == "市辖区") {
                $("#town").focus();
                tip.showTip('err', "请选择区域！", 2000);
                return;
            }

            if (data.address.length < 6) {
                $("#address").focus();
                tip.showTip('err', "请填写正确的详细地址！", 2000);
                return;
            }
            if (data.detail.length < 10 || data.detail.length >90) {
                $("#town").focus();
                tip.showTip('err', "请填写企业简介，10字以上 90字以内！", 2000);
                return;
            }

            // 联系信息验证
            if (!checkField('#contact_person', /^[\u4E00-\u9FA5]{2,6}$/)) {
                $('#contact_person').focus();
                return;
            }

            if (!checkField('#contact_phone', /^1[\d]{10}$/)) {
                $('#contact_phone').focus();
                return;
            }

            if (data.contact_tel) {
                if (!checkField('#contact_tel', /^(1[\d]{10})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{3,7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$/)) {
                    $('#contact_tel').focus();
                    return;
                }
            }

            if (!checkField('#contact_email', /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/)) {
                $('#contact_email').focus();
                return false;
            }

            if (data.contact_qq) {
                if (!checkField('#contact_qq', /^\d{5,11}$/)) {
                    $('#contact_qq').focus();
                    return;
                }
            }

            base.requestApi("/api/customer/saveProfile", {data: data}, function (res) {
                if (res.result == 1) {
                    if (fromPage) {
                        setTimeout(function () {
                            window.location.reload()
                        }, 1000)
                    } else {
                        window.location.href = res.data;
                    }
                }
            });
        });
    };

    exports.pickPackage = function () {
        $("#industry").on("change", function () {
            var ind = $(this).val();
            window.location.href = '/customer/pick?ind=' + ind;
        });

        $("#pickPackageBtn").click(function (e) {
            var pack = $(this).attr("data-package");
            var industry = $("#industry").val();
            if (!industry) {
                tip.showTip('err', '请先选择行业!', 2000);
            }
            base.requestApi("/api/customer/pickPackage", {"industry": industry, "package": pack}, function (res) {
                if (res.result == 1) {
                    tip.showTip("ok", "恭喜你，套餐选择成功！", 5000);
                    setTimeout(function () {
                        window.location.href = "/customer/order";
                    }, 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };


    // 自定义选择套餐
    exports.customPackage = function () {
        getChoosedFunc();
        // sticky
        $('.asideNav').stickyNavbar({
            activeClass: "active", // Class to be added to highlight nav elements
            sectionSelector: "aside", // Class of the section that is interconnected with nav links
            animDuration: 350, // Duration of jQuery animation as well as jQuery scrolling duration
            startAt: 0, // Stick the menu at XXXpx from the top of the this() (nav container)
            easing: "swing", // Easing type if jqueryEffects = true, use jQuery Easing plugin to extend easing types - gsgd.co.uk/sandbox/jquery/easing
            animateCSS: true, // AnimateCSS effect on/off
            animateCSSRepeat: false, // Repeat animation everytime user scrolls
            cssAnimation: "fadeInDown", // AnimateCSS class that will be added to selector
            jqueryEffects: false, // jQuery animation on/off
            jqueryAnim: "slideDown", // jQuery animation type: fadeIn, show or slideDown
            selector: "a", // Selector to which activeClass will be added, either "a" or "li"
            mobile: false, // If false, nav will not stick under viewport width of 480px (default) or user defined mobileWidth
            mobileWidth: 480, // The viewport width (without scrollbar) under which stickyNavbar will not be applied (due user usability on mobile)
            zindex: 1, // The zindex value to apply to the element: default 9999, other option is "auto"
            stickyModeClass: "sticky", // Class that will be applied to 'this' in sticky mode
            unstickyModeClass: "unsticky" // Class that will be applied to 'this' in non-sticky mode
        });

        // 功能选择
        $(".customForm .func").on("click", function (e) {
            // 基础功能必选
            if ($(this).attr("data-name") == "base") {
                tip.showTip("err", "微官网为基础功能，必须选择", 3000);
                return;
            }

            // 商城只能选择一种
            if ($(this).attr("data-module_name") == "shop_full") {
                $('.func[data-module_name="shop_base"]').removeClass("choosed");
            }

            // 商城只能选择一种
            if ($(this).attr("data-module_name") == "shop_base") {
                $('.func[data-module_name="shop_full"]').removeClass("choosed");
            }

            if ($(this).hasClass("choosed")) {
                $(this).removeClass("choosed");
            } else {
                $(this).addClass("choosed");
            }

            getChoosedFunc();
            e.stopImmediatePropagation();
        });

        // 取消功能
        $('.choosedFuncList').on('click', '.rmFunc', function (e) {
            var module_name = $(this).attr("data-name");
            $(".custom").find(".func[data-module_name=" + module_name + "]").removeClass("choosed");
            getChoosedFunc();
            e.stopImmediatePropagation();
        });

        // 已选择功能
        function getChoosedFunc() {
            var year_price = 0;
            var month_price = 0;
            var all_func = [];
            var choosedFuncList = "";
            $(".func.choosed").each(function () {
                var y_price = $(this).attr("data-year_price");
                var m_price = $(this).attr("data-month_price");
                var module_name = $(this).attr("data-module_name");
                var name = $(this).attr("data-name");
                if (!isNaN(y_price) && y_price > 0) {
                    year_price += parseInt(y_price);
                }
                if (!isNaN(m_price) && m_price > 0) {
                    month_price += parseInt(m_price);
                }

                all_func.push(module_name);

                var rm = module_name != "base" ? '<sapn class="right"> <i class="icon icon-remove rmFunc" data-name="' + module_name + '"></i></sapn>' : "";

                choosedFuncList += '<li>' + rm +
                '<span class="name">' + name + '</span><span class="normal">[<span class="price">¥<i>' + m_price + '</i>/月 - ¥<i>' + y_price + '</i>/年</span>]</span></li>';
            });
            // 更新右边
            $(".choosedFuncList").html(choosedFuncList);
            // 设置表单
            $(".txtChoosedPackage").attr("data-year_price", year_price).attr("data-month_price", month_price).attr("data-all_func", $.base64.encode(JSON.stringify(all_func)));
            // 设置价格
            $(".total_year").html(year_price);
            $(".total_month").html(month_price);
        }

        // 直接下单
        $(".customPackageBtn").on('click', function order(e) {
            var type = $(this).attr("data-type");
            var modules = $(".txtChoosedPackage").attr('data-all_func');
            base.requestApi('/api/customer/customPackage', {
                "all_func": modules
            }, function (res) {
                if (res.result == 1) {
                    tip.showTip("ok", "恭喜你，套餐选择成功！", 5000);
                    setTimeout(function () {
                        window.location.href = "/customer/order";
                    }, 1000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    // 生成订单
    exports.genOrderOrTrial = function (minUnit) {

        // 购买时长选择功能
        function Duration() {
            this.clickChoose();
            this.dragChoose();
            Duration.leftPos = $('.bar').offset().left;
        }

        Duration.nowUnit = 1;// 最新选择
        Duration.minUnit = 1;// 最小选择
        Duration.prototype = {
            init: function (minUnit) {
                Duration.minUnit = minUnit;
                this.setBar(minUnit);
            },
            clickChoose: function () {
                var _this = this;
                $(".range .item").on("click", function (e) {
                    var unit = $(this).text().trim();
                    var year = $(this).attr("data-year");
                    if (unit < Duration.minUnit) {
                        unit = Duration.minUnit;
                        var tipWord = Duration.minUnit > 9 ? (Duration.minUnit - 9) + '年' : Duration.minUnit + '月';
                        tip.showTip('err', "最低选择为：" + tipWord, 3000);
                    }
                    if (!isNaN(year)) {
                        unit = 9 + parseInt(year);
                    }
                    _this.setBar(unit);
                    e.stopImmediatePropagation();
                });

                $(".bar .unit").on("click", function (e) {
                    var unit = $(this).text().trim();
                    var year = $(this).attr("data-year");
                    if (unit < Duration.minUnit) {
                        unit = Duration.minUnit;
                        var tipWord = Duration.minUnit > 9 ? (Duration.minUnit - 9) + '年' : Duration.minUnit + '月';

                        tip.showTip('err', "最低购买时长为：" + tipWord, 3000);
                    }

                    if (!isNaN(year)) {
                        unit = 9 + parseInt(year);
                    }

                    _this.setBar(unit);
                    e.stopImmediatePropagation();
                });
            },
            dragChoose: function () {
                var _this = this;
                $(".drag").drag(function (e, pos) {
                    Duration.nowUnit = Math.ceil((pos.offsetX - Duration.leftPos) / 37);
                    Duration.nowUnit = Duration.nowUnit < Duration.minUnit ? Duration.minUnit : Duration.nowUnit;
                    Duration.nowUnit = Duration.nowUnit > 12 ? 12 : Duration.nowUnit;
                    // 拖到过程，直接改变位置
                    $(".bar").css({"width": (pos.offsetX - Duration.leftPos) + "px"});
                    $(".drag").animate({"left": (pos.offsetX - Duration.leftPos) + "px"}, 5);
                }).drag("end", function (e) {
                    // 拖到结束，设置
                    _this.setBar(Duration.nowUnit);
                    e.stopImmediatePropagation();
                });
            },
            setBar: function (unit) {
                if (unit < 10) {
                    $(".bar .unit img").hide();
                    $(".bar .unit img[data-unit=" + unit + "]").show();
                } else {
                    $(".bar .unit img").show();
                }

                var left = unit * 37;
                $(".bar").css({"width": left + "px"});
                $(".drag").animate({"left": (left - 8) + "px"}, 300);
                $(".txtDuration").val(unit);
                var mon_price = $(".txtDuration").attr("data-month_price");
                var year_price = $(".txtDuration").attr("data-year_price");
                // 计算价格
                if (unit < 10) {
                    if (isNaN(mon_price) || mon_price == 0.00 || mon_price == 0) {
                        tip.showTip("err", "对不起，该套餐不支持月付！", 3000);
                    } else {
                        $(".total_paid").html("￥" + (parseFloat(mon_price) * unit) + '元');
                    }
                } else {
                    $(".total_paid").html("￥" + (parseFloat(year_price) * (unit - 9)) + '元');
                }
            }
        };

        // 默认最小购买时长1年
        var dur = new Duration();
        dur.init(minUnit);
        dur.setBar(10);

        // 直接下单
        $(".settleOrderBtn").on('click', order);
        // 直接试用
        $(".trialBtn").on('click', order);
        function order(e) {
            var type = $(this).attr("data-type");
            var duration = $(".txtDuration").val();

            if (!duration) {
                tip.showTip('err', "请选择购买时长", 3000);
                return;
            }

            base.requestApi('/api/customer/genOrder', {"duration": $.base64.encode("estt" + duration)}, function (res) {
                if (res.result == 1) {
                    if (type == 'trial') {
                        tip.showTip("ok", "恭喜你，试用成功！", 5000);
                        setTimeout(function () {
                            window.location.href = "/panel";
                        }, 1000);
                    } else {
                        tip.showTip("ok", "恭喜你，下单成功，即将跳转到支付！", 5000);
                        setTimeout(function () {
                            window.location.href = "/payment/alipay/pay/PO/" + res.data;
                        }, 1000);
                    }
                }
            });

            e.stopImmediatePropagation();
        }
    };

    exports.applyO2O = function () {
        $('#applyBtn').on('click', function (e) {
            base.requestApi('/api/customer/applyO2O', {}, function (res) {
                if (res.result == 1) {
                    tip.showTip("ok", "恭喜你，申请入住O2O平台成功！", 5000);
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    function checkField(elem, regx) {
        var val = $(elem).val();
        var placeholder = $(elem).attr("placeholder");
        var tips = $(elem).attr("data-tip");
        if ($(elem).val() == '') {
            tip.showTip('err', placeholder + '不能为空', 2000);
            return false;
        } else {
            if (regx) {
                tips = tips || "格式不正确";
                if (!new RegExp(regx).test(val)) {
                    tip.showTip('err', placeholder + tips, 3000);
                    return false;
                }
            }
        }

        return true;
    }

});