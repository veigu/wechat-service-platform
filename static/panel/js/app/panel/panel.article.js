define(function (require, exports) {
    var base = require('app/panel/panel.base');//公共函数
    var store = require('app/panel/panel.storage');//公共函数
    var appIcon = require('app/app.icon');
    var color = require('app/app.color');

    base.selectNone();
    base.selectCheckbox();

    exports.setArticle = function () {
        // 初始化上传
        store.getImg('#upCoverIcon', function (res) {
            $('#cover').val(res.url);
            $('.imgPreview').attr('src', res.url).show();
        });

        // 初始化编辑器
        var editor = require('app/app.editor');
        editor.init('#txtContent');
    };
    /**
     * del nav
     * @param btn
     */
    exports.delCat = function (btn) {
        $("table.list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/article/delCat', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！',3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del all nav
     * @param btn
     */
    exports.delAllCats = function (btn) {
        $("table.list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/article/delCat', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！',3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };
    exports.setCat = function () {
        store.getImg('.getCoverBtn', function func(res) {
            $('#catModal #cover_img').val(res.url);
            $('.preview_img').attr('src', res.url);
        }, false);


        // update event
        $('.upBtn').click(function (e) {
            var id = $(this).attr('data-id');
            // api request
            base.requestApi('/api/article/getCat', {'id': id}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '获取数据成功！',1000);
                    $('#catModal').modal('show');

                    setModal(res.data);
                }
            });

            e.stopImmediatePropagation();
        });

        // add event
        $('.addBtn').click(function (e) {
            resetModal();
        });

        // set icon
        appIcon.setIcon('.icon-preview', '.icon-wrap', function (icon) {
            $('#catModal #icon').val(icon);
        });

        // save event
        save();

        function setModal(data) {
            // set color
            color.pick(data.color, function (colorCls, hex) {
                $('#catModal #color').val(colorCls);
            });
            $('#catModal').find('#color').val(data.color);

            // set icon
            var icon = data.icon;
            $('.icon-preview').removeClass().addClass('icon-preview ' + icon);
            $('#catModal').find('#icon').val(data.icon);
            // 二级分类
            $('#catModal').find('#parent_id option').show();
            $('#catModal').find('#parent_id option[value="' + data.id + '"]').hide();
            if (data.parent_id > 0) {
                $('#catModal').find('#parent_id option[value="' + data.parent_id + '"]').prop('selected', true);
            }

            $('#catModal').find('#cover_img').val(data.cover);
            $('#catModal').find('.preview_img').attr('src', data.cover);

            $('#catModal').find('#name').val(data.name);
            $('#catModal').find('#detail').val(data.detail);
            $('#catModal').find('#sort').val(data.sort);
            $('#catModal').find('#page').val(data.page);


            var allow_login=parseInt(data.allow_login);
            $('#catModal').find("input[name='allow_login']:eq("+allow_login+")").attr('checked','checked');
            if(allow_login==1)
            {
                $('#catModal').find('#pagec').css('display','block');
            }
            $('#catModal').find('.saveBtn').attr('data-id', data.id);

        }

        function resetModal() {
            // set color
            color.pick('color117', function (colorCls, hex) {
                $('#catModal #color').val(colorCls);
            });
            $('#catModal').find('#color').val('color117');

            // set icon
            $('.icon-preview').removeClass().addClass('icon-preview icon-sun');
            $('#catModal').find('#icon').val('icon-sun');

            $('#catModal').find('#name').val('');
            $('#catModal').find('#detail').val('');
            $('#catModal').find('#sort').val(0);
            $('#catModal').find('.saveBtn').removeAttr('data-id');
        }

        function save() {
            $('#catModal .saveBtn').click(function (e) {
                var name = $('#catModal').find('#name').val();
                var detail = $('#catModal').find('#detail').val();
                var icon = $('#catModal').find('#icon').val();
                var color = $('#catModal').find('#color').val();
                var cover_img = $('#catModal').find('#cover_img').val();
                var sort = $('#catModal').find('#sort').val();
                var parent_id = $('#catModal').find('#parent_id option:selected').val();
                var allow_login  = $('#catModal').find("input[name='allow_login']:checked").val();
                var page  = $('#catModal').find("#page").val();



                var id = $(this).attr('data-id');

                if (!checkNull('#catModal #name', '分类名称不能为空')) {
                    return false;
                }

                if (!checkNull('#catModal #icon', '请选择图标')) {
                    return false;
                }

                var data = {
                    name: name,
                    detail: detail,
                    icon: icon,
                    color: color,
                    cover: cover_img,
                    sort: sort,
                    parent_id: parent_id,
                    allow_login:allow_login,
                    page:page
                };

                // api request
                base.requestApi('/api/article/saveCat', {'data': data, id: id}, function (res) {
                    if (res.result == 1) {
                        $('#catModal').modal('hide');
                        window.location.reload();
                        base.showTip('ok', '操作成功！',1000);
                    }
                });

                e.stopImmediatePropagation();
            });
        }
    }


    /**
     * publish post or not
     *
     * @param btn
     * @param referer
     */
    exports.pubPost = function (btn) {
        $('.list .listData').on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var is_publish = $(this).attr('data-status');
            var data = {
                'id': id,
                'published': is_publish
            };

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/article/publish', data, function (res) {
                if (res.result == 1) {
                    var btns = $('.pubBtn[data-id="' + id + '"]');
                    if (is_publish == 1) {
                        btns.attr('data-status', 0).text("取消发布").css('color', '');
                    } else {
                        btns.attr('data-status', 1).text("发布").css('color', '#f00');
                    }
                    base.showTip('ok', '操作成功！',1000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    // all select status
    exports.selectCheckbox = function () {
        // tr click
        $('.list .listData').on('click', ".chk", function (e) {
            var chk_id = $(this).attr('data-id');

            if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                $(this).parents('.item[data-id=' + chk_id + ']').addClass('selected');
            } else {
                $(this).parents('.item[data-id=' + chk_id + ']').removeClass('selected');
            }
            e.stopImmediatePropagation();
        });

        // checkbox click
        $('.list .listData').on('click', ".item", function (e) {
            var chk_id = $(this).find('.chk').attr('data-id');
            if ($(this).find('.chk').attr('checked') == true || $(this).find('.chk').attr('checked') == 'checked') {
                $(this).removeClass('selected');
                $(this).find('.chk').attr('checked', false);
            } else {
                $(this).addClass('selected');
                $(this).find('.chk').attr('checked', true);
            }
            e.stopImmediatePropagation();
        });

        // 全选
        $(document).on('click', ".selectAll", function () {
            $(this).addClass('current');
            $(this).siblings('.btn-light').removeClass('current');
            $(".list .listData .item input.chk").attr("checked", true);
            $('.list .listData .item').addClass('selected');
        });
        // 全不选
        $(document).on('click', ".selectNone", function () {
            $(this).siblings('.btn-light').removeClass('current');
            $(".list .listData .item input.chk").attr("checked", false);
            $('.list .listData .item').removeClass('selected');
        });
        // 反选
        $(document).on('click', ".selectInvert", function () {
            $(this).addClass('current');
            $(this).siblings('.btn-light').removeClass('current');
            $(".list .listData .item input.chk").each(function () {
               // $(this).prop("checked", !this.checked);//反选
                var chk_id = $(this).prop('data-id');
                if ($(this).prop('checked') == true || $(this).prop('checked') == 'checked') {
                    $(this).parents('.item[data-id=' + chk_id + ']').addClass('selected');
                } else {
                    $(this).parents('.item[data-id=' + chk_id + ']').removeClass('selected');
                }
            });
        });
    }


    /**
     * del post
     * @param btn
     */
    exports.delPost = function (btn) {
        $("#article-list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/article/del', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！',3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };
    /**
     * del post
     * @param btn
     */
    exports.delAllPost = function (btn) {
        $("#article-list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/article/del', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！',3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * mv cat and their children to new parent
     * @param btn
     */
    exports.mvPost = function (btn) {
        $("#article-list").on('click', btn, function (e) {
            // params
            var toCid = $('select.mvPostCat').val();

            if (isNaN(toCid) || toCid == '') {
                base.showTip('err', '请选择栏目！', 3000);
                return false;
            }
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要移动的项', 3000);
                return;
            }

            // confirm
            var cm = window.confirm('你确定需要移动选中的文章到新分类吗？');
            if (!cm) {
                return;
            }

            // disable the button
            $(btn).attr('disabled', true);
            // api request
            base.requestApi('/api/article/mvPost', {'data': data, 'cid': toCid}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！',3000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    function checkNull(elem, msg) {
        if ($(elem).val() == '') {
            $(elem).siblings('.help-inline').html(msg);

            return false;
        } else {
            $(elem).siblings('.help-inline').html('');
        }

        return true;
    }
});