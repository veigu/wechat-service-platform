"use strict";
window.alert = BSDialog.alert;
window.confirm = BSDialog.confirm;
$(document).ready(function() {
    $('#time_start').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 0,
        format: "yyyy-mm-dd",
        minView: 2
    });
    $('#time_end').datetimepicker({
        language:  'zh-CN',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 0,
        format: "yyyy-mm-dd",
        minView: 2
    });
    var total = parseInt($('#ruleTotalNum').text());
    if(total == 0) {
        initModalFormData();
        $('#ruleInfoModal').modal('show');
    }

    $('#addRuleTrigger').click(function() {
        initModalFormData();
    });

    $('#saveRule').bind('click', function() {
        var rid = $('#rid').val();
        var name = $('#name').val();
        var desc = $('#desc').val();
        var consume_start = parseFloat($('#consume_start').val());
        var consume_end = parseFloat($('#consume_end').val());
        var discount = parseFloat($('#discount').val());
        var time_start = $('#time_start').val();
        var time_end = $('#time_end').val();
        if(!name || name.length == 0) {
            alert("规则名称必须填写", function() {
                $('#name').focus();
            });
            return false;
        }
        if(!desc || desc.length == 0) {
            alert("规则描述必须填写", function() {
                $('#desc').focus();
            });
            return false;
        }
        if(!consume_start || !consume_end || consume_start > consume_end) {
            alert("消费金额必须填写，且起始值必须小于终止值", function() {
                $('#consume_start').focus();
            });
            return false;
        }
        if(!discount || discount <= 0) {
            alert("折扣填写错误，范围在0~100，不包含0", function() {
                $('#discount').focus();
            });
            return false;
        }
        if(consume_start > consume_end) {
            alert("结束时间不得大于开始时间", function() {
                $('#consume_start').focus();
            });
            return false;
        }

        var data = {};
        if(rid && rid > 0) {
            data.rid = rid;
        }
        data.name = name;
        data.desc = desc;
        data.consume_start = consume_start;
        data.consume_end = consume_end;
        data.discount = discount;
        data.time_start = time_start;
        data.time_end = time_end;

        $.ajax({
            url: '/panel/users/groupRuleAdd',
            dataType: 'json',
            type: 'post',
            data: data
        }).done(function(data) {
            if(data.code > 0) {
                alert("规则添加失败！" + data.message);
                return false;
            }
            else {
                alert("规则添加成功!", function() {
                    $('#ruleInfoModal').modal('hide');
                    window.location.href = window.location.href;
                });
            }
        });
    });

    $('.item-edit').bind('click', function() {
        var data = JSON.parse($(this).attr('data-data'));
        $('body').data('currentRule', data);
        initModalFormData(data);
    });

    $('.item-remove').bind('click', function() {
        var rid = $(this).attr('data-id');
        if(!rid || rid <= 0) {
            alert("请指定要删除的规则");
            return false;
        }
        confirm("确定删除此规则？", [
            {
                label: "放弃",
                cssClass: "btn-default"
            },
            {
                label: '确定',
                cssClass: 'btn-danger',
                onclick:function(e) {
                    $.ajax({
                        url: "/panel/users/groupRuleRemove",
                        type: "post",
                        dataType: 'json',
                        data: {
                            rid: rid
                        }
                    }).done(function(data) {
                        alert("删除成功!", function() {
                            window.location.href = window.location.href;
                        });
                    });
                }}]);
    });

    $('#ruleInfoModal').bind("hide.bs.modal", function() {
        $('body').data('currentRule', false);
    });
});


function initModalFormData(data) {
    if(!data) {
        data = $('body').data('currentRule');
    }
    if(!data) {
        $('#rid').val('');
        $('#name').val('');
        $('#desc').val('');
        $('#consume_start').val('');
        $('#consume_end').val('');
        $('#discount').val('');
        $('#time_start').val('');
        $('#time_end').val('');
    }
    else {
        $('#rid').val(data.id);
        $('#name').val(data.name);
        $('#desc').val(data.description);
        $('#consume_start').val(data.consume_start);
        $('#consume_end').val(data.consume_end);
        $('#discount').val(data.discount);
        $('#time_start').val(data.time_start);
        $('#time_end').val(data.time_end);
    }
}