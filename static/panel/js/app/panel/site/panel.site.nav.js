define(function (require, exports) {
    var base = require('app/panel/panel.base');
    base.selectNone();
    base.selectCheckbox();
    var link = require('app/panel/panel.linkChoose');
    var storage = require('app/panel/panel.storage');

    var appIcon = require('app/app.icon');
    var color = require('app/app.color');

    /**
     * del nav
     * @param btn
     */
    exports.delNav = function (btn) {
        $("table.list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/site/delNav', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 1000);
                    window.location.reload();
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del all nav
     * @param btn
     */
    exports.delAllNav = function (btn) {
        $("table.list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/site/delNav', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！', 2000);
                    window.location.reload();
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * set nav
     * -- update or add
     */
    exports.setNav = function () {

        storage.getImg('.getCoverBtn', function func(res) {
            $('#navModal #cover_img').val(res.url);
            $('.preview_img').attr('src', res.url);
        }, false);


        // update event
        $('.upBtn').click(function (e) {
            var id = $(this).attr('data-id');
            // api request
            base.requestApi('/api/site/getNav', {'id': id}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '获取数据成功！', 1000);
                    $('#navModal').modal('show');


                    setModal(res.data);
                }
            });

            e.stopImmediatePropagation();
        });

        // add event
        $('.addBtn').click(function (e) {
            resetModal();
        });

        // set icon
        appIcon.setIcon('.icon-preview', '.icon-wrap', function (icon) {
            $('#navModal #icon').val(icon);
        });

        // set link
        link.getLink('.getLinkUrl', function (link, type) {
            $('#navModal #link').val(link);
            $('#navModal #link_type').val(type);
        });

        // save event
        save();
    };


    function setModal(data) {
        // set color
        color.pick(data.color, function (colorCls, hex) {
            $('#navModal #color').val(colorCls);
        });
        $('#navModal').find('#color').val(data.color);

        // set icon
        var icon = data.icon;
        $('.icon-preview').removeClass().addClass('icon-preview ' + icon);
        $('#navModal').find('#icon').val(data.icon);

        $('#navModal').find('#cover_img').val(data.cover_img);
        $('#navModal').find('.preview_img').attr('src', data.cover_img);
        // 二级分类
        $('#navModal').find('#parent_id option').show();
        $('#navModal').find('#parent_id option[value="' + data.id + '"]').hide();
        if (data.parent_id > 0) {
            $('#navModal').find('#parent_id option[value="' + data.parent_id + '"]').prop('selected', true);
        }

        $('#navModal').find('#name').val(data.name);
        $('#navModal').find('#sub_name').val(data.sub_name);
        $('#navModal').find('#detail').val(data.detail);
        $('#navModal').find('#link').val(data.link);
        $('#navModal').find('#link_type').val(data.link_type);
        $('#navModal').find('#sort').val(data.sort);
        $('#navModal').find('.saveBtn').attr('data-id', data.id);

        // radio
        $('#navModal').find('.published').attr("checked", false);
        $('#navModal').find('.published[value="' + data.published + '"]').attr("checked", true).parent('label').addClass('active');
    }

    function resetModal() {
        // set color
        color.pick('color117', function (colorCls, hex) {
            $('#navModal #color').val(colorCls);
        });
        $('#navModal').find('#color').val('color117');

        // set icon
        $('.icon-preview').removeClass().addClass('icon-preview icon-sun');
        $('#navModal').find('#icon').val('icon-sun');

        $('#navModal').find('#name').val('');
        $('#navModal').find('#sub_name').val('');
        $('#navModal').find('#detail').val('');
        $('#navModal').find('#link').val('');
        $('#navModal').find('#link_type').val('');
        $('#navModal').find('#sort').val(0);
        $('#navModal').find('.saveBtn').removeAttr('data-id');

        // radio
        $('#navModal').find('.published').attr("checked", false);
        $('#navModal').find('.published[value="1"]').attr("checked", true).parent('label').addClass('active');
    }

    function save() {
        $('#navModal .saveBtn').click(function (e) {
            var name = $('#navModal').find('#name').val();
            var sub_name = $('#navModal').find('#sub_name').val();
            var detail = $('#navModal').find('#detail').val();
            var icon = $('#navModal').find('#icon').val();
            var color = $('#navModal').find('#color').val();
            var cover_img = $('#navModal').find('#cover_img').val();
            var link = $('#navModal').find('#link').val();
            var link_type = $('#navModal').find('#link_type').val();
            var sort = $('#navModal').find('#sort').val();
            var published = $('#navModal').find('.published:checked').val();
            var position = $('#navModal').find('#position').val();
            var parent_id = $('#navModal').find('#parent_id option:selected').val();


            var id = $(this).attr('data-id');

            if (!checkNull('#navModal #name', '分类名称不能为空')) {
                return false;
            }

            if (!checkNull('#navModal #link', '链接不能为空')) {
                return false;
            }

            if (!checkNull('#navModal #icon', '请选择图标')) {
                return false;
            }
           // var page_type=$('#page_type').val();
            var data = {
                name: name,
                sub_name: sub_name,
                detail: detail,
                icon: icon,
                color: color,
                cover_img: cover_img,
                link: link,
                link_type: link_type,
                sort: sort,
                parent_id: parent_id,
                position: position,
                published: published,
             //   page_type:page_type
            };

            // api request
            base.requestApi('/api/site/saveNav', {'data': data, id: id}, function (res) {
                if (res.result == 1) {
                    $('#navModal').modal('hide');
                    window.location.reload();
                }
            });

            e.stopImmediatePropagation();
        });
    }

    function checkNull(elem, msg) {
        if ($(elem).val() == '') {
            $(elem).siblings('.help-inline').html(msg);

            return false;
        } else {
            $(elem).siblings('.help-inline').html('');
        }

        return true;
    }

});