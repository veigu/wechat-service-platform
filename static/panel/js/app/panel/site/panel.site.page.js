define(function (require, exports) {
    var base = require('app/panel/panel.base');
    base.selectNone();
    base.selectCheckbox();
    var storage = require('app/panel/panel.storage');

    /**
     * del Page
     * @param btn
     */
    exports.delPage = function (btn) {
        $("table.list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/site/delPage', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！', 3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del all Page
     * @param btn
     */
    exports.delAllPage = function (btn) {
        $("table.list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/site/delPage', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                }
            });
            e.stopImmediatePropagation();
        });
    };


    exports.savePage = function (page_type) {
        // 初始化上传
        storage.getImg('#upCoverIcon', function (res) {
            $('#cover').val(res.url);
            $('.imgPreview').attr('src', res.url).show();
        });

        // 初始化编辑器
        var editor = require('app/app.editor');
        editor.init('#txtContent');
       $("#sort").on('keyup',function(){
            $(this).val($(this).val().replace(/\D/g,''));
            if($(this).val()>255)
            {
                ($(this).val(255));
            }

        });
        $('#pageForm .saveBtn').click(function (e) {
            var img = $('#pageForm').find('#cover').val();
            var name = $('#pageForm').find('#name').val();
            var intro = $('#pageForm').find('#intro').val();
            var content = $('#pageForm').find('#txtContent').val();
            var sort = $('#pageForm').find('#sort').val();
            var published = $('#pageForm').find('.published:checked').val();

            var id = $(this).attr('data-id');

            if (!checkNull('#pageForm #name', '名称不能为空')) {
                return false;
            }

            var data = {
                cover: img,
                name: name,
                intro: intro,
                content: content,
                sort: sort,
                published: published
            };

            // api request
            base.requestApi('/api/site/savePage', {'data': data, id: id}, function (res) {
                if (res.result == 1) {
                    if (page_type == 'update') {
                        window.location.reload();
                        base.showTip('ok', '数据更新成功！', 3000);
                    } else {
                        window.location.href = '/panel/site/page';
                        base.showTip('ok', '数据插入成功！', 3000);
                    }
                }
            });

            e.stopImmediatePropagation();
        });
    };

    function checkNull(elem, msg) {
        if ($(elem).val() == '') {
            $(elem).siblings('.help-inline').html(msg);

            return false;
        } else {
            $(elem).siblings('.help-inline').html('');
        }

        return true;
    }

});