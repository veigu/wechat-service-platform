define(function (require, exports) {
    var base = require('app/panel/panel.base');
    base.selectNone();
    base.selectCheckbox();
    var link = require('app/panel/panel.linkChoose');
    var storage = require('app/panel/panel.storage');

    /**
     * del Focus
     * @param btn
     */
    exports.delFocus = function (btn) {
        $("table.list .listData").on('click', btn, function (e) {
            // params
            var id = $(this).attr('data-id');
            var data = [id];
            // confirm
            var cm = window.confirm('你确定需要该条数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/site/delFocus', {data: data}, function (res) {
                if (res.result == 1) {
                    $('.list .listData .item[data-id="' + id + '"]').fadeOut();
                    setTimeout(function () {
                        $('.list .listData .item[data-id="' + id + '"]').remove();
                    }, 1000);
                    base.showTip('ok', '删除成功！',3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * del all Focus
     * @param btn
     */
    exports.delAllFocus = function (btn) {
        $("table.list").on('click', btn, function (e) {
            var data = [];
            $(".list .listData input.chk").each(function () {
                if ($(this).attr('checked') == true || $(this).attr('checked') == 'checked') {
                    data.push($(this).attr('data-id'));
                }
            });

            //  has no selected
            if (data.length == 0) {
                base.showTip('err', '请选择需要删除的项', 3000);
                return;
            }
            // confirm
            var cm = window.confirm('你确定需要删除选中的数据吗？');
            if (!cm) {
                return;
            }

            // api request
            base.requestApi('/api/site/delFocus', {'data': data}, function (res) {
                if (res.result == 1) {
                    for (var i = 0; i < data.length; i++) {
                        $('.listData .item[data-id="' + data[i] + '"]').remove();
                    }
                    base.showTip('ok', '操作成功！',3000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    /**
     * set Focus
     * -- update or add
     */
    exports.setFocus = function () {
        storage.getImg('.getImg', function (res) {
            $('#focusModal #img').val(res.url);
            $('#focusModal .previewImg').attr('src', res.url).show();
            $('#focusModal #name').val(res.name);

        }, false);

        // update event
        $('.upBtn').click(function (e) {
            var id = $(this).attr('data-id');
            // api request
            base.requestApi('/api/site/getFocus', {'id': id}, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '获取数据成功！',1000);
                    $('#focusModal').modal('show');
                    setModal(res.data);
                }
            });

            e.stopImmediatePropagation();
        });

        // add event
        $('.addBtn').click(function () {
            resetModal();
        });

        // set link
        link.getLink('.getLinkUrl', function (link, type) {
            $('#focusModal #link').val(link);
            $('#focusModal #link_type').val(type);
        });

        // save event
        save();
    };
    function setModal(data) {

        $('#focusModal').find('#name').val(data.name);
        $('#focusModal').find('#img').val(data.img);
        $('#focusModal').find('.previewImg').attr('src', data.img).show();
        $('#focusModal').find('#link').val(data.link);
        $('#focusModal').find('#link_type').val(data.link_type);
        $('#focusModal').find('#sort').val(data.sort);
        $('#focusModal').find('.saveBtn').attr('data-id', data.id);

        // radio
        $('#focusModal').find('.published').attr("checked", false);
        $('#focusModal').find('.published[value="' + data.published + '"]').attr("checked", true).parent('label').addClass('active');
    }

    function resetModal() {


        $('#focusModal').find('#name').val('');
        $('#focusModal').find('#img').val('');
        $('#focusModal').find('.previewImg').attr('src', '').hide();
        $('#focusModal').find('#link').val('');
        $('#focusModal').find('#link_type').val('');
        $('#focusModal').find('#sort').val(0);
        $('#focusModal').find('.saveBtn').removeAttr('data-id');

        // radio
        $('#focusModal').find('.published').attr("checked", false);
        $('#focusModal').find('.published[value="1"]').attr("checked", true).parent('label').addClass('active');
    }

    function save() {
        $('#focusModal .saveBtn').click(function (e) {
            var img = $('#focusModal').find('#img').val();
            var name = $('#focusModal').find('#name').val();
            var detail = $('#focusModal').find('#detail').val();
            var link = $('#focusModal').find('#link').val();
            var link_type = $('#focusModal').find('#link_type').val();
            var sort = $('#focusModal').find('#sort').val();
            var published = $('#focusModal').find('.published:checked').val();

            var id = $(this).attr('data-id');

            if (!checkNull('#focusModal #img', '请上传图片')) {
                return false;
            }

            if (!checkNull('#focusModal #name', '名称不能为空')) {
                return false;
            }


            if (!checkNull('#focusModal #link', '链接不能为空')) {
                return false;
            }
          //  var page_type=$('#page_type').val();


            var data = {
                img: img,
                name: name,
                detail: detail,
                link: link,
                link_type: link_type,
                sort: sort,
                published: published,
             //   page_type:page_type
            };

            // api request
            base.requestApi('/api/site/saveFocus', {'data': data, id: id}, function (res) {
                if (res.result == 1) {
                    $('#focusModal').modal('hide');
                    window.location.reload();
                }
            });

            e.stopImmediatePropagation();
        });
    }

    function checkNull(elem, msg) {
        if ($(elem).val() == '') {
            $(elem).siblings('.help-inline').html(msg);

            return false;
        } else {
            $(elem).siblings('.help-inline').html('');
        }

        return true;
    }

});