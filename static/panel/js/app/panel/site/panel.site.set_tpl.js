define(function (require, exports) {
    var base = require('app/panel/panel.base');

    exports.setTpl = function () {
        $('.tpl-content .tpl').click(function (e) {
            var cat = $(this).attr('data-cat');
            var industry = $(this).attr('data-industry');
            var serial_number = $(this).attr('data-serial_number');
            var type = $(this).attr('data-type');
            var id = $(this).attr('data-id');

            // remove default
            $('.tpl-content .tpl').find('label.active').removeClass('active').find('input.tplChoose').attr('checked', false);

            // add active
            $(this).find('label').addClass('active').find('input.tplChoose').attr('checked', true);

            var data = {
                id: id,
                type: type,
                data: {
                    cat: cat,
                    serial_number: serial_number
                }
            };

            // api request
            base.requestApi('/api/site/setTpl', data, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '模板选择成功！', 2000)
                }
            });

            e.stopImmediatePropagation();
        });
    };

    exports.changeMode = function () {
        $('.changeModeBtn').on('click', function (e) {
            var mode = $(this).attr('data-mode');
            var data = {'mode': mode};
            var cm = window.confirm('您确认需要切换模板方式吗？');
            if (!cm) {
                return;
            }
            // api request
            base.requestApi('/api/site/setTplMode', data, function (res) {
                if (res.result == 1) {
                    base.showTip('ok', '切换模板方式成功！', 2000)
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            });

            e.stopImmediatePropagation();
        });
    };
});