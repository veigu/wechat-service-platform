/*------------------------   index-top   -----------------------*/
$(function () {

    var sly_layer = null;

    $(".nm_advertising").click(function () {
        $(".notice_close").show();
        $(".notice_layer").animate({ "top": "0" }, "2000", "easeOutExpo");
        $("body").css({ 'overflow': 'hidden' });
        $(".blur").addClass("actived");

        $("#" + $("#first_notification").val()).parent().addClass("hovered").siblings().removeClass("hovered");//通知列表样式
        jQuery(function ($) {
            'use strict';
            var obj = {
                speed: 300,
                easing: 'easeOutExpo',
                activatePageOn: 'click',
                scrollBar: null,
                scrollBy: 143,
                dragHandle: 1,
                dynamicHandle: 1,
                clickBar: 1,
                activateOn: 'click',
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1
            };


            (function () {
                var $frame = $('#notice_layer');
                var $wrap = $frame.parent();
                $frame.sly($.extend({}, obj, { scrollBar: $wrap.find('.scrollbar'), scrollBy: 50 }));
            }());

            (function () {
                var $frame = $('#notice_layer_con');
                var $wrap = $frame.parent();

                sly_layer = new Sly($frame, $.extend({}, obj, { scrollBar: $wrap.find('.scrollbar'), scrollBy: 100 })).init();


            }());


        });
    })


    $(".notice_close").click(function () {
        $(this).hide();
        $("body").css({ 'overflow': 'auto' });
        $(".blur").removeClass("actived");

        $(".notice_layer").animate({ "top": "-100%" }, "2000", "easeOutExpo", function () {
            $(this).css({ 'top': '100%' });
        })
    })


    /*------------------------   index-box  -----------------------*/

    //通知列表 标题点击事件
    $("#notificationTitleList a").click(function () {
        var title_id = $(this).attr("id");
        $(this).parent().addClass("hovered").siblings().removeClass("hovered");//通知标题样式
        show(title_id);
        return false;
    });

    //显示通知内容
    function show(id) {
        $(".spinner").show();
        $.ajax({
            url: "Index.aspx",
            data: "action=GetNotifactionInfo&id=" + id,
            datatype: 'text',
            success: function (msg) {

                console.log(msg)
                $("#notificationContent").html(msg);
                $(".spinner").hide();

                $("#notificationContent img").load(function () {
                    sly_layer.destroy().init();
                });
            },
            error: function (a, b, c) {
                alert(c);
            }
        });
    }


    //services
    $(".serBox").hover(
        function () {
            $(this).children().stop(false, true);
            $(this).children(".serBoxOn").fadeIn("slow");
            $(this).children(".pic1").animate({ right: -110 }, 400);
            $(this).children(".pic2").animate({ left: 41 }, 400);
            $(this).children(".txt1").animate({ left: -240 }, 400);
            $(this).children(".txt2").animate({ right: 0 }, 400);
        },
        function () {
            $(this).children().stop(false, true);
            $(this).children(".serBoxOn").fadeOut("slow");
            $(this).children(".pic1").animate({ right: 41 }, 400);
            $(this).children(".pic2").animate({ left: -110 }, 400);
            $(this).children(".txt1").animate({ left: 0 }, 400);
            $(this).children(".txt2").animate({ right: -240 }, 400);
        }
    );

});

//tab
var curPortflioi = "page21";
var nn;
var curNumi = 1;
var jj = 0;
function showPagei(idi, nn, i) {
    for (var j = 1; j <= nn; j++) {
        if (j == idi) {
            $("#box" + i + j).removeClass();
            $("#box" + i + j).addClass("tab1");
            $("#page" + i + j).fadeIn(400);
            if (i == 2) {

                curPortflioi = "page" + i + j;
            }
        } else {
            $("#box" + i + j).removeClass();
            $("#box" + i + j).addClass("tab2");
            //$("#page"+i+j).fadeOut("fast");
            $("#page" + i + j).hide();
        }
    }

}
/*------------------------   案列滚动  -----------------------*/
function index() {
    this.init();
}
index.prototype = {
    init: function () {

        if ($('#case .caseScroll .box li').length > 4) {
            this.cases();
        }
    },
    cases: function () {
        var t2 = null;
        var $caseScroll = $('#case .caseScroll'),
            $btn = $('.btn span', $caseScroll),
            $box = $('.box ul', $caseScroll);
        var len = $box.find('li').length;
        $box.width(len * 135);
        var maxLeft = len * 135;
        for (var i = 0; i < len; i++) {
            $box.find('li').eq(i).css({'left': i * 135 + 'px'});
        }
        function auto() {
            t2 = setInterval(function () {
                $box.addClass('moving');
                $box.find('li').animate({'left': '-=135'}, 500, function () {
                    if (parseInt($(this).css('left')) < 0) {
                        $(this).css({'left': '+=' + maxLeft});
                    }
                    $box.removeClass('moving');
                });
            }, 3000)
        }

        auto();
        $btn.on('click', function () {
            var active = $(this).attr('class');
            clearInterval(t2);
            if (!$box.hasClass('moving')) {
                if (active == 'btn_left') {
                    $box.find('li').each(function () {
                        if (parseInt($(this).css('left')) >= (4 * 135)) {
                            $(this).css({'left': '-=' + maxLeft});
                        }
                    });
                    $box.addClass('moving');
                    $box.find('li').animate({'left': '+=135'}, 500, function () {
                        if (parseInt($(this).css('left')) >= (4 * 135)) {
                            $(this).css({'left': '-=' + maxLeft});
                        }
                        $box.removeClass('moving');
                    });
                } else {
                    $box.find('li').each(function () {
                        if (parseInt($(this).css('left')) < 0) {
                            $(this).css({'left': '+=' + maxLeft});
                        }
                    });
                    $box.addClass('moving');
                    $box.find('li').animate({'left': '-=135'}, 500, function () {
                        if (parseInt($(this).css('left')) < 0) {
                            $(this).css({'left': '+=' + maxLeft});
                        }
                        $box.removeClass('moving');
                    });
                }
            }
            auto();
        })
    }
}
$(document).ready(function () {
    new index();
});
/*----------------------------------- banner  ---------------------------------*/

/*----------------------------    -------------------------*/
$(document).ready(function () {

    $(".tab_menu li:first").children('img').attr('src', '/static/home/pc/estt/images/index_more_g1.png');

    $(".tab_menu li").each(function (index) {
        $(this).mouseover(function () {
            var i = $(".tab_menu .selected").index() + 1;
            $(".tab_menu .selected").removeClass("selected").children('img').attr('src', '/static/home/pc/estt/images/index_more_h' + i + '.png');
            $('div.tab_box > div').eq(index).show().siblings().hide();
            $(this).addClass("selected").children('img').attr('src', '/static/home/pc/estt/images/index_more_g' + (index + 1) + '.png');

        });
    })
});
/*----------------------   服务项目  --------------------------*/
$(function () {
    //定义相应位置信息
    var posArr = [];
//        var leftBarOriPos = parseInt($("#product_left").css('top')); //get 0 returned
    var leftBarOriPos = 730;
    var scrollSpyOpen = true; //是否开始滚动监听，当点击左侧菜单导航时，不监听

    function init_position() {
        $('[data-toggle="scroll-spy"][data-type="item"]').each(function() {
            var ori_top = parseInt($(this).offset().top);
            posArr.push(ori_top);
        });
        posArr = posArr.sort(function(a, b) { if(a > b) {return true;} else {return false;}});
    }
    init_position();
    //绑定滚动监听事件
    window.onscroll = function () {
        //获取当前滚动距离
        var top = document.documentElement.scrollTop || document.body.scrollTop;

        //设置左侧导航的位置
        if(top > leftBarOriPos) {
            $('#product_left').css("position", 'absolute  ').css("top", "50px");
        }
//            else if(top < leftBarOriPos - 640) {
//                $('#product_left').css("position", 'fixed').css("bottom", "0");
//            }
        else {
            $('#product_left').css("position", 'relative').css("top", "0");
        }

        if(!scrollSpyOpen) {
            return false;
        }

        //设置左侧导航高亮item
        var curIndex = 1;
        for(var index in posArr) {
            var item = posArr[index];
            if(item && item <= top + 5) {
                curIndex ++;
            }
            else {
                curIndex --;
                break;
            }
        }

        if(curIndex > posArr.length) {
            curIndex = posArr.length;
        }
        else if(curIndex <= 0) {
            curIndex = 1;
        }
        $("[data-toggle='scroll-spy'][data-type='nav']").removeClass("active");
        $("[data-toggle='scroll-spy'][data-target='position_" + curIndex + "']").addClass("active");
    };

    //绑定左侧导航点击事件
    $("[data-toggle='scroll-spy'][data-type='nav']").click(function() {
        var curIndex = parseInt($(this).attr('data-target').substring(9));
        var scrollTop = posArr[curIndex - 1];
        scrollSpyOpen = false;
        $("[data-toggle='scroll-spy'][data-type='nav']").removeClass("active");
        $("[data-toggle='scroll-spy'][data-target='position_" + curIndex + "']").addClass("active");
        $("html,body").animate({scrollTop:scrollTop - 50}, 500);
        setTimeout(function() {
            scrollSpyOpen = true;
        }, 1000);
    });

    $('.serve_tab').on("click", function (e) {
        var tab = $(this).attr("data-tab");
        // remove all
        $(".serve_tab").removeClass("active");
        // add current
        $(this).addClass("active");
        if (tab == 'all') {
            $(".serBox").show();
        } else if(tab == 'ok') {
            $(".serBox").hide();
            $(".ok").show();
        }else if(tab == 'ing') {
            $(".serBox").hide();
            $(".ing").show();
            $(".dev").hide();
        }

        //init_position();
        e.stopImmediatePropagation();
    });

})









