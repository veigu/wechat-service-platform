$(function () {
   // $('.key_com').val(window.location.href);
    var options = {
        'title': '',
        'desc': '',
        'pics': '',
        'comment': ''
    };

    function AppShare() {

    }

    AppShare.prototype = {

        // 初始化
        'init': function (param) {
            var _this = this;
            options.title = "迪诺雅10周年庆，朋友们快来围观吧.....";
            options.desc = "迪诺雅10周年庆，朋友们快来围观吧.....";
            options.url = location.href;

            $('.tit').on('click', '.shareBtn', function (e) {
                var share = $(this).attr('data-share');
                options.desc = $('#share_content').val();

                eval("_this." + share + "()");

                e.stopImmediatePropagation();
            });
        },

        // 新浪微博
        'wbshare': function () {
            var submitUrl = 'http://service.weibo.com/share/share.php';
            var wbShareparam = {
                url: options.url,
                type: '3',
                appkey: '207042031',
                title: options.desc,
                pic: options.pics[0],
                ralateUid: '@looklo',
                language: 'zh-cn',
                rnd: new Date().valueOf()
            };
            this._openUrl(wbShareparam, submitUrl);
            return false;
        },

        // 人人网
        'rrshare': function (param) {
            var rrShareParam = {
                url: options.url, // 默认为header中的Referer,如果分享失败可以调整此值为resourceUrl试试
                title: options.title,
                content: options.desc,
                image_src: options.pics
            };
            var submitUrl = 'http://www.connect.renren.com/sharer.do';
            this._openUrl(rrShareParam, submitUrl);
            return false;
        },

        // qq空间
        'qqshare': function (param) {
            var qqShareparam = {
                url: options.url,
                desc: options.desc,
                summary: '仁豪家具 - 迪诺雅', /* 分享摘要(可选) */
                title: options.title,
                site: '迪诺雅', /* 分享来源 如：腾讯网(可选) */
                pics: options.pics
            };
            var submitUrl = 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey';
            this._openUrl(qqShareparam, submitUrl);
            return false;
        },

        // 腾讯微博
        'tqshare': function (param) {
            var tqShareparam = {
                c: 'share',
                a: 'index',
                url: options.url,
                desc: options.desc,
                title: options.title,
                site: '迪诺雅', /* 分享来源 如：腾讯网(可选) */
                pic: options.pics[0], /* 分享图片的路径(可选) */
                appkey: '801242612'
            };
            var submitUrl = 'http://share.v.t.qq.com/index.php';
            this._openUrl(tqShareparam, submitUrl);
            return false;
        },

        // 淘宝
        'taoshare': function (param) {
            var tqShareparam = {
                url: location.href,
                desc: options.desc,
                title: options.title,
                site: '迪诺雅', /* 分享来源 如：腾讯网(可选) */
                pics: options.pics, /* 分享图片的路径(可选) */
                appkey: '21180828'

            };
            var submitUrl = 'http://share.jianghu.taobao.com/share/addShare.htm';
            this._openUrl(tqShareparam, submitUrl);
            return false;
        },

        // 百度
        'bdshare': function (param) {
            var tqShareparam = {
                url: location.href,
                content: options.desc,
                title: options.title,
                linkid: 'yansueh',
                pics: options.pics, /* 分享图片的路径(可选) */
                appkey: ''
            };
            var submitUrl = 'http://hi.baidu.com/pub/show/share';
            this._openUrl(tqShareparam, submitUrl);
            return false;
        },

        // 豆瓣网
        'dbshare': function () {
            var tqShareparam = {
                href: options.url,
                desc: options.desc,
                name: options.title,
                linkid: 'looklo',
                image: options.pics[0],
                appkey: '0e1f49fc92bcafab2a5dddf5b8360f01'
            };
            var submitUrl = 'http://shuo.douban.com/!service/share';
            this._openUrl(tqShareparam, submitUrl);
            return false;
        },

        // 开心网
        'kxshare': function () {
            var shareparam = {
                url: options.url,
                content: options.desc,
                style: 11,
                time: new Date().valueOf(),
                sig: '',
                pic: options.pics
            };
            var submitUrl = 'http://www.kaixin001.com/rest/records.php';
            this._openUrl(shareparam, submitUrl);
            return false;
        },

        // 网易微博
        't163share': function () {
            var shareparam = {
                info: options.desc,
                source: options.url,
                images: options.pics
            };
            var submitUrl = 'http://t.163.com/article/user/checkLogin.do';
            this._openUrl(shareparam, submitUrl);
            return false;
        },

        // 搜狐微博
        'shshare': function () {
            var shareparam = {
                url: options.url,
                title: options.title,
                content: options.desc,
                pic: options.pics
            };
            var submitUrl = 'http://t.sohu.com/third/post.jsp';
            this._openUrl(shareparam, submitUrl);
            return false;
        },


        // 公用打开窗口
        '_openUrl': function (shareparam, submitUrl) {
            var temp = [];
            for (var p in shareparam) {
                temp
                    .push(p + '='
                        + encodeURIComponent(shareparam[p] || ''));
            }
            var url = submitUrl + "?" + temp.join('&');
            console.log(url);
            var wa = 'width=700,height=650,left=0,top=0,resizable=yes,scrollbars=1';
            window.open(url, 'looklo', wa);
        }

        // end
    };


    var app = new AppShare();
    app.init();
});