/**
 * Created by Arimis on 14-5-22.
 */
"use strict";

$(document).ready(function() {
    $('#postBtn').click(function() {
        var name = $('#contact_name').val();
        if(!name) {
            alert("姓名必须填写");
            $('#contact_name').focus();
            return false;
        }

        var email = $('#contact_email').val();
        if(!email || !validateEmail(email)) {
            alert("邮件必须按格式填写");
            $('#contact_email').focus();
            return false;
        }

        var phone = $('#contact_phone').val();
        if(!phone) {
            alert("电话必须填写");
            $('#contact_phone').focus();
            return false;
        }

        var message = $('#contact_message').val();
        if(!message || message.length < 10) {
            alert("建议不少于10个字符");
            $('#contact_message').focus();
            return false;
        }

        $.ajax({
            url: '/home/contact/saveMessage',
            type: 'post',
            dataType: 'json',
            data: {
                contact_name: name,
                contact_email: email,
                contact_phone: phone,
                contact_message: message
            }

        }).done(function(data) {
            alert(data.message);
            if(!data.code) {
                $('#contact_name').val('');
                $('#contact_email').val('');
                $('#contact_phone').val('');
                $('#contact_message').val('');
            }
        });
    });
});

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}