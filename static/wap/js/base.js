
/**
 * ajax tip
 *
 * @constructor
 */
window.inAjaxProcess = false;

function ajaxTip() {

}

ajaxTip.ajaxTimer = null;
ajaxTip.tip = null;
ajaxTip.time = 0;
ajaxTip.status = null;
ajaxTip.prototype = {
    showTip: function (status, tip, time) {
        ajaxTip.status = status;
        ajaxTip.tip = tip;
        ajaxTip.time = time;

        $('#ajaxStatus').show();
        $('#ajaxStatus #ajaxTip').html(ajaxTip.tip).removeClass().addClass(ajaxTip.status);


        if (ajaxTip.time) {
            if (ajaxTip.ajaxTimer) {
                clearTimeout(ajaxTip.ajaxTimer);
            }
            ajaxTip.ajaxTimer = setTimeout(function () {
                $('#ajaxStatus').hide();
                ajaxTip.inProcess = true;
            }, ajaxTip.time);
        }
    },
    hideTip: function () {
        $('#ajaxStatus').hide();
    }
};
window.tip = new ajaxTip();

window.requestApi = function (uri, data, func, endProcess) {

    // 手动更改请求，立即结束ajax状态
    if (endProcess) {
        window.inAjaxProcess = false;
    }

    var param = {
        url: uri,
        async: true,
        data: data,
        dataType: 'json',
        type: 'post',
        beforeSend: function () {
            tip.showTip('wait', '处理请求...');
            if (window.inAjaxProcess) {
                tip.showTip('wait', '正在请求...');
                return false;
            }
            // 正在处理状态
            window.inAjaxProcess = true;
        },
        timeout: function () {
            tip.showTip('err', '请求超时,请重试！', 2000);
        },
        abort: function () {
            tip.showTip('err', '网路连接被中断！', 2000);
        },
        parsererror: function () {
            tip.showTip('err', '运行时发生错误！', 2000);
        },
        error: function () {
            tip.showTip('err', '运行错误，请重试！', 2000);
        },
        complete: function () {

            setTimeout(function () {
                if ($('#ajaxStatus').css('display') !== 'none') {
                    tip.showTip('ok', '操作成功！', 2000);
                }

                // 清除处理状态
                window.inAjaxProcess = false;
            }, ajaxTip.time);// 最后一次tip时间
        },
        success: function (res) {
            if (typeof func == 'function') {
                func(res);
            }

            if (res.error.code != 0) {
                tip.showTip('err', res.error.msg + ' ' + res.error.more, 3000);
            }
        }
    };

    $.ajax(param);
};

function checkField(elem, msg, regx) {
    var val = $(elem).val();
    if ($(elem).val() == '') {
        $(elem).siblings('.tip').css({'display': 'block'});
        $(elem).siblings('.tip').html('不能为空');
        return false;
    } else {
        if (regx) {
            if (!new RegExp(regx).test(val)) {
                $(elem).siblings('.tip').css({'display': 'block'});
                $(elem).siblings('.tip').html(msg || '格式不正确');
                return false;
            } else {
                $(elem).siblings('.tip').css({'display': 'none'});
                $(elem).siblings('.tip').html('');
            }
        }
        $(elem).siblings('.tip').css({'display': 'none'});
        $(elem).siblings('.tip').html('');
    }

    return true;
}


/**
 * 判断浏览器是否为微信浏览器，并且版本是5.0以上版本
 */
function is_weixin(ver5) {
    //Mozilla/5.0(iphone;CPU iphone OS 5_1_1 like Mac OS X) AppleWebKit/534.46(KHTML,likeGeocko) Mobile/9B206 MicroMessenger/5.0
    var ua = navigator.userAgent.toLowerCase();
    var rwx = /.*(micromessenger)\/([\w.]+).*/;
    var match = rwx.exec(ua);
    if (match) {
        if (match[1] === 'micromessenger') {
            if (ver5) {
                if (parseFloat(match[2]) >= 5) {
                    return true;
                }
            } else {
                return true;
            }
        }
    }

    return false;
}

/**
 * 判断浏览器是否为微信浏览器，并且版本是5.0以上版本
 */
function is_weibo(ver5) {
    //Mozilla/5.0(iphone;CPU iphone OS 5_1_1 like Mac OS X) AppleWebKit/534.46(KHTML,likeGeocko) Mobile/9B206 MicroMessenger/5.0
    var ua = navigator.userAgent.toLowerCase();
    var rwx = /.*(weibo)\/([\w.]+).*/;
    var match = rwx.exec(ua);
    if (match) {
        if (match[1] === 'weibo') {
            return true;
        }
    }

    return false;
}

function datepicker() {
    $('#selectYear').on('change', function () {
        changeFbirary();
    });
    $('#selectMonth').on('change', function () {
        changeFbirary();
    });
    // 根据是否闰月改变二月份
    function changeFbirary() {
        $('#birthday').val('0000-00-00');

        var selectYear = parseInt($('#selectYear').val());
        var selectMonth = parseInt($('#selectMonth').val());
        if (!(selectYear && selectMonth)) return false;
        var monthDay = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        // 年下拉框改变时，判断是否是是闰年，更改二月分的天数
        if (selectMonth == 2 && ((selectYear % 100 == 0) && (selectYear % 400 == 0) || (selectYear % 100 != 0) && (selectYear % 4 == 0))) {
            monthDay[2]++;
        }
        var opt = '<option value="">日期</option>';
        // 获取用户原来的数据
        for (var i = 1; i <= monthDay[selectMonth]; i++) {
            i = i < 10 ? 0 + '' + i : i;
            opt += "<option value='" + i + "'>" + i + "</option>";
        }
        $("#selectDay").html(opt);
    }

    $('#selectDay').on('change', function () {
        var selectYear = $('#selectYear').val();
        var selectMonth = $('#selectMonth').val();
        var selectDay = $('#selectDay').val();

        $('#birthday').val(selectYear + '-' + selectMonth + '-' + selectDay);
    });


}

/**
 * Implements JSON stringify and parse functions
 * v1.0
 *
 * By Craig Buckler, Optimalworks.net
 *
 * As featured on SitePoint.com
 * Please use as you wish at your own risk.
 *
 * Usage:
 *
 * // serialize a JavaScript object to a JSON string
 * var str = JSON.stringify(object);
 *
 * // de-serialize a JSON string to a JavaScript object
 * var obj = JSON.parse(str);
 */

var JSON = JSON || {};

// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {

    var t = typeof (obj);
    if (t != "object" || obj === null) {

        // simple data type
        if (t == "string") obj = '"' + obj + '"';
        return String(obj);

    }
    else {

        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);

        for (n in obj) {
            v = obj[n];
            t = typeof(v);

            if (t == "string") v = '"' + v + '"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);

            json.push((arr ? "" : '"' + n + '":') + String(v));
        }

        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};


// implement JSON.parse de-serialization
JSON.parse = JSON.parse || function (str) {
    if (str === "") str = '""';
    eval("var p=" + str + ";");
    return p;
};

// Zepto.cookie plugin
//
// Copyright (c) 2010, 2012
// @author Klaus Hartl (stilbuero.de)
// @author Daniel Lacy (daniellacy.com)
//
// Dual licensed under the MIT and GPL licenses:
// http://www.opensource.org/licenses/mit-license.php
// http://www.gnu.org/licenses/gpl.html

/**
 * base64
 * Usage
 $.base64.encode( "this is a test" ) returns "dGhpcyBpcyBhIHRlc3Q="
 $.base64.decode( "dGhpcyBpcyBhIHRlc3Q=" ) returns "this is a test"
 */
Zepto.base64=(function($){var _PADCHAR="=",_ALPHA="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",_VERSION="1.0";function _getbyte64(s,i){var idx=_ALPHA.indexOf(s.charAt(i));if(idx===-1){throw"Cannot decode base64"}return idx}function _decode(s){var pads=0,i,b10,imax=s.length,x=[];s=String(s);if(imax===0){return s}if(imax%4!==0){throw"Cannot decode base64"}if(s.charAt(imax-1)===_PADCHAR){pads=1;if(s.charAt(imax-2)===_PADCHAR){pads=2}imax-=4}for(i=0;i<imax;i+=4){b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6)|_getbyte64(s,i+3);x.push(String.fromCharCode(b10>>16,(b10>>8)&255,b10&255))}switch(pads){case 1:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6);x.push(String.fromCharCode(b10>>16,(b10>>8)&255));break;case 2:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12);x.push(String.fromCharCode(b10>>16));break}return x.join("")}function _getbyte(s,i){var x=s.charCodeAt(i);if(x>255){throw"INVALID_CHARACTER_ERR: DOM Exception 5"}return x}function _encode(s){if(arguments.length!==1){throw"SyntaxError: exactly one argument required"}s=String(s);var i,b10,x=[],imax=s.length-s.length%3;if(s.length===0){return s}for(i=0;i<imax;i+=3){b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8)|_getbyte(s,i+2);x.push(_ALPHA.charAt(b10>>18));x.push(_ALPHA.charAt((b10>>12)&63));x.push(_ALPHA.charAt((b10>>6)&63));x.push(_ALPHA.charAt(b10&63))}switch(s.length-imax){case 1:b10=_getbyte(s,i)<<16;x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_PADCHAR+_PADCHAR);break;case 2:b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8);x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_ALPHA.charAt((b10>>6)&63)+_PADCHAR);break}return x.join("")}return{decode:_decode,encode:_encode,VERSION:_VERSION}}(Zepto));