define(function (require, exports) {
    require('base');
    //关于状态码
    //BMAP_STATUS_SUCCESS	检索成功。对应数值“0”。
    //BMAP_STATUS_CITY_LIST	城市列表。对应数值“1”。
    //BMAP_STATUS_UNKNOWN_LOCATION	位置结果未知。对应数值“2”。
    //BMAP_STATUS_UNKNOWN_ROUTE	导航结果未知。对应数值“3”。
    //BMAP_STATUS_INVALID_KEY	非法密钥。对应数值“4”。
    //BMAP_STATUS_INVALID_REQUEST	非法请求。对应数值“5”。
    //BMAP_STATUS_PERMISSION_DENIED	没有权限。对应数值“6”。(自 1.1 新增)
    //BMAP_STATUS_SERVICE_UNAVAILABLE	服务不可用。对应数值“7”。(自 1.1 新增)
    //BMAP_STATUS_TIMEOUT	超时。对应数值“8”。(自 1.1 新增)
    exports.getNearest = function () {
        // 百度地图API功能
        var geolocation = new BMap.Geolocation();
        var myGeo = new BMap.Geocoder();
        geolocation.getCurrentPosition(function (res) {
            if (this.getStatus() == BMAP_STATUS_SUCCESS) {
                myGeo.getLocation(res.point, function (rs) {
                    $('#your_address .curAddr').html(rs.address);
                    getSubStores(res.point.lat, res.point.lng);
                });
            } else {
                switch (this.getStatus()) {
                    case BMAP_STATUS_UNKNOWN_LOCATION:
                        error_msg = '无法获取地理位置！';
                        break;
                    case BMAP_STATUS_UNKNOWN_ROUTE:
                        error_msg = '导航结果未知，无法导航！';
                        break;
                    case BMAP_STATUS_INVALID_KEY:
                        error_msg = '非法密钥！';
                        break;
                    case BMAP_STATUS_INVALID_REQUEST:
                        error_msg = '没有权限！';
                        break;
                    case BMAP_STATUS_PERMISSION_DENIED:
                        error_msg = '非法请求！';
                        break;
                    case BMAP_STATUS_SERVICE_UNAVAILABLE:
                        error_msg = '服务不可用！';
                        break;
                    case BMAP_STATUS_TIMEOUT:
                        error_msg = '超时！';
                        break;
                    default:
                        error_msg = '未知错误！';
                        break;
                }
                // 请求附加门店
                $('#your_address .curAddr').html(error_msg);
                getSubStores(0, 0);
            }
            $('.loading').hide();
        }, {enableHighAccuracy: true});
    };

    function getSubStores(lat, lng) {
        $.ajax({
            url: '/addon/api/store/getList',
            data: {'lat': lat, 'lng': lng},
            dataType: 'json',
            type: 'post',
            success: function (res) {
                var html = (function (data) {      //数据渲染
                    var liArr = [];
                    $.each(data, function () {
                        liArr.push(this.html);
                    });
                    return liArr.join('');
                })(res);
                $('.sub_stores').html(html);
            }
        });
    }
});