define(function (require, exports) {
    require('base');

    exports.switchTab = function () {
        $('.coupon-tab').on('click', '.tab', function (e) {
            var tab = $(this).attr('data-tab');
            $('.coupon-tab .tab').removeClass('active');
            $(this).addClass('active');

            $('.tab-content .tab-pane').hide();
            $('.tab-content .tab-pane[data-tab="' + tab + '"]').show();

            e.stopImmediatePropagation();
        });
    };

    exports.offlineUseCoupon = function () {
        $('#offlineUseBtn').click(function (e) {
            var serial = $(this).attr('data-serial');
            var passwd = $('#passwd').val();
            if (passwd.length < 6) {
                tip.showTip('err', '密码6位以上', 1500);
                return;
            }

            requestApi('/addon/api/coupon/offlineUse', {'serial': serial, passwd: passwd}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '优惠劵商家确认使用成功~!', 3000);
                    //
                    $('#offline-content').hide().remove();
                    $('.offlineUseTip').show();
                }
            });
            e.stopImmediatePropagation();
        });
    };

    exports.receiveCoupon = function () {
        $('.receiveBtn').click(function (e) {
            var id = $(this).attr('data-id');
            requestApi('/addon/api/coupon/receive', {'coupon_id': id}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '优惠劵领取成功~!', 3000);
                    window.location.href = '/addon/coupon/mine/' + res.data;
                }
            });
            e.stopImmediatePropagation();
        });
    }
});