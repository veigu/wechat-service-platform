define(function (require, exports) {
    require('base');
    require('/static/wap/gmu/widget/calendar');

    exports.editReserve = function (redi_url) {

        $('#reserve_date').click(function (e) {//展开或收起日期
            $('#datepicker').show();
            e.stopImmediatePropagation();
        });


        $('#reserveBtn').click(function (e) {
            var id = $(this).attr('data-id');
            var username = $('#reserveForm').find('#username').val();
            var phone = $('#reserveForm').find('#phone').val();
            var reserve_date = $('#reserveForm').find('#reserve_date').val();
            var reserve_time = $('#reserveForm').find('#reserve_time').val();
            var remark = $('#reserveForm').find('#remark').val();
            var reserve_id = $('#reserveForm').find('#reserve_id').val();

            if (!checkField('#reserveForm #username', '请填写正确的联系人姓名', /^[\u4E00-\u9FA5]{2,6}$/)) {
                tip.showTip('err', '请填写正确的联系人姓名', 2000);
                return false;
            }

            if (!checkField('#reserveForm #phone', '请填写正确的手机号', /^(1[\d]{10})$/)) {
                tip.showTip('err', '请填写正确的手机号', 2000);
                return false;
            }

            // 附加选项
            var param = '';
            var tmpChk = '';
            // 选项值组合规则：如 |##|25-单选|#|呵呵呵
            // |##|为开始，所以分为： |##|25-单选|#|呵呵呵 和 |##|26-多选|#|cc@dd
            // |#|为key于val的分割
            $('#column .input').each(function () {
                var val = $(this).val();
                var id = $(this).attr("data-id");
                var key = $(this).attr("data-name");

                if ($(this).attr('type') == 'checkbox' && $(this).attr('checked')) {
                    if (tmpChk == key) {
                        param += '@' + val;
                    } else {
                        param += '|##|' + id + '-' + key + '|#|' + val;
                    }
                    tmpChk = key;
                } else {
                    if ($(this).attr('type') != 'checkbox') {
                        param += '|##|' + id + '-' + key + '|#|' + val;
                    }
                }
            });

            var data = {
                username: username,
                phone: phone,
                param: param,
                reserve_date: reserve_date,
                reserve_time: reserve_time,
                remark: remark
            };

            requestApi('/addon/api/reserve/submitOrder', {
                id: id,
                data: data,
                reserve_id: reserve_id
            }, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '订单修改成功! ', 2000);
                    window.location.href = redi_url;
                }
            });

            e.stopImmediatePropagation();
        });
    }

});