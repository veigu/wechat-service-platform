define(function (require, exports) {
    require('base.js');
    require('/static/panel/js/tools/region.select.js');

    exports.setVipCardInfo = function (btn, uri) {

        $(btn).on('click', function (e) {
            var realname = $('#realname').val();
            var phone = $('#user_phone').val();
            var email = $('#email').val();
            var gender = $('.gender:checked').val();
            var address = $('#address').val();
            var accept_email = $('.accept_email:checked').val();
            var accept_msg = $('.accept_msg:checked').val();
            var accept_dm = $('.accept_dm:checked').val();
            var province = $('#province').val();
            var city = $('#city').val();
            var birthday = $('#birthday').val();
            var town = $('#town').val();

            if (!checkField('#realname', '请填写正确的真实姓名', /^[\u4E00-\u9FA5]{2,8}|^[a-zA-Z]{0,8}[a-zA-Z\.]{0,20}$/)) {
                tip.showTip('err', '请填写正确的真实姓名', 1500);
                return;
            }

            if (!checkField('#user_phone', '手机号不正确！', /^(1[\d]{10})$/)) {
                tip.showTip('err', '手机号不正确', 1500);
                return false;
            }

            if (birthday == '0000-00-00' || birthday == undefined || birthday == null || birthday == "") {
                tip.showTip('err', '请选择生日日期，填写后将不能修改', 1500);
                return false;
            }

            // 附加选项
            var param = '';
            var tmpChk = '';
            $('#card-column .input').each(function () {
                var val = $(this).val();
                var id = $(this).attr("data-id");
                var key = $(this).attr("data-name");

                if ($(this).attr('type') == 'checkbox' && $(this).attr('checked')) {
                    if (tmpChk == key) {
                        param += '@' + val;
                    } else {
                        param += '|##|' + id + '-' + key + '|#|' + val;
                    }
                    tmpChk = key;
                } else {
                    if ($(this).attr('type') != 'checkbox') {
                        param += '|##|' + id + '-' + key + '|#|' + val;
                    }
                }
            });
            var data = {
                realname: realname,
                phone: phone,
                email: email,
                gender: gender,
                param: param,
                province: province,
                city: city,
                town: town,
                address: address
            };

            if (birthday) {
                data.birthday = birthday;
            }

            requestApi('/addon/api/vipcard/' + uri, {data: data}, function (res) {
                if (res.result == 1) {
                    tip.showTip('err', '资料修改成功！');
                    if (uri == 'apply') {
                      window.location.reload();
                    } else {
                        window.location.href = '/addon/vipcard';
                    }
                }
            });
        });
    }
});