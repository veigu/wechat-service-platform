define(function (require, exports) {
    require('base');
    exports.reply = function () {
        $('#replyForm').on('click', '#smbBtn', function (e) {
                var detail = $('#txtDetail').val();
                var demand_id = $(this).attr('data-demand_id');
                if (!detail) {
                    tip.showTip('err', '请填写回复内容～！', 3000);
                    $('#txtDetail').focus();
                    return;
                }

                var data = {
                    demand_id: demand_id, detail: detail, is_customer: 0
                };

                var cm = window.confirm('您填写完善并回复吗？');

                if (!cm) return;

                requestApi('/addon/api/demand/reply', {data: data}, function (res) {
                    if (res.result == 1) {
                        tip.showTip('ok', '恭喜您，继续追问成功～！', 3000);
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    }
                });

                e.stopImmediatePropagation();
            }
        )
    };

    exports.endReply = function () {
        $('#endReply').on('click', function (e) {
                var demand_id = $(this).attr('data-id');

                var data = {
                    id: demand_id
                };

                var cm = window.confirm('您确认需要结束吗，结束后将不能回复？');

                if (!cm) return;

                requestApi('/addon/api/demand/endReply', data, function (res) {
                    if (res.result == 1) {
                        tip.showTip('ok', '恭喜您，需求设置结束成功～！', 3000);
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    }
                });

                e.stopImmediatePropagation();
            }
        )
    };

    exports.addDemand = function () {
        $('#addDemandBtn').click(function (e) {
            var username = $('#demandForm').find('#username').val();
            var phone = $('#demandForm').find('#phone').val();
            var email = $('#demandForm').find('#email').val();
            var title = $('#demandForm').find('#title').val();
            var detail = $('#demandForm').find('#detail').val();
            var type_id = $('#demandForm').find('#type_id').val();
            var expected_time = $("#selectYear").val() + '-' + $("#selectMonth").val() + '-' + $("#selectDay").val();

            if (!checkField('#demandForm #username', '请填写正确的联系人姓名', /^[\u4E00-\u9FA5]{2,6}$/)) {
                tip.showTip('err', '请填写正确的联系人姓名', 2000);
                return false;
            }

            if (!checkField('#demandForm #phone', '请填写正确的手机号', /^(1[\d]{10})$/)) {
                tip.showTip('err', '请填写正确的手机号', 2000);
                return false;
            }

            var unixtime = Date.parse(expected_time + " 23:59:59");
            if (unixtime < Date.now()) {
                tip.showTip('err', '请正确选择期望服务时间！', 2000);
                return false;
            }

            if (type_id == undefined || type_id == '') {
                tip.showTip('err', '请选择需求类型', 2000);
                return false;
            }

            var data = {
                username: username,
                phone: phone,
                email: email,
                type_id: type_id,
                title: title,
                detail: detail,
                expected_time: expected_time
            };

            requestApi('/addon/api/demand/addDemand', {data: data}, function (res) {
                if (res.result == 1) {
                    window.location.href = res.data;
                    tip.showTip('ok', '需求提交成功! 即将跳转～', 1000);
                }

            });

            e.stopImmediatePropagation();
        });
    };

    exports.selectMonth = function () {
        $('#selectMonth').on('click', function () {

            var month = parseInt($(this).val());
            var year = $("#selectYear").val();
            //2月是闰年
            if (month == 2 && (year % 100 == 0) && (year % 400 == 0) || (year % 100 != 0) && (year % 4 == 0)) {

                $("#selectDay").find("option[value='30']").remove();
                $("#selectDay").find("option[value='31']").remove();

            }
            //2月不是闰年
            if (month == 2 && !((year % 100 == 0) && (year % 400 == 0) || (year % 100 != 0) && (year % 4 == 0))) {
                $("#selectDay").find("option[value='29']").remove();
                $("#selectDay").find("option[value='30']").remove();
                $("#selectDay").find("option[value='31']").remove();
            }
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {

                if ($("#selectDay").find("option[value='29']").length == 0) {
                    $("#selectDay").append("<option value='29'>29</option>");
                }
                if ($("#selectDay").find("option[value='30']").length == 0) {
                    $("#selectDay").append("<option value='30'>30</option>");
                }
                if ($("#selectDay").find("option[value='31']").length == 0) {
                    $("#selectDay").append("<option value='31'>31</option>");
                }
            }
            if (month == 4 || month == 6 || month == 9 || month == 11) {
                $("#selectDay option[value='31']").remove();
                if ($("#selectDay").find("option[value='29']").length == 0) {
                    $("#selectDay").append("<option value='29'>29</option>");
                }
                if ($("#selectDay").find("option[value='30']").length == 0) {
                    $("#selectDay").append("<option value='30'>30</option>");
                }
            }

        })


    }
});