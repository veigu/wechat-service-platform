define(function (require, exports) {
    require('base.js');
    require('/static/panel/js/tools/region.select.js');
    require('/static/wap/gmu/widget/dialog.js');

    exports.user_dialog = function () {
        // 注册窗口
        $(document).on('click', '.switchToRegBtn', function (e) {
            //    $('#user-login-dialog').dialog('close');
            //   $('#user-reg-dialog').dialog('open');
            window.location.href = '/user';
            e.stopImmediatePropagation();
        });

        // 登陆窗口
        $(document).on('click', '.switchToLoginBtn', function (e) {
            $('#user-login-dialog').dialog('open');
            $('#user-reg-dialog').dialog('close');

            e.stopImmediatePropagation();
        });

        // 登陆注册接口
        exports.user_login(false);
        exports.user_reg(false);
    };

    /**
     * check login and open login dialog if not login
     */
    exports.check_login = function () {
        if (!($.cookie('_APP_CUID') && $.cookie('_APP_CUID') > 0)) {
            $('#user-login-dialog').dialog('open');

            return false;
        }
        return true;
    };

    /**
     * Created by yanue on 6/17/14.
     */
    exports.user_login = function (url) {
        // 登陆
        $('#loginForm .loginBtn').on('click', function () {
            var phone = $('#loginForm .phone').val();
            var pass = $('#loginForm .password').val();

            if (!checkField('#loginForm .phone', '手机号不正确！', /^(1[\d]{10})$/)) {
                tip.showTip('err', '手机号不正确', 1500);
                return false;
            }

            if (!checkField('#loginForm .password')) {
                tip.showTip('err', '密码不能为空', 1500);
                return false;
            }
            var data = {'phone': phone, 'pass': pass};

            requestApi('/api/account/login', data, function (res) {
                // 用户密码不正确
                if (res.error.code == 1008) {
                    $('#goForgotBtn').attr('data-phone', phone).css({'display': 'inline'});
                } else {
                    $('#goForgotBtn').hide();
                }

                if (res.result == 1) {
                    if (res.data.need_card_bind == true) {
                        window.location.href = '/addon/vipcard';
                    } else {
                        if (url === false) {
                            setTimeout(function () {
                                $('#user-login-dialog').dialog('close');
                            }, 1000);

                            // 积分
                            window.app.user_point = res.data.point;
                            window.location.reload();
                        } else {
                            window.location.href = url || '/user';
                        }
                    }

                    tip.showTip('ok', '登陆成功', 2000);
                }
            });
        });

        // 跳转到找回密码
        $(document).on('click', '#goForgotBtn', function (e) {
            var phone = $(this).attr('data-phone');

            if (!phone) {
                tip.showTip('');
            }

            var go = $(this).attr('data-go');

            window.location.href = '/user/forgot?p=' + $.base64.encode(phone) + '&go=' + go;

            e.stopImmediatePropagation();
        });
    };

    /**
     * Created by yanue on 6/17/14.
     */
    exports.card_bind = function () {
        // 登陆
        $('#cardBindBtn').on('click', function () {
            var phone = $('#phone_number').val();
            var card = $('#card_number').val();
            var code = $('#verifyCode').val();

            if (!checkField('#card_number')) {
                tip.showTip('err', '会员卡号不能为空', 1500);
                return false;
            }
            if (!checkField('#phone_number', '手机号不正确！', /^(1[\d]{10})$/)) {
                tip.showTip('err', '手机号不正确', 1500);
                return false;
            }
            if (!checkField('#verifyCode')) {
                tip.showTip('err', '验证码不能为空', 1500);
                return false;
            }
            var data = {'phone': phone, 'card': card, 'code': code};
            requestApi('/emoiapi/card_bind', data, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '恭喜您，绑定成功，本平台初始密码是：' + res.data, 10000);
                    setTimeout(function () {
                        window.location.href = '/user/resetPass';
                    }, 3000);
                }
            });
        });
    };

    exports.user_reg = function (url) {

        exports.send_code();
        /*

         exports.check_phone_exists(function() {
         $('#sendCodeBtn').removeClass('btn-gray').addClass('btn-green').attr('disabled', false);
         tip.showTip('ok', "该手机可以注册", 1000);
         }, function() {
         tip.showTip('ok', "该手机已经注册，您可以直接登录", 1000);
         $('#sendCodeBtn').removeClass('btn-green').addClass('btn-gray').attr('disabled', true);
         });
         */
        exports.check_nick_exists(function () {
            $('#signUpForm .regBtn').removeClass('btn-gray').addClass('btn-green').attr('disabled', false);
            tip.showTip('ok', "该昵称可以注册", 1000);
        }, function () {
            tip.showTip('ok', "该昵称已经被其他人使用，请您更换一个", 1000);
            $('#signUpForm .regBtn').removeClass('btn-green').addClass('btn-gray').attr('disabled', true);
        });

        $('.phoneCheckForm .nextBtn').on('click', function () {
            exports.check_verify_code(function () {
                var isOk = $('#verifyCode').attr('data-checked');
                if (isOk && isOk == "true") {
                    $('.phoneCheckForm').hide();
                    $('.signUpForm').show();
                }
                else {
                    tip.showTip("err", "手机没有通过验证！", 1000);
                    return false;
                }
            });
        });

        $('.signUpForm .regBtn').on('click', function () {

            var isDisabled = true; //$(this).attr('disabled');
            if (isDisabled && isDisabled == 'true') {
                return false;
            }

            var phone = $('.phoneCheckForm .phone').val();
            var pass = $('.signUpForm .password').val();


            if (!checkField('.signUpForm .password')) {
                return false;
            }


            var data = {
                'username': phone,
                'phone': phone,
                'pass': pass
            };

            requestApi('/api/account/reg', data, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '注册成功', 1000);
                    if (res.data.need_card_bind == true) {
                        window.location.href = '/addon/vipcard';
                    } else {
                        if (url === false) {
                            setTimeout(function () {
                                $('#user-login-dialog').dialog('close');
                            }, 1000);

                            // 积分
                            window.location.reload();
                        } else {
                            window.location.href = url || '/user';
                        }
                    }
                }
                else {
                    //已经注册过，直接登录
                    if (res.error.code == 1016) {

                    }
                }
            });
        });
    };

    exports.bind_wechat = function (url) {

    };

    exports.bind_weibo = function (url) {

    };


    exports.check_phone_exists = function (okCall, failCall) {
        $('.phone').on('change', function () {
            if (!checkField(".phone", '手机号码格式不正确', /^(1[\d]{10})$/)) {
                return false;
            }
            var phone = $('.phone').val();
            requestApi('/api/checkPhone', {phone: phone}, function (res) {
                if (res.result == 1) {
                    failCall();
                }
                else if (res.result == 2) {
                    okCall();
                }
                else {

                }
            });
        });
    };

    exports.check_nick_exists = function (okCall, failCall) {
        $('.username').on('blue', function () {
            if (!checkField(".username", '用户名', /^(1[\d]{10})$/)) {
                return false;
            }
            var phone = $(this).val();
            requestApi('/api/checkNick', {phone: phone}, function (res) {
                if (res.result == 1) {
                    tip.showTip("ok", "昵称可用", 1000);
                    okCall();
                }
                else if (res.result == 2) {
                    failCall();
                }
            });
        });
    };

    exports.user_profile = function () {
        $('#settingForm .saveBtn').on('click', function () {
            var name = $('#settingForm .username').val();
            var email = $('#settingForm .email').val();
            var gender = $('#settingForm .gender:checked').val();
            var province = $('#settingForm #province').val();
            var city = $('#settingForm #city').val();
            var town = $('#settingForm #town').val();

            if (!checkField('#settingForm .username')) {
                return false;
            }

            if (!checkField('#settingForm .email', '邮箱格式不正确', /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/)) {
                return false;
            }

            if (!gender || gender == undefined) {
                $('#settingForm .gender').siblings('.tip').html('不能为空');
                return false;
            } else {
                $('#settingForm .gender').siblings('.tip').html('');
            }

            var data = {
                'username': name,
                'email': email,
                'gender': gender,
                'province': province,
                'city': city,
                'town': town
            };

            requestApi('/api/user/saveProfile', data, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '修改成功', 1000);
                    window.location.href = '/user';
                }
            });
        });
    };


    exports.user_forgot = function (url) {
        $('#phoneCheckForm .nextBtn').on('click', function () {
            exports.check_verify_code(function () {
                var isOk = $('#verifyCode').attr('data-checked');
                if (isOk && isOk == "true") {

                    $('#login-list-bg').hide();
                    $('#login-list-bg01').show();
                }
                else {
                    tip.showTip("err", "手机没有通过验证！", 1000);
                    return false;
                }
            });
        });
        // 跳转到找回密码
        $('#signUpForm .reset-pass').on('click', function (e) {
            var phone = $('#phoneCheckForm .phone').val();
            var pass = $('#signUpForm .password').val();
            var repass = $('#signUpForm .repassword').val();

            if (!checkField('#signUpForm .password')) {
                return false;
            }

            if (!checkField('#signUpForm .repassword')) {
                return false;
            }

            if (repass != pass) {
                tip.showTip('err', '两次密码不一致!', 1500);
                return false
            }

            requestApi('/api/user/forgetPass', {phone: phone, pass: pass}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '密码已修改，请登录～！', 3000);
                    setTimeout(function () {
                        window.location.href = '/user/login';
                    }, 2000);
                }
            });
        });
    };

    exports.user_resetPass = function () {
        // 重新设置密码
        $('#resetPassForm .resetPassBtn').on('click', function (e) {
            var oldPass = $('#resetPassForm .old_pass').val();
            var pass = $('#resetPassForm .password').val();
            var repass = $('#resetPassForm .repassword').val();

            if (!checkField('#resetPassForm .old_pass')) {
                return false;
            }

            if (!checkField('#resetPassForm .password')) {
                return false;
            }

            if (!checkField('#resetPassForm .repassword')) {
                return false;
            }

            if (repass != pass) {
                tip.showTip('err', '两次密码不一致!', 1500);
                return false
            }

            requestApi('/api/user/resetPass', {oldPass: oldPass, pass: pass}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '密码已修改，下次登陆有效～！', 3000);
                    setTimeout(function () {
                        window.location.href = '/addon/vipcard/mine';
                    }, 2000);
                }
            });
            e.stopImmediatePropagation();
        });
    };


    exports.send_code = function () {
        $('#sendCodeBtn').on('click', function (e) {

            if (!checkField('.phone', '手机号不正确！', /^(1[\d]{10})$/)) {
                tip.showTip('err', '手机号不正确!', 1500);
                return false;
            }

            var check_unique = $(this).attr('data-checkUnique');
            console.log(check_unique);
            var disabled = $(this).attr('disabled');
            if (disabled && disabled == 'true') {
                return false;
            }
            var phone = $('.phone').val();
            requestApi('/api/account/sendVerifyCode', {phone: phone, check_unique: check_unique}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '验证码已经发送，请稍等……！', 3000);
                    codeTimer('sendCodeBtn', 60);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    exports.check_verify_code = function (callback) {
        if (!checkField('#verifyCode', '验证码不正确！', /^([\d]{6})$/)) {
            return false;
        }
        var isChecked = $('#verifyCode').attr('data-checked');
        if (isChecked == 'true') {
            return false;
        }
        var code = $('#verifyCode').val();
        requestApi('/api/account/checkVerifyCode', {code: code}, function (res) {
            if (res.result == 0) {
                tip.showTip('err', '验证码错误！', 3000);
            }
            else {
                tip.showTip('ok', '手机号验证通过！', 3000);
                $('#verifyCode').attr('data-checked', true);
                if (callback) {
                    callback();
                }
            }
        });
    };

    exports.bind_phone = function (refer) {
        exports.send_code();
        // 绑定
        $('#bindForm .bind-btn').on('click', function (e) {
            var phone = $('#bindForm #phone').val();
            var code = $('#bindForm #verifyCode').val();

            if (!checkField('#bindForm #phone', '手机号不正确！', /^(1[\d]{10})$/)) {
                return false;
            }

            if (!checkField('#bindForm #verifyCode')) {
                return false;
            }

            requestApi('/api/user/bindPhone', {}, function (res) {
                if (res.result == 1) {
                    if (res.data.need_pwd) {
                        tip.showTip('ok', '手机号已绑定！还没有设置密码，请设置一个登陆密码', 3000);
                        $('#setPassForm').show();
                        $('#bindForm').hide();
                        exports.setPassword(refer);
                    }
                    else {
                        tip.showTip('ok', '手机号已绑定成功', 3000);
                        setTimeout(function () {
                            if (refer) {
                                window.location.href = refer;
                            }
                            else {
                                window.location.href = '/user/setting';
                            }
                        }, 2000);
                    }
                }
                e.stopImmediatePropagation();
            });
        });

        exports.setPassword = function (refer) {
            $('#setPassForm .set-pwd-btn').on('click', function () {
                if (!checkField('')) {
                    return false;
                }

                if (!checkField('')) {
                    return false;
                }

                var pass = $('#resetPassForm .password').val();
                var repass = $('#resetPassForm .repassword').val();

                if (repass != pass) {
                    $('#setPassForm .repassword').siblings('.tip').html('两次输入的密码不一致！');
                    return false
                } else {
                    $('#setPassForm .repassword').siblings('.tip').html('');
                }

                requestApi('/api/usr/setPass', {pass: pass}, function (res) {
                    if (res.result == 1) {
                        tip.showTip('ok', '密码已设置，下次登陆有效～！', 3000);
                        setTimeout(function () {
                            if (refer) {
                                window.location.href = refer;
                            }
                            else {
                                window.location.href = '/user/setting';
                            }
                        }, 2000);
                    }
                });
                e.stopImmediatePropagation();
            });
        }
    };

    function codeTimer(targetId, times) {
        var target = $('#' + targetId);
        if (target) {
            if (times && times > 0) {
                target.attr('disabled', true);
                if (times == 60) {
                    target.removeClass('btn-green');
                    target.addClass('btn-gray');
                }
                var newTimes = times - 1;
                target.html('<span style="color: red; font-size: 12px;">' + newTimes + "</span>秒后可重发");
                setTimeout(function () {
                    codeTimer(targetId, times - 1);
                }, 1000);
            }
            else {
                target.removeClass('btn-gray');
                target.addClass('btn-green');
                target.attr('disabled', false);
                target.html("重发验证码");
            }
        }
    }

    exports.setVipCardInfo = function (btn, uri) {
        $('.is_company').on('change', function (e) {
            var is_company = $(this).val();
            if (is_company == 1) {
                $('.company_info').show();
            } else {
                $('.company_info').hide();
            }

            e.stopImmediatePropagation();
        });

        $(btn).on('click', function (e) {
            var realname = $('#realname').val();
            var phone = $('#user_phone').val();
            var email = $('#email').val();
            var gender = $('.gender:checked').val();
            var address = $('#address').val();
            var is_company = $('.is_company').val();
            var company_info = "";
            var accept_email = $('.accept_email:checked').val();
            var accept_msg = $('.accept_msg:checked').val();
            var accept_dm = $('.accept_dm:checked').val();
            var province = $('#province').val();
            var city = $('#city').val();
            var birthday = $('#birthday').val();
            var town = $('#town').val();

            if (!checkField('#realname', '请填写正确的真实姓名', /^[\u4E00-\u9FA5]{2,8}|^[a-zA-Z]{0,8}[a-zA-Z\.]{0,20}$/)) {
                tip.showTip('err', '请填写正确的真实姓名', 1500);
                $('#company_name').focus();
                return;
            }

            if (!checkField('#user_phone', '手机号不正确！', /^(1[\d]{10})$/)) {
                tip.showTip('err', '手机号不正确', 1500);
                $('#user_phone').focus();
                return false;
            }

            /*if (birthday == '0000-00-00' || birthday == undefined || birthday == null) {
                tip.showTip('err', '请选择生日日期，填写后将不能修改', 1500);
                return false;
            }*/

            if (is_company == 1) {
                var company_name = $('#company_name').val();
                var company_tel = $('#company_tel').val();
                var company_fax = $('#company_fax').val();
                var company_site = $('#company_site').val();
                if (!checkField('#company_name', '请填写公司名称')) {
                    tip.showTip('err', '请填写公司名称', 1500);
                    $('#company_name').focus();
                    return;
                }

                if (!checkField('#company_tel', '电话格式不正确', /^(1[\d]{10})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{3,7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$/)) {
                    tip.showTip('err', '企业电话格式不正确', 1500);
                    $('#company_tel').focus();
                    return;
                }

                /*if (!checkField('#company_fax', '电话格式不正确', /^(1[\d]{10})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{3,7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$/)) {
                    tip.showTip('err', '企业传真号码格式不正确', 1500);
                    return;
                }*/

                if (company_site && company_site.length > 0) {
                    if (!checkField('#company_site', '请填写正确的公司网址', /[a-zA-z]+:\/\/[^\s]*/)) {
                        tip.showTip('err', '请填写正确的公司网址', 1500);
                        $('#company_site').focus();
                        return;
                    }
                }

                company_info = {
                    company_name: company_name,
                    company_tel: company_tel,
                    company_fax: company_fax,
                    company_site: company_site
                };
            }

            // 附加选项
            var param = '';
            var tmpChk = '';
            $('#card-column .input').each(function () {
                var val = $(this).val();
                var id = $(this).attr("data-id");
                var key = $(this).attr("data-name");

                if ($(this).attr('type') == 'checkbox' && $(this).attr('checked')) {
                    if (tmpChk == key) {
                        param += '@' + val;
                    } else {
                        param += '|##|' + id + '-' + key + '|#|' + val;
                    }
                    tmpChk = key;
                } else {
                    if ($(this).attr('type') != 'checkbox') {
                        param += '|##|' + id + '-' + key + '|#|' + val;
                    }
                }
            });
            var data = {
                realname: realname,
                phone: phone,
                email: email,
                gender: gender,
                param: param,
                is_company: is_company,
                company_info: company_info,
                accept_email: accept_email,
                accept_msg: accept_msg,
                accept_dm: accept_dm,
                'province': province,
                'city': city,
                'town': town,
                address: address
            };

            if (birthday) {
                data.birthday = birthday;
            }

            requestApi('/addon/api/vipcard/' + uri, {data: data}, function (res) {
                if (res.result == 1) {
                    tip.showTip('err', '资料修改成功！');
                    if (uri == 'apply') {
                        window.location.reload();
                    } else {
                        window.location.href = '/addon/vipcard';

                    }
                }
            });
        });
    }
});
if (navigator.userAgent.indexOf("iPhone") != -1) {
    $("input[type='text'],input[type='password'],textarea").focus(function () {

        //  $(".footer").css({'position':'absolute','bottom':'0'});
        $(".header").css({'position': 'absolute', 'top': '0px'});
        $(".footer").css({'display': 'none'});

    }).blur(function () {
            $(".header").css({'position': 'fixed'});
            $(".footer").css({'display': 'block'});
            //  $(".footer").css({'position':'fixed'});
        });


}
