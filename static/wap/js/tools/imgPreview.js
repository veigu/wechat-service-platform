define(function (require, exports) {
    exports.wxPreview = function (elem) {
        $(function () {
            var list = new Array();
            $(elem).find("img").each(function (index, element) {
                list[index] = $(this).attr('src');
            });
            $(elem).on('click', 'img', function () {
                if (window.WeixinJSBridge) {
                    WeixinJSBridge.invoke('imagePreview', { 'current': $(this).attr('src'), 'urls': list});
                }
            });
        })
    }
});



