/**
 *  自定义分享功能
 @author : yanue
 @site : http://yanue.net/
 @time : 2014.07.08

 <a href="javascript:;" class="shareItem">转发分成</a>

 <?php
 TplManager::init(CUR_APP_ID)->baseTpl('widget/share');
 ?>

 <script>
 seajs.use('app/app.share', function (share) {
            var shareParams = {
                'url':'',
                'pics': [''],//图片地址数组
                'title': '',//分享标题
                'desc': '' //分享描述
            };
            share.share('.shareItem', shareParams);
        });
 </script>

 */
define(function (require, exports) {
    var appShare = {}, options = {};
    require('base');

    appShare.share = {
        // 初始化
        'init': function (elem, param) {
            options.url = param.url || location.href;
            options.pics = param.pics || [];
            options.comment = param.comment || '';
            options.title = param.title;
            options.desc = param.desc;
            // open share widget
            $(document).on('click', elem, function (e) {
                $('.share-widget').show();

                e.stopImmediatePropagation();
            });

            if (!is_weixin()) {
                $('.share-widget .only_weixin').remove();
            }

            $('.share-widget').on('click', '.shareBtn', function () {
                var share = $(this).attr('data-share');
                if (share) {
                    eval("appShare.share." + share + "()");
                } else {
                    $('#mcover').show();
                }
            });

            // cancel
            $('.share-widget').on('click', '.cancelShareBtn', function (e) {
                $('.share-widget').hide();

                e.stopImmediatePropagation();
            });

        },

        // 新浪微博
        'wbshare': function () {
            var submitUrl = 'http://service.weibo.com/share/share.php';
            var wbShareparam = {
                url: options.url,
                type: '3',
                appkey: '207042031',
                title: options.title,
                pic: options.pics[0],
                ralateUid: '@looklo',
                language: 'zh-cn',
                rnd: new Date().valueOf()
            };
            this._openUrl(wbShareparam, submitUrl);
            return false;
        },

        // qq空间
        'qzshare': function () {
            var qqShareparam = {
                url: options.url,
                desc: options.desc,
                title: options.title,
                pics: options.pics
            };
            var submitUrl = 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey';
            this._openUrl(qqShareparam, submitUrl);
        },

        // 百度
        'bdshare': function (param) {
            var tqShareparam = {
                url: location.href,
                content: options.desc,
                title: options.title,
                linkid: 'yansueh',
                pics: options.pics, /* 分享图片的路径(可选) */
                appkey: ''
            };
            var submitUrl = 'http://hi.baidu.com/pub/show/share';
            this._openUrl(tqShareparam, submitUrl);
            return false;
        },

        // 公用打开窗口
        '_openUrl': function (shareparam, submitUrl) {
            var temp = [];
            for (var p in shareparam) {
                temp
                    .push(p + '='
                        + encodeURIComponent(shareparam[p] || ''));
            }
            var url = submitUrl + "?" + temp.join('&');
            var wa = 'width=700,height=650,left=0,top=0,resizable=yes,scrollbars=1';
            window.open(url, 'looklo', wa);
        }

        // end
    };

    exports.share = function (elem, param) {
        appShare.share.init(elem, param);
    }
});

