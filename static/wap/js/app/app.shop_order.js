define(function (require, exports) {
    var base = require('base');
    require("/static/panel/js/jquery/jquery.serializeObject.min");
    require('/static/panel/js/tools/region.select.js');
    var u = require('app/app.user.js');

    /**
     * 地址管理
     *
     * @constructor
     */
    function UserAddress() {
        // 初始化
        new PCAS('province', 'city', 'town', '广东省', '深圳市', '福田区');

        this.init();
        this.delAddr();
        this.editAddr();
        this.saveAddr();
        this.setDefault();
    }

    UserAddress.prototype = {
        init: function () {

            $('#confirmOrderBtn').show();
            $('#showAddrFormBtn').hide();
            $('#saveAddrBtn').hide();

            // hide form
            $(document).on('click', '.addrForm .hideAddrFormBtn', function (e) {
                $('.addrManage .addrForm').hide();
                $('#confirmOrderBtn').hide();
                $('#showAddrFormBtn').css("display", 'block').show();
                $('#saveAddrBtn').hide();

                e.stopImmediatePropagation();
            });

            // open form and reset
            $(document).on('click', ' .showAddrFormBtn', function (e) {
                $('.addrManage .addrForm').show();

                $('.addrForm .saveAddrBtn').removeAttr('data-id').html('添加');

                $('.addrForm').find('#name').val('');
                $('.addrForm').find('#phone').val('');
                $('.addrForm').find('#address').val('');

                $('.addrManage .addrForm').show();
                $(".addr .addr_detail").hide();
                $('#confirmOrderBtn').hide();
                $('#showAddrFormBtn').hide();
                $('#saveAddrBtn').css("display", 'block').show().removeAttr('data-id');//去除data-id

                e.stopImmediatePropagation();
            });

            // 选择地址
            $('.addrManage').on('click', ' .addr-list .addr .tap-addr', function (e) {
                var id = $(this).attr('data-id');
                var html = $(this).html();

                $('.choose-address .address-content').html(html);

                $('.choose-address #address_id').val(id);

                // tap事件穿透？
                setTimeout(function () {
                    $('.addrManage').hide();
                }, 350);
                $('#confirmOrderBtn').css("display", 'block').show();
                $('#showAddrFormBtn').hide();
                $('#saveAddrBtn').hide();

                // 获取地址并刷新
                $.cookie('_user_addr', id);
                window.location.reload();

                e.stopImmediatePropagation();
            });

            $(document).on('click', '.addrManage .hideAddrManageBtn', function (e) {
                $('.addrManage').hide();
                $('#confirmOrderBtn').css("display", 'block').show();
                $('#showAddrFormBtn').hide();
                $('#saveAddrBtn').hide();

                e.stopImmediatePropagation();
            });

            // 打开地址
            $(document).on('click', '.choose-address .chooseAddr', function (e) {
                showAddr();
                e.stopImmediatePropagation();
            });

            // 点击地址
            $(document).on('click', '.choose-address .address-content', function (e) {
                showAddr();
                e.stopImmediatePropagation();
            });

            function showAddr() {
                var id = $('.choose-address #address_id').val();
                $('.addrManage').show();
                $('#confirmOrderBtn').hide();
                $('#showAddrFormBtn').css("display", 'block').show();
                $('#saveAddrBtn').hide();
                $('.addrManage .addr-list .addr').removeClass("choosed");
                $('.addrManage .addr-list .addr[data-id="' + id + '"]').addClass("choosed");
            }
        },

        editAddr: function () {
            // open form
            $(document).on('click', ' .editAddrBtn', function (e) {
                // set field
                var name = $(this).attr('data-name');
                var phone = $(this).attr('data-phone');
                var province = $(this).attr('data-province');
                var city = $(this).attr('data-city');
                var town = $(this).attr('data-town');
                var address = $(this).attr('data-address');
                var id = $(this).attr('data-id');

                $('.saveAddrBtn').attr('data-id', id);

                new PCAS('province', 'city', 'town', province, city, town);

                $('.addrForm').find('#name').val(name);
                $('.addrForm').find('#phone').val(phone);
                $('.addrForm').find('#address').val(address);

                $('.addrManage .addrForm').show();

                $('#confirmOrderBtn').hide();
                $('#showAddrFormBtn').hide();
                $('#saveAddrBtn').css({"display": "block"}).show();

                e.stopImmediatePropagation();
            });
        },

        saveAddr: function () {
            $(document).on('click', ' .saveAddrBtn', function (e) {
                var name = $('.addrForm').find('#name').val();
                var phone = $('.addrForm').find('#phone').val();
                var province = $('.addrForm').find('#province').val();
                var city = $('.addrForm').find('#city').val();
                var town = $('.addrForm').find('#town').val();
                var address = $('.addrForm').find('#address').val();

                var id = $(this).attr('data-id');

                if (!checkField('.addrForm #name', '请填写正确的收件人姓名', /^[\u4E00-\u9FA5]{2,6}$/)) {
                    $('.addrForm #name').focus();
                    tip.showTip('err', "请填写正确的收件人姓名", 2000);
                    return;
                }

                if (!checkField('.addrForm #phone', '电话格式不正确', /^(1[\d]{10})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{3,7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$/)) {
                    $('.addrForm #phone').focus();
                    tip.showTip('err', "电话格式不正确", 2000);
                    return;
                }

                if (!checkField('.addrForm #province', '请选择省份')) {
                    tip.showTip('err', "请选择省份", 2000);
                    return;
                }

                if (!checkField('.addrForm #city', '请选择地区')) {
                    tip.showTip('err', "请选择地区", 2000);
                    return;
                }

                if (!checkField('.addrForm #town', '请选择县市')) {
                    tip.showTip('err', "请选择县市", 2000);
                    return;
                }

                if (town == "市辖区") {
                    tip.showTip('err', "请选择县市", 2000);
                    return;
                }

                if (!checkField('.addrForm #address', '地址长度6-30字符', /^([\u4E00-\u9FA5]|[0-9a-zA-Z_-]|\s){6,30}$/)) {
                    $('.addrForm #address').focus();
                    tip.showTip('err', "请正确填写地址，长度6-30字符", 2000);
                    return;
                }

                var data = {
                    name: name,
                    phone: phone,
                    province: province,
                    city: city,
                    town: town,
                    address: address
                };

                // api request
                requestApi('/api/addr/save', {'data': data, id: id}, function (res) {

                    tip.showTip('ok', '操作成功！', 0);

                    if (res.result == 1) {
                        requestApi('/api/addr/getList', '', function (list) {
                            if (list.result == 1) {
                                $('.addrManage .addrForm').hide();
                                $('.addrManage .addr-list').html(list.data);

                                var len = $('.addrManage .addr-list .addr').length;
                                // 只有一个数据时，直接返回
                                if (len == 1) {
                                    var obj = $('.addrManage .addr-list .addr .addr-content');
                                    var id = obj.attr('data-id');

                                    var html = obj.html();

                                    $('.choose-address .address-content').html(html);

                                    $('.choose-address #address_id').val(id);
                                    $('.choose-address .chosenAttrIcon').hide();

                                    // tap事件穿透？
                                    $('.addrManage').hide('display', 'none').hide();

                                    $('#confirmOrderBtn').css("display", 'block').show();
                                    $('#showAddrFormBtn').hide();
                                    $('#saveAddrBtn').hide();
                                } else {
                                    $('#confirmOrderBtn').hide();
                                    $('#saveAddrBtn').hide();
                                    $('#showAddrFormBtn').css({"display": "block"}).show();
                                }
                            }
                        }, true);
                    }
                });

                e.stopImmediatePropagation();
            });
        },

        setDefault: function () {
            $(document).on('click', '.addrManage .is_default', function (e) {
                // check
                if (!u.check_login()) {
                    return;
                }

                var id = $(this).attr('data-id');
                requestApi('/api/addr/setDefault', {id: id}, function (res) {
                    if (res.result == 1) {
                    }
                });

                e.stopImmediatePropagation();
            });
        },

        delAddr: function () {
            $(document).on('click', '.addrManage .rmAddrBtn', function (e) {
                // check
                if (!u.check_login()) {
                    return;
                }

                var id = $(this).attr('data-id');
                var cm = window.confirm('您确认删除改地址吗？');
                if (!cm) {
                    return;
                }
                requestApi('/api/addr/del', {id: id}, function (res) {
                    if (res.result == 1) {
                        $('.addrManage .addr-list .addr[data-id="' + id + '"]').fadeOut();

                        setTimeout(function () {
                            $('.addrManage .addr-list .addr[data-id="' + id + '"]').remove();
                        }, 100);

                        $('.choose-address .address_id').val('');
                        $('.choose-address .address-content').html('<p style="margin: 10px 0;"><a href="javascript:;" class="chooseAddr btn">添加新地址</a></p>');
                    }
                });

                e.stopImmediatePropagation();
            });
        }
    };
    /**
     *  订单生成
     *
     * @constructor
     */
    function OrderSettle() {
    }

    OrderSettle.prototype = {

        /**
         * 选择邮费
         */
        postage: function () {
            // 选择邮费
            $(document).on('change', '#postage', function (e) {
                var fee = $("#postage option").not(function () {
                    return !this.selected
                }).attr("data-fee");

                if ($(this).val() != "") {
                    fee = parseFloat(fee);
                    $("#postage_fee").val(fee);
                    var total = parseFloat($('.total_fee').attr('data-total'));
                    $('.total_fee').html('&yen; ' + (fee + total));
                } else {
                    var total = parseFloat($('.total_fee').attr('data-total'));
                    $("#postage_fee").val(0);
                    $('.total_fee').html('&yen; ' + (total));
                }
                $("#point_fee").val(0);
                $('#usePoint').val('');

                e.stopImmediatePropagation();
            });
        },
        /*
         * 选择配送方式
         *
         * */
        freight: function () {
            var _this = this;
            $(".freight_type").on('change', function () {
                var total_freight_fee = 0;
                // 计算总邮费
                $('.freight_type').each(function () {
                    var val = parseFloat($(this).find("option:selected").attr('data-val'));
                    val = isNaN(val) ? 0 : val;
                    total_freight_fee += val;
                });
                // 设置
                $('.total_fee').attr('data-freight_fee', total_freight_fee);

                // 计算最终支付
                _this.calcTotal();
            })
        },

        /**
         * 展开商品详情
         */
        itemDetail: function () {
            $(document).on('tap', '.order-detail .item .head', function (e) {
                var show = $(this).attr('data-show-flag');
                if (show) {
                    $(this).siblings('.detail').hide();
                    $(this).removeAttr('data-show-flag');
                } else {
                    $(this).siblings('.detail').show();
                    $(this).attr('data-show-flag', true);
                }

                e.stopImmediatePropagation();
            });
        },

        /**
         * 优惠劵
         */
        coupon: function () {
            var _this = this;
            $('.coupon-widget').on('change', '#coupon', function () {
                var optObj = $(this).find('option:selected');
                var tip = optObj.attr('data-coupon_tip');
                var coupon_minus = parseFloat(optObj.attr('data-coupon_minus'));
                $('.coupon-widget .coupon-tip').html(tip);
                // 优惠金额
                coupon_minus = isNaN(coupon_minus) ? 0 : coupon_minus;
                $(".total_fee").attr("data-coupon_minus", coupon_minus);
                // 计算最终支付
                _this.calcTotal();
            });
        },

        // 积分选择
        pointChoose: function () {
            var _this = this;
            var available_points = parseInt($("#pointsBox").val());//用户可用积分
            var redeem_proportion = parseInt($("#redeem_proportion").val());//积分兑换比例
            $("input[name='pointsCheckbox']").on('change', function () {
                if ($(this).prop('checked')) {
                    /*没有积分*/
                    if (available_points == 0) {
                        tip.showTip('err', '你的积分不足', 500);
                        $(this).each(function () {
                            this.checked = !this.checked;
                        });
                        return false;
                    }
                    $('#usePoint').css('display', 'inline-block');
                } else {
                    $('#usePoint').css('display', 'none');
                    $("#usePoint").val('');
                    var total = parseFloat($(".total_fee").attr('data-total'));
                    var coupon_minus = parseFloat($(".total_fee").attr('data-coupon_minus'));
                    var freight_fee = parseFloat($(".total_fee").attr('data-freight_fee'));
                    $(".total_fee").html("&yen" + (total - coupon_minus + freight_fee));
                }
            });

            $('#usePoint').on('keyup', function () {
                var points = $('#usePoint').val();
                if ($.trim(points) == '') {
                    tip.showTip('err', '请输入积分', 1000);
                    $(".total_fee").html("&yen" + $(".total_fee").attr('data-total'));
                    $(this).focus();
                    return false;
                }
                /*输入为非数字*/
                if (isNaN(points)) {
                    $(this).val('');
                    $(this).focus();
                    return false;
                }

                if (points.indexOf('.') > 0) {
                    $(this).val(points.substring(0, points.length - 1));
                    $(this).focus();
                    return false;
                }

                /*输入的积分数小于0*/
                if (parseInt(points) <= 0 && points != '') {
                    tip.showTip('err', '积分必须大于0', 1000);
                    $(this).focus();
                    return false;
                }

                /*输入的积分数大于总积分*/
                if (points > available_points) {
                    tip.showTip('err', '积分必须小于' + available_points, 1000);
                    $(this).val(available_points);
                    $(this).focus();
                    return false;
                }
            });

            // 使用积分
            $('#usePoint').on('blur', function () {
                if ($("#pointsCheckbox").prop("checked") != false) {
                    if (_this.checkUsefulPoint()) {
                        // 计算最终支付
                        _this.calcTotal();
                    }
                }
            })
        },

        // 检测订单支付积分
        checkUsefulPoint: function () {
            var pointChkBox = $("#pointsCheckbox");
            if (pointChkBox.length > 0 && pointChkBox.prop("checked") != false) {
                var available_points = parseInt($("#pointsBox").val());//用户可用积分
                var redeem_proportion = parseInt($("#redeem_proportion").val());//积分兑换比例
                var points = parseInt($('#usePoint').val());
                /*输入为非数字*/
                if (isNaN(points)) {
                    $(this).val('');
                    $('#usePoint').focus();
                    return false;
                }
                /*输入的积分数小于0*/
                if (points <= 0 || points == '') {
                    tip.showTip('err', '积分必须大于0', 1000);
                    $('#usePoint').focus();
                    return false;
                }

                /*输入的积分数大于总积分*/
                if (points > available_points) {
                    tip.showTip('err', '积分必须小于' + available_points, 1000);
                    $('#usePoint').focus();
                    return false;
                }
            }
            return true;
        },

        // 计算最终支付
        calcTotal: function () {
            var redeem_proportion = parseFloat($("#redeem_proportion").val());//积分兑换比例
            var points = parseInt($('#usePoint').val());
            var postage_fee = parseFloat($(".total_fee").attr('data-freight_fee'));
            var coupon = parseFloat($(".total_fee").attr('data-coupon_minus'));
            var total_paid = parseFloat($(".total_fee").attr('data-total'));//商品价
            var sum = total_paid - coupon + postage_fee;//商品价加运费
            var use_point = 0;
            points = isNaN(points) ? 0 : points;
            if (isNaN(redeem_proportion) || points == 0) {
                $(".total_fee").html('&yen;' + sum);
                $('#usePoint').val(0);
            } else {
                //积分足够
                if (sum * redeem_proportion - points < 0) {
                    $(".total_fee").html('&yen;0');
                    use_point = Math.ceil(sum * redeem_proportion);
                    $('#usePoint').val(use_point);
                    $("#point_fee").val(sum);
                } else {

                    //积分不够,商品使用积分,运费使用付款方式
                    if ((Math.ceil(sum * redeem_proportion) < points) && points > (total_paid * redeem_proportion)) {
                        $(".total_fee").html('&yen;' + postage_fee);
                        use_point = Math.ceil(total_paid * redeem_proportion);
                        $('#usePoint').val(use_point);

                        $("#point_fee").val(total_paid * redeem_proportion);
                    } else {
                        var last = sum - points / redeem_proportion;

                        $(".total_fee").html('&yen;' + last.toFixed(2));
                        use_point = Math.ceil(points);
                        $('#usePoint').val(use_point);
                        $("#point_fee").val(Math.round(points / redeem_proportion));
                    }
                }
            }
        },

        /**
         * 生成订单
         */
        genOrder: function () {
            this.postage();
            this.freight();
            this.itemDetail();
            this.coupon();
            this.pointChoose();
            var _this = this;
            // 普通结算生成订单(/shop/settle)
            $(document).on('click', '.generateOrderBtn', function (e) {
                if (!u.check_login()) {
                    return;
                }

                //  检测可用积分
                if (!_this.checkUsefulPoint()) {
                    return;
                }
                var use_point = $("#usePoint").val();

                var address_id = $('#address_id').val();
                if (!address_id || address_id == undefined) {
                    tip.showTip('err', '请填写收货地址信息！', 1000);
                    return;
                }

                var freight = [];
                $(".freight_type").each(function () {
                    freight.push($(this).val());
                });

                var all_item = $('#all_item').val();
                var remark = $('.remark').val();

                var coupon = $('#coupon').val();

                var data = {
                    remark: remark,
                    t: all_item,
                    freight: freight,
                    addr: address_id,
                    coupon: coupon,
                    use_point: use_point
                };
                // api request
                requestApi('/api/order/generateOrder', data, function (res) {
                    if (res.result == 1) {
                        tip.showTip('ok', '订单生成成功~! 即将跳转到支付..', 2000);

                        window.location.href = '/order/pay?order=' + res.data;
                    }
                });

                e.stopImmediatePropagation();
            });
        },

        /**
         * 积分商城订单
         */
        pointOrder: function () {
            this.postage();
            this.itemDetail();
            // 积分换购生成订单(/addon/pointmall/settle)
            $('.pointMallOrderBtn').click(function (e) {
                if (!u.check_login()) {
                    return;
                }

                var address_id = $('#address_id').val();
                if (!address_id || address_id == undefined) {
                    tip.showTip('err', '请填写收货地址信息！', 1000);
                    return;
                }

                var item = $('#order_item').val();
                var remark = $('.remark').val();
                var post_key = $('#postage').val();

                // api request
                requestApi('/addon/api/pointmall/generateOrder', {
                    'remark': remark,
                    t: item,
                    postage: post_key,
                    addr: address_id,
                    'type': 'pointmall'
                }, function (res) {
                    if (res.result == 1) {
                        tip.showTip('ok', '订单生成成功~! 即将跳转到支付..', 2000);
                        if (res.data.paid) {
                            window.location.href = '/order/detail/' + res.data.order_number;
                        } else {
                            window.location.href = '/order/pay?order=' + res.data.order_number;
                        }
                    }
                });

                e.stopImmediatePropagation();
            });
        }
    };

    exports.orderStatus = function () {
        $('.delOrderBtn').on('click', function (e) {
            var order = $(this).attr('data-order');

            var cm = window.confirm('你确认需要删除该订单吗，删除后数据不存在?');
            if (!cm) {
                return false;
            }

            requestApi('/api/order/delOrder', {order: order}, function (res) {
                if (res.result == 1) {
                    $('.order-wrap[data-order="' + order + '"]').fadeOut();
                    setTimeout(function () {
                        $('.order-wrap[data-order="' + order + '"]').remove();
                    }, 1000);
                    tip.showTip('ok', '订单已删除', 1000);
                }
            });

            e.stopImmediatePropagation();
        });

        $('.cancelOrderBtn').on('click', function (e) {
            var order = $(this).attr('data-order');

            var cm = window.confirm('你确认需要删除该订单吗?');
            if (!cm) {
                return false;
            }

            requestApi('/api/order/cancelOrder', {order: order}, function (res) {
                if (res.result == 1) {
                    setTimeout(function () {
                        $('.order-wrap[data-order="' + order + '"] .foot .right').html('<span class="money">关闭交易</span>');
                    }, 300);

                    tip.showTip('ok', '订单已取消', 1000);
                }
            });

            e.stopImmediatePropagation();
        });

        $('.confirmReceiptBtn').on('click', function (e) {
            var order = $(this).attr('data-order');
            var _this = $(this);

            var cm = window.confirm('你确定需要确认收货吗?');
            if (!cm) {
                return false;
            }

            requestApi('/api/order/confirmReceipt', {order: order}, function (res) {
                if (res.result == 1) {
                    setTimeout(function () {
                        _this.parent().html('<span class="money">交易完成</span>');
                    }, 300);
                    tip.showTip('ok', '交易完成', 1000);
                    window.location = window.location
                }
            });

            e.stopImmediatePropagation();
        });
    }


    // 结算页面初始化
    exports.
        settlePageInit = function () {
        var settle = new OrderSettle();
        settle.genOrder();
        new UserAddress();
    };

    // 积分商城结算
    exports.settlePointMall = function () {
        var settle = new OrderSettle();
        settle.pointOrder();
        new UserAddress();
    };

    exports.addrManage = function () {
        new UserAddress();
    };
    /*退换货申请提交*/
    exports.backBtn = function (e) {
        $('.btn-back').click(function () {
            var type = $('#type').val();
            if (type == 2) {
                var reason_option = $("select[name='reason_option']").val();
                var reason = $.trim($("#reason").val());
                var tips = $("#reason").attr("placeholder");
                var reason_option_msg = $(".back option[value='" + reason_option + "']").text();
            }
            else {
                var reason_option = $("select[name='reason_option_exchange']").val();
                var reason = $.trim($("#reason_exchange").val());
                var tips = $("#reason_exchange").attr("placeholder");
                var reason_option_msg = $(".exchange option[value='" + reason_option + "']").text();
            }
            var id = $("#item_id").val();
            if (reason_option == 0) {
                alert(reason_option_msg);

                return false;
            }
            if (reason == tips || reason == '') {
                alert(tips);
                return false;
            }
            var data = {"type": type, "reason_option": reason_option_msg, 'reason': reason, 'id': id};
            requestApi('/api/order/returnApply', data, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '申请成功', 1000);
                    window.location = '/order/mine?b=1009'
                }
            });
            //  e.stopImmediatePropagation();
        })
        $(".reason").focus(function () {
            $(this).text('');
        })
        $("#button .prev").on('click', function () {
            history.go(-1);
        })
        $("#button .submit").on('click', function () {
            var deliver_no = $("#deliver_no").val();
            var deliver_logistics = $("#deliver_logistics").val();
            var preg = /^[0-9]+$/;
            if (!preg.test(deliver_no)) {
                tip.showTip('err', '请输入正确的运单号', 1000);
                $("#deliver_no").focus();
                return false;
            }
            if ((deliver_logistics == 'shunfeng' || deliver_logistics == 'zhongtong' || deliver_logistics == 'shentong') && deliver_no.length != 12) {
                tip.showTip('err', '请输入正确的运单号', 1000);
                $("#deliver_no").focus();
                return false;
            }
            if (deliver_logistics == 'yunda' && deliver_no.length != 13) {
                tip.showTip('err', '请输入正确的韵达运单号', 1000);
                $("#deliver_no").focus();
                return false;
            }
            if (deliver_logistics == 'yuantong' && deliver_no.length != 10) {
                tip.showTip('err', '请输入正确的圆通运单号', 1000);
                $("#deliver_no").focus();
                return false;
            }
            var id = $("#item_id").val();
            var data = {"deliver_no": deliver_no, "deliver_logistics": deliver_logistics, 'id': id};
            requestApi('/api/order/backGood', data, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '提交成功', 1000);
                    window.location = '/order/mine?b=1009'
                }
            });


        })


    };

    /*退货*/
    exports.cancelBack = function (e) {
        $(".cancelBack").on('click', function () {
            var id = $(this).parent().attr('data-id');
            requestApi('/api/order/cancelBack', {'id': id}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '取消成功', 1000);
                    window.location = window.location
                }
            })
            //  e.stopImmediatePropagation();
        });
        /*回寄商品*/
        $(".backGood").on('click', function () {
            var id = $(this).parent().attr('data-code');
            window.location = '/order/backGood/' + id;
        })
    };
    /*评价*/

    exports.rate = function (e) {
        $("#rateFormBtn").on('click', function () {
            var data = $('form#rateForm').serializeObject();
            requestApi('/api/order/commentSubmit', {data: data}, function (res) {
                if (res.result == 1) {
                    window.location = "/order/mine";
                }
            })
        })
    }


});