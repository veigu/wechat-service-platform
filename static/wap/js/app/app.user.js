define(function (require, exports) {
    require('base.js');
    require('/static/panel/js/tools/region.select.js');
    require('/static/wap/gmu/widget/dialog.js');

    exports.user_dialog = function () {
        // 注册窗口
        $(document).on('click', '.switchToRegBtn', function (e) {
          //  $('#user-login-dialog').dialog('close');
          //  $('#user-reg-dialog').dialog('open');
            window.location.href='/user';
            e.stopImmediatePropagation();
        });

        // 登陆窗口
        $(document).on('click', '.switchToLoginBtn', function (e) {
            $('#user-login-dialog').dialog('open');
            $('#user-reg-dialog').dialog('close');

            e.stopImmediatePropagation();
        });

        // 登陆注册接口
        exports.user_login(false);
        exports.user_reg(false);
    };

    /**
     * check login and open login dialog if not login
     */
    exports.check_login = function (hideLoginDialog) {
        if (!($.cookie('_APP_CUID') && $.cookie('_APP_CUID') > 0)) {

            if (!hideLoginDialog) {
                $('#user-login-dialog').dialog('open');
            }
            return false;
        }
        return true;
    };

    /**
     * Created by yanue on 6/17/14.
     */
    exports.user_login = function (url) {
        // 登陆
        $('#loginForm .loginBtn').on('click', function () {
            var phone = $('#loginForm .phone').val();
            var pass = $('#loginForm .password').val();

            if (!checkField('#loginForm .phone', '手机号不正确！', /^(1[\d]{10})$/)) {
                tip.showTip('err', '手机号不正确', 1500);
                return false;
            }

            if (!checkField('#loginForm .password')) {
                tip.showTip('err', '密码不能为空', 1500);
                return false;
            }
            var data = {'phone': phone, 'pass': pass};

            requestApi('/api/account/login', data, function (res) {

                // 用户密码不正确
                if (res.error.code == 1008) {
                    $('#goForgotBtn').attr('data-phone', phone).css({'display': 'inline'});
                } else {
                    $('#goForgotBtn').hide();
                }

                if (res.result == 1) {
                    if (url === false) {
                        setTimeout(function () {
                            $('#user-login-dialog').dialog('close');
                        }, 1000);

                        // 积分
                        window.app.user_point = res.data.point;
                        window.location.reload();
                    } else {
                        window.location.href = url || '/user';
                    }
                    tip.showTip('ok', '登陆成功', 2000);
                }
            });
        });

        // 跳转到找回密码
        $(document).on('click', '#goForgotBtn', function (e) {
            var phone = $(this).attr('data-phone');

            if (!phone) {
                tip.showTip('');
            }

            var go = $(this).attr('data-go');

            window.location.href = '/user/forgot?p=' + $.base64.encode(phone) + '&go=' + go;

            e.stopImmediatePropagation();
        });
    };

    exports.user_reg = function (url) {
        /*
         exports.send_code();


         exports.check_phone_exists(function() {
         $('#sendCodeBtn').removeClass('btn-gray').addClass('btn-green').attr('disabled', false);
         tip.showTip('ok', "该手机可以注册", 1000);
         }, function() {
         tip.showTip('ok', "该手机已经注册，您可以直接登录", 1000);
         $('#sendCodeBtn').removeClass('btn-green').addClass('btn-gray').attr('disabled', true);
         });
         */
        exports.check_nick_exists(function () {
            $('#signUpForm .regBtn').removeClass('btn-gray').addClass('btn-green').attr('disabled', false);
            tip.showTip('ok', "该昵称可以注册", 1000);
        }, function () {
            tip.showTip('ok', "该昵称已经被其他人使用，请您更换一个", 1000);
            $('#signUpForm .regBtn').removeClass('btn-green').addClass('btn-gray').attr('disabled', true);
        });

        $('#phoneCheckForm .nextBtn').on('click', function () {
            var isOk = $('#verifyCode').attr('data-checked');
            if (isOk && isOk == "true") {
                $('#phoneCheckForm').hide();
                $('#signUpForm').show();
            }
            else {
                tip.showTip("ok", "手机没有通过验证！", 1000);
                return false;
            }
        });

        $('#signUpForm .regBtn').on('click', function (e) {

            var isDisabled = $(this).attr('disabled');
            if (isDisabled && isDisabled == 'true') {
                return false;
            }

            var name = $('#signUpForm .username').val();
            var phone = $('#signUpForm .phone').val();
            var pass = $('#signUpForm .password').val();
            var repass = $('#signUpForm .repassword').val();

            if (!checkField('#signUpForm .username')) {
                return false;
            }

            if (!checkField('#signUpForm .password')) {
                return false;
            }

            if (!checkField('#signUpForm .repassword')) {
                return false;
            }

            if (repass != pass) {
                $('#signUpForm .repassword').siblings('.tip').html('两次输入的密码不一致！');
                return false
            } else {
                $('#signUpForm .repassword').siblings('.tip').html('');
            }

            var data = {
                'username': name,
                'phone': phone,
                'pass': pass
            };

            requestApi('/api/account/reg', data, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '注册成功', 1000);
                    setTimeout(function () {
                        window.location.href = "/user";
                        /*if(is_weixin()) {
                         window.location.href = "/user/bindWechat";
                         }
                         else if(is_weibo()) {
                         window.location.href = "/user/bindWeibo";
                         }
                         else {
                         window.location.href = "/user/bind";
                         }*/
                    }, 2000);

                    /*if (url === false) {
                     setTimeout(function () {
                     $('#user-reg-dialog').dialog('close');
                     }, 1000);

                     // 积分
                     window.app.user_point = res.data.point;
                     window.location.reload();
                     } else {
                     window.location.href = url || '/user';
                     }*/
                }
                else {
                    //已经注册过，直接登录
                    if (res.error.code == 1016) {

                    }
                }
            });

            e.stopImmediatePropagation();
        });
    };

    exports.bind_wechat = function (url) {

    };

    exports.bind_weibo = function (url) {

    };

    exports.check_phone_exists = function (okCall, failCall) {
        $('.phone').on('change', function () {
            if (!checkField(".phone", '手机号码格式不正确', /^(1[\d]{10})$/)) {
                return false;
            }
            var phone = $('.phone').val();
            requestApi('/api/account/checkPhone', {phone: phone}, function (res) {
                if (res.result == 1) {
                    failCall();
                }
                else if (res.result == 2) {
                    okCall();
                }
                else {

                }
            });
        });
    };

    exports.check_nick_exists = function (okCall, failCall) {
        $('.username').on('blue', function () {
            if (!checkField(".username", '用户名', /^(1[\d]{10})$/)) {
                return false;
            }
            var phone = $(this).val();
            requestApi('/api/account/checkNick', {phone: phone}, function (res) {
                if (res.result == 1) {
                    tip.showTip("ok", "昵称可用", 1000);
                    okCall();
                }
                else if (res.result == 2) {
                    failCall();
                }
            });
        });
    };

    exports.user_profile = function () {
        $('#settingForm .saveBtn').on('click', function () {
            var name = $('#settingForm .username').val();
            var email = $('#settingForm .email').val();
            var gender = $('#settingForm .gender:checked').val();
            var province = $('#settingForm #province').val();
            var city = $('#settingForm #city').val();
            var town = $('#settingForm #town').val();

            if (!checkField('#settingForm .username')) {
                return false;
            }

            if (!checkField('#settingForm .email', '邮箱格式不正确', /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/)) {
                return false;
            }

            if (!gender || gender == undefined) {
                $('#settingForm .gender').siblings('.tip').html('不能为空');
                return false;
            } else {
                $('#settingForm .gender').siblings('.tip').html('');
            }

            var data = {
                'username': name,
                'email': email,
                'gender': gender,
                'province': province,
                'city': city,
                'town': town
            };

            requestApi('/api/user/saveProfile', data, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '修改成功', 1000);
                    window.location.href = '/user';
                }
            });

            e.stopImmediatePropagation();
        });
    };


    exports.user_forgot = function (url) {
        // 跳转到找回密码
        $('#forgotPassForm .resetPassBtn').on('click', function (e) {
            var email = $('#forgotPassForm .email').val();
            var phone = $(this).attr('data-phone');
            if (!checkField('#forgotPassForm .email', '邮箱格式不正确', /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/)) {
                return false;
            }

            requestApi('/api/account/sendPass', {email: email, phone: phone}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '重置后的密码已发送到指定邮箱～！', 3000);
                    setTimeout(function () {
                        window.location.href = '/user/login?go=' + encodeURI(url);
                    }, 2000);
                }
            });

            e.stopImmediatePropagation();
        });
    };

    exports.user_resetPass = function () {
        // 重新设置密码
        $('#resetPassForm .resetPassBtn').on('click', function (e) {
            var oldPass = $('#resetPassForm .old_pass').val();
            var pass = $('#resetPassForm .password').val();
            var repass = $('#resetPassForm .repassword').val();

            if (!checkField('#resetPassForm .old_pass')) {
                return false;
            }

            if (!checkField('#resetPassForm .password')) {
                return false;
            }

            if (!checkField('#resetPassForm .repassword')) {
                return false;
            }

            if (repass != pass) {
                $('#resetPassForm .repassword').siblings('.tip').html('两次输入的密码不一致！');
                return false
            } else {
                $('#resetPassForm .repassword').siblings('.tip').html('');
            }

            requestApi('/api/user/resetPass', {oldPass: oldPass, pass: pass}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '密码已修改，下次登陆有效～！', 3000);
                    setTimeout(function () {
                        window.location.href = '/user/setting';
                    }, 2000);
                }
            });
            e.stopImmediatePropagation();
        });
    };

    exports.send_code = function () {
        $('#sendCodeBtn').on('click', function (e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }
            if (!checkField('.phone', '手机号不正确！', /^(1[\d]{10})$/)) {
                return false;
            }

            var disabled = $(this).attr('disabled');
            if (disabled && disabled == 'true') {
                return false;
            }
            var phone = $('.phone').val();
            requestApi('/api/account/sendVerifyCode', {phone: phone}, function (res) {
                if (res.result == 1) {
                    tip.showTip('ok', '验证码已经发送，请稍等……！', 3000);
                    codeTimer('sendCodeBtn', 60);
                }
            });
            e.stopImmediatePropagation();
        });

        $('#verifyCode').on('blur', function (e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else {
                e.returnValue = false;
            }
            if (!checkField('#verifyCode', '验证码不正确！', /^([\d]{6})$/)) {
                return false;
            }
            var isChecked = $('#verifyCode').attr('data-checked');
            if (isChecked == 'true') {
                return false;
            }
            var code = $('#verifyCode').val();
            requestApi('/api/account/checkVerifyCode', {code: code}, function (res) {
                if (res.result == 0) {
                    tip.showTip('alert', '验证码验证码错误！', 3000);
                }
                else {
                    $('#verifyCode').attr('data-checked', true);
                    $('.code-next-btn').removeClass('btn-gray').addClass('btn-green');
                }
            });
            e.stopImmediatePropagation();
        });
    };

    exports.bind_phone = function (refer) {
        exports.send_code();
        // 绑定
        $('#bindForm .bind-btn').on('click', function (e) {
            var phone = $('#bindForm #phone').val();
            var code = $('#bindForm #verifyCode').val();

            if (!checkField('#bindForm #phone', '手机号不正确！', /^(1[\d]{10})$/)) {
                return false;
            }

            if (!checkField('#bindForm #verifyCode')) {
                return false;
            }

            requestApi('/api/user/bindPhone', {}, function (res) {
                if (res.result == 1) {
                    if (res.data.need_pwd) {
                        tip.showTip('ok', '手机号已绑定！还没有设置密码，请设置一个登陆密码', 3000);
                        $('#setPassForm').show();
                        $('#bindForm').hide();
                        exports.setPassword(refer);
                    }
                    else {
                        tip.showTip('ok', '手机号已绑定成功', 3000);
                        setTimeout(function () {
                            if (refer) {
                                window.location.href = refer;
                            }
                            else {
                                window.location.href = '/user/setting';
                            }
                        }, 2000);
                    }
                }
                e.stopImmediatePropagation();
            });
        });

        exports.setPassword = function (refer) {
            $('#setPassForm .set-pwd-btn').on('click', function () {
                if (!checkField('')) {
                    return false;
                }

                if (!checkField('')) {
                    return false;
                }

                var pass = $('#resetPassForm .password').val();
                var repass = $('#resetPassForm .repassword').val();

                if (repass != pass) {
                    $('#setPassForm .repassword').siblings('.tip').html('两次输入的密码不一致！');
                    return false
                } else {
                    $('#setPassForm .repassword').siblings('.tip').html('');
                }

                requestApi('/api/user/setPass', {pass: pass}, function (res) {
                    if (res.result == 1) {
                        tip.showTip('ok', '密码已设置，下次登陆有效～！', 3000);
                        setTimeout(function () {
                            if (refer) {
                                window.location.href = refer;
                            }
                            else {
                                window.location.href = '/user/setting';
                            }
                        }, 2000);
                    }
                });
                e.stopImmediatePropagation();
            });
        };


    };
    /*删除收藏*/
    exports.delCollection=function(){
        $(".delCollection").on('click',function(){
            var id=$(this).attr('data-id');
            requestApi('/api/user/rmCollect',{id:id},function(res){
                if(res.result==1)
                {
                    window.location.reload();
                }
            })


        })


    }

    function codeTimer(targetId, times) {
        var target = $('#' + targetId);
        if (target) {
            if (times && times > 0) {
                target.attr('disabled', true);
                if (times == 60) {
                    target.removeClass('btn-green');
                    target.addClass('btn-gray');
                }
                var newTimes = times - 1;
                target.html('<span style="color: red;">' + newTimes + "</span>秒后可重发");
                setTimeout(function () {
                    codeTimer(targetId, times - 1);
                }, 1000);
            }
            else {
                target.removeClass('btn-gray');
                target.addClass('btn-green');
                target.attr('disabled', false);
                target.html("重发验证码");
            }
        }
    }
});
