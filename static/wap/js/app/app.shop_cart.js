define(function (require, exports) {

    require('base.js');

    var u = require('app/app.user.js');

    /**
     * Created by yanue on 6/9/14.
     */

    function ShopCart() {

    }

    ShopCart.total_fee = 0;
    ShopCart.chosed_spec = ""; //已经选好的商品规格
    ShopCart.prototype = {
        buyImmediately: function (btn, hideLoginDialog) {

            var _this = this;
            if (!btn) return;
            $(document).on('click', btn, function (e) {
                // check
                if (!u.check_login(hideLoginDialog) && hideLoginDialog) {
                    window.location.href = '/user';
                }
                else if (!u.check_login(hideLoginDialog) && !hideLoginDialog) {
                    return;
                }
                // 商品信息
                var num = $('.buy-wrap .item_quantity').val();
                var id = $(this).attr('data-id');
                var has_spec = $(this).attr('data-has_spec');
                if (has_spec == "must" && !ShopCart.chosed_spec) {
                    tip.showTip('err', '请选择商品规格~！', 3000);
                    $('.buy-wrap .product-spec').show();
                    return;
                }

                var item = {
                    "id": id,
                    "num": num,
                    "spec": ShopCart.chosed_spec
                };
                var location = '/order/settle';

                // 跳转到结算
                window.location.href = location + '/' + $.base64.encode(JSON.stringify(item)) + '?f=item';

                e.stopImmediatePropagation();
            })
        },
        /**
         * 添加到购物车
         */
        addToCart: function (btn) {
            if (!btn) return;

            $(document).on('click', btn, function (e) {
                var num = $('.buy-wrap .item_quantity').val();
                var id = $(this).attr('data-id');
                var has_spec = $(this).attr('data-has_spec');

                if (has_spec == "must" && ShopCart.chosed_spec == "") {
                    tip.showTip("err", '请先选择商品规格~！', 3000);
                    $('.buy-wrap .product-spec').show();
                    return;
                }

                var item = {
                    "id": id,
                    "num": num,
                    "spec": ShopCart.chosed_spec
                };

                var data_tmp = new Object();
                var cookie_data = $.cookie('_my_cart_all');
                // 原有订单数据
                if (cookie_data) {
                    var old_data = JSON.parse($.base64.decode(cookie_data));
                    if (old_data[id + ShopCart.chosed_spec]) {
                        // 替换原有数据
                        data_tmp[id + ShopCart.chosed_spec] = item;
                    } else {
                        // 增加到原有
                        old_data[id + ShopCart.chosed_spec] = item;
                    }
                    // replace
                    data_tmp = old_data;
                } else {
                    // 避免数组自动长度
                    data_tmp[id + ShopCart.chosed_spec] = item;
                }

                $.cookie('_my_cart_all', $.base64.encode(JSON.stringify(data_tmp)), {expires: 1, path: '/'});

                tip.showTip('ok', '已加入购物车', 500);

                e.stopImmediatePropagation();
            })
        },

        // 积分换购
        redemption: function () {
            $('.btn-buy').click(function (e) {
                // check
                if (!u.check_login()) {
                    return;
                }
                var has_spec = $(this).attr('data-has_spec');

                if (has_spec == "must" && ShopCart.chosed_spec == "") {
                    tip.showTip("err", '请先选择商品规格~！', 3000);
                    $('.buy-wrap .product-spec').show();
                    return;
                }

                var prod_id = $(this).attr('data-id');
                var quantity = $('.item_quantity[data-id="' + prod_id + '"]').val();
                // get need point
                var cost_point = $(this).attr('data-point');

                if (!(app.user_point && app.user_point >= cost_point * quantity)) {
                    tip.showTip('err', '积分不够！ 您的积分:' + app.user_point + ' 所需积分:' + cost_point * quantity, 3000);
                    return;
                }

                if (!confirm("兑换提示:\n积分商城的商品，一经兑换，概不退货，不退积分！")) {
                    return false;
                }

                var item = {
                    "id": prod_id,
                    "num": quantity,
                    "spec": ShopCart.chosed_spec
                };
                var location = '/addon/pointmall/settle';
                // 跳转到结算
                window.location.href = location + '/' + $.base64.encode(JSON.stringify(item));

                e.stopImmediatePropagation();
            });

        },

        /**
         * 选择商品数量
         */
        choiceQuantity: function () {
            var _this = this;

            // 选择型号
            $('.product-spec .spec .spec_val').on('click', function () {
                var key = $(this).attr('data-key');
                var spec_id = $(this).attr('data-spec_id');

                $('.product-spec .spec[data-spec_id="' + spec_id + '"]').attr('data-spec_key', key);
                $(this).siblings().removeClass('current');
                $(this).addClass('current');

                upSpecInfo($(this));
            });

            // 更新规格信息
            function upSpecInfo(elem) {
                // 清空
                ShopCart.chosed_spec = "";
                var spec_unique_key = '';
                var spec_all_data = $('.product-spec .spec_all_data').val();
                if (spec_all_data) {
                    spec_all_data = JSON.parse($.base64.decode(spec_all_data));
                    if (!spec_all_data) {
                        tip.showTip('err', '请不要改写参数!', 3000);
                        return;
                    }
                }

                $('.product-spec .spec').each(function () {
                    var spec_id = $(this).attr('data-spec_id');
                    var spec_key = $(this).attr('data-spec_key');

                    spec_unique_key += spec_id + ':' + spec_key + ';';
                });
                var chosed_spec = spec_all_data[spec_unique_key];
                // 匹配选择
                if (chosed_spec) {
                    if (chosed_spec.num == 0) {
                        tip.showTip('err', '该规格商品库存不足', 3000);
                        elem.removeClass('current');
                        $('.inventory .num').text(0);
                        return;
                    }
                    ShopCart.chosed_spec = chosed_spec.spec_unique_key;
                    // 更新可输入
                    $('.product-num .item_quantity').val(1).attr('data-sell_price', chosed_spec.sell_price)
                        .attr('data-original_price', chosed_spec.original_price)
                        .attr('data-inventory', chosed_spec.num)
                        .attr('data-spec_unique_key', spec_unique_key);

                    // 更新库存
                    $('.inventory .num').text(chosed_spec.num);
                    $('.price-wrap .price').html("¥ " + chosed_spec.sell_price);
                    $('.price-wrap #priceMarket').html(chosed_spec.original_price);
                }
            }

            // plus
            $(document).on('click', '.plusBtn', function (e) {
                var id = $(this).siblings('.item_quantity').attr('data-id');
                var inventory = parseInt($(this).siblings('.item_quantity').attr('data-inventory'));
                var limit_num = parseInt($(this).siblings('.item_quantity').attr('data-limit'));
                var price = parseFloat($(this).siblings('.item_quantity').attr('data-price'));
                var quantity = parseInt($('.item_quantity[data-id="' + id + '"]').val());

                // 购买限制
                inventory = (!isNaN(limit_num) && limit_num > 0) ? limit_num : inventory;

                if (quantity < inventory) {
                    var num = 1 + quantity;
                    var total_price = (num * price);
                    _this._set_cart(id, num, total_price);
                }

                e.stopImmediatePropagation();
            });

            // minus
            $(document).on('click', '.minusBtn', function (e) {
                var id = $(this).siblings('.item_quantity').attr('data-id');
                var inventory = parseInt($(this).siblings('.item_quantity').attr('data-inventory'));
                var price = parseFloat($(this).siblings('.item_quantity').attr('data-price'));

                var quantity = parseInt($('.item_quantity[data-id="' + id + '"]').val());
                if (quantity > 1) {
                    var num = quantity - 1;
                    var total_price = (num * price);
                    _this._set_cart(id, num, total_price);
                }

                e.stopImmediatePropagation();
            });

            // change val
            $(document).on('change', '.item_quantity', function (e) {
                var id = $(this).attr('data-id');
                var inventory = parseInt($(this).attr('data-inventory'));
                var quantity = parseInt($(this).val());
                var price = parseFloat($(this).attr('data-price'));
                var limit_num = parseInt($(this).attr('data-limit'));

                // 避免类似12ewe不合理数据存在
                _this._set_cart(id, quantity, (quantity * price));

                // 购买限制
                inventory = (!isNaN(limit_num) && limit_num > 0) ? limit_num : inventory;

                if (quantity > inventory) {
                    _this._set_cart(id, inventory, (inventory * price));
                }

                if (quantity < 1) {
                    _this._set_cart(id, 1, price);
                }

                if (isNaN(quantity)) {
                    _this._set_cart(id, 1, price);
                }

                e.stopImmediatePropagation();
            });
        },


        _set_cart: function (id, num, total_price) {
            $('.item_quantity[data-id="' + id + '"]').val(num);
            $('.item_total[data-id="' + id + '"]').html('&yen; ' + total_price);
            // 购物车页面
            $('.num[data-id="' + id + '"]').text(num);
            $('.chk[data-id="' + id + '"]').attr('data-num', num).attr('data-item-total', total_price);
        },

        editCart: function () {
            $(document).on('click', '.saveCartBtn', function (e) {
                $('.shop-cart  .item .detail').show();
                $('.shop-cart  .item .edit').hide();
                $('.shop-cart  .item .delCart').hide();

                $('.openEditModelBtn').css("display", "inline-block").show();
                $(this).hide();

                // 开启checkbox功能
                $('.chooseTotal').removeAttr('disabled');
                $('.chk').removeAttr('disabled');

                // update cart db
                var data = [];
                $('.shop-cart  .item_quantity').each(function () {
                    var id = $(this).attr('data-id');
                    var num = $(this).val();
                    var item = {"id": id, "num": num};
                    data.push(item);
                });
                if (data.length > 0) {
                    requestApi('/api/order/upCart', {data: JSON.stringify(data)}, function (res) {
                        if (res.result == 1) {
                            tip.showTip('ok', '购物车修改成功', 2000);
                        }
                    });
                }
                // 显示复选框
                $(".cart-page .chk").show();
                $(".cart-page .chooseTotal").show();

                e.stopImmediatePropagation();
            });

            $(document).on('click', '.openEditModelBtn', function (e) {
                $('.shop-cart  .item .detail').hide();
                $('.shop-cart  .item .edit').show();
                $('.shop-cart  .item .delCart').show();

                $('.saveCartBtn').css("display", "inline-block").show();

                // 禁用checkbox功能
                $('.chooseTotal').attr('disabled', true);
                $('.chk').attr('disabled', true);

                // 取消选中
                $(".shop-cart  .item .chk").prop("checked", false);
                $(".settlement .chooseTotal").prop("checked", false);
                $(".settlement .total").html("&yen; 0.00");
                ShopCart.total_fee = 0;

                //禁止结算
                $('.settleBtn').removeClass('btn-red');

                $(this).hide();
                // 隐藏复选框
                $(".cart-page .chk").hide();
                $(".cart-page .chooseTotal").hide();

                e.stopImmediatePropagation();
            });

            $(document).on('click', '.delCart', function (e) {
                var id = $(this).attr('data-id');
                //setup模式
                var cm = window.confirm("您确定要删除宝贝吗~！");
                if (cm) {
                    requestApi('/api/order/delCart', {id: id}, function (list) {
                        if (list.result == 1) {
                            $('.shop-cart  .item[data-id="' + id + '"]').fadeOut();
                            setTimeout(function () {
                                $('.shop-cart  .item[data-id="' + id + '"]').remove();
                            }, 500);
                        }
                    });
                }
                e.stopImmediatePropagation();
            });
        },

        /**
         * 结算
         */
        goToSettle: function () {
            // 全选
            $(document).on('click', ".settlement .chooseTotal", function (e) {
                if ($(this).attr('data-checked') == "checked") {
                    $(this).removeAttr('data-checked').removeClass("checked");
                    $(".shop-cart  .item .chk").removeAttr("data-checked").removeClass('checked');
                } else {
                    $(".shop-cart  .item .chk").attr("data-checked", "checked").addClass("checked");
                    $(this).attr('data-checked', "checked").addClass("checked");
                }

                calculateAll();
                e.stopImmediatePropagation();
            });

            // 单选
            $(document).on('click', ".shop-cart .item .chk", function (e) {
                if ($(this).attr('data-checked') == "checked") {
                    $(this).removeAttr("data-checked").removeClass("checked");
                } else {
                    $(this).attr("data-checked", "checked").addClass("checked");
                }

                calculateAll();
                e.stopImmediatePropagation();
            });

            // 计算所有
            function calculateAll() {
                var allParam = [];
                var total = 0;
                $('.shop-cart .item .chk.checked').each(function () {
                    var checked = $(this).attr("data-checked");
                    var id = $(this).attr("data-id");
                    var num = $(this).attr("data-num");
                    var spec = $(this).attr("data-spec");
                    var subtotal = parseFloat($(this).attr('data-item-total'));
//                    subtotal = subtotal == NaN ? 0 : subtotal;
                    if (checked == "checked") {
                        total += subtotal;
                        allParam.push(id);
                    }
                });

                // 设置总数
                $(".cart-total").html("&yen; " + total);

                //
                if (allParam.length > 0) {
                    // 设置参数
                    $(".goSettleBtn").attr("data-param", $.base64.encode(JSON.stringify(allParam)));
                    $('.goSettleBtn').addClass("btn-red").attr("data-canSettle", "checked");
                } else {
                    $(".goSettleBtn").removeAttr("data-param");
                    $('.goSettleBtn').removeClass("btn-red").removeAttr("data-canSettle");
                }
            }

            $(document).on('click', '.goSettleBtn', function (e) {
                // check
                if (!u.check_login()) {
                    return;
                }

                var param = $(this).attr("data-param");
                if ($(this).attr("data-canSettle") == "checked") {
                    // 跳转到结算
                    window.location.href = '/order/settle' + '/' + param + '?f=cart';
                } else {
                    tip.showTip("err", "请选择至少一件商品！", 2000);
                    return false;
                }
                e.stopImmediatePropagation();
            });
            $(".delBtn").on('click', function () {
                // check
                if (!u.check_login()) {
                    return;
                }
                var param = $(this).attr("data-param");
                if ($(".goSettleBtn").attr("data-canSettle") == "checked") {
                    var data = [];
                    $(".choosed").each(function () {
                        data.push($(this).attr('data-item-id'));
                    })
                    requestApi('/api/order/delProduct', {data: data}, function (res) {
                        if (res.result == 1) {
                            window.location = window.location;
                        }

                    })
                } else {
                    tip.showTip("err", "请选择至少一件商品！", 2000);
                }
            })
        }
    };

    window.cart = new ShopCart();

    // exports
    // 商品详情页面初始化
    exports.itemPageInit = function (checkLogin) {
        cart.choiceQuantity();
        cart.buyImmediately('.btn-buy', checkLogin);
        cart.addToCart('.btn-addToCart');
    };

    // 商品详情页面初始化
    exports.pointMallInit = function (checkLogin) {
        cart.choiceQuantity();
        cart.redemption('.btn-buy', checkLogin);
    };

    // 购物车页面初始化
    exports.cartPageInit = function () {
        cart.choiceQuantity();
        cart.goToSettle();
        cart.editCart();
    };
});