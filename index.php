<?php
//test
use Phalcon\Mvc\Application;

error_reporting(E_ALL);
ini_set('display_errors', 1);
define('ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
//initilize a error logger
$filePath = ROOT . "/Cache/log/error/" . date('Ymd') . '/' . date('H') . ".log";
$path = dirname($filePath);
if (!is_dir($path)) {
    mkdir($path, 0777, true);
}
if (file_exists($filePath)) {
    @chmod($filePath, FILE_WRITE_MODE);
}
$logger = new \Phalcon\Logger\Adapter\File($filePath);

try {

    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/Config/config.php";

    /**
     * Read auto-loader
     */
    include __DIR__ . "/Config/loader.php";

    /**
     * Read services
     */
    include __DIR__ . "/Config/services.php";

    /**
     * Handle the request
     */
    $application = new Application();

    /**
     * Assign the DI
     */
    $application->setDI($di);

    /**
     * Include modules
     */
    require __DIR__ . '/Config/modules.php';

    $app = $application->handle();
    echo $app->getContent();

} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    $logger->debug($e->getFile() . ' ' . $e->getLine() . '行 ' . $e->getMessage());
} catch (\PDOException $e) {
    echo $e->getMessage();
    $logger->debug($e->getFile() . ' ' . $e->getLine() . '行 ' . $e->getMessage());
} catch (\Exception $e) {
    echo $e->getMessage();
    $logger->debug($e->getFile() . ' ' . $e->getLine() . '行 ' . $e->getMessage());
}
